INSERT INTO `drugdistribution`.`user_account` (`name`, `surname`, `login`, `password`) VALUES ('admin', 'admin', 'admin', 'admin');

INSERT INTO `drugdistribution`.`user_role` (`name`) VALUES ('admin');
INSERT INTO `drugdistribution`.`user_role` (`name`) VALUES ('pharmacist');
INSERT INTO `drugdistribution`.`user_role` (`name`) VALUES ('driver');
INSERT INTO `drugdistribution`.`user_role` (`name`) VALUES ('drug_provider');

INSERT INTO `drugdistribution`.`user_role_relation` (`user_id`, `user_role_id`) VALUES ('1', '1');
