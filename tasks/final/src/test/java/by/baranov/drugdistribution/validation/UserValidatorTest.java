package by.baranov.drugdistribution.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UserValidatorTest {
	private Validator userValidator;

	@Before
	public void setUp() throws Exception {
		userValidator = new UserValidator();
	}

	@Test
	public void testValidateEmptyField() {
		ValidationResult validationResult = userValidator.validate("asd", "","123");
		assertFalse(validationResult.isEmpty());
	}
	
	@Test
	public void testValidateNotEmptyFields() {
		ValidationResult validationResult = userValidator.validate("asd", "qew","123");
		assertTrue(validationResult.isEmpty());
	}

	@After
	public void tearDown() throws Exception {
		userValidator = null;
	}

}
