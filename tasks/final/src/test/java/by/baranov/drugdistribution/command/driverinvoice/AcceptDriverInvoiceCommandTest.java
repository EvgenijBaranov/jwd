package by.baranov.drugdistribution.command.driverinvoice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.ApplicationContext;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.connectionpool.ConnectionPool;
import by.baranov.drugdistribution.connectionpool.ConnectionPoolImpl;
import by.baranov.drugdistribution.dao.DriverInvoiceDao;
import by.baranov.drugdistribution.dao.DrugOrderDao;
import by.baranov.drugdistribution.entity.DriverInvoice;
import by.baranov.drugdistribution.entity.DrugOrder;
import by.baranov.drugdistribution.entity.DrugOrderStatus;
import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.exception.DaoException;
import by.baranov.drugdistribution.exception.ServiceException;

@RunWith(MockitoJUnitRunner.class)
public class AcceptDriverInvoiceCommandTest {
	private static final Logger LOGGER = LogManager.getLogger();
	private ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
	private ApplicationContext applicationContext = ApplicationContext.getInstance();
	private Command acceptDriverInvoiceCommand;
	
	@Mock
	private HttpServletRequest request;
	@Mock
	private HttpServletResponse response;
	@Mock
	private HttpSession session;

	@Before
	public void setUp() throws Exception {
		
		applicationContext.initialize();
		
		acceptDriverInvoiceCommand = applicationContext.getBean(AcceptDriverInvoiceCommand.class);
		
		String sqlCreateDrugOrderTable = "CREATE TABLE drug_order (\n"
				+ "  id INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 1, INCREMENT BY 1) PRIMARY KEY,\n"
				+ "  pharmacy_id INTEGER,\n" 
				+ "  pharmacist_id INTEGER,\n" 
				+ "  drug_id INTEGER,\n" 
				+ "  drug_quantity INTEGER,\n" 
				+ "  creation_date TIMESTAMP(0),\n"
				+ "  termination_date TIMESTAMP(0),\n"  
				+ "  order_status VARCHAR(100) )";
		executeSql(sqlCreateDrugOrderTable);
		
		String sqlCreateDriverInvoiceTable = "CREATE TABLE driver_invoice (\n"
				+ "  id INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 1, INCREMENT BY 1) PRIMARY KEY,\n"
				+ "  driver_id INTEGER,\n" 
				+ "  drug_order_id INTEGER,\n" 
				+ "  departure_date TIMESTAMP(0),\n"
				+ "  delivery_date TIMESTAMP(0),\n"  
				+ "  drug_manufacture_date DATE,\n"  
				+ "  drug_expiry_date DATE,\n"  
				+ "  drug_price DECIMAL(10,2),\n"
				+ "	 FOREIGN KEY (drug_order_id) REFERENCES drug_order(id) )";
		executeSql(sqlCreateDriverInvoiceTable);
	}
	private void executeSql(String sql) {
		try (Connection connection = connectionPool.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
			preparedStatement.executeUpdate();
		} catch (SQLException exception) {
			LOGGER.log(Level.DEBUG, "SQL exception: " + exception);
		}
	}

	@Test
	public void testExecute() throws ServiceException, DaoException {
		User driver = new User(1L, "name", "surname", "login", "password");
		
		DrugOrder drugOrder = new DrugOrder();
		drugOrder.setPharmacyId(1L);
		drugOrder.setPharmacistId(1L);
		drugOrder.setDrugId(1L);
		drugOrder.setDrugQuantity(11);
		drugOrder.setCreationDate(LocalDateTime.parse("2001-01-01 01:01:01", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		drugOrder.setTerminationDate(null);
		drugOrder.setOrderStatus(DrugOrderStatus.CREATED);
		DrugOrderDao drugOrderDao = applicationContext.getBean(DrugOrderDao.class);
		drugOrderDao.save(drugOrder);
		
		DriverInvoice driverInvoice = new DriverInvoice();
		driverInvoice.setDriverId(null);
		driverInvoice.setDrugOrderId(drugOrder.getId());
		driverInvoice.setDepartureDate(null);
		driverInvoice.setDeliveryDate(null);
		driverInvoice.setDrugManufactureDate(LocalDate.parse("2006-06-06"));
		driverInvoice.setDrugExpiryDate(LocalDate.parse("2007-07-07"));
		driverInvoice.setDrugPrice(new BigDecimal("11.11"));
		DriverInvoiceDao driverInvoiceDao = applicationContext.getBean(DriverInvoiceDao.class);
		driverInvoiceDao.save(driverInvoice);

		when(request.getParameter(AppConstants.PARAMETER_DRIVER_INVOICE_ID)).thenReturn(driverInvoice.getId().toString());
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute(AppConstants.ATTRIBUTE_NAME_REGISTERED_USER)).thenReturn(driver);
		
		acceptDriverInvoiceCommand.execute(request, response);
		
		DrugOrder drugOrderAfterAccepting = drugOrderDao.getById(drugOrder.getId());
		assertEquals(DrugOrderStatus.DELIVERY_IN_PROGRESS, drugOrderAfterAccepting.getOrderStatus());
		
		DriverInvoice driverInvoiceAfterConfirming = driverInvoiceDao.getById(driverInvoice.getId());
		assertEquals(driver.getId(), driverInvoiceAfterConfirming.getDriverId());
		assertNotNull(driverInvoiceAfterConfirming.getDepartureDate());
	}
	
	@After
	public void tearDown() throws Exception {
		executeSql("DROP TABLE drug_order");
		executeSql("DROP TABLE driver_invoice");
		applicationContext.destroy();
		connectionPool.destroy();
		acceptDriverInvoiceCommand = null;
		request = null;
		response = null;
		session = null;
	}

}
