package by.baranov.drugdistribution.validation;

import static org.junit.Assert.assertFalse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DrugOrderValidatorTest {
	private Validator drugOrderValidator;

	@Before
	public void setUp() throws Exception {
		drugOrderValidator = new DrugOrderValidator();
	}

	@Test
	public void testValidateEmptyField() {
		ValidationResult validationResult = drugOrderValidator.validate("");
		assertFalse(validationResult.isEmpty());
	}
	
	@Test
	public void testValidateDrugQuantityField() {
		ValidationResult validationResult = drugOrderValidator.validate("10.23");
		assertFalse(validationResult.isEmpty());
	}

	@Test
	public void testValidateWrongDrugQuantityField() {
		ValidationResult validationResult = drugOrderValidator.validate("10.23s");
		assertFalse(validationResult.isEmpty());
	}

	@After
	public void tearDown() throws Exception {
		drugOrderValidator = null;
	}

}
