package by.baranov.drugdistribution.command;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.drugdistribution.command.driverinvoice.AcceptDriverInvoiceCommand;
import by.baranov.drugdistribution.service.DriverInvoiceService;


public class CommandRegistryImplTest {
	private CommandRegistryImpl registration;
	private DriverInvoiceService service;

	@Before
	public void init() {
		registration = new CommandRegistryImpl();
	}

	@Test
	public void testGetCommand() {
		registration.register(CommandName.ACCEPT_DRIVER_INVOICE, new AcceptDriverInvoiceCommand(service));
		String expectedNameCommand = "AcceptDriverInvoiceCommand";
		String foundNameCommmand = registration.getCommand(CommandName.ACCEPT_DRIVER_INVOICE).getClass()
				.getSimpleName();
		assertEquals(expectedNameCommand, foundNameCommmand);
	}

	@Test
	public void testGetCommand2() {
		String expectedNameCommand = "DefaultCommand";
		String foundNameCommmand = registration.getCommand(CommandName.DEFAULT).getClass().getSimpleName();
		assertEquals(expectedNameCommand, foundNameCommmand);
	}

	@After
	public void clean() {
		registration = null;
	}

}
