package by.baranov.drugdistribution.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.drugdistribution.ApplicationContext;
import by.baranov.drugdistribution.connectionpool.ConnectionPool;
import by.baranov.drugdistribution.connectionpool.ConnectionPoolImpl;
import by.baranov.drugdistribution.dao.DriverInvoiceDao;
import by.baranov.drugdistribution.dao.DrugOrderDao;
import by.baranov.drugdistribution.entity.DriverInvoice;
import by.baranov.drugdistribution.entity.DrugOrder;
import by.baranov.drugdistribution.entity.DrugOrderStatus;
import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.exception.DaoException;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DriverInvoiceService;

public class DriverInvoiceServiceImplTest {
	private static final Logger LOGGER = LogManager.getLogger();
	private ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
	private ApplicationContext applicationContext = ApplicationContext.getInstance();
	private DriverInvoiceService driverInvoiceService;
	private DrugOrder drugOrder;
	private DriverInvoice driverInvoice;

	@Before
	public void setUp() throws Exception {
		
		applicationContext.initialize();

		String sqlCreateDrugOrderTable = "CREATE TABLE drug_order (\n"
				+ "  id INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 1, INCREMENT BY 1) PRIMARY KEY,\n"
				+ "  pharmacy_id INTEGER,\n" 
				+ "  pharmacist_id INTEGER,\n" 
				+ "  drug_id INTEGER,\n" 
				+ "  drug_quantity INTEGER,\n" 
				+ "  creation_date TIMESTAMP(0),\n"
				+ "  termination_date TIMESTAMP(0),\n"  
				+ "  order_status VARCHAR(100) )";
		executeSql(sqlCreateDrugOrderTable);
		
		String sqlCreateDriverInvoiceTable = "CREATE TABLE driver_invoice (\n"
				+ "  id INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 1, INCREMENT BY 1) PRIMARY KEY,\n"
				+ "  driver_id INTEGER,\n" 
				+ "  drug_order_id INTEGER,\n" 
				+ "  departure_date TIMESTAMP(0),\n"
				+ "  delivery_date TIMESTAMP(0),\n"  
				+ "  drug_manufacture_date DATE,\n"  
				+ "  drug_expiry_date DATE,\n"  
				+ "  drug_price DECIMAL(10,2),\n"
				+ "	 FOREIGN KEY (drug_order_id) REFERENCES drug_order(id) )";
		executeSql(sqlCreateDriverInvoiceTable);
		
		driverInvoiceService = applicationContext.getBean(DriverInvoiceService.class);

		drugOrder = new DrugOrder();
		drugOrder.setPharmacyId(1L);
		drugOrder.setPharmacistId(1L);
		drugOrder.setDrugId(1L);
		drugOrder.setDrugQuantity(11);
		drugOrder.setCreationDate(LocalDateTime.parse("2001-01-01 01:01:01", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		drugOrder.setTerminationDate(LocalDateTime.parse("2002-02-02 02:02:02", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		drugOrder.setOrderStatus(DrugOrderStatus.CREATED);
		
		driverInvoice = new DriverInvoice();
		driverInvoice.setDriverId(1L);
		driverInvoice.setDrugOrderId(1L);
		driverInvoice.setDepartureDate(LocalDateTime.parse("2001-01-01 01:01:01", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		driverInvoice.setDeliveryDate(LocalDateTime.parse("2002-02-02 02:02:02", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		driverInvoice.setDrugManufactureDate(LocalDate.parse("2006-06-06"));
		driverInvoice.setDrugExpiryDate(LocalDate.parse("2007-07-07"));
		driverInvoice.setDrugPrice(new BigDecimal("11.11"));
		
	}

	private void executeSql(String sql) {
		try (Connection connection = connectionPool.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
			preparedStatement.executeUpdate();
		} catch (SQLException exception) {
			LOGGER.log(Level.DEBUG, "SQL exception: " + exception);
		}
	}
	
	@Test
	public void testGetAvailableInvoicesForDriver() throws ServiceException, DaoException {
		User driver = new User(1L, "Ivan1", "Ivanov1", "Login1", "Password1");
		DrugOrderDao drugOrderDao = applicationContext.getBean(DrugOrderDao.class);
		drugOrderDao.save(drugOrder);
		
		DriverInvoiceDao driverInvoiceDao = applicationContext.getBean(DriverInvoiceDao.class);
		driverInvoice.setDrugOrderId(drugOrder.getId());
		driverInvoice.setDriverId(driver.getId());
		driverInvoiceDao.save(driverInvoice);
		
		driverInvoice.setDriverId(null);
		driverInvoiceDao.save(driverInvoice);
		
		driverInvoice.setDriverId(driver.getId() + 1L);
		driverInvoiceDao.save(driverInvoice);
		
		List<DriverInvoice> driverInvoices = driverInvoiceDao.getAll();
		assertEquals(3, driverInvoices.size());
		
		List<DriverInvoice> found = driverInvoiceService.getAvailableInvoicesForDriver(driver);
		assertEquals(2, found.size());
	}
	
	@Test
	public void testDelete() throws ServiceException, DaoException {
		DrugOrderDao drugOrderDao = applicationContext.getBean(DrugOrderDao.class);
		drugOrderDao.save(drugOrder);
		
		DriverInvoiceDao driverInvoiceDao = applicationContext.getBean(DriverInvoiceDao.class);
		driverInvoice.setDrugOrderId(drugOrder.getId());
		driverInvoiceDao.save(driverInvoice);
		
		List<DriverInvoice> beforeDeleting = driverInvoiceDao.getAll();
		assertEquals(1, beforeDeleting.size());
		
		driverInvoiceService.delete(drugOrder.getId());
		List<DriverInvoice> afterDeleting = driverInvoiceDao.getAll();
		assertEquals(0, afterDeleting.size());
	}
	
	@Test(expected = ServiceException.class)
	public void testDeleteWasNotDelivered() throws ServiceException, DaoException {
		DrugOrderDao drugOrderDao = applicationContext.getBean(DrugOrderDao.class);
		drugOrderDao.save(drugOrder);
		
		DriverInvoiceDao driverInvoiceDao = applicationContext.getBean(DriverInvoiceDao.class);
		driverInvoice.setDrugOrderId(drugOrder.getId());
		driverInvoice.setDeliveryDate(null);
		driverInvoiceDao.save(driverInvoice);
		
		driverInvoiceService.delete(drugOrder.getId());
	}

	@Test(expected = ServiceException.class)
	public void testAcceptDriverInvoiceByDriverWasAcceptedBefore() throws DaoException, ServiceException {
		DrugOrderDao drugOrderDao = applicationContext.getBean(DrugOrderDao.class);
		drugOrderDao.save(drugOrder);
		
		DriverInvoiceDao driverInvoiceDao = applicationContext.getBean(DriverInvoiceDao.class);
		driverInvoiceDao.save(driverInvoice);
		
		driverInvoiceService.acceptDriverInvoiceByDriver(driverInvoice.getId(), new User());
	}
	
	@Test
	public void testAcceptDriverInvoiceByDriver() throws DaoException, ServiceException {
		DrugOrderDao drugOrderDao = applicationContext.getBean(DrugOrderDao.class);
		drugOrder.setTerminationDate(null);
		drugOrder.setOrderStatus(DrugOrderStatus.CREATED);
		drugOrderDao.save(drugOrder);
		
		DriverInvoiceDao driverInvoiceDao = applicationContext.getBean(DriverInvoiceDao.class);
		driverInvoice.setDepartureDate(null);
		driverInvoice.setDeliveryDate(null);
		driverInvoice.setDriverId(null);
		driverInvoice.setDrugOrderId(drugOrder.getId());
		driverInvoiceDao.save(driverInvoice);
		
		User driver = new User(1L, "name", "surname", "login", "password");
		driverInvoiceService.acceptDriverInvoiceByDriver(driverInvoice.getId(), driver);
		
		DrugOrder drugOrderFound = drugOrderDao.getById(drugOrder.getId());
		assertEquals(DrugOrderStatus.DELIVERY_IN_PROGRESS, drugOrderFound.getOrderStatus());
		
		DriverInvoice acceptedDriverInvoice = driverInvoiceDao.getDriverInvoiceByDrugOrderId(drugOrder.getId());
		assertNotNull(acceptedDriverInvoice.getDepartureDate());
		assertEquals(driver.getId(), acceptedDriverInvoice.getDriverId());
	}

	@After
	public void tearDown() throws Exception {
		executeSql("DROP TABLE drug_order");
		executeSql("DROP TABLE driver_invoice");
		applicationContext.destroy();
		connectionPool.destroy();
		driverInvoiceService = null;
		drugOrder = null;
		driverInvoice = null;
	}
}
