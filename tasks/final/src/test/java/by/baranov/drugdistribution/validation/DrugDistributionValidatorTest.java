package by.baranov.drugdistribution.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DrugDistributionValidatorTest {
	private Validator drugDistributionValidator;

	@Before
	public void setUp() throws Exception {
		drugDistributionValidator = new DrugDistributionValidator();
	}

	@Test
	public void testValidate() {
		ValidationResult validationResult = drugDistributionValidator.validate("2001-01-01", "2002-02-02", "12", "20.45");
		assertTrue(validationResult.isEmpty());
	}

	@Test
	public void testValidateEmptyFields() {
		ValidationResult validationResult = drugDistributionValidator.validate(" ", " ", " ", " ");
		assertFalse(validationResult.isEmpty());
	}
	
	@Test
	public void testValidateDateFieldsWrongData() {
		ValidationResult validationResult = drugDistributionValidator.validate("2003-01-01", "2002-02-02", "12", "20.45");
		assertEquals(1, validationResult.getValidationResult().size());
	}
	@Test
	public void testValidateDateFieldsWrongData2() {
		ValidationResult validationResult = drugDistributionValidator.validate("01-01-2001", "02-02-2002", "12", "20.45");
		assertEquals(2, validationResult.getValidationResult().size());
	}

	@Test
	public void testValidateWrongDrugQuantityField() {
		ValidationResult validationResult = drugDistributionValidator.validate("2001-01-01", "2002-02-02", "12.2", "20.45");
		assertEquals(1, validationResult.getValidationResult().size());
	}

	@Test
	public void testValidateWrongDrugPriceField() {
		ValidationResult validationResult = drugDistributionValidator.validate("2001-01-01", "2002-02-02", "12", "-20.45");
		assertEquals(1, validationResult.getValidationResult().size());
	}

	@After
	public void tearDown() throws Exception {
		drugDistributionValidator = null;
	}

}
