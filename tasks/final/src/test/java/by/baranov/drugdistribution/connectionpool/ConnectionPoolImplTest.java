package by.baranov.drugdistribution.connectionpool;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.Connection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ConnectionPoolImplTest {
	private ConnectionPool connectionPool;

	@Before
	public void setUp() throws Exception {
		connectionPool = ConnectionPoolImpl.getInstance();
	}

	@Test
	public void testGetInstance() {
		ConnectionPool connectionPoolSecond = ConnectionPoolImpl.getInstance();
		assertEquals(connectionPoolSecond, connectionPool);
	}

	@Test
	public void testGetConnection() {
		Connection connection  = connectionPool.getConnection();
		assertNotNull(connection);
	}

	@Test
	public void testReleaseConnection() {
		
		Connection connection = connectionPool.getConnection();
		connectionPool.releaseConnection(connection);
		
		int availableConnectionsQuantity = connectionPool.getAvailableConnectionsQuantity();
		assertEquals(1, availableConnectionsQuantity);
	}

	@Test
	public void testDestroyConnectionPool() {
		connectionPool.getConnection();
		connectionPool.destroy();
		
		int availableConnectionsQuantity = connectionPool.getAvailableConnectionsQuantity();
		int usedConnectionsQuantity = connectionPool.getUsedConnectionsQuantity();
		assertEquals(0, availableConnectionsQuantity);
		assertEquals(0, usedConnectionsQuantity);
	}

	@After
	public void tearDown() {
		connectionPool.destroy();
	}
}
