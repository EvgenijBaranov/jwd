package by.baranov.drugdistribution.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DrugValidatorTest {
	private Validator drugValidator;

	@Before
	public void setUp() throws Exception {
		drugValidator = new DrugValidator();
	}

	@Test
	public void testValidateEmptyField() {
		ValidationResult validationResult = drugValidator.validate("asd", "","123");
		assertFalse(validationResult.isEmpty());
	}
	
	@Test
	public void testValidateNotEmptyFields() {
		ValidationResult validationResult = drugValidator.validate("asd", "10.23","123");
		assertTrue(validationResult.isEmpty());
	}
	
	@Test
	public void testValidateWrongDrugDosageField() {
		ValidationResult validationResult = drugValidator.validate("asd", "10.23s","123");
		assertFalse(validationResult.isEmpty());
	}

	@After
	public void tearDown() throws Exception {
		drugValidator = null;
	}

}
