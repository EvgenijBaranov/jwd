package by.baranov.drugdistribution.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PharmacyValidatorTest {
	private Validator pharmacyValidator;

	@Before
	public void setUp() throws Exception {
		pharmacyValidator = new PharmacyValidator();
	}

	@Test
	public void testValidateEmptyField() {
		ValidationResult validationResult = pharmacyValidator.validate("asd", "","123");
		assertFalse(validationResult.isEmpty());
	}
	
	@Test
	public void testValidatePhoneNumberField() {
		ValidationResult validationResult = pharmacyValidator.validate("asd", "qew","+375(29)-111-11-11", "asd");
		assertTrue(validationResult.isEmpty());
	}
	
	@Test
	public void testValidateWrongPhoneNumberField() {
		ValidationResult validationResult = pharmacyValidator.validate("asd", "qew","+375-29-111-11-11", "asd");
		assertFalse(validationResult.isEmpty());
	}

	@After
	public void tearDown() throws Exception {
		pharmacyValidator = null;
	}

}
