package by.baranov.drugdistribution.command.user;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.drugdistribution.ApplicationContext;
import by.baranov.drugdistribution.SecurityContext;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.exception.DaoException;
import by.baranov.drugdistribution.exception.ServiceException;

@RunWith(MockitoJUnitRunner.class)
public class UserLogoutCommandTest {
	private ApplicationContext applicationContext = ApplicationContext.getInstance();
	private SecurityContext securityContext = SecurityContext.getInstance();
	private Command userLogoutCommand;
	
	@Mock
	private HttpServletRequest request;
	@Mock
	private HttpServletResponse response;
	@Spy
	private HttpSession session;

	@Before
	public void setUp() throws Exception {
		
		applicationContext.initialize();
		securityContext.initialize();
		
		userLogoutCommand = applicationContext.getBean(UserLogoutCommand.class);
	}
		

	@Test
	public void testExecute() throws ServiceException, DaoException {
		
		when(request.getSession()).thenReturn(session);
		when(session.getId()).thenReturn("QW1");
		
		userLogoutCommand.execute(request, response);
		
		verify(session, times(1)).invalidate();
	}
	
	@After
	public void tearDown() throws Exception {
		applicationContext.destroy();
		userLogoutCommand = null;
		request = null;
		response = null;
		session = null;
	}

}
