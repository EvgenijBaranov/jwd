package by.baranov.drugdistribution.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.drugdistribution.ApplicationContext;
import by.baranov.drugdistribution.connectionpool.ConnectionPool;
import by.baranov.drugdistribution.connectionpool.ConnectionPoolImpl;
import by.baranov.drugdistribution.dao.DriverInvoiceDao;
import by.baranov.drugdistribution.dao.DrugDao;
import by.baranov.drugdistribution.dao.DrugOrderDao;
import by.baranov.drugdistribution.dao.PharmacyDao;
import by.baranov.drugdistribution.entity.Address;
import by.baranov.drugdistribution.entity.DriverInvoice;
import by.baranov.drugdistribution.entity.Drug;
import by.baranov.drugdistribution.entity.DrugAvailability;
import by.baranov.drugdistribution.entity.DrugForm;
import by.baranov.drugdistribution.entity.DrugOrder;
import by.baranov.drugdistribution.entity.DrugOrderStatus;
import by.baranov.drugdistribution.entity.DrugProducer;
import by.baranov.drugdistribution.entity.Pharmacy;
import by.baranov.drugdistribution.entity.PharmacyHasDrug;
import by.baranov.drugdistribution.exception.DaoException;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DrugService;

public class DrugServiceImplTest {
	private static final Logger LOGGER = LogManager.getLogger();
	private ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
	private ApplicationContext applicationContext = ApplicationContext.getInstance();
	private DrugService drugService;
	private Drug drug1;
	private Drug drug2;
	private DrugForm drugForm;
	private DrugProducer drugProducer;
	private Address drugProducerAddress;
	private Pharmacy pharmacy;
	
	@Before
	public void setUp() throws Exception {
		
		applicationContext.initialize();
		
		String sqlCreateAddressTable = "CREATE TABLE address (\n"
				+ "  id INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 1, INCREMENT BY 1) PRIMARY KEY,\n"
				+ "  country VARCHAR(100),\n" 
				+ "  town VARCHAR(100),\n" 
				+ "  street VARCHAR(100),\n"
				+ "  house VARCHAR(100))";
		executeSql(sqlCreateAddressTable);
		
		String sqlCreateDrugProducerTable = "CREATE TABLE drug_producer (\n"
				+ "  id INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 1, INCREMENT BY 1) PRIMARY KEY,\n"
				+ "  name VARCHAR(100),\n" 
				+ "  licence_number VARCHAR(100),\n" 
				+ "  address_id INTEGER,\n"
				+ "  FOREIGN KEY (address_id) REFERENCES address(id))";
		executeSql(sqlCreateDrugProducerTable);
		
		String sqlCreateDrugTable = "CREATE TABLE drug (\n"
				+ "  id INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 1, INCREMENT BY 1) PRIMARY KEY,\n"
				+ "  name VARCHAR(100),\n" 
				+ "  dosage DOUBLE,\n" 
				+ "  availability VARCHAR(100),\n"
				+ "  producer_id INTEGER,\n"
				+ "  FOREIGN KEY (producer_id) REFERENCES drug_producer(id))";
		executeSql(sqlCreateDrugTable);
		
		String sqlCreatePharmacyTable = "CREATE TABLE pharmacy (\n"
				+ "  id INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 1, INCREMENT BY 1) PRIMARY KEY,\n"
				+ "  name VARCHAR(100),\n" 
				+ "  licence_number VARCHAR(100),\n" 
				+ "  phone VARCHAR(100),\n" 
				+ "  address_id INTEGER,\n"
				+ "  FOREIGN KEY (address_id) REFERENCES address(id) )";
		executeSql(sqlCreatePharmacyTable);
		
		String sqlCreatePharmacyHasDrugTable = "CREATE TABLE pharmacy_has_drug (\n"
				+ "  pharmacy_id INTEGER,\n" 
				+ "  drug_id INTEGER,\n" 
				+ "  drug_quantity INTEGER,\n" 
				+ "  manufacture_date DATE,\n" 
				+ "  expiry_date DATE,\n" 
				+ "  price DECIMAL,\n" 
				+ "  FOREIGN KEY (drug_id) REFERENCES drug(id),\n"
				+ "  FOREIGN KEY (pharmacy_id) REFERENCES pharmacy(id))";
		executeSql(sqlCreatePharmacyHasDrugTable);
		
		String sqlCreateDrugFormTable = "CREATE TABLE drug_form (\n"
				+ "  id INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 1, INCREMENT BY 1) PRIMARY KEY,\n"
				+ "  name VARCHAR(100))";
		executeSql(sqlCreateDrugFormTable);
		
		String sqlCreateDrugFormRelationTable = "CREATE TABLE drug_form_relation (\n"
				+ "  drug_id INTEGER,\n" 
				+ "  drug_form_id INTEGER,\n" 
				+ "  FOREIGN KEY (drug_id) REFERENCES drug(id) ON DELETE CASCADE,\n"
				+ "  FOREIGN KEY (drug_form_id) REFERENCES drug_form(id))";
		executeSql(sqlCreateDrugFormRelationTable);
		
		String sqlCreateDrugOrderTable = "CREATE TABLE drug_order (\n"
				+ "  id INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 1, INCREMENT BY 1) PRIMARY KEY,\n"
				+ "  pharmacy_id INTEGER,\n" 
				+ "  pharmacist_id INTEGER,\n" 
				+ "  drug_id INTEGER,\n" 
				+ "  drug_quantity INTEGER,\n" 
				+ "  creation_date TIMESTAMP(0),\n"
				+ "  termination_date TIMESTAMP(0),\n"  
				+ "  order_status VARCHAR(100),\n"
				+ "	 FOREIGN KEY (pharmacy_id) REFERENCES pharmacy(id),\n"
				+ "	 FOREIGN KEY (drug_id) REFERENCES drug(id) )";
		executeSql(sqlCreateDrugOrderTable);
		
		String sqlCreateDriverInvoiceTable = "CREATE TABLE driver_invoice (\n"
				+ "  id INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 1, INCREMENT BY 1) PRIMARY KEY,\n"
				+ "  driver_id INTEGER,\n" 
				+ "  drug_order_id INTEGER,\n" 
				+ "  departure_date TIMESTAMP(0),\n"
				+ "  delivery_date TIMESTAMP(0),\n"  
				+ "  drug_manufacture_date DATE,\n"  
				+ "  drug_expiry_date DATE,\n"  
				+ "  drug_price DECIMAL(10,2),\n"
				+ "	 FOREIGN KEY (drug_order_id) REFERENCES drug_order(id) )";
		executeSql(sqlCreateDriverInvoiceTable);
		
		
		drugService = applicationContext.getBean(DrugService.class);
		
		drug1 = new Drug();
		drug1.setName("Drug1");
		drug1.setDosage(11.11);
		drug1.setAvailability(DrugAvailability.FREE);
		drug1.setProducerId(1L);
		
		drug2 = new Drug();
		drug2.setName("Drug2");
		drug2.setDosage(22.22);
		drug2.setAvailability(DrugAvailability.WITH_PRESCRIPTION);
		drug2.setProducerId(2L);
		
		drugForm = new DrugForm();
		drugForm.setName("drops");
		
		drugProducer = new DrugProducer();
		drugProducer.setName("Produce1");
		drugProducer.setLicenceNumber("Licence1");
		drugProducer.setAddressId(1L);
		
		drugProducerAddress = new Address();
		drugProducerAddress.setCountry("Belarus1");
		drugProducerAddress.setTown("Minsk1");
		drugProducerAddress.setStreet("Pushkina1");
		drugProducerAddress.setHouse("12a1");
		
		pharmacy = new Pharmacy();
		pharmacy.setName("PharmacyN1");
		pharmacy.setLicenceNumber("AB1123CY1");
		pharmacy.setPhone("+375-29-111-11-11");
		pharmacy.setAddressId(1L);
		
		
	}
	private void executeSql(String sql) {
		try (Connection connection = connectionPool.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
			preparedStatement.executeUpdate();
		} catch (SQLException exception) {
			LOGGER.log(Level.DEBUG, "SQL exception: " + exception);
		}
	}
	
	@Test
	public void testGetAll() throws ServiceException {
		drugService.save(drug1, drugForm, drugProducer, drugProducerAddress);
		List<Drug> listDrugs = drugService.getAll();
		assertEquals(1, listDrugs.size());
	}

	@Test
	public void testGetById() throws ServiceException {
		drugService.save(drug1, drugForm, drugProducer, drugProducerAddress);
		Drug found = drugService.getById(drug1.getId());
		assertEquals(drug1, found);
		
	}

	@Test
	public void testSave() throws ServiceException {
		List<Drug> beforeSaving = drugService.getAll();
		assertEquals(0, beforeSaving.size());
		drugService.save(drug1, drugForm, drugProducer, drugProducerAddress);
		List<Drug> afterSaving = drugService.getAll();
		assertEquals(1, afterSaving.size());
	}

	@Test
	public void testUpdate() throws ServiceException {
		drugService.save(drug1, drugForm, drugProducer, drugProducerAddress);
		Drug foundBeforeUpdating = drugService.getById(drug1.getId());
		assertEquals(drug1, foundBeforeUpdating);
		drug1.setName(drug2.getName());
		drugService.update(drug1, drugForm, drugProducer, drugProducerAddress);
		Drug foundAfterUpdating = drugService.getById(drug1.getId());
		assertNotEquals(foundBeforeUpdating, foundAfterUpdating);
	}

	@Test
	public void testDelete() throws ServiceException {
		drugService.save(drug1, drugForm, drugProducer, drugProducerAddress);
		drugService.save(drug2, drugForm, drugProducer, drugProducerAddress);
		List<Drug> beforeDeleting = drugService.getAll();
		assertEquals(2, beforeDeleting.size());
		drugService.delete(drug1.getId());
		List<Drug> afterDeleting = drugService.getAll();
		assertEquals(1, afterDeleting.size());
	}


	@Test
	public void testGetAllDrugQuantityForPharmacy() throws ServiceException, DaoException {
		
		drugService.save(drug1, drugForm, drugProducer, drugProducerAddress);
		
		PharmacyDao pharmacyDao = applicationContext.getBean(PharmacyDao.class);
		pharmacyDao.save(pharmacy);
		
		DrugDao drugDao = applicationContext.getBean(DrugDao.class);
		int drugQuantity = 10;
		LocalDate drugManufactureDate = LocalDate.parse("2001-01-01");
		LocalDate drugExpiryDate = LocalDate.parse("2002-02-02");
		BigDecimal drugPrice = new BigDecimal("11.11");
		List<PharmacyHasDrug> beforeSaving = drugService.getAllDrugQuantityForPharmacy(pharmacy.getId(), drug1.getId());
		assertEquals(0, beforeSaving.size());
		drugDao.saveDrugForPharmacy(pharmacy.getId(), drug1.getId(), drugQuantity, drugManufactureDate, drugExpiryDate, drugPrice);
		List<PharmacyHasDrug> afterSaving = drugService.getAllDrugQuantityForPharmacy(pharmacy.getId(), drug1.getId());
		assertEquals(1, afterSaving.size());
	}

	@Test
	public void testGetAllDrugsByPharmacyId() throws ServiceException, DaoException {
		
		drugService.save(drug1, drugForm, drugProducer, drugProducerAddress);
		
		PharmacyDao pharmacyDao = applicationContext.getBean(PharmacyDao.class);
		pharmacyDao.save(pharmacy);
		
		DrugDao drugDao = applicationContext.getBean(DrugDao.class);
		int drugQuantity = 10;
		LocalDate drugManufactureDate = LocalDate.parse("2001-01-01");
		LocalDate drugExpiryDate = LocalDate.parse("2002-02-02");
		BigDecimal drugPrice = new BigDecimal("11.11");
		
		drugDao.saveDrugForPharmacy(pharmacy.getId(), drug1.getId(), drugQuantity, drugManufactureDate, drugExpiryDate, drugPrice);
		Set<Drug> drugs = drugService.getAllDrugsByPharmacyId(pharmacy.getId());
		assertEquals(1, drugs.size());
		
		LocalDate drugManufactureDate2 = LocalDate.parse("2000-02-02");
		drugDao.saveDrugForPharmacy(pharmacy.getId(), drug1.getId(), drugQuantity, drugManufactureDate2, drugExpiryDate, drugPrice);
		Set<Drug> drugs2 = drugService.getAllDrugsByPharmacyId(pharmacy.getId());
		assertEquals(1, drugs2.size());
		
		drugService.save(drug2, drugForm, drugProducer, drugProducerAddress);
		drugDao.saveDrugForPharmacy(pharmacy.getId(), drug2.getId(), drugQuantity, drugManufactureDate2, drugExpiryDate, drugPrice);
		Set<Drug> drugs3 = drugService.getAllDrugsByPharmacyId(pharmacy.getId());
		assertEquals(2, drugs3.size());
		
	}

	@Test
	public void testDistributeDrugs() throws DaoException, ServiceException {
		drugService.save(drug1, drugForm, drugProducer, drugProducerAddress);
		
		PharmacyDao pharmacyDao = applicationContext.getBean(PharmacyDao.class);
		pharmacyDao.save(pharmacy);
		
		DrugOrder drugOrder = new DrugOrder();
		drugOrder.setPharmacyId(pharmacy.getId());
		drugOrder.setPharmacistId(1L);
		drugOrder.setDrugId(drug1.getId());
		drugOrder.setDrugQuantity(100);
		drugOrder.setCreationDate(LocalDateTime.parse("2001-01-01 01:01:01", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		drugOrder.setTerminationDate(null);
		drugOrder.setOrderStatus(DrugOrderStatus.CREATED);
		DrugOrderDao drugOrderDao = applicationContext.getBean(DrugOrderDao.class);
		drugOrderDao.save(drugOrder);
		
		List<DrugOrder> drugOrdersBeforeDistributionDrugs = drugOrderDao.getAll();
		assertEquals(1, drugOrdersBeforeDistributionDrugs.size());
		
		DriverInvoiceDao driverInvoiceDao = applicationContext.getBean(DriverInvoiceDao.class);
		List<DriverInvoice> driverInvoicesBeforeDistributionDrugs = driverInvoiceDao.getAll();
		assertEquals(0, driverInvoicesBeforeDistributionDrugs.size());
		
		drugService.distributeDrugs(drug1.getId(), LocalDate.parse("2006-06-06"), LocalDate.parse("2007-07-07"), 50, new BigDecimal("10.90"));
		
		List<DrugOrder> drugOrdersAfterDistributionDrugs = drugOrderDao.getAll();
		DrugOrder existDrugOrderWithUpdatedDrugQuantity = drugOrdersAfterDistributionDrugs.get(0);
		DrugOrder newDrugOrderWithRemainedDrugQuantity = drugOrdersAfterDistributionDrugs.get(1);
		
		assertEquals(2, drugOrdersAfterDistributionDrugs.size());
		assertEquals(DrugOrderStatus.INVOICE_CREATED, existDrugOrderWithUpdatedDrugQuantity.getOrderStatus());
		assertEquals(50, existDrugOrderWithUpdatedDrugQuantity.getDrugQuantity());
		assertEquals(drugOrder.getPharmacyId(), newDrugOrderWithRemainedDrugQuantity.getPharmacyId());
		assertEquals(drugOrder.getPharmacistId(), newDrugOrderWithRemainedDrugQuantity.getPharmacistId());
		assertEquals(drugOrder.getDrugId(), newDrugOrderWithRemainedDrugQuantity.getDrugId());
		assertEquals(drugOrder.getDrugQuantity(), existDrugOrderWithUpdatedDrugQuantity.getDrugQuantity() + newDrugOrderWithRemainedDrugQuantity.getDrugQuantity());
		assertEquals(drugOrder.getCreationDate(), newDrugOrderWithRemainedDrugQuantity.getCreationDate());
		assertEquals(DrugOrderStatus.CREATED, newDrugOrderWithRemainedDrugQuantity.getOrderStatus());
		
		List<DriverInvoice> driverInvoicesAfterDistributionDrugs = driverInvoiceDao.getAll();
		assertEquals(1, driverInvoicesAfterDistributionDrugs.size());
		assertEquals(drugOrder.getId(), driverInvoicesAfterDistributionDrugs.get(0).getDrugOrderId());
		assertEquals(LocalDate.parse("2006-06-06"), driverInvoicesAfterDistributionDrugs.get(0).getDrugManufactureDate());
		assertEquals(LocalDate.parse("2007-07-07"), driverInvoicesAfterDistributionDrugs.get(0).getDrugExpiryDate());
		assertEquals(new BigDecimal("10.90"), driverInvoicesAfterDistributionDrugs.get(0).getDrugPrice());
	}
	
	@Test
	public void testDistributeDrugsNotDrugOrders() throws DaoException, ServiceException {
		drugService.save(drug1, drugForm, drugProducer, drugProducerAddress);
		
		PharmacyDao pharmacyDao = applicationContext.getBean(PharmacyDao.class);
		pharmacyDao.save(pharmacy);
		
		DrugOrder drugOrder = new DrugOrder();
		drugOrder.setPharmacyId(pharmacy.getId());
		drugOrder.setPharmacistId(1L);
		drugOrder.setDrugId(drug1.getId());
		drugOrder.setDrugQuantity(100);
		drugOrder.setCreationDate(LocalDateTime.parse("2001-01-01 01:01:01", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		drugOrder.setTerminationDate(null);
		drugOrder.setOrderStatus(DrugOrderStatus.CREATED);
		DrugOrderDao drugOrderDao = applicationContext.getBean(DrugOrderDao.class);
		drugOrderDao.save(drugOrder);
		
		drugService.save(drug2, drugForm, drugProducer, drugProducerAddress);
		
		String result = drugService.distributeDrugs(drug2.getId(), LocalDate.parse("2006-06-06"), LocalDate.parse("2007-07-07"), 50, new BigDecimal("10.90"));
		
		assertEquals("drug.service.distribute.drugs.not.drug.orders.message", result);
		
	}

	@After
	public void tearDown() throws Exception {
		executeSql("DROP TABLE address");
		executeSql("DROP TABLE drug_producer");
		executeSql("DROP TABLE drug");
		executeSql("DROP TABLE pharmacy");
		executeSql("DROP TABLE pharmacy_has_drug");
		executeSql("DROP TABLE drug_form");
		executeSql("DROP TABLE drug_form_relation");
		executeSql("DROP TABLE drug_order");
		executeSql("DROP TABLE driver_invoice");
		applicationContext.destroy();
		connectionPool.destroy();
		drugService = null;
		drug1 = null;
		drug2 = null;
		drugForm = null;
		drugProducer = null;
		drugProducerAddress = null;
	}
	
}
