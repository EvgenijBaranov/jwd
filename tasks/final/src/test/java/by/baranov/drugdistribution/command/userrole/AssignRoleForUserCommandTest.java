package by.baranov.drugdistribution.command.userrole;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.ApplicationContext;
import by.baranov.drugdistribution.SecurityContext;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.connectionpool.ConnectionPool;
import by.baranov.drugdistribution.connectionpool.ConnectionPoolImpl;
import by.baranov.drugdistribution.dao.UserRoleDao;
import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.entity.UserRole;
import by.baranov.drugdistribution.exception.DaoException;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.UserRoleService;
import by.baranov.drugdistribution.service.UserService;

@RunWith(MockitoJUnitRunner.class)
public class AssignRoleForUserCommandTest {
	private static final Logger LOGGER = LogManager.getLogger();
	private ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
	private ApplicationContext applicationContext = ApplicationContext.getInstance();
	private SecurityContext securityContext = SecurityContext.getInstance();
	private Command assignRoleForUserCommand;
	private UserService userService;
	private UserRoleService userRoleService;
	
	@Mock
	private HttpServletRequest request;
	@Mock
	private HttpServletResponse response;
	@Mock
	private HttpSession session;

	@Before
	public void setUp() throws Exception {
		
		applicationContext.initialize();
		securityContext.initialize();
		
		assignRoleForUserCommand = applicationContext.getBean(AssignRoleForUserCommand.class);
		userService = applicationContext.getBean(UserService.class);
		userRoleService = applicationContext.getBean(UserRoleService.class);
		
		String sqlCreateUserTable = "CREATE TABLE user_account (\n"
				+ "  id INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 1, INCREMENT BY 1) PRIMARY KEY,\n"
				+ "  name VARCHAR(100),\n" 
				+ "  surname VARCHAR(100),\n" 
				+ "  login VARCHAR(100),\n"
				+ "  password VARCHAR(100))";
		executeSql(sqlCreateUserTable);
		
		String sqlCreateUserRoleTable = "CREATE TABLE user_role (\n"
				+ "  id INTEGER GENERATED BY DEFAULT AS IDENTITY(START WITH 1, INCREMENT BY 1) PRIMARY KEY,\n"
				+ "  name VARCHAR(100))";
		executeSql(sqlCreateUserRoleTable);

		String sqlCreateUserRoleRelationTable = "CREATE TABLE user_role_relation (\n"
				+ "  user_id INTEGER,\n" 
				+ "  user_role_id INTEGER,\n" 
				+ "  FOREIGN KEY (user_id) REFERENCES user_account(id),\n"
				+ "  FOREIGN KEY (user_role_id) REFERENCES user_role(id))";
		executeSql(sqlCreateUserRoleRelationTable);
		
	}
	private void executeSql(String sql) {
		try (Connection connection = connectionPool.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
			preparedStatement.executeUpdate();
		} catch (SQLException exception) {
			LOGGER.log(Level.DEBUG, "SQL exception: " + exception);
		}
	}

	@Test
	public void testExecute() throws ServiceException, DaoException {
		User user = new User(1L, "name", "surname", "login", "password");
		userService.save(user);
		
		UserRoleDao userRoleDao = applicationContext.getBean(UserRoleDao.class);
		userRoleDao.save(UserRole.ADMIN);
		
		securityContext.setCurrentSessionId("sessionId");
		
		when(request.getParameter(AppConstants.PARAMETER_USER_ID)).thenReturn(user.getId().toString());
		when(request.getParameter(AppConstants.PARAMETER_USER_ROLE_ID)).thenReturn(UserRole.ADMIN.getId().toString());
		
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute(AppConstants.ATTRIBUTE_NAME_REGISTERED_USER)).thenReturn(user);
		
		assignRoleForUserCommand.execute(request, response);
		List<UserRole> roles = userRoleService.getUserRolesByUserId(user.getId());
		assertEquals(1, roles.size());
	}
	
	@After
	public void tearDown() throws Exception {
		executeSql("DROP TABLE user_account");
		executeSql("DROP TABLE user_role");
		executeSql("DROP TABLE user_role_relation");
		applicationContext.destroy();
		connectionPool.destroy();
		securityContext=null;
		assignRoleForUserCommand = null;
		userService = null;
		userRoleService = null;
		request = null;
		response = null;
		session = null;
	}
}
