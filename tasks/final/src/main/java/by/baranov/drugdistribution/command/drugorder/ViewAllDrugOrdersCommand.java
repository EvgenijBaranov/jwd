package by.baranov.drugdistribution.command.drugorder;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.Drug;
import by.baranov.drugdistribution.entity.DrugOrder;
import by.baranov.drugdistribution.entity.Pharmacy;
import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DrugOrderService;
import by.baranov.drugdistribution.service.DrugService;
import by.baranov.drugdistribution.service.PharmacyService;
import by.baranov.drugdistribution.service.UserService;

public class ViewAllDrugOrdersCommand implements Command{
	
	private static final String ATTRIBUTE_NAME_FOR_DRUG_ORDERS = "drugOrdersForPharmaciesWithPharmacist";
	private static final String ATTRIBUTE_NAME_FOR_PHARMACIES = "pharmacies";
	private static final String ATTRIBUTE_NAME_FOR_DRUGS = "drugs";
	private static final String ATTRIBUTE_NAME_FOR_PHARMACISTS = "pharmacists";
	private static final String ATTRIBUTE_VALUE_FOR_VIEW = "viewAllDrugOrders";
	private static final Logger LOGGER = LogManager.getLogger();

	private final DrugOrderService drugOrderService;
	private final PharmacyService pharmacyService;
	private final DrugService drugService;
	private final UserService userService;

	public ViewAllDrugOrdersCommand(DrugOrderService drugOrderService, PharmacyService pharmacyService, 
									DrugService drugService, UserService userService) {
		this.drugOrderService = drugOrderService;
		this.pharmacyService = pharmacyService;
		this.drugService = drugService;
		this.userService = userService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		try {
			User registeredUser = (User) request.getSession().getAttribute(AppConstants.ATTRIBUTE_NAME_REGISTERED_USER);
			List<DrugOrder> drugOrdersForPharmaciesWithPharmacist = drugOrderService.getDrugOrdersForPharmaciesByPharmacistId(registeredUser.getId());
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_DRUG_ORDERS, drugOrdersForPharmaciesWithPharmacist);
			
			Set<Pharmacy> pharmacies = new HashSet<>();
			Set<Drug> drugs =  new HashSet<>();
			Set<User> pharmacists =  new HashSet<>();
			for(DrugOrder drugOrder : drugOrdersForPharmaciesWithPharmacist) {
				
				Pharmacy pharmacy = pharmacyService.getById(drugOrder.getPharmacyId());
				pharmacies.add(pharmacy);
				
				Drug drug = drugService.getById(drugOrder.getDrugId());
				drugs.add(drug);
				
				User pharmacist = userService.getById(drugOrder.getPharmacistId());
				pharmacists.add(pharmacist);
			}
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_PHARMACIES, pharmacies);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_DRUGS, drugs);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_PHARMACISTS, pharmacists);
		} catch (ServiceException exception) {
			request.setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
			LOGGER.log(Level.ERROR, "Error of view all drug orders, which the pharmacist can view", exception);
		}
		request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_VIEW, ATTRIBUTE_VALUE_FOR_VIEW);
		LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
				getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_DRUG_ORDER_COMMANDS);
		return TypeCommandAction.FORWARD + AppConstants.FILE_PATH_FOR_DRUG_ORDER_COMMANDS;
	}
	
}
