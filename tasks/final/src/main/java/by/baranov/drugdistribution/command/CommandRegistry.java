package by.baranov.drugdistribution.command;

public interface CommandRegistry {
	
	void register(CommandName commandName, Command command);
	
	void remove(CommandName commandName);
	
	Command getCommand(CommandName commandName);
	
}
