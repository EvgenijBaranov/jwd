package by.baranov.drugdistribution.command.drugform;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.DrugForm;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DrugFormService;
import by.baranov.drugdistribution.validation.DrugFormValidator;
import by.baranov.drugdistribution.validation.ValidationResult;
import by.baranov.drugdistribution.validation.Validator;

public class AssignDrugFormForDrugCommand implements Command{
	private static final String ATTRIBUTE_NAME_FOR_DRUG_FORMS = "drugFormsForDrug";
	private static final Logger LOGGER = LogManager.getLogger();
	private final DrugFormService drugFormService;

	public AssignDrugFormForDrugCommand(DrugFormService drugFormService) {
		this.drugFormService = drugFormService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String inputedDrugId = request.getParameter(AppConstants.PARAMETER_DRUG_ID).trim();
		String inputedDrugFormName = request.getParameter(AppConstants.PARAMETER_DRUG_FORM_NAME).trim();
		
		Validator drugFormValidator = new DrugFormValidator();
		ValidationResult validationResult = drugFormValidator.validate(inputedDrugFormName);
		
		if (validationResult.isEmpty()) {
			try {
				Long drugId = Long.valueOf(inputedDrugId);
				
				DrugForm drugForm = new DrugForm();
				drugForm.setName(inputedDrugFormName);
				drugFormService.assignDrugFormForDrug(drugId, drugForm);
				
				List<DrugForm> drugForms = drugFormService.getDrugFormsByDrugId(drugId);
				request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_DRUG_FORMS, drugForms);
			} catch (ServiceException exception) {
				request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
				LOGGER.log(Level.ERROR, "Error of assigning drug form =" + inputedDrugFormName + " for drug with id=" + inputedDrugId, exception);
			} 
			LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
					getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_DRUG_COMMANDS);
		} else {
			request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_VALIDATION_RESULT, validationResult);
			LOGGER.log(Level.ERROR, "{} was executed with mistakes:{}. Page path for controller is {}",
					getClass().getSimpleName(), validationResult, AppConstants.FILE_PATH_FOR_DRUG_ORDER_COMMANDS);
		}
		
		return TypeCommandAction.REDIRECT + AppConstants.FILE_PATH_FOR_DRUG_COMMANDS;
	}
}
