package by.baranov.drugdistribution.dao;

import java.util.List;

import by.baranov.drugdistribution.entity.AbstractEntity;
import by.baranov.drugdistribution.exception.DaoException;

public interface CRUDOperation  <K, T extends AbstractEntity> {

	List<T> getAll() throws DaoException;
	
	T getById(K id) throws DaoException;	

	void save(T entity) throws DaoException;

	void update(T entity) throws DaoException;

	void delete (T entity) throws DaoException;
}
