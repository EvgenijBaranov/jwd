package by.baranov.drugdistribution.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.connectionpool.ConnectionManager;
import by.baranov.drugdistribution.dao.DrugOrderDao;
import by.baranov.drugdistribution.entity.DrugOrder;
import by.baranov.drugdistribution.entity.DrugOrderStatus;
import by.baranov.drugdistribution.exception.DaoException;

public class DrugOrderDaoImpl implements DrugOrderDao{
	private static final String GET_ALL_DRUG_ORDER = "SELECT id, pharmacy_id, pharmacist_id, drug_id, drug_quantity, creation_date, termination_date, order_status FROM drug_order";
	private static final String GET_DRUG_ORDER_BY_ID = "SELECT id, pharmacy_id, pharmacist_id, drug_id, drug_quantity, creation_date, termination_date, order_status FROM drug_order WHERE id=?";
	private static final String SAVE_DRUG_ORDER = "INSERT INTO drug_order (pharmacy_id, pharmacist_id, drug_id, drug_quantity, creation_date, termination_date, order_status) VALUES (?,?,?,?,?,?,?)";
	private static final String UPDATE_DRUG_ORDER = "UPDATE drug_order SET pharmacy_id=?, pharmacist_id=?, drug_id=?, drug_quantity=?, creation_date=?, termination_date=?, order_status=? WHERE id=?";
	private static final String DELETE_DRUG_ORDER = "DELETE FROM drug_order WHERE id=?";
	
	private static final String GET_ALL_DRUG_ORDERS_BY_PHARMACY_ID = "SELECT id, pharmacy_id, pharmacist_id, drug_id, drug_quantity, creation_date, termination_date, order_status FROM drug_order WHERE pharmacy_id=?";
	private static final String GET_ALL_DRUG_ORDERS_BY_DRUG_ID = "SELECT id, pharmacy_id, pharmacist_id, drug_id, drug_quantity, creation_date, termination_date, order_status FROM drug_order WHERE drug_id=?";
	
	private static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private static final Logger LOGGER = LogManager.getLogger();

	private ConnectionManager connectionManager;

	public DrugOrderDaoImpl(ConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	@Override
	public List<DrugOrder> getAll() throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_DRUG_ORDER);
				ResultSet resultSet = preparedStatement.executeQuery()) {
			List<DrugOrder> result = new ArrayList<>();
			while (resultSet.next()) {
				DrugOrder drugOrder = parseDrugOrder(resultSet);
				result.add(drugOrder);
			}
			LOGGER.log(Level.DEBUG, "Result of executing method getAll() : {}", result);
			return result;
		} catch (SQLException exception) {
			throw new DaoException("Error getting all drug orders", exception);
		} 
	}

	private DrugOrder parseDrugOrder(ResultSet resultSet) throws SQLException {
		DrugOrder drugOrder = new DrugOrder();
		drugOrder.setId(resultSet.getLong("id"));
		drugOrder.setPharmacyId(resultSet.getLong("pharmacy_id"));
		drugOrder.setPharmacistId(resultSet.getLong("pharmacist_id"));
		drugOrder.setDrugId(resultSet.getLong("drug_id"));
		drugOrder.setDrugQuantity(resultSet.getInt("drug_quantity"));
		
		String stringCreationDateTime = resultSet.getString("creation_date");
		if (stringCreationDateTime != null) {
			LocalDateTime creationDateTime = LocalDateTime.parse(stringCreationDateTime, DateTimeFormatter.ofPattern(DATE_TIME_FORMAT));
			drugOrder.setCreationDate(creationDateTime);
		}
		
		String stringTerminationDateTime = resultSet.getString("termination_date");
		if (stringTerminationDateTime != null) {
			LocalDateTime terminationDateTime = LocalDateTime.parse(stringTerminationDateTime, DateTimeFormatter.ofPattern(DATE_TIME_FORMAT));
			drugOrder.setTerminationDate(terminationDateTime);
		}
		
		String stringDrugOrderStatus = resultSet.getString("order_status");
		DrugOrderStatus drugOrderStatus = DrugOrderStatus.valueOf(stringDrugOrderStatus);
		drugOrder.setOrderStatus(drugOrderStatus);
		return drugOrder;
	}

	@Override
	public DrugOrder getById(Long id) throws DaoException {
		DrugOrder result = new DrugOrder();
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_DRUG_ORDER_BY_ID)) {
			preparedStatement.setLong(1, id);
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				resultSet.next();
				result = parseDrugOrder(resultSet);
				LOGGER.log(Level.DEBUG, "Result of executing method getById() : {}", result);
				return result;
			}
		} catch (SQLException exception) {
			throw new DaoException("Error getting drug order with id=" + id, exception);
		} 
	}

	@Override
	public void save(DrugOrder drugOrder) throws DaoException {

		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SAVE_DRUG_ORDER, Statement.RETURN_GENERATED_KEYS)) {
			int i = 0;
			preparedStatement.setLong(++i, drugOrder.getPharmacyId());
			preparedStatement.setLong(++i, drugOrder.getPharmacistId());
			preparedStatement.setLong(++i, drugOrder.getDrugId());
			preparedStatement.setInt(++i, drugOrder.getDrugQuantity());
			preparedStatement.setObject(++i, drugOrder.getCreationDate());
			preparedStatement.setObject(++i, drugOrder.getTerminationDate());
			preparedStatement.setString(++i, drugOrder.getOrderStatus().name());
			preparedStatement.executeUpdate();

			try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
				while (generatedKeys.next()) {
					drugOrder.setId(generatedKeys.getLong(1));
				}
			}
			LOGGER.log(Level.DEBUG, "Result of executing method save() is success");
		} catch (SQLException exception) {
			throw new DaoException("Error saving drug order=" + drugOrder, exception);
		}
	}

	@Override
	public void update(DrugOrder drugOrder) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_DRUG_ORDER)) {
			int i = 0;
			preparedStatement.setLong(++i, drugOrder.getPharmacyId());
			preparedStatement.setLong(++i, drugOrder.getPharmacistId());
			preparedStatement.setLong(++i, drugOrder.getDrugId());
			preparedStatement.setInt(++i, drugOrder.getDrugQuantity());
			preparedStatement.setObject(++i, drugOrder.getCreationDate());
			preparedStatement.setObject(++i, drugOrder.getTerminationDate());
			preparedStatement.setString(++i, drugOrder.getOrderStatus().name());
			preparedStatement.setLong(++i, drugOrder.getId());
			preparedStatement.executeUpdate();
			LOGGER.log(Level.DEBUG, "Result of executing method update() is success");
		} catch (SQLException exception) {
			throw new DaoException("Error updating drug order=" + drugOrder, exception);
		}
	}

	@Override
	public void delete(DrugOrder drugOrder) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(DELETE_DRUG_ORDER)) {
			preparedStatement.setLong(1, drugOrder.getId());
			preparedStatement.executeUpdate();
			LOGGER.log(Level.DEBUG, "Result of executing method delete() is success");
		} catch (SQLException exception) {
			throw new DaoException("Error deleting drug order=" + drugOrder, exception);
		}
	}

	@Override
	public List<DrugOrder> getDrugOrdersByPharmacyId(Long pharmacyId) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_DRUG_ORDERS_BY_PHARMACY_ID)) {
			preparedStatement.setLong(1, pharmacyId);
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				List<DrugOrder> result = new ArrayList<>();
				while(resultSet.next()) {
					DrugOrder drugOrder = parseDrugOrder(resultSet);
					result.add(drugOrder);
				}
				LOGGER.log(Level.DEBUG, "Result of executing method getDrugOrdersByPharmacyId() : {}", result);
				return result;
			}
		} catch (SQLException exception) {
			throw new DaoException("Error getting drug orders by pharmacy id=" + pharmacyId, exception);
		} 
	}

	@Override
	public List<DrugOrder> getDrugOrdersByDrugId(Long drugId) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_DRUG_ORDERS_BY_DRUG_ID)) {
			preparedStatement.setLong(1, drugId);
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				List<DrugOrder> result = new ArrayList<>();
				while(resultSet.next()) {
					DrugOrder drugOrder = parseDrugOrder(resultSet);
					result.add(drugOrder);
				}
				LOGGER.log(Level.DEBUG, "Result of executing method getDrugOrdersByDrugId() : {}", result);
				return result;
			}
		} catch (SQLException exception) {
			throw new DaoException("Error getting drug orders by drug id=" + drugId, exception);
		} 
	}
}
