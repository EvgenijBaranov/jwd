package by.baranov.drugdistribution.service.impl;

import java.util.List;

import by.baranov.drugdistribution.connectionpool.Transactional;
import by.baranov.drugdistribution.dao.AddressDao;
import by.baranov.drugdistribution.dao.PharmacyDao;
import by.baranov.drugdistribution.entity.Address;
import by.baranov.drugdistribution.entity.Pharmacy;
import by.baranov.drugdistribution.exception.DaoException;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.PharmacyService;

public class PharmacyServiceImpl implements PharmacyService{
	private final PharmacyDao pharmacyDao;
	private final AddressDao addressDao;

	public PharmacyServiceImpl(PharmacyDao pharmacyDao, AddressDao addressDao) {
		this.pharmacyDao = pharmacyDao;
		this.addressDao = addressDao;
	}

	@Override
	public List<Pharmacy> getAll() throws ServiceException {
		try {
			return pharmacyDao.getAll();
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	@Override
	public Pharmacy getById(Long id) throws ServiceException {
		try {
			return pharmacyDao.getById(id);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}
	
	@Transactional
	@Override
	public void delete(Long id) throws ServiceException {
		try {
			Pharmacy pharmacy = pharmacyDao.getById(id);
			pharmacyDao.delete(pharmacy);
		} catch (DaoException exception) {
			throw new ServiceException("error.pharmacy.service.pharmacy.delete.message", exception);
		}
	}
	
	@Transactional
	@Override
	public void save(Pharmacy pharmacy, Address pharmacyAddress) throws ServiceException {
			
		try {
			savePharmacyAddress(pharmacy, pharmacyAddress);
			savePharmacy(pharmacy);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	private void savePharmacy(Pharmacy pharmacy) throws DaoException, ServiceException {
		List<Pharmacy> pharmacies = pharmacyDao.getAll();
		boolean isPharmacyExist = pharmacies.stream()
				.anyMatch(pharmacyFromDataBase -> pharmacyFromDataBase.getLicenceNumber().equals(pharmacy.getLicenceNumber()));
		if (isPharmacyExist) {
			throw new ServiceException("error.pharmacy.service.pharmacy.exist.message");
		}
		pharmacyDao.save(pharmacy);
	}

	private void savePharmacyAddress(Pharmacy pharmacy, Address pharmacyAddress) throws DaoException {
		List<Address> listAddress = addressDao.getAll();
		boolean isPharmacyAddressExist = listAddress.stream()
				.anyMatch(addressFromDataBase -> addressFromDataBase.equals(pharmacyAddress));
		if (isPharmacyAddressExist) {
			Long pharmacyAddressId = listAddress.stream()
					.filter(addressFromDataBase -> addressFromDataBase.equals(pharmacyAddress))
					.map(Address::getId)
					.findAny().orElseThrow(NullPointerException::new);
			pharmacy.setAddressId(pharmacyAddressId);
		} else {
			addressDao.save(pharmacyAddress);
			pharmacy.setAddressId(pharmacyAddress.getId());
		}
	}

	@Transactional
	@Override
	public void update(Pharmacy pharmacy, Address pharmacyAddress) throws ServiceException {
		try {
			savePharmacyAddress(pharmacy, pharmacyAddress);
			updatePharmacy(pharmacy);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	private void updatePharmacy(Pharmacy pharmacy) throws DaoException, ServiceException {
		List<Pharmacy> pharmacies = pharmacyDao.getAll();
		boolean isPharmacyExist = pharmacies.stream()
				.filter(pharmacyFromDataBase -> !pharmacyFromDataBase.getId().equals(pharmacy.getId()))
				.anyMatch(pharmacyFromDataBase -> pharmacyFromDataBase.getLicenceNumber().equals(pharmacy.getLicenceNumber()));
		if (isPharmacyExist) {
			throw new ServiceException("error.pharmacy.service.pharmacy.exist.message");
		}
		
		pharmacyDao.update(pharmacy);
	}

	@Override
	public List<Pharmacy> getPharmaciesByParmacistId(Long pharmacistId) throws ServiceException {
		try {
			return pharmacyDao.getPharmaciesByPharmacistId(pharmacistId);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

}
