package by.baranov.drugdistribution.command.drug;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.Drug;
import by.baranov.drugdistribution.entity.Pharmacy;
import by.baranov.drugdistribution.entity.PharmacyHasDrug;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DrugService;
import by.baranov.drugdistribution.service.PharmacyService;

public class ViewAllDrugsForPharmacyCommand implements Command{
	private static final String ATTRIBUTE_NAME_FOR_DRUGS = "drugsForPharmacy";
	private static final String ATTRIBUTE_NAME_FOR_ALL_DRUGS = "drugsInApplication";
	private static final String ATTRIBUTE_NAME_FOR_DRUG_QUANTITY_IN_PHARMACY = "drugsQuantityForPharmacy";
	private static final String ATTRIBUTE_NAME_FOR_PHARMACY = "pharmacy";
	private static final String ATTRIBUTE_VALUE_FOR_VIEW = "viewDrugsForPharmacy";
	private static final Logger LOGGER = LogManager.getLogger();
	private final PharmacyService pharmacyService;
	private final DrugService drugService;

	public ViewAllDrugsForPharmacyCommand(PharmacyService pharmacyService, DrugService drugService) {
		this.pharmacyService = pharmacyService;
		this.drugService = drugService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String inputedPharmacyId = request.getParameter(AppConstants.PARAMETER_PHARMACY_ID).trim();
		try {
			Long pharmacyId = Long.valueOf(inputedPharmacyId);
			
			Pharmacy pharmacy = pharmacyService.getById(pharmacyId);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_PHARMACY, pharmacy);
			
			Set<Drug> drugsForPharmacy = drugService.getAllDrugsByPharmacyId(pharmacyId);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_DRUGS, drugsForPharmacy);

			List<Drug> drugsInApplication = drugService.getAll();
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_ALL_DRUGS, drugsInApplication);

			List<List<PharmacyHasDrug>> listDrugQuantityForPharmacy = new ArrayList<>();
			for(Drug drug : drugsForPharmacy) {
				List<PharmacyHasDrug> drugQuantityForPharmacy = drugService.getAllDrugQuantityForPharmacy(pharmacyId, drug.getId());
				listDrugQuantityForPharmacy.add(drugQuantityForPharmacy);
			}
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_DRUG_QUANTITY_IN_PHARMACY, listDrugQuantityForPharmacy);
		} catch (ServiceException exception) {
			request.setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
			LOGGER.log(Level.ERROR, "Error of view all drugs for pharmacy with id= " + inputedPharmacyId + ": ", exception);
		} 
		request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_VIEW, ATTRIBUTE_VALUE_FOR_VIEW);
		LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
				getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_PHARMACY_COMMANDS);
		return TypeCommandAction.FORWARD + AppConstants.FILE_PATH_FOR_PHARMACY_COMMANDS;
	}
}
