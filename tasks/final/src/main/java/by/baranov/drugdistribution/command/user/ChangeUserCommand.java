package by.baranov.drugdistribution.command.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.UserService;
import by.baranov.drugdistribution.validation.UserValidator;
import by.baranov.drugdistribution.validation.ValidationResult;
import by.baranov.drugdistribution.validation.Validator;

public class ChangeUserCommand implements Command{

	private static final Logger LOGGER = LogManager.getLogger();
	private final UserService userService;

	public ChangeUserCommand(UserService userService) {
		this.userService = userService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		String inputedUserId = request.getParameter(AppConstants.PARAMETER_USER_ID).trim();
		String inputedUserName = request.getParameter(AppConstants.PARAMETER_USER_NAME).trim();
		String inputedUserSurname = request.getParameter(AppConstants.PARAMETER_USER_SURNAME).trim();
		String inputedUserLogin = request.getParameter(AppConstants.PARAMETER_USER_LOGIN).trim();
		String inputedUserPassword = request.getParameter(AppConstants.PARAMETER_USER_PASSWORD).trim();

		Validator userValidator = new UserValidator();
		ValidationResult validationResult = userValidator.validate(inputedUserLogin, inputedUserPassword,
																	inputedUserName, inputedUserSurname);
		if(validationResult.isEmpty()) {
			try {
				User user = new User();
				Long userId = Long.valueOf(inputedUserId);
				user.setId(userId);
				user.setName(inputedUserName);
				user.setSurname(inputedUserSurname);
				user.setLogin(inputedUserLogin);
				user.setPassword(inputedUserPassword);
				userService.update(user);
			} catch (ServiceException exception) {
				request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
				LOGGER.log(Level.ERROR, "User changing error", exception);
			}
			LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
					getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_USER_COMMANDS);
		} else {
			request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_VALIDATION_RESULT, validationResult);
			LOGGER.log(Level.ERROR, "{} was executed with mistakes:{}. Page path for controller is {}",
					getClass().getSimpleName(), validationResult, AppConstants.FILE_PATH_FOR_USER_COMMANDS);
		}
		return TypeCommandAction.REDIRECT + AppConstants.FILE_PATH_FOR_USER_COMMANDS;
	}
	
}
