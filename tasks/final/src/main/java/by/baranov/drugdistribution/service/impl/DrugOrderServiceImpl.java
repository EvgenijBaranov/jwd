package by.baranov.drugdistribution.service.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.connectionpool.Transactional;
import by.baranov.drugdistribution.dao.DriverInvoiceDao;
import by.baranov.drugdistribution.dao.DrugDao;
import by.baranov.drugdistribution.dao.DrugOrderDao;
import by.baranov.drugdistribution.dao.PharmacyDao;
import by.baranov.drugdistribution.entity.DriverInvoice;
import by.baranov.drugdistribution.entity.DrugOrder;
import by.baranov.drugdistribution.entity.DrugOrderStatus;
import by.baranov.drugdistribution.entity.Pharmacy;
import by.baranov.drugdistribution.entity.PharmacyHasDrug;
import by.baranov.drugdistribution.exception.DaoException;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DrugOrderService;

public class DrugOrderServiceImpl implements DrugOrderService{
	private static final Logger LOGGER = LogManager.getLogger();
	private final DrugOrderDao drugOrderDao;
	private final PharmacyDao pharmacyDao;
	private final DriverInvoiceDao driverInvoiceDao;
	private final DrugDao drugDao;

	public DrugOrderServiceImpl(DrugOrderDao drugOrderDao, PharmacyDao pharmacyDao, DriverInvoiceDao driverInvoiceDao, DrugDao drugDao) {
		this.drugOrderDao = drugOrderDao;
		this.pharmacyDao = pharmacyDao;
		this.driverInvoiceDao = driverInvoiceDao;
		this.drugDao = drugDao;
	}

	@Override
	public DrugOrder getById(Long id) throws ServiceException {
		try {
			return drugOrderDao.getById(id);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}
	
	@Transactional
	@Override
	public void save(DrugOrder drugOrder) throws ServiceException {
		try {
			drugOrderDao.save(drugOrder);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	@Transactional
	@Override
	public void update(Long drugOrderId, int drugQuantity) throws ServiceException {
		try {
			DrugOrder drugOrder = drugOrderDao.getById(drugOrderId);
			if (drugOrder.getOrderStatus() != DrugOrderStatus.CREATED) {
				throw new ServiceException("error.drug.order.service.drug.order.processed.message");
			}
			drugOrder.setDrugQuantity(drugQuantity);
			LocalDateTime creationOrderDate = LocalDateTime.now();
			drugOrder.setCreationDate(creationOrderDate);
			drugOrderDao.update(drugOrder);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}

	}

	@Transactional
	@Override
	public void delete(Long id) throws ServiceException {
		try {
			DrugOrder drugOrder = drugOrderDao.getById(id);
			if (drugOrder.getOrderStatus() == DrugOrderStatus.INVOICE_CREATED || 
					drugOrder.getOrderStatus() == DrugOrderStatus.DELIVERY_IN_PROGRESS) {
				throw new ServiceException("error.drug.order.service.drug.order.delete.message");
			}
			drugOrderDao.delete(drugOrder);
		} catch (DaoException exception) {
			throw new ServiceException("error.drug.order.service.drug.order.delete.from.database.message", exception);
		}
	}

	@Override
	public List<DrugOrder> getDrugOrdersForPharmaciesByPharmacistId(Long pharmacistId) throws ServiceException {
		try {
			List<Pharmacy> pharmaciesForPharmacist = pharmacyDao.getPharmaciesByPharmacistId(pharmacistId);
			
			List<DrugOrder> drugOrdersForPharmacies = new ArrayList<>();
			for(Pharmacy pharmacy : pharmaciesForPharmacist) {
				List<DrugOrder> drugOrdersForPharmacy = drugOrderDao.getDrugOrdersByPharmacyId(pharmacy.getId());
				drugOrdersForPharmacies.addAll(drugOrdersForPharmacy);
			}
			LOGGER.log(Level.DEBUG, "getDrugOrdersForPharmaciesByPharmacistId() was executed with result = {}", drugOrdersForPharmacies);
			return drugOrdersForPharmacies;
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	@Transactional
	@Override
	public void confirmDrugOrderDelivery(Long drugOrderId) throws ServiceException {
		try {
			
			DrugOrder drugOrder = drugOrderDao.getById(drugOrderId);
			
			if (drugOrder.getOrderStatus() == DrugOrderStatus.DELIVERED) {
				throw new ServiceException("error.drug.order.service.drug.order.confirm.delivery.message");
			}
			
			DriverInvoice driverInvoice = driverInvoiceDao.getDriverInvoiceByDrugOrderId(drugOrderId);
			if(driverInvoice.getDriverId() == null) {
				throw new ServiceException("error.drug.order.service.drug.order.confirm.delivery.not.driver.message");
			}
			
			LocalDateTime deliveryDate = LocalDateTime.now();
			
			setDriverInvoiceDeliveryDate(driverInvoice, deliveryDate);
			setDrugOrderTerminationDateAndChangeStatus(drugOrder, deliveryDate);
			setDrugQuantityForPharmacy(driverInvoice, drugOrder);
			
		} catch (DaoException exception) {
			throw new ServiceException("error.drug.order.service.drug.order.confirm.delivery.from.database.message", exception);
		}
	}

	private void setDrugOrderTerminationDateAndChangeStatus(DrugOrder drugOrder, LocalDateTime deliveryDate) throws DaoException {
		drugOrder.setTerminationDate(deliveryDate);
		drugOrder.setOrderStatus(DrugOrderStatus.DELIVERED);
		drugOrderDao.update(drugOrder);
	}

	private void setDriverInvoiceDeliveryDate(DriverInvoice driverInvoice, LocalDateTime deliveryDate) throws DaoException {
		driverInvoice.setDeliveryDate(deliveryDate);
		driverInvoiceDao.update(driverInvoice);
	}

	private void setDrugQuantityForPharmacy(DriverInvoice driverInvoice, DrugOrder drugOrder) throws DaoException {
		
		Long pharmacyId = drugOrder.getPharmacyId();
		Long drugId = drugOrder.getDrugId();
		int drugQuantity = drugOrder.getDrugQuantity();
		LocalDate drugManufactureDate = driverInvoice.getDrugManufactureDate();
		LocalDate drugExpiryDate = driverInvoice.getDrugExpiryDate();
		BigDecimal drugPrice = driverInvoice.getDrugPrice();
		
		PharmacyHasDrug pharmacyHasDrug = new PharmacyHasDrug(pharmacyId, drugId, drugQuantity, 
				drugManufactureDate, drugExpiryDate, drugPrice);
		List<PharmacyHasDrug> pharmacyHasDrugs = drugDao.getAllDrugsForPharmacy(pharmacyId, drugId);
		boolean isPharmacyHasDrugExist = pharmacyHasDrugs.stream()
				.anyMatch(pharmacyHasDrugFromDataBase -> pharmacyHasDrugFromDataBase.equals(pharmacyHasDrug));
		if (isPharmacyHasDrugExist) {
			int drugQuantityInPharmacy = drugDao.getDrugQuantityForPharmacy(pharmacyId, drugId, 
																			drugManufactureDate, drugExpiryDate, drugPrice);
			drugQuantity += drugQuantityInPharmacy;
			drugDao.updateDrugQuantityForPharmacy(pharmacyId, drugId, drugQuantity, drugManufactureDate, drugExpiryDate, drugPrice);
		} else {
			drugDao.saveDrugForPharmacy(pharmacyId, drugId, drugQuantity, drugManufactureDate, drugExpiryDate, drugPrice);
		}
	}
	
}
