package by.baranov.drugdistribution.dao;

import java.util.List;

import by.baranov.drugdistribution.entity.DrugForm;
import by.baranov.drugdistribution.exception.DaoException;

public interface DrugFormDao extends CRUDOperation<Long, DrugForm>{

	void assignDrugFormForDrug(Long drugId, Long drugFormId) throws DaoException;
	
	List<DrugForm> getDrugFormsByDrugId(Long drugId) throws DaoException;
	
	void deleteDrugFormForDrug(Long drugId, Long drugFormId) throws DaoException;
	
}
