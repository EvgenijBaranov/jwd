package by.baranov.drugdistribution.exception;

public class DaoException extends Exception{

	private static final long serialVersionUID = 8162313651382165527L;

	public DaoException(String message, Throwable cause) {
		super(message, cause);
	}

	public DaoException(String message) {
		super(message);
	}

	
	
}
