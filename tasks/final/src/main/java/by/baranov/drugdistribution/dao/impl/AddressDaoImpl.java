package by.baranov.drugdistribution.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.connectionpool.ConnectionManager;
import by.baranov.drugdistribution.dao.AddressDao;
import by.baranov.drugdistribution.entity.Address;
import by.baranov.drugdistribution.exception.DaoException;

public class AddressDaoImpl implements AddressDao {

	private static final String GET_ALL_ADDRESS = "SELECT id, country, town, street, house FROM address";
	private static final String GET_ADDRESS_BY_ID = "SELECT id, country, town, street, house FROM address WHERE id=?";
	private static final String SAVE_ADDRESS = "INSERT INTO address (country, town, street, house) VALUES (?,?,?,?)";
	private static final String UPDATE_ADDRESS = "UPDATE address SET country=?, town=?, street=?, house=? WHERE id=?";
	private static final String DELETE_ADDRESS = "DELETE FROM address WHERE id=?";

	private static final Logger LOGGER = LogManager.getLogger();
	private ConnectionManager connectionManager;

	public AddressDaoImpl(ConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	@Override
	public List<Address> getAll() throws DaoException {

		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_ADDRESS);
				ResultSet resultSet = preparedStatement.executeQuery()) {
			List<Address> result = new ArrayList<>();
			while (resultSet.next()) {
				Address address = parseAddress(resultSet);
				result.add(address);
			}
			LOGGER.log(Level.DEBUG, "Result of executing method getAll() : {}", result);
			return result;
		} catch (SQLException exception) {
			throw new DaoException("Error getting all addresses", exception);
		}
	}

	private Address parseAddress(ResultSet resultSet) throws SQLException {
		Address address = new Address();
		address.setId(resultSet.getLong("id"));
		address.setCountry(resultSet.getString("country"));
		address.setTown(resultSet.getString("town"));
		address.setStreet(resultSet.getString("street"));
		address.setHouse(resultSet.getString("house"));
		return address;
	}

	@Override
	public Address getById(Long id) throws DaoException {
		
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_ADDRESS_BY_ID)) {
			preparedStatement.setLong(1, id);
			
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				resultSet.next();
				Address result = parseAddress(resultSet);
				LOGGER.log(Level.DEBUG, "Result of executing method getById() : {}", result);
				return result;
			}
		} catch (SQLException exception) {
			throw new DaoException("Error getting address by address id=" + id, exception);
		}
	}

	@Override
	public void save(Address address) throws DaoException {

		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SAVE_ADDRESS, Statement.RETURN_GENERATED_KEYS)) {
			int i = 0;
			preparedStatement.setString(++i, address.getCountry());
			preparedStatement.setString(++i, address.getTown());
			preparedStatement.setString(++i, address.getStreet());
			preparedStatement.setString(++i, address.getHouse());
			preparedStatement.executeUpdate();

			try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
				while (generatedKeys.next()) {
					address.setId(generatedKeys.getLong(1));
				}
				LOGGER.log(Level.DEBUG, "Result of executing method save() is success");
			}
		} catch (SQLException exception) {
			throw new DaoException("Error saving address=" + address, exception);
		}
	}

	@Override
	public void update(Address address) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ADDRESS)) {
			int i = 0;
			preparedStatement.setString(++i, address.getCountry());
			preparedStatement.setString(++i, address.getTown());
			preparedStatement.setString(++i, address.getStreet());
			preparedStatement.setString(++i, address.getHouse());
			preparedStatement.setLong(++i, address.getId());
			preparedStatement.executeUpdate();
			LOGGER.log(Level.DEBUG, "Result of executing method update() is success");
		} catch (SQLException exception) {
			throw new DaoException("Error updating address=" + address, exception);
		}
	}

	@Override
	public void delete(Address address) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ADDRESS)) {
			preparedStatement.setLong(1, address.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException exception) {
			throw new DaoException("Error deleting address" + address, exception);
		}
		LOGGER.log(Level.DEBUG, "Result of executing method delete() is success");
	}

}
