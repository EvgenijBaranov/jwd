package by.baranov.drugdistribution.command.drug;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.Address;
import by.baranov.drugdistribution.entity.Drug;
import by.baranov.drugdistribution.entity.DrugAvailability;
import by.baranov.drugdistribution.entity.DrugForm;
import by.baranov.drugdistribution.entity.DrugProducer;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DrugService;
import by.baranov.drugdistribution.validation.DrugValidator;
import by.baranov.drugdistribution.validation.ValidationResult;
import by.baranov.drugdistribution.validation.Validator;

public class ChangeDrugCommand implements Command{
	
	private static final Logger LOGGER = LogManager.getLogger();
	private final DrugService drugService;

	public ChangeDrugCommand(DrugService drugService) {
		this.drugService = drugService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String inputedDrugId = request.getParameter(AppConstants.PARAMETER_DRUG_ID).trim();
		String inputedDrugName = request.getParameter(AppConstants.PARAMETER_DRUG_NAME).trim();
		String inputedDrugDosage = request.getParameter(AppConstants.PARAMETER_DRUG_DOSAGE).trim();
		String inputedDrugFormName = request.getParameter(AppConstants.PARAMETER_DRUG_FORM_NAME).trim();
		String inputedDrugAvailability = request.getParameter(AppConstants.PARAMETER_DRUG_AVAILABILITY).trim();
		String inputedDrugProducerName = request.getParameter(AppConstants.PARAMETER_DRUG_PRODUCER_NAME).trim();
		String inputedDrugProducerLicenceNumber = request.getParameter(AppConstants.PARAMETER_DRUG_PRODUCER_LICENCE_NUMBER).trim();
		String inputedDrugProducerAddressCountry = request.getParameter(AppConstants.PARAMETER_DRUG_PRODUCER_ADDRESS_COUNTRY).trim();
		String inputedDrugProducerAddressTown = request.getParameter(AppConstants.PARAMETER_DRUG_PRODUCER_ADDRESS_TOWN).trim();
		String inputedDrugProducerAddressStreet = request.getParameter(AppConstants.PARAMETER_DRUG_PRODUCER_ADDRESS_STREET).trim();
		String inputedDrugProducerAddressHouse = request.getParameter(AppConstants.PARAMETER_DRUG_PRODUCER_ADDRESS_HOUSE).trim();
		
		Validator drugValidator = new DrugValidator();
		ValidationResult validationResult = drugValidator.validate(inputedDrugName, inputedDrugDosage, inputedDrugFormName, 
																inputedDrugAvailability, inputedDrugProducerName, inputedDrugProducerLicenceNumber,  
																inputedDrugProducerAddressCountry, inputedDrugProducerAddressTown, 
																inputedDrugProducerAddressStreet, inputedDrugProducerAddressHouse);
		if(validationResult.isEmpty()) {
			try {
				Drug drug = new Drug();
				Long drugId = Long.valueOf(inputedDrugId);
				drug.setId(drugId);
				drug.setName(inputedDrugName);
				double drugDosage = Double.parseDouble(inputedDrugDosage);
				drug.setDosage(drugDosage);
				DrugAvailability drugAvailability = DrugAvailability.valueOf(inputedDrugAvailability);
				drug.setAvailability(drugAvailability);

				DrugForm drugForm = new DrugForm();
				drugForm.setName(inputedDrugFormName);
				
				DrugProducer drugProducer = new DrugProducer();
				drugProducer.setName(inputedDrugProducerName);
				drugProducer.setLicenceNumber(inputedDrugProducerLicenceNumber);
				
				Address drugProducerAddress = new Address();
				drugProducerAddress.setCountry(inputedDrugProducerAddressCountry);
				drugProducerAddress.setTown(inputedDrugProducerAddressTown);
				drugProducerAddress.setStreet(inputedDrugProducerAddressStreet);
				drugProducerAddress.setHouse(inputedDrugProducerAddressHouse);
				
				drugService.update(drug, drugForm, drugProducer, drugProducerAddress);
			} catch (ServiceException exception) {
				request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
				LOGGER.log(Level.ERROR, "Drug changing error: ", exception);
			}
			LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
					getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_DRUG_COMMANDS);
		} else {
			request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_VALIDATION_RESULT, validationResult);
			LOGGER.log(Level.ERROR, "{} was executed with mistakes:{}. Page path for controller is {}",
					getClass().getSimpleName(), validationResult, AppConstants.FILE_PATH_FOR_DRUG_COMMANDS);
			
		}
		return TypeCommandAction.REDIRECT + AppConstants.FILE_PATH_FOR_DRUG_COMMANDS;
	}
}
