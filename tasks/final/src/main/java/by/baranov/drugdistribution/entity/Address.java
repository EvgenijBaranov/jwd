package by.baranov.drugdistribution.entity;

import java.io.Serializable;
import java.util.Objects;

public class Address implements AbstractEntity, Serializable {
	
	private static final long serialVersionUID = -2774861373134864901L;
	private Long id;
	private String country;
	private String town;
	private String street;
	private String house;

	public Address() {
	}

	public Address(Long id, String country, String town, String street, String house) {
		this.id = id;
		this.country = country;
		this.town = town;
		this.street = street;
		this.house = house;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouse() {
		return house;
	}

	public void setHouse(String house) {
		this.house = house;
	}

	@Override
	public int hashCode() {
		return Objects.hash(country, house, street, town);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Address))
			return false;
		Address other = (Address) obj;
		return Objects.equals(country.toLowerCase(), other.country.toLowerCase()) && Objects.equals(house.toLowerCase(), other.house.toLowerCase())
				&& Objects.equals(street.toLowerCase(), other.street.toLowerCase()) && Objects.equals(town.toLowerCase(), other.town.toLowerCase());
	}

	@Override
	public String toString() {
		return "Address [country=" + country + ", town=" + town + ", street=" + street + ", house=" + house + "]";
	}

}
