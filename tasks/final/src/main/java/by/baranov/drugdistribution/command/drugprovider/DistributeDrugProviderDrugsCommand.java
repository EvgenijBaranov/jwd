package by.baranov.drugdistribution.command.drugprovider;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DrugService;
import by.baranov.drugdistribution.validation.DrugDistributionValidator;
import by.baranov.drugdistribution.validation.ValidationResult;
import by.baranov.drugdistribution.validation.Validator;

public class DistributeDrugProviderDrugsCommand implements Command{

		private static final String ATTRIBUTE_NAME_RESULT_MESSAGE = "resultMessage";
		private static final Logger LOGGER = LogManager.getLogger();
		private final DrugService drugService;

		public DistributeDrugProviderDrugsCommand(DrugService drugService) {
			this.drugService = drugService;
		}

		@Override
		public String execute(HttpServletRequest request, HttpServletResponse response) {
			String inputedDrugId = request.getParameter(AppConstants.PARAMETER_DRUG_ID).trim();
			String inputedDrugManufactureDate = request.getParameter(AppConstants.PARAMETER_DRUG_MANUFACTURE_DATE).trim();
			String inputedDrugExpiryDate = request.getParameter(AppConstants.PARAMETER_DRUG_EXPIRY_DATE).trim();
			String inputedDrugQuantity = request.getParameter(AppConstants.PARAMETER_DRUG_QUANTITY).trim();
			String inputedDrugPrice = request.getParameter(AppConstants.PARAMETER_DRUG_PRICE).trim();
			
			Validator drugDistributionValidator = new DrugDistributionValidator();
			ValidationResult validationResult = drugDistributionValidator.validate(inputedDrugManufactureDate, inputedDrugExpiryDate, 
																		inputedDrugQuantity, inputedDrugPrice);
			if(validationResult.isEmpty()) {
				try {
					Long drugId = Long.valueOf(inputedDrugId);
					LocalDate drugManufactureDate = LocalDate.parse(inputedDrugManufactureDate);
					LocalDate drugExpiryDate = LocalDate.parse(inputedDrugExpiryDate);
					int drugQuantity = Integer.parseInt(inputedDrugQuantity);
					BigDecimal drugPrice = new BigDecimal(inputedDrugPrice);
										
					String resultMessages = drugService.distributeDrugs(drugId, drugManufactureDate, drugExpiryDate, drugQuantity, drugPrice);
					
					List<String> resultMessage = Arrays.asList(resultMessages.split(":"));
					request.getSession().setAttribute(ATTRIBUTE_NAME_RESULT_MESSAGE, resultMessage);
				} catch (ServiceException exception) {
					request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
					LOGGER.log(Level.ERROR, "Drug distribution error ", exception);
				}
				LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
						getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_DRUG_PROVIDER_COMMANDS);
			} else {
				request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_VALIDATION_RESULT, validationResult);
				LOGGER.log(Level.ERROR, "{} was executed with mistakes:{}. Page path for controller is {}",
						getClass().getSimpleName(), validationResult, AppConstants.FILE_PATH_FOR_DRUG_PROVIDER_COMMANDS);
			}
			
			return TypeCommandAction.REDIRECT + AppConstants.FILE_PATH_FOR_DRUG_PROVIDER_COMMANDS;
	}

}
