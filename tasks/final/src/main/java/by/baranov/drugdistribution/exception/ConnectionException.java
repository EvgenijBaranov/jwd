package by.baranov.drugdistribution.exception;

public class ConnectionException extends RuntimeException{

	private static final long serialVersionUID = -3282783622510762434L;

	public ConnectionException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConnectionException(String message) {
		super(message);
	}

}
