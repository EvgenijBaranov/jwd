package by.baranov.drugdistribution.service.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import by.baranov.drugdistribution.connectionpool.Transactional;
import by.baranov.drugdistribution.dao.AddressDao;
import by.baranov.drugdistribution.dao.DriverInvoiceDao;
import by.baranov.drugdistribution.dao.DrugDao;
import by.baranov.drugdistribution.dao.DrugFormDao;
import by.baranov.drugdistribution.dao.DrugOrderDao;
import by.baranov.drugdistribution.dao.DrugProducerDao;
import by.baranov.drugdistribution.entity.Address;
import by.baranov.drugdistribution.entity.DriverInvoice;
import by.baranov.drugdistribution.entity.Drug;
import by.baranov.drugdistribution.entity.DrugForm;
import by.baranov.drugdistribution.entity.DrugOrder;
import by.baranov.drugdistribution.entity.DrugOrderStatus;
import by.baranov.drugdistribution.entity.DrugProducer;
import by.baranov.drugdistribution.entity.PharmacyHasDrug;
import by.baranov.drugdistribution.exception.DaoException;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DrugService;

public class DrugServiceImpl implements DrugService{
	
	private final DrugDao drugDao;
	private final AddressDao addressDao;
	private final DrugFormDao drugFormDao;
	private final DrugProducerDao drugProducerDao;
	private final DrugOrderDao drugOrderDao;
	private final DriverInvoiceDao driverInvoiceDao;

	public DrugServiceImpl(DrugDao drugDao, AddressDao addressDao, DrugFormDao drugFormDao,
			DrugProducerDao drugProducerDao, DrugOrderDao drugOrderDao, DriverInvoiceDao driverInvoiceDao) {
		this.drugDao = drugDao;
		this.addressDao = addressDao;
		this.drugFormDao = drugFormDao;
		this.drugProducerDao = drugProducerDao;
		this.drugOrderDao = drugOrderDao;
		this.driverInvoiceDao = driverInvoiceDao;
	}

	@Override
	public List<Drug> getAll() throws ServiceException {
		try {
			return drugDao.getAll();
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	@Override
	public Drug getById(Long id) throws ServiceException {
		try {
			return drugDao.getById(id);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	@Transactional
	@Override
	public void delete(Long id) throws ServiceException {
		try {
			Drug drug = drugDao.getById(id);
			drugDao.delete(drug);
		} catch (DaoException exception) {
			throw new ServiceException("error.drug.service.drug.delete.message", exception);
		}
	}

	@Override
	public List<PharmacyHasDrug> getAllDrugQuantityForPharmacy(Long pharmacyId, Long drugId) throws ServiceException {
		try {
			return drugDao.getAllDrugsForPharmacy(pharmacyId, drugId);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}
	
	@Transactional
	@Override
	public void save(Drug drug, DrugForm drugForm, DrugProducer drugProducer, Address drugProducerAddress)
			throws ServiceException {
		try {
			
			saveDrugProducerAddress(drugProducer, drugProducerAddress);
			saveDrugProducer(drug, drugProducer);
			saveDrug(drug);
			assignDrugFormForDrug(drug, drugForm);
			
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	private void assignDrugFormForDrug(Drug drug, DrugForm drugForm) throws DaoException {
		List<DrugForm> drugForms = drugFormDao.getAll();
		boolean isDrugFormExist = drugForms.stream()
				.anyMatch(drugFormFromDataBase -> drugFormFromDataBase.equals(drugForm));
		if(isDrugFormExist) {
			Long drugFormId = drugForms.stream()
										.filter(drugFormFromDataBase -> drugFormFromDataBase.equals(drugForm))
										.map(DrugForm::getId)
										.findAny().orElseThrow(NullPointerException::new);
			drugFormDao.assignDrugFormForDrug(drug.getId(), drugFormId);
		} else {
			drugFormDao.save(drugForm);
			drugFormDao.assignDrugFormForDrug(drug.getId(), drugForm.getId());
		}
	}

	private void saveDrug(Drug drug) throws DaoException, ServiceException {
		List<Drug> drugs = drugDao.getAll();
		boolean isDrugExist = drugs.stream()
				.anyMatch(drugFromDataBase -> drugFromDataBase.equals(drug));
		if (isDrugExist) {
			throw new ServiceException("error.drug.service.drug.exist.message");
		}
		drugDao.save(drug);
	}

	private void saveDrugProducer(Drug drug, DrugProducer drugProducer) throws DaoException {
		List<DrugProducer> listDrugProducer = drugProducerDao.getAll();
		boolean isDrugProducerExist = listDrugProducer.stream()
				.anyMatch(drugProducerFromDataBase -> drugProducerFromDataBase.equals(drugProducer));
		if (isDrugProducerExist) {
			Long drugProducerId = listDrugProducer.stream()
										.filter(drugProducerFromDataBase -> drugProducerFromDataBase.equals(drugProducer))
										.map(DrugProducer::getId)
										.findAny().orElseThrow(NullPointerException::new);
			drug.setProducerId(drugProducerId);
		} else {
			drugProducerDao.save(drugProducer);
			drug.setProducerId(drugProducer.getId());
		}
	}

	private void saveDrugProducerAddress(DrugProducer drugProducer, Address drugProducerAddress) throws DaoException {
		List<Address> listAddress = addressDao.getAll();
		
		boolean isDrugProducerAddressExist = listAddress.stream()
				.anyMatch(addressFromDataBase -> addressFromDataBase.equals(drugProducerAddress));
		if (isDrugProducerAddressExist) {
			Long drugProducerAddressId = listAddress.stream()
					.filter(addressFromDataBase -> addressFromDataBase.equals(drugProducerAddress))
					.map(Address::getId)
					.findAny().orElseThrow(NullPointerException::new);
			drugProducer.setAddressId(drugProducerAddressId);
		} else {
			addressDao.save(drugProducerAddress);
			drugProducer.setAddressId(drugProducerAddress.getId());
		}
	}

	@Transactional
	@Override
	public void update(Drug drug, DrugForm drugForm, DrugProducer drugProducer, Address drugProducerAddress)
			throws ServiceException {
		try {
			saveDrugProducerAddress(drugProducer, drugProducerAddress);
			saveDrugProducer(drug, drugProducer);
			updateDrug(drug);
			checkAndAssignDrugFormForDrug(drug, drugForm);
			
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	private void checkAndAssignDrugFormForDrug(Drug drug, DrugForm drugForm) throws DaoException {
		List<DrugForm> drugForms = drugFormDao.getAll();
		boolean isDrugFormNotExist = drugForms.stream()
				.noneMatch(drugFormFromDataBase -> drugFormFromDataBase.equals(drugForm));
		if (isDrugFormNotExist) {
			drugFormDao.save(drugForm);
			drugFormDao.assignDrugFormForDrug(drug.getId(), drugForm.getId());
		} else {
			List<DrugForm> drugFormsForDrugInDataBase = drugFormDao.getDrugFormsByDrugId(drug.getId());
			boolean isDrugFormNotExistForDrug = drugFormsForDrugInDataBase.stream()
					.noneMatch(drugFormFromDataBase -> drugFormFromDataBase.equals(drugForm));
			if(isDrugFormNotExistForDrug) {
				drugFormDao.assignDrugFormForDrug(drug.getId(), drugForm.getId());
			}
		}
	}

	private void updateDrug(Drug drug) throws DaoException, ServiceException {
		List<Drug> drugs = drugDao.getAll();
		
		boolean isDrugExist = drugs.stream()
				.anyMatch(drugFromDataBase -> drugFromDataBase.equals(drug));
		if (isDrugExist) {
			throw new ServiceException("error.drug.service.drug.exist.message");
		}
		
		drugDao.update(drug);
	}

	@Override
	public Set<Drug> getAllDrugsByPharmacyId(Long pharmacyId) throws ServiceException {
		try {
			return drugDao.getDrugsByPharmacyId(pharmacyId);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	@Transactional
	@Override
	public String distributeDrugs(Long drugId, LocalDate drugManufactureDate, LocalDate drugExpiryDate, int drugQuantity,
			BigDecimal drugPrice) throws ServiceException {
		try {
			List<DrugOrder> drugOrders = drugOrderDao.getDrugOrdersByDrugId(drugId);
			
			List<DrugOrder> listNotDistributedDrugOrders = drugOrders.stream()
																	.filter(drugOrder -> drugOrder.getOrderStatus() == DrugOrderStatus.CREATED)
																	.sorted(Comparator.comparing(DrugOrder::getCreationDate))
																	.collect(Collectors.toList());
			if(listNotDistributedDrugOrders.isEmpty()) {
				return "drug.service.distribute.drugs.not.drug.orders.message";
			}
			
			for (DrugOrder drugOrder : listNotDistributedDrugOrders) {						
				if (drugOrder.getDrugQuantity() <= drugQuantity) {
					
					createNewDriverInvoiceForDrugOrder(drugManufactureDate, drugExpiryDate, drugPrice, drugOrder);
					drugQuantity -= drugOrder.getDrugQuantity();
					changeDrugOrderStatus(drugOrder);
					
				} else if (drugOrder.getDrugQuantity() > drugQuantity){
					
					int drugQuantityForNewDrugOrder = drugOrder.getDrugQuantity() - drugQuantity;
					
					drugOrder.setDrugQuantity(drugQuantity);
					changeDrugOrderStatus(drugOrder);
					createNewDriverInvoiceForDrugOrder(drugManufactureDate, drugExpiryDate, drugPrice, drugOrder);
					createNewDrugOrder(drugOrder, drugQuantityForNewDrugOrder);
					
					return "drug.service.distribute.drugs.all.drugs.disributed.message";
				}
			}
			return drugQuantity + ":drug.service.distribute.drugs.not.all.drugs.disributed.message";
		} catch (DaoException exception) {
			throw new ServiceException("Error distribution drugs with id= " + drugId, exception);
		}
	}

	private void createNewDrugOrder(DrugOrder drugOrder, int drugQuantityForNewDrugOrder) throws DaoException {
		DrugOrder newDrugOrder = new DrugOrder();
		newDrugOrder.setPharmacyId(drugOrder.getPharmacyId());
		newDrugOrder.setPharmacistId(drugOrder.getPharmacistId());
		newDrugOrder.setDrugId(drugOrder.getDrugId());
		newDrugOrder.setDrugQuantity(drugQuantityForNewDrugOrder);
		newDrugOrder.setCreationDate(drugOrder.getCreationDate());
		newDrugOrder.setOrderStatus(DrugOrderStatus.CREATED);
		drugOrderDao.save(newDrugOrder);
	}

	private void changeDrugOrderStatus(DrugOrder drugOrder) throws DaoException {
		drugOrder.setOrderStatus(DrugOrderStatus.INVOICE_CREATED);
		drugOrderDao.update(drugOrder);
	}

	private void createNewDriverInvoiceForDrugOrder(LocalDate drugManufactureDate, LocalDate drugExpiryDate, BigDecimal drugPrice,
			DrugOrder drugOrder) throws DaoException {
		DriverInvoice driverInvoice = new DriverInvoice();
		driverInvoice.setDrugOrderId(drugOrder.getId());
		driverInvoice.setDrugManufactureDate(drugManufactureDate);
		driverInvoice.setDrugExpiryDate(drugExpiryDate);
		driverInvoice.setDrugPrice(drugPrice);
		driverInvoiceDao.save(driverInvoice);
	}

}
