package by.baranov.drugdistribution.validation;

public interface Validator {
	
	ValidationResult validate(String ...inputedFieldsData);
}
