package by.baranov.drugdistribution.command.pharmacy;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.Address;
import by.baranov.drugdistribution.entity.Pharmacy;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.AddressService;
import by.baranov.drugdistribution.service.PharmacyService;

public class ViewAllPharmaciesCommand implements Command{
	private static final String ATTRIBUTE_NAME_FOR_PHARMACIES = "pharmacies";
	private static final String ATTRIBUTE_NAME_FOR_PHARMACIES_ADDRESSES = "pharmaciesAddresses";
	private static final String ATTRIBUTE_VALUE_FOR_VIEW = "viewAllPharmacies";
	private static final Logger LOGGER = LogManager.getLogger();

	private final PharmacyService pharmacyService;
	private final AddressService addressService;

	public ViewAllPharmaciesCommand(PharmacyService pharmacyService, AddressService addressService) {
		this.pharmacyService = pharmacyService;
		this.addressService = addressService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		try {
			List<Pharmacy> pharmacies = pharmacyService.getAll();
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_PHARMACIES, pharmacies);
			
			Set<Address> pharmaciesAddresses = new HashSet<>();
			for(Pharmacy pharmacy : pharmacies) {
				Address pharmacyAddress = addressService.getById(pharmacy.getAddressId());
				pharmaciesAddresses.add(pharmacyAddress);
			}
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_PHARMACIES_ADDRESSES, pharmaciesAddresses);
		} catch (ServiceException exception) {
			request.setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
			LOGGER.log(Level.ERROR, "Error of view all pharmacies", exception);
		}
		request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_VIEW, ATTRIBUTE_VALUE_FOR_VIEW);
		LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
				getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_PHARMACY_COMMANDS);
		return TypeCommandAction.FORWARD + AppConstants.FILE_PATH_FOR_PHARMACY_COMMANDS;
	}
}
