package by.baranov.drugdistribution.dao;

import java.util.List;

import by.baranov.drugdistribution.entity.DrugOrder;
import by.baranov.drugdistribution.exception.DaoException;

public interface DrugOrderDao extends CRUDOperation<Long, DrugOrder>{
	
	List<DrugOrder> getDrugOrdersByPharmacyId(Long pharmacyId) throws DaoException;
	
	List<DrugOrder> getDrugOrdersByDrugId(Long drugId) throws DaoException;
	
}
