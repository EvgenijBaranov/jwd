package by.baranov.drugdistribution.service.impl;

import by.baranov.drugdistribution.dao.DrugProducerDao;
import by.baranov.drugdistribution.entity.DrugProducer;
import by.baranov.drugdistribution.exception.DaoException;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DrugProducerService;

public class DrugProducerServiceImpl implements DrugProducerService{
	private final DrugProducerDao drugProducerDao;

	public DrugProducerServiceImpl(DrugProducerDao drugProducerDao) {
		this.drugProducerDao = drugProducerDao;
	}

	@Override
	public DrugProducer getById(Long id) throws ServiceException {
		try {
			return drugProducerDao.getById(id);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

}
