package by.baranov.drugdistribution.command.pharmacy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.Pharmacy;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.PharmacyService;

public class ViewChangePharmacyFormCommand implements Command{
	private static final String ATTRIBUTE_NAME_FOR_PHARMACY = "pharmacy";
	private static final String ATTRIBUTE_VALUE_FOR_VIEW = "changePharmacyForm";
	private static final Logger LOGGER = LogManager.getLogger();
	private final PharmacyService pharmacyService;

	public ViewChangePharmacyFormCommand(PharmacyService pharmacyService) {
		this.pharmacyService = pharmacyService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String inputedPharmacyId = request.getParameter(AppConstants.PARAMETER_PHARMACY_ID).trim();
		
		try {
			Long userId = Long.valueOf(inputedPharmacyId);
			Pharmacy pharmacy = pharmacyService.getById(userId);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_PHARMACY, pharmacy);
		} catch (ServiceException exception) {
			request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
			LOGGER.log(Level.ERROR, "Error changing pharmacy", exception);
		}
		request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_VIEW, ATTRIBUTE_VALUE_FOR_VIEW);
		LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
				getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_PHARMACY_COMMANDS);
		return TypeCommandAction.FORWARD + AppConstants.FILE_PATH_FOR_PHARMACY_COMMANDS;
	}
}
