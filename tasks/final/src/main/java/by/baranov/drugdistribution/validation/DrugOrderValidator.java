package by.baranov.drugdistribution.validation;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DrugOrderValidator implements Validator{
	private static final Logger LOGGER = LogManager.getLogger();
	private static final String FIELD_NAME_DRUG_QUANTITY = "error.drug.order.validator.field.drug.quantity";
	private ValidationResult validationResult = new ValidationResult();
	
	@Override
	public ValidationResult validate(String... inputedFieldsData) {
		
		List<String> listInputedFieldsData = Arrays.asList(inputedFieldsData);
		LOGGER.log(Level.DEBUG, "DrugOrderValidator inputed data = {}", listInputedFieldsData);
		
		if(listInputedFieldsData.get(0).isEmpty()) {
			validationResult.addMessage(new ValidationMessage(FIELD_NAME_DRUG_QUANTITY, "error.validator.field.value.empty"));
		}
		
		try{
			int drugQuantity = Integer.parseInt(listInputedFieldsData.get(0));
			if(drugQuantity <= 0) {
				validationResult.addMessage(new ValidationMessage(FIELD_NAME_DRUG_QUANTITY, "error.validator.field.value.positive.integer.number"));
			}
		} catch (NumberFormatException exception) {
			validationResult.addMessage(new ValidationMessage(FIELD_NAME_DRUG_QUANTITY, "error.validator.field.value.positive.integer.number"));
		}
		LOGGER.log(Level.DEBUG, "validationResult={}", validationResult);
		return validationResult;
	}
}
