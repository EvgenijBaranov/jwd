package by.baranov.drugdistribution.service;

import java.util.List;

import by.baranov.drugdistribution.entity.UserRole;
import by.baranov.drugdistribution.exception.ServiceException;

public interface UserRoleService{
	
	List<UserRole> getUserRolesByUserId(Long userId) throws ServiceException;
	
	void assignRoleForUser(Long userId, Long userRoleId) throws ServiceException;

	void deleteUserRoleForUser(Long userId, Long userRoleId) throws ServiceException;
}
