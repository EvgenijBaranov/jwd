package by.baranov.drugdistribution.exception;

public class DataBasePropertyFileException extends RuntimeException {

	private static final long serialVersionUID = 5397286265452655396L;

	public DataBasePropertyFileException(String message, Throwable cause) {
		super(message, cause);
	}

	public DataBasePropertyFileException(String message) {
		super(message);
	}

}
