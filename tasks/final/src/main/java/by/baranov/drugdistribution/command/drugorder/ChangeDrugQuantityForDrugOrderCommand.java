package by.baranov.drugdistribution.command.drugorder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DrugOrderService;
import by.baranov.drugdistribution.validation.DrugOrderValidator;
import by.baranov.drugdistribution.validation.ValidationResult;
import by.baranov.drugdistribution.validation.Validator;

public class ChangeDrugQuantityForDrugOrderCommand implements Command{
	
	private static final Logger LOGGER = LogManager.getLogger();
	private final DrugOrderService drugOrderService;

	public ChangeDrugQuantityForDrugOrderCommand(DrugOrderService drugOrderService) {
		this.drugOrderService = drugOrderService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String inputedDrugOrderId = request.getParameter(AppConstants.PARAMETER_DRUG_ORDER_ID).trim();
		String inputedDrugQuantity = request.getParameter(AppConstants.PARAMETER_DRUG_ORDER_DRUG_QUANTITY).trim();
		
		Validator drugOrderValidator = new DrugOrderValidator();
		ValidationResult validationResult = drugOrderValidator.validate(inputedDrugQuantity);
		
		if(validationResult.isEmpty()) {
			try {
				Long drugOrderId = Long.valueOf(inputedDrugOrderId);
				int drugQuantity = Integer.parseInt(inputedDrugQuantity);
				drugOrderService.update(drugOrderId, drugQuantity);
			} catch (ServiceException exception) {
				request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
				LOGGER.log(Level.ERROR, "Error updating drug order", exception);
			}
			LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
					getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_DRUG_ORDER_COMMANDS);
		} else {
			request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_VALIDATION_RESULT, validationResult);
			LOGGER.log(Level.ERROR, "{} was executed with mistakes:{}. Page path for controller is {}",
					getClass().getSimpleName(), validationResult, AppConstants.FILE_PATH_FOR_DRUG_ORDER_COMMANDS);
		}

		return TypeCommandAction.REDIRECT + AppConstants.FILE_PATH_FOR_DRUG_ORDER_COMMANDS;
	}
}
