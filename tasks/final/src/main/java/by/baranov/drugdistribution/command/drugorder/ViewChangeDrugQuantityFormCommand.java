package by.baranov.drugdistribution.command.drugorder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.DrugOrder;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DrugOrderService;

public class ViewChangeDrugQuantityFormCommand implements Command{
	private static final String ATTRIBUTE_NAME_FOR_DRUG_ORDER = "drugOrder";
	private static final String ATTRIBUTE_VALUE_FOR_VIEW = "changeDrugQuantityForDrugOrderForm";
	private static final Logger LOGGER = LogManager.getLogger();
	private final DrugOrderService drugOrderService;

	public ViewChangeDrugQuantityFormCommand(DrugOrderService drugOrderService) {
		this.drugOrderService = drugOrderService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String inputedDrugOrderId = request.getParameter(AppConstants.PARAMETER_DRUG_ORDER_ID).trim();

		try {
			Long drugOrderId = Long.valueOf(inputedDrugOrderId);
			DrugOrder drugOrder = drugOrderService.getById(drugOrderId);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_DRUG_ORDER, drugOrder);
		} catch (ServiceException exception) {
			request.setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
			LOGGER.log(Level.ERROR, "Error changing drug order", exception);
		}
		request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_VIEW, ATTRIBUTE_VALUE_FOR_VIEW);
		LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
				getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_DRUG_ORDER_COMMANDS);
		return TypeCommandAction.FORWARD + AppConstants.FILE_PATH_FOR_DRUG_ORDER_COMMANDS;
	}
}
