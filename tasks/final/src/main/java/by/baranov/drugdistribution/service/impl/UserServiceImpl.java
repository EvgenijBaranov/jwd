package by.baranov.drugdistribution.service.impl;

import java.util.List;

import by.baranov.drugdistribution.connectionpool.Transactional;
import by.baranov.drugdistribution.dao.UserDao;
import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.entity.UserRole;
import by.baranov.drugdistribution.exception.DaoException;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.UserService;

public class UserServiceImpl implements UserService {

	private final UserDao userDao;

	public UserServiceImpl(UserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public List<User> getAll() throws ServiceException {
		try {
			return userDao.getAll();
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	@Override
	public User getById(Long id) throws ServiceException {
		try {
			return userDao.getById(id);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	@Transactional
	@Override
	public void save(User user) throws ServiceException {
		try {
			boolean isLoginExist = userDao.getAll().stream()
					.anyMatch(userFromDataBase -> userFromDataBase.getLogin().equalsIgnoreCase(user.getLogin()));
			if (isLoginExist) {
				throw new ServiceException("error.user.service.user.login.exist.message");
			}
			userDao.save(user);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	@Transactional
	@Override
	public void update(User user) throws ServiceException {
		try {
			
			List<User> users = userDao.getAll();
			boolean isLoginExist = users.stream()
					.filter(userFromDataBase -> !userFromDataBase.getId().equals(user.getId()))
					.anyMatch(userFromDataBase -> userFromDataBase.getLogin().equalsIgnoreCase(user.getLogin()));
			if (isLoginExist) {
				throw new ServiceException("error.user.service.user.login.exist.message");
			}
			userDao.update(user);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	@Transactional
	@Override
	public void delete(Long id) throws ServiceException {
		try {
			User user = userDao.getById(id);
			userDao.delete(user);
		} catch (DaoException exception) {
			throw new ServiceException("error.user.service.user.delete.message", exception);
		}
	}

	@Override
	public boolean loginUser(String login, String password) throws ServiceException {
		try {
			User registeredUser = userDao.getUserByLogin(login);
			return registeredUser.getPassword().equals(password);
		} catch (DaoException exception) {
			throw new ServiceException("error.user.service.user.login.message", exception);
		} 
	}

	@Override
	public User getUserByLogin(String login) throws ServiceException {
		try {
			return userDao.getUserByLogin(login);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}
	
	@Transactional
	@Override
	public void assignPharmacistForPharmacy(Long pharmacyId, Long userId) throws ServiceException {
		try {
			
			List<User> pharmacistsInPharmacy = userDao.getPharmacistsForPharmacy(pharmacyId);
			boolean isPharmacistWorksInThePharmacy = pharmacistsInPharmacy.stream().anyMatch(pharmacist -> pharmacist.getId().equals(userId));
			if(isPharmacistWorksInThePharmacy) {
				throw new ServiceException("error.user.service.add.pharmacist.for.pharmacy.message");
			}
			userDao.assignPharmacistToPharmacy(pharmacyId, userId);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	@Transactional
	@Override
	public void deletePharmacistForPharmacy(Long pharmacyId, Long userId) throws ServiceException {
		try {
			userDao.deletePharmacistFromPharmacy(pharmacyId, userId);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	@Override
	public List<User> getPharmacistsByPharmacyId(Long pharmacyId) throws ServiceException {
		try {
			return userDao.getPharmacistsForPharmacy(pharmacyId);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	@Override
	public List<User> getAllUsersByUserRole(UserRole userRole) throws ServiceException {
		try {
			return userDao.getAllUsersByUserRoleId(userRole.getId());
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

}
