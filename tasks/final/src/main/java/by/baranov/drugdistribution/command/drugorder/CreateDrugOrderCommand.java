package by.baranov.drugdistribution.command.drugorder;

import java.time.LocalDateTime;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.DrugOrder;
import by.baranov.drugdistribution.entity.DrugOrderStatus;
import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DrugOrderService;
import by.baranov.drugdistribution.validation.DrugOrderValidator;
import by.baranov.drugdistribution.validation.ValidationResult;
import by.baranov.drugdistribution.validation.Validator;

public class CreateDrugOrderCommand implements Command{
	private static final Logger LOGGER = LogManager.getLogger();
	private final DrugOrderService drugOrderService;
	
	public CreateDrugOrderCommand(DrugOrderService drugOrderService) {
		this.drugOrderService = drugOrderService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		String inputedPharmacyId = request.getParameter(AppConstants.PARAMETER_PHARMACY_ID).trim();
		User pharmacist = (User) request.getSession().getAttribute(AppConstants.ATTRIBUTE_NAME_REGISTERED_USER);
		String inputedDrugId = request.getParameter(AppConstants.PARAMETER_DRUG_ID).trim();
		String inputedDrugQuantity = request.getParameter(AppConstants.PARAMETER_DRUG_QUANTITY).trim();
		
		Validator drugOrderValidator = new DrugOrderValidator();
		ValidationResult validationResult = drugOrderValidator.validate(inputedDrugQuantity);
		
		if(validationResult.isEmpty()) {
		
			try {
				DrugOrder drugOrder = new DrugOrder();
				Long pharmacyId = Long.valueOf(inputedPharmacyId);
				drugOrder.setPharmacyId(pharmacyId);
				drugOrder.setPharmacistId(pharmacist.getId());
				Long drugId = Long.valueOf(inputedDrugId);
				drugOrder.setDrugId(drugId);
				int drugQuantity = Integer.parseInt(inputedDrugQuantity);
				drugOrder.setDrugQuantity(drugQuantity);
				LocalDateTime creationOrderDate = LocalDateTime.now();
				drugOrder.setCreationDate(creationOrderDate);
				drugOrder.setOrderStatus(DrugOrderStatus.CREATED);
				
				drugOrderService.save(drugOrder);
				
			} catch (ServiceException exception) {
				request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
				LOGGER.log(Level.ERROR, "Drug addition error: ", exception);
			}
			LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
					getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_DRUG_ORDER_COMMANDS);
		} else {
			request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_VALIDATION_RESULT, validationResult);
			LOGGER.log(Level.ERROR, "{} was executed with mistakes:{}. Page path for controller is {}",
					getClass().getSimpleName(), validationResult, AppConstants.FILE_PATH_FOR_DRUG_ORDER_COMMANDS);
		}
		
		return TypeCommandAction.REDIRECT + AppConstants.FILE_PATH_FOR_DRUG_ORDER_COMMANDS;
	}
}
