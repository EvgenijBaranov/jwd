package by.baranov.drugdistribution.entity;

import java.io.Serializable;
import java.util.Objects;

public class Drug implements AbstractEntity, Serializable {
	private static final long serialVersionUID = 950735552516562476L;
	private Long id;
	private String name;
	private double dosage;
	private DrugAvailability availability;
	private Long producerId;

	public Drug() {
	}

	public Drug(Long id, String name, double dosage,  DrugAvailability availability, Long producerId) {
		this.id = id;
		this.name = name;
		this.dosage = dosage;
		this.availability = availability;
		this.producerId = producerId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getDosage() {
		return dosage;
	}

	public void setDosage(double dosage) {
		this.dosage = dosage;
	}

	public DrugAvailability getAvailability() {
		return availability;
	}

	public void setAvailability(DrugAvailability availability) {
		this.availability = availability;
	}

	public Long getProducerId() {
		return producerId;
	}

	public void setProducerId(Long producerId) {
		this.producerId = producerId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(availability, dosage, name, producerId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Drug))
			return false;
		Drug other = (Drug) obj;
		return availability == other.availability
				&& Double.doubleToLongBits(dosage) == Double.doubleToLongBits(other.dosage)
				&& Objects.equals(name.toLowerCase(), other.name.toLowerCase())
				&& Objects.equals(producerId, other.producerId);
	}

	@Override
	public String toString() {
		return "Drug [name=" + name + ", dosage=" + dosage + ", availability=" + availability + ", producerId=" + producerId + "]";
	}

}
