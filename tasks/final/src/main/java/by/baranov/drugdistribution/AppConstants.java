package by.baranov.drugdistribution;

public final class AppConstants {
	
	public static final String PARAMETER_COMMAND_NAME = "commandName";
	public static final String ATTRIBUTE_NAME_ERROR = "errorMessage";
	public static final String ATTRIBUTE_NAME_VALIDATION_RESULT = "validationResult";
	public static final String ATTRIBUTE_NAME_VIEW = "view";
	
	
	public static final String PARAMETER_USER_LOGIN = "userLogin";
	public static final String PARAMETER_USER_PASSWORD = "userPassword";
	public static final String PARAMETER_USER_NAME = "userName";
	public static final String PARAMETER_USER_SURNAME = "userSurname";
	public static final String PARAMETER_USER_ID = "userId";
	public static final String PARAMETER_USER_ROLE_ID = "userRoleId";
	public static final String ATTRIBUTE_NAME_REGISTERED_USER = "registeredUser";
	public static final String FILE_PATH_FOR_USER_COMMANDS = "/jsp/user/userPage.jsp";
	
	
	public static final String PARAMETER_PHARMACY_ID = "pharmacyId";
	public static final String PARAMETER_PHARMACY_NAME = "pharmacyName";
	public static final String PARAMETER_PHARMACY_LICENCE_NUMBER = "pharmacyLicenceNumber";
	public static final String PARAMETER_PHARMACY_PHONE = "pharmacyPhone";
	public static final String PARAMETER_PHARMACY_ADDRESS_COUNTRY = "pharmacyAddressCountry";
	public static final String PARAMETER_PHARMACY_ADDRESS_TOWN = "pharmacyAddressTown";
	public static final String PARAMETER_PHARMACY_ADDRESS_STREET = "pharmacyAddressStreet";
	public static final String PARAMETER_PHARMACY_ADDRESS_HOUSE = "pharmacyAddressHouse";
	public static final String FILE_PATH_FOR_PHARMACY_COMMANDS = "/jsp/pharmacy/pharmacyPage.jsp";
	
	
	public static final String PARAMETER_DRUG_ID = "drugId";
	public static final String PARAMETER_DRUG_NAME = "drugName";
	public static final String PARAMETER_DRUG_DOSAGE = "drugDosage";
	public static final String PARAMETER_DRUG_AVAILABILITY = "drugAvailability";
	public static final String PARAMETER_DRUG_PRODUCER_NAME = "drugProducerName";
	public static final String PARAMETER_DRUG_PRODUCER_LICENCE_NUMBER = "drugProducerLicenceNumber";
	public static final String PARAMETER_DRUG_PRODUCER_ADDRESS_COUNTRY = "drugProducerAddressCountry";
	public static final String PARAMETER_DRUG_PRODUCER_ADDRESS_TOWN = "drugProducerAddressTown";
	public static final String PARAMETER_DRUG_PRODUCER_ADDRESS_STREET = "drugProducerAddressStreet";
	public static final String PARAMETER_DRUG_PRODUCER_ADDRESS_HOUSE = "drugProducerAddressHouse";
	public static final String PARAMETER_DRUG_FORM_ID = "drugFormId";
	public static final String PARAMETER_DRUG_FORM_NAME = "drugFormName";
	public static final String PARAMETER_DRUG_QUANTITY = "drugQuantity";
	public static final String PARAMETER_DRUG_MANUFACTURE_DATE = "drugManufactureDate";
	public static final String PARAMETER_DRUG_EXPIRY_DATE = "drugExpiryDate";
	public static final String PARAMETER_DRUG_PRICE = "drugPrice";
	public static final String FILE_PATH_FOR_DRUG_COMMANDS = "/jsp/drug/drugPage.jsp";
	
	
	public static final String PARAMETER_DRUG_ORDER_ID = "drugOrderId";
	public static final String PARAMETER_DRUG_ORDER_DRUG_QUANTITY = "drugQuantityForDrugOrder";
	public static final String FILE_PATH_FOR_DRUG_ORDER_COMMANDS = "/jsp/drugOrder/drugOrderPage.jsp";
	
	
	public static final String FILE_PATH_FOR_DRUG_PROVIDER_COMMANDS = "/jsp/drugProvider/drugProviderPage.jsp";
	
	
	public static final String PARAMETER_DRIVER_INVOICE_ID = "driverInvoiceId";
	public static final String FILE_PATH_FOR_DRIVER_INVOICE_COMMANDS = "/jsp/driverInvoice/driverInvoicePage.jsp";
	
	
}
