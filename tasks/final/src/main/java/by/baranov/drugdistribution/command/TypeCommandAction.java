package by.baranov.drugdistribution.command;

public enum TypeCommandAction {
	FORWARD, REDIRECT
}
