package by.baranov.drugdistribution.validation;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DrugFormValidator implements Validator{
	private static final Logger LOGGER = LogManager.getLogger();
	private ValidationResult validationResult = new ValidationResult();
	
	@Override
	public ValidationResult validate(String... inputedFieldsData) {
		
		List<String> listInputedFieldsData = Arrays.asList(inputedFieldsData);
		LOGGER.log(Level.DEBUG, "DrugFormValidator inputed data = {}", listInputedFieldsData);
		
		if(listInputedFieldsData.get(0).isEmpty()) {
			validationResult.addMessage(new ValidationMessage("error.drug.form.validator.field.drug.form", "error.validator.field.value.empty"));
		}
		
		LOGGER.log(Level.DEBUG, "validationResult={}", validationResult);
		return validationResult;
	}
}
