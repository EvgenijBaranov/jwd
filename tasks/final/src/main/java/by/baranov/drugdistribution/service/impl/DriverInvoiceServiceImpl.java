package by.baranov.drugdistribution.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import by.baranov.drugdistribution.connectionpool.Transactional;
import by.baranov.drugdistribution.dao.DriverInvoiceDao;
import by.baranov.drugdistribution.dao.DrugOrderDao;
import by.baranov.drugdistribution.entity.DriverInvoice;
import by.baranov.drugdistribution.entity.DrugOrder;
import by.baranov.drugdistribution.entity.DrugOrderStatus;
import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.exception.DaoException;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DriverInvoiceService;

public class DriverInvoiceServiceImpl implements DriverInvoiceService{
	private final DriverInvoiceDao driverInvoiceDao;
	private final DrugOrderDao drugOrderDao;

	public DriverInvoiceServiceImpl(DriverInvoiceDao driverInvoiceDao, DrugOrderDao drugOrderDao) {
		this.driverInvoiceDao = driverInvoiceDao;
		this.drugOrderDao = drugOrderDao;
	}

	@Override
	public List<DriverInvoice> getAvailableInvoicesForDriver(User driver) throws ServiceException {
		try {
			Predicate<DriverInvoice> notAcceptedInvoices = invoice -> Objects.isNull(invoice.getDriverId());
			Predicate<DriverInvoice> acceptedInvoicesByDriverId = invoice -> invoice.getDriverId().equals(driver.getId());
			
			return driverInvoiceDao.getAll().stream()
											.filter(notAcceptedInvoices.or(acceptedInvoicesByDriverId))
											.collect(Collectors.toList());
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	@Transactional	
	@Override
	public void delete(Long id) throws ServiceException {
		try {
			DriverInvoice driverInvoice = driverInvoiceDao.getById(id);
			if(driverInvoice.getDeliveryDate() == null) {
				throw new ServiceException("error.driver.invoice.service.driver.invoice.delete.message");
			}
			driverInvoiceDao.delete(driverInvoice);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}
	
	@Transactional
	@Override
	public void acceptDriverInvoiceByDriver(Long driverInvoiceId, User driver) throws ServiceException {
		try {
			DriverInvoice driverInvoice = driverInvoiceDao.getById(driverInvoiceId);
			if(driverInvoice.getDepartureDate() != null) {
				throw new ServiceException("error.driver.invoice.service.driver.invoice.accept.message");
			}
			
			setDriverAndDepartureDateForDriverInvoice(driver, driverInvoice);
			changeDrugOrderAssociatedWithDriverInvoice(driverInvoice);
		} catch (DaoException exception) {
			throw new ServiceException("Error accept driver invoice with id=" + driverInvoiceId + " by driver=" + driver, exception);
		}
	}

	private void setDriverAndDepartureDateForDriverInvoice(User driver, DriverInvoice driverInvoice) throws DaoException {
		LocalDateTime departureDate = LocalDateTime.now();
		driverInvoice.setDepartureDate(departureDate);
		driverInvoice.setDriverId(driver.getId());
		driverInvoiceDao.update(driverInvoice);
	}

	private void changeDrugOrderAssociatedWithDriverInvoice(DriverInvoice driverInvoice) throws DaoException {
		DrugOrder drugOrder = drugOrderDao.getById(driverInvoice.getDrugOrderId());
		drugOrder.setOrderStatus(DrugOrderStatus.DELIVERY_IN_PROGRESS);
		drugOrderDao.update(drugOrder);
	}

}
