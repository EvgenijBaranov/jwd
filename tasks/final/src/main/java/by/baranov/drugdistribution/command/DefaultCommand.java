package by.baranov.drugdistribution.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DefaultCommand implements Command {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {

		return TypeCommandAction.FORWARD + "/jsp/userLoginPage.jsp";

	}

}
