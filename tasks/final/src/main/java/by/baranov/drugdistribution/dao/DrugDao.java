package by.baranov.drugdistribution.dao;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import by.baranov.drugdistribution.entity.Drug;
import by.baranov.drugdistribution.entity.PharmacyHasDrug;
import by.baranov.drugdistribution.exception.DaoException;

public interface DrugDao extends CRUDOperation<Long, Drug> {

	void saveDrugForPharmacy(Long pharmacyId, Long drugId, int drugQuantity, LocalDate manufactureDate, LocalDate expiryDate, BigDecimal price) throws DaoException;
	void updateDrugQuantityForPharmacy(Long pharmacyId, Long drugId, int drugQuantity, LocalDate manufactureDate, LocalDate expiryDate, BigDecimal price) throws DaoException;
	List<PharmacyHasDrug> getAllDrugsForPharmacy(Long pharmacyId, Long drugId) throws DaoException;
	int getDrugQuantityForPharmacy(Long pharmacyId, Long drugId, LocalDate manufactureDate, LocalDate expiryDate, BigDecimal price) throws DaoException;

	Set<Drug> getDrugsByPharmacyId(Long pharmacyId) throws DaoException;
	
}
