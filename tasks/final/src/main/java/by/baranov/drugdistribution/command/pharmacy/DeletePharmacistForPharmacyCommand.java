package by.baranov.drugdistribution.command.pharmacy;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.UserService;

public class DeletePharmacistForPharmacyCommand implements Command{
	private static final String ATTRIBUTE_NAME_FOR_PHARMACISTS = "pharmacistsForPharmacy";
	private static final Logger LOGGER = LogManager.getLogger();
	private final UserService userService;

	public DeletePharmacistForPharmacyCommand(UserService userService) {
		this.userService = userService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String inputedUserId = request.getParameter(AppConstants.PARAMETER_USER_ID).trim();
		String inputedPharmacyId = request.getParameter(AppConstants.PARAMETER_PHARMACY_ID).trim();
		try {
			Long userId = Long.valueOf(inputedUserId);
			Long pharmacyId = Long.valueOf(inputedPharmacyId);
			userService.deletePharmacistForPharmacy(pharmacyId, userId);
			
			List<User> pharmacistsInPharmacy = userService.getPharmacistsByPharmacyId(pharmacyId);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_PHARMACISTS, pharmacistsInPharmacy);
		} catch (ServiceException exception) {
			request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
			LOGGER.log(Level.ERROR, "Error of deleting user with id=" + inputedUserId + " for pharmacy with id=" + inputedPharmacyId, exception);
		} 
		LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
				getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_PHARMACY_COMMANDS);
		return TypeCommandAction.REDIRECT + AppConstants.FILE_PATH_FOR_PHARMACY_COMMANDS;
	}
}
