package by.baranov.drugdistribution.validation;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PharmacyValidator implements Validator {
	private static final Logger LOGGER = LogManager.getLogger();
	private ValidationResult validationResult = new ValidationResult();

	@Override
	public ValidationResult validate(String... inputedFieldsData) {

		List<String> listInputedFieldsData = Arrays.asList(inputedFieldsData);
		LOGGER.log(Level.DEBUG, "PharmacyValidator inputed data = {}", listInputedFieldsData);

		checkEmptyFields(listInputedFieldsData);
		checkPhoneNumber(listInputedFieldsData);
		LOGGER.log(Level.DEBUG, "validationResult={}", validationResult);
		return validationResult;
	}

	private void checkEmptyFields(List<String> listInputedFieldsData) {
		for (int fieldNumber = 0; fieldNumber < listInputedFieldsData.size(); fieldNumber++) {
			if (listInputedFieldsData.get(fieldNumber).isEmpty()) {
				validationResult
						.addMessage(new ValidationMessage("error.pharmacy.validator.field.number." + (fieldNumber + 1),
															"error.validator.field.value.empty"));
			}
		}
	}

	private void checkPhoneNumber(List<String> listInputedFieldsData) {
		String pharmacyPhoneNumber = listInputedFieldsData.get(2);
		Pattern pattern = Pattern.compile("\\+375\\(\\d{2}\\)\\-\\d{3}\\-\\d{2}\\-\\d{2}");
		Matcher matcher = pattern.matcher(pharmacyPhoneNumber);
		if (!matcher.matches()) {
			validationResult.addMessage(new ValidationMessage("error.pharmacy.validator.field.number.3",
																"error.validator.field.value.not.valid"));
		}
	}
}
