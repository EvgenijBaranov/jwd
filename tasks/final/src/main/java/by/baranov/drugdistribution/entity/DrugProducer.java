package by.baranov.drugdistribution.entity;

import java.io.Serializable;
import java.util.Objects;

public class DrugProducer implements AbstractEntity, Serializable {
	
	private static final long serialVersionUID = -188251503593668258L;
	private Long id;
	private String name;
	private String licenceNumber;
	private Long addressId;

	public DrugProducer() {
	}

	public DrugProducer(Long id, String name, String licenceNumber, Long addressId) {
		this.id = id;
		this.name = name;
		this.licenceNumber = licenceNumber;
		this.addressId = addressId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLicenceNumber() {
		return licenceNumber;
	}

	public void setLicenceNumber(String licenceNumber) {
		this.licenceNumber = licenceNumber;
	}

	public Long getAddressId() {
		return addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(addressId, licenceNumber, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof DrugProducer))
			return false;
		DrugProducer other = (DrugProducer) obj;
		return Objects.equals(addressId, other.addressId) && Objects.equals(licenceNumber.toLowerCase(), other.licenceNumber.toLowerCase())
				&& Objects.equals(name.toLowerCase(), other.name.toLowerCase());
	}

	@Override
	public String toString() {
		return "DrugProducer [name=" + name + ", licenceNumber=" + licenceNumber + ", addressId=" + addressId + "]";
	}

}
