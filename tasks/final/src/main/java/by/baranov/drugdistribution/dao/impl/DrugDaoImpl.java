package by.baranov.drugdistribution.dao.impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.connectionpool.ConnectionManager;
import by.baranov.drugdistribution.dao.DrugDao;
import by.baranov.drugdistribution.entity.Drug;
import by.baranov.drugdistribution.entity.DrugAvailability;
import by.baranov.drugdistribution.entity.PharmacyHasDrug;
import by.baranov.drugdistribution.exception.DaoException;

public class DrugDaoImpl implements DrugDao {
	private static final String GET_ALL_DRUG = "SELECT id, name, dosage, availability, producer_id FROM drug";
	private static final String GET_DRUG_BY_ID = "SELECT id, name, dosage, availability, producer_id FROM drug WHERE id=?";
	private static final String SAVE_DRUG = "INSERT INTO drug (name, dosage, availability, producer_id) VALUES (?,?,?,?)";
	private static final String UPDATE_DRUG = "UPDATE drug SET name=?, dosage=?, availability=?, producer_id=? WHERE id=?";
	private static final String DELETE_DRUG = "DELETE FROM drug WHERE id=?";

	private static final String GET_DRUGS_BY_PHARMACY_ID = "SELECT drug.id, drug.name, drug.dosage, drug.availability, drug.producer_id "
															+ "FROM drug "
															+ "LEFT JOIN pharmacy_has_drug ON drug.id = pharmacy_has_drug.drug_id "
															+ "LEFT JOIN pharmacy ON pharmacy_has_drug.pharmacy_id = pharmacy.id "
															+ "WHERE pharmacy.id=?";
	
	private static final String SAVE_DRUG_FOR_PHARMACY = "INSERT INTO pharmacy_has_drug (pharmacy_id, drug_id, drug_quantity, manufacture_date, expiry_date, price) VALUES (?,?,?,?,?,?)";
	private static final String UPDATE_DRUG_QUANTITY_FOR_PHARMACY = "UPDATE pharmacy_has_drug SET drug_quantity=? "
																	+ "WHERE pharmacy_id=? AND drug_id=? AND manufacture_date=? AND expiry_date=? AND price=?";
	private static final String GET_ALL_DRUG_QUANTITY_FOR_PHARMACY = "SELECT pharmacy_id, drug_id, drug_quantity, manufacture_date, expiry_date, price "
																+ "FROM pharmacy_has_drug "
																+ "WHERE pharmacy_id=? AND drug_id=?";
	private static final String GET_DRUG_QUANTITY_FOR_PHARMACY = "SELECT drug_quantity FROM pharmacy_has_drug "
																+ "WHERE pharmacy_id=? AND drug_id=? AND manufacture_date=? AND expiry_date=? AND price=?";
	
	private static final Logger LOGGER = LogManager.getLogger();
	private ConnectionManager connectionManager;

	public DrugDaoImpl(ConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	@Override
	public List<Drug> getAll() throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_DRUG);
				ResultSet resultSet = preparedStatement.executeQuery()) {
			List<Drug> result = new ArrayList<>();
			while (resultSet.next()) {
				
				Drug drug = parseDrug(resultSet);

				result.add(drug);
			}
			LOGGER.log(Level.DEBUG, "Result of executing the method getAll() : {}", result);
			return result;
		} catch (SQLException exception) {
			throw new DaoException("Error getting all drugs", exception);
		}
	}

	private Drug parseDrug(ResultSet resultSet) throws SQLException {
		Drug drug = new Drug();
		drug.setId(resultSet.getLong("id"));
		drug.setName(resultSet.getString("name"));
		drug.setDosage(resultSet.getDouble("dosage"));

		String stringDrugAvailability = resultSet.getString("availability");
		DrugAvailability drugAvailability = DrugAvailability.valueOf(stringDrugAvailability.toUpperCase());
		drug.setAvailability(drugAvailability);

		drug.setProducerId(resultSet.getLong("producer_id"));
		return drug;
		
	}

	@Override
	public Drug getById(Long id) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_DRUG_BY_ID)) {
			preparedStatement.setLong(1, id);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				resultSet.next();
				Drug result = parseDrug(resultSet);
				LOGGER.log(Level.DEBUG, "Result of executing the method getById() : {}", result);
				return result;
			}
		} catch (SQLException exception) {
			throw new DaoException("Error getting drug by id=" + id, exception);
		}
	}

	@Override
	public void save(Drug drug) throws DaoException {

		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SAVE_DRUG,Statement.RETURN_GENERATED_KEYS)) {
			int i = 0;
			preparedStatement.setString(++i, drug.getName());
			preparedStatement.setDouble(++i, drug.getDosage());
			preparedStatement.setString(++i, drug.getAvailability().name().toLowerCase());
			preparedStatement.setLong(++i, drug.getProducerId());
			preparedStatement.executeUpdate();
			try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
				while (generatedKeys.next()) {
					drug.setId(generatedKeys.getLong(1));
				}
			}
			LOGGER.log(Level.DEBUG, "Result of executing the method save({}) is success", drug);
		} catch (SQLException exception) {
			throw new DaoException("Error saving drug=" + drug, exception);
		}
	}

	@Override
	public void update(Drug drug) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_DRUG)) {
			int i = 0;
			preparedStatement.setString(++i, drug.getName());
			preparedStatement.setDouble(++i, drug.getDosage());
			preparedStatement.setString(++i, drug.getAvailability().name().toLowerCase());
			preparedStatement.setLong(++i, drug.getProducerId());
			preparedStatement.setLong(++i, drug.getId());
			preparedStatement.executeUpdate();
			LOGGER.log(Level.DEBUG, "Result of executing the method update({}) is success", drug);
		} catch (SQLException exception) {
			throw new DaoException("Error updating drug=" + drug, exception);
		}
	}

	@Override
	public void delete(Drug drug) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(DELETE_DRUG)) {
			preparedStatement.setLong(1, drug.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException exception) {
			throw new DaoException("Error deleting drug=" + drug, exception);
		}
		LOGGER.log(Level.DEBUG, "Result of executing the method delete({}) is success ", drug);
	}

	@Override
	public void saveDrugForPharmacy(Long pharmacyId, Long drugId, int drugQuantity, 
												LocalDate manufactureDate, LocalDate expiryDate, BigDecimal price) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SAVE_DRUG_FOR_PHARMACY)) {
			int i = 0;
			preparedStatement.setLong(++i, pharmacyId);
			preparedStatement.setLong(++i, drugId);
			preparedStatement.setInt(++i, drugQuantity);
			preparedStatement.setDate(++i, Date.valueOf(manufactureDate));
			preparedStatement.setDate(++i, Date.valueOf(expiryDate));
			preparedStatement.setBigDecimal(++i, price);
			preparedStatement.executeUpdate();

		} catch (SQLException exception) {
			throw new DaoException("Error saving drug quantity(" + drugId+ ", " + drugQuantity + ", "+manufactureDate + ", " + expiryDate + ", " 
														+ price+ ") " +" for pharmacy with id=" + pharmacyId, exception);
		}
		LOGGER.log(Level.DEBUG, "Result of executing the method saveDrugForPharmacy() is success");
	}
	
	@Override
	public void updateDrugQuantityForPharmacy(Long pharmacyId, Long drugId, int drugQuantity, LocalDate manufactureDate,
												LocalDate expiryDate, BigDecimal price) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_DRUG_QUANTITY_FOR_PHARMACY)) {
			int i = 0;
			preparedStatement.setInt(++i, drugQuantity);
			preparedStatement.setLong(++i, pharmacyId);
			preparedStatement.setLong(++i, drugId);
			preparedStatement.setDate(++i, Date.valueOf(manufactureDate));
			preparedStatement.setDate(++i, Date.valueOf(expiryDate));
			preparedStatement.setBigDecimal(++i, price);
			preparedStatement.executeUpdate();

		} catch (SQLException exception) {
			throw new DaoException("Error updating drug quantity(" + drugId+ ", " + drugQuantity + ", "+manufactureDate + ", " + expiryDate + ", " 
																	+ price+ ") " +" for pharmacy with id=" + pharmacyId, exception);
		}
		LOGGER.log(Level.DEBUG, "Result of executing the method updateDrugForPharmacy() is success");
	}

	@Override
	public List<PharmacyHasDrug> getAllDrugsForPharmacy(Long pharmacyId, Long drugId) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_DRUG_QUANTITY_FOR_PHARMACY)) {
			int i = 0;
			preparedStatement.setLong(++i, pharmacyId);
			preparedStatement.setLong(++i, drugId);
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				List<PharmacyHasDrug> result = new ArrayList<>();
				while(resultSet.next()) {
					PharmacyHasDrug pharmacyDrugQuantity = parsePharmacyDrugQuantity(resultSet);
					
					result.add(pharmacyDrugQuantity);
				}
				LOGGER.log(Level.DEBUG, "Result of executing the method getAllDrugQuantityInPharmacy({}, {}) is : {}", pharmacyId, drugId, result);
				return result;
			}
		} catch (SQLException exception) {
			throw new DaoException("Error getting all drugs quantity for pharmacy", exception);
		} 
	}

	private PharmacyHasDrug parsePharmacyDrugQuantity(ResultSet resultSet) throws SQLException {
		PharmacyHasDrug pharmacyDrugQuantity = new PharmacyHasDrug();
		pharmacyDrugQuantity.setPharmacyId(resultSet.getLong("pharmacy_id"));
		pharmacyDrugQuantity.setDrugId(resultSet.getLong("drug_id"));
		pharmacyDrugQuantity.setDrugQuantity(resultSet.getInt("drug_quantity"));
		
		String stringManufactureDate = resultSet.getString("manufacture_date");
		LocalDate manufactureDate = LocalDate.parse(stringManufactureDate);
		pharmacyDrugQuantity.setManufactureDate(manufactureDate);

		String stringExpiryDate = resultSet.getString("expiry_date");
		LocalDate expiryDate = LocalDate.parse(stringExpiryDate);
		pharmacyDrugQuantity.setExpiryDate(expiryDate);

		pharmacyDrugQuantity.setPrice(resultSet.getBigDecimal("price"));
		return pharmacyDrugQuantity;
	}
	
	@Override
	public int getDrugQuantityForPharmacy(Long pharmacyId, Long drugId, LocalDate manufactureDate, LocalDate expiryDate,
														BigDecimal drugPrice) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_DRUG_QUANTITY_FOR_PHARMACY)) {
			int i = 0;
			preparedStatement.setLong(++i, pharmacyId);
			preparedStatement.setLong(++i, drugId);
			preparedStatement.setDate(++i, Date.valueOf(manufactureDate));
			preparedStatement.setDate(++i, Date.valueOf(expiryDate));
			preparedStatement.setBigDecimal(++i, drugPrice);
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				resultSet.next();
				int drugQuantity = resultSet.getInt("drug_quantity");
				LOGGER.log(Level.DEBUG, "Result of executing the method getDrugQuantityInPharmacy(): {}", drugQuantity);
				return drugQuantity;
			}
		} catch (SQLException exception) {
			throw new DaoException("Error getDrugQuantityForPharmacy(" + pharmacyId + "," + drugId + "," + manufactureDate + "," 
																	+ expiryDate + "," + drugPrice + ")", exception);
		} 
	}

	@Override
	public Set<Drug> getDrugsByPharmacyId(Long pharmacyId) throws DaoException {
		
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_DRUGS_BY_PHARMACY_ID)) {
			preparedStatement.setLong(1, pharmacyId);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				Set<Drug> result = new HashSet<>();
				while(resultSet.next()) {
					Drug drug = parseDrug(resultSet);
					result.add(drug);
				}
				LOGGER.log(Level.DEBUG, "Result of executing the method getDrugsByPharmacyId() : {}", result);
				return result;
			}
		} catch (SQLException exception) {
			throw new DaoException("Error getting drugs by pharmacy id=" + pharmacyId, exception);
		} 
	}

}
