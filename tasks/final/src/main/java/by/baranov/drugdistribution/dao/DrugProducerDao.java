package by.baranov.drugdistribution.dao;

import by.baranov.drugdistribution.entity.DrugProducer;

public interface DrugProducerDao extends CRUDOperation<Long, DrugProducer>{

}
