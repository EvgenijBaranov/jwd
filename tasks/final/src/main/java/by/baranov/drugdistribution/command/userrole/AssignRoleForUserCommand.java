package by.baranov.drugdistribution.command.userrole;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.SecurityContext;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.entity.UserRole;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.UserRoleService;

public class AssignRoleForUserCommand implements Command {
	private static final String ATTRIBUTE_NAME_FOR_USER_ROLES = "userRolesForUser";
	private static final Logger LOGGER = LogManager.getLogger();
	private final UserRoleService userRoleService;

	public AssignRoleForUserCommand(UserRoleService userRoleService) {
		this.userRoleService = userRoleService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		String inputedUserId = request.getParameter(AppConstants.PARAMETER_USER_ID).trim();
		String inputedUserRoleId = request.getParameter(AppConstants.PARAMETER_USER_ROLE_ID).trim();
		
		try {
			Long userId = Long.valueOf(inputedUserId);
			Long userRoleId = Long.valueOf(inputedUserRoleId);
			userRoleService.assignRoleForUser(userId, userRoleId);
			
			List<UserRole> userRoles = userRoleService.getUserRolesByUserId(userId);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_USER_ROLES, userRoles);
			
			User registeredUser = (User) request.getSession().getAttribute(AppConstants.ATTRIBUTE_NAME_REGISTERED_USER);
			if(registeredUser.getId().equals(userId)) {
				updateUserRolesForSecurityContext(userId);
			}
		} catch (ServiceException exception) {
			request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
			LOGGER.log(Level.ERROR, "Error of assigning user role with id=" + inputedUserRoleId + " for user with id=" + inputedUserId, exception);
		} 
		LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
				getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_USER_COMMANDS);
		return TypeCommandAction.REDIRECT + AppConstants.FILE_PATH_FOR_USER_COMMANDS;
	}
	
	private void updateUserRolesForSecurityContext(Long userId) throws ServiceException {
		String currentSessionId = SecurityContext.getInstance().getCurrentSessionId();
		List<UserRole> userRolesAfterSaving = userRoleService.getUserRolesByUserId(userId);
		SecurityContext.getInstance().addCurrentUserRolesToSession(currentSessionId, userRolesAfterSaving);
	}

}
