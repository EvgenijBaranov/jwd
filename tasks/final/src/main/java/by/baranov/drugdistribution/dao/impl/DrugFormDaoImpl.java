package by.baranov.drugdistribution.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.connectionpool.ConnectionManager;
import by.baranov.drugdistribution.dao.DrugFormDao;
import by.baranov.drugdistribution.entity.DrugForm;
import by.baranov.drugdistribution.exception.DaoException;

public class DrugFormDaoImpl implements DrugFormDao{
	private static final String GET_ALL_DRUG_FORM = "SELECT id, name FROM drug_form";
	private static final String GET_DRUG_FORM_BY_ID = "SELECT id, name FROM drug_form WHERE id=?";
	private static final String SAVE_DRUG_FORM = "INSERT INTO drug_form (name) VALUES (?)";
	private static final String UPDATE_DRUG_FORM = "UPDATE drug_form SET name=?  WHERE id=?";
	private static final String DELETE_DRUG_FORM = "DELETE FROM drug_form WHERE id=?";
	
	private static final String ASSIGN_DRUG_FORM = "INSERT INTO drug_form_relation (drug_id, drug_form_id) VALUES (?,?)";
	private static final String DELETE_DRUG_FORM_FOR_DRUG = "DELETE FROM drug_form_relation WHERE drug_id=? AND drug_form_id=?";
	private static final String GET_DRUG_FORMS_BY_DRUG_ID = "SELECT drug_form.id, drug_form.name "
															+ "FROM drug_form "
															+ "LEFT JOIN drug_form_relation ON drug_form.id = drug_form_relation.drug_form_id "
															+ "LEFT JOIN drug ON drug_form_relation.drug_id = drug.id "
															+ "WHERE drug.id=?";
	
	private static final Logger LOGGER = LogManager.getLogger();

	private ConnectionManager connectionManager;

	public DrugFormDaoImpl(ConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	@Override
	public List<DrugForm> getAll() throws DaoException {

		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_DRUG_FORM);
				ResultSet resultSet = preparedStatement.executeQuery()) {
			List<DrugForm> result = new ArrayList<>();
			while (resultSet.next()) {
				DrugForm drugForm = parseDrugForm(resultSet);
				result.add(drugForm);
			}
			LOGGER.log(Level.DEBUG, "Result of executing method getAll() : {}", result);
			return result;
		} catch (SQLException exception) {
			throw new DaoException("Error getting all drug forms", exception);
		}
	}

	private DrugForm parseDrugForm(ResultSet resultSet) throws SQLException {
		DrugForm drugForm = new DrugForm();
		drugForm.setId(resultSet.getLong("id"));
		drugForm.setName(resultSet.getString("name"));
		return drugForm;
	}

	@Override
	public DrugForm getById(Long id) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_DRUG_FORM_BY_ID)) {
			preparedStatement.setLong(1, id);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				resultSet.next();
				DrugForm result = parseDrugForm(resultSet);
				LOGGER.log(Level.DEBUG, "Result of executing method getById() : {}", result);
				return result;
			}
		} catch (SQLException exception) {
			throw new DaoException("Error getting drug form with id=" + id, exception);
		}
	}

	@Override
	public void save(DrugForm drugForm) throws DaoException {

		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SAVE_DRUG_FORM, Statement.RETURN_GENERATED_KEYS)) {
			int i = 0;
			preparedStatement.setString(++i, drugForm.getName());
			preparedStatement.executeUpdate();

			try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
				while (generatedKeys.next()) {
					drugForm.setId(generatedKeys.getLong(1));
				}
			}
			LOGGER.log(Level.DEBUG, "Result of executing method save() is success");
		} catch (SQLException exception) {
			throw new DaoException("Error saving drug form=" + drugForm, exception);
		}
	}

	@Override
	public void update(DrugForm drugForm) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_DRUG_FORM)) {
			int i = 0;
			preparedStatement.setString(++i, drugForm.getName());
			preparedStatement.setLong(++i, drugForm.getId());
			preparedStatement.executeUpdate();
			LOGGER.log(Level.DEBUG, "Result of executing method update() is success");
		} catch (SQLException exception) {
			throw new DaoException("Error updating drug form=" + drugForm, exception);
		}
	}

	@Override
	public void delete(DrugForm drugForm) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(DELETE_DRUG_FORM)) {
			preparedStatement.setLong(1, drugForm.getId());
			preparedStatement.executeUpdate();
			LOGGER.log(Level.DEBUG, "Result of executing method delete() is success");
		} catch (SQLException exception) {
			throw new DaoException("Error deleting drug form=" + drugForm, exception);
		}
	}

	@Override
	public void assignDrugFormForDrug(Long drugId, Long drugFormId) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(ASSIGN_DRUG_FORM)) {
			int i = 0;
			preparedStatement.setLong(++i, drugId);
			preparedStatement.setLong(++i, drugFormId);
			preparedStatement.executeUpdate();
			LOGGER.log(Level.DEBUG, "Result of executing method assignDrugForm({}, {}) is success", drugId, drugFormId);
		} catch (SQLException exception) {
			throw new DaoException("Error assigning drug form with id=" + drugFormId + " for drug with id=" + drugId, exception);
		}
	}

	@Override
	public List<DrugForm> getDrugFormsByDrugId(Long drugId) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_DRUG_FORMS_BY_DRUG_ID)) {
			preparedStatement.setLong(1, drugId);
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				List<DrugForm> result = new ArrayList<>();
				while(resultSet.next()) {
					DrugForm drugForm = new DrugForm();
					drugForm.setId(resultSet.getLong("id"));
					drugForm.setName(resultSet.getString("name"));
					result.add(drugForm);
				}
				LOGGER.log(Level.DEBUG, "Result of executing method getDrugFormsByDrugId({}) : {}", drugId, result);
				return result;
			}
		} catch (SQLException exception) {
			throw new DaoException("Error getting drug forms by drug id=" + drugId, exception);
		}
	}

	@Override
	public void deleteDrugFormForDrug(Long drugId, Long drugFormId) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(DELETE_DRUG_FORM_FOR_DRUG)) {
			int i=0;
			preparedStatement.setLong(++i, drugId);
			preparedStatement.setLong(++i, drugFormId);
			preparedStatement.executeUpdate();
			LOGGER.log(Level.DEBUG, "Result of executing method deleteDrugFormForDrug() is success");
		} catch (SQLException exception) {
			throw new DaoException("Error deleting drug form with id=" + drugFormId + " for drug with id=" + drugId, exception);
		}
	}
}
