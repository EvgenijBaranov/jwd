package by.baranov.drugdistribution.service;

import by.baranov.drugdistribution.entity.DrugProducer;
import by.baranov.drugdistribution.exception.ServiceException;

public interface DrugProducerService{
	
	DrugProducer getById(Long id) throws ServiceException;	

}
