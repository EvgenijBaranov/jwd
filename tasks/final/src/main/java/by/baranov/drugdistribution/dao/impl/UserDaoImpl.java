package by.baranov.drugdistribution.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.connectionpool.ConnectionManager;
import by.baranov.drugdistribution.dao.UserDao;
import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.exception.DaoException;

public class UserDaoImpl implements UserDao{

	private static final String GET_ALL_USER = "SELECT id, name, surname, login, password FROM user_account";
	private static final String GET_USER_BY_ID = "SELECT id, name, surname, login, password FROM user_account WHERE id=?";
	private static final String SAVE_USER = "INSERT INTO user_account (name, surname, login, password) VALUES (?,?,?,?)";
	private static final String UPDATE_USER = "UPDATE user_account SET name=?, surname=?, login=?, password=? WHERE id=?";
	private static final String DELETE_USER = "DELETE FROM user_account WHERE id=?";
	
	private static final String ASSIGN_PHARMACIST_FOR_PHARMACY = "INSERT INTO pharmacy_has_pharmacist (pharmacy_id, pharmacist_id) VALUES (?,?)";
	private static final String DELETE_PHARMACIST_FROM_PHARMACY = "DELETE FROM pharmacy_has_pharmacist WHERE pharmacy_id=? AND pharmacist_id=?";
	private static final String GET_PHARMACISTS_BY_PHARMACY_ID = "SELECT user_account.id, user_account.name, user_account.surname, user_account.login, user_account.password "
																+ "FROM user_account "
																+ "LEFT JOIN pharmacy_has_pharmacist ON user_account.id = pharmacy_has_pharmacist.pharmacist_id "
																+ "LEFT JOIN pharmacy ON pharmacy_has_pharmacist.pharmacy_id = pharmacy.id "
																+ "WHERE pharmacy.id=? ";
	
	private static final String GET_USERS_BY_USER_ROLE_ID = "SELECT user_account.id, user_account.name, user_account.surname, user_account.login, user_account.password "
															+ "FROM user_account "
															+ "LEFT JOIN user_role_relation ON user_account.id = user_role_relation.user_id "
															+ "LEFT JOIN user_role ON user_role_relation.user_role_id = user_role.id "
															+ "WHERE user_role.id=? ";
	
	private static final String GET_USER_BY_LOGIN = "SELECT id, name, surname, login, password FROM user_account WHERE login=?";
	
	private static final Logger LOGGER = LogManager.getLogger();
	private ConnectionManager connectionManager;

	public UserDaoImpl(ConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	@Override
	public List<User> getAll() throws DaoException {

		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_USER);
				ResultSet resultSet = preparedStatement.executeQuery()) {
			List<User> result = new ArrayList<>();
			while (resultSet.next()) {
				User user = parseUser(resultSet);
				result.add(user);
			}
			LOGGER.log(Level.DEBUG, "Result of executing method getAll() : {}", result);
			return result;
		} catch (SQLException exception) {
			throw new DaoException("Error getting all users", exception);
		}
	}


	@Override
	public User getById(Long id) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_USER_BY_ID)) {
			preparedStatement.setLong(1, id);
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				resultSet.next();
				User result = parseUser(resultSet);
				LOGGER.log(Level.DEBUG, "Result of executing method getById() : {}", result);
				return result;
			}
		} catch (SQLException exception) {
			throw new DaoException("Error getting user with id=" + id, exception);
		}
	}

	@Override
	public void save(User user) throws DaoException {

		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SAVE_USER, Statement.RETURN_GENERATED_KEYS)) {
			int i = 0;
			preparedStatement.setString(++i, user.getName());
			preparedStatement.setString(++i, user.getSurname());
			preparedStatement.setString(++i, user.getLogin());
			preparedStatement.setString(++i, user.getPassword());
			preparedStatement.executeUpdate();

			try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
				while (generatedKeys.next()) {
					user.setId(generatedKeys.getLong(1));
				}
			}
			LOGGER.log(Level.DEBUG, "Result of executing method save() is success");
		} catch (SQLException exception) {
			throw new DaoException("Error saving user=" + user, exception);
		}
	}

	@Override
	public void update(User user) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER)) {
			int i = 0;
			preparedStatement.setString(++i, user.getName());
			preparedStatement.setString(++i, user.getSurname());
			preparedStatement.setString(++i, user.getLogin());
			preparedStatement.setString(++i, user.getPassword());
			preparedStatement.setLong(++i, user.getId());
			preparedStatement.executeUpdate();
			LOGGER.log(Level.DEBUG, "Result of executing method update() is success");
		} catch (SQLException exception) {
			throw new DaoException("Error updating user=" + user, exception);
		}
	}

	@Override
	public void delete(User user) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(DELETE_USER)) {
			preparedStatement.setLong(1, user.getId());
			preparedStatement.executeUpdate();
			LOGGER.log(Level.DEBUG, "Result of executing method delete() is success");
		} catch (SQLException exception) {
			throw new DaoException("Error deleting user=" + user, exception);
		}
	}


	@Override
	public User getUserByLogin(String login) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_USER_BY_LOGIN)) {
			preparedStatement.setString(1, login);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				resultSet.next();
				User result = parseUser(resultSet);
				LOGGER.log(Level.DEBUG, "Result of executing method getUserByLogin() : {}", result);
				return result;
			}
		} catch (SQLException exception) {
			throw new DaoException("Error getting user by login=" + login, exception);
		}
	}

	private User parseUser(ResultSet resultSet) throws SQLException {
		User user = new User();
		user.setId(resultSet.getLong("id"));
		user.setName(resultSet.getString("name"));
		user.setSurname(resultSet.getString("surname"));
		user.setLogin(resultSet.getString("login"));
		user.setPassword(resultSet.getString("password"));
		return user;
	}

	@Override
	public void assignPharmacistToPharmacy(Long pharmacyId, Long userId) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(ASSIGN_PHARMACIST_FOR_PHARMACY)) {
			int i = 0;
			preparedStatement.setLong(++i, pharmacyId);
			preparedStatement.setLong(++i, userId);
			preparedStatement.executeUpdate();
			LOGGER.log(Level.DEBUG, "Result of executing method assignPharmacistToPharmacy({}, {}) is success", userId, pharmacyId);
		} catch (SQLException exception) {
			throw new DaoException("Error assigning  pharmacist with id=" + userId + " for pharmacy with id=" + pharmacyId, exception);
		}
	}
	
	@Override
	public void deletePharmacistFromPharmacy(Long pharmacyId, Long userId) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(DELETE_PHARMACIST_FROM_PHARMACY)) {
			int i = 0;
			preparedStatement.setLong(++i, pharmacyId);
			preparedStatement.setLong(++i, userId);
			preparedStatement.executeUpdate();
			LOGGER.log(Level.DEBUG, "Result of executing method delete() is success");
		} catch (SQLException exception) {
			throw new DaoException("Error deleting  pharmacist with id=" + userId + " from pharmacy with id=" + pharmacyId, exception);
		}
	}

	@Override
	public List<User> getPharmacistsForPharmacy(Long pharmacyId) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_PHARMACISTS_BY_PHARMACY_ID)) {
			preparedStatement.setLong(1, pharmacyId);
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				List<User> result = new ArrayList<>();
				while(resultSet.next()) {
					User pharmacist = parseUser(resultSet);
					result.add(pharmacist);
				}
				LOGGER.log(Level.DEBUG, "Result of executing method getPharmacistsForPharmacy() : {}", result);
				return result;
			}
		} catch (SQLException exception) {
			throw new DaoException("Error getting all pharmacists for pharmacy with id=" + pharmacyId, exception);
		}
	}

	@Override
	public List<User> getAllUsersByUserRoleId(Long userRoleId) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_USERS_BY_USER_ROLE_ID)) {
			preparedStatement.setLong(1, userRoleId);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				List<User> result = new ArrayList<>();
				while(resultSet.next()) {
					User pharmacist = parseUser(resultSet);
					result.add(pharmacist);
				}
				LOGGER.log(Level.DEBUG, "Result of executing method getAllUsersByUserRoleId() : {}", result);
				return result;
			}
		} catch (SQLException exception) {
			throw new DaoException("Error getting all users by user role with id=" + userRoleId, exception);
		}
	}

}
