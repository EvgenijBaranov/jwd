package by.baranov.drugdistribution.exception;

public class SecurityPropertyFileException extends RuntimeException{
	
	private static final long serialVersionUID = -3718635001332817660L;

	public SecurityPropertyFileException(String message, Throwable cause) {
		super(message, cause);
	}

	public SecurityPropertyFileException(String message) {
		super(message);
	}

}
