package by.baranov.drugdistribution.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.connectionpool.ConnectionManager;
import by.baranov.drugdistribution.dao.DriverInvoiceDao;
import by.baranov.drugdistribution.entity.DriverInvoice;
import by.baranov.drugdistribution.exception.DaoException;

public class DriverInvoiceDaoImpl implements DriverInvoiceDao{
	private static final String GET_ALL_DRIVER_INVOICE = "SELECT id, driver_id, drug_order_id, departure_date, delivery_date, drug_manufacture_date, drug_expiry_date, drug_price FROM driver_invoice";
	private static final String GET_DRIVER_INVOICE_BY_ID = "SELECT id, driver_id, drug_order_id, departure_date, delivery_date, drug_manufacture_date, drug_expiry_date, drug_price FROM driver_invoice WHERE id=?";
	private static final String SAVE_DRIVER_INVOICE = "INSERT INTO driver_invoice (driver_id, drug_order_id, departure_date, delivery_date, drug_manufacture_date, drug_expiry_date, drug_price) VALUES (?,?,?,?,?,?,?)";
	private static final String UPDATE_DRIVER_INVOICE = "UPDATE driver_invoice SET driver_id=?, drug_order_id=?, departure_date=?, delivery_date=?, drug_manufacture_date=?, drug_expiry_date=?, drug_price=? WHERE id=?";
	private static final String DELETE_DRIVER_INVOICE = "DELETE FROM driver_invoice WHERE id=?";
	
	private static final String GET_DRIVER_INVOICE_BY_DRUG_ORDER_ID = "SELECT id, driver_id, drug_order_id, departure_date, delivery_date, drug_manufacture_date, drug_expiry_date, drug_price "
																	+ "FROM driver_invoice  WHERE drug_order_id=?";
	
	private static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private static final Logger LOGGER = LogManager.getLogger();

	private ConnectionManager connectionManager;

	public DriverInvoiceDaoImpl(ConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	@Override
	public List<DriverInvoice> getAll() throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_DRIVER_INVOICE);
				ResultSet resultSet = preparedStatement.executeQuery()) {
			List<DriverInvoice> result = new ArrayList<>();
			while (resultSet.next()) {
				DriverInvoice driverInvoice = parseDriverInvoice(resultSet);
				result.add(driverInvoice);
			}
			LOGGER.log(Level.DEBUG, "Result of executing method getAll() : {}", result);
			return result;
		} catch (SQLException exception) {
			throw new DaoException("Error getting all driver invoices", exception);
		} 
	}

	private DriverInvoice parseDriverInvoice(ResultSet resultSet) throws SQLException {
		DriverInvoice driverInvoice = new DriverInvoice();
		driverInvoice.setId(resultSet.getLong("id"));

		Long driverId = resultSet.getLong("driver_id");
		if(driverId != 0) {
			driverInvoice.setDriverId(driverId);
		}
		
		driverInvoice.setDrugOrderId(resultSet.getLong("drug_order_id"));
		
		String stringDepartureDateTime = resultSet.getString("departure_date");
		if(stringDepartureDateTime != null) {
			LocalDateTime departureDateTime = LocalDateTime.parse(stringDepartureDateTime, DateTimeFormatter.ofPattern(DATE_TIME_FORMAT));
			driverInvoice.setDepartureDate(departureDateTime);
		}
		
		String stringDeliveryDateTime = resultSet.getString("delivery_date");
		if(stringDeliveryDateTime != null) {
			LocalDateTime deliveryDateTime = LocalDateTime.parse(stringDeliveryDateTime, DateTimeFormatter.ofPattern(DATE_TIME_FORMAT));
			driverInvoice.setDeliveryDate(deliveryDateTime);
		}
		
		String stringDrugManufactureDate = resultSet.getString("drug_manufacture_date");
		LocalDate drugManufactureDate = LocalDate.parse(stringDrugManufactureDate);
		driverInvoice.setDrugManufactureDate(drugManufactureDate);
		
		String stringDrugExpiryDate = resultSet.getString("drug_expiry_date");
		LocalDate drugExpiryDate = LocalDate.parse(stringDrugExpiryDate);
		driverInvoice.setDrugExpiryDate(drugExpiryDate);
		
		driverInvoice.setDrugPrice(resultSet.getBigDecimal("drug_price"));
		
		return driverInvoice;
	}

	@Override
	public DriverInvoice getById(Long id) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_DRIVER_INVOICE_BY_ID)) {
			preparedStatement.setLong(1, id);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				resultSet.next();
				DriverInvoice result = parseDriverInvoice(resultSet);
				LOGGER.log(Level.DEBUG, "Result of executing method getById() : {}", result);
				return result;
			}
		} catch (SQLException exception) {
			throw new DaoException("Error getting driver invoice by id=" + id, exception);
		} 
	}

	@Override
	public void save(DriverInvoice driverInvoice) throws DaoException {

		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SAVE_DRIVER_INVOICE, Statement.RETURN_GENERATED_KEYS)) {
			int i = 0;
			preparedStatement.setObject(++i, driverInvoice.getDriverId());
			preparedStatement.setLong(++i, driverInvoice.getDrugOrderId());
			preparedStatement.setObject(++i, driverInvoice.getDepartureDate());
			preparedStatement.setObject(++i, driverInvoice.getDeliveryDate());
			preparedStatement.setDate(++i, Date.valueOf(driverInvoice.getDrugManufactureDate()));
			preparedStatement.setDate(++i, Date.valueOf(driverInvoice.getDrugExpiryDate()));
			preparedStatement.setBigDecimal(++i, driverInvoice.getDrugPrice());
			preparedStatement.executeUpdate();

			try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
				while (generatedKeys.next()) {
					driverInvoice.setId(generatedKeys.getLong(1));
				}
			}
			LOGGER.log(Level.DEBUG, "Result of executing method save() is success");
		} catch (SQLException exception) {
			throw new DaoException("Error saving driver invoice=" + driverInvoice, exception);
		}
	}

	@Override
	public void update(DriverInvoice driverInvoice) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_DRIVER_INVOICE)) {
			int i = 0;
			preparedStatement.setObject(++i, driverInvoice.getDriverId());
			preparedStatement.setLong(++i, driverInvoice.getDrugOrderId());
			preparedStatement.setObject(++i, driverInvoice.getDepartureDate());
			preparedStatement.setObject(++i, driverInvoice.getDeliveryDate());
			preparedStatement.setDate(++i, Date.valueOf(driverInvoice.getDrugManufactureDate()));
			preparedStatement.setDate(++i, Date.valueOf(driverInvoice.getDrugExpiryDate()));
			preparedStatement.setBigDecimal(++i, driverInvoice.getDrugPrice());
			preparedStatement.setLong(++i, driverInvoice.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException exception) {
			throw new DaoException("Error updating driver invoice =" + driverInvoice, exception);
		}
		LOGGER.log(Level.DEBUG, "Result of executing method update() is success");
	}

	@Override
	public void delete(DriverInvoice driverInvoice) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(DELETE_DRIVER_INVOICE)) {
			preparedStatement.setLong(1, driverInvoice.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException exception) {
			throw new DaoException("Error deleting driver invoice=" + driverInvoice, exception);
		}
		LOGGER.log(Level.DEBUG, "Result of executing method delete() is success");
	}

	@Override
	public DriverInvoice getDriverInvoiceByDrugOrderId(Long drugOrderId) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_DRIVER_INVOICE_BY_DRUG_ORDER_ID)) {
			preparedStatement.setLong(1, drugOrderId);
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				resultSet.next();
				DriverInvoice result = parseDriverInvoice(resultSet);
				LOGGER.log(Level.DEBUG, "Result of executing method getDriverInvoiceByDrugOrderId() : {}", result);
				return result;
			}
		} catch (SQLException exception) {
			throw new DaoException("Error getting driver invoice by drug order id=" + drugOrderId, exception);
		} 
	}
}
