package by.baranov.drugdistribution.command.drug;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.Address;
import by.baranov.drugdistribution.entity.Drug;
import by.baranov.drugdistribution.entity.DrugProducer;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.AddressService;
import by.baranov.drugdistribution.service.DrugProducerService;
import by.baranov.drugdistribution.service.DrugService;

public class ViewAllDrugsCommand implements Command{
	private static final String ATTRIBUTE_NAME_FOR_DRUGS = "drugs";
	private static final String ATTRIBUTE_NAME_FOR_DRUG_PRODUCERS = "drugProducers";
	private static final String ATTRIBUTE_NAME_FOR_DRUG_PRODUCER_ADDRESSES = "drugProducerAddresses";
	private static final String ATTRIBUTE_VALUE_FOR_VIEW = "viewAllDrugs";
	private static final Logger LOGGER = LogManager.getLogger();
	private final DrugService drugService;
	private final DrugProducerService drugProducerService;
	private final AddressService addressService;

	public ViewAllDrugsCommand(DrugService drugService, DrugProducerService drugProducerService,
			AddressService addressService) {
		this.drugService = drugService;
		this.drugProducerService = drugProducerService;
		this.addressService = addressService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		try {
			List<Drug> drugs = drugService.getAll();
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_DRUGS, drugs);
			
			Set<DrugProducer> drugProducers = new HashSet<>();
			Set<Address> drugProducerAddresses = new HashSet<>();
			for(Drug drug : drugs) {
				DrugProducer drugProducer = drugProducerService.getById(drug.getProducerId());
				drugProducers.add(drugProducer);
				Address drugProducerAddress = addressService.getById(drugProducer.getAddressId());
				drugProducerAddresses.add(drugProducerAddress);
			}
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_DRUG_PRODUCERS, drugProducers);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_DRUG_PRODUCER_ADDRESSES, drugProducerAddresses);
		} catch (ServiceException exception) {
			request.setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
			LOGGER.log(Level.ERROR, "Error of view all drugs : ", exception);
		}
		request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_VIEW, ATTRIBUTE_VALUE_FOR_VIEW);
		LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
				getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_DRUG_COMMANDS);
		return TypeCommandAction.FORWARD + AppConstants.FILE_PATH_FOR_DRUG_COMMANDS;
	}
}
