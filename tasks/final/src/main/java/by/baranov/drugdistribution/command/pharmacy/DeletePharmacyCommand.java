package by.baranov.drugdistribution.command.pharmacy;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.Pharmacy;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.PharmacyService;

public class DeletePharmacyCommand implements Command{
	private static final String ATTRIBUTE_NAME_FOR_PHARMACIES = "pharmacies";
	private static final Logger LOGGER = LogManager.getLogger();
	private final PharmacyService pharmacyService;

	public DeletePharmacyCommand(PharmacyService pharmacyService) {
		this.pharmacyService = pharmacyService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String inputedPharmacyId = request.getParameter(AppConstants.PARAMETER_PHARMACY_ID).trim();
		try {
			Long pharmacyId = Long.valueOf(inputedPharmacyId);
			pharmacyService.delete(pharmacyId);
			
			List<Pharmacy> pharmacies = pharmacyService.getAll();
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_PHARMACIES, pharmacies);
		} catch (ServiceException exception) {
			request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
			LOGGER.log(Level.ERROR, "Pharmacy deleting error", exception);
		}
		LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
				getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_PHARMACY_COMMANDS);
		return TypeCommandAction.REDIRECT + AppConstants.FILE_PATH_FOR_PHARMACY_COMMANDS;
	}
}
