package by.baranov.drugdistribution.service;

import java.util.List;

import by.baranov.drugdistribution.entity.Address;
import by.baranov.drugdistribution.entity.Pharmacy;
import by.baranov.drugdistribution.exception.ServiceException;

public interface PharmacyService{
	
	List<Pharmacy> getAll() throws ServiceException;
	Pharmacy getById(Long id) throws ServiceException;	
	void save(Pharmacy pharmacy, Address pharmacyAddress) throws ServiceException;
	void update(Pharmacy pharmacy, Address pharmacyAddress) throws ServiceException;
	void delete (Long id) throws ServiceException;
	
	List<Pharmacy> getPharmaciesByParmacistId(Long pharmacistId) throws ServiceException;
	
}
