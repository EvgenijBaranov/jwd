package by.baranov.drugdistribution.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

public class DriverInvoice implements AbstractEntity, Serializable {
	private static final long serialVersionUID = 2120791218634425324L;
	private Long id;
	private Long driverId;
	private Long drugOrderId;
	private LocalDateTime departureDate;
	private LocalDateTime deliveryDate;
	private LocalDate drugManufactureDate;
	private LocalDate drugExpiryDate;
	private BigDecimal drugPrice;

	public DriverInvoice() {
	}

	public DriverInvoice(Long id, Long driverId, Long drugOrderId, LocalDateTime departureDate,
			LocalDateTime deliveryDate, LocalDate drugManufactureDate, LocalDate drugExpiryDate, BigDecimal drugPrice) {
		this.id = id;
		this.driverId = driverId;
		this.drugOrderId = drugOrderId;
		this.departureDate = departureDate;
		this.deliveryDate = deliveryDate;
		this.drugManufactureDate = drugManufactureDate;
		this.drugExpiryDate = drugExpiryDate;
		this.drugPrice = drugPrice;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDriverId() {
		return driverId;
	}

	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}

	public Long getDrugOrderId() {
		return drugOrderId;
	}

	public void setDrugOrderId(Long drugOrderId) {
		this.drugOrderId = drugOrderId;
	}

	public LocalDateTime getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(LocalDateTime departureDate) {
		this.departureDate = departureDate;
	}

	public LocalDateTime getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(LocalDateTime deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	
	public LocalDate getDrugManufactureDate() {
		return drugManufactureDate;
	}

	public void setDrugManufactureDate(LocalDate drugManufactureDate) {
		this.drugManufactureDate = drugManufactureDate;
	}

	public LocalDate getDrugExpiryDate() {
		return drugExpiryDate;
	}

	public void setDrugExpiryDate(LocalDate drugExpiryDate) {
		this.drugExpiryDate = drugExpiryDate;
	}

	public BigDecimal getDrugPrice() {
		return drugPrice;
	}

	public void setDrugPrice(BigDecimal drugPrice) {
		this.drugPrice = drugPrice;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(deliveryDate, departureDate, driverId, drugExpiryDate, drugManufactureDate, drugOrderId,
				drugPrice);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof DriverInvoice))
			return false;
		DriverInvoice other = (DriverInvoice) obj;
		return Objects.equals(deliveryDate, other.deliveryDate) && Objects.equals(departureDate, other.departureDate)
				&& Objects.equals(driverId, other.driverId) && Objects.equals(drugExpiryDate, other.drugExpiryDate)
				&& Objects.equals(drugManufactureDate, other.drugManufactureDate)
				&& Objects.equals(drugOrderId, other.drugOrderId) && Objects.equals(drugPrice, other.drugPrice);
	}

	@Override
	public String toString() {
		return "DriverInvoice [id=" + id + ", driverId=" + driverId + ", drugOrderId=" + drugOrderId
				+ ", departureDate=" + departureDate + ", deliveryDate=" + deliveryDate + ", drugManufactureDate="
				+ drugManufactureDate + ", drugExpiryDate=" + drugExpiryDate + ", drugPrice=" + drugPrice + "]";
	}

}
