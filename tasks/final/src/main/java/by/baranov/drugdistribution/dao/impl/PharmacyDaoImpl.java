package by.baranov.drugdistribution.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.connectionpool.ConnectionManager;
import by.baranov.drugdistribution.dao.PharmacyDao;
import by.baranov.drugdistribution.entity.Pharmacy;
import by.baranov.drugdistribution.exception.DaoException;

public class PharmacyDaoImpl implements PharmacyDao{
	private static final String GET_ALL_PHARMACY = "SELECT id, name, licence_number, phone, address_id FROM pharmacy";
	private static final String GET_PHARMACY_BY_ID = "SELECT id, name, licence_number, phone, address_id FROM pharmacy WHERE id=?";
	private static final String SAVE_PHARMACY = "INSERT INTO pharmacy (name, licence_number, phone, address_id) VALUES (?,?,?,?)";
	private static final String UPDATE_PHARMACY = "UPDATE pharmacy SET name=?, licence_number=?, phone=?, address_id=? WHERE id=?";
	private static final String DELETE_PHARMACY = "DELETE FROM pharmacy WHERE id=?";
	
	private static final String GET_PHARMACIES_BY_PHARMACIST_ID = "SELECT pharmacy.id, pharmacy.name, pharmacy.licence_number, pharmacy.phone, pharmacy.address_id "
																+ "FROM pharmacy "
																+ "LEFT JOIN pharmacy_has_pharmacist ON pharmacy_has_pharmacist.pharmacy_id = pharmacy.id "
																+ "LEFT JOIN user_account ON user_account.id = pharmacy_has_pharmacist.pharmacist_id "
																+ "WHERE user_account.id=? ";
	
	private static final Logger LOGGER = LogManager.getLogger();

	private ConnectionManager connectionManager;

	public PharmacyDaoImpl(ConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	@Override
	public List<Pharmacy> getAll() throws DaoException {

		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_PHARMACY);
				ResultSet resultSet = preparedStatement.executeQuery()) {
			List<Pharmacy> result = new ArrayList<>();
			while (resultSet.next()) {
				Pharmacy pharmacy = parsePharmacy(resultSet);
				result.add(pharmacy);
			}
			LOGGER.log(Level.DEBUG, "Result of executing method getAll() : {}", result);
			return result;
		} catch (SQLException exception) {
			throw new DaoException("Error getting all pharmacies", exception);
		}
	}

	private Pharmacy parsePharmacy(ResultSet resultSet) throws SQLException {
		Pharmacy pharmacy = new Pharmacy();
		pharmacy.setId(resultSet.getLong("id"));
		pharmacy.setName(resultSet.getString("name"));
		pharmacy.setLicenceNumber(resultSet.getString("licence_number"));
		pharmacy.setPhone(resultSet.getString("phone"));
		pharmacy.setAddressId(resultSet.getLong("address_id"));
		return pharmacy;
	}

	@Override
	public Pharmacy getById(Long id) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_PHARMACY_BY_ID)) {
			preparedStatement.setLong(1, id);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				resultSet.next();
				Pharmacy result = parsePharmacy(resultSet);
				LOGGER.log(Level.DEBUG, "Result of executing method getById() : {}", result);
				return result;
			}
		} catch (SQLException exception) {
			throw new DaoException("Error getting pharmacy with id=" + id, exception);
		}
	}

	@Override
	public void save(Pharmacy pharmacy) throws DaoException {

		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SAVE_PHARMACY, Statement.RETURN_GENERATED_KEYS)) {
			int i = 0;
			preparedStatement.setString(++i, pharmacy.getName());
			preparedStatement.setString(++i, pharmacy.getLicenceNumber());
			preparedStatement.setString(++i, pharmacy.getPhone());
			preparedStatement.setLong(++i, pharmacy.getAddressId());
			preparedStatement.executeUpdate();

			try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
				while (generatedKeys.next()) {
					pharmacy.setId(generatedKeys.getLong(1));
				}
				LOGGER.log(Level.DEBUG, "Result of executing method save() is success");
			}
		} catch (SQLException exception) {
			throw new DaoException("Error saving pharmacy=" + pharmacy, exception);
		}
	}
	
	@Override
	public void update(Pharmacy pharmacy) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_PHARMACY)) {
			int i = 0;
			preparedStatement.setString(++i, pharmacy.getName());
			preparedStatement.setString(++i, pharmacy.getLicenceNumber());
			preparedStatement.setString(++i, pharmacy.getPhone());
			preparedStatement.setLong(++i, pharmacy.getAddressId());
			preparedStatement.setLong(++i, pharmacy.getId());
			preparedStatement.executeUpdate();
			LOGGER.log(Level.DEBUG, "Result of executing method update() is success");
		} catch (SQLException exception) {
			throw new DaoException("Error updating pharmacy=" + pharmacy, exception);
		}
	}

	@Override
	public void delete(Pharmacy pharmacy) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(DELETE_PHARMACY)) {
			preparedStatement.setLong(1, pharmacy.getId());
			preparedStatement.executeUpdate();
			LOGGER.log(Level.DEBUG, "Result of executing method delete() is success");
		} catch (SQLException exception) {
			throw new DaoException("Error deleting pharmacy=" + pharmacy, exception);
		}
	}

	@Override
	public List<Pharmacy> getPharmaciesByPharmacistId(Long pharmacistId) throws DaoException {
		
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_PHARMACIES_BY_PHARMACIST_ID)) {
			preparedStatement.setLong(1, pharmacistId);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				List<Pharmacy> result = new ArrayList<>();
				while(resultSet.next()) {
					Pharmacy pharmacy = parsePharmacy(resultSet);
					result.add(pharmacy);
				}
				LOGGER.log(Level.DEBUG, "Result of executing method getPharmaciesByPharmacistId() : {}", result);
				return result;
			}
		} catch (SQLException exception) {
			throw new DaoException("Error getting pharmacies for pharmacist with id=" + pharmacistId, exception);
		}
	}

}
