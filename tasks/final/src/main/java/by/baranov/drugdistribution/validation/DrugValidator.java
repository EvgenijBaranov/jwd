package by.baranov.drugdistribution.validation;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DrugValidator implements Validator {
	private static final Logger LOGGER = LogManager.getLogger();
	private ValidationResult validationResult = new ValidationResult();

	@Override
	public ValidationResult validate(String... inputedFieldsData) {

		List<String> listInputedFieldsData = Arrays.asList(inputedFieldsData);
		LOGGER.log(Level.DEBUG, "DrugValidator inputed data = {}", listInputedFieldsData);

		checkEmptyFields(listInputedFieldsData);
		checkDrugDosage(listInputedFieldsData);
		LOGGER.log(Level.DEBUG, "validationResult={}", validationResult);
		return validationResult;
	}

	private void checkEmptyFields(List<String> listInputedFieldsData) {
		for (int fieldNumber = 0; fieldNumber < listInputedFieldsData.size(); fieldNumber++) {
			if (listInputedFieldsData.get(fieldNumber).isEmpty()) {
				validationResult.addMessage(new ValidationMessage(
						"error.drug.validator.field.number." + (fieldNumber + 1), "error.validator.field.value.empty"));
			}
		}
	}

	private void checkDrugDosage(List<String> listInputedFieldsData) {
		try {
			double drugDosage = Double.parseDouble(listInputedFieldsData.get(1));
			if (drugDosage <= 0) {
				validationResult.addMessage(new ValidationMessage("error.drug.validator.field.number.2",
																	"error.validator.field.value.positive.number"));
			}
		} catch (NumberFormatException exception) {
			validationResult.addMessage(new ValidationMessage("error.drug.validator.field.number.2",
																"error.validator.field.value.positive.number"));
		}
	}
}
