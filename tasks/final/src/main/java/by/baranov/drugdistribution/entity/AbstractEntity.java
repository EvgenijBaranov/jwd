package by.baranov.drugdistribution.entity;

public interface AbstractEntity {
	
	Long getId();
	
	void setId(Long id);

}
