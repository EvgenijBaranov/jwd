package by.baranov.drugdistribution.command.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.UserService;

public class ViewChangeUserFormCommand implements Command{
	private static final String ATTRIBUTE_NAME_FOR_USER = "user";
	private static final String ATTRIBUTE_VALUE_FOR_VIEW = "changeUserForm";
	private static final Logger LOGGER = LogManager.getLogger();

	private final UserService userService;

	public ViewChangeUserFormCommand(UserService userService) {
		this.userService = userService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String inputedUserId = request.getParameter(AppConstants.PARAMETER_USER_ID).trim();
		try {
			Long userId = Long.valueOf(inputedUserId);
			User user = userService.getById(userId);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_USER, user);
		} catch (ServiceException exception) {
			request.setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
			LOGGER.log(Level.ERROR, "Error: can't view change user form", exception);
		} 
		request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_VIEW, ATTRIBUTE_VALUE_FOR_VIEW);
		LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
				getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_USER_COMMANDS);
		return TypeCommandAction.FORWARD + AppConstants.FILE_PATH_FOR_USER_COMMANDS;
	}
}
