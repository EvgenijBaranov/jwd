package by.baranov.drugdistribution.entity;

public enum UserRole implements AbstractEntity{
	ADMIN (1L),
	PHARMACIST(2L),
	DRIVER(3L),
	DRUG_PROVIDER(4L);
	
	private Long id;

	private UserRole(Long id) {
		this.id = id;
	}
	
	public static UserRole getUserRoleByRoleId(Long id) {
		UserRole[] userRoles = UserRole.values();
		for(UserRole userRole : userRoles) {
			if(userRole.getId().equals(id)) {
				return userRole;
			}
		}
		return null;
	}
	
	public static UserRole getUserRoleFromString(String role) {
		UserRole[] userRoles = UserRole.values();
		for(UserRole userRole : userRoles) {
			if(userRole.name().equalsIgnoreCase(role)) {
				return userRole;
			}
		}
		return null;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return toString();
	}
	
}
