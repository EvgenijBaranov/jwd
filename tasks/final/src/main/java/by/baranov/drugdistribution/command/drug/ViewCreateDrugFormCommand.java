package by.baranov.drugdistribution.command.drug;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;

public class ViewCreateDrugFormCommand implements Command{
	private static final String ATTRIBUTE_VALUE_FOR_VIEW = "createDrugForm";

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_VIEW, ATTRIBUTE_VALUE_FOR_VIEW);
		return TypeCommandAction.FORWARD + AppConstants.FILE_PATH_FOR_DRUG_COMMANDS;
	}
}
