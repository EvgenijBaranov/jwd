package by.baranov.drugdistribution.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import by.baranov.drugdistribution.ApplicationContext;
import by.baranov.drugdistribution.SecurityContext;

public class ApplicationContextListener implements ServletContextListener{
	
	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		ApplicationContext.getInstance().initialize();
		
		SecurityContext securityContext = SecurityContext.getInstance();
		securityContext.initialize();
		servletContextEvent.getServletContext().setAttribute("securityContext", securityContext);
		
	}

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		ApplicationContext.getInstance().destroy();
	}

}
