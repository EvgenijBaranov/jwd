package by.baranov.drugdistribution.connectionpool;

import java.sql.Connection;

public interface TransactionManager {
	
	void beginTransaction();
	
	void commitTransaction();
	
	void rollbackTransaction();
	
	Connection getConnection();
	
}
