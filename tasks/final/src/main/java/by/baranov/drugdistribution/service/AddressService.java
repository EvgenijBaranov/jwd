package by.baranov.drugdistribution.service;

import by.baranov.drugdistribution.entity.Address;
import by.baranov.drugdistribution.exception.ServiceException;

public interface AddressService{
	
	Address getById(Long id) throws ServiceException;	

}
