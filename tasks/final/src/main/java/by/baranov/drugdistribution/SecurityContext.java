package by.baranov.drugdistribution;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.command.CommandName;
import by.baranov.drugdistribution.entity.UserRole;
import by.baranov.drugdistribution.exception.SecurityPropertyFileException;

public class SecurityContext {
	private static final String SECURITY_PROPERTY_FILE_NAME = "security.properties";
	private static final Logger LOGGER = LogManager.getLogger();
	private static SecurityContext securityContext = null;
	private static Lock lock = new ReentrantLock();
	private Properties securityProperties = new Properties();
	private final Map<String, List<UserRole>> mapUserRolesForCurrentSessionId = new ConcurrentHashMap<>(1000);
	private final ThreadLocal<String> storageForCurrentSessionId = new ThreadLocal<>();

	private SecurityContext() {
	}

	public static SecurityContext getInstance() {
		if (securityContext == null) {
			lock.lock();
			try {
				if (securityContext == null) {
					securityContext = new SecurityContext();
				}
			} finally {
				lock.unlock();
			}
		}
		return securityContext;
	}

	public void initialize() {
		try (InputStream inputStreamFromPropertyFile = getClass().getClassLoader().getResourceAsStream(SECURITY_PROPERTY_FILE_NAME)) {
			securityProperties.load(inputStreamFromPropertyFile);
			LOGGER.log(Level.INFO, "Property file of SecurityContext was loaded");
		} catch (IOException exception) {
			LOGGER.log(Level.FATAL, "Can't load security property file: " + SECURITY_PROPERTY_FILE_NAME, exception);
			throw new SecurityPropertyFileException(
					"The file " + SECURITY_PROPERTY_FILE_NAME + " wasn't found or can't be read", exception);
		}
		LOGGER.log(Level.INFO, "SecurityContext was initialized");
	}

	public String getCurrentSessionId() {
		return storageForCurrentSessionId.get();
	}

	public void setCurrentSessionId(String sessionId) {
		storageForCurrentSessionId.set(sessionId);
		LOGGER.log(Level.DEBUG, "Session with id={} was set to threadLocal", sessionId );
	}

	public List<UserRole> getUserRolesForCurrentSession() {
		String currentSessionId = getCurrentSessionId();
		return mapUserRolesForCurrentSessionId.get(currentSessionId);
	}

	public boolean canExecute(CommandName commandName) {
		List<UserRole> userRolesForCurrentSession = getUserRolesForCurrentSession();

		String userRoleNamesForCommandFromPropertyFile = securityProperties.getProperty("command." + commandName.name());
		List<String> listUserRoleNamesFromPropertyFile = Optional.ofNullable(userRoleNamesForCommandFromPropertyFile)
				.map(stringUserRoleNamesFromPropetyFile -> Arrays.asList(stringUserRoleNamesFromPropetyFile.split(", ")))
				.orElseGet(ArrayList::new);
		boolean isCurrentUserCanExecuteCommand =listUserRoleNamesFromPropertyFile.stream()
										.anyMatch(userRoleNameFromPropertyFile -> userRolesForCurrentSession.stream()
												.anyMatch(userRoleFromCurrentSession -> userRoleFromCurrentSession.name().equalsIgnoreCase(userRoleNameFromPropertyFile)));
		LOGGER.log(Level.INFO, "Current user with roles = {} can execute command {} - {}", 
												userRolesForCurrentSession, commandName, isCurrentUserCanExecuteCommand);
		return isCurrentUserCanExecuteCommand;
	}
	public void addCurrentUserRolesToSession(String sessionId, List<UserRole> userRoles) {
		mapUserRolesForCurrentSessionId.put(sessionId, userRoles);
		LOGGER.log(Level.INFO, "User roles = {} and session id = {} was added to SecurityContext ", userRoles, sessionId);
	}
	
	public void deleteCurrentUserRolesFromSession(String sessionId) {
		List<UserRole> userRoles = mapUserRolesForCurrentSessionId.remove(sessionId);
		LOGGER.log(Level.INFO, "User roles {} for session id = {} was deleted from SecurityContext ", userRoles, sessionId);
	}
	
}
