package by.baranov.drugdistribution.command.drugform;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.DrugForm;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DrugFormService;

public class DeleteDrugFormForDrugCommand implements Command{
	private static final String ATTRIBUTE_NAME_FOR_DRUG_FORMS = "drugFormsForDrug";
	private static final Logger LOGGER = LogManager.getLogger();
	private final DrugFormService drugFormService;

	public DeleteDrugFormForDrugCommand(DrugFormService drugFormService) {
		this.drugFormService = drugFormService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String inputedDrugId = request.getParameter(AppConstants.PARAMETER_DRUG_ID).trim();
		String inputedDrugFormId = request.getParameter(AppConstants.PARAMETER_DRUG_FORM_ID).trim();
		try {
			Long drugId = Long.valueOf(inputedDrugId);
			
			Long drugFormId = Long.valueOf(inputedDrugFormId);
			drugFormService.deleteDrugFormForDrug(drugId, drugFormId);
			
			List<DrugForm> drugForms = drugFormService.getDrugFormsByDrugId(drugId);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_DRUG_FORMS, drugForms);
		} catch (ServiceException exception) {
			request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
			LOGGER.log(Level.ERROR, "Error of deleting drug form with id=" + inputedDrugFormId + " for drug with id=" + inputedDrugId, exception);
		} 
		LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
				getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_DRUG_COMMANDS);
		return TypeCommandAction.REDIRECT + AppConstants.FILE_PATH_FOR_DRUG_COMMANDS;
	}
}
