package by.baranov.drugdistribution.entity;

import java.io.Serializable;
import java.util.Objects;

public class Pharmacy implements AbstractEntity, Serializable {
	
	private static final long serialVersionUID = -1086466888897089605L;
	private Long id;
	private String name;
	private String licenceNumber;
	private String phone;
	private Long addressId;

	public Pharmacy() {
	}
	
	public Pharmacy(Long id, String name, String licenceNumber, String phone, Long addressId) {
		this.id = id;
		this.name = name;
		this.licenceNumber = licenceNumber;
		this.phone = phone;
		this.addressId = addressId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLicenceNumber() {
		return licenceNumber;
	}

	public void setLicenceNumber(String licenceNumber) {
		this.licenceNumber = licenceNumber;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getAddressId() {
		return addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}


	@Override
	public int hashCode() {
		return Objects.hash(addressId, licenceNumber, name, phone);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Pharmacy))
			return false;
		Pharmacy other = (Pharmacy) obj;
		return Objects.equals(addressId, other.addressId) && Objects.equals(licenceNumber.toLowerCase(), other.licenceNumber.toLowerCase())
				&& Objects.equals(name.toLowerCase(), other.name.toLowerCase()) && Objects.equals(phone.toLowerCase(), other.phone.toLowerCase());
	}

	@Override
	public String toString() {
		return "Pharmacy [name=" + name + ", licenceNumber=" + licenceNumber + ", phone=" + phone + ", addressId="
				+ addressId + "]";
	}

}
