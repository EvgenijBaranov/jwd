package by.baranov.drugdistribution.service.impl;

import java.util.List;

import by.baranov.drugdistribution.connectionpool.Transactional;
import by.baranov.drugdistribution.dao.UserRoleDao;
import by.baranov.drugdistribution.entity.UserRole;
import by.baranov.drugdistribution.exception.DaoException;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.UserRoleService;

public class UserRoleServiceImpl implements UserRoleService {
	private final UserRoleDao userRoleDao;

	public UserRoleServiceImpl(UserRoleDao userRoleDao) {
		this.userRoleDao = userRoleDao;
	}

	@Override
	public List<UserRole> getUserRolesByUserId(Long userId) throws ServiceException {
		try {
			return userRoleDao.getUserRolesByUserId(userId);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	@Transactional
	@Override
	public void assignRoleForUser(Long userId, Long userRoleId) throws ServiceException {
		try {
			List<UserRole> userRoles = userRoleDao.getUserRolesByUserId(userId);
			UserRole userRole = UserRole.getUserRoleByRoleId(userRoleId);
			boolean isRoleExistForUser = userRoles.stream().anyMatch(someUserRole -> someUserRole == userRole);
			if (isRoleExistForUser) {
				throw new ServiceException("error.user.role.service.add.user.role.message");
			}
			userRoleDao.assignRole(userId, userRole.getId());
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	@Transactional
	@Override
	public void deleteUserRoleForUser(Long userId, Long userRoleId) throws ServiceException {
		try {
			userRoleDao.deleteUserRoleForUser(userId, userRoleId);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}
}
