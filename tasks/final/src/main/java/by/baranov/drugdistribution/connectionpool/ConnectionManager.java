package by.baranov.drugdistribution.connectionpool;

import java.sql.Connection;

public interface ConnectionManager {

	Connection getConnection() ;
}
