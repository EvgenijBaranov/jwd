package by.baranov.drugdistribution.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.SecurityContext;

@WebFilter("/*")
public class CommandSecurityFilter implements Filter {
	private static final Logger LOGGER = LogManager.getLogger();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException { }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest servletRequest = (HttpServletRequest) request;
        String sessionId = servletRequest.getSession().getId();
        LOGGER.log(Level.DEBUG, "From CommandSecurityFilter session id = {}" , sessionId);
        SecurityContext.getInstance().setCurrentSessionId(sessionId);
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() { }
}
