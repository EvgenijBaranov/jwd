package by.baranov.drugdistribution.entity;

import java.io.Serializable;
import java.util.Objects;

public class DrugForm implements AbstractEntity, Serializable {
	private static final long serialVersionUID = -5204126237301507547L;
	private Long id;
	private String name;

	public DrugForm() {
	}

	public DrugForm(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof DrugForm))
			return false;
		DrugForm other = (DrugForm) obj;
		return Objects.equals(name.toLowerCase(), other.name.toLowerCase());
	}

	@Override
	public String toString() {
		return "DrugForm [name=" + name + "]";
	}

}
