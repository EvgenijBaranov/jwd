package by.baranov.drugdistribution.validation;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UserValidator implements Validator {
	private static final Logger LOGGER = LogManager.getLogger();
	private ValidationResult validationResult = new ValidationResult();

	@Override
	public ValidationResult validate(String... inputedFieldsData) {
		List<String> listInputedFieldsData = Arrays.asList(inputedFieldsData);
		LOGGER.log(Level.DEBUG, "UserValidator inputed data = {}", listInputedFieldsData);

		checkEmptyFields(listInputedFieldsData);
		LOGGER.log(Level.DEBUG, "validationResult={}", validationResult);
		return validationResult;
	}

	private void checkEmptyFields(List<String> listInputedFieldsData) {
		for (int fieldNumber = 0; fieldNumber < listInputedFieldsData.size(); fieldNumber++) {
			if (listInputedFieldsData.get(fieldNumber).isEmpty()) {
				validationResult.addMessage(new ValidationMessage(
						"error.user.validator.field.number." + (fieldNumber + 1), "error.validator.field.value.empty"));
			}
		}
	}
}
