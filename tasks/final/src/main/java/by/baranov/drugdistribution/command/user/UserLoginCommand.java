package by.baranov.drugdistribution.command.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.SecurityContext;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.entity.UserRole;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.UserRoleService;
import by.baranov.drugdistribution.service.UserService;

public class UserLoginCommand implements Command {

	private static final String RESULT_FILE_PATH = "/jsp/mainPage.jsp";
	private static final String ERROR_FILE_PATH = "/jsp/userLoginPage.jsp";
	private static final Logger LOGGER = LogManager.getLogger();
	private final UserService userService;
	private final UserRoleService userRoleService;

	public UserLoginCommand(UserService userService, UserRoleService userRoleService) {
		this.userService = userService;
		this.userRoleService = userRoleService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String resultFilePathFromCommand = ERROR_FILE_PATH;
		try {
			String inputedLogin = request.getParameter(AppConstants.PARAMETER_USER_LOGIN).trim();
			String inputedPassword = request.getParameter(AppConstants.PARAMETER_USER_PASSWORD).trim();
			
			boolean isLoginedUser = userService.loginUser(inputedLogin, inputedPassword);
			if(isLoginedUser) {
				User user = userService.getUserByLogin(inputedLogin);
				List<UserRole> userRoles = userRoleService.getUserRolesByUserId(user.getId());
				
				SecurityContext.getInstance().addCurrentUserRolesToSession(request.getSession().getId(), userRoles);
				
				request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_REGISTERED_USER, user);
				resultFilePathFromCommand = RESULT_FILE_PATH;
			} else {
				request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, "error.user.service.user.login.message");
				LOGGER.log(Level.ERROR, "User Login error. Invalid login or password");
			}
		} catch (ServiceException exception) {
			request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
			LOGGER.log(Level.ERROR, "User Login error", exception);
		}
		LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
				getClass().getSimpleName(), resultFilePathFromCommand);
		
		return TypeCommandAction.REDIRECT + resultFilePathFromCommand;
	}

}
