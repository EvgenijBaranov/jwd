package by.baranov.drugdistribution.validation;

import java.io.Serializable;

public class ValidationMessage implements Serializable{
	
	private static final long serialVersionUID = -6547633811077415301L;
	private String field;
	private String errorMessage;
	
	public ValidationMessage(String field, String errorMessage) {
		this.field = field;
		this.errorMessage = errorMessage;
	}
	public String getField() {
		return field;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	@Override
	public String toString() {
		return field + " " + errorMessage;
	}
	
	
}
