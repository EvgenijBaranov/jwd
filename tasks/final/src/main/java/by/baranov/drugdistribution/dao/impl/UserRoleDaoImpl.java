package by.baranov.drugdistribution.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.connectionpool.ConnectionManager;
import by.baranov.drugdistribution.dao.UserRoleDao;
import by.baranov.drugdistribution.entity.UserRole;
import by.baranov.drugdistribution.exception.DaoException;

public class UserRoleDaoImpl implements UserRoleDao{
	
	private static final String GET_ALL_USER_ROLE = "SELECT id, name FROM user_role";
	private static final String GET_USER_ROLE_BY_ID = "SELECT id, name FROM user_role WHERE id=?";
	private static final String SAVE_USER_ROLE = "INSERT INTO user_role (name) VALUES (?)";
	private static final String UPDATE_USER_ROLE = "UPDATE user_role SET name=? WHERE id=?";
	private static final String DELETE_USER_ROLE = "DELETE FROM user_role WHERE id=?";
	
	private static final String ASSIGN_USER_ROLE = "INSERT INTO user_role_relation (user_id, user_role_id) VALUES (?,?)";
	private static final String DELETE_USER_ROLE_FROM_USER = "DELETE FROM user_role_relation WHERE user_id=? AND user_role_id=?";
	private static final String GET_USER_ROLES_BY_USER_ID = "SELECT user_role.id, user_role.name "
													+ "FROM user_role "
													+ "LEFT JOIN user_role_relation ON user_role.id = user_role_relation.user_role_id "
													+ "LEFT JOIN user_account ON user_role_relation.user_id = user_account.id "
													+ "WHERE user_account.id=?";
	
	private static final Logger LOGGER = LogManager.getLogger();
	private ConnectionManager connectionManager;

	public UserRoleDaoImpl(ConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	@Override
	public List<UserRole> getAll() throws DaoException {

		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_USER_ROLE);
				ResultSet resultSet = preparedStatement.executeQuery()) {
			List<UserRole> result = new ArrayList<>();
			while (resultSet.next()) {
				UserRole userRole = parseUserRole(resultSet);
				result.add(userRole);
			}
			LOGGER.log(Level.DEBUG, "Result of executing method getAll() : {}", result);
			return result;
		} catch (SQLException exception) {
			throw new DaoException("Error getting all user roles", exception);
		}
	}

	private UserRole parseUserRole(ResultSet resultSet) throws SQLException {
		String userRoleName = resultSet.getString("name");
		LOGGER.log(Level.DEBUG, "userRoleName={}", userRoleName);
		return UserRole.getUserRoleFromString(userRoleName);
	}

	@Override
	public UserRole getById(Long id) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_USER_ROLE_BY_ID)) {
			preparedStatement.setLong(1, id);
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				resultSet.next();
				UserRole result = parseUserRole(resultSet);
				LOGGER.log(Level.DEBUG, "Result of executing method getById() : {}", result);
				return result;
			}
		} catch (SQLException exception) {
			throw new DaoException("Error getting user role with id=" + id, exception);
		}
	}

	@Override
	public void save(UserRole userRole) throws DaoException {

		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SAVE_USER_ROLE, Statement.RETURN_GENERATED_KEYS)) {
			int i = 0;
			preparedStatement.setString(++i, userRole.name().toLowerCase());
			preparedStatement.executeUpdate();

			try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
				while (generatedKeys.next()) {
					userRole.setId(generatedKeys.getLong(1));
				}
			}
			LOGGER.log(Level.DEBUG, "Result of executing method save() is success");
		} catch (SQLException exception) {
			throw new DaoException("Error saving user role=" + userRole + ". The user role already exist.", exception);
		}
	}

	@Override
	public void update(UserRole userRole) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_ROLE)) {
			int i = 0;
			preparedStatement.setString(++i, userRole.name().toLowerCase());
			preparedStatement.setLong(++i, userRole.getId());
			preparedStatement.executeUpdate();
			LOGGER.log(Level.DEBUG, "Result of executing method update() is success");
		} catch (SQLException exception) {
			throw new DaoException("Error updating user role=" + userRole, exception);
		}
	}

	@Override
	public void delete(UserRole userRole) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(DELETE_USER_ROLE)) {
			preparedStatement.setLong(1, userRole.getId());
			preparedStatement.executeUpdate();
			LOGGER.log(Level.DEBUG, "Result of executing method delete() is success");
		} catch (SQLException exception) {
			throw new DaoException("Error deleting user role=" + userRole, exception);
		}
	}

	
	@Override
	public void assignRole(Long userId, Long userRoleId) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(ASSIGN_USER_ROLE)) {
			int i = 0;
			preparedStatement.setLong(++i, userId);
			preparedStatement.setLong(++i, userRoleId);
			preparedStatement.executeUpdate();
			LOGGER.log(Level.DEBUG, "Result of executing method assignRole({}, {}) is success", userId, userRoleId);
		} catch (SQLException exception) {
			throw new DaoException("The user with id=" + userId + " already has role with id=" + userRoleId, exception);
		}
	}

	@Override
	public List<UserRole> getUserRolesByUserId(Long userId) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_USER_ROLES_BY_USER_ID)) {
			preparedStatement.setLong(1, userId);
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				List<UserRole> result = new ArrayList<>();
				while(resultSet.next()) {
					UserRole userRole = parseUserRole(resultSet);
					result.add(userRole);
				}
				LOGGER.log(Level.DEBUG, "Result of executing method getUserRolesByUserId() : {}", result);
				return result;
			}
		} catch (SQLException exception) {
			throw new DaoException("Error getting user roles by user id=" + userId, exception);
		}
	}


	@Override
	public void deleteUserRoleForUser(Long userId, Long userRoleId) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(DELETE_USER_ROLE_FROM_USER)) {
			int i=0;
			preparedStatement.setLong(++i, userId);
			preparedStatement.setLong(++i, userRoleId);
			preparedStatement.executeUpdate();
			LOGGER.log(Level.DEBUG, "Result of executing method deleteUserRoleForUser() is success");
		} catch (SQLException exception) {
			throw new DaoException("User with id=" + userId + " hasn't user role with id=" + userRoleId, exception);
		}
	}
}
