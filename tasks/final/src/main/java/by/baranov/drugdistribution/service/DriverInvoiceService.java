package by.baranov.drugdistribution.service;

import java.util.List;

import by.baranov.drugdistribution.entity.DriverInvoice;
import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.exception.ServiceException;

public interface DriverInvoiceService{
	
	List<DriverInvoice> getAvailableInvoicesForDriver(User driver) throws ServiceException;
	
	void delete (Long id) throws ServiceException;
	
	void acceptDriverInvoiceByDriver(Long driverInvoiceId, User driver) throws ServiceException;
	
}
