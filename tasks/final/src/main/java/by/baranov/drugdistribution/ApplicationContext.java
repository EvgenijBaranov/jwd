package by.baranov.drugdistribution;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.CommandName;
import by.baranov.drugdistribution.command.CommandRegistry;
import by.baranov.drugdistribution.command.CommandRegistryImpl;
import by.baranov.drugdistribution.command.DefaultCommand;
import by.baranov.drugdistribution.command.driverinvoice.AcceptDriverInvoiceCommand;
import by.baranov.drugdistribution.command.driverinvoice.DeleteDriverInvoiceCommand;
import by.baranov.drugdistribution.command.driverinvoice.ViewAllDriverInvoicesCommand;
import by.baranov.drugdistribution.command.drug.ChangeDrugCommand;
import by.baranov.drugdistribution.command.drug.CreateDrugCommand;
import by.baranov.drugdistribution.command.drug.DeleteDrugCommand;
import by.baranov.drugdistribution.command.drug.ViewAllDrugsCommand;
import by.baranov.drugdistribution.command.drug.ViewAllDrugsForPharmacyCommand;
import by.baranov.drugdistribution.command.drug.ViewChangeDrugFormCommand;
import by.baranov.drugdistribution.command.drug.ViewCreateDrugFormCommand;
import by.baranov.drugdistribution.command.drugform.AssignDrugFormForDrugCommand;
import by.baranov.drugdistribution.command.drugform.DeleteDrugFormForDrugCommand;
import by.baranov.drugdistribution.command.drugform.ViewAllDrugFormsForDrugCommand;
import by.baranov.drugdistribution.command.drugorder.ChangeDrugQuantityForDrugOrderCommand;
import by.baranov.drugdistribution.command.drugorder.ConfirmDrugOrderDeliveryCommand;
import by.baranov.drugdistribution.command.drugorder.CreateDrugOrderCommand;
import by.baranov.drugdistribution.command.drugorder.DeleteDrugOrderCommand;
import by.baranov.drugdistribution.command.drugorder.ViewAllDrugOrdersCommand;
import by.baranov.drugdistribution.command.drugorder.ViewChangeDrugQuantityFormCommand;
import by.baranov.drugdistribution.command.drugorder.ViewCreateDrugOrderFormCommand;
import by.baranov.drugdistribution.command.drugprovider.DistributeDrugProviderDrugsCommand;
import by.baranov.drugdistribution.command.drugprovider.ViewDrugProviderFormCommand;
import by.baranov.drugdistribution.command.pharmacy.AssignPharmacistForPharmacyCommand;
import by.baranov.drugdistribution.command.pharmacy.ChangePharmacyCommand;
import by.baranov.drugdistribution.command.pharmacy.CreatePharmacyCommand;
import by.baranov.drugdistribution.command.pharmacy.DeletePharmacistForPharmacyCommand;
import by.baranov.drugdistribution.command.pharmacy.DeletePharmacyCommand;
import by.baranov.drugdistribution.command.pharmacy.ViewAllPharmaciesCommand;
import by.baranov.drugdistribution.command.pharmacy.ViewAllPharmacistForPharmacyCommand;
import by.baranov.drugdistribution.command.pharmacy.ViewChangePharmacyFormCommand;
import by.baranov.drugdistribution.command.pharmacy.ViewCreatePharmacyFormCommand;
import by.baranov.drugdistribution.command.user.ChangeUserCommand;
import by.baranov.drugdistribution.command.user.DeleteUserCommand;
import by.baranov.drugdistribution.command.user.UserLoginCommand;
import by.baranov.drugdistribution.command.user.UserLogoutCommand;
import by.baranov.drugdistribution.command.user.UserRegistrationCommand;
import by.baranov.drugdistribution.command.user.ViewAllUsersCommand;
import by.baranov.drugdistribution.command.user.ViewChangeUserFormCommand;
import by.baranov.drugdistribution.command.user.ViewUserRegistrationFormCommand;
import by.baranov.drugdistribution.command.userrole.AssignRoleForUserCommand;
import by.baranov.drugdistribution.command.userrole.DeleteUserRoleFromUserCommand;
import by.baranov.drugdistribution.command.userrole.ViewUserRolesForUserCommand;
import by.baranov.drugdistribution.connectionpool.ConnectionManager;
import by.baranov.drugdistribution.connectionpool.ConnectionManagerImpl;
import by.baranov.drugdistribution.connectionpool.ConnectionPool;
import by.baranov.drugdistribution.connectionpool.ConnectionPoolImpl;
import by.baranov.drugdistribution.connectionpool.TransactionManager;
import by.baranov.drugdistribution.connectionpool.TransactionManagerImpl;
import by.baranov.drugdistribution.connectionpool.Transactional;
import by.baranov.drugdistribution.dao.AddressDao;
import by.baranov.drugdistribution.dao.DriverInvoiceDao;
import by.baranov.drugdistribution.dao.DrugDao;
import by.baranov.drugdistribution.dao.DrugFormDao;
import by.baranov.drugdistribution.dao.DrugOrderDao;
import by.baranov.drugdistribution.dao.DrugProducerDao;
import by.baranov.drugdistribution.dao.PharmacyDao;
import by.baranov.drugdistribution.dao.UserDao;
import by.baranov.drugdistribution.dao.UserRoleDao;
import by.baranov.drugdistribution.dao.impl.AddressDaoImpl;
import by.baranov.drugdistribution.dao.impl.DriverInvoiceDaoImpl;
import by.baranov.drugdistribution.dao.impl.DrugDaoImpl;
import by.baranov.drugdistribution.dao.impl.DrugFormDaoImpl;
import by.baranov.drugdistribution.dao.impl.DrugOrderDaoImpl;
import by.baranov.drugdistribution.dao.impl.DrugProducerDaoImpl;
import by.baranov.drugdistribution.dao.impl.PharmacyDaoImpl;
import by.baranov.drugdistribution.dao.impl.UserDaoImpl;
import by.baranov.drugdistribution.dao.impl.UserRoleDaoImpl;
import by.baranov.drugdistribution.exception.ConnectionException;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.AddressService;
import by.baranov.drugdistribution.service.DriverInvoiceService;
import by.baranov.drugdistribution.service.DrugFormService;
import by.baranov.drugdistribution.service.DrugOrderService;
import by.baranov.drugdistribution.service.DrugProducerService;
import by.baranov.drugdistribution.service.DrugService;
import by.baranov.drugdistribution.service.PharmacyService;
import by.baranov.drugdistribution.service.UserRoleService;
import by.baranov.drugdistribution.service.UserService;
import by.baranov.drugdistribution.service.impl.AddressServiceImpl;
import by.baranov.drugdistribution.service.impl.DriverInvoiceServiceImpl;
import by.baranov.drugdistribution.service.impl.DrugFormServiceImpl;
import by.baranov.drugdistribution.service.impl.DrugOrderServiceImpl;
import by.baranov.drugdistribution.service.impl.DrugProducerServiceImpl;
import by.baranov.drugdistribution.service.impl.DrugServiceImpl;
import by.baranov.drugdistribution.service.impl.PharmacyServiceImpl;
import by.baranov.drugdistribution.service.impl.UserRoleServiceImpl;
import by.baranov.drugdistribution.service.impl.UserServiceImpl;

public class ApplicationContext {
	private static final Logger LOGGER = LogManager.getLogger();
	private static final Lock lock = new ReentrantLock();
	private static final AtomicBoolean instanceNotCreated = new AtomicBoolean(true);
	private static ApplicationContext applicationContext = null;
	private final Map<Class<?>, Object> beens = new HashMap<>();
	
	private ApplicationContext() {
	}

	public static ApplicationContext getInstance() {
		if (instanceNotCreated.get()) {
			lock.lock();
			try {
				if (instanceNotCreated.get()) {
					applicationContext = new ApplicationContext();
					instanceNotCreated.set(false);
				}
			} finally {
				lock.unlock();
			}
		}
		return applicationContext;
	}

	public void initialize() {
		ConnectionPool connectionPool = ConnectionPoolImpl.getInstance();
		TransactionManager transactionManager = new TransactionManagerImpl(connectionPool);
		ConnectionManager connectionManager = new ConnectionManagerImpl(connectionPool, transactionManager);

		
		AddressDao addressDao = new AddressDaoImpl(connectionManager);
		DriverInvoiceDao driverInvoiceDao = new DriverInvoiceDaoImpl(connectionManager);
		DrugDao drugDao = new DrugDaoImpl(connectionManager);
		DrugFormDao drugFormDao = new DrugFormDaoImpl(connectionManager);
		DrugOrderDao drugOrderDao = new DrugOrderDaoImpl(connectionManager);
		DrugProducerDao drugProducerDao = new DrugProducerDaoImpl(connectionManager);
		PharmacyDao pharmacyDao = new PharmacyDaoImpl(connectionManager);
		UserDao userDao = new UserDaoImpl(connectionManager);
		UserRoleDao userRoleDao = new UserRoleDaoImpl(connectionManager);

		
		UserService userService = new UserServiceImpl(userDao);
		UserRoleService userRoleService = new UserRoleServiceImpl(userRoleDao);
		PharmacyService pharmacyService = new PharmacyServiceImpl(pharmacyDao, addressDao);
		DrugProducerService drugProducerService = new DrugProducerServiceImpl(drugProducerDao);
		DrugFormService drugFormService = new DrugFormServiceImpl(drugFormDao);
		DrugService drugService = new DrugServiceImpl(drugDao, addressDao, drugFormDao, drugProducerDao, drugOrderDao, driverInvoiceDao);
		AddressService addressService = new AddressServiceImpl(addressDao);
		DrugOrderService drugOrderService = new DrugOrderServiceImpl(drugOrderDao, pharmacyDao, driverInvoiceDao, drugDao);
		DriverInvoiceService driverInvoiceService = new DriverInvoiceServiceImpl(driverInvoiceDao, drugOrderDao);

		
		
		InvocationHandler userHandler = createTransactionalInvocationHandler(transactionManager, userService);
		UserService proxyUserService = createProxy(getClass().getClassLoader(), userHandler, UserService.class);

		InvocationHandler userRoleHandler = createTransactionalInvocationHandler(transactionManager, userRoleService);
		UserRoleService proxyUserRoleService = createProxy(getClass().getClassLoader(), userRoleHandler, UserRoleService.class);

		InvocationHandler pharmacyHandler = createTransactionalInvocationHandler(transactionManager, pharmacyService);
		PharmacyService proxyPharmacyService = createProxy(getClass().getClassLoader(), pharmacyHandler, PharmacyService.class);
		
		InvocationHandler drugProducerHandler = createTransactionalInvocationHandler(transactionManager, drugProducerService);
		DrugProducerService proxyDrugProducerService = createProxy(getClass().getClassLoader(), drugProducerHandler, DrugProducerService.class);
		
		InvocationHandler drugFormHandler = createTransactionalInvocationHandler(transactionManager, drugFormService);
		DrugFormService proxyDrugFormService = createProxy(getClass().getClassLoader(), drugFormHandler, DrugFormService.class);
		
		InvocationHandler drugHandler = createTransactionalInvocationHandler(transactionManager, drugService);
		DrugService proxyDrugService = createProxy(getClass().getClassLoader(), drugHandler, DrugService.class);
		
		InvocationHandler addressHandler = createTransactionalInvocationHandler(transactionManager, addressService);
		AddressService proxyAddressService = createProxy(getClass().getClassLoader(), addressHandler, AddressService.class);
		
		InvocationHandler drugOrderHandler = createTransactionalInvocationHandler(transactionManager, drugOrderService);
		DrugOrderService proxyDrugOrderService = createProxy(getClass().getClassLoader(), drugOrderHandler, DrugOrderService.class);
		
		InvocationHandler driverInvoiceHandler = createTransactionalInvocationHandler(transactionManager, driverInvoiceService);
		DriverInvoiceService proxyDriverInvoiceService = createProxy(getClass().getClassLoader(), driverInvoiceHandler, DriverInvoiceService.class);
		
		
		
		Command defaultCommand = new DefaultCommand();

		Command viewChangeUserFormCommand = new ViewChangeUserFormCommand(proxyUserService);
		Command changeUserCommand = new ChangeUserCommand(proxyUserService);
		Command deleteUserCommand = new DeleteUserCommand(proxyUserService);
		Command userLoginCommand = new UserLoginCommand(proxyUserService, proxyUserRoleService);
		Command userLogoutCommand = new UserLogoutCommand();
		Command viewUserRegistrationFormCommand = new ViewUserRegistrationFormCommand();
		Command userRegistrationCommand = new UserRegistrationCommand(proxyUserService);
		Command viewAllUsersCommand = new ViewAllUsersCommand(proxyUserService);
		Command assignPharmacistForPharmacy = new AssignPharmacistForPharmacyCommand(proxyUserService);
		Command deletePharmacistFromPharmacy = new DeletePharmacistForPharmacyCommand(proxyUserService);
		Command viewAllPharmacistsForPharmacy = new ViewAllPharmacistForPharmacyCommand(proxyPharmacyService, proxyUserService);

		Command assignRoleForUserCommand = new AssignRoleForUserCommand(proxyUserRoleService);
		Command deleteUserRoleFromUserCommand = new DeleteUserRoleFromUserCommand(proxyUserRoleService);
		Command viewAllUserRolesForUserCommand = new ViewUserRolesForUserCommand(proxyUserRoleService, proxyUserService);

		Command viewChangePharmacyFormCommand = new ViewChangePharmacyFormCommand(proxyPharmacyService);
		Command changePharmacyCommand = new ChangePharmacyCommand(proxyPharmacyService);
		Command viewCreatePharmacyFormCommand = new ViewCreatePharmacyFormCommand();
		Command createPharmacyCommand = new CreatePharmacyCommand(proxyPharmacyService);
		Command deletePharmacyCommand = new DeletePharmacyCommand(proxyPharmacyService);
		Command viewAllPharmaciesCommand = new ViewAllPharmaciesCommand(proxyPharmacyService, proxyAddressService);

		Command assignDrugFormForDrugCommand = new AssignDrugFormForDrugCommand(proxyDrugFormService);
		Command viewAllDrugFormsForDrugCommand = new ViewAllDrugFormsForDrugCommand(proxyDrugFormService, proxyDrugService);
		Command deleteDrugFormForDrugCommand = new DeleteDrugFormForDrugCommand(proxyDrugFormService);

		Command viewChangeDrugFormCommand = new ViewChangeDrugFormCommand(proxyDrugService);
		Command changeDrugCommand = new ChangeDrugCommand(proxyDrugService);
		Command viewCreateDrugFormCommand = new ViewCreateDrugFormCommand();
		Command createDrugCommand = new CreateDrugCommand(proxyDrugService);
		Command deleteDrugCommand = new DeleteDrugCommand(proxyDrugService);
		Command viewAllDrugsCommand = new ViewAllDrugsCommand(proxyDrugService, proxyDrugProducerService, proxyAddressService);
		Command viewAllDrugsForPharmacyCommand = new ViewAllDrugsForPharmacyCommand(proxyPharmacyService, proxyDrugService);

		Command viewChangeDrugQuantityFormCommand = new ViewChangeDrugQuantityFormCommand(proxyDrugOrderService);
		Command changeDrugQuantityForDrugOrderCommand = new ChangeDrugQuantityForDrugOrderCommand(proxyDrugOrderService);
		Command viewCreateDrugOrderFormCommand = new ViewCreateDrugOrderFormCommand(proxyPharmacyService, proxyDrugService);
		Command createDrugOrderCommand = new CreateDrugOrderCommand(proxyDrugOrderService);
		Command viewAllDrugOrdersCommand = new ViewAllDrugOrdersCommand(proxyDrugOrderService, proxyPharmacyService, proxyDrugService, proxyUserService);
		Command deleteDrugOrderCommand = new DeleteDrugOrderCommand(proxyDrugOrderService);
		Command confirmDrugOrderDeliveryCommand = new ConfirmDrugOrderDeliveryCommand(proxyDrugOrderService);

		Command viewDrugProviderFormCommand = new ViewDrugProviderFormCommand(proxyDrugService);
		Command distributeDrugProviderDrugsCommand = new DistributeDrugProviderDrugsCommand(proxyDrugService);

		Command viewAllDriverInvoicesCommand = new ViewAllDriverInvoicesCommand(proxyDriverInvoiceService, proxyDrugOrderService, proxyPharmacyService, proxyDrugService, proxyAddressService);
		Command acceptDriverInvoiceCommand = new AcceptDriverInvoiceCommand(proxyDriverInvoiceService);
		Command deleteDriverInvoiceCommand = new DeleteDriverInvoiceCommand(proxyDriverInvoiceService);

		
		
		CommandRegistry registrationCommands = new CommandRegistryImpl();
		registrationCommands.register(CommandName.DEFAULT, defaultCommand);

		registrationCommands.register(CommandName.VIEW_CHANGE_USER_FORM, viewChangeUserFormCommand);
		registrationCommands.register(CommandName.CHANGE_USER, changeUserCommand);
		registrationCommands.register(CommandName.DELETE_USER, deleteUserCommand);
		registrationCommands.register(CommandName.USER_LOGIN, userLoginCommand);
		registrationCommands.register(CommandName.USER_LOGOUT, userLogoutCommand);
		registrationCommands.register(CommandName.VIEW_USER_REGISTRATION_FORM, viewUserRegistrationFormCommand);
		registrationCommands.register(CommandName.USER_REGISTRATION, userRegistrationCommand);
		registrationCommands.register(CommandName.VIEW_ALL_USERS, viewAllUsersCommand);
		registrationCommands.register(CommandName.ASSIGN_PHARMACIST_FOR_PHARMACY, assignPharmacistForPharmacy);
		registrationCommands.register(CommandName.DELETE_PHARMACIST_FOR_PHARMACY, deletePharmacistFromPharmacy);
		registrationCommands.register(CommandName.VIEW_ALL_PHARMACIST_FOR_PHARMACY, viewAllPharmacistsForPharmacy);

		registrationCommands.register(CommandName.ASSIGN_USER_ROLE_FOR_USER, assignRoleForUserCommand);
		registrationCommands.register(CommandName.DELETE_USER_ROLE_FROM_USER, deleteUserRoleFromUserCommand);
		registrationCommands.register(CommandName.VIEW_ALL_USER_ROLES_FOR_USER, viewAllUserRolesForUserCommand);

		registrationCommands.register(CommandName.VIEW_CHANGE_PHARMACY_FORM, viewChangePharmacyFormCommand);
		registrationCommands.register(CommandName.CHANGE_PHARMACY, changePharmacyCommand);
		registrationCommands.register(CommandName.DELETE_PHARMACY, deletePharmacyCommand);
		registrationCommands.register(CommandName.VIEW_CREATE_PHARMACY_FORM, viewCreatePharmacyFormCommand);
		registrationCommands.register(CommandName.CREATE_PHARMACY, createPharmacyCommand);
		registrationCommands.register(CommandName.VIEW_ALL_PHARMACIES, viewAllPharmaciesCommand);

		registrationCommands.register(CommandName.ASSIGN_DRUG_FORM_FOR_DRUG, assignDrugFormForDrugCommand);
		registrationCommands.register(CommandName.VIEW_ALL_DRUG_FORMS_FOR_DRUG, viewAllDrugFormsForDrugCommand);
		registrationCommands.register(CommandName.DELETE_DRUG_FORM_FOR_DRUG, deleteDrugFormForDrugCommand);

		registrationCommands.register(CommandName.VIEW_CHANGE_DRUG_FORM, viewChangeDrugFormCommand);
		registrationCommands.register(CommandName.CHANGE_DRUG, changeDrugCommand);
		registrationCommands.register(CommandName.DELETE_DRUG, deleteDrugCommand);
		registrationCommands.register(CommandName.VIEW_CREATE_DRUG_FORM, viewCreateDrugFormCommand);
		registrationCommands.register(CommandName.CREATE_DRUG, createDrugCommand);
		registrationCommands.register(CommandName.VIEW_ALL_DRUGS, viewAllDrugsCommand);
		registrationCommands.register(CommandName.VIEW_ALL_DRUGS_FOR_PHARMACY, viewAllDrugsForPharmacyCommand);

		registrationCommands.register(CommandName.VIEW_CHANGE_DRUG_QUANTITY_FOR_DRUG_ORDER_FORM, viewChangeDrugQuantityFormCommand);
		registrationCommands.register(CommandName.CHANGE_DRUG_QUANTITY_FOR_DRUG_ORDER, changeDrugQuantityForDrugOrderCommand);
		registrationCommands.register(CommandName.VIEW_CREATE_DRUG_ORDER_FORM, viewCreateDrugOrderFormCommand);
		registrationCommands.register(CommandName.CREATE_DRUG_ORDER, createDrugOrderCommand);
		registrationCommands.register(CommandName.VIEW_ALL_DRUG_ORDERS, viewAllDrugOrdersCommand);
		registrationCommands.register(CommandName.DELETE_DRUG_ORDER, deleteDrugOrderCommand);
		registrationCommands.register(CommandName.CONFIRM_DRUG_ORDER_DELIVERY, confirmDrugOrderDeliveryCommand);

		registrationCommands.register(CommandName.VIEW_DRUG_PROVIDER_FORM, viewDrugProviderFormCommand);
		registrationCommands.register(CommandName.DISTRIBUTE_DRUG_PROVIDER_DRUGS, distributeDrugProviderDrugsCommand);

		registrationCommands.register(CommandName.VIEW_ALL_DRIVER_INVOICES, viewAllDriverInvoicesCommand);
		registrationCommands.register(CommandName.ACCEPT_DRIVER_INVOICE, acceptDriverInvoiceCommand);
		registrationCommands.register(CommandName.DELETE_DRIVER_INVOICE, deleteDriverInvoiceCommand);

		
		
		beens.put(ConnectionPool.class, connectionPool);
		beens.put(TransactionManager.class, transactionManager);
		beens.put(ConnectionManager.class, connectionManager);

		beens.put(AddressDao.class, addressDao);
		beens.put(DriverInvoiceDao.class, driverInvoiceDao);
		beens.put(DrugDao.class, drugDao);
		beens.put(DrugFormDao.class, drugFormDao);
		beens.put(DrugOrderDao.class, drugOrderDao);
		beens.put(DrugProducerDao.class, drugProducerDao);
		beens.put(PharmacyDao.class, pharmacyDao);
		beens.put(UserDao.class, userDao);
		beens.put(UserRoleDao.class, userRoleDao);

		beens.put(UserService.class, proxyUserService);
		beens.put(UserRoleService.class, proxyUserRoleService);
		beens.put(PharmacyService.class, proxyPharmacyService);
		beens.put(DrugProducerService.class, proxyDrugProducerService);
		beens.put(DrugFormService.class, proxyDrugFormService);
		beens.put(DrugService.class, proxyDrugService);
		beens.put(AddressService.class, proxyAddressService);
		beens.put(DrugOrderService.class, proxyDrugOrderService);
		beens.put(DriverInvoiceService.class, proxyDriverInvoiceService);

		beens.put(DefaultCommand.class, defaultCommand);
		beens.put(ViewChangeUserFormCommand.class, viewChangeUserFormCommand);
		beens.put(ChangeUserCommand.class, changeUserCommand);
		beens.put(DeleteUserCommand.class, deleteUserCommand);
		beens.put(UserLoginCommand.class, userLoginCommand);
		beens.put(UserLogoutCommand.class, userLogoutCommand);
		beens.put(ViewUserRegistrationFormCommand.class, viewUserRegistrationFormCommand);
		beens.put(UserRegistrationCommand.class, userRegistrationCommand);
		beens.put(ViewAllUsersCommand.class, viewAllUsersCommand);
		beens.put(AssignPharmacistForPharmacyCommand.class, assignPharmacistForPharmacy);
		beens.put(DeletePharmacistForPharmacyCommand.class, deletePharmacistFromPharmacy);
		beens.put(ViewAllPharmacistForPharmacyCommand.class, viewAllPharmacistsForPharmacy);

		beens.put(AssignRoleForUserCommand.class, assignRoleForUserCommand);
		beens.put(DeleteUserRoleFromUserCommand.class, deleteUserRoleFromUserCommand);
		beens.put(ViewUserRolesForUserCommand.class, viewAllUserRolesForUserCommand);
		
		beens.put(ViewChangePharmacyFormCommand.class, viewChangePharmacyFormCommand);
		beens.put(ChangePharmacyCommand.class, changePharmacyCommand);
		beens.put(ViewCreatePharmacyFormCommand.class, viewCreatePharmacyFormCommand);
		beens.put(CreatePharmacyCommand.class, createPharmacyCommand);
		beens.put(DeletePharmacyCommand.class, deletePharmacyCommand);
		beens.put(ViewAllPharmaciesCommand.class, viewAllPharmaciesCommand);
		
		beens.put(AssignDrugFormForDrugCommand.class, assignDrugFormForDrugCommand);
		beens.put(ViewAllDrugFormsForDrugCommand.class, viewAllDrugFormsForDrugCommand);
		beens.put(DeleteDrugFormForDrugCommand.class, deleteDrugFormForDrugCommand);
		
		beens.put(ViewChangeDrugFormCommand.class, viewChangeDrugFormCommand);
		beens.put(ChangeDrugCommand.class, changeDrugCommand);
		beens.put(ViewCreateDrugFormCommand.class, viewCreateDrugFormCommand);
		beens.put(CreateDrugCommand.class, createDrugCommand);
		beens.put(DeleteDrugCommand.class, deleteDrugCommand);
		beens.put(ViewAllDrugsCommand.class, viewAllDrugsCommand);
		beens.put(ViewAllDrugsForPharmacyCommand.class, viewAllDrugsForPharmacyCommand);
		
		beens.put(ViewChangeDrugQuantityFormCommand.class, viewChangeDrugQuantityFormCommand);
		beens.put(ChangeDrugQuantityForDrugOrderCommand.class, changeDrugQuantityForDrugOrderCommand);
		beens.put(ViewCreateDrugOrderFormCommand.class, viewCreateDrugOrderFormCommand);
		beens.put(CreateDrugOrderCommand.class, createDrugOrderCommand);
		beens.put(ViewAllDrugOrdersCommand.class, viewAllDrugOrdersCommand);
		beens.put(DeleteDrugOrderCommand.class, deleteDrugOrderCommand);
		beens.put(ConfirmDrugOrderDeliveryCommand.class, confirmDrugOrderDeliveryCommand);
		
		beens.put(ViewAllDriverInvoicesCommand.class, viewAllDriverInvoicesCommand);
		beens.put(AcceptDriverInvoiceCommand.class, acceptDriverInvoiceCommand);
		beens.put(DeleteDriverInvoiceCommand.class, deleteDriverInvoiceCommand);
		
		beens.put(ViewDrugProviderFormCommand.class, viewDrugProviderFormCommand);
		beens.put(DistributeDrugProviderDrugsCommand.class, distributeDrugProviderDrugsCommand);
		

		
		beens.put(CommandRegistryImpl.class, registrationCommands);	

		LOGGER.log(Level.INFO, "ApplicationContext was initialized");
	}

	
	private <T> T createProxy(ClassLoader classLoader, InvocationHandler handler, Class<T>... interfaces) {
		return (T) Proxy.newProxyInstance(classLoader, interfaces, handler);
	}

	private <T> InvocationHandler createTransactionalInvocationHandler(TransactionManager transactionManager,
			T service) {
		return (proxy, method, methodArgs) -> {
			Method declaredMethod = service.getClass().getDeclaredMethod(method.getName(), method.getParameterTypes());
			if (method.isAnnotationPresent(Transactional.class) || declaredMethod.isAnnotationPresent(Transactional.class)) {
				try {
					transactionManager.beginTransaction();
					Object result = method.invoke(service, methodArgs);
					transactionManager.commitTransaction();
					return result;
				} catch (ConnectionException|InvocationTargetException exception) {
					transactionManager.rollbackTransaction();
					throw new ServiceException(exception.getCause().getMessage(), exception.getCause());
				}
			} else {
				try {
					return method.invoke(service, methodArgs);
				} catch (InvocationTargetException exception) {
					throw new ServiceException(exception.getCause().getMessage(), exception.getCause());
				}
			}
		};
	}

	public void destroy() {
		
		ConnectionPoolImpl.getInstance().destroy();
		beens.clear();
		LOGGER.log(Level.INFO, "ApplicationContext was destroyed");
	}

	
	@SuppressWarnings("unchecked")
	public <T> T getBean(Class<T> clazz) {
		return (T) beens.get(clazz);
	}

}
