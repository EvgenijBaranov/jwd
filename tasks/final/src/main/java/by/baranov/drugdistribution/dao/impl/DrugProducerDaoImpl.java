package by.baranov.drugdistribution.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.connectionpool.ConnectionManager;
import by.baranov.drugdistribution.dao.DrugProducerDao;
import by.baranov.drugdistribution.entity.DrugProducer;
import by.baranov.drugdistribution.exception.DaoException;

public class DrugProducerDaoImpl implements DrugProducerDao{

	private static final String GET_ALL_DRUG_PRODUCER = "SELECT id, name, licence_number, address_id FROM drug_producer";
	private static final String GET_DRUG_PRODUCER_BY_ID = "SELECT id, name, licence_number, address_id FROM drug_producer WHERE id=?";
	private static final String SAVE_DRUG_PRODUCER = "INSERT INTO drug_producer (name, licence_number, address_id) VALUES (?,?,?)";
	private static final String UPDATE_DRUG_PRODUCER = "UPDATE drug_producer SET name=?, licence_number=?, address_id=? WHERE id=?";
	private static final String DELETE_DRUG_PRODUCER = "DELETE FROM drug_producer WHERE id=?";
	
	private static final Logger LOGGER = LogManager.getLogger();

	private ConnectionManager connectionManager;

	public DrugProducerDaoImpl(ConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	@Override
	public List<DrugProducer> getAll() throws DaoException {

		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_ALL_DRUG_PRODUCER);
				ResultSet resultSet = preparedStatement.executeQuery()) {
			List<DrugProducer> result = new ArrayList<>();
			while (resultSet.next()) {
				DrugProducer drugProducer = parseDrugProducer(resultSet);
				result.add(drugProducer);
			}
			LOGGER.log(Level.DEBUG, "Result of executing method getAll() : {}", result);
			return result;
		} catch (SQLException exception) {
			throw new DaoException("Error getting all drug producers", exception);
		}
	}

	private DrugProducer parseDrugProducer(ResultSet resultSet) throws SQLException {
		DrugProducer drugProducer = new DrugProducer();
		drugProducer.setId(resultSet.getLong("id"));
		drugProducer.setName(resultSet.getString("name"));
		drugProducer.setLicenceNumber(resultSet.getString("licence_number"));
		drugProducer.setAddressId(resultSet.getLong("address_id"));
		return drugProducer;
	}

	@Override
	public DrugProducer getById(Long id) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_DRUG_PRODUCER_BY_ID)) {
			preparedStatement.setLong(1, id);
			try (ResultSet resultSet = preparedStatement.executeQuery();) {
				resultSet.next();
				DrugProducer result = parseDrugProducer(resultSet);
				LOGGER.log(Level.DEBUG, "Result of executing method getById() : {}", result);
				return result;
			}
		} catch (SQLException exception) {
			throw new DaoException("Error getting drug producer with id=" + id, exception);
		}
	}

	@Override
	public void save(DrugProducer drugProducer) throws DaoException {

		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SAVE_DRUG_PRODUCER, Statement.RETURN_GENERATED_KEYS)) {
			int i = 0;
			preparedStatement.setString(++i, drugProducer.getName());
			preparedStatement.setString(++i, drugProducer.getLicenceNumber());
			preparedStatement.setLong(++i, drugProducer.getAddressId());
			preparedStatement.executeUpdate();

			try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
				while (generatedKeys.next()) {
					drugProducer.setId(generatedKeys.getLong(1));
				}
			}
			LOGGER.log(Level.DEBUG, "Result of executing method save() is success");
		} catch (SQLException exception) {
			throw new DaoException("Error saving drug producer=" + drugProducer, exception);
		}
	}

	@Override
	public void update(DrugProducer drugProducer) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_DRUG_PRODUCER)) {
			int i = 0;
			preparedStatement.setString(++i, drugProducer.getName());
			preparedStatement.setString(++i, drugProducer.getLicenceNumber());
			preparedStatement.setLong(++i, drugProducer.getAddressId());
			preparedStatement.setLong(++i, drugProducer.getId());
			preparedStatement.executeUpdate();
			LOGGER.log(Level.DEBUG, "Result of executing method update() is success");
		} catch (SQLException exception) {
			throw new DaoException("Error updating drug producer=" + drugProducer, exception);
		}
	}

	@Override
	public void delete(DrugProducer drugProducer) throws DaoException {
		try (Connection connection = connectionManager.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(DELETE_DRUG_PRODUCER)) {
			preparedStatement.setLong(1, drugProducer.getId());
			preparedStatement.executeUpdate();
			LOGGER.log(Level.DEBUG, "Result of executing method delete() is success");
		} catch (SQLException exception) {
			throw new DaoException("Error deleting drug producer=" + drugProducer, exception);
		}
	}
	
}
