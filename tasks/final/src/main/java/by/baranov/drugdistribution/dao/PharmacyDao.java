package by.baranov.drugdistribution.dao;

import java.util.List;

import by.baranov.drugdistribution.entity.Pharmacy;
import by.baranov.drugdistribution.exception.DaoException;

public interface PharmacyDao extends CRUDOperation<Long, Pharmacy>{
	
	List<Pharmacy> getPharmaciesByPharmacistId(Long pharmacistId) throws DaoException;
	
}
