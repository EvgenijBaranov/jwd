package by.baranov.drugdistribution.dao;

import java.util.List;

import by.baranov.drugdistribution.entity.UserRole;
import by.baranov.drugdistribution.exception.DaoException;

public interface UserRoleDao extends CRUDOperation<Long, UserRole>{
	
	void assignRole(Long userId, Long userRoleId) throws DaoException;
	
	List<UserRole> getUserRolesByUserId(Long userId) throws DaoException;
	
	void deleteUserRoleForUser(Long userId, Long userRoleId)  throws DaoException;
	
}
