package by.baranov.drugdistribution.entity;

public enum DrugAvailability {
	WITH_PRESCRIPTION, FREE;
	
	public String getName() {
		return toString();
	}
}
