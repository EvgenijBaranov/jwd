package by.baranov.drugdistribution.connectionpool;

import java.sql.Connection;

public interface ConnectionPool {
	
	Connection getConnection();
	
	void releaseConnection(Connection connection);
	
	void destroy();

	int getAvailableConnectionsQuantity();
	int getUsedConnectionsQuantity();
}
