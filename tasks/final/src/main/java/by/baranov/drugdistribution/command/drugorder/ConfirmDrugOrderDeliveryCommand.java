package by.baranov.drugdistribution.command.drugorder;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.DrugOrder;
import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DrugOrderService;

public class ConfirmDrugOrderDeliveryCommand implements Command{
	private static final String ATTRIBUTE_NAME_FOR_DRUG_ORDERS = "drugOrdersForPharmaciesWithPharmacist";
	private static final Logger LOGGER = LogManager.getLogger();

	private final DrugOrderService drugOrderService;

	public ConfirmDrugOrderDeliveryCommand(DrugOrderService drugOrderService) {
		this.drugOrderService = drugOrderService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String inputedDrugOrderId = request.getParameter(AppConstants.PARAMETER_DRUG_ORDER_ID).trim();
		try {
			Long drugOrderId = Long.valueOf(inputedDrugOrderId);
			drugOrderService.confirmDrugOrderDelivery(drugOrderId);
			
			User registeredUser = (User) request.getSession().getAttribute(AppConstants.ATTRIBUTE_NAME_REGISTERED_USER);
			List<DrugOrder> drugOrdersForPharmaciesWithPharmacist = drugOrderService.getDrugOrdersForPharmaciesByPharmacistId(registeredUser.getId());
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_DRUG_ORDERS, drugOrdersForPharmaciesWithPharmacist);
		} catch (ServiceException exception) {
			request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
			LOGGER.log(Level.ERROR, "Drug order delivery confirming error", exception);
		}
		LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
				getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_DRUG_ORDER_COMMANDS);
		return TypeCommandAction.REDIRECT + AppConstants.FILE_PATH_FOR_DRUG_ORDER_COMMANDS;
	}
}
