package by.baranov.drugdistribution.command.pharmacy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.Address;
import by.baranov.drugdistribution.entity.Pharmacy;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.PharmacyService;
import by.baranov.drugdistribution.validation.PharmacyValidator;
import by.baranov.drugdistribution.validation.ValidationResult;
import by.baranov.drugdistribution.validation.Validator;

public class ChangePharmacyCommand implements Command{
	private static final Logger LOGGER = LogManager.getLogger();
	private final PharmacyService pharmacyService;

	public ChangePharmacyCommand(PharmacyService pharmacyService) {
		this.pharmacyService = pharmacyService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String inputedPharmacyId = request.getParameter(AppConstants.PARAMETER_PHARMACY_ID).trim();
		String inputedPharmacyName = request.getParameter(AppConstants.PARAMETER_PHARMACY_NAME).trim();
		String inputedPharmacyLicenceNumber = request.getParameter(AppConstants.PARAMETER_PHARMACY_LICENCE_NUMBER).trim();
		String inputedPharmacyPhone = request.getParameter(AppConstants.PARAMETER_PHARMACY_PHONE).trim();
		String inputedPharmacyAddressCountry = request.getParameter(AppConstants.PARAMETER_PHARMACY_ADDRESS_COUNTRY).trim();
		String inputedPharmacyAddressTown = request.getParameter(AppConstants.PARAMETER_PHARMACY_ADDRESS_TOWN).trim();
		String inputedPharmacyAddressStreet = request.getParameter(AppConstants.PARAMETER_PHARMACY_ADDRESS_STREET).trim();
		String inputedPharmacyAddressHouse = request.getParameter(AppConstants.PARAMETER_PHARMACY_ADDRESS_HOUSE).trim();
		
		Validator pharmacyValidator = new PharmacyValidator();
		ValidationResult validationResult = pharmacyValidator.validate(inputedPharmacyName, inputedPharmacyLicenceNumber, inputedPharmacyPhone,
																		inputedPharmacyAddressCountry, inputedPharmacyAddressTown,
																		inputedPharmacyAddressStreet, inputedPharmacyAddressHouse);
		if(validationResult.isEmpty()) {
			try {
				Pharmacy pharmacy = new Pharmacy();
				Long pharmacyId = Long.valueOf(inputedPharmacyId);
				pharmacy.setId(pharmacyId);
				pharmacy.setName(inputedPharmacyName);
				pharmacy.setLicenceNumber(inputedPharmacyLicenceNumber);
				pharmacy.setPhone(inputedPharmacyPhone);
				
				Address pharmacyAddress = new Address();
				pharmacyAddress.setCountry(inputedPharmacyAddressCountry);
				pharmacyAddress.setTown(inputedPharmacyAddressTown);
				pharmacyAddress.setStreet(inputedPharmacyAddressStreet);
				pharmacyAddress.setHouse(inputedPharmacyAddressHouse);
				pharmacyService.update(pharmacy, pharmacyAddress);
			} catch (ServiceException exception) {
				request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
				LOGGER.log(Level.ERROR, "Error updating pharmacy", exception);
			}
			LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
					getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_PHARMACY_COMMANDS);
		} else {
			request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_VALIDATION_RESULT, validationResult);
			LOGGER.log(Level.ERROR, "{} was executed with mistakes:{}. Page path for controller is {}",
					getClass().getSimpleName(), validationResult, AppConstants.FILE_PATH_FOR_PHARMACY_COMMANDS);
		}

		return TypeCommandAction.REDIRECT + AppConstants.FILE_PATH_FOR_PHARMACY_COMMANDS;
	}
}
