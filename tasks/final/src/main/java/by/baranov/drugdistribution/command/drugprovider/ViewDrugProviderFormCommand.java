package by.baranov.drugdistribution.command.drugprovider;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.Drug;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DrugService;

public class ViewDrugProviderFormCommand implements Command {
	
	private static final String ATTRIBUTE_NAME_FOR_DRUGS = "drugs";
	private static final String ATTRIBUTE_VALUE_FOR_VIEW = "drugDistributionProviderForm";
	private static final Logger LOGGER = LogManager.getLogger();
	private final DrugService drugService;

	public ViewDrugProviderFormCommand(DrugService drugService) {
		this.drugService = drugService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		try {
			List<Drug> drugs = drugService.getAll();
			request.setAttribute(ATTRIBUTE_NAME_FOR_DRUGS, drugs);
		} catch (ServiceException exception) {
			request.setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
			LOGGER.log(Level.ERROR, "Drugs view error for drug provider", exception);
		}
		request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_VIEW, ATTRIBUTE_VALUE_FOR_VIEW);
		LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
				getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_DRUG_PROVIDER_COMMANDS);
		return TypeCommandAction.FORWARD + AppConstants.FILE_PATH_FOR_DRUG_PROVIDER_COMMANDS;
	}
}
