package by.baranov.drugdistribution.service;

import java.util.List;

import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.entity.UserRole;
import by.baranov.drugdistribution.exception.ServiceException;

public interface UserService{
	
	List<User> getAll() throws ServiceException;
	User getById(Long id) throws ServiceException;	
	void save(User entity) throws ServiceException;
	void update(User entity) throws ServiceException;
	void delete (Long id) throws ServiceException;
	
	boolean loginUser(String login, String password) throws ServiceException;
	
	User getUserByLogin(String login) throws ServiceException;
	
	List<User> getPharmacistsByPharmacyId(Long pharmacyId) throws ServiceException;
	void assignPharmacistForPharmacy(Long pharmacyId, Long userId) throws ServiceException;
	void deletePharmacistForPharmacy(Long pharmacyId, Long userId) throws ServiceException;
	
	List<User> getAllUsersByUserRole(UserRole userRole) throws ServiceException;
	
}
