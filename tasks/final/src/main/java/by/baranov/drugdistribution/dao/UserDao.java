package by.baranov.drugdistribution.dao;

import java.util.List;

import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.exception.DaoException;

public interface UserDao extends CRUDOperation<Long, User>{
	
	User getUserByLogin(String login) throws DaoException;
	
	void assignPharmacistToPharmacy(Long pharmacyId, Long userId) throws DaoException;
	void deletePharmacistFromPharmacy(Long pharmacyId, Long userId) throws DaoException;
	List<User> getPharmacistsForPharmacy(Long pharmacyId) throws DaoException;
	
	List<User> getAllUsersByUserRoleId(Long userRoleId) throws DaoException;
	
}
