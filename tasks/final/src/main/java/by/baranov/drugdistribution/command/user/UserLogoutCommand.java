package by.baranov.drugdistribution.command.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.SecurityContext;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;

public class UserLogoutCommand implements Command{
	
	private static final Logger LOGGER = LogManager.getLogger();

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		SecurityContext.getInstance().deleteCurrentUserRolesFromSession(request.getSession().getId());
		request.getSession().invalidate();
		LOGGER.log(Level.INFO, "{} was executed, session invalidated", getClass().getSimpleName());
		return TypeCommandAction.REDIRECT + "/jsp/userLoginPage.jsp";
	}
}
