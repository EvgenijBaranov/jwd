package by.baranov.drugdistribution.validation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ValidationResult implements Serializable{
	private static final long serialVersionUID = -147490264910851604L;
	private List<ValidationMessage> validationMessages = new ArrayList<>();
	
	public void addMessage(ValidationMessage message) {
		validationMessages.add(message);
	}
	
	public List<ValidationMessage> getValidationResult() {
		return validationMessages;
	}
	
	public boolean isEmpty() {
		return validationMessages.isEmpty();
	}

	@Override
	public String toString() {
		return validationMessages.toString();
	}
	
}
