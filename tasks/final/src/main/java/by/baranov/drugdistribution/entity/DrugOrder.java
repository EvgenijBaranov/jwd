package by.baranov.drugdistribution.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class DrugOrder implements AbstractEntity, Serializable {

	private static final long serialVersionUID = -6060809338917359350L;
	private Long id;
	private Long pharmacyId;
	private Long pharmacistId;
	private Long drugId;
	private int drugQuantity;
	private LocalDateTime creationDate;
	private LocalDateTime terminationDate;
	private DrugOrderStatus orderStatus;

	public DrugOrder() {
	}

	public DrugOrder(Long id, Long pharmacyId, Long pharmacistId, Long drugId, int drugQuantity,
			LocalDateTime creationDate, LocalDateTime terminationDate, DrugOrderStatus orderStatus) {
		this.id = id;
		this.pharmacyId = pharmacyId;
		this.pharmacistId = pharmacistId;
		this.drugId = drugId;
		this.drugQuantity = drugQuantity;
		this.creationDate = creationDate;
		this.terminationDate = terminationDate;
		this.orderStatus = orderStatus;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPharmacyId() {
		return pharmacyId;
	}

	public void setPharmacyId(Long pharmacyId) {
		this.pharmacyId = pharmacyId;
	}

	public Long getPharmacistId() {
		return pharmacistId;
	}

	public void setPharmacistId(Long pharmacistId) {
		this.pharmacistId = pharmacistId;
	}

	public Long getDrugId() {
		return drugId;
	}

	public void setDrugId(Long drugId) {
		this.drugId = drugId;
	}

	public int getDrugQuantity() {
		return drugQuantity;
	}

	public void setDrugQuantity(int drugQuantity) {
		this.drugQuantity = drugQuantity;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public LocalDateTime getTerminationDate() {
		return terminationDate;
	}

	public void setTerminationDate(LocalDateTime terminationDate) {
		this.terminationDate = terminationDate;
	}

	public DrugOrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(DrugOrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	@Override
	public int hashCode() {
		return Objects.hash(creationDate, drugId, drugQuantity, id, orderStatus, pharmacistId, pharmacyId,
				terminationDate);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof DrugOrder))
			return false;
		DrugOrder other = (DrugOrder) obj;
		return Objects.equals(creationDate, other.creationDate) && Objects.equals(drugId, other.drugId)
				&& drugQuantity == other.drugQuantity && Objects.equals(id, other.id)
				&& orderStatus == other.orderStatus && Objects.equals(pharmacistId, other.pharmacistId)
				&& Objects.equals(pharmacyId, other.pharmacyId)
				&& Objects.equals(terminationDate, other.terminationDate);
	}

	@Override
	public String toString() {
		return "DrugOrder [id=" + id + ", pharmacyId=" + pharmacyId + ", pharmacistId=" + pharmacistId + ", drugId="
				+ drugId + ", drugQuantity=" + drugQuantity + ", creationDate=" + creationDate + ", terminationDate="
				+ terminationDate + ", orderStatus=" + orderStatus + "]";
	}

}
