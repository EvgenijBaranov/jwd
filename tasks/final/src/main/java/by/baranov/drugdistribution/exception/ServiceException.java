package by.baranov.drugdistribution.exception;

public class ServiceException extends Exception{

	private static final long serialVersionUID = -3130353417095775268L;
	
	public ServiceException(Throwable cause) {
		super(cause);
	}

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ServiceException(String message) {
		super(message);
	}

	
	
}
