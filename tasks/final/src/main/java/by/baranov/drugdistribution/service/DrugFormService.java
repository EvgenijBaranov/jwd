package by.baranov.drugdistribution.service;

import java.util.List;

import by.baranov.drugdistribution.entity.DrugForm;
import by.baranov.drugdistribution.exception.ServiceException;

public interface DrugFormService{
	
	List<DrugForm> getDrugFormsByDrugId(Long drugId) throws ServiceException;

	void assignDrugFormForDrug(Long drugId, DrugForm drugForm) throws ServiceException;
	
	void deleteDrugFormForDrug(Long drugId, Long drugFormId) throws ServiceException;
}
