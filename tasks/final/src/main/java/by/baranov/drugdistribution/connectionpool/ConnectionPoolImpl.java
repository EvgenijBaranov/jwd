package by.baranov.drugdistribution.connectionpool;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.exception.ConnectionException;
import by.baranov.drugdistribution.exception.DataBasePropertyFileException;

public class ConnectionPoolImpl implements ConnectionPool {
	private static final String CONNECTION_PROPERTY_FILE_NAME = "connection.properties";
	private static final Logger LOGGER = LogManager.getLogger();
	private static final Lock lock = new ReentrantLock();
	private static final AtomicBoolean instanceNotCreated = new AtomicBoolean(true);
	private static ConnectionPoolImpl connectionPoolInstance = null;

	private Properties connectionPropertyFile;

	private String url;
	private String login;
	private String password;
	private int poolSize;
	private String driverDataBase;
	private BlockingQueue<Connection> availableConnections;
	private BlockingQueue<Connection> usedConnections;

	private ConnectionPoolImpl() {
		readConnectionPropertyFile(CONNECTION_PROPERTY_FILE_NAME);
		setParametrsConnectionPoolFromPropertyFile();
		initDriver();
		availableConnections = new ArrayBlockingQueue<>(poolSize);
		usedConnections = new ArrayBlockingQueue<>(poolSize);
	}

	public static ConnectionPoolImpl getInstance() {
		if (instanceNotCreated.get()) {
			lock.lock();
				try {
					if (instanceNotCreated.get()) {
						connectionPoolInstance = new ConnectionPoolImpl();
						instanceNotCreated.set(false);
					}
				} finally {
					lock.unlock();
				}
		}
		return connectionPoolInstance;
	}

	@Override
	public Connection getConnection() {
		Connection proxyConnection = null;
		lock.lock();
		if (availableConnections.isEmpty() && usedConnections.size() < poolSize) {
			try {
				LOGGER.log(Level.INFO,
						"There aren't available connections. But it's possible to create a new connection for the pool.");
				Connection connection = createConnection();
				usedConnections.put(connection);
				proxyConnection = createProxyConnection(connection);
			} catch (InterruptedException exception) {
				Thread.currentThread().interrupt();
				throw new ConnectionException(
						"Trying to put a connection to the queue of used connections in the connection pool has been interrupted",
						exception);
			} finally {
				lock.unlock();
			}
		} else {
			lock.unlock();
			try {
				LOGGER.log(Level.INFO, "Trying to receive a connection from the connection pool. The connection pool size :{}", availableConnections.size());
				Connection connection = availableConnections.take();
				LOGGER.log(Level.INFO, "The connection is received from the connection pool. The connection pool size :{}", availableConnections.size());
				usedConnections.put(connection);
				proxyConnection = createProxyConnection(connection);
			} catch (InterruptedException exception) {
				Thread.currentThread().interrupt();
				throw new ConnectionException(
						"Trying to take a connection from the queue of available connections in the connection pool has been interrupted",
						exception);
			}
		}
		return proxyConnection;
	}

	@Override
	public void releaseConnection(Connection connection) {
		try {
			usedConnections.remove(connection);
			availableConnections.put(connection);
			LOGGER.log(Level.INFO, "The connection was returned in the ConnectionPool. The connection pool size : {}",
					availableConnections.size());
		} catch (InterruptedException exception) {
			Thread.currentThread().interrupt();
			throw new ConnectionException(
					"Trying to return a connection to used connections in the connection pool has been interrupted",
					exception);
		}
	}

	@Override
	public void destroy() {
		lock.lock();
		LOGGER.log(Level.DEBUG, "usedConnections : {} , availableConnections : {}", usedConnections.size(),
				availableConnections.size());
		try {

			for (Connection connection : usedConnections) {
				releaseConnection(connection);
				LOGGER.log(Level.DEBUG, "After release connection - usedConnections : {} , availableConnections : {}",
						usedConnections.size(), availableConnections.size());
			}
			
			for (int i = 0 ; i < availableConnections.size(); i++) {
				availableConnections.take().close();
				LOGGER.log(Level.DEBUG, "Trying to close connections - usedConnections : {} , availableConnections : {}",
						usedConnections.size(), availableConnections.size());
			}
			
		} catch (InterruptedException exception) {
			Thread.currentThread().interrupt();
			throw new ConnectionException(
					"Trying to take a connection from the queue of available connections in the connection pool has been interrupted",
					exception);
		} catch (SQLException sqlException) {
			throw new ConnectionException("Trying to close a connection in the connection pool failed",sqlException);
		} finally {
			lock.unlock();
		}
	}

	@Override
	public int getAvailableConnectionsQuantity() {
		return availableConnections.size();
	}

	@Override
	public int getUsedConnectionsQuantity() {
		return usedConnections.size();
	}

	private Connection createConnection() {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(url, login, password);
			LOGGER.log(Level.INFO, "A new connection has been created.");
		} catch (SQLException exception) {
			throw new ConnectionException("Can't create a new connection to database with url " + url, exception);
		}
		return connection;
	}
	
	private Connection createProxyConnection(Connection connection) {
		return (Connection) Proxy.newProxyInstance(connection.getClass().getClassLoader(),
				new Class[] { Connection.class }, 
				(proxy, method, methodArgs) -> {
					if ("close".equals(method.getName())) {
						releaseConnection(connection);
						return null;
					} else {
						return method.invoke(connection, methodArgs);
					}
				});
	}
	
	private void readConnectionPropertyFile(String propertyConnectionFileName) {
		try (InputStream inputStreamFromPropertyFile = getClass().getClassLoader().getResourceAsStream(propertyConnectionFileName)) {
			connectionPropertyFile = new Properties();
			connectionPropertyFile.load(inputStreamFromPropertyFile);
		} catch (IOException exception) {
			LOGGER.log(Level.FATAL, "Can't load data base property file: " + propertyConnectionFileName, exception);
			throw new DataBasePropertyFileException("The file " + propertyConnectionFileName + " wasn't found or can't be read",
					exception);
		}
	}

	private void setParametrsConnectionPoolFromPropertyFile() {
		url = connectionPropertyFile.getProperty("db.url");
		login = connectionPropertyFile.getProperty("db.user_login");
		password = connectionPropertyFile.getProperty("db.user_pasword");
		poolSize = Integer.parseInt(connectionPropertyFile.getProperty("db.connection_pool_size"));
		driverDataBase = connectionPropertyFile.getProperty("db.driver_jdbc");
	}

	private void initDriver() {
        try {
            Class.forName(driverDataBase);
        } catch (ClassNotFoundException exception) {
            LOGGER.log(Level.FATAL, "Driver for database can't be found and loaded", exception);
            throw new DataBasePropertyFileException("Driver for database can't be found", exception);
        }
    }


}
