package by.baranov.drugdistribution.entity;

public enum DrugOrderStatus {
	
	CREATED, INVOICE_CREATED, DELIVERY_IN_PROGRESS, DELIVERED;
	
	public String getName() {
		return toString();
	}
}
