package by.baranov.drugdistribution.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import by.baranov.drugdistribution.entity.Address;
import by.baranov.drugdistribution.entity.Drug;
import by.baranov.drugdistribution.entity.DrugForm;
import by.baranov.drugdistribution.entity.DrugProducer;
import by.baranov.drugdistribution.entity.PharmacyHasDrug;
import by.baranov.drugdistribution.exception.ServiceException;

public interface DrugService{
	
	List<Drug> getAll() throws ServiceException;
	Drug getById(Long id) throws ServiceException;	
	void save(Drug drug, DrugForm drugForm, DrugProducer drugProducer, Address drugProducerAddress) throws ServiceException;
	void update(Drug drug, DrugForm drugForm, DrugProducer drugProducer, Address drugProducerAddress) throws ServiceException;
	void delete (Long id) throws ServiceException;
	
	List<PharmacyHasDrug> getAllDrugQuantityForPharmacy(Long pharmacyId, Long drugId) throws ServiceException;
	
	Set<Drug> getAllDrugsByPharmacyId(Long pharmacyId) throws ServiceException;
	
	String distributeDrugs(Long drugId, LocalDate drugManufactureDate, LocalDate drugExpiryDate, 
															int drugQuantity, BigDecimal drugPrice) throws ServiceException;
	
}
