package by.baranov.drugdistribution.dao;

import by.baranov.drugdistribution.entity.DriverInvoice;
import by.baranov.drugdistribution.exception.DaoException;

public interface DriverInvoiceDao extends CRUDOperation<Long, DriverInvoice>{
	
	DriverInvoice getDriverInvoiceByDrugOrderId(Long drugOrderId) throws DaoException;
	
}
