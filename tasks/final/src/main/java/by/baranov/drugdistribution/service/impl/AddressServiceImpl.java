package by.baranov.drugdistribution.service.impl;

import by.baranov.drugdistribution.dao.AddressDao;
import by.baranov.drugdistribution.entity.Address;
import by.baranov.drugdistribution.exception.DaoException;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.AddressService;

public class AddressServiceImpl implements AddressService{
	private final AddressDao addressDao;

	public AddressServiceImpl(AddressDao addressDao) {
		this.addressDao = addressDao;
	}

	@Override
	public Address getById(Long id) throws ServiceException {
		try {
			return addressDao.getById(id);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

}
