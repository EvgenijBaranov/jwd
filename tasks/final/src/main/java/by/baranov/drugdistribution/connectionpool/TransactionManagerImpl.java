package by.baranov.drugdistribution.connectionpool;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.exception.ConnectionException;

public class TransactionManagerImpl implements TransactionManager {
	private static final Logger LOGGER = LogManager.getLogger();
	private final ConnectionPool connectionPool;
	private ThreadLocal<Connection> localConnection = new ThreadLocal<>();

	public TransactionManagerImpl(ConnectionPool connectionPool) {
		this.connectionPool = connectionPool;
	}

	@Override
	public void beginTransaction() {
		if (localConnection.get() == null) {
			try {
				Connection connection = connectionPool.getConnection();
				connection.setAutoCommit(false);
				LOGGER.log(Level.INFO, "Transaction is started");
				localConnection.set(connection);
			} catch (SQLException exception) {
				throw new ConnectionException("Error accessing database or connection was closed", exception);
			}
		} else {
			LOGGER.log(Level.WARN, "Transaction has already started");
		}
	}

	@Override
	public void commitTransaction() {
		Connection connection = localConnection.get();
		if (connection != null) {
			try {
				connection.commit();
				LOGGER.log(Level.INFO, "Transaction is committed");
				connection.close();
				localConnection.remove();
			} catch (SQLException exception) {
				throw new ConnectionException(
						"Error accessing database, the connection was closed or wrong auto-commit mode on the connection",
						exception);
			}
		} else {
			LOGGER.log(Level.WARN, "Transaction has already committed");
		}
	}

	@Override
	public void rollbackTransaction() {
		Connection connection = localConnection.get();
		if (connection != null) {
			try {
				connection.rollback();
				LOGGER.log(Level.INFO, "Transaction is rollbacked");
				connection.close();
				localConnection.remove();
			} catch (SQLException exception) {
				throw new ConnectionException(
						"Error accessing database, the connection was closed or wrong auto-commit mode on the connection",
						exception);
			}
		} else {
			LOGGER.log(Level.WARN, "Transaction has already rollbacked");
		}
	}

	@Override
	public Connection getConnection() {
		Connection connection = localConnection.get();
		if (connection != null) {
			return (Connection) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[] { Connection.class },
				(proxy, method, methodArgs) -> {
					if ("close".equals(method.getName())) {
						return null;
					} else {
						return method.invoke(connection, methodArgs);
					}
				});
		} else {
			return connection;
		}
	}

}
