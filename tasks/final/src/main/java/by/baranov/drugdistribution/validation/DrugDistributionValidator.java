package by.baranov.drugdistribution.validation;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DrugDistributionValidator implements Validator {
	private static final Logger LOGGER = LogManager.getLogger();
	private ValidationResult validationResult = new ValidationResult();

	@Override
	public ValidationResult validate(String... inputedFieldsData) {

		List<String> listInputedFieldsData = Arrays.asList(inputedFieldsData);
		LOGGER.log(Level.DEBUG, "DrugDistributionValidator inputed data = {}", listInputedFieldsData);

		checkEmptyFields(listInputedFieldsData);
		boolean isValidManufactureDate = checkDate("error.drug.distribution.validator.field.number.1",
														listInputedFieldsData.get(0));
		boolean isValidExpiryDate = checkDate("error.drug.distribution.validator.field.number.2",
														listInputedFieldsData.get(1));
		if (isValidManufactureDate && isValidExpiryDate) {
			checkManufactureAndExpiryDate(listInputedFieldsData.get(0), listInputedFieldsData.get(1));
		}
		checkDrugQuantity(listInputedFieldsData);
		checkDrugPrice(listInputedFieldsData);
		LOGGER.log(Level.DEBUG, "validationResult={}", validationResult);
		return validationResult;
	}

	private void checkManufactureAndExpiryDate(String stringManufactureDate, String stringExpiryDate) {
		LocalDate manufactureDate = LocalDate.parse(stringManufactureDate);
		LocalDate expiryDate = LocalDate.parse(stringExpiryDate);
		if (expiryDate.isBefore(manufactureDate)) {
			validationResult.addMessage(new ValidationMessage("error.drug.distribution.validator.field.number.1",
																"error.drug.distribution.validator.manufacture.and.expiry.date.message"));
		}
	}

	private void checkEmptyFields(List<String> listInputedFieldsData) {
		for (int fieldNumber = 0; fieldNumber < listInputedFieldsData.size(); fieldNumber++) {
			if (listInputedFieldsData.get(fieldNumber).isEmpty()) {
				validationResult.addMessage(
						new ValidationMessage("error.drug.distribution.validator.field.number." + (fieldNumber + 1),
												"error.validator.field.value.empty"));
			}
		}
	}

	private boolean checkDate(String nameField, String date) {
		try {
			LocalDate.parse(date);
		} catch (DateTimeParseException exception) {
			validationResult.addMessage(new ValidationMessage(nameField, "error.validator.field.value.not.valid"));
			return false;
		}
		return true;
	}

	private void checkDrugQuantity(List<String> listInputedFieldsData) {
		try {
			int drugQuantity = Integer.parseInt(listInputedFieldsData.get(2));
			if (drugQuantity <= 0) {
				validationResult.addMessage(new ValidationMessage("error.drug.distribution.validator.field.number.3",
																	"error.validator.field.value.positive.integer.number"));
			}
		} catch (NumberFormatException exception) {
			validationResult.addMessage(new ValidationMessage("error.drug.distribution.validator.field.number.3",
																"error.validator.field.value.positive.integer.number"));
		}
	}

	private void checkDrugPrice(List<String> listInputedFieldsData) {
		try {
			BigDecimal drugPrice = new BigDecimal(listInputedFieldsData.get(3));
			if (drugPrice.compareTo(BigDecimal.ZERO) <= 0) {
				validationResult.addMessage(new ValidationMessage("error.drug.distribution.validator.field.number.4",
																	"error.validator.field.value.positive.number"));
			}
		} catch (NumberFormatException exception) {
			validationResult.addMessage(new ValidationMessage("error.drug.distribution.validator.field.number.4",
																"error.validator.field.value.positive.number"));
		}
	}

}
