package by.baranov.drugdistribution.service.impl;

import java.util.List;

import by.baranov.drugdistribution.connectionpool.Transactional;
import by.baranov.drugdistribution.dao.DrugFormDao;
import by.baranov.drugdistribution.entity.DrugForm;
import by.baranov.drugdistribution.exception.DaoException;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DrugFormService;

public class DrugFormServiceImpl implements DrugFormService{
	private final DrugFormDao drugFormDao;

	public DrugFormServiceImpl(DrugFormDao drugFormDao) {
		this.drugFormDao = drugFormDao;
	}

	@Override
	public List<DrugForm> getDrugFormsByDrugId(Long drugId) throws ServiceException {
		try {
			return drugFormDao.getDrugFormsByDrugId(drugId);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	@Transactional
	@Override
	public void assignDrugFormForDrug(Long drugId, DrugForm drugForm) throws ServiceException {
		try {
			
			List<DrugForm> drugForms = drugFormDao.getAll();
			boolean isDrugFormNotExist = drugForms.stream()
					.noneMatch(drugFormFromDataBase -> drugFormFromDataBase.equals(drugForm));
			if (isDrugFormNotExist) {
				drugFormDao.save(drugForm);
				drugFormDao.assignDrugFormForDrug(drugId, drugForm.getId());
			} else {
				List<DrugForm> drugFormsForDrugInDataBase = drugFormDao.getDrugFormsByDrugId(drugId);
				boolean isDrugFormNotExistForDrug = drugFormsForDrugInDataBase.stream()
						.noneMatch(drugFormFromDataBase -> drugFormFromDataBase.equals(drugForm));
				if(isDrugFormNotExistForDrug) {
					Long drugFormId = drugForms.stream()
												.filter(drugFormFromDataBase -> drugFormFromDataBase.equals(drugForm))
												.map(DrugForm::getId)
												.findAny().orElseThrow(NullPointerException::new);
					drugFormDao.assignDrugFormForDrug(drugId, drugFormId);
				} else {
					throw new ServiceException("error.drug.form.service.add.drug.form.message");
				}
			}
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
	}

	@Transactional
	@Override
	public void deleteDrugFormForDrug(Long drugId, Long drugFormId) throws ServiceException {
		try {
			drugFormDao.deleteDrugFormForDrug(drugId, drugFormId);
		} catch (DaoException exception) {
			throw new ServiceException(exception.getMessage(), exception);
		}
		
	}
}
