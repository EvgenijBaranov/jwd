package by.baranov.drugdistribution.command.driverinvoice;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.DriverInvoice;
import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DriverInvoiceService;

public class DeleteDriverInvoiceCommand implements Command{
	private static final String ATTRIBUTE_NAME_FOR_DRIVER_INVOICES = "driverInvoices";
	private static final Logger LOGGER = LogManager.getLogger();
	private final DriverInvoiceService driverInvoiceService;

	public DeleteDriverInvoiceCommand(DriverInvoiceService driverInvoiceService) {
		this.driverInvoiceService = driverInvoiceService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String inputedDriverInvoiceId = request.getParameter(AppConstants.PARAMETER_DRIVER_INVOICE_ID).trim();
		try {
			Long driverInvoiceId = Long.valueOf(inputedDriverInvoiceId);
			driverInvoiceService.delete(driverInvoiceId);
			
			User driver = (User) request.getSession().getAttribute(AppConstants.ATTRIBUTE_NAME_REGISTERED_USER);
			List<DriverInvoice> driverInvoices = driverInvoiceService.getAvailableInvoicesForDriver(driver);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_DRIVER_INVOICES, driverInvoices);
		} catch (ServiceException exception) {
			request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
			LOGGER.log(Level.ERROR, "Driver invoice deleting error", exception);
		}
		LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
				getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_DRIVER_INVOICE_COMMANDS);
		return TypeCommandAction.REDIRECT + AppConstants.FILE_PATH_FOR_DRIVER_INVOICE_COMMANDS;
	}
}
