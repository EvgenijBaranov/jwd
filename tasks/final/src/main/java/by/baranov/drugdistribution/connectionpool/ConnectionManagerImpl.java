package by.baranov.drugdistribution.connectionpool;

import java.sql.Connection;

public class ConnectionManagerImpl implements ConnectionManager {

	private final ConnectionPool connectionPool;
	private final TransactionManager transactionManager;

	public ConnectionManagerImpl(ConnectionPool connectionPool, TransactionManager transactionManager) {
		this.connectionPool = connectionPool;
		this.transactionManager = transactionManager;
	}

	@Override
	public Connection getConnection() {
		Connection connection = transactionManager.getConnection();
		return connection == null ? connectionPool.getConnection() : connection;
	}

}
