package by.baranov.drugdistribution.command.drugform;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.Drug;
import by.baranov.drugdistribution.entity.DrugForm;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DrugFormService;
import by.baranov.drugdistribution.service.DrugService;

public class ViewAllDrugFormsForDrugCommand implements Command{
	private static final String ATTRIBUTE_NAME_FOR_DRUG_FORMS = "drugFormsForDrug";
	private static final String ATTRIBUTE_NAME_FOR_DRUG = "drug";
	private static final String ATTRIBUTE_VALUE_FOR_VIEW = "viewDrugFormsForDrug";
	private static final Logger LOGGER = LogManager.getLogger();
	private final DrugFormService drugFormService;
	private final DrugService drugService;

	public ViewAllDrugFormsForDrugCommand(DrugFormService drugFormService, DrugService drugService) {
		this.drugFormService = drugFormService;
		this.drugService = drugService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String inputedDrugId = request.getParameter(AppConstants.PARAMETER_DRUG_ID).trim();
		try {
			Long drugId = Long.valueOf(inputedDrugId);
			
			List<DrugForm> drugForms = drugFormService.getDrugFormsByDrugId(drugId);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_DRUG_FORMS, drugForms);
			
			Drug drug = drugService.getById(drugId);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_DRUG, drug);
		} catch (ServiceException exception) {
			request.setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
			LOGGER.log(Level.ERROR, "Error of view all drug forms for drug with id= " + inputedDrugId + ": ", exception);
		} 
		request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_VIEW, ATTRIBUTE_VALUE_FOR_VIEW);
		LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
				getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_DRUG_COMMANDS);
		return TypeCommandAction.FORWARD + AppConstants.FILE_PATH_FOR_DRUG_COMMANDS;
	}
}
