package by.baranov.drugdistribution.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class PharmacyHasDrug implements Serializable{
	
	private static final long serialVersionUID = 600222743100893255L;
	private Long pharmacyId;
	private Long drugId;
	private int drugQuantity;
	private LocalDate manufactureDate;
	private LocalDate expiryDate;
	private BigDecimal price;
	
	public PharmacyHasDrug() {}
	
	public PharmacyHasDrug(Long pharmacyId, Long drugId, int drugQuantity, 
			LocalDate manufactureDate, LocalDate expiryDate, BigDecimal price) {
		this.pharmacyId = pharmacyId;
		this.drugId = drugId;
		this.drugQuantity = drugQuantity;
		this.manufactureDate = manufactureDate;
		this.expiryDate = expiryDate;
		this.price = price;
	}

	public Long getPharmacyId() {
		return pharmacyId;
	}

	public void setPharmacyId(Long pharmacyId) {
		this.pharmacyId = pharmacyId;
	}

	public Long getDrugId() {
		return drugId;
	}

	public void setDrugId(Long drugId) {
		this.drugId = drugId;
	}

	public int getDrugQuantity() {
		return drugQuantity;
	}

	public void setDrugQuantity(int drugQuantity) {
		this.drugQuantity = drugQuantity;
	}

	public LocalDate getManufactureDate() {
		return manufactureDate;
	}

	public void setManufactureDate(LocalDate manufactureDate) {
		this.manufactureDate = manufactureDate;
	}

	public LocalDate getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(LocalDate expiryDate) {
		this.expiryDate = expiryDate;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Override
	public int hashCode() {
		return Objects.hash(drugId, expiryDate, manufactureDate, pharmacyId, price);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof PharmacyHasDrug))
			return false;
		PharmacyHasDrug other = (PharmacyHasDrug) obj;
		return Objects.equals(drugId, other.drugId)
				&& Objects.equals(expiryDate, other.expiryDate)
				&& Objects.equals(manufactureDate, other.manufactureDate)
				&& Objects.equals(pharmacyId, other.pharmacyId) 
				&&(price.compareTo(other.price) == 0);
	}

	@Override
	public String toString() {
		return "PharmacyHasDrug [pharmacyId=" + pharmacyId + ", drugId=" + drugId + ", drugQuantity=" + drugQuantity
				+ ", manufactureDate=" + manufactureDate + ", expiryDate=" + expiryDate + ", price=" + price + "]";
	}
	

}
