package by.baranov.drugdistribution.service;

import java.util.List;

import by.baranov.drugdistribution.entity.DrugOrder;
import by.baranov.drugdistribution.exception.ServiceException;

public interface DrugOrderService {
	
	DrugOrder getById(Long id) throws ServiceException;	
	void save(DrugOrder drugOrder) throws ServiceException;
	void update(Long drugOrderId, int drugQuantity) throws ServiceException;
	void delete (Long id) throws ServiceException;
	
	List<DrugOrder> getDrugOrdersForPharmaciesByPharmacistId(Long pharmacistId) throws ServiceException;
	
	void confirmDrugOrderDelivery(Long drugOrderId) throws ServiceException; 
}
