package by.baranov.drugdistribution.command;

import java.util.EnumMap;
import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CommandRegistryImpl implements CommandRegistry {
	private final Map<CommandName, Command> commandMap = new EnumMap<>(CommandName.class);
	private static final Logger LOGGER = LogManager.getLogger();

	@Override
	public void register(CommandName commandName, Command command) {
		commandMap.put(commandName, command);
	}

	@Override
	public void remove(CommandName commandName) {
		commandMap.remove(commandName);
	}

	@Override
	public Command getCommand(CommandName commandName) {
		Command command = commandMap.getOrDefault(commandName, new DefaultCommand());
		LOGGER.log(Level.INFO, "The Command from CommandsRegistrationImpl using commandName={} is {}",
				commandName, command.getClass().getSimpleName());
		return command;
	}

}
