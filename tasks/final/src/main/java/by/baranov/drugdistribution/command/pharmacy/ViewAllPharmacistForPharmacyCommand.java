package by.baranov.drugdistribution.command.pharmacy;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.Pharmacy;
import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.entity.UserRole;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.PharmacyService;
import by.baranov.drugdistribution.service.UserService;

public class ViewAllPharmacistForPharmacyCommand implements Command{
	
	private static final String ATTRIBUTE_NAME_FOR_PHARMACISTS = "pharmacistsForPharmacy";
	private static final String ATTRIBUTE_NAME_FOR_ALL_PHARMACISTS = "pharmacistsInApplication";
	private static final String ATTRIBUTE_NAME_FOR_PHARMACY = "pharmacy";
	private static final String ATTRIBUTE_VALUE_FOR_VIEW = "viewPharmacistsForPharmacy";
	private static final Logger LOGGER = LogManager.getLogger();
	private final PharmacyService pharmacyService;
	private final UserService userService;

	public ViewAllPharmacistForPharmacyCommand(PharmacyService pharmacyService, UserService userService) {
		this.pharmacyService = pharmacyService;
		this.userService = userService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String inputedPharmacyId = request.getParameter(AppConstants.PARAMETER_PHARMACY_ID).trim();
		try {
			Long pharmacyId = Long.valueOf(inputedPharmacyId);
			
			Pharmacy pharmacy = pharmacyService.getById(pharmacyId);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_PHARMACY, pharmacy);

			List<User> pharmacistsInPharmacy = userService.getPharmacistsByPharmacyId(pharmacyId);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_PHARMACISTS, pharmacistsInPharmacy);
			
			List<User> pharmacistsInApplication = userService.getAllUsersByUserRole(UserRole.PHARMACIST);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_ALL_PHARMACISTS, pharmacistsInApplication);
		} catch (ServiceException exception) {
			request.setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
			LOGGER.log(Level.ERROR, "Error of view all user roles for user with id=" + inputedPharmacyId, exception);
		} 
		request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_VIEW, ATTRIBUTE_VALUE_FOR_VIEW);
		LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
				getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_PHARMACY_COMMANDS);
		return TypeCommandAction.FORWARD + AppConstants.FILE_PATH_FOR_PHARMACY_COMMANDS;
	}

}
