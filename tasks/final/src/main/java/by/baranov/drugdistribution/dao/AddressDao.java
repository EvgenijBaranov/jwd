package by.baranov.drugdistribution.dao;

import by.baranov.drugdistribution.entity.Address;

public interface AddressDao extends CRUDOperation<Long, Address>{
	
}
