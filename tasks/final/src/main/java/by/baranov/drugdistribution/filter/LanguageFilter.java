package by.baranov.drugdistribution.filter;

import java.io.IOException;
import java.util.Optional;
import java.util.stream.Stream;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter("/*")
public class LanguageFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException { }

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		if (request instanceof HttpServletRequest) {

            String lang = request.getParameter("lang");
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            if (("en".equalsIgnoreCase(lang) || "ru".equalsIgnoreCase(lang))) {
                Cookie langCookie = new Cookie("lang", lang);
                langCookie.setPath(httpRequest.getContextPath());
                httpRequest.setAttribute("lang", lang);
                ((HttpServletResponse) response).addCookie(langCookie);
            } else {
                Optional<Cookie[]> cookies = Optional.ofNullable(httpRequest.getCookies());
                Cookie langCookie = cookies.map(Stream::of)
					                		.orElse(Stream.empty())
					                        .filter(cookie -> cookie.getName().equalsIgnoreCase("lang")).findFirst()
					                        .orElse(new Cookie("lang", "en"));
                langCookie.setPath(httpRequest.getContextPath());
                httpRequest.setAttribute("lang", lang);
                ((HttpServletResponse) response).addCookie(langCookie);
            }

        }
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() { }

}
