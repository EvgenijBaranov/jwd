package by.baranov.drugdistribution.command.driverinvoice;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.Address;
import by.baranov.drugdistribution.entity.DriverInvoice;
import by.baranov.drugdistribution.entity.Drug;
import by.baranov.drugdistribution.entity.DrugOrder;
import by.baranov.drugdistribution.entity.Pharmacy;
import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.AddressService;
import by.baranov.drugdistribution.service.DriverInvoiceService;
import by.baranov.drugdistribution.service.DrugOrderService;
import by.baranov.drugdistribution.service.DrugService;
import by.baranov.drugdistribution.service.PharmacyService;

public class ViewAllDriverInvoicesCommand implements Command{
	private static final String ATTRIBUTE_NAME_FOR_DRIVER_INVOICES = "driverInvoices";
	private static final String ATTRIBUTE_NAME_FOR_DRUG_ORDERS = "drugOrders";
	private static final String ATTRIBUTE_NAME_FOR_PHARMACIES = "pharmacies";
	private static final String ATTRIBUTE_NAME_FOR_DRUGS = "drugs";
	private static final String ATTRIBUTE_NAME_FOR_DRIVER = "driver";
	private static final String ATTRIBUTE_NAME_FOR_ADDRESSES = "addresses";
	private static final String ATTRIBUTE_VALUE_FOR_VIEW = "viewAllDriverInvoices";
	private static final Logger LOGGER = LogManager.getLogger();

	private final DriverInvoiceService driverInvoiceService;
	private final DrugOrderService drugOrderService;
	private final PharmacyService pharmacyService;
	private final DrugService drugService;
	private final AddressService addressService;

	public ViewAllDriverInvoicesCommand(DriverInvoiceService driverInvoiceService, DrugOrderService drugOrderService, 
									PharmacyService pharmacyService, DrugService drugService, AddressService addressService) {
		this.driverInvoiceService = driverInvoiceService;
		this.drugOrderService = drugOrderService;
		this.pharmacyService = pharmacyService;
		this.drugService = drugService;
		this.addressService = addressService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		try {
			
			User driver = (User) request.getSession().getAttribute(AppConstants.ATTRIBUTE_NAME_REGISTERED_USER);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_DRIVER, driver);

			List<DriverInvoice> driverInvoices = driverInvoiceService.getAvailableInvoicesForDriver(driver);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_DRIVER_INVOICES, driverInvoices);
			
			Set<DrugOrder> drugOrders = new HashSet<>();
			Set<Pharmacy> pharmacies = new HashSet<>();
			Set<Drug> drugs =  new HashSet<>();
			Set<Address> addresses =  new HashSet<>();
			
			for(DriverInvoice driverInvoice : driverInvoices) {
				DrugOrder drugOrder = drugOrderService.getById(driverInvoice.getDrugOrderId());
				drugOrders.add(drugOrder);
				
				Pharmacy pharmacy = pharmacyService.getById(drugOrder.getPharmacyId());
				pharmacies.add(pharmacy);

				Address address = addressService.getById(pharmacy.getAddressId());
				addresses.add(address);
				
				Drug drug = drugService.getById(drugOrder.getDrugId());
				drugs.add(drug);
			}
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_DRUG_ORDERS, drugOrders);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_PHARMACIES, pharmacies);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_ADDRESSES, addresses);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_DRUGS, drugs);
		} catch (ServiceException exception) {
			request.setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
			LOGGER.log(Level.ERROR, "Error of view all driver invoices", exception);
		}
		request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_VIEW, ATTRIBUTE_VALUE_FOR_VIEW);
		LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
				getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_DRIVER_INVOICE_COMMANDS);
		return TypeCommandAction.FORWARD + AppConstants.FILE_PATH_FOR_DRIVER_INVOICE_COMMANDS;
	}
}
