package by.baranov.drugdistribution.command.driverinvoice;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.DriverInvoice;
import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DriverInvoiceService;

public class AcceptDriverInvoiceCommand implements Command{
	private static final String ATTRIBUTE_NAME_FOR_DRIVER_INVOICES = "driverInvoices";
	private static final Logger LOGGER = LogManager.getLogger();
	private final DriverInvoiceService driverInvoiceService;
	
	public AcceptDriverInvoiceCommand(DriverInvoiceService driverInvoiceService) {
		this.driverInvoiceService = driverInvoiceService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		String inputedDriverInvoiceId = request.getParameter(AppConstants.PARAMETER_DRIVER_INVOICE_ID).trim();
		User driver = (User) request.getSession().getAttribute(AppConstants.ATTRIBUTE_NAME_REGISTERED_USER);
		
		try {
			Long driverInvoiceId = Long.valueOf(inputedDriverInvoiceId);
			driverInvoiceService.acceptDriverInvoiceByDriver(driverInvoiceId, driver);
			
			List<DriverInvoice> driverInvoices = driverInvoiceService.getAvailableInvoicesForDriver(driver);
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_DRIVER_INVOICES, driverInvoices);
		} catch (ServiceException exception) {
			request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
			LOGGER.log(Level.ERROR, "Driver invoice accept error", exception);
		} 
		LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
				getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_DRIVER_INVOICE_COMMANDS);
		return TypeCommandAction.REDIRECT + AppConstants.FILE_PATH_FOR_DRIVER_INVOICE_COMMANDS;
	}
	
}
