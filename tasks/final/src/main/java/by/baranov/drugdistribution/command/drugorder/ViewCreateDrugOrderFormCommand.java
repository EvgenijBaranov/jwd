package by.baranov.drugdistribution.command.drugorder;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.drugdistribution.AppConstants;
import by.baranov.drugdistribution.command.Command;
import by.baranov.drugdistribution.command.TypeCommandAction;
import by.baranov.drugdistribution.entity.Drug;
import by.baranov.drugdistribution.entity.Pharmacy;
import by.baranov.drugdistribution.entity.User;
import by.baranov.drugdistribution.exception.ServiceException;
import by.baranov.drugdistribution.service.DrugService;
import by.baranov.drugdistribution.service.PharmacyService;

public class ViewCreateDrugOrderFormCommand implements Command {

	private static final String ATTRIBUTE_NAME_FOR_PHARMACIES = "pharmaciesForPharmacist";
	private static final String ATTRIBUTE_NAME_FOR_DRUGS = "drugs";
	private static final String ATTRIBUTE_VALUE_FOR_VIEW = "createDrugOrderForm";
	private static final Logger LOGGER = LogManager.getLogger();
	
	private final PharmacyService pharmacyService;
	private final DrugService drugService;

	public ViewCreateDrugOrderFormCommand(PharmacyService pharmacyService, DrugService drugService) {
		this.pharmacyService = pharmacyService;
		this.drugService = drugService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		try {
			User registeredUser = (User) request.getSession().getAttribute(AppConstants.ATTRIBUTE_NAME_REGISTERED_USER);
			List<Pharmacy> pharmaciesForPharmacist = pharmacyService.getPharmaciesByParmacistId(registeredUser.getId());
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_PHARMACIES, pharmaciesForPharmacist);
			
			List<Drug> drugs = drugService.getAll();
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_DRUGS, drugs);
		} catch (ServiceException exception) {
			request.setAttribute(AppConstants.ATTRIBUTE_NAME_ERROR, exception.getMessage());
			LOGGER.log(Level.ERROR, "Error: can't view form for creation drug order", exception);
		}
		request.getSession().setAttribute(AppConstants.ATTRIBUTE_NAME_VIEW, ATTRIBUTE_VALUE_FOR_VIEW);
		LOGGER.log(Level.INFO, "{} was executed. Page path for controller is {}",
				getClass().getSimpleName(), AppConstants.FILE_PATH_FOR_DRUG_ORDER_COMMANDS);
		return TypeCommandAction.FORWARD + AppConstants.FILE_PATH_FOR_DRUG_ORDER_COMMANDS;
	}

}
