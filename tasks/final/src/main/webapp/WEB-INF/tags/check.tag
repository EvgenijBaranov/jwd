<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:directive.attribute name="commandName" required="true"
	type="by.baranov.drugdistribution.command.CommandName" description="Command name to execute" />

<jsp:useBean id="securityContext" scope="application" type="by.baranov.drugdistribution.SecurityContext" />

<c:if
	test="${securityContext.canExecute(commandName)}">
	<jsp:doBody/>
</c:if>