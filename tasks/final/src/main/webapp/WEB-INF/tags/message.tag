<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:directive.attribute name="resultMessage" type="java.util.List"
	required="true" description="Message about drugs distribution"/>

<c:if test="${not empty resultMessage}">
	<div class="container">
		<div class="alert alert-success" role="alert">
			<p>
				<c:choose>
					<c:when test="${resultMessage.size() == 1}">
						<fmt:message key="${resultMessage.get(0)}"/>
					</c:when>
					<c:otherwise>
	                       ${resultMessage.get(0)} - <fmt:message key="${resultMessage.get(1)}"/>
					</c:otherwise>
				</c:choose>
			</p>
		</div>
	</div>
	<c:remove var="resultMessage"/>
</c:if>