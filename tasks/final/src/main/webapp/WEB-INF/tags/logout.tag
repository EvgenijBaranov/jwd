<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<form action="userLogout" method="GET" accept-charset="utf-8">
        <div>
            <input type="hidden" name="commandName" value="USER_LOGOUT">
            <button type="submit" class="btn btn-primary"><fmt:message key="user.logout.button"/></button>
        </div>
</form>