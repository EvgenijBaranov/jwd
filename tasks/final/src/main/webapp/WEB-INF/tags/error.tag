<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:directive.attribute name="errorMessage" type="java.lang.String" required="true" description="Error message" />

<c:if test="${not empty errorMessage}">
	<div class="container">
		<div class="alert alert-danger" role="alert">
			<p>
				<strong><fmt:message key="error"/>!</strong> <br/> 
				<fmt:message key="${errorMessage}"/>
			</p>
		</div>
	</div>
	<c:remove var="errorMessage"/>
</c:if>

