<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:directive.attribute name="validationResult" type="by.baranov.drugdistribution.validation.ValidationResult" 
                                                            required="true" description="Error fields validation"/>

<c:if test="${not empty validationResult}">
	<div class="container">
		<div class="alert alert-danger" role="alert">
			<p>
				<strong><fmt:message key="error"/>!</strong> <br/>
				<c:forEach var="validationMessage" items="${validationResult.getValidationResult()}">
					<fmt:message key="${validationMessage.field}"/> - <fmt:message key="${validationMessage.errorMessage}"/> <br/>
				</c:forEach>
			</p>
		</div>
	</div>
	<c:remove var="validationResult"/>
</c:if>