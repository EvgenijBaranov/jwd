<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.baranov.drugdistribution.command.CommandName" %>
<%@ page import="by.baranov.drugdistribution.AppConstants" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<html lang="ru">

<head>
	<title>Driver invoice page</title>
	<tag:meta/>
</head>

<style>
table, th, td {
	text-align: center;
}

caption {
	caption-side: top;
}
</style>

<body>

	<div class="jumbotron text-center" style="margin-bottom: 0">
		<h1>
			<fmt:message key="driver.invoice.page.title" />
		</h1>
	</div>

	<div class="container" style="margin-top: 30px">
		<ul class="nav nav-tabs" role="tablist">
			<li class="nav-item">
			    <a class="nav-link active" data-toggle="tab" href="${pageContext.request.contextPath}/jsp/driverInvoice/driverInvoicePage.jsp">
			        <fmt:message key="main.page.tab.driver.invoices"/>
			    </a>
			</li>
		</ul>
		<div class="tab-content">
			<div id="<fmt:message key="main.page.user.tab.driver.invoices"/>" class="container tab-pane active">
				<br>
				<div class="row">
					<div class="col-2">
						<ul class="nav nav-pills flex-column">
							<tag:check commandName="${CommandName.VIEW_ALL_DRIVER_INVOICES}">
								<li>
									<form action="viewAllDriverInvoices" method="GET" accept-charset="utf-8">
										<input type="hidden" name="${AppConstants.PARAMETER_COMMAND_NAME}" value="${CommandName.VIEW_ALL_DRIVER_INVOICES}">
										<button type="submit" class="btn btn-info"><fmt:message key="driver.invoice.page.button.command.view.all.driver.invoices"/></button>
									</form>
								</li>
							</tag:check>
							<tag:main/>
						</ul>
						<br/>
						<tag:logout/>
					</div>
					<div class="col-10">
						<tag:error errorMessage="${errorMessage}"/>
						<jsp:include page="views/${view}.jsp"/>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>