<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.baranov.drugdistribution.command.CommandName" %>
<%@ page import="by.baranov.drugdistribution.AppConstants" %>
<%@ taglib prefix="dtg" uri="/WEB-INF/tld/dates.tld" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<div class="container">
	<tag:check commandName="${CommandName.VIEW_ALL_DRIVER_INVOICES}">
		<table class="table table-bordered">
			<thead class="thead-light">
				<tr>
					<th colspan="3"><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.driver.invoice.title"/></th>
					<th colspan="2"><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.drug.order.title"/></th>
					<th colspan="3"><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.driver.title"/></th>
					<th colspan="6"><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.pharmacy.title"/></th>
					<th colspan="5"><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.drug.title"/></th>
				</tr>
				<tr>
					<th><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.driver.invoice.id"/></th>
					<th><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.driver.invoice.departure.date"/></th>
					<th><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.driver.invoice.delivery.date"/></th>
					<th><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.drug.order.id"/></th>
					<th><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.drug.order.creation.date"/></th>
					<th><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.driver.id"/></th>
					<th><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.driver.name"/></th>
					<th><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.driver.surname"/></th>
					<th><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.pharmacy.id"/></th>
					<th><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.pharmacy.name"/></th>
					<th><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.pharmacy.address.country"/></th>
					<th><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.pharmacy.address.town"/></th>
					<th><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.pharmacy.address.street"/></th>
					<th><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.pharmacy.address.house"/></th>
					<th><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.drug.id"/></th>
					<th><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.drug.name"/></th>
					<th><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.drug.dosage"/></th>
					<th><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.drug.order.drug.quantity"/></th>
					<th><fmt:message key="driver.invoice.page.view.all.driver.invoices.table.drug.order.drug.price"/></th>
				</tr>
			</thead>

			<tbody>
				<c:forEach var="driverInvoice" items="${driverInvoices}">
					<tr>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-light"><c:out value="${driverInvoice.id}"/></button>
								<button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
									<span class="caret"></span>
								</button>
								<div class="dropdown-menu">
									<tag:check commandName="${CommandName.ACCEPT_DRIVER_INVOICE}">
										<form action="${pageContext.request.contextPath}/jsp/driverInvoice/acceptDriverInvoice" method="POST" accept-charset="utf-8">
                                             <input type="hidden" name="${AppConstants.PARAMETER_COMMAND_NAME}" value="${CommandName.ACCEPT_DRIVER_INVOICE}">
                                             <input type="hidden" name="${AppConstants.PARAMETER_DRIVER_INVOICE_ID}" value="${driverInvoice.id}">
                                             <button type="submit" class="dropdown-item"><fmt:message key="driver.invoice.page.view.all.driver.invoices.link.accept.driver.invoice"/></button>
                                        </form>
									</tag:check>
									<tag:check commandName="${CommandName.DELETE_DRIVER_INVOICE}">
										<form action="${pageContext.request.contextPath}/jsp/driverInvoice/deleteDriverInvoice" method="POST" accept-charset="utf-8">
                                             <input type="hidden" name="${AppConstants.PARAMETER_COMMAND_NAME}" value="${CommandName.DELETE_DRIVER_INVOICE}">
                                             <input type="hidden" name="${AppConstants.PARAMETER_DRIVER_INVOICE_ID}" value="${driverInvoice.id}">
                                             <button type="submit" class="dropdown-item"><fmt:message key="driver.invoice.page.view.all.driver.invoices.link.delete.driver.invoice"/></button>
                                        </form>
									</tag:check>
								</div>
							</div>
						</td>
						<td>
						    <c:if test="${not empty driverInvoice.departureDate}">
								<c:out value="${dtg:formatDate(driverInvoice.departureDate, 'yyyy-MM-dd HH:mm:ss')}"/>
							</c:if>
						</td>
						<td>
						    <c:if test="${not empty driverInvoice.deliveryDate}">
								<c:out value="${dtg:formatDate(driverInvoice.deliveryDate, 'yyyy-MM-dd HH:mm:ss')}"/>
							</c:if>
						</td>
						<c:forEach var="drugOrder" items="${drugOrders}">
							<c:if test="${drugOrder.id eq driverInvoice.drugOrderId}">
								<td><c:out value="${drugOrder.id}" /></td>
								<td><c:out value="${dtg:formatDate(drugOrder.creationDate, 'yyyy-MM-dd HH:mm:ss')}"/></td>
							</c:if>
						</c:forEach>
						<c:choose>
							<c:when test="${not empty driverInvoice.driverId}">
								<td><c:out value="${driver.id}"/></td>
								<td><c:out value="${driver.name}"/></td>
								<td><c:out value="${driver.surname}"/></td>								
							</c:when>
							<c:otherwise>
								<td/>
								<td/>
								<td/>
							</c:otherwise>
						</c:choose>
						<c:forEach var="drugOrder" items="${drugOrders}">
							<c:if test="${drugOrder.id eq driverInvoice.drugOrderId}">
								<c:forEach var="pharmacy" items="${pharmacies}">
									<c:if test="${pharmacy.id eq drugOrder.pharmacyId}">
										<td><c:out value="${pharmacy.id}"/></td>
										<td><c:out value="${pharmacy.name}"/></td>
										<c:forEach var="address" items="${addresses}">
											<c:if test="${address.id eq pharmacy.addressId}">
												<td><c:out value="${address.country}"/></td>
												<td><c:out value="${address.town}"/></td>
												<td><c:out value="${address.street}"/></td>
												<td><c:out value="${address.house}"/></td>
											</c:if>
										</c:forEach>
									</c:if>
								</c:forEach>
								<c:forEach var="drug" items="${drugs}">
									<c:if test="${drug.id eq drugOrder.drugId}">
										<td><c:out value="${drug.id}"/></td>
										<td><c:out value="${drug.name}"/></td>
										<td><c:out value="${drug.dosage}"/></td>
									</c:if>
								</c:forEach>
								<td><c:out value="${drugOrder.drugQuantity}"/></td>
							</c:if>
						</c:forEach>
						<td><c:out value="${driverInvoice.drugPrice}"/></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</tag:check>
</div>