<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ page import="by.baranov.drugdistribution.command.CommandName" %>
<%@ page import="by.baranov.drugdistribution.AppConstants" %>

<div class="container">
	<tag:check commandName="${CommandName.CREATE_DRUG_ORDER}">
		<h2>
			<fmt:message key="drug.order.page.create.drug.order.form.greeting" />
		</h2>
		<form action="createDrugOrder" method="POST" accept-charset="utf-8">
			<div class="form-group">
				<label for="choosePharmacy"><fmt:message key="drug.order.page.create.drug.order.form.pharmacy.label"/></label> 
				<select class="form-control" id="choosePharmacy" name="${AppConstants.PARAMETER_PHARMACY_ID}">
					<c:forEach var="pharmacy" items="${pharmaciesForPharmacist}">
						<option value="${pharmacy.id}">
						    <fmt:message key="drug.order.page.create.drug.order.form.pharmacy.name"/>=${pharmacy.name}, 
						    <fmt:message key="drug.order.page.create.drug.order.form.pharmacy.licence.number"/>=${pharmacy.licenceNumber}
						</option>
					</c:forEach>
				</select>
			</div>
			<div class="form-group">
				<label for="chooseDrug"><fmt:message key="drug.order.page.create.drug.order.form.drug.label"/></label> 
				<select class="form-control" id="chooseDrug" name="${AppConstants.PARAMETER_DRUG_ID}">
					<c:forEach var="drug" items="${drugs}">
						<option value="${drug.id}">
						    <fmt:message key="drug.order.page.create.drug.order.form.drug.name"/>=${drug.name}, 
						    <fmt:message key="drug.order.page.create.drug.order.form.drug.dosage"/>=${drug.dosage}, 
						    <fmt:message key="drug.order.page.create.drug.order.form.drug.availability"/>=${drug.availability}
						</option>
					</c:forEach>
				</select>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_DRUG_QUANTITY}"><fmt:message key="drug.order.page.create.drug.order.form.drug.quantity.legend"/></label>
				<input id="${AppConstants.PARAMETER_DRUG_QUANTITY}" type="text" class="form-control"
					placeholder="<fmt:message key="drug.order.page.create.drug.order.form.drug.quantity.legend.input"/>" name="${AppConstants.PARAMETER_DRUG_QUANTITY}"/>
			</div>
			<input type="hidden" name="${AppConstants.PARAMETER_COMMAND_NAME}" value="${CommandName.CREATE_DRUG_ORDER}">
			<button type="submit" class="btn btn-info"><fmt:message key="drug.order.page.create.drug.order.form.submit"/></button>
		</form>
	</tag:check>
</div>




