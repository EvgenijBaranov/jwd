<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.baranov.drugdistribution.command.CommandName" %>
<%@ page import="by.baranov.drugdistribution.AppConstants" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="dtg" uri="/WEB-INF/tld/dates.tld" %>

<div class="container">
	<tag:check commandName="${CommandName.VIEW_ALL_DRUG_ORDERS}">
		<table class="table table-bordered">
			<thead class="thead-light">
				<tr>
					<th rowspan="2"><fmt:message key="drug.order.page.view.all.drug.orders.table.drug.order.id"/></th>
					<th colspan="2"><fmt:message key="drug.order.page.view.all.drug.orders.table.pharmacy.title"/></th>
					<th colspan="3"><fmt:message key="drug.order.page.view.all.drug.orders.table.pharmacist.title"/></th>
					<th colspan="3"><fmt:message key="drug.order.page.view.all.drug.orders.table.drug.title"/></th>
					<th rowspan="2"><fmt:message key="drug.order.page.view.all.drug.orders.table.drug.quantity"/></th>
					<th rowspan="2"><fmt:message key="drug.order.page.view.all.drug.orders.table.drug.order.creation.time"/></th>
					<th rowspan="2"><fmt:message key="drug.order.page.view.all.drug.orders.table.drug.order.termination.time"/></th>
					<th rowspan="2"><fmt:message key="drug.order.page.view.all.drug.orders.table.drug.order.status"/></th>
				</tr>
				<tr>
					<th><fmt:message key="drug.order.page.view.all.drug.orders.table.pharmacy.id"/></th>
					<th><fmt:message key="drug.order.page.view.all.drug.orders.table.pharmacy.name"/></th>
					<th><fmt:message key="drug.order.page.view.all.drug.orders.table.pharmacist.id"/></th>
					<th><fmt:message key="drug.order.page.view.all.drug.orders.table.pharmacist.name"/></th>
					<th><fmt:message key="drug.order.page.view.all.drug.orders.table.pharmacist.surname"/></th>
					<th><fmt:message key="drug.order.page.view.all.drug.orders.table.drug.id"/></th>
					<th><fmt:message key="drug.order.page.view.all.drug.orders.table.drug.name"/></th>
					<th><fmt:message key="drug.order.page.view.all.drug.orders.table.drug.dosage"/></th>
				</tr>
			</thead>

			<tbody>
				<c:forEach var="drugOrder" items="${drugOrdersForPharmaciesWithPharmacist}">
					<tr>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-light"> <c:out value="${drugOrder.id}"/> </button>
								<button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
									<span class="caret"></span>
								</button>
								<div class="dropdown-menu">
									<tag:check commandName="${CommandName.CONFIRM_DRUG_ORDER_DELIVERY}">
                                        <form action="${pageContext.request.contextPath}/jsp/drugOrder/confirmDeliveryDrugOrder" method="POST" accept-charset="utf-8">
                                             <input type="hidden" name="${AppConstants.PARAMETER_COMMAND_NAME}" value="${CommandName.CONFIRM_DRUG_ORDER_DELIVERY}">
                                             <input type="hidden" name="${AppConstants.PARAMETER_DRUG_ORDER_ID}" value="${drugOrder.id}">
                                             <button type="submit" class="dropdown-item"><fmt:message key="drug.order.page.view.all.drug.orders.link.confirm.drug.order.delivery"/></button>
                                        </form>
									</tag:check>
									<tag:check commandName="${CommandName.DELETE_DRUG_ORDER}">
										<form action="${pageContext.request.contextPath}/jsp/drugOrder/deleteDrugOrder" method="POST" accept-charset="utf-8">
                                             <input type="hidden" name="${AppConstants.PARAMETER_COMMAND_NAME}" value="${CommandName.DELETE_DRUG_ORDER}">
                                             <input type="hidden" name="${AppConstants.PARAMETER_DRUG_ORDER_ID}" value="${drugOrder.id}">
                                             <button type="submit" class="dropdown-item"><fmt:message key="drug.order.page.view.all.drug.orders.link.delete.drug.order"/></button>
                                        </form>
									</tag:check>
								</div>
							</div>
						</td>
						<c:forEach var="pharmacy" items="${pharmacies}">
							<c:if test="${pharmacy.id eq drugOrder.pharmacyId}">
								<td><c:out value="${pharmacy.id}"/></td>
								<td><c:out value="${pharmacy.name}"/></td>
							</c:if>
						</c:forEach>
						<c:forEach var="pharmacist" items="${pharmacists}">
							<c:if test="${pharmacist.id eq drugOrder.pharmacistId}">
								<td><c:out value="${pharmacist.id}"/></td>
								<td><c:out value="${pharmacist.name}"/></td>
								<td><c:out value="${pharmacist.surname}"/></td>
							</c:if>
						</c:forEach>
						<c:forEach var="drug" items="${drugs}">
							<c:if test="${drug.id eq drugOrder.drugId}">
								<td><c:out value="${drug.id}"/></td>
								<td><c:out value="${drug.name}"/></td>
								<td><c:out value="${drug.dosage}"/></td>
							</c:if>
						</c:forEach>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-light"> <c:out value="${drugOrder.drugQuantity}"/> </button>
								<button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
									<span class="caret"></span>
								</button>
								<div class="dropdown-menu">
									<tag:check commandName="${CommandName.VIEW_CHANGE_DRUG_QUANTITY_FOR_DRUG_ORDER_FORM}">
										<a class="dropdown-item" href="${pageContext.request.contextPath}/jsp/drugOrder/changeDrugQyantityForDrugOrder
                                            ?${AppConstants.PARAMETER_COMMAND_NAME}=${CommandName.VIEW_CHANGE_DRUG_QUANTITY_FOR_DRUG_ORDER_FORM}
                                            &${AppConstants.PARAMETER_DRUG_ORDER_ID}=${drugOrder.id}">
                                                <fmt:message key="drug.order.page.view.all.drug.orders.link.change.drug.quantity"/>
                                        </a>
									</tag:check>
								</div>
							</div>
						</td>
						<td><c:out value="${dtg:formatDate(drugOrder.creationDate, 'yyyy-MM-dd HH:mm:ss')}"/></td>
						<td>
						    <c:if test="${not empty drugOrder.terminationDate}">
								<c:out value="${dtg:formatDate(drugOrder.terminationDate, 'yyyy-MM-dd HH:mm:ss')}"/>
							</c:if>
						</td>
						<td><c:out value="${drugOrder.orderStatus.name}"/></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</tag:check>
</div>