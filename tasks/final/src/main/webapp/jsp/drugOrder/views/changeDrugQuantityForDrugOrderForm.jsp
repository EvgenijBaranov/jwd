<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ page import="by.baranov.drugdistribution.command.CommandName" %>
<%@ page import="by.baranov.drugdistribution.AppConstants" %>

<div class="container">
	<tag:check commandName="${CommandName.CHANGE_DRUG_QUANTITY_FOR_DRUG_ORDER}">
		<h2>
			<fmt:message key="drug.order.page.change.drug.order.form.greeting"/>
		</h2>
		<form action="changeDrugQuantityForDrugOrder" method="POST" accept-charset="utf-8">
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_DRUG_ORDER_DRUG_QUANTITY}"><fmt:message key="drug.order.page.change.drug.order.quantity.form.drug.quantity.legend"/></label>
				<input id="${AppConstants.PARAMETER_DRUG_ORDER_DRUG_QUANTITY}" type="text" class="form-control"
					placeholder="<fmt:message key="drug.order.page.change.drug.order.quantity.form.drug.quantity.legend.input"/>" name="${AppConstants.PARAMETER_DRUG_ORDER_DRUG_QUANTITY}"/>
			</div>
			<input type="hidden" name="${AppConstants.PARAMETER_DRUG_ORDER_ID}" value="${drugOrder.id}"> 
			<input type="hidden" name="${AppConstants.PARAMETER_COMMAND_NAME}" value="${CommandName.CHANGE_DRUG_QUANTITY_FOR_DRUG_ORDER}">
			<button type="submit" class="btn btn-info"> <fmt:message key="drug.order.page.change.drug.order.quantity.form.submit"/> </button>
		</form>
	</tag:check>
</div>




