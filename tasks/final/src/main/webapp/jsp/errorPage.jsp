<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>Error page</title>
</head>
<body>
	<p>
		Unfortunately there are next problem in the application: 
		
		<c:out value="${pageContext.exception.message}"/>
		
	</p>
	<a href="${pageContext.request.contextPath}/jsp/userLoginPage.jsp">Home page</a>
</body>
</html>