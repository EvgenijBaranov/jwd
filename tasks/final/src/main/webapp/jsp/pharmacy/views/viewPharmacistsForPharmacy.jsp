<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.baranov.drugdistribution.command.CommandName" %>
<%@ page import="by.baranov.drugdistribution.AppConstants" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>


<div class="container">
	<tag:check commandName="${CommandName.VIEW_ALL_PHARMACIST_FOR_PHARMACY}">
		
		<fmt:message key="pharmacy.page.view.all.pharmacies.title.pharmacy.name"/>${pharmacy.name},  
        <fmt:message key="pharmacy.page.view.all.pharmacies.title.pharmacy.licenceNumber"/>${pharmacy.licenceNumber},  
        <fmt:message key="pharmacy.page.view.all.pharmacies.title.pharmacy.phone"/>${pharmacy.phone})  

		<table class="table table-bordered">
			<caption>
				<p class="font-weight-bold">
					<fmt:message key="pharmacy.page.view.pharmacists.table.pharmacist.in.pharmacy.caption"/>
				</p>
			</caption>
			<thead class="thead-light">
				<tr>
					<th><fmt:message key="pharmacy.page.view.pharmacists.table.pharmacist.id"/></th>
					<th><fmt:message key="pharmacy.page.view.pharmacists.table.pharmacist.login"/></th>
					<th><fmt:message key="pharmacy.page.view.pharmacists.table.pharmacist.name"/></th>
					<th><fmt:message key="pharmacy.page.view.pharmacists.table.pharmacist.surname"/></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="pharmacist" items="${pharmacistsForPharmacy}">
					<tr>
						<td><c:out value="${pharmacist.id}"/></td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-light"><c:out value="${pharmacist.login}"/></button>
								<button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
									<span class="caret"></span>
								</button>
								<tag:check commandName="${CommandName.DELETE_PHARMACIST_FOR_PHARMACY}">
									<div class="dropdown-menu">
	                                    <form action="${pageContext.request.contextPath}/jsp/pharmacy/deletePharmacistFromPharmacy" method="POST" accept-charset="utf-8">
                                             <input type="hidden" name="${AppConstants.PARAMETER_COMMAND_NAME}" value="${CommandName.DELETE_PHARMACIST_FOR_PHARMACY}">
                                             <input type="hidden" name="${AppConstants.PARAMETER_PHARMACY_ID}" value="${pharmacy.id}">
                                             <input type="hidden" name="${AppConstants.PARAMETER_USER_ID}" value="${pharmacist.id}">
                                             <button type="submit" class="dropdown-item"><fmt:message key="pharmacy.page.view.pharmacists.link.delete.pharmacist"/></button>
                                        </form>
									</div>
								</tag:check>
							</div>
						</td>
						<td><c:out value="${pharmacist.name}"/></td>
						<td><c:out value="${pharmacist.surname}"/></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<br />
		<table class="table table-bordered">
			<caption>
				<p class="font-weight-bold">
					<fmt:message key="pharmacy.page.view.pharmacists.table.pharmacist.in.application.caption"/>
				</p>
			</caption>
			<thead class="thead-light">
				<tr>
					<th><fmt:message key="pharmacy.page.view.pharmacists.table.pharmacist.id"/></th>
					<th><fmt:message key="pharmacy.page.view.pharmacists.table.pharmacist.login"/></th>
					<th><fmt:message key="pharmacy.page.view.pharmacists.table.pharmacist.name"/></th>
					<th><fmt:message key="pharmacy.page.view.pharmacists.table.pharmacist.surname"/></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="pharmacist" items="${pharmacistsInApplication}">
					<tr>
						<td><c:out value="${pharmacist.id}"/></td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-light"><c:out value="${pharmacist.login}"/></button>
								<button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
									<span class="caret"></span>
								</button>
								<tag:check commandName="${CommandName.ASSIGN_PHARMACIST_FOR_PHARMACY}">
									<div class="dropdown-menu">
                                        <form action="${pageContext.request.contextPath}/jsp/pharmacy/addPharmacistForPharmacy" method="POST" accept-charset="utf-8">
                                             <input type="hidden" name="${AppConstants.PARAMETER_COMMAND_NAME}" value="${CommandName.ASSIGN_PHARMACIST_FOR_PHARMACY}">
                                             <input type="hidden" name="${AppConstants.PARAMETER_PHARMACY_ID}" value="${pharmacy.id}">
                                             <input type="hidden" name="${AppConstants.PARAMETER_USER_ID}" value="${pharmacist.id}">
                                             <button type="submit" class="dropdown-item"><fmt:message key="pharmacy.page.view.pharmacists.link.add.pharmacist"/></button>
                                        </form>
									</div>
								</tag:check>
							</div>
						</td>
						<td><c:out value="${pharmacist.name}"/></td>
						<td><c:out value="${pharmacist.surname}"/></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</tag:check>

</div>