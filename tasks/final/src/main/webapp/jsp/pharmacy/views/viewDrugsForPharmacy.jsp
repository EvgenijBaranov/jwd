<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.baranov.drugdistribution.command.CommandName" %>
<%@ page import="by.baranov.drugdistribution.entity.UserRole" %>
<%@ page import="by.baranov.drugdistribution.AppConstants" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<div class="container">
	<tag:check commandName="${CommandName.VIEW_ALL_DRUGS_FOR_PHARMACY}">
	    <fmt:message key="pharmacy.page.view.all.pharmacies.title.pharmacy.name"/>${pharmacy.name},  
        <fmt:message key="pharmacy.page.view.all.pharmacies.title.pharmacy.licenceNumber"/>${pharmacy.licenceNumber},  
        <fmt:message key="pharmacy.page.view.all.pharmacies.title.pharmacy.phone"/>${pharmacy.phone}) 
	    <table class="table table-bordered">
			<caption>
				<p class="font-weight-bold">
					<fmt:message key="pharmacy.page.view.drugs.table.drug.in.pharmacy.caption"/>
				</p>
			</caption>
			<thead class="thead-light">
				<tr>
					<th><fmt:message key="pharmacy.page.view.drugs.table.drug.id"/></th>
					<th><fmt:message key="pharmacy.page.view.drugs.table.drug.name"/></th>
					<th><fmt:message key="pharmacy.page.view.drugs.table.drug.dosage"/></th>
					<th><fmt:message key="pharmacy.page.view.drugs.table.drug.availability"/></th>
					<th><fmt:message key="pharmacy.page.view.drugs.table.drug.manufacture.date"/></th>
					<th><fmt:message key="pharmacy.page.view.drugs.table.drug.expiry.date"/></th>
					<th><fmt:message key="pharmacy.page.view.drugs.table.drug.quantity"/></th>
					<th><fmt:message key="pharmacy.page.view.drugs.table.drug.price"/></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="listDrugQuantity" items="${drugsQuantityForPharmacy}">
					<c:forEach var="drugQuantity" items="${listDrugQuantity}">
						<tr>
							<c:forEach var="drug" items="${drugsForPharmacy}">
								<c:if test="${drugQuantity.drugId eq drug.id}">
									<td><c:out value="${drug.id}"/></td>
									<td><c:out value="${drug.name}"/></td>
									<td><c:out value="${drug.dosage}"/></td>
									<td><c:out value="${drug.availability}"/></td>
								</c:if>
							</c:forEach>
							<td><c:out value="${drugQuantity.manufactureDate}"/></td>
							<td><c:out value="${drugQuantity.expiryDate}"/></td>
							<td><c:out value="${drugQuantity.drugQuantity}"/></td>
							<td><c:out value="${drugQuantity.price}"/></td>
						</tr>
					</c:forEach>
				</c:forEach>
			</tbody>
		</table>
	</tag:check>
</div>