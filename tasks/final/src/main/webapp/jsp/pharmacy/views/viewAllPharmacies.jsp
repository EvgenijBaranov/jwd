<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.baranov.drugdistribution.command.CommandName" %>
<%@ page import="by.baranov.drugdistribution.AppConstants" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<div class="container">
	<tag:check commandName="${CommandName.VIEW_ALL_PHARMACIES}">
		<table class="table table-bordered">
			<thead class="thead-light">
				<tr>
					<th rowspan="2"><fmt:message key="pharmacy.page.view.all.pharmacies.table.pharmacy.id"/></th>
					<th rowspan="2"><fmt:message key="pharmacy.page.view.all.pharmacies.table.pharmacy.name"/></th>
					<th rowspan="2"><fmt:message key="pharmacy.page.view.all.pharmacies.table.pharmacy.licence.number"/></th>
					<th rowspan="2"><fmt:message key="pharmacy.page.view.all.pharmacies.table.pharmacy.phone"/></th>
					<th colspan="4"><fmt:message key="pharmacy.page.view.all.pharmacies.table.pharmacy.address.title"/></th>
				</tr>
				<tr>
					<th><fmt:message key="pharmacy.page.view.all.pharmacies.table.pharmacy.address.country"/></th>
					<th><fmt:message key="pharmacy.page.view.all.pharmacies.table.pharmacy.address.town"/></th>
					<th><fmt:message key="pharmacy.page.view.all.pharmacies.table.pharmacy.address.street"/></th>
					<th><fmt:message key="pharmacy.page.view.all.pharmacies.table.pharmacy.address.house"/></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="pharmacy" items="${pharmacies}">
					<tr>
						<td><c:out value="${pharmacy.id}"/></td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-light"><c:out value="${pharmacy.name}"/></button>
								<button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
									<span class="caret"></span>
								</button>
								<div class="dropdown-menu">
									<tag:check commandName="${CommandName.VIEW_CHANGE_PHARMACY_FORM}">
										<a class="dropdown-item" href="${pageContext.request.contextPath}/jsp/pharmacy/changePharmacy
                                            ?${AppConstants.PARAMETER_COMMAND_NAME}=${CommandName.VIEW_CHANGE_PHARMACY_FORM}
                                            &${AppConstants.PARAMETER_PHARMACY_ID}=${pharmacy.id}">
                                                <fmt:message key="pharmacy.page.view.all.pharmacies.link.change.pharmacy"/>
                                        </a>
									</tag:check>
									<tag:check commandName="${CommandName.VIEW_ALL_PHARMACIST_FOR_PHARMACY}">
										<a class="dropdown-item" href="${pageContext.request.contextPath}/jsp/pharmacy/viewAllPharmacistInPharmacy
                                            ?${AppConstants.PARAMETER_COMMAND_NAME}=${CommandName.VIEW_ALL_PHARMACIST_FOR_PHARMACY}
                                            &${AppConstants.PARAMETER_PHARMACY_ID}=${pharmacy.id}">
                                                <fmt:message key="pharmacy.page.view.all.pharmacies.link.view.pharmacists"/>
                                        </a>
									</tag:check>
									<tag:check commandName="${CommandName.VIEW_ALL_DRUGS_FOR_PHARMACY}">
										<a class="dropdown-item" href="${pageContext.request.contextPath}/jsp/pharmacy/viewAllDrugInPharmacy
                                            ?${AppConstants.PARAMETER_COMMAND_NAME}=${CommandName.VIEW_ALL_DRUGS_FOR_PHARMACY}
                                            &${AppConstants.PARAMETER_PHARMACY_ID}=${pharmacy.id}">
                                                <fmt:message key="pharmacy.page.view.all.pharmacies.link.view.drugs"/>
                                        </a>
									</tag:check>
									<tag:check commandName="${CommandName.DELETE_PHARMACY}">
									    <div class="dropdown-divider"></div>
                                        <form action="${pageContext.request.contextPath}/jsp/pharmacy/deletePharmacy" method="POST" accept-charset="utf-8">
                                             <input type="hidden" name="${AppConstants.PARAMETER_COMMAND_NAME}" value="${CommandName.DELETE_PHARMACY}">
                                             <input type="hidden" name="${AppConstants.PARAMETER_PHARMACY_ID}" value="${pharmacy.id}">
                                             <button type="submit" class="dropdown-item"><fmt:message key="pharmacy.page.view.all.pharmacies.link.delete.pharmacy"/></button>
                                        </form>
									</tag:check>
								</div>
							</div>
						</td>
						<td><c:out value="${pharmacy.licenceNumber}"/></td>
						<td><c:out value="${pharmacy.phone}"/></td>
						<c:forEach var="address" items="${pharmaciesAddresses}">
							<c:if test="${address.id eq pharmacy.addressId}">
								<td><c:out value="${address.country}"/></td>
								<td><c:out value="${address.town}"/></td>
								<td><c:out value="${address.street}"/></td>
								<td><c:out value="${address.house}"/></td>
							</c:if>
						</c:forEach>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</tag:check>
</div>