<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.baranov.drugdistribution.command.CommandName" %>
<%@ page import="by.baranov.drugdistribution.AppConstants" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<div class="container">
	<tag:check commandName="${CommandName.CREATE_PHARMACY}">
		<h2>
			<fmt:message key="pharmacy.page.create.pharmacy.form.greeting" />
		</h2>

		<form action="createPharmacy" method="POST" accept-charset="utf-8">
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_PHARMACY_NAME}"><fmt:message key="pharmacy.page.create.pharmacy.form.pharmacy.name.legend"/></label> 
				<input id="${AppConstants.PARAMETER_PHARMACY_NAME}" type="text" class="form-control"
					placeholder="<fmt:message key="pharmacy.page.create.pharmacy.form.pharmacy.name.legend.input"/>" name="${AppConstants.PARAMETER_PHARMACY_NAME}"/>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_PHARMACY_LICENCE_NUMBER}"><fmt:message key="pharmacy.page.create.pharmacy.form.pharmacy.licence.number.legend"/></label>
				<input id="${AppConstants.PARAMETER_PHARMACY_LICENCE_NUMBER}" type="text" class="form-control"
					placeholder="<fmt:message key="pharmacy.page.create.pharmacy.form.pharmacy.licence.number.legend.input"/>" name="${AppConstants.PARAMETER_PHARMACY_LICENCE_NUMBER}"/>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_PHARMACY_PHONE}"><fmt:message key="pharmacy.page.create.pharmacy.form.pharmacy.phone.legend"/></label>
				<input id="${AppConstants.PARAMETER_PHARMACY_PHONE}" type="text" class="form-control"
					placeholder="<fmt:message key="pharmacy.page.create.pharmacy.form.pharmacy.phone.legend.input"/>" name="${AppConstants.PARAMETER_PHARMACY_PHONE}"/>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_PHARMACY_ADDRESS_COUNTRY}"><fmt:message key="pharmacy.page.create.pharmacy.form.pharmacy.address.country.legend"/></label>
				<input id="${AppConstants.PARAMETER_PHARMACY_ADDRESS_COUNTRY}" type="text" class="form-control"
					placeholder="<fmt:message key="pharmacy.page.create.pharmacy.form.pharmacy.address.country.legend.input"/>" name="${AppConstants.PARAMETER_PHARMACY_ADDRESS_COUNTRY}"/>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_PHARMACY_ADDRESS_TOWN}"><fmt:message key="pharmacy.page.create.pharmacy.form.pharmacy.address.town.legend"/></label>
				<input id="${AppConstants.PARAMETER_PHARMACY_ADDRESS_TOWN}" type="text" class="form-control"
					placeholder="<fmt:message key="pharmacy.page.create.pharmacy.form.pharmacy.address.town.legend.input"/>" name="${AppConstants.PARAMETER_PHARMACY_ADDRESS_TOWN}"/>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_PHARMACY_ADDRESS_STREET}"><fmt:message key="pharmacy.page.create.pharmacy.form.pharmacy.address.street.legend"/></label>
				<input id="${AppConstants.PARAMETER_PHARMACY_ADDRESS_STREET}" type="text" class="form-control"
					placeholder="<fmt:message key="pharmacy.page.create.pharmacy.form.pharmacy.address.street.legend.input"/>" name="${AppConstants.PARAMETER_PHARMACY_ADDRESS_STREET}"/>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_PHARMACY_ADDRESS_HOUSE}"><fmt:message key="pharmacy.page.create.pharmacy.form.pharmacy.address.house.legend"/></label>
				<input id="${AppConstants.PARAMETER_PHARMACY_ADDRESS_HOUSE}" type="text" class="form-control"
					placeholder="<fmt:message key="pharmacy.page.create.pharmacy.form.pharmacy.address.house.legend.input"/>" name="${AppConstants.PARAMETER_PHARMACY_ADDRESS_HOUSE}"/>
			</div>
			<input type="hidden" name="${AppConstants.PARAMETER_COMMAND_NAME}" value="${CommandName.CREATE_PHARMACY}">
			<button type="submit" class="btn btn-info"><fmt:message key="pharmacy.page.create.pharmacy.form.submit"/></button>
		</form>
	</tag:check>
</div>




