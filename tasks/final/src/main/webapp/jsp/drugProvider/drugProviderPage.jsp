<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<html lang="ru">

<head>
	<title>Drug provider page</title>
	<tag:meta/>
</head>
<body>

	<div class="jumbotron text-center" style="margin-bottom: 0">
		<h1>
			<fmt:message key="drug.provider.page.title" />
		</h1>
	</div>

	<div class="container" style="margin-top: 30px">
		<ul class="nav nav-tabs" role="tablist">
			<li class="nav-item">
			    <a class="nav-link active" data-toggle="tab" href="${pageContext.request.contextPath}/jsp/drugProvider/drugProviderPage.jsp">
			        <fmt:message key="main.page.tab.drug.providers"/>
			    </a>
			</li>
		</ul>
		<div class="tab-content">
			<div id="<fmt:message key="main.page.user.tab.users"/>" class="container tab-pane active">
				<br>
				<div class="row">
					<div class="col-2">
						<ul class="nav nav-pills flex-column">
							<li><tag:main /></li>
						</ul>
						<br />
						<tag:logout />
					</div>
					<div class="col-10">
						<tag:message resultMessage="${resultMessage}"/>
						<tag:validationResult validationResult="${validationResult}"/>
						<tag:error errorMessage="${errorMessage}" />
						<jsp:include page="views/${view}.jsp" />
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>