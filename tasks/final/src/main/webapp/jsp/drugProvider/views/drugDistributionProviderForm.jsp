<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.baranov.drugdistribution.command.CommandName" %>
<%@ page import="by.baranov.drugdistribution.AppConstants" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<div class="container">
	<tag:check commandName="${CommandName.DISTRIBUTE_DRUG_PROVIDER_DRUGS}">
		<h2>
			<fmt:message key="drug.provider.page.drug.distribution.form.greeting" />
		</h2>
		<form action="createDrug" method="POST" accept-charset="utf-8">
			<div class="form-group">
				<label for="chooseDrug"><fmt:message key="drug.provider.page.drug.distribution.form.drug.label"/></label> 
				<select class="form-control" id="chooseDrug" name="${AppConstants.PARAMETER_DRUG_ID}">
					<c:forEach var="drug" items="${drugs}">
						<option value="${drug.id}">
						    <fmt:message key="drug.order.page.create.drug.order.form.drug.name"/>=${drug.name}, 
                            <fmt:message key="drug.order.page.create.drug.order.form.drug.dosage"/>=${drug.dosage}, 
                            <fmt:message key="drug.order.page.create.drug.order.form.drug.availability"/>=${drug.availability}
						</option>
					</c:forEach>
				</select>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_DRUG_MANUFACTURE_DATE}"><fmt:message key="drug.provider.page.drug.distribution.form.drug.manufacture.date.label"/></label>
				<input id="${AppConstants.PARAMETER_DRUG_MANUFACTURE_DATE}" type="text" class="form-control"
					placeholder="<fmt:message key="drug.provider.page.drug.distribution.form.drug.manufacture.date.label.input"/>" name="${AppConstants.PARAMETER_DRUG_MANUFACTURE_DATE}"/>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_DRUG_EXPIRY_DATE}"><fmt:message key="drug.provider.page.drug.distribution.form.drug.expiry.date.label"/></label>
				<input id="${AppConstants.PARAMETER_DRUG_EXPIRY_DATE}" type="text" class="form-control"
					placeholder="<fmt:message key="drug.provider.page.drug.distribution.form.drug.expiry.date.label.input"/>" name="${AppConstants.PARAMETER_DRUG_EXPIRY_DATE}"/>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_DRUG_QUANTITY}"><fmt:message key="drug.provider.page.drug.distribution.form.drug.quantity.label"/></label>
				<input id="${AppConstants.PARAMETER_DRUG_QUANTITY}" type="text" class="form-control"
					placeholder="<fmt:message key="drug.provider.page.drug.distribution.form.drug.quantity.label.input"/>" name="${AppConstants.PARAMETER_DRUG_QUANTITY}"/>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_DRUG_PRICE}"><fmt:message key="drug.provider.page.drug.distribution.form.drug.price.label"/></label>
				<input id="${AppConstants.PARAMETER_DRUG_PRICE}" type="text" class="form-control"
					placeholder="<fmt:message key="drug.provider.page.drug.distribution.form.drug.price.label.input"/>" name="${AppConstants.PARAMETER_DRUG_PRICE}"/>
			</div>
			<input type="hidden" name="${AppConstants.PARAMETER_COMMAND_NAME}" value="${CommandName.DISTRIBUTE_DRUG_PROVIDER_DRUGS}">
			<button type="submit" class="btn btn-info"><fmt:message key="drug.provider.page.drug.distribution.form.submit"/></button>
		</form>
	</tag:check>
</div>




