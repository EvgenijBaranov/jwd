<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.baranov.drugdistribution.command.CommandName" %>
<%@ page import="by.baranov.drugdistribution.AppConstants" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<div class="container">
	<tag:check commandName="${CommandName.VIEW_ALL_DRUG_FORMS_FOR_DRUG}">
		<tag:check commandName="${CommandName.ASSIGN_DRUG_FORM_FOR_DRUG}">
			<form action="addDrugFormForDrug" method="POST" accept-charset="utf-8">
				<div class="form-group">
					<label for="${AppConstants.PARAMETER_DRUG_FORM_NAME}"><fmt:message key="drug.page.view.drug.forms.drug.form.name.legend"/></label> 
					<input id="${AppConstants.PARAMETER_DRUG_FORM_NAME}" type="text" class="form-control"
						placeholder="<fmt:message key="drug.page.view.drug.forms.drug.form.name.legend.input"/>" name="${AppConstants.PARAMETER_DRUG_FORM_NAME}"/>
				</div>
				<input type="hidden" name="${AppConstants.PARAMETER_DRUG_ID}" value="${drug.id}"> 
				<input type="hidden" name="${AppConstants.PARAMETER_COMMAND_NAME}" value="${CommandName.ASSIGN_DRUG_FORM_FOR_DRUG}">
				<button type="submit" class="btn btn-info"><fmt:message key="drug.page.view.drug.forms.button.add.drug.form.for.drug"/></button>
			</form>
		</tag:check>
		
		<fmt:message key="drug.page.view.drug.forms.title.drug.name"/>${drug.name},  
		<fmt:message key="drug.page.view.drug.forms.title.drug.dosage"/>${drug.dosage},  
		<fmt:message key="drug.page.view.drug.forms.title.drug.availability"/>${drug.availability})  
		<fmt:message key="drug.page.view.drug.forms.title.end"/>  :

		<table class="table table-bordered">
			<thead class="thead-light">
				<tr>
					<th><fmt:message key="drug.page.view.all.drugs.table.drug.id"/></th>
					<th><fmt:message key="drug.page.view.all.drugs.table.drug.name"/></th>
					<th><fmt:message key="drug.page.view.all.drugs.table.drug.dosage"/></th>
					<th><fmt:message key="drug.page.view.all.drugs.table.drug.availability"/></th>
					<th><fmt:message key="drug.page.view.drug.forms.drug.form.id"/></th>
					<th><fmt:message key="drug.page.view.drug.forms.drug.form.name"/></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="drugForm" items="${drugFormsForDrug}">
					<tr>
						<td><c:out value="${drug.id}"/></td>
						<td><c:out value="${drug.name}"/></td>
						<td><c:out value="${drug.dosage}"/></td>
						<td><c:out value="${drug.availability}"/></td>
						<td><c:out value="${drugForm.id}"/></td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-light"><c:out value="${drugForm.name}"/></button>
								<button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
									<span class="caret"></span>
								</button>
								<div class="dropdown-menu">
									<tag:check commandName="${CommandName.DELETE_DRUG_FORM_FOR_DRUG}">
                                        <form action="${pageContext.request.contextPath}/jsp/drug/deleteDrugForm" method="POST" accept-charset="utf-8">
                                             <input type="hidden" name="${AppConstants.PARAMETER_COMMAND_NAME}" value="${CommandName.DELETE_DRUG_FORM_FOR_DRUG}">
                                             <input type="hidden" name="${AppConstants.PARAMETER_DRUG_ID}" value="${drug.id}">
                                             <input type="hidden" name="${AppConstants.PARAMETER_DRUG_FORM_ID}" value="${drugForm.id}">
                                             <button type="submit" class="dropdown-item"><fmt:message key="drug.page.view.drug.forms.link.delete.drug.from"/></button>
                                        </form>
									</tag:check>
								</div>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</tag:check>
</div>