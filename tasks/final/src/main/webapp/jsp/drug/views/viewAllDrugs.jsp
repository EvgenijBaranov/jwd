<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.baranov.drugdistribution.command.CommandName" %>
<%@ page import="by.baranov.drugdistribution.AppConstants" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<div class="container">
	<tag:check commandName="${CommandName.VIEW_ALL_DRUGS}">
		<table class="table table-bordered">
			<thead class="thead-light">
				<tr>
					<th rowspan="3"><fmt:message key="drug.page.view.all.drugs.table.drug.id"/></th>
					<th rowspan="3"><fmt:message key="drug.page.view.all.drugs.table.drug.name"/></th>
					<th rowspan="3"><fmt:message key="drug.page.view.all.drugs.table.drug.dosage"/></th>
					<th rowspan="3"><fmt:message key="drug.page.view.all.drugs.table.drug.availability"/></th>
					<th colspan="5"><fmt:message key="drug.page.view.all.drugs.table.drug.producer.title"/></th>
				</tr>
				<tr>
					<th rowspan="2"><fmt:message key="drug.page.view.all.drugs.table.drug.producer.name"/></th>
					<th colspan="4"><fmt:message key="drug.page.view.all.drugs.table.drug.producer.address.title"/></th>
				</tr>
				<tr>
					<th><fmt:message key="drug.page.view.all.drugs.table.drug.producer.address.country"/></th>
					<th><fmt:message key="drug.page.view.all.drugs.table.drug.producer.address.town"/></th>
					<th><fmt:message key="drug.page.view.all.drugs.table.drug.producer.address.street"/></th>
					<th><fmt:message key="drug.page.view.all.drugs.table.drug.producer.address.house"/></th>
				</tr>
			</thead>

			<tbody>
				<c:forEach var="drug" items="${drugs}">
					<tr>
						<td><c:out value="${drug.id}"/></td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-light"> <c:out value="${drug.name}"/> </button>
								<button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
									<span class="caret"></span>
								</button>
								<div class="dropdown-menu">
									<tag:check commandName="${CommandName.VIEW_CHANGE_DRUG_FORM}">
										<a class="dropdown-item" href="${pageContext.request.contextPath}/jsp/drug/changeDrug
                                            ?${AppConstants.PARAMETER_COMMAND_NAME}=${CommandName.VIEW_CHANGE_DRUG_FORM}
                                            &${AppConstants.PARAMETER_DRUG_ID}=${drug.id}">
                                                <fmt:message key="drug.page.view.all.drugs.link.change.drug"/>
                                        </a>
									</tag:check>
									<tag:check commandName="${CommandName.VIEW_ALL_DRUG_FORMS_FOR_DRUG}">
										<a class="dropdown-item" href="${pageContext.request.contextPath}/jsp/drug/viewAllDrugForms
                                            ?${AppConstants.PARAMETER_COMMAND_NAME}=${CommandName.VIEW_ALL_DRUG_FORMS_FOR_DRUG}
                                            &${AppConstants.PARAMETER_DRUG_ID}=${drug.id}">
                                                <fmt:message key="drug.page.view.all.drugs.link.view.drug.forms"/>
                                        </a>
									</tag:check>
									<tag:check commandName="${CommandName.DELETE_DRUG}">
									    <div class="dropdown-divider"></div>
                                        <form action="${pageContext.request.contextPath}/jsp/drug/deleteDrug" method="POST" accept-charset="utf-8">
                                             <input type="hidden" name="${AppConstants.PARAMETER_COMMAND_NAME}" value="${CommandName.DELETE_DRUG}">
                                             <input type="hidden" name="${AppConstants.PARAMETER_DRUG_ID}" value="${drug.id}">
                                             <button type="submit" class="dropdown-item"><fmt:message key="drug.page.view.all.drugs.link.delete.drug"/></button>
                                        </form>
									</tag:check>
								</div>
							</div>
						</td>
						<td><c:out value="${drug.dosage}"/></td>
						<td><c:out value="${drug.availability}"/></td>
						<c:forEach var="producer" items="${drugProducers}">
							<c:if test="${producer.id eq drug.producerId}">
								<td><c:out value="${producer.name}"/></td>
								<c:forEach var="address" items="${drugProducerAddresses}">
									<c:if test="${address.id eq producer.addressId}">
										<td><c:out value="${address.country}"/></td>
										<td><c:out value="${address.town}"/></td>
										<td><c:out value="${address.street}"/></td>
										<td><c:out value="${address.house}"/></td>
									</c:if>
								</c:forEach>
							</c:if>
						</c:forEach>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</tag:check>
</div>