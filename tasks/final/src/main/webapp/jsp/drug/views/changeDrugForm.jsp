<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.baranov.drugdistribution.command.CommandName" %>
<%@ page import="by.baranov.drugdistribution.entity.DrugAvailability" %>
<%@ page import="by.baranov.drugdistribution.AppConstants" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<div class="container">
	<tag:check commandName="${CommandName.CHANGE_DRUG}">
		<h2>
			<fmt:message key="drug.page.change.drug.form.greeting"/>
		</h2>
		<form action="createDrug" method="POST" accept-charset="utf-8">
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_DRUG_NAME}"><fmt:message key="drug.page.create.drug.form.drug.name.legend"/></label> 
				<input id="${AppConstants.PARAMETER_DRUG_NAME}" type="text" class="form-control"
					placeholder="<fmt:message key="drug.page.create.drug.form.drug.name.legend.input"/>" name="${AppConstants.PARAMETER_DRUG_NAME}"/>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_DRUG_DOSAGE}"><fmt:message key="drug.page.create.drug.form.drug.dosage.legend"/></label> 
				<input id="${AppConstants.PARAMETER_DRUG_DOSAGE}" type="text" class="form-control"
					placeholder="<fmt:message key="drug.page.create.drug.form.drug.dosage.legend.input"/>" name="${AppConstants.PARAMETER_DRUG_DOSAGE}"/>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_DRUG_FORM_NAME}"><fmt:message key="drug.page.create.drug.form.drug.form.name.legend"/></label> 
				<input id="${AppConstants.PARAMETER_DRUG_FORM_NAME}" type="text" class="form-control"
					placeholder="<fmt:message key="drug.page.create.drug.form.drug.form.name.legend.input"/>" name="${AppConstants.PARAMETER_DRUG_FORM_NAME}"/>
			</div>
			<div class="form-group">

				<label for="chooseUserRole"><fmt:message key="drug.page.create.drug.form.drug.availability.label"/></label> 
				<select class="form-control" id="chooseUserRole" name="${AppConstants.PARAMETER_DRUG_AVAILABILITY}">
					<c:forEach var="availability" items="${DrugAvailability.values()}">
						<option value="${availability.name}">${availability}</option>
					</c:forEach>
				</select>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_DRUG_PRODUCER_NAME}"><fmt:message key="drug.page.create.drug.form.drug.producer.name.legend"/></label> 
				<input id="${AppConstants.PARAMETER_DRUG_PRODUCER_NAME}" type="text" class="form-control"
					placeholder="<fmt:message key="drug.page.create.drug.form.drug.producer.name.legend.input"/>" name="${AppConstants.PARAMETER_DRUG_PRODUCER_NAME}"/>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_DRUG_PRODUCER_LICENCE_NUMBER}"><fmt:message key="drug.page.create.drug.form.drug.producer.licence.number.legend"/></label>
				<input id="${AppConstants.PARAMETER_DRUG_PRODUCER_LICENCE_NUMBER}" type="text" class="form-control"
					placeholder="<fmt:message key="drug.page.create.drug.form.drug.producer.licence.number.legend.input"/>" name="${AppConstants.PARAMETER_DRUG_PRODUCER_LICENCE_NUMBER}"/>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_DRUG_PRODUCER_ADDRESS_COUNTRY}"><fmt:message key="drug.page.create.drug.form.drug.producer.address.country.legend"/></label>
				<input id="${AppConstants.PARAMETER_DRUG_PRODUCER_ADDRESS_COUNTRY}" type="text" class="form-control"
					placeholder="<fmt:message key="drug.page.create.drug.form.drug.producer.address.country.legend.input"/>" name="${AppConstants.PARAMETER_DRUG_PRODUCER_ADDRESS_COUNTRY}"/>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_DRUG_PRODUCER_ADDRESS_TOWN}"><fmt:message key="drug.page.create.drug.form.drug.producer.address.town.legend"/></label>
				<input id="${AppConstants.PARAMETER_DRUG_PRODUCER_ADDRESS_TOWN}" type="text" class="form-control"
					placeholder="<fmt:message key="drug.page.create.drug.form.drug.producer.address.town.legend.input"/>" name="${AppConstants.PARAMETER_DRUG_PRODUCER_ADDRESS_TOWN}"/>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_DRUG_PRODUCER_ADDRESS_STREET}"><fmt:message key="drug.page.create.drug.form.drug.producer.address.street.legend"/></label>
				<input id="${AppConstants.PARAMETER_DRUG_PRODUCER_ADDRESS_STREET}" type="text" class="form-control"
					placeholder="<fmt:message key="drug.page.create.drug.form.drug.producer.address.street.legend.input"/>" name="${AppConstants.PARAMETER_DRUG_PRODUCER_ADDRESS_STREET}"/>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_DRUG_PRODUCER_ADDRESS_HOUSE}"><fmt:message key="drug.page.create.drug.form.drug.producer.address.house.legend"/></label>
				<input id="${AppConstants.PARAMETER_DRUG_PRODUCER_ADDRESS_HOUSE}" type="text" class="form-control"
					placeholder="<fmt:message key="drug.page.create.drug.form.drug.producer.address.house.legend.input"/>" name="${AppConstants.PARAMETER_DRUG_PRODUCER_ADDRESS_HOUSE}"/>
			</div>
			<input type="hidden" name="${AppConstants.PARAMETER_DRUG_ID}" value="${drug.id}"> 
			<input type="hidden" name="${AppConstants.PARAMETER_COMMAND_NAME}" value="${CommandName.CHANGE_DRUG}">
			<button type="submit" class="btn btn-info"><fmt:message key="drug.page.change.drug.form.submit"/></button>
		</form>
	</tag:check>
</div>




