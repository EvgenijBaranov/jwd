<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="by.baranov.drugdistribution.AppConstants"%>
<%@ page import="by.baranov.drugdistribution.command.CommandName"%>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags"%>

<html lang="ru">
<head>
	<title>Choose category</title>
	<tag:meta/>
</head>

<style>
div.container {
	text-align: center;
}
</style>

<body>
	<div class="container">
		<ul class="nav nav-pills justify-content-end" role="tablist">
			<li class="nav-item"><tag:logout /></li>
		</ul>
	</div>
	<div class="jumbotron text-center" style="margin-bottom: 0">
		<h1> 
             ${registeredUser.name} ${registeredUser.surname}, <fmt:message key="main.page.tab.title" />
		</h1>
	</div>

	<div class="container" style="margin-top: 30px">
		<br />
		<tag:check commandName="${CommandName.VIEW_ALL_USERS}">
			<a href="${pageContext.request.contextPath}/jsp/user/
                ?${AppConstants.PARAMETER_COMMAND_NAME}=${CommandName.VIEW_ALL_USERS}" class="btn btn-info" role="button"> 
                    <fmt:message key="main.page.tab.users"/>
			</a>
		</tag:check>
		
		<tag:check commandName="${CommandName.VIEW_ALL_PHARMACIES}">
			<a href="${pageContext.request.contextPath}/jsp/pharmacy/
                ?${AppConstants.PARAMETER_COMMAND_NAME}=${CommandName.VIEW_ALL_PHARMACIES}" class="btn btn-info" role="button"> 
                    <fmt:message key="main.page.tab.pharmacies"/>
			</a>
		</tag:check>
		
		<tag:check commandName="${CommandName.VIEW_ALL_DRUGS}">
			<a href="${pageContext.request.contextPath}/jsp/drug/
                ?${AppConstants.PARAMETER_COMMAND_NAME}=${CommandName.VIEW_ALL_DRUGS}" class="btn btn-info" role="button"> 
                    <fmt:message key="main.page.tab.drugs"/> 
            </a>
		</tag:check>

		<tag:check commandName="${CommandName.VIEW_ALL_DRUG_ORDERS}">
			<a href="${pageContext.request.contextPath}/jsp/drugOrder/
                ?${AppConstants.PARAMETER_COMMAND_NAME}=${CommandName.VIEW_ALL_DRUG_ORDERS}" class="btn btn-info" role="button"> 
                    <fmt:message key="main.page.tab.drug.orders"/>
			</a>
		</tag:check>
		<tag:check commandName="${CommandName.VIEW_DRUG_PROVIDER_FORM}">
			<a href="${pageContext.request.contextPath}/jsp/drugProvider/
                ?${AppConstants.PARAMETER_COMMAND_NAME}=${CommandName.VIEW_DRUG_PROVIDER_FORM}" class="btn btn-info" role="button"> 
                    <fmt:message key="main.page.tab.drug.providers"/>
			</a>
		</tag:check>
		<tag:check commandName="${CommandName.VIEW_ALL_DRIVER_INVOICES}">
			<a href="${pageContext.request.contextPath}/jsp/jsp/driverInvoice/
                ?${AppConstants.PARAMETER_COMMAND_NAME}=${CommandName.VIEW_ALL_DRIVER_INVOICES}" class="btn btn-info" role="button"> 
                    <fmt:message key="main.page.tab.driver.invoices"/>
			</a>
		</tag:check>
	</div>
</body>
</html>