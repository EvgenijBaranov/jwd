<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ page import="by.baranov.drugdistribution.AppConstants" %>
<%@ page import="by.baranov.drugdistribution.command.CommandName" %>

<html lang="ru">

<head>
	<title>Login User</title>
	<tag:meta/>
</head>
<body>
	<c:choose>
		<c:when test="${not empty requestScope.get('lang')}">
			<fmt:setLocale value="${requestScope.get('lang')}"/>
		</c:when>
		<c:otherwise>
			<fmt:setLocale value="${cookie['lang'].value}"/>
		</c:otherwise>
	</c:choose>

	<fmt:setBundle basename="/i18n/ApplicationMessages" scope="application"/>

	<div class="container">
		<ul class="nav nav-pills justify-content-end" role="tablist">
			<li class="nav-item">
			    <a class="nav-link" href="${pageContext.request.contextPath}?lang=en"> 
				    <fmt:message key="choose.language.english"/>
			    </a>
			</li>
			<li class="nav-item">
			    <a class="nav-link" href="${pageContext.request.contextPath}?lang=ru"> 
			        <fmt:message key="choose.language.russian"/>
			    </a>
			</li>
		</ul>
	</div>
	<div class="jumbotron text-center" style="margin-bottom: 0">
		<h1>
			<fmt:message key="user.login.greeting"/>
		</h1>
	</div>

	<div class="container">
		<form action="userLogin" method="POST" accept-charset="utf-8">
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_USER_LOGIN}"><fmt:message key="user.login.legend"/></label> 
				<input id="${AppConstants.PARAMETER_USER_LOGIN}" type="text" class="form-control"
					placeholder="<fmt:message key="user.login.legend.input"/>" name="${AppConstants.PARAMETER_USER_LOGIN}"/>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_USER_PASSWORD}"><fmt:message key="user.password.legend"/></label> 
				<input id="${AppConstants.PARAMETER_USER_PASSWORD}" type="password" class="form-control"
					placeholder="<fmt:message key="user.password.legend.input"/>" name="${AppConstants.PARAMETER_USER_PASSWORD}"/>
			</div>
			<input type="hidden" name="${AppConstants.PARAMETER_COMMAND_NAME}" value="${CommandName.USER_LOGIN}">
			<button type="submit" class="btn btn-info"> <fmt:message key="user.login.button"/></button>
		</form>
		<tag:error errorMessage="${errorMessage}"/>
	</div>
</body>
</html>