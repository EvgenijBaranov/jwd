<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.baranov.drugdistribution.AppConstants" %>
<%@ page import="by.baranov.drugdistribution.command.CommandName" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<div class="container">
	<h2>
		<fmt:message key="user.page.user.registration.form.greeting"/>
	</h2>
	<tag:check commandName="${CommandName.USER_REGISTRATION}">
		<form action="userRegistration" method="POST" accept-charset="utf-8">
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_USER_LOGIN}"><fmt:message key="user.login.legend"/></label> 
				<input id="${AppConstants.PARAMETER_USER_LOGIN}" type="text" class="form-control"
					placeholder="<fmt:message key="user.login.legend.input"/>" name="${AppConstants.PARAMETER_USER_LOGIN}"/>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_USER_PASSWORD}"><fmt:message key="user.password.legend"/>
				</label> <input id="${AppConstants.PARAMETER_USER_PASSWORD}" type="password" class="form-control"
					placeholder="<fmt:message key="user.password.legend.input"/>" name="${AppConstants.PARAMETER_USER_PASSWORD}">
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_USER_NAME}"><fmt:message key="user.name.legend"/></label> 
				<input id="${AppConstants.PARAMETER_USER_NAME}" type="text" class="form-control"
					placeholder="<fmt:message key="user.name.legend.input"/>" name="${AppConstants.PARAMETER_USER_NAME}"/>
			</div>
			<div class="form-group">
				<label for="${AppConstants.PARAMETER_USER_SURNAME}"><fmt:message key="user.surname.legend"/></label> 
				<input id="${AppConstants.PARAMETER_USER_SURNAME}" type="text" class="form-control"
					placeholder="<fmt:message key="user.surname.legend.input"/>" name="${AppConstants.PARAMETER_USER_SURNAME}">
			</div>
			<input type="hidden" name="${AppConstants.PARAMETER_COMMAND_NAME}" value="${CommandName.USER_REGISTRATION}">
			<button type="submit" class="btn btn-info"><fmt:message key="user.page.user.registration.form.submit"/></button>
		</form>
	</tag:check>
</div>
