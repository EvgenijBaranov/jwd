<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.baranov.drugdistribution.command.CommandName" %>
<%@ page import="by.baranov.drugdistribution.entity.UserRole" %>
<%@ page import="by.baranov.drugdistribution.AppConstants" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<div class="container">
	<tag:check commandName="${CommandName.VIEW_ALL_USER_ROLES_FOR_USER}">
		<tag:check commandName="${CommandName.ASSIGN_USER_ROLE_FOR_USER}">
			<form action="addUserRole" method="POST" accept-charset="utf-8">
				<div class="form-group">
					<label for="chooseUserRole"><fmt:message key="user.page.view.user.roles.add.user.role.for.user.label"/></label> 
					<select class="form-control" id="chooseUserRole" name="${AppConstants.PARAMETER_USER_ROLE_ID}">
						<c:forEach var="userRole" items="${UserRole.values()}">
							<option value="${userRole.id}">${userRole}</option>
						</c:forEach>
					</select>
				</div>
				<input type="hidden" name="${AppConstants.PARAMETER_USER_ID}" value="${user.id}"> 
				<input type="hidden" name="${AppConstants.PARAMETER_COMMAND_NAME}" value="${CommandName.ASSIGN_USER_ROLE_FOR_USER}">
				<button type="submit" class="btn btn-info"><fmt:message key="user.page.view.user.roles.button.add.user.role.for.user"/></button>
			</form>
		</tag:check>
		
		<fmt:message key="user.page.view.user.roles.title.user.name"/>${user.name},  
        <fmt:message key="user.page.view.user.roles.title.user.surname"/>${user.surname},  
        <fmt:message key="user.page.view.user.roles.title.user.login"/>${user.login})  
        <fmt:message key="user.page.view.user.roles.title.end"/>  :
				
		<table class="table table-bordered">
			<thead class="thead-light">
				<tr>
					<th><fmt:message key="user.page.view.all.users.table.user.id"/></th>
					<th><fmt:message key="user.page.view.all.users.table.user.login"/></th>
					<th><fmt:message key="user.page.view.all.users.table.user.name"/></th>
					<th><fmt:message key="user.page.view.all.users.table.user.surname"/></th>
					<th><fmt:message key="user.page.view.user.roles.user.role.id"/></th>
					<th><fmt:message key="user.page.view.user.roles.user.role.name"/></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="userRole" items="${userRolesForUser}">
					<tr>
						<td><c:out value="${user.id}"/></td>
						<td><c:out value="${user.login}"/></td>
						<td><c:out value="${user.name}"/></td>
						<td><c:out value="${user.surname}"/></td>
						<td><c:out value="${userRole.id}"/></td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-light"><c:out value="${userRole.name}"/></button>
								<button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
									<span class="caret"></span>
								</button>
								<tag:check commandName="${CommandName.DELETE_USER_ROLE_FROM_USER}">
									<div class="dropdown-menu">
	                                    <form action="${pageContext.request.contextPath}/jsp/user/deleteUserRole" method="POST" accept-charset="utf-8">
                                             <input type="hidden" name="${AppConstants.PARAMETER_COMMAND_NAME}" value="${CommandName.DELETE_USER_ROLE_FROM_USER}">
                                             <input type="hidden" name="${AppConstants.PARAMETER_USER_ID}" value="${user.id}">
                                             <input type="hidden" name="${AppConstants.PARAMETER_USER_ROLE_ID}" value="${userRole.id}">
                                             <button type="submit" class="dropdown-item"><fmt:message key="user.page.view.user.roles.link.delete.user.role"/></button>
                                        </form>
									</div>
								</tag:check>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</tag:check>
</div>