<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.baranov.drugdistribution.command.CommandName" %>
<%@ page import="by.baranov.drugdistribution.AppConstants" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<div class="container">
    <tag:check commandName="${CommandName.VIEW_ALL_USERS}">
		<table class="table table-bordered">
			<thead class="thead-light">
				<tr>
					<th><fmt:message key="user.page.view.all.users.table.user.id"/></th>
					<th><fmt:message key="user.page.view.all.users.table.user.login"/></th>
					<th><fmt:message key="user.page.view.all.users.table.user.name"/></th>
					<th><fmt:message key="user.page.view.all.users.table.user.surname"/></th>
					<th><fmt:message key="user.page.view.all.users.table.user.password"/></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="user" items="${users}">
					<tr>
						<td><c:out value="${user.id}"/></td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-light"><c:out value="${user.login}"/></button>
								<button type="button" class="btn btn-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
									<span class="caret"></span>
								</button>
								<div class="dropdown-menu" >
									<tag:check commandName="${CommandName.VIEW_CHANGE_USER_FORM}">
		                                <a class="dropdown-item" href="${pageContext.request.contextPath}/jsp/user/changeUser
                                           ?${AppConstants.PARAMETER_COMMAND_NAME}=${CommandName.VIEW_CHANGE_USER_FORM}
                                           &${AppConstants.PARAMETER_USER_ID}=${user.id}">
                                               <fmt:message key="user.page.view.all.users.link.change.user"/>
                                        </a>
									</tag:check>
									<tag:check commandName="${CommandName.VIEW_ALL_USER_ROLES_FOR_USER}">
										<a class="dropdown-item" href="${pageContext.request.contextPath}/jsp/user/addUserRole
		                                   ?${AppConstants.PARAMETER_COMMAND_NAME}=${CommandName.VIEW_ALL_USER_ROLES_FOR_USER}
		                                   &${AppConstants.PARAMETER_USER_ID}=${user.id}">
		                                       <fmt:message key="user.page.view.all.users.link.view.user.roles"/>
		                                </a>
									</tag:check>
									<tag:check commandName="${CommandName.DELETE_USER}">
									   <div class="dropdown-divider"></div>
									   <form action="${pageContext.request.contextPath}/jsp/user/deleteUser" method="POST" accept-charset="utf-8">
                                            <input type="hidden" name="${AppConstants.PARAMETER_COMMAND_NAME}" value="${CommandName.DELETE_USER}">
                                            <input type="hidden" name="${AppConstants.PARAMETER_USER_ID}" value="${user.id}">
                                            <button type="submit" class="dropdown-item"><fmt:message key="user.page.view.all.users.link.delete.user"/></button>
                                        </form>
									</tag:check>
								</div>
							</div>
						</td>
						<td><c:out value="${user.name}"/></td>
						<td><c:out value="${user.surname}"/></td>
						<td><c:out value="${user.password}"/></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</tag:check>
</div>