package by.baranov.simplehttpserver.command;

import java.util.List;

import by.baranov.simplehttpserver.exception.CreateObjectImpossibleException;
import by.baranov.simplehttpserver.model.AbstractTransport;

public class CommandUtility {

	private CommandUtility() {
		throw new CreateObjectImpossibleException("Utility class CommandUtility can't construct objects");
	}

	public static String listTransportToStringForTemplateHTMLFile(List<? extends AbstractTransport> listTransport) {
		StringBuilder result = new StringBuilder();
		result.append("<h2>" + listTransport.get(0).getClass().getSimpleName() + ":</h2>");
		result.append("<ul>");
		for (AbstractTransport transport : listTransport) {
			result.append("<li>");
			result.append(transport.toString());
			result.append("</li>");
		}
		result.append("</ul>");
		return result.toString();
	}

}
