package by.baranov.simplehttpserver.repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.specification.Specification;

public class RepositoryTransportBodyImpl implements Repository<TransportBody> {
	
	private static final Logger LOGGER = Logger.getLogger(RepositoryTransportBodyImpl.class);
	private List<TransportBody> bodies = new ArrayList<>();
	private Random rand = new Random();

	public RepositoryTransportBodyImpl() {
		
		TransportBody body0 = new TransportBody(0, Material.STEEL, getCapacityBody(), "BMW", LocalDate.of(2015, 5, 29));
		TransportBody body1 = new TransportBody(1, Material.STEEL, getCapacityBody(), "Ford", LocalDate.of(1990, 10, 10));
		TransportBody body2 = new TransportBody(2, Material.ALUMINIUM, getCapacityBody(), "Hyundai", LocalDate.of(1993, 12, 10));
		TransportBody body3 = new TransportBody(3, Material.CARBON, getCapacityBody(), "Honda", LocalDate.of(2000, 12, 21));
		TransportBody body4 = new TransportBody(4, Material.STEEL, getCapacityBody(), "Volkswagen", LocalDate.of(2010, 9, 30));
		
		bodies.add(body0);
		bodies.add(body1);
		bodies.add(body2);
		bodies.add(body3);
		bodies.add(body4);
	}

	private double getCapacityBody() {
		return ThreadLocalRandom.current().nextDouble(TransportBody.MIN_CAPACITY, TransportBody.MAX_CAPACITY);
	}

	@Override
	public List<TransportBody> findSome(Specification<TransportBody> specification) {
		List<TransportBody> result = new ArrayList<>();
		for (TransportBody body : bodies) {
			if (specification.match(body)) {
				result.add(body);
			}
		}
		return result;
	}

	@Override
	public Optional<TransportBody> getBy(Specification<TransportBody> specification) {
		for (TransportBody body : bodies) {
			if (specification.match(body)) {
				return Optional.of(body);
			}
		}
		LOGGER.warn(this.getClass().getSimpleName() + " didn't find a body of transport by : " + specification.getClass().getSimpleName());
		return Optional.empty();
	}

	@Override
	public List<TransportBody> getAll() {
		return bodies;
	}

	@Override
	public TransportBody getRandom() {
		int sizeRepository = bodies.size();
		TransportBody randomBody = bodies.get(rand.nextInt(sizeRepository));
		return randomBody;
	}

	@Override
	public String toString() {
		return "Repository of bodies for transport : " + bodies;
	}

	@Override
	public void addElements(List<TransportBody> elements) {
		bodies.addAll(elements);
	}

	@Override
	public void sortRepositoryByParameters(Comparator<? super TransportBody> comparator) {
		Collections.sort(bodies, comparator);
	}	

}
