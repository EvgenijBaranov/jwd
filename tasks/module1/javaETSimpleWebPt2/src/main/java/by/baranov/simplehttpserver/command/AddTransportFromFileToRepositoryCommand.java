package by.baranov.simplehttpserver.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.reader.ReaderTaxiParkFromFile;
import by.baranov.simplehttpserver.validator.Validator;
import by.baranov.simplehttpserver.validator.ValidatorPathFile;

public class AddTransportFromFileToRepositoryCommand implements AppCommand {
	
	private static final Logger LOGGER = Logger.getLogger(AddTransportFromFileToRepositoryCommand.class);
	private final ReaderTaxiParkFromFile readerTaxiPark;
	private final Validator validatorPathFile = new ValidatorPathFile();
	
	public AddTransportFromFileToRepositoryCommand(ReaderTaxiParkFromFile readerTaxiPark) {
		this.readerTaxiPark = readerTaxiPark;
	}

	@Override
	public String execute(Map<String, String> mapCommandNameAndParameters) {
		String inputedPathFile = mapCommandNameAndParameters.get("filePath");
		
		if(!validatorPathFile.validate(mapCommandNameAndParameters)) {
			return "<h4>Unfortunately you entered wrong file path : \""+ mapCommandNameAndParameters.get("filePath") +"\". Try again.</h4>";
		} 
		
	    String resultParsing = "";
		try (BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(inputedPathFile))){
			resultParsing = readerTaxiPark.parseTaxiPark(bufferedReader);
			LOGGER.info("resultParsing : " + resultParsing);
		} catch (IOException exception) {
			LOGGER.error("Can't create BufferedReader object : ", exception);
		}
		
		if("<h4> Taxi: </h4><h4> TaxiCargo: </h4><h4> MiniBus: </h4>".equals(resultParsing)) {
			return "<h4>Taxipark was be loaded from file and added to repository. <br>"
					+ "Click \"Show taxipark\" to display the taxipark with extra data from the file : \"" + inputedPathFile + "\" </h4>";
		}
		
		return "<h4> There are some complexities with the specified file. <br>"
				+ "Next data weren't read from the file and didn't add to the taxipark :</h4>"  
				+ resultParsing;
	}
}
