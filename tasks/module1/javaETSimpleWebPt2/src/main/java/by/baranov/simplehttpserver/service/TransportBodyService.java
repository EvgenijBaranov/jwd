package by.baranov.simplehttpserver.service;

import by.baranov.simplehttpserver.model.TransportBody;

public interface TransportBodyService extends CRUDOperation<TransportBody> {
	
}
