package by.baranov.simplehttpserver.model;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

public abstract class AbstractTransport {
	private String registrationNumber;
	private BrandTransport brand;
	private TransportBody transportBody;
	private Engine engine;
	private List<Wheel> wheels;
	private BigDecimal cost;

	public AbstractTransport() {
	}

	public AbstractTransport(String registrationNumber, BrandTransport brand, TransportBody transportBody,
			Engine engine, List<Wheel> wheels, BigDecimal cost) {
		this.registrationNumber = registrationNumber;
		this.brand = brand;
		this.transportBody = transportBody;
		this.engine = engine;
		this.wheels = wheels;
		this.cost = cost;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public BrandTransport getBrand() {
		return brand;
	}

	public void setBrand(BrandTransport brand) {
		this.brand = brand;
	}

	public TransportBody getTransportBody() {
		return transportBody;
	}

	public void setTransportBody(TransportBody transportBody) {
		this.transportBody = transportBody;
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public List<Wheel> getWheels() {
		return wheels;
	}

	public void setWheels(List<Wheel> wheels) {
		this.wheels = wheels;
	}

	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	
	@Override
	public int hashCode() {
		return Objects.hash(brand, cost, engine, registrationNumber, transportBody, wheels);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof AbstractTransport))
			return false;
		AbstractTransport other = (AbstractTransport) obj;
		return brand == other.brand && Objects.equals(cost, other.cost) && Objects.equals(engine, other.engine)
				&& Objects.equals(registrationNumber, other.registrationNumber)
				&& Objects.equals(transportBody, other.transportBody) && Objects.equals(wheels, other.wheels);
	}

	@Override
	public String toString() {
		return "registrationNumber=" + registrationNumber + ", brand=" + brand + ", cost=" + cost 
				+ ", transportBody=" + transportBody + ", engine=" + engine + ", wheels=" + wheels;
	}

}
