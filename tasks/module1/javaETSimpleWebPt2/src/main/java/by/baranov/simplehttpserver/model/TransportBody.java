package by.baranov.simplehttpserver.model;

import java.time.LocalDate;
import java.util.Objects;

public class TransportBody extends AbstractElementOfTransport {
	public static final double MIN_CAPACITY = 500;
	public static final double MAX_CAPACITY = 2000;
	private Material material;
	private double capacity;

	public TransportBody() {}
	
	public TransportBody(long id, Material material, double capacity, String producer, LocalDate dateOfManufacture) {
		super(id, producer, dateOfManufacture);
		this.material = material;
		this.capacity = capacity;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public double getCapacity() {
		return capacity;
	}

	public void setCapacity(double capacity) {
		this.capacity = capacity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(capacity, material);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof TransportBody))
			return false;
		TransportBody other = (TransportBody) obj;
		return Double.doubleToLongBits(capacity) == Double.doubleToLongBits(other.capacity)
				&& Objects.equals(material, other.material);
	}

	@Override
	public String toString() {
		return "Body [" + super.toString() + ", material=" + material + ", capacity=" + capacity + "]";
	}

}
