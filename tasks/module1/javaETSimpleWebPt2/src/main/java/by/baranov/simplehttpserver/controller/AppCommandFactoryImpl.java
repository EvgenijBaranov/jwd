package by.baranov.simplehttpserver.controller;

import java.util.Map;
import java.util.Optional;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.command.AppCommand;

public class AppCommandFactoryImpl implements AppCommandFactory {
	private static final Logger LOGGER = Logger.getLogger(AppCommandFactoryImpl.class);
	private final Map<AppCommandName, AppCommand> mapCommands;

	public AppCommandFactoryImpl(Map<AppCommandName, AppCommand> mapCommands) {
		this.mapCommands = mapCommands;
		LOGGER.info(this.getClass().getSimpleName() + " is created with command map : " + mapCommands);
	}

	@Override
	public AppCommand getCommand(String commandName) {

		Optional<AppCommandName> nameCommand = AppCommandName.fromString(commandName);

		AppCommand command = null;
		if (nameCommand.isPresent()) {
			AppCommandName name = nameCommand.get();
			command = mapCommands.getOrDefault(name, mapFactory -> "There aren't command : " + nameCommand.get() + " in AppCommandFactory");
		} else {			
			command = userData -> "Application has't such name in AppComandName";
		}

		return command;
	}

}
