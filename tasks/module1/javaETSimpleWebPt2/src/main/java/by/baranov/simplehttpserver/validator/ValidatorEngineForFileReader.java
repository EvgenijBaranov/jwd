package by.baranov.simplehttpserver.validator;

import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.exception.DataFromFileOutOfBoundException;
import by.baranov.simplehttpserver.model.Engine;

public class ValidatorEngineForFileReader implements Validator {
	
	private static final Logger LOGGER = Logger.getLogger(ValidatorEngineForFileReader.class);
	private final Validator validatorTransportElement = new ValidatorElementOfTransportForFileReader();

	@Override
	public boolean validate(Map<String, String> mapParametersFromFile) {
		LOGGER.info("Try to validate parameters for engine in the file : " + mapParametersFromFile);
		Set<String> engineFields = ValidatorUtils.getFieldsOfElement(new Engine());

		Set<String> parameterNames = mapParametersFromFile.keySet();

		if (!parameterNames.containsAll(engineFields)) {
			LOGGER.error("The names of the engine parameters in the file : " + parameterNames
					+ "\n don't correspond to the names of the fields in engine model : " + engineFields);
			return false;
		}

		LOGGER.info( "The names of the engine parameters in the file : " + parameterNames 
					+ "\n  correspond to the names of the fields in engine model : " + engineFields);
		
		boolean idAndDateofManufactureIsValid = validatorTransportElement.validate(mapParametersFromFile);
		
		boolean powerValueIsValid = false;
		try {
			String powerValue = mapParametersFromFile.get("power");
			double power = Double.parseDouble(powerValue);
			if (power < Engine.MIN_POWER || power > Engine.MAX_POWER) {
				throw new DataFromFileOutOfBoundException("Engine power get out of established limits : " 
						+ "[" + Engine.MIN_POWER + " - " + Engine.MAX_POWER + "]");
			}
			powerValueIsValid = true;
		} catch (DataFromFileOutOfBoundException exception) {
			LOGGER.error("Invalid data for engine power in the file ", exception);
		} catch (NumberFormatException exception) {
			LOGGER.error("The power value for engine must be double number ", exception);
		} catch (NullPointerException exception) {
			LOGGER.error("The file must have parameter name \"power\" for engine ", exception);
		}

		return idAndDateofManufactureIsValid && powerValueIsValid;
	}

}
