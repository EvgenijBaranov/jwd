package by.baranov.simplehttpserver.repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.specification.Specification;

public class RepositoryWheelImpl implements Repository<Wheel> {

	private static final Logger LOGGER = Logger.getLogger(RepositoryWheelImpl.class);
	private List<Wheel> wheels = new ArrayList<>();
	private Random rand = new Random();

	public RepositoryWheelImpl() {
		Wheel wheel0 = new Wheel(0, getRadius(), "Belshina", LocalDate.of(2020, 2, 2));
		Wheel wheel1 = new Wheel(1, getRadius(), "Hankook", LocalDate.of(2010, 10, 10));
		Wheel wheel2 = new Wheel(2, getRadius(), "Michelin", LocalDate.of(2011, 11, 11));
		Wheel wheel3 = new Wheel(3, getRadius(), "Nokian", LocalDate.of(2012, 12, 12));
		Wheel wheel4 = new Wheel(4, getRadius(), "Michelin", LocalDate.of(2019, 9, 9));
		wheels.add(wheel4);
		wheels.add(wheel3);
		wheels.add(wheel2);
		wheels.add(wheel1);
		wheels.add(wheel0);
	}

	private int getRadius() {
		return ThreadLocalRandom.current().nextInt(Wheel.MIN_RADIUS, Wheel.MAX_RADIUS);
	}

	@Override
	public List<Wheel> findSome(Specification<Wheel> specification) {
		List<Wheel> result = new ArrayList<>();
		for (Wheel wheel : wheels) {
			if (specification.match(wheel)) {
				result.add(wheel);
			}
		}
		return result;
	}

	@Override
	public Optional<Wheel> getBy(Specification<Wheel> specification) {
		for (Wheel wheel : wheels) {
			if (specification.match(wheel)) {
				return Optional.of(wheel);
			}
		}
		LOGGER.warn(this.getClass().getSimpleName() + " didn't find a wheel by : "
				+ specification.getClass().getSimpleName());
		return Optional.empty();
	}

	@Override
	public List<Wheel> getAll() {
		return wheels;
	}

	@Override
	public Wheel getRandom() {
		int sizeRepository = wheels.size();
		Wheel randomWheel = wheels.get(rand.nextInt(sizeRepository));
		return randomWheel;
	}

	@Override
	public String toString() {
		return "Repository of wheels for transport : " + wheels;
	}

	@Override
	public void addElements(List<Wheel> elements) {
		wheels.addAll(elements);
	}

	@Override
	public void sortRepositoryByParameters(Comparator<? super Wheel> comparator) {
		Collections.sort(wheels, comparator);
	}

}
