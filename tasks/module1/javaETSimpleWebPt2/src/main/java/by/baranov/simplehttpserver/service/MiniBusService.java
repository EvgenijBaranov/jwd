package by.baranov.simplehttpserver.service;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;

import by.baranov.simplehttpserver.model.MiniBus;

public interface MiniBusService extends CRUDOperation<MiniBus>{

	void addMiniBuses(List<MiniBus> listMiniBuses);
	
	BigDecimal calculateCostMiniBuses();
	
	void sortMiniBusesByParameters(Comparator<? super MiniBus> comparator);
}
