package by.baranov.simplehttpserver.specification;

import java.util.Objects;

import by.baranov.simplehttpserver.model.Taxi;

public class SpecificationByTaxiNumberImpl implements Specification<Taxi> {

	private final String taxiNumber;

	public SpecificationByTaxiNumberImpl(String taxiNumber) {
		this.taxiNumber = taxiNumber;
	}

	@Override
	public boolean match(Taxi taxi) {
		return taxiNumber.equalsIgnoreCase(taxi.getRegistrationNumber());
	}

	@Override
	public int hashCode() {
		return Objects.hash(taxiNumber);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof SpecificationByTaxiNumberImpl))
			return false;
		SpecificationByTaxiNumberImpl other = (SpecificationByTaxiNumberImpl) obj;
		return Objects.equals(taxiNumber, other.taxiNumber);
	}	
	
}
