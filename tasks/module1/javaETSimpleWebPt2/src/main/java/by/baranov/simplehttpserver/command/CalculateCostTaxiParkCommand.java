package by.baranov.simplehttpserver.command;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.service.MiniBusService;
import by.baranov.simplehttpserver.service.TaxiCargoService;
import by.baranov.simplehttpserver.service.TaxiService;

public class CalculateCostTaxiParkCommand implements AppCommand {
	private static final Logger LOGGER = Logger.getLogger(CalculateCostTaxiParkCommand.class);
	private final TaxiService taxiService;
	private final TaxiCargoService taxiCargoService;
	private final MiniBusService miniBusService;

	public CalculateCostTaxiParkCommand(TaxiService taxiService, TaxiCargoService taxiCargoService, MiniBusService miniBusService) {
		this.taxiService = taxiService;
		this.taxiCargoService = taxiCargoService;
		this.miniBusService = miniBusService;
		LOGGER.info(this.getClass().getSimpleName() + " is created with services : " 
								+ taxiService.getClass().getSimpleName()
								+ taxiCargoService.getClass().getSimpleName()
								+ miniBusService.getClass().getSimpleName());
	}

	@Override
	public String execute(Map<String, String> mapCommandNameAndParameters) {

		BigDecimal costAllTaxiInTaxiPark = taxiService.calculateCostTaxies();
		BigDecimal costAllTaxiCargoInTaxiPark = taxiCargoService.calculateCostTaxiesCargo();
		BigDecimal costAllMiniBusInTaxiPark = miniBusService.calculateCostMiniBuses();
		
		BigDecimal costTaxiPark = costAllTaxiInTaxiPark.add(costAllTaxiCargoInTaxiPark).add(costAllMiniBusInTaxiPark);
		
		LOGGER.info(this.getClass().getSimpleName() + " is executed");
		return "<h4>The cost of the taxi park : "+ costTaxiPark.doubleValue() + "</h4>";
	}
}
