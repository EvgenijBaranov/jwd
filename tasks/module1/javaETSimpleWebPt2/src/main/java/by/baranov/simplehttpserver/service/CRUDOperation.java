package by.baranov.simplehttpserver.service;

import java.util.List;
import java.util.Optional;

import by.baranov.simplehttpserver.specification.Specification;

public interface CRUDOperation<T> {
	
	List<T> getAll();
	
	Optional<T> getBy(Specification<T> specification);
	
}
