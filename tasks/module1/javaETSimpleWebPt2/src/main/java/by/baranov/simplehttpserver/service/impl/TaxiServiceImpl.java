package by.baranov.simplehttpserver.service.impl;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import by.baranov.simplehttpserver.model.Taxi;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.TaxiService;
import by.baranov.simplehttpserver.specification.Specification;

public class TaxiServiceImpl implements TaxiService {
	private final Repository<Taxi> repositoryTaxi;

	public TaxiServiceImpl(Repository<Taxi> repositoryTaxi) {
		this.repositoryTaxi = repositoryTaxi;
	}

	@Override
	public List<Taxi> getAll() {
		return repositoryTaxi.getAll();
	}

	@Override
	public Optional<Taxi> getBy(Specification<Taxi> specification) {
		return repositoryTaxi.getBy(specification);
	}

	@Override
	public void addTaxies(List<Taxi> listTaxies) {
		repositoryTaxi.addElements(listTaxies);
	}

	@Override
	public BigDecimal calculateCostTaxies() {
		List<Taxi> listTaxi = repositoryTaxi.getAll();
		BigDecimal costListTaxi = listTaxi.stream()
										  .map(Taxi::getCost)
										  .reduce(BigDecimal.ZERO, BigDecimal::add);
		return costListTaxi;
	}

	@Override
	public void sortTaxiesByParameters(Comparator<? super Taxi> comparator) {
		repositoryTaxi.sortRepositoryByParameters(comparator);
	}

}
