package by.baranov.simplehttpserver.exception;

import java.io.IOException;

public class ElementReadFromFileException extends IOException{

	public ElementReadFromFileException(String message, Throwable cause) {
		super(message, cause);
	}

	public ElementReadFromFileException(String message) {
		super(message);
	}
	
}
