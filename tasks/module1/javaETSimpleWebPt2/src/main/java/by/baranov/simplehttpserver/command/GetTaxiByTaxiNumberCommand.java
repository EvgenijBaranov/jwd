package by.baranov.simplehttpserver.command;

import java.util.Map;
import java.util.Optional;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.model.Taxi;
import by.baranov.simplehttpserver.service.TaxiService;
import by.baranov.simplehttpserver.specification.Specification;
import by.baranov.simplehttpserver.specification.SpecificationByTaxiNumberImpl;

public class GetTaxiByTaxiNumberCommand implements AppCommand {
	private static final Logger LOGGER = Logger.getLogger(GetTaxiByTaxiNumberCommand.class);
	private final TaxiService taxiService;

	public GetTaxiByTaxiNumberCommand(TaxiService taxiService) {
		this.taxiService = taxiService;
		LOGGER.info(this.getClass().getSimpleName() + " is created with services : "
				+ taxiService.getClass().getSimpleName());
	}

	@Override
	public String execute(Map<String, String> mapCommandNameAndParameters) {
		String inputedTaxiNumber = mapCommandNameAndParameters.getOrDefault("taxiNumber",
									"The map of command names and parameters hasn't name \"taxiNumber\" in the html file");
		LOGGER.info(this.getClass().getSimpleName() + " has recieved next taxi number : " + inputedTaxiNumber);
		Specification<Taxi> spec = new SpecificationByTaxiNumberImpl(inputedTaxiNumber);
		Optional<Taxi> taxi = taxiService.getBy(spec);
		LOGGER.info(this.getClass().getSimpleName() + " has received taxi : " + taxi);
		
		StringBuilder result = new StringBuilder();
		result.append("<h2>Taxi with number = " + inputedTaxiNumber + "</h2>");
		result.append("<ul>");
		result.append("<li>");
		String resultTaxi = taxi.isPresent() ? taxi.get().toString()
				: "There aren't such registartion number of the taxi in the taxi park";
		result.append(resultTaxi);
		result.append("</li>");
		result.append("</ul>");
		LOGGER.info(this.getClass().getSimpleName() + " is executed");
		return result.toString();
	}

}
