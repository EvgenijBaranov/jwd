package by.baranov.simplehttpserver.launch;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;

import by.baranov.simplehttpserver.command.AddTransportFromFileToRepositoryCommand;
import by.baranov.simplehttpserver.command.AppCommand;
import by.baranov.simplehttpserver.command.CalculateCostTaxiParkCommand;
import by.baranov.simplehttpserver.command.GetMiniBusByMiniBusNumberCommand;
import by.baranov.simplehttpserver.command.GetTaxiByTaxiNumberCommand;
import by.baranov.simplehttpserver.command.GetTaxiCargoByTaxiCargoNumberCommand;
import by.baranov.simplehttpserver.command.GetTaxiParkCommand;
import by.baranov.simplehttpserver.command.SortTaxiParkByParametersCommand;
import by.baranov.simplehttpserver.controller.AppCommandFactory;
import by.baranov.simplehttpserver.controller.AppCommandFactoryImpl;
import by.baranov.simplehttpserver.controller.AppCommandName;
import by.baranov.simplehttpserver.controller.AppController;
import by.baranov.simplehttpserver.controller.SimpleServerHttpHandler;
import by.baranov.simplehttpserver.model.AbstractTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.MiniBus;
import by.baranov.simplehttpserver.model.Taxi;
import by.baranov.simplehttpserver.model.TaxiCargo;
import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.reader.ReaderElement;
import by.baranov.simplehttpserver.reader.ReaderEngineFromFile;
import by.baranov.simplehttpserver.reader.ReaderListMiniBusFromFile;
import by.baranov.simplehttpserver.reader.ReaderListTaxiCargoFromFile;
import by.baranov.simplehttpserver.reader.ReaderListTaxiFromFile;
import by.baranov.simplehttpserver.reader.ReaderListTransport;
import by.baranov.simplehttpserver.reader.ReaderListWheelFromFile;
import by.baranov.simplehttpserver.reader.ReaderMiniBusFromFile;
import by.baranov.simplehttpserver.reader.ReaderTaxiCargoFromFile;
import by.baranov.simplehttpserver.reader.ReaderTaxiFromFile;
import by.baranov.simplehttpserver.reader.ReaderTaxiParkFromFile;
import by.baranov.simplehttpserver.reader.ReaderTransportBodyFromFile;
import by.baranov.simplehttpserver.reader.ReaderWheelFromFile;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.repository.RepositoryEngineImpl;
import by.baranov.simplehttpserver.repository.RepositoryMiniBusImpl;
import by.baranov.simplehttpserver.repository.RepositoryTaxiCargoImpl;
import by.baranov.simplehttpserver.repository.RepositoryTaxiImpl;
import by.baranov.simplehttpserver.repository.RepositoryTransportBodyImpl;
import by.baranov.simplehttpserver.repository.RepositoryWheelImpl;
import by.baranov.simplehttpserver.service.MiniBusService;
import by.baranov.simplehttpserver.service.TaxiCargoService;
import by.baranov.simplehttpserver.service.TaxiService;
import by.baranov.simplehttpserver.service.impl.MiniBusServiceImpl;
import by.baranov.simplehttpserver.service.impl.TaxiCargoServiceImpl;
import by.baranov.simplehttpserver.service.impl.TaxiServiceImpl;

public class ServerApp {
	private static final int QUANTITY_TAXI_IN_REPOSITORY = 5;
	private static final int QUANTITY_TAXI_CARGO_IN_REPOSITORY = 5;
	private static final int QUANTITY_MINI_BUS_IN_REPOSITORY = 5;
	private static final Logger LOGGER = Logger.getLogger(ServerApp.class);

	public static void main(String[] args) throws IOException {
		InetSocketAddress localhost = new InetSocketAddress(49090);
		HttpServer server;
		try {
			server = HttpServer.create(localhost, 0);
		} catch (IOException exception) {
			LOGGER.fatal("Server wasn't created : ", exception);
			throw new IOException("Server wasn't created", exception);
		}

		Repository<TransportBody> repositoryBodies = new RepositoryTransportBodyImpl();
		Repository<Engine> repositoryEngines = new RepositoryEngineImpl();
		Repository<Wheel> repositoryWheels = new RepositoryWheelImpl();
		Repository<Taxi> repositoryTaxies = new RepositoryTaxiImpl(QUANTITY_TAXI_IN_REPOSITORY, repositoryBodies, repositoryEngines, repositoryWheels);
		Repository<TaxiCargo> repositoryTaxiesCargo = new RepositoryTaxiCargoImpl(QUANTITY_TAXI_CARGO_IN_REPOSITORY, repositoryBodies, repositoryEngines, repositoryWheels);
		Repository<MiniBus> repositoryMiniBuses = new RepositoryMiniBusImpl(QUANTITY_MINI_BUS_IN_REPOSITORY, repositoryBodies, repositoryEngines, repositoryWheels);
		LOGGER.info("Repository of minibuses is created : " + repositoryMiniBuses);
		LOGGER.info("Repository of taxi cargo is created : " + repositoryTaxiesCargo);
		LOGGER.info("Repository of taxies is created : " + repositoryTaxies);

		TaxiService taxiService = new TaxiServiceImpl(repositoryTaxies);
		TaxiCargoService taxiCargoService = new TaxiCargoServiceImpl(repositoryTaxiesCargo);
		MiniBusService miniBusService = new MiniBusServiceImpl(repositoryMiniBuses);

		ReaderElement<TransportBody> readerBodyOfTransport = new ReaderTransportBodyFromFile();
		ReaderElement<Wheel> readerWheel = new ReaderWheelFromFile();
		ReaderElement<List<Wheel>> readerListWheel = new ReaderListWheelFromFile(readerWheel);
		ReaderElement<Engine> readerEngine = new ReaderEngineFromFile();
		ReaderElement<Taxi> readerTaxi = new ReaderTaxiFromFile(readerBodyOfTransport, readerEngine, readerListWheel);
		ReaderElement<TaxiCargo> readerTaxiCargo = new ReaderTaxiCargoFromFile(readerBodyOfTransport, readerEngine, readerListWheel);
		ReaderElement<MiniBus> readerMiniBus = new ReaderMiniBusFromFile(readerBodyOfTransport, readerEngine, readerListWheel);
		ReaderListTransport readerListTaxi = new ReaderListTaxiFromFile(readerTaxi, taxiService);
		ReaderListTransport readerListTaxiCargo = new ReaderListTaxiCargoFromFile(readerTaxiCargo, taxiCargoService);
		ReaderListTransport readerListMiniBus = new ReaderListMiniBusFromFile(readerMiniBus, miniBusService);
		ReaderTaxiParkFromFile readerTaxiPark = new ReaderTaxiParkFromFile(readerListTaxi, readerListTaxiCargo, readerListMiniBus);

		Comparator<AbstractTransport> transportCostComparator = (transport1, transport2) -> 
																transport1.getCost().compareTo(transport2.getCost());
		Comparator<AbstractTransport> transportRegistrationNumberComparator = (transport1, transport2) -> 
																transport1.getRegistrationNumber().compareTo(transport2.getRegistrationNumber());
		Comparator<AbstractTransport> transportBrandComparator = (transport1, transport2) -> 
																transport1.getBrand().name().compareTo(transport2.getBrand().name());
		
		Comparator<AbstractTransport> transportCostRegistrationNumberComparator = transportCostComparator
																		.thenComparing(transportRegistrationNumberComparator);
		Comparator<AbstractTransport> transportCostBrandComparator = transportCostComparator
																		.thenComparing(transportBrandComparator);
		
		Comparator<AbstractTransport> transportRegistrationNumberCostComparator = transportRegistrationNumberComparator
																		.thenComparing(transportCostComparator);
		Comparator<AbstractTransport> transportRegistrationNumberBrandComparator = transportRegistrationNumberComparator
																		.thenComparing(transportBrandComparator);
		
		Comparator<AbstractTransport> transportBrandCostComparator = transportBrandComparator
																		.thenComparing(transportCostComparator);
		Comparator<AbstractTransport> transportBrandRegistrationNumberComparator = transportBrandComparator
																		.thenComparing(transportRegistrationNumberComparator);

		Map<String, Comparator<AbstractTransport>> mapParametersOfPOSTRequestAndComparatorsForSortings = new HashMap<>();
		mapParametersOfPOSTRequestAndComparatorsForSortings.put("cost & cost", transportCostComparator);
		mapParametersOfPOSTRequestAndComparatorsForSortings.put("cost & registrationNumber", transportCostRegistrationNumberComparator);
		mapParametersOfPOSTRequestAndComparatorsForSortings.put("cost & brand", transportCostBrandComparator);
		mapParametersOfPOSTRequestAndComparatorsForSortings.put("registrationNumber & registrationNumber", transportRegistrationNumberComparator);
		mapParametersOfPOSTRequestAndComparatorsForSortings.put("registrationNumber & cost", transportRegistrationNumberCostComparator);
		mapParametersOfPOSTRequestAndComparatorsForSortings.put("registrationNumber & brand", transportRegistrationNumberBrandComparator);
		mapParametersOfPOSTRequestAndComparatorsForSortings.put("brand & brand", transportBrandComparator);
		mapParametersOfPOSTRequestAndComparatorsForSortings.put("brand & cost", transportBrandCostComparator);
		mapParametersOfPOSTRequestAndComparatorsForSortings.put("brand & registrationNumber", transportBrandRegistrationNumberComparator);

		AppCommand printTaxiParkCommand = new GetTaxiParkCommand(taxiService, taxiCargoService, miniBusService);
		AppCommand getTaxiByTaxiNumberCommand = new GetTaxiByTaxiNumberCommand(taxiService);
		AppCommand getTaxiCargoByTaxiCargoNumberCommand = new GetTaxiCargoByTaxiCargoNumberCommand(taxiCargoService);
		AppCommand getMiniBusByMiniBusNumberCommand = new GetMiniBusByMiniBusNumberCommand(miniBusService);
		AppCommand addTransportFromFileToRepositoryCommand = new AddTransportFromFileToRepositoryCommand( readerTaxiPark);
		AppCommand calculateCostTaxiParkCommand = new CalculateCostTaxiParkCommand(taxiService, taxiCargoService, miniBusService);
		AppCommand sortTaxiParkByParametersCommand = new SortTaxiParkByParametersCommand(taxiService, taxiCargoService, miniBusService,mapParametersOfPOSTRequestAndComparatorsForSortings);

		Map<AppCommandName, AppCommand> mapCommands = new EnumMap<>(AppCommandName.class);
		mapCommands.put(AppCommandName.GET_ALL_TAXIPARK, printTaxiParkCommand);
		mapCommands.put(AppCommandName.GET_TAXI_BY_TAXI_NUMBER, getTaxiByTaxiNumberCommand);
		mapCommands.put(AppCommandName.GET_TAXICARGO_BY_TAXICARGO_NUMBER, getTaxiCargoByTaxiCargoNumberCommand);
		mapCommands.put(AppCommandName.GET_MINIBUS_BY_MINIBUS_NUMBER, getMiniBusByMiniBusNumberCommand);
		mapCommands.put(AppCommandName.ADD_TRANSPORT_FROM_FILE_TO_REPOSITORY, addTransportFromFileToRepositoryCommand);
		mapCommands.put(AppCommandName.CALCULATE_COST_TAXIPARK, calculateCostTaxiParkCommand);
		mapCommands.put(AppCommandName.SORT_TRANSPORT_BY_PARAMETERS, sortTaxiParkByParametersCommand);

		LOGGER.info("Command map is assembled : " + mapCommands);

		AppCommandFactory commandFactory = new AppCommandFactoryImpl(mapCommands);

		AppController controller = new AppController(commandFactory);
		LOGGER.info("Controller is assembled");

		HttpContext serverContext = server.createContext("/SimpleServer", new SimpleServerHttpHandler(controller));

		ExecutorService executor = Executors.newFixedThreadPool(4);
		server.setExecutor(executor);
		server.start();
		LOGGER.info("Server started on " + server.getAddress().toString());
	}
}
