package by.baranov.simplehttpserver.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import by.baranov.simplehttpserver.exception.CreateObjectImpossibleException;
import by.baranov.simplehttpserver.exception.FileNotAvailableException;

public class ReaderUtility {

	private ReaderUtility() {
		throw new CreateObjectImpossibleException("Utility class ReaderUtility can't construct objects");
	}

	public static Map<String, String> getTransportElementParameters(BufferedReader bufferedReader) throws FileNotAvailableException {

		Map<String, String> mapParamemetrsForElementOfTransport = new HashMap<>();

		boolean isLineElementTransport = true;
		while (isLineElementTransport) {
			String lineInElement = null;
			try {
				lineInElement = bufferedReader.readLine();
			} catch (IOException exception) {
				throw new FileNotAvailableException("Can't read the file. The file isn't available for reading.", exception);
			}
			Pattern pattern = Pattern.compile("\\s*\\},*\\s*");
			Matcher matcher = pattern.matcher(lineInElement);

			if (matcher.matches()) {
				isLineElementTransport = false;
			} else {
				lineInElement = lineInElement.replaceAll("\\s*,\\s*", "").trim();
				String[] separateParameterAndValue = lineInElement.split("=");
				String parameterName = separateParameterAndValue[0];
				String parameterValue = separateParameterAndValue.length == 2 ? separateParameterAndValue[1] : "";
				mapParamemetrsForElementOfTransport.put(parameterName, parameterValue);
			}
		}
		return mapParamemetrsForElementOfTransport;
	}
}
