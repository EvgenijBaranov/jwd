package by.baranov.simplehttpserver.model;

import java.time.LocalDate;
import java.util.Objects;

public class Engine extends AbstractElementOfTransport {
	public static final double MAX_POWER = 300;
	public static final double MIN_POWER  =100;

	private double power;
	
	public Engine() {}
	
	public Engine(long id, double power, String producer, LocalDate dateOfManufacture) {
		super(id, producer, dateOfManufacture);
		this.power = power;
	}

	public double getPower() {
		return power;
	}

	public void setPower(double power) {
		this.power = power;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(power);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Engine))
			return false;
		Engine other = (Engine) obj;
		return Double.doubleToLongBits(power) == Double.doubleToLongBits(other.power);
	}

	@Override
	public String toString() {
		return "Engine [" + super.toString() + ", power=" + power + "]";
	}

}
