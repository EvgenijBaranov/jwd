package by.baranov.simplehttpserver.specification;

import java.util.Objects;

import by.baranov.simplehttpserver.model.TaxiCargo;

public class SpecificationByTaxiCargoNumberImpl implements Specification<TaxiCargo> {

	private final String taxiCargoNumber;

	public SpecificationByTaxiCargoNumberImpl(String taxiCargoNumber) {
		this.taxiCargoNumber = taxiCargoNumber;
	}

	@Override
	public boolean match(TaxiCargo taxiCargo) {
		return taxiCargoNumber.equalsIgnoreCase(taxiCargo.getRegistrationNumber());
	}

	@Override
	public int hashCode() {
		return Objects.hash(taxiCargoNumber);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof SpecificationByTaxiCargoNumberImpl))
			return false;
		SpecificationByTaxiCargoNumberImpl other = (SpecificationByTaxiCargoNumberImpl) obj;
		return Objects.equals(taxiCargoNumber, other.taxiCargoNumber);
	}
	
	
}
