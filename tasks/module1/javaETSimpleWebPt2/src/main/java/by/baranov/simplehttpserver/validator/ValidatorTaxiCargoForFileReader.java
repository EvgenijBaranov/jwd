package by.baranov.simplehttpserver.validator;

import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.exception.DataFromFileOutOfBoundException;
import by.baranov.simplehttpserver.model.TaxiCargo;

public class ValidatorTaxiCargoForFileReader implements Validator {

	private static final Logger LOGGER = Logger.getLogger(ValidatorTaxiCargoForFileReader.class);
	private final Validator validatorAbstractTransport = new ValidatorTransportForFileReader();
	
	@Override
	public boolean validate(Map<String, String> mapParametersFromFile) {
		LOGGER.info("Try to validate parameters for taxi cargo in the file : " + mapParametersFromFile);
		Set<String> taxiCargoFields = ValidatorUtils.getFieldsOfElement(new TaxiCargo());

		Set<String> parameterNames = mapParametersFromFile.keySet();
		if (!parameterNames.containsAll(taxiCargoFields)) {
			LOGGER.error("The names of the taxi cargo parameters in the file : " + parameterNames
					+ "\n don't correspond to the names of the fields in taxiCargo model : " + taxiCargoFields);
			return false;
		}

		LOGGER.info("The names of the taxi cargo parameters in the file : " + parameterNames
				+ "\n  correspond to the names of the fields in taxiCargo model : " + taxiCargoFields);

		boolean dataForTransportIsValid = validatorAbstractTransport.validate(mapParametersFromFile);
		
		double maxLoadCapacity = 0;
		boolean maxLoadCapacityValueIsValid = false;
		try {
			String maxLoadCapacityValue = mapParametersFromFile.get("maxLoadCapacity");
			maxLoadCapacity = Double.parseDouble(maxLoadCapacityValue);

			if (maxLoadCapacity < TaxiCargo.MIN_VALUE_MASS_OF_GOODS || maxLoadCapacity > TaxiCargo.MAX_VALUE_MASS_OF_GOODS) {
				throw new DataFromFileOutOfBoundException("Value for \"maxLoadCapacity\" in the taxi cargo gets out of established limits : " 
						+ "[" + TaxiCargo.MIN_VALUE_MASS_OF_GOODS + " - " + TaxiCargo.MAX_VALUE_MASS_OF_GOODS + "]");
			}
			maxLoadCapacityValueIsValid = true;
		} catch (DataFromFileOutOfBoundException exception) {
			LOGGER.error("Invalid data for \"maxLoadCapacity\" in the file ", exception);
		} catch (NumberFormatException exception) {
			LOGGER.error("The \"maxLoadCapacity\" value for taxi cargo must be double number ", exception);
		}catch (NullPointerException exception) {
			LOGGER.error("The file must have parameter name \"maxLoadCapacity\" for taxi cargo ", exception);
		}
		
		boolean loadCapacityValueIsValid = false;
		try {
			String loadCapacityValue = mapParametersFromFile.get("loadCapacity");
			double loadCapacity = Double.parseDouble(loadCapacityValue);

			if (loadCapacity > maxLoadCapacity ) {
				throw new DataFromFileOutOfBoundException("Value for \"loadCapacity\" in the taxi cargo must be <= " + maxLoadCapacity);
			}
			loadCapacityValueIsValid = true;
		} catch (DataFromFileOutOfBoundException exception) {
			LOGGER.error("Invalid data for \"loadCapacity\" in the file ", exception);
		} catch (NumberFormatException exception) {
			LOGGER.error("The \"loadCapacity\" value for taxi cargo in the file must be double number ", exception);
		}catch (NullPointerException exception) {
			LOGGER.error("The file must have parameter name \"loadCapacity\" for taxi cargo ", exception);
		}
		
		return dataForTransportIsValid && loadCapacityValueIsValid && maxLoadCapacityValueIsValid;
	}
	
}
