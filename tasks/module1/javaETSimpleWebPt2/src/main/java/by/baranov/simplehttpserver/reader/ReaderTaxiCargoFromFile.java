package by.baranov.simplehttpserver.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.exception.ElementReadFromFileException;
import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.TaxiCargo;
import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.validator.Validator;
import by.baranov.simplehttpserver.validator.ValidatorTaxiCargoForFileReader;

public class ReaderTaxiCargoFromFile implements ReaderElement<TaxiCargo> {
	private static final Logger LOGGER = Logger.getLogger(ReaderTaxiCargoFromFile.class);
	private final ReaderElement<TransportBody> readerBody;
	private final ReaderElement<Engine> readerEngine;
	private final ReaderElement<List<Wheel>> readerWheels;
	private final Validator validatorTaxiCargo = new ValidatorTaxiCargoForFileReader();

	public ReaderTaxiCargoFromFile(ReaderElement<TransportBody> readerBody, ReaderElement<Engine> readerEngine,
									ReaderElement<List<Wheel>> readerWheels) {
		this.readerBody = readerBody;
		this.readerEngine = readerEngine;
		this.readerWheels = readerWheels;
	}

	@Override
	public TaxiCargo parseElement(BufferedReader bufferedReader) throws IOException {
		Map<String, String> mapParametersTaxiCargo = new HashMap<>();

		TransportBody body = null;
		Engine engine = null;
		List<Wheel> wheels = null;

		Pattern patternForBody = Pattern.compile("\\s*transportBody=\\s*\\{\\s*");
		Pattern patternForEngine = Pattern.compile("\\s*engine=\\s*\\{\\s*");
		Pattern patternForWheels = Pattern.compile("\\s*wheels=\\s*\\[\\s*");
		Pattern patternForEndTaxiCargo = Pattern.compile("\\s*\\}{2},*\\s*");

		boolean isLineInTaxiCargo = true;
		while (isLineInTaxiCargo) {

			String lineInTaxiCargo = bufferedReader.readLine();

			Matcher matcherForBody = patternForBody.matcher(lineInTaxiCargo);
			if (matcherForBody.matches()) {
				LOGGER.info("Starting to parse the taxi cargo body beginning with string : " + lineInTaxiCargo);
				body = readerBody.parseElement(bufferedReader);
			}

			Matcher matcherForEngine = patternForEngine.matcher(lineInTaxiCargo);
			if (matcherForEngine.matches()) {
				LOGGER.info("Starting to parse the taxi cargo engine beginning with string : " + lineInTaxiCargo);
				engine = readerEngine.parseElement(bufferedReader);
			}

			Matcher matcherForWheels = patternForWheels.matcher(lineInTaxiCargo);
			if (matcherForWheels.matches()) {
				LOGGER.info("Starting to parse the taxi cargo wheels beginning with string : " + lineInTaxiCargo);
				wheels = readerWheels.parseElement(bufferedReader);
			}

			Matcher matcherForEndTaxiCargo = patternForEndTaxiCargo.matcher(lineInTaxiCargo);
			if (matcherForEndTaxiCargo.matches()) {
				isLineInTaxiCargo = false;
			}

			LOGGER.info("Starting to parse the taxi cargo parameters beginning with string : " + lineInTaxiCargo);
			lineInTaxiCargo = lineInTaxiCargo.replace(",", "").trim();
			String[] separateParameterAndValue = lineInTaxiCargo.split("=");
			String parameterName = separateParameterAndValue[0];
			String parameterValue = separateParameterAndValue.length == 2 ? separateParameterAndValue[1] : "";
			mapParametersTaxiCargo.put(parameterName, parameterValue);

		}

		LOGGER.info("The map for taxi cargo with parameter names and values was created with next result : "
				+ mapParametersTaxiCargo);

		TaxiCargo taxiCargo = null;

		if (validatorTaxiCargo.validate(mapParametersTaxiCargo)) {

			String registrationNumber = mapParametersTaxiCargo.get("registrationNumber");

			String brandValue = mapParametersTaxiCargo.get("brand");
			BrandTransport brand = BrandTransport.valueOf(brandValue);

			String maxLoadCapacityValue = mapParametersTaxiCargo.get("maxLoadCapacity");
			double maxLoadCapacity = Double.parseDouble(maxLoadCapacityValue);
			
			String costValue = mapParametersTaxiCargo.get("cost");
			BigDecimal cost = new BigDecimal(costValue);

			taxiCargo = new TaxiCargo(registrationNumber, brand, body, engine, wheels, maxLoadCapacity, cost);
			LOGGER.info("The taxi cargo was read from the file succesfully : " + taxiCargo);

		} else {
			throw new ElementReadFromFileException("The taxi cargo wasn't read from the file, because of wrong data in taxi cargo parameters.");
		}

		return taxiCargo;
	}
}
