package by.baranov.simplehttpserver.service;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;

import by.baranov.simplehttpserver.model.TaxiCargo;

public interface TaxiCargoService extends CRUDOperation<TaxiCargo>{
	
	void addTaxiesCargo(List<TaxiCargo> listTaxiesCargo);
	
	BigDecimal calculateCostTaxiesCargo();
	
	void sortTaxiesCargoByParameters(Comparator<? super TaxiCargo> comparator);
	
}
