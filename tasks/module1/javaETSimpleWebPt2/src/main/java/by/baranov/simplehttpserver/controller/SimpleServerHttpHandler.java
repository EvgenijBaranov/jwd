package by.baranov.simplehttpserver.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class SimpleServerHttpHandler implements HttpHandler {
	private static final Logger LOGGER = Logger.getLogger(SimpleServerHttpHandler.class);
	private static final String NAME_TEMPLATE_FILE_FOR_RESPONSE_PAGE = "defaultIndex.html";
	private static final String NAME_OF_PARAMETER_IN_HTML_FOR_COMMANDS = "commandName";
	private final AppController controller;

	public SimpleServerHttpHandler(AppController controller) {
		this.controller = controller;
	}

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		LOGGER.info("Enter to the handle method of the " + this.getClass().getSimpleName());
		String nameRequestMethod = exchange.getRequestMethod();
		LOGGER.info(this.getClass().getSimpleName() + " has received method : " + nameRequestMethod);

		String responseTemplate = getResponseTemplate(NAME_TEMPLATE_FILE_FOR_RESPONSE_PAGE);
		LOGGER.info(this.getClass().getSimpleName() + " has read template file for response : " + responseTemplate);

		if ("GET".equalsIgnoreCase(nameRequestMethod)) {
			String responseViewForGet = MessageFormat.format(responseTemplate, " ");
			LOGGER.info(this.getClass().getSimpleName() + " has combined template file with extra information for GET method : " + responseViewForGet);
			makeResponse(exchange, responseViewForGet);
		}

		String parametersAndValuesPOST = inputStreamToString(exchange.getRequestBody());
		LOGGER.info(this.getClass().getSimpleName() + " has read parameters and values of POST method : " + parametersAndValuesPOST);

		/*
		 * create Map for parameter names and values of the POST request parse POST
		 * request and fill map, in which keys are parameter names of the POST request,
		 * values are parameter values of the POST request
		 */

		Map<String, String> mapParameters = new HashMap<>();
		fillMapWithParametrs(mapParameters, parametersAndValuesPOST);

		String commandNameFromPOSTRequest = mapParameters.getOrDefault(NAME_OF_PARAMETER_IN_HTML_FOR_COMMANDS, "Bad command name");
		LOGGER.info("Command name from POST request : " + commandNameFromPOSTRequest);
		
		String resultCommand = controller.handleUserDate(commandNameFromPOSTRequest, mapParameters);
		LOGGER.info("Result of command execute : " + resultCommand);

		String responseViewForPOST = MessageFormat.format(responseTemplate, resultCommand);
		LOGGER.info(this.getClass().getSimpleName() + " has combined template file with result of command execute : " + responseViewForPOST);
		
		makeResponse(exchange, responseViewForPOST);

	}

	private void makeResponse(HttpExchange exchange, String responseView) throws IOException {
		LOGGER.info(this.getClass().getSimpleName() + " is trying to construct the response with view : " + responseView);
		byte[] bytesForResponse = responseView.getBytes(StandardCharsets.UTF_8);
		try (OutputStream responseBody = exchange.getResponseBody()){
			exchange.sendResponseHeaders(200, bytesForResponse.length);
			responseBody.write(bytesForResponse);
			responseBody.flush();
		} catch (IOException exception) {
			LOGGER.error(this.getClass().getSimpleName() + " can't construct the response", exception);
			exchange.sendResponseHeaders(500, -1);
		}
		LOGGER.info(this.getClass().getSimpleName() + " has constructed the response");
	}

	private String getResponseTemplate(String resourceFileName) {
		InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream(resourceFileName);
		String template = inputStreamToString(resourceAsStream);
		return template;
	}

	private String inputStreamToString(InputStream inputStream){
		String result = null;
		try {
			result = URLDecoder.decode(new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8)).lines().collect(Collectors.joining()), StandardCharsets.UTF_8.name());
		} catch (UnsupportedEncodingException exception) {
			LOGGER.error("Error of decoding inputstream", exception);
			result = "Error of decoding inputstream";
		}
		return result;
	}

	private void fillMapWithParametrs(Map<String, String> mapForParameterNamesAndValues, String parametersAndValuesPOSTRequest) {
		String[] parametersWithValues = parametersAndValuesPOSTRequest.split("&");
		for (String parameterWithValue : parametersWithValues) {
			String[] separateParameterAndValue = parameterWithValue.split("=");
			String parameterName = separateParameterAndValue[0];
			String parameterValue = separateParameterAndValue.length == 2 ? separateParameterAndValue[1] : "";
			mapForParameterNamesAndValues.put(parameterName, parameterValue);
		}
		LOGGER.info("Map of parameters and values from request is formed : " + mapForParameterNamesAndValues);
	}

}
