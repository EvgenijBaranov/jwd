package by.baranov.simplehttpserver.model;

import java.time.LocalDate;
import java.util.Objects;

public abstract class AbstractElementOfTransport {
	private long id;
	private String producer;
	private LocalDate dateOfManufacture;
	
	public AbstractElementOfTransport(){}

	public AbstractElementOfTransport(long id, String producer, LocalDate dateOfManufacture) {		
		this.id = id;
		this.producer = producer;
		this.dateOfManufacture = dateOfManufacture;
	}

	public long getId() {
		return id;
	}

	public String getProducer() {
		return producer;
	}

	public LocalDate getDateOfManufacture() {
		return dateOfManufacture;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public void setDateOfManufacture(LocalDate dateOfManufacture) {
		this.dateOfManufacture = dateOfManufacture;
	}

	@Override
	public int hashCode() {
		return Objects.hash(dateOfManufacture, id, producer);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof AbstractElementOfTransport))
			return false;
		AbstractElementOfTransport other = (AbstractElementOfTransport) obj;
		return Objects.equals(dateOfManufacture, other.dateOfManufacture) && id == other.id
				&& Objects.equals(producer, other.producer);
	}

	@Override
	public String toString() {
		return "id=" + id + ", producer=" + producer + ", dateOfManufacture=" + dateOfManufacture;
	}

}
