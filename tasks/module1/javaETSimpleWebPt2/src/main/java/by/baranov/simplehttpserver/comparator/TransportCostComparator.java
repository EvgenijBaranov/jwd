package by.baranov.simplehttpserver.comparator;

import java.util.Comparator;

import by.baranov.simplehttpserver.model.AbstractTransport;

public class TransportCostComparator implements Comparator<AbstractTransport>{

	@Override
	public int compare(AbstractTransport transport1, AbstractTransport transport2) {
		return transport1.getCost().compareTo(transport2.getCost());
	}

}
