package by.baranov.simplehttpserver.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.exception.ElementReadFromFileException;
import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.Taxi;
import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.validator.Validator;
import by.baranov.simplehttpserver.validator.ValidatorTaxiForFileReader;
import by.baranov.simplehttpserver.validator.ValidatorUtils;

public class ReaderTaxiFromFile implements ReaderElement<Taxi> {
	private static final Logger LOGGER = Logger.getLogger(ReaderTaxiFromFile.class);
	private final ReaderElement<TransportBody> readerBody;
	private final ReaderElement<Engine> readerEngine;
	private final ReaderElement<List<Wheel>> readerWheels;
	private final Validator validatorTaxi = new ValidatorTaxiForFileReader();

	public ReaderTaxiFromFile(ReaderElement<TransportBody> readerBody, ReaderElement<Engine> readerEngine,
								ReaderElement<List<Wheel>> readerWheels) {
		this.readerBody = readerBody;
		this.readerEngine = readerEngine;
		this.readerWheels = readerWheels;
	}

	@Override
	public Taxi parseElement(BufferedReader bufferedReader) throws IOException {
		Map<String, String> mapParametersTaxi = new HashMap<>();

		TransportBody body = null;
		Engine engine = null;
		List<Wheel> wheels = null;

		Pattern patternForBody = Pattern.compile("\\s*transportBody=\\s*\\{\\s*");
		Pattern patternForEngine = Pattern.compile("\\s*engine=\\s*\\{\\s*");
		Pattern patternForWheels = Pattern.compile("\\s*wheels=\\s*\\[\\s*");
		Pattern patternForEndTaxi = Pattern.compile("\\s*\\}{2},*\\s*");

		boolean isLineInTaxi = true;
		while (isLineInTaxi) {

			String lineInTaxi = bufferedReader.readLine();

			Matcher matcherForBody = patternForBody.matcher(lineInTaxi);
			if (matcherForBody.matches()) {
				LOGGER.info("Starting to parse the taxi body beginning with string : " + lineInTaxi);
				body = readerBody.parseElement(bufferedReader);
			}

			Matcher matcherForEngine = patternForEngine.matcher(lineInTaxi);
			if (matcherForEngine.matches()) {
				LOGGER.info("Starting to parse the taxi engine beginning with string : " + lineInTaxi);
				engine = readerEngine.parseElement(bufferedReader);
			}

			Matcher matcherForWheels = patternForWheels.matcher(lineInTaxi);
			if (matcherForWheels.matches()) {
				LOGGER.info("Starting to parse the taxi wheels beginning with string : " + lineInTaxi);
				wheels = readerWheels.parseElement(bufferedReader);
			}

			Matcher matcherForEndTaxi = patternForEndTaxi.matcher(lineInTaxi);
			if (matcherForEndTaxi.matches()) {
				isLineInTaxi = false;
			}
			
			LOGGER.info("Starting to parse the taxi parameters beginning with string : " + lineInTaxi);
			lineInTaxi = lineInTaxi.replace(",", "").trim();
			String[] separateParameterAndValue = lineInTaxi.split("=");
			String parameterName = separateParameterAndValue[0];
			String parameterValue = separateParameterAndValue.length == 2 ? separateParameterAndValue[1] : "";
			mapParametersTaxi.put(parameterName, parameterValue);

		}
		LOGGER.info("The map for taxi with parameter names and values was created with next result : "
				+ mapParametersTaxi);

		Taxi taxi = null;

		if (validatorTaxi.validate(mapParametersTaxi)
				&& ValidatorUtils.isValidQuantityAndRadiusWheelsFromFile(wheels, Taxi.NUMBERS_WHEEL)) {
			String registrationNumber = mapParametersTaxi.get("registrationNumber");

			String brandValue = mapParametersTaxi.get("brand");
			BrandTransport brand = BrandTransport.valueOf(BrandTransport.class, brandValue);
			
			String costValue = mapParametersTaxi.get("cost");
			BigDecimal cost = new BigDecimal(costValue);
			
			taxi = new Taxi(registrationNumber, brand, body, engine, wheels, cost);
			LOGGER.info("The taxi was read from the file succesfully : " + taxi);

		} else {
			throw new ElementReadFromFileException("The taxi wasn't read from the file, because of wrong data in taxi parameters.");
		}

		return taxi;
	}

}
