package by.baranov.simplehttpserver.model;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

public class TaxiCargo extends AbstractTransport {
	public static final double MAX_VALUE_MASS_OF_GOODS = 2000;
	public static final double MIN_VALUE_MASS_OF_GOODS = 1000;
	public static final int NUMBERS_WHEEL = 6;
	public static final int MAX_QUANTITY_PASSENGERS = 2;
	private double maxLoadCapacity;
	private double loadCapacity;

	public TaxiCargo() {}
	
	public TaxiCargo(String taxiNumber, BrandTransport brand, TransportBody transportBody,
								Engine engine, List<Wheel> wheels, double maxLoadCapacity, BigDecimal cost) {
		
		super(taxiNumber, brand, transportBody, engine, wheels, cost);
		this.maxLoadCapacity = maxLoadCapacity;
	}

	public double getMaxLoadCapacity() {
		return maxLoadCapacity;
	}

	public void setMaxLoadCapacity(double maxLoadCapacity) {
		this.maxLoadCapacity = maxLoadCapacity;
	}

	public double getLoadCapacity() {
		return loadCapacity;
	}

	public void setLoadCapacity(double loadCapacity) {
		this.loadCapacity = loadCapacity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(loadCapacity, maxLoadCapacity);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof TaxiCargo))
			return false;
		TaxiCargo other = (TaxiCargo) obj;
		return Double.doubleToLongBits(loadCapacity) == Double.doubleToLongBits(other.loadCapacity)
				&& Double.doubleToLongBits(maxLoadCapacity) == Double.doubleToLongBits(other.maxLoadCapacity);
	}

	@Override
	public String toString() {
		return "\nTaxiCargo [" + super.toString() + ", maxLoadCapacity=" + maxLoadCapacity + ", loadCapacity="
				+ loadCapacity + "]";
	}

}
