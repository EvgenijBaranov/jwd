package by.baranov.simplehttpserver.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.exception.ElementReadFromFileException;
import by.baranov.simplehttpserver.exception.FileNotAvailableException;
import by.baranov.simplehttpserver.model.MiniBus;
import by.baranov.simplehttpserver.service.MiniBusService;

public class ReaderListMiniBusFromFile implements ReaderListTransport {
	private static final Logger LOGGER = Logger.getLogger(ReaderListMiniBusFromFile.class);
	private final ReaderElement<MiniBus> readerMiniBus;
	private final MiniBusService miniBusService;

	public ReaderListMiniBusFromFile(ReaderElement<MiniBus> readerMiniBus, MiniBusService miniBusService) {
		this.readerMiniBus = readerMiniBus;
		this.miniBusService = miniBusService;
	}

	@Override
	public String parseListTransport(BufferedReader bufferedReader) throws IOException {
		List<MiniBus> listMiniBus = new ArrayList<>();
		long numberMiniBusWithWrongData = 1L;
		StringBuilder wrongInformationAboutParseListMiniBus = new StringBuilder();

		Pattern patternForMiniBus = Pattern.compile("\\s*MiniBus\\s*\\{{2}\\s*");
		Pattern patternEndListMiniBus = Pattern.compile("\\s*\\}\\],*\\s*");

		boolean isLineInMiniBuses = true;

		while (isLineInMiniBuses) {

			String lineInMiniBuses = null;
			try {
				lineInMiniBuses = bufferedReader.readLine();
			} catch (IOException exception) {
				throw new FileNotAvailableException("Can't read the file. The file isn't available for reading.", exception);
			}

			Matcher matcherForMiniBus = patternForMiniBus.matcher(lineInMiniBuses);
			if (matcherForMiniBus.matches()) {
				LOGGER.info("Starting to parse a minibus ...");
				MiniBus miniBus = null;
				try {
					miniBus = readerMiniBus.parseElement(bufferedReader);
					listMiniBus.add(miniBus);
					numberMiniBusWithWrongData++;
				} catch (ElementReadFromFileException exception) {
					wrongInformationAboutParseListMiniBus.append("MiniBus " + numberMiniBusWithWrongData + "th has wrong data : "+ exception.getMessage() +"<br>");
					LOGGER.error("There are next complexities with creating " + numberMiniBusWithWrongData++ + "th minibus :", exception);
				}
			}

			Matcher matcherEndListMiniBus = patternEndListMiniBus.matcher(lineInMiniBuses);
			if (matcherEndListMiniBus.matches()) {
				miniBusService.addMiniBuses(listMiniBus);
				isLineInMiniBuses = false;
				LOGGER.info("Minibus list was read from the file with next result : " + listMiniBus);
			}
		}
		return wrongInformationAboutParseListMiniBus.toString();
	}
}
