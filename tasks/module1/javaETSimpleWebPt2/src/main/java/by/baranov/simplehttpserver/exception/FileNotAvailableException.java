package by.baranov.simplehttpserver.exception;

import java.io.IOException;

public class FileNotAvailableException extends IOException {

	public FileNotAvailableException(String message, Throwable cause) {
		super(message, cause);
	}

	public FileNotAvailableException(String message) {
		super(message);
	}

}
