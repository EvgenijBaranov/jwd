package by.baranov.simplehttpserver.service;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;

import by.baranov.simplehttpserver.model.Taxi;

public interface TaxiService extends CRUDOperation<Taxi>{

	void addTaxies(List<Taxi> listTaxies);
	
	BigDecimal calculateCostTaxies();
	
	void sortTaxiesByParameters(Comparator<? super Taxi> comparator);
}
