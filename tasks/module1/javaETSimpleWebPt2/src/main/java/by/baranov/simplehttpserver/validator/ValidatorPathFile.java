package by.baranov.simplehttpserver.validator;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

public class ValidatorPathFile implements Validator {
	private static final Logger LOGGER = Logger.getLogger(ValidatorPathFile.class);

	@Override
	public boolean validate(Map<String, String> mapParametersFromFile) {
		
		String inputedPathFile = mapParametersFromFile.get("filePath");
		LOGGER.info(this.getClass().getSimpleName() + " has received next file path : " + inputedPathFile);
		
		Path path = Paths.get(inputedPathFile);
		
		String extensionFile = FilenameUtils.getExtension(inputedPathFile);
		
		return Files.isReadable(path) && "txt".equalsIgnoreCase(extensionFile);
	}

}
