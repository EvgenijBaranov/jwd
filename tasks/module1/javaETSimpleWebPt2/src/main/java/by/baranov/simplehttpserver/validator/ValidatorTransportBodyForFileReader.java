package by.baranov.simplehttpserver.validator;

import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.exception.DataFromFileOutOfBoundException;
import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.model.TransportBody;

public class ValidatorTransportBodyForFileReader implements Validator {
	
	private static final Logger LOGGER = Logger.getLogger(ValidatorTransportBodyForFileReader.class);
	private final Validator validatorTransportElement = new ValidatorElementOfTransportForFileReader();
	
	@Override
	public boolean validate(Map<String, String> mapParametersFromFile) {
		LOGGER.info("Try to validate parameters for body of the transport in the file : " + mapParametersFromFile);
		Set<String> transportBodyFields = ValidatorUtils.getFieldsOfElement(new TransportBody());

		Set<String> parameterNames = mapParametersFromFile.keySet();

		if (!parameterNames.containsAll(transportBodyFields)) {
			LOGGER.error("The names of the transport body parameters in the file : " + parameterNames
					+ "\n don't correspond to the names of the fields in transportBody model : " + transportBodyFields);
			return false;
		}

		LOGGER.info( "The names of the transport body parameters in the file : " + parameterNames 
					+ "\n  correspond to the names of the fields in transportBody model : " + transportBodyFields);
		
		boolean idAndDateofManufactureIsValid = validatorTransportElement.validate(mapParametersFromFile);
		
		boolean capacityValueIsValid = false;
		try {
			String capacityValue = mapParametersFromFile.get("capacity");
			double capacity = Double.parseDouble(capacityValue);
			if (capacity < TransportBody.MIN_CAPACITY || capacity > TransportBody.MAX_CAPACITY) {
				throw new DataFromFileOutOfBoundException("Capacity of the transport body get out of established limits : " 
						+ "[" + TransportBody.MIN_CAPACITY + " - " + TransportBody.MAX_CAPACITY + "]");
			}
			capacityValueIsValid = true;
		} catch (DataFromFileOutOfBoundException exception) {
			LOGGER.error("Invalid value for capacity of the transport body in the file ", exception);
		} catch (NumberFormatException exception) {
			LOGGER.error("The capacity value for the transport body must be double number ", exception);
		} catch (NullPointerException exception) {
			LOGGER.error("The file must have parameter name \"capacity\" for transport body ", exception);
		}
		
		boolean materalIsExist = false;
		try {
			String materialValue = mapParametersFromFile.get("material");
			Material.valueOf(Material.class, materialValue.toUpperCase());
			materalIsExist = true;
		} catch (IllegalArgumentException exception) {
			LOGGER.error("Invalid value for material of the transport body in the file. ", exception);
		} catch (NullPointerException exception) {
			LOGGER.error("The file must have parameter name \"material\" for transport body ", exception);
		}
		
		return idAndDateofManufactureIsValid && capacityValueIsValid && materalIsExist;
	}
}
