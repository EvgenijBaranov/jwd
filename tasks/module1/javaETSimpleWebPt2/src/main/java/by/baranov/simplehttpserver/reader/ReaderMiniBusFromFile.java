package by.baranov.simplehttpserver.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.exception.ElementReadFromFileException;
import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.MiniBus;
import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.validator.Validator;
import by.baranov.simplehttpserver.validator.ValidatorMiniBusForFileReader;
import by.baranov.simplehttpserver.validator.ValidatorUtils;

public class ReaderMiniBusFromFile implements ReaderElement<MiniBus> {
	private static final Logger LOGGER = Logger.getLogger(ReaderMiniBusFromFile.class);
	private final ReaderElement<TransportBody> readerBody;
	private final ReaderElement<Engine> readerEngine;
	private final ReaderElement<List<Wheel>> readerWheels;
	private final Validator validatorMiniBus = new ValidatorMiniBusForFileReader();

	public ReaderMiniBusFromFile(ReaderElement<TransportBody> readerBody, ReaderElement<Engine> readerEngine,
									ReaderElement<List<Wheel>> readerWheels) {
		this.readerBody = readerBody;
		this.readerEngine = readerEngine;
		this.readerWheels = readerWheels;
	}

	@Override
	public MiniBus parseElement(BufferedReader bufferedReader) throws IOException {
		Map<String, String> mapParametersMiniBus = new HashMap<>();

		TransportBody body = null;
		Engine engine = null;
		List<Wheel> wheels = null;

		Pattern patternForBody = Pattern.compile("\\s*transportBody=\\s*\\{\\s*");
		Pattern patternForEngine = Pattern.compile("\\s*engine=\\s*\\{\\s*");
		Pattern patternForWheels = Pattern.compile("\\s*wheels=\\s*\\[\\s*");
		Pattern patternForEndMiniBus = Pattern.compile("\\s*\\}{2},*\\s*");

		boolean isLineInMiniBus = true;
		while (isLineInMiniBus) {

			String lineInMiniBus = bufferedReader.readLine();
			Matcher matcherForMiniBus = patternForEndMiniBus.matcher(lineInMiniBus);

			Matcher matcherForBody = patternForBody.matcher(lineInMiniBus);
			if (matcherForBody.matches()) {
				LOGGER.info("Starting to parse the minibus body beginning with string : " + lineInMiniBus);
				body = readerBody.parseElement(bufferedReader);

			}

			Matcher matcherForEngine = patternForEngine.matcher(lineInMiniBus);
			if (matcherForEngine.matches()) {
				LOGGER.info("Starting to parse the minibus engine beginning with string : " + lineInMiniBus);
				engine = readerEngine.parseElement(bufferedReader);

			}

			Matcher matcherForWheels = patternForWheels.matcher(lineInMiniBus);
			if (matcherForWheels.matches()) {
				LOGGER.info("Starting to parse the minibus wheels beginning with string : " + lineInMiniBus);
				wheels = readerWheels.parseElement(bufferedReader);

			}

			if (matcherForMiniBus.matches()) {
				isLineInMiniBus = false;
			}

			LOGGER.info("Starting to parse the minibus parameters beginning with string : " + lineInMiniBus);
			lineInMiniBus = lineInMiniBus.replace(",", "").trim();
			String[] separateParameterAndValue = lineInMiniBus.split("=");
			String parameterName = separateParameterAndValue[0];
			String parameterValue = separateParameterAndValue.length == 2 ? separateParameterAndValue[1] : "";
			mapParametersMiniBus.put(parameterName, parameterValue);

		}

		LOGGER.info("The map for minibus with parameter names and values was created with next result : "
				+ mapParametersMiniBus);

		MiniBus miniBus = null;

		if (validatorMiniBus.validate(mapParametersMiniBus)
				&& ValidatorUtils.isValidQuantityAndRadiusWheelsFromFile(wheels, MiniBus.NUMBERS_WHEEL)) {
			String registrationNumber = mapParametersMiniBus.get("registrationNumber");

			String brandValue = mapParametersMiniBus.get("brand");
			BrandTransport brand = BrandTransport.valueOf(brandValue);
			
			String costValue = mapParametersMiniBus.get("cost");
			BigDecimal cost = new BigDecimal(costValue);

			miniBus = new MiniBus(registrationNumber, brand, body, engine, wheels, cost);
			LOGGER.info("The minibus was read from the file succesfully : " + miniBus);

		} else {
			throw new ElementReadFromFileException("The minibus wasn't read from the file, because of wrong data in minibus parameters.");
		}

		return miniBus;
	}
}
