package by.baranov.simplehttpserver.repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.TaxiCargo;
import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.specification.Specification;

public class RepositoryTaxiCargoImpl implements Repository<TaxiCargo> {
	
	private static final Logger LOGGER = Logger.getLogger(RepositoryTaxiCargoImpl.class);
	private List<TaxiCargo> taxiesCargo = new ArrayList<>();
	private Random rand = new Random();

	public RepositoryTaxiCargoImpl(int numberTaxiesCargoCreatedInRepository, Repository<TransportBody> bodies,
										Repository<Engine> engines, Repository<Wheel> wheels) {

		for (int i = 0; i < numberTaxiesCargoCreatedInRepository; i++) {

			Engine engine = engines.getRandom();
			TransportBody body = bodies.getRandom();
			Wheel wheel = wheels.getRandom();
			List<Wheel> taxiWheels = new ArrayList<>();
			for (int j = 0; j < TaxiCargo.NUMBERS_WHEEL; j++) {
				taxiWheels.add(wheel);
			}

			int sizeEnumBrandTrasport = BrandTransport.values().length;
			int randomIndexEnumBrandTransport = rand.nextInt(sizeEnumBrandTrasport);
			BrandTransport brand = BrandTransport.values()[randomIndexEnumBrandTransport];

			double randomMaxPermissibleMassOfGoods = getMaxPermissibleMassOfGoods();

			TaxiCargo taxiCargo = new TaxiCargo("number" + i, brand, body, engine, taxiWheels,
					randomMaxPermissibleMassOfGoods, new BigDecimal((i+1)*10*3));
			taxiesCargo.add(taxiCargo);
		}
	}

	private double getMaxPermissibleMassOfGoods() {
		return ThreadLocalRandom.current().nextDouble(TaxiCargo.MIN_VALUE_MASS_OF_GOODS,
				TaxiCargo.MAX_VALUE_MASS_OF_GOODS);
	}

	@Override
	public List<TaxiCargo> findSome(Specification<TaxiCargo> specification) {
		List<TaxiCargo> result = new ArrayList<>();
		for (TaxiCargo taxiCargo : taxiesCargo) {
			if (specification.match(taxiCargo)) {
				result.add(taxiCargo);
			}
		}
		return result;
	}

	@Override
	public List<TaxiCargo> getAll() {
		return taxiesCargo;
	}

	@Override
	public Optional<TaxiCargo> getBy(Specification<TaxiCargo> specification) {
		for (TaxiCargo taxiCargo : taxiesCargo) {
			if (specification.match(taxiCargo)) {
				return Optional.of(taxiCargo);
			}
		}
		LOGGER.warn(this.getClass().getSimpleName() + " didn't find a taxi cargo by : " + specification.getClass().getSimpleName());
		return Optional.empty();
	}

	@Override
	public TaxiCargo getRandom() {
		int sizeRepository = taxiesCargo.size();
		TaxiCargo randomTaxiCargo = taxiesCargo.get(rand.nextInt(sizeRepository));
		return randomTaxiCargo;
	}

	@Override
	public String toString() {
		return "Repository of taxi cargo : " + taxiesCargo;
	}

	@Override
	public void addElements(List<TaxiCargo> elements) {
		taxiesCargo.addAll(elements);
	}

	@Override
	public void sortRepositoryByParameters(Comparator<? super TaxiCargo> comparator) {
		Collections.sort(taxiesCargo, comparator);
		
	}

}
