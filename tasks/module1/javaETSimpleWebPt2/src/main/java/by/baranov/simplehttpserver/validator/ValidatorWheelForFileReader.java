package by.baranov.simplehttpserver.validator;

import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.exception.DataFromFileOutOfBoundException;
import by.baranov.simplehttpserver.model.Wheel;

public class ValidatorWheelForFileReader implements Validator {

	private static final Logger LOGGER = Logger.getLogger(ValidatorWheelForFileReader.class);
	private final Validator validatorTransportElement = new ValidatorElementOfTransportForFileReader();;

	@Override
	public boolean validate(Map<String, String> mapParametersFromFile) {
		LOGGER.info("Try to validate parameters for wheel in the file : " + mapParametersFromFile);
		Set<String> wheelFields = ValidatorUtils.getFieldsOfElement(new Wheel());

		Set<String> parameterNames = mapParametersFromFile.keySet();

		if (!parameterNames.containsAll(wheelFields)) {
			LOGGER.error("The names of the wheel parameters in the file : " + parameterNames
					+ "\n don't correspond to the names of the fields in wheel model : " + wheelFields);
			return false;
		}

		LOGGER.info("The names of the wheel parameters in the file : " + parameterNames
				+ "\n  correspond to the names of the fields in wheel model : " + wheelFields);

		boolean idAndDateOfManufactureIsValid = validatorTransportElement.validate(mapParametersFromFile);

		boolean radiusValueIsValid = false;
		try {
			String radiusValue = mapParametersFromFile.get("radius");
			int radius = Integer.parseInt(radiusValue);

			if (radius < Wheel.MIN_RADIUS || radius > Wheel.MAX_RADIUS) {
				throw new DataFromFileOutOfBoundException("Wheel radius get out of established limits : " + "["
						+ Wheel.MIN_RADIUS + " - " + Wheel.MAX_RADIUS + "]");
			}
			radiusValueIsValid = true;
		} catch (DataFromFileOutOfBoundException exception) {
			LOGGER.error("Invalid data for radius in the file ", exception);
		} catch (NumberFormatException exception) {
			LOGGER.error("The radius value for wheel must be integer number ", exception);
		}catch (NullPointerException exception) {
			LOGGER.error("The file must have parameter name \"radius\" for wheel ", exception);
		}
		return idAndDateOfManufactureIsValid && radiusValueIsValid;
	}
}
