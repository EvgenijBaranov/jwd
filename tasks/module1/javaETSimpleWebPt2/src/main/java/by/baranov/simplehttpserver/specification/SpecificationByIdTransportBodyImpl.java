package by.baranov.simplehttpserver.specification;

import by.baranov.simplehttpserver.model.TransportBody;

public class SpecificationByIdTransportBodyImpl implements Specification<TransportBody>{
	private long id;

	public SpecificationByIdTransportBodyImpl(long id) {
		this.id = id;
	}
	
	@Override
	public boolean match(TransportBody body) {
		return this.id == body.getId();
	}
}
