package by.baranov.simplehttpserver.specification;

import by.baranov.simplehttpserver.model.TransportBody;

public class SpecificationByIdBodyOfTransportImpl implements Specification<TransportBody>{
	private long id;

	public SpecificationByIdBodyOfTransportImpl(long id) {
		this.id = id;
	}
	
	@Override
	public boolean match(TransportBody body) {
		return this.id == body.getId();
	}
}
