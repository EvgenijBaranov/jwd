package by.baranov.simplehttpserver.repository;

import java.util.Comparator;
import java.util.List;

import by.baranov.simplehttpserver.service.CRUDOperation;
import by.baranov.simplehttpserver.specification.Specification;

public interface Repository<T> extends CRUDOperation<T>{
	
	List<T> findSome(Specification<T> specification);
	
	T getRandom();
	
	void addElements(List<T> elements);
	
	void sortRepositoryByParameters(Comparator<? super T> comparator);
	
}