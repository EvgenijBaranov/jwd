package by.baranov.simplehttpserver.exception;

public class CreateObjectImpossibleException extends RuntimeException {

	public CreateObjectImpossibleException(String message, Throwable cause) {
		super(message, cause);
	}

	public CreateObjectImpossibleException(String message) {
		super(message);
	}
}
