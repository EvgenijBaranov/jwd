package by.baranov.simplehttpserver.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.exception.ElementReadFromFileException;
import by.baranov.simplehttpserver.exception.FileNotAvailableException;
import by.baranov.simplehttpserver.model.TaxiCargo;
import by.baranov.simplehttpserver.service.TaxiCargoService;

public class ReaderListTaxiCargoFromFile implements ReaderListTransport {
	private static final Logger LOGGER = Logger.getLogger(ReaderListTaxiCargoFromFile.class);
	private final ReaderElement<TaxiCargo> readerTaxiCargo;
	private final TaxiCargoService taxiCargoService;

	public ReaderListTaxiCargoFromFile(ReaderElement<TaxiCargo> readerTaxiCargo, TaxiCargoService taxiCargoService) {
		this.readerTaxiCargo = readerTaxiCargo;
		this.taxiCargoService = taxiCargoService;
	}

	@Override
	public String parseListTransport(BufferedReader bufferedReader) throws IOException {
		List<TaxiCargo> listTaxiCargo = new ArrayList<>();
		long numberTaxiCargoWithWrongData = 1L;
		StringBuilder wrongInformationAboutParseListTaxiCargo = new StringBuilder();
		

		Pattern patternForTaxi = Pattern.compile("\\s*TaxiCargo\\s*\\{{2}\\s*");
		Pattern patternEndListTaxi = Pattern.compile("\\s*\\}\\],*\\s*");

		boolean isLineInTaxiesCargo = true;

		while (isLineInTaxiesCargo) {

			String lineInTaxiesCargo = null;
			try {
				lineInTaxiesCargo = bufferedReader.readLine();				
			} catch (IOException exception) {
				throw new FileNotAvailableException("Can't read the file. The file isn't available for reading.", exception);
			}

			Matcher matcherForTaxi = patternForTaxi.matcher(lineInTaxiesCargo);
			if (matcherForTaxi.matches()) {
				LOGGER.info("Starting to parse a taxi cargo ...");
				TaxiCargo taxiCargo = null;
				try {
					taxiCargo = readerTaxiCargo.parseElement(bufferedReader);
					listTaxiCargo.add(taxiCargo);
					numberTaxiCargoWithWrongData++;
				} catch (ElementReadFromFileException exception) {
					wrongInformationAboutParseListTaxiCargo.append("TaxiCargo " + numberTaxiCargoWithWrongData + "th has wrong data : "+ exception.getMessage() +"<br>");
					LOGGER.error("There are next complexities with creating " + numberTaxiCargoWithWrongData++ + "th minibus :", exception);
				}
			}

			Matcher matcherEndListTaxi = patternEndListTaxi.matcher(lineInTaxiesCargo);
			if (matcherEndListTaxi.matches()) {
				taxiCargoService.addTaxiesCargo(listTaxiCargo);
				isLineInTaxiesCargo = false;
				LOGGER.info("Taxi cargo list was read from the file with next result : " + listTaxiCargo);
			}
		}
		return wrongInformationAboutParseListTaxiCargo.toString();
	}
}
