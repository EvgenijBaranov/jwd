package by.baranov.simplehttpserver.service.impl;

import java.util.List;
import java.util.Optional;

import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.TransportBodyService;
import by.baranov.simplehttpserver.specification.Specification;

public class TransportBodyServiceImpl implements TransportBodyService {
	private final Repository<TransportBody> repository;

	public TransportBodyServiceImpl(Repository<TransportBody> repository) {
		this.repository = repository;
	}

	@Override
	public List<TransportBody> getAll() {
		return repository.getAll();
	}

	@Override
	public Optional<TransportBody> getBy(Specification<TransportBody> specification) {
		return repository.getBy(specification);
	}

}
