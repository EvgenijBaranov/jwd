package by.baranov.simplehttpserver.service.impl;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import by.baranov.simplehttpserver.model.MiniBus;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.MiniBusService;
import by.baranov.simplehttpserver.specification.Specification;

public class MiniBusServiceImpl implements MiniBusService {
	private final Repository<MiniBus> repositoryMiniBus;

	public MiniBusServiceImpl(Repository<MiniBus> repositoryMiniBus) {
		this.repositoryMiniBus = repositoryMiniBus;
	}

	@Override
	public List<MiniBus> getAll() {
		return repositoryMiniBus.getAll();
	}

	@Override
	public Optional<MiniBus> getBy(Specification<MiniBus> specification) {
		return repositoryMiniBus.getBy(specification);
	}

	@Override
	public void addMiniBuses(List<MiniBus> listMiniBuses) {
		repositoryMiniBus.addElements(listMiniBuses);
	}

	@Override
	public BigDecimal calculateCostMiniBuses() {
		List<MiniBus> listMiniBus = repositoryMiniBus.getAll();
		BigDecimal costListMiniBus = listMiniBus.stream()
												.map(MiniBus::getCost)
												.reduce(BigDecimal.ZERO, BigDecimal::add);
		return costListMiniBus;
	}

	@Override
	public void sortMiniBusesByParameters(Comparator<? super MiniBus> comparator) {
		repositoryMiniBus.sortRepositoryByParameters(comparator);
	}

}
