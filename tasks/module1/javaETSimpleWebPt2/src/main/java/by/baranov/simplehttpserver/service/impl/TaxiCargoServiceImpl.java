package by.baranov.simplehttpserver.service.impl;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import by.baranov.simplehttpserver.model.TaxiCargo;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.TaxiCargoService;
import by.baranov.simplehttpserver.specification.Specification;

public class TaxiCargoServiceImpl implements TaxiCargoService {
	private final Repository<TaxiCargo> repositoryTaxiCargo;

	public TaxiCargoServiceImpl(Repository<TaxiCargo> repositoryTaxiCargo) {
		this.repositoryTaxiCargo = repositoryTaxiCargo;
	}

	@Override
	public List<TaxiCargo> getAll() {
		return repositoryTaxiCargo.getAll();
	}

	@Override
	public Optional<TaxiCargo> getBy(Specification<TaxiCargo> specification) {
		return repositoryTaxiCargo.getBy(specification);
	}

	@Override
	public void addTaxiesCargo(List<TaxiCargo> listTaxiesCargo) {
		repositoryTaxiCargo.addElements(listTaxiesCargo);
	}

	@Override
	public BigDecimal calculateCostTaxiesCargo() {
		List<TaxiCargo> listTaxiCargo = repositoryTaxiCargo.getAll();
		BigDecimal costListTaxiCargo = listTaxiCargo.stream()
													.map(TaxiCargo::getCost)
													.reduce(BigDecimal.ZERO, BigDecimal::add);
		return costListTaxiCargo;
	}

	@Override
	public void sortTaxiesCargoByParameters(Comparator<? super TaxiCargo> comparator) {
		repositoryTaxiCargo.sortRepositoryByParameters(comparator);
	}

}
