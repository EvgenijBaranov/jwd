package by.baranov.simplehttpserver.reader;

import java.io.BufferedReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.exception.ElementReadFromFileException;
import by.baranov.simplehttpserver.exception.FileNotAvailableException;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.validator.ValidatorEngineForFileReader;
import by.baranov.simplehttpserver.validator.Validator;

public class ReaderEngineFromFile implements ReaderElement<Engine> {
	private static final Logger LOGGER = Logger.getLogger(ReaderEngineFromFile.class);
	private final Validator engineValidator = new ValidatorEngineForFileReader();

	@Override
	public Engine parseElement(BufferedReader bufferedReader) throws FileNotAvailableException, ElementReadFromFileException {

		Map<String, String> mapParamemetrsEngine = ReaderUtility.getTransportElementParameters(bufferedReader);

		Engine engine = null;

		if (engineValidator.validate(mapParamemetrsEngine)) {

			String idValue = mapParamemetrsEngine.get("id");
			long id = Long.parseLong(idValue);

			String producer = mapParamemetrsEngine.get("producer");

			String powerValue = mapParamemetrsEngine.get("power");
			double power = Double.parseDouble(powerValue);

			String dateOfManufactureValue = mapParamemetrsEngine.get("dateOfManufacture");
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate dateOfManufacture = LocalDate.parse(dateOfManufactureValue, formatter);

			engine = new Engine(id, power, producer, dateOfManufacture);

			LOGGER.info("The engine was read from the file succesfully : " + engine);
		} else {
			throw new ElementReadFromFileException("The engine wasn't read from the file, because of wrong data.");
		}
		return engine;
	}

}
