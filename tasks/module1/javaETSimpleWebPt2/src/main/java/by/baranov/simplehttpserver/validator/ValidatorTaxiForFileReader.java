package by.baranov.simplehttpserver.validator;

import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.exception.DataFromFileOutOfBoundException;
import by.baranov.simplehttpserver.model.Taxi;

public class ValidatorTaxiForFileReader implements Validator {

	private static final Logger LOGGER = Logger.getLogger(ValidatorTaxiForFileReader.class);
	private final Validator validatorAbstractTransport = new ValidatorTransportForFileReader();
	
	@Override
	public boolean validate(Map<String, String> mapParametersFromFile) {
		LOGGER.info("Try to validate parameters for taxi in the file : " + mapParametersFromFile);
		Set<String> taxiFields = ValidatorUtils.getFieldsOfElement(new Taxi());

		Set<String> parameterNames = mapParametersFromFile.keySet();
		if (!parameterNames.containsAll(taxiFields)) {
			LOGGER.error("The names of the taxi parameters in the file : " + parameterNames
					+ "\n don't correspond to the names of the fields in taxi model : " + taxiFields);
			return false;
		}

		LOGGER.info("The names of the taxi parameters in the file : " + parameterNames
				+ "\n  correspond to the names of the fields in taxi model : " + taxiFields);

		boolean dataForTransportIsValid = validatorAbstractTransport.validate(mapParametersFromFile);
		
		boolean currentQuantityPassengersValueIsValid = false;
		try {
			String currentQuantityPassengersValue = mapParametersFromFile.get("currentQuantityPassengers");
			int currentQuantityPassengers = Integer.parseInt(currentQuantityPassengersValue);

			if (currentQuantityPassengers > Taxi.MAX_QUANTITY_PASSENGERS) {
				throw new DataFromFileOutOfBoundException("Value for \"currentQuantityPassengers\" in the taxi gets out of established limits : " 
						+ "value must be <= "+ Taxi.MAX_QUANTITY_PASSENGERS);
			}
			currentQuantityPassengersValueIsValid = true;
		} catch (DataFromFileOutOfBoundException exception) {
			LOGGER.error("Invalid data for \"currentQuantityPassengers\" in the file ", exception);
		} catch (NumberFormatException exception) {
			LOGGER.error("The \"currentQuantityPassengers\" value for taxi in the file must be integer number ", exception);
		}catch (NullPointerException exception) {
			LOGGER.error("The file must have parameter name \"currentQuantityPassengers\" for taxi ", exception);
		}

		return dataForTransportIsValid && currentQuantityPassengersValueIsValid;
	}

}
