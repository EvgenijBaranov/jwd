package by.baranov.simplehttpserver.command;

import java.util.Comparator;
import java.util.Map;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.model.AbstractTransport;
import by.baranov.simplehttpserver.service.MiniBusService;
import by.baranov.simplehttpserver.service.TaxiCargoService;
import by.baranov.simplehttpserver.service.TaxiService;

public class SortTaxiParkByParametersCommand implements AppCommand {
	private static final Logger LOGGER = Logger.getLogger(SortTaxiParkByParametersCommand.class);
	private final TaxiService taxiService;
	private final TaxiCargoService taxiCargoService;
	private final MiniBusService miniBusService;
	private final Map<String, Comparator<AbstractTransport>> mapComparatorsForParametersFromPOSTRequest;

	public SortTaxiParkByParametersCommand(TaxiService taxiService, TaxiCargoService taxiCargoService, MiniBusService miniBusService,
			Map<String, Comparator<AbstractTransport>> mapComparatorsForParametersFromPOSTRequest) {
		this.taxiService = taxiService;
		this.taxiCargoService = taxiCargoService;
		this.miniBusService = miniBusService;
		this.mapComparatorsForParametersFromPOSTRequest = mapComparatorsForParametersFromPOSTRequest;
		LOGGER.info(this.getClass().getSimpleName() + " is created with services : "
				+ taxiService.getClass().getSimpleName() + taxiCargoService.getClass().getSimpleName()
				+ miniBusService.getClass().getSimpleName());
	}

	@Override
	public String execute(Map<String, String> mapCommandNameAndParameters) {
		
		String firstParameter = mapCommandNameAndParameters.get("firstParameter");
		String secondParameter = mapCommandNameAndParameters.get("secondParameter");
		
		String keyInMapComparators = firstParameter.concat(" & ").concat(secondParameter);
		
		Comparator<AbstractTransport> comparatorForSortRepository = mapComparatorsForParametersFromPOSTRequest.get(keyInMapComparators);
		
		taxiService.sortTaxiesByParameters(comparatorForSortRepository);
		taxiCargoService.sortTaxiesCargoByParameters(comparatorForSortRepository);
		miniBusService.sortMiniBusesByParameters(comparatorForSortRepository);
		
		LOGGER.info(this.getClass().getSimpleName() + " is executed");
		return "<h4>Taxipark is sorted by selected parameters. Click \"Show taxipark\" to display the sorted taxipark.</h4>";
	}
}
