package by.baranov.simplehttpserver.command;

import java.util.Map;
import java.util.Optional;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.model.MiniBus;
import by.baranov.simplehttpserver.service.MiniBusService;
import by.baranov.simplehttpserver.specification.Specification;
import by.baranov.simplehttpserver.specification.SpecificationByMiniBusNumberImpl;

public class GetMiniBusByMiniBusNumberCommand implements AppCommand {
	private static final Logger LOGGER = Logger.getLogger(GetMiniBusByMiniBusNumberCommand.class);
	private final MiniBusService miniBusService;

	public GetMiniBusByMiniBusNumberCommand(MiniBusService miniBusService) {
		this.miniBusService = miniBusService;
		LOGGER.info(this.getClass().getSimpleName() + " is created with services : "
				+ miniBusService.getClass().getSimpleName());
	}

	@Override
	public String execute(Map<String, String> mapCommandNameAndParameters) {
		String inputedMiniBusNumber = mapCommandNameAndParameters.getOrDefault("miniBusNumber",
								"The map of command names and parameters hasn't name \"miniBusNumber\" in the html file");
		LOGGER.info(this.getClass().getSimpleName() + " has recieved next minibus number : " + inputedMiniBusNumber);
		Specification<MiniBus> spec = new SpecificationByMiniBusNumberImpl(inputedMiniBusNumber);
		Optional<MiniBus> miniBus = miniBusService.getBy(spec);
		LOGGER.info(this.getClass().getSimpleName() + " has received minibus : " + miniBus);

		StringBuilder result = new StringBuilder();
		result.append("<h2>Minibus with number = " + inputedMiniBusNumber + "</h2>");
		result.append("<ul>");
		result.append("<li>");
		String resultMiniBus = miniBus.isPresent() ? miniBus.get().toString()
				: "There aren't such registartion number of the minibus in the taxi park";
		result.append(resultMiniBus);
		result.append("</li>");
		result.append("</ul>");
		LOGGER.info(this.getClass().getSimpleName() + " is executed");
		return result.toString();
	}

}
