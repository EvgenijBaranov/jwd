package by.baranov.simplehttpserver.repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.MiniBus;
import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.specification.Specification;

public class RepositoryMiniBusImpl implements Repository<MiniBus> {

	private static final Logger LOGGER = Logger.getLogger(RepositoryMiniBusImpl.class);
	private List<MiniBus> miniBuses = new ArrayList<>();
	private Random rand = new Random();

	public RepositoryMiniBusImpl(int numberMiniBusesCreatedInRepository, Repository<TransportBody> bodies,
									Repository<Engine> engines, Repository<Wheel> wheels) {

		for (int i = 0; i < numberMiniBusesCreatedInRepository; i++) {

			Engine engine = engines.getRandom();
			TransportBody body = bodies.getRandom();
			Wheel wheel = wheels.getRandom();
			List<Wheel> miniBusWheels = new ArrayList<>();
			for (int j = 0; j < MiniBus.NUMBERS_WHEEL; j++) {
				miniBusWheels.add(wheel);
			}

			int sizeEnumBrandTrasport = BrandTransport.values().length;
			int randomIndexEnumBrandTransport = rand.nextInt(sizeEnumBrandTrasport);
			BrandTransport brand = BrandTransport.values()[randomIndexEnumBrandTransport];

			MiniBus miniBus = new MiniBus("number" + i, brand, body, engine, miniBusWheels, new BigDecimal((i+1)*10*2));
			miniBuses.add(miniBus);
		}
	}

	@Override
	public List<MiniBus> findSome(Specification<MiniBus> specification) {
		List<MiniBus> result = new ArrayList<>();
		for (MiniBus miniBus : miniBuses) {
			if (specification.match(miniBus)) {
				result.add(miniBus);
			}
		}
		return result;
	}

	@Override
	public List<MiniBus> getAll() {
		return miniBuses;
	}

	@Override
	public Optional<MiniBus> getBy(Specification<MiniBus> specification) {
		for (MiniBus miniBus : miniBuses) {
			if (specification.match(miniBus)) {
				return Optional.of(miniBus);
			}
		}
		LOGGER.warn(this.getClass().getSimpleName() + " didn't find a minibus by : " + specification.getClass().getSimpleName());
		return Optional.empty();
	}

	@Override
	public MiniBus getRandom() {
		int sizeRepository = miniBuses.size();
		MiniBus randomMiniBus = miniBuses.get(rand.nextInt(sizeRepository));
		return randomMiniBus;
	}

	@Override
	public String toString() {
		return "Repository of miniBuses : " + miniBuses;
	}

	@Override
	public void addElements(List<MiniBus> elements) {
		miniBuses.addAll(elements);
	}

	@Override
	public void sortRepositoryByParameters(Comparator<? super MiniBus> comparator) {
		Collections.sort(miniBuses, comparator);
	}
	
}
