package by.baranov.simplehttpserver.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

public class ReaderTaxiParkFromFile {
	private static final Logger LOGGER = Logger.getLogger(ReaderTaxiParkFromFile.class);
	private final ReaderListTransport readerTaxies;
	private final ReaderListTransport readerTaxiesCargo;
	private final ReaderListTransport readerMiniBuses;

	public ReaderTaxiParkFromFile(ReaderListTransport readerTaxies,	ReaderListTransport readerTaxiesCargo, ReaderListTransport readerMiniBuses) {
		this.readerTaxies = readerTaxies;
		this.readerTaxiesCargo = readerTaxiesCargo;
		this.readerMiniBuses = readerMiniBuses;
	}

	public String parseTaxiPark(BufferedReader bufferedReader) throws IOException {
		StringBuilder wrongInformationAboutParseTaxiPark = new StringBuilder();

		Pattern patternForListTaxi = Pattern.compile("\\s*Taxies:\\s*\\[\\{\\s*");
		Pattern patternForListTaxiCargo = Pattern.compile("\\s*TaxiesCargo:\\s*\\[\\{\\s*");
		Pattern patternForListMiniBus = Pattern.compile("\\s*MiniBuses:\\s*\\[\\{\\s*");

		String lineInFile = null;
		
		while ((lineInFile = bufferedReader.readLine()) != null) {

			Matcher matcherForListTaxi = patternForListTaxi.matcher(lineInFile);
			if (matcherForListTaxi.matches()) {
				LOGGER.info("Starting to parse a taxi list ...");
				wrongInformationAboutParseTaxiPark.append("<h4> Taxi: </h4>");
				wrongInformationAboutParseTaxiPark.append(readerTaxies.parseListTransport(bufferedReader));
			}

			Matcher matcherForListTaxiCargo = patternForListTaxiCargo.matcher(lineInFile);
			if (matcherForListTaxiCargo.matches()) {
				LOGGER.info("Starting to parse a taxi cargo list ...");
				wrongInformationAboutParseTaxiPark.append("<h4> TaxiCargo: </h4>");
				wrongInformationAboutParseTaxiPark.append(readerTaxiesCargo.parseListTransport(bufferedReader));
			}

			Matcher matcherForListMiniBus = patternForListMiniBus.matcher(lineInFile);
			if (matcherForListMiniBus.matches()) {
				LOGGER.info("Starting to parse a minibus list ...");
				wrongInformationAboutParseTaxiPark.append("<h4> MiniBus: </h4>");
				wrongInformationAboutParseTaxiPark.append(readerMiniBuses.parseListTransport(bufferedReader));
			}
		}

		return wrongInformationAboutParseTaxiPark.toString();
	}

}
