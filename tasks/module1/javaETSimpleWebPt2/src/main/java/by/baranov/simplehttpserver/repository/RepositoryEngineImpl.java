package by.baranov.simplehttpserver.repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.specification.Specification;

public class RepositoryEngineImpl implements Repository<Engine> {
	
	private static final Logger LOGGER = Logger.getLogger(RepositoryEngineImpl.class);
	private List<Engine> engines = new ArrayList<>();
	private Random rand = new Random();

	public RepositoryEngineImpl() {
		Engine enegine0 = new Engine(0, getPower(), "Hyundai", LocalDate.of(2010, 9, 04));
		Engine enegine1 = new Engine(1, getPower(), "Daimler-Benz", LocalDate.of(1990, 10, 10));
		Engine enegine2 = new Engine(2, getPower(), "Honda", LocalDate.of(1991, 11, 11));
		Engine enegine3 = new Engine(3, getPower(), "Honda", LocalDate.of(1992, 12, 12));
		Engine enegine4 = new Engine(4, getPower(), "General Motors", LocalDate.of(1993, 2, 20));

		engines.add(enegine0);
		engines.add(enegine1);
		engines.add(enegine2);
		engines.add(enegine3);
		engines.add(enegine4);
	}

	private double getPower() {
		return ThreadLocalRandom.current().nextDouble(Engine.MIN_POWER, Engine.MAX_POWER);
	}

	@Override
	public List<Engine> findSome(Specification<Engine> specification) {
		List<Engine> result = new ArrayList<>();

		for (Engine engine : engines) {
			if (specification.match(engine)) {
				result.add(engine);
			}
		}
		return result;
	}

	@Override
	public Optional<Engine> getBy(Specification<Engine> specification) {
		for (Engine engine : engines) {
			if (specification.match(engine)) {
				return Optional.ofNullable(engine);
			}
		}
		LOGGER.warn(this.getClass().getSimpleName() + " didn't find a engine by : " + specification.getClass().getSimpleName());
		return Optional.empty();
	}

	@Override
	public List<Engine> getAll() {
		return engines;
	}

	@Override
	public Engine getRandom() {
		int sizeRepository = engines.size();
		Engine randomEngine = engines.get(rand.nextInt(sizeRepository));
		return randomEngine;
	}

	@Override
	public String toString() {
		return "Repository of engines for transport : " + engines;
	}

	@Override
	public void addElements(List<Engine> elements) {
		engines.addAll(elements);
	}

	@Override
	public void sortRepositoryByParameters(Comparator<? super Engine> comparator) {
		Collections.sort(engines, comparator);
	}
	
}
