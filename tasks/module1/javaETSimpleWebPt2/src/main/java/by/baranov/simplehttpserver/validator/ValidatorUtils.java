package by.baranov.simplehttpserver.validator;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.exception.CreateObjectImpossibleException;
import by.baranov.simplehttpserver.model.Wheel;

public class ValidatorUtils {
	private static final Logger LOGGER = Logger.getLogger(ValidatorUtils.class);

	private ValidatorUtils() {
		throw new CreateObjectImpossibleException("Utility class ValidatorUtility can't construct objects");
	}

	public static <T> Set<String> getFieldsOfElement(T element) {

		Set<String> fields = new HashSet<>();
		Class<?> current = element.getClass();

		while (current.getSuperclass() != null) {
			for (Field field : current.getDeclaredFields()) {
				if (!Modifier.isStatic(field.getModifiers())) {
					fields.add(field.getName());
				}
			}
			current = current.getSuperclass();
		}
		return fields;
	}

	public static boolean isValidQuantityAndRadiusWheelsFromFile(List<Wheel> wheels, int quantityWheelsInTransport) {
		boolean isValidRadius = isWheelsHaveEqualRadius(wheels);
		if (wheels.size() != quantityWheelsInTransport) {
			LOGGER.error("There are not valid quantity of wheels in transport. "
					+ "The quantity wheels for the transport must be = " + quantityWheelsInTransport);
		}
		return isValidRadius && wheels.size() == quantityWheelsInTransport;
	}
	
	private static boolean isWheelsHaveEqualRadius(List<Wheel> wheels) {
		Wheel wheelForComparison = wheels.get(0);
		for(Wheel wheel: wheels) {
			if(wheel.getRadius() != wheelForComparison.getRadius()) {
				LOGGER.error("The wheels has different radius in one transport");
				return false;
			}
		}
		return true;
	}

}
