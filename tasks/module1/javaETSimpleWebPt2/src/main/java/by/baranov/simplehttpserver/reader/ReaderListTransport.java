package by.baranov.simplehttpserver.reader;

import java.io.BufferedReader;
import java.io.IOException;

public interface ReaderListTransport {

	String parseListTransport(BufferedReader bufferedReader) throws IOException;
	
}
