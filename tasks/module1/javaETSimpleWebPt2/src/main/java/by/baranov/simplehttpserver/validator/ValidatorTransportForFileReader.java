package by.baranov.simplehttpserver.validator;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.exception.DataFromFileOutOfBoundException;
import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Material;

public class ValidatorTransportForFileReader implements Validator {
	
	private static final Logger LOGGER = Logger.getLogger(ValidatorTransportForFileReader.class);
	
	@Override
	public boolean validate(Map<String, String> mapParametersFromFile) {
		
		boolean registartionNumberValueIsValid = false;
		try {
			mapParametersFromFile.get("registrationNumber");
			registartionNumberValueIsValid = true;
		} catch (NullPointerException exception) {
			LOGGER.error("The file must have parameter name \"registrationNumber\" for transport ", exception);
		}
		
		boolean brandIsExist = false;
		try {
			String brandValue = mapParametersFromFile.get("brand");
			Material.valueOf(BrandTransport.class, brandValue.toUpperCase());
			brandIsExist = true;
		} catch (IllegalArgumentException exception) {
			LOGGER.error("Invalid value for brand of the transport in the file. ", exception);
		} catch (NullPointerException exception) {
			LOGGER.error("The file must have parameter name \"brand\" for transport ", exception);
		}
		
		boolean costIsValid = false;
		try {
			String costValue = mapParametersFromFile.get("cost");
			BigDecimal cost = new BigDecimal(costValue);
			if(cost.compareTo(BigDecimal.ZERO) < 0) {
				throw new DataFromFileOutOfBoundException("Cost of the transport can't be less then zero");
			}
			costIsValid = true;
		} catch (DataFromFileOutOfBoundException exception) {
			LOGGER.error("Invalid value for cost of the transport in the file. ", exception);
		}catch (NumberFormatException exception) {
			LOGGER.error("Invalid value for transport cost in the file. The cost value must be number. ", exception);
		} catch (NullPointerException exception) {
			LOGGER.error("The file must have parameter name \"cost\" for transport. ", exception);
		}
		
		return registartionNumberValueIsValid && brandIsExist && costIsValid;
	}
}
