package by.baranov.simplehttpserver.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.exception.FileNotAvailableException;
import by.baranov.simplehttpserver.model.Wheel;

public class ReaderListWheelFromFile implements ReaderElement<List<Wheel>> {
	private static final Logger LOGGER = Logger.getLogger(ReaderListWheelFromFile.class);
	private final ReaderElement<Wheel> readerWheel;

	public ReaderListWheelFromFile(ReaderElement<Wheel> readerWheel) {
		this.readerWheel = readerWheel;
	}

	@Override
	public List<Wheel> parseElement(BufferedReader bufferedReader) throws IOException {
		List<Wheel> wheels = new ArrayList<>();

		Pattern patternForWheel = Pattern.compile("\\s*wheel\\s*\\{\\s*");
		Pattern patternForEndListWheels = Pattern.compile("\\s*\\],*\\s*");

		boolean isLineInWheels = true;
		while (isLineInWheels) {
			String lineInWheels;
			try {
				lineInWheels = bufferedReader.readLine();
			} catch (IOException exception) {
				throw new FileNotAvailableException(
						"Can't read the file for wheels. The file isn't available for reading.", exception);
			}

			Matcher matcherForWheel = patternForWheel.matcher(lineInWheels);
			if (matcherForWheel.matches()) {
				LOGGER.info("Starting to parse a wheel ...");
				Wheel wheel = readerWheel.parseElement(bufferedReader);
				wheels.add(wheel);
			}

			Matcher matcherForEndListWheels = patternForEndListWheels.matcher(lineInWheels);
			if (matcherForEndListWheels.matches()) {
				isLineInWheels = false;
			}

		}
		LOGGER.info("Wheel list was read from the file with next result : " + wheels);
		return wheels;
	}

}
