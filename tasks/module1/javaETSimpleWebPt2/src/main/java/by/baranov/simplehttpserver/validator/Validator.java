package by.baranov.simplehttpserver.validator;

import java.util.Map;

public interface Validator {

	boolean validate(Map<String,String> mapParametersFromFile);

}
