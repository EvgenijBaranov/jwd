package by.baranov.simplehttpserver.model;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

public class MiniBus extends AbstractTransport {
	public static final int NUMBERS_WHEEL = 6;
	public static final int MAX_QUANTITY_PASSENGERS = 20;
	private int currentQuantityPassengers;

	public MiniBus() {}
	
	public MiniBus(String busNumber, BrandTransport brand, TransportBody transportBody, Engine engine, List<Wheel> wheels, BigDecimal cost) {
		super(busNumber, brand, transportBody, engine, wheels, cost);
	}

	public int getCurrentQuantityPassengers() {
		return currentQuantityPassengers;
	}

	public void setCurrentQuantityPassengers(int currentQuantityPassengers) {
		this.currentQuantityPassengers = currentQuantityPassengers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(currentQuantityPassengers);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof MiniBus))
			return false;
		MiniBus other = (MiniBus) obj;
		return currentQuantityPassengers == other.currentQuantityPassengers;
	}

	@Override
	public String toString() {
		return "\nMiniBus [" + super.toString() + ", currentQuantityPassengers=" + currentQuantityPassengers + "]";
	}

}
