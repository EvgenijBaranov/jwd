package by.baranov.simplehttpserver.validator;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Map;

import org.apache.log4j.Logger;

public class ValidatorElementOfTransportForFileReader implements Validator {
	
	private static final Logger LOGGER = Logger.getLogger(ValidatorElementOfTransportForFileReader.class);

	@Override
	public boolean validate(Map<String, String> mapParametersFromFile) {

		boolean idValueIsValid = false;
		try {
			String idValue = mapParametersFromFile.get("id");
			Long.parseLong(idValue);
			idValueIsValid = true;
		} catch (NumberFormatException exception) {
			LOGGER.error("Invalid value for id in the file. The id value must be integer number", exception);
		} catch (NullPointerException exception) {
			LOGGER.error("The file must have parameter name \"id\" for the transport element ", exception);
		}
		
		boolean producerExist = false;
		try {
			mapParametersFromFile.get("producer");			
			producerExist = true;
		} catch (NullPointerException exception) {
			LOGGER.error("The file must have parameter name \"producer\" for the transport element ", exception);
		}
				
		boolean dateOfManufactureIsValid = false;
		try {
			String dateOfManufactureValue = mapParametersFromFile.get("dateOfManufacture");
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate.parse(dateOfManufactureValue, formatter);
			dateOfManufactureIsValid = true;
		} catch (DateTimeParseException exception) {
			LOGGER.error("Invalid value for dateOfManufacture in the file. The dateOfManufacture value must have format : yyyy-MM-dd ", exception);
		} catch (NullPointerException exception) {
			LOGGER.error("The file must have parameter name \"dateOfManufacture\" for the transport element ", exception);
		}
		return idValueIsValid && producerExist && dateOfManufactureIsValid;
	}
}
