package by.baranov.simplehttpserver.repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.Taxi;
import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.specification.Specification;

public class RepositoryTaxiImpl implements Repository<Taxi> {
	
	private static final Logger LOGGER = Logger.getLogger(RepositoryTaxiImpl.class);
	private List<Taxi> taxies = new ArrayList<>();
	private Random rand = new Random();

	public RepositoryTaxiImpl(int numberTaxiesCreatedInRepository, Repository<TransportBody> bodies,
								Repository<Engine> engines, Repository<Wheel> wheels) {

		for (int i = 0; i < numberTaxiesCreatedInRepository; i++) {

			Engine engine = engines.getRandom();
			TransportBody body = bodies.getRandom();
			Wheel wheel = wheels.getRandom();
			List<Wheel> taxiWheels = new ArrayList<>();
			for (int j = 0; j < Taxi.NUMBERS_WHEEL; j++) {
				taxiWheels.add(wheel);
			}

			int sizeEnumBrandTrasport = BrandTransport.values().length;
			int randomIndexEnumBrandTransport = rand.nextInt(sizeEnumBrandTrasport);
			BrandTransport brand = BrandTransport.values()[randomIndexEnumBrandTransport];

			Taxi taxi = new Taxi("number" + i, brand, body, engine, taxiWheels, new BigDecimal((i+1)*10));
			taxies.add(taxi);
		}
	}

	@Override
	public List<Taxi> findSome(Specification<Taxi> specification) {
		List<Taxi> result = new ArrayList<>();
		for (Taxi taxi : taxies) {
			if (specification.match(taxi)) {
				result.add(taxi);
			}
		}
		return result;
	}

	@Override
	public List<Taxi> getAll() {
		return taxies;
	}

	@Override
	public Optional<Taxi> getBy(Specification<Taxi> specification) {
		for (Taxi taxi : taxies) {
			if (specification.match(taxi)) {
				return Optional.of(taxi);
			}
		}
		LOGGER.warn(this.getClass().getSimpleName() + " didn't find a taxi by : " + specification.getClass().getSimpleName());
		return Optional.empty();
	}

	@Override
	public Taxi getRandom() {
		int sizeRepository = taxies.size();
		Taxi randomTaxi = taxies.get(rand.nextInt(sizeRepository));
		return randomTaxi;
	}

	@Override
	public String toString() {
		return "Repository of taxies : " + taxies;
	}

	@Override
	public void addElements(List<Taxi> elements) {
		taxies.addAll(elements);
	}

	@Override
	public void sortRepositoryByParameters(Comparator<? super Taxi> comparator) {
		Collections.sort(taxies, comparator);
	}

}
