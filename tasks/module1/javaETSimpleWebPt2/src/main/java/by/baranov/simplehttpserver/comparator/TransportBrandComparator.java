package by.baranov.simplehttpserver.comparator;

import java.util.Comparator;

import by.baranov.simplehttpserver.model.AbstractTransport;

public class TransportBrandComparator implements Comparator<AbstractTransport> {

	@Override
	public int compare(AbstractTransport transport1, AbstractTransport transport2) {
		String brandName1 = transport1.getBrand().name();
		String brandName2 = transport2.getBrand().name();

		return brandName1.compareTo(brandName2);
	}
}
