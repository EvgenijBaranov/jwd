package by.baranov.simplehttpserver.specification;

import java.util.Objects;

import by.baranov.simplehttpserver.model.MiniBus;

public class SpecificationByMiniBusNumberImpl implements Specification<MiniBus> {

	private final String miniBusNumber;

	public SpecificationByMiniBusNumberImpl(String taxiCargoNumber) {
		this.miniBusNumber = taxiCargoNumber;
	}

	@Override
	public boolean match(MiniBus miniBus) {
		return miniBusNumber.equalsIgnoreCase(miniBus.getRegistrationNumber());
	}

	@Override
	public int hashCode() {
		return Objects.hash(miniBusNumber);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof SpecificationByMiniBusNumberImpl))
			return false;
		SpecificationByMiniBusNumberImpl other = (SpecificationByMiniBusNumberImpl) obj;
		return Objects.equals(miniBusNumber, other.miniBusNumber);
	}
	
	
}
