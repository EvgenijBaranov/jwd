package by.baranov.simplehttpserver.reader;

import java.io.BufferedReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.exception.ElementReadFromFileException;
import by.baranov.simplehttpserver.exception.FileNotAvailableException;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.validator.Validator;
import by.baranov.simplehttpserver.validator.ValidatorWheelForFileReader;

public class ReaderWheelFromFile implements ReaderElement<Wheel> {
	
	private static final Logger LOGGER = Logger.getLogger(ReaderWheelFromFile.class);
	private final Validator wheelValidator = new ValidatorWheelForFileReader();

	@Override
	public Wheel parseElement(BufferedReader bufferedReader) throws FileNotAvailableException, ElementReadFromFileException {

		Map<String, String> mapParamemetrsWheel = ReaderUtility.getTransportElementParameters(bufferedReader);

		Wheel wheel = null;

		if (wheelValidator.validate(mapParamemetrsWheel)) {

			String idValue = mapParamemetrsWheel.get("id");
			long id = Long.parseLong(idValue);

			String producer = mapParamemetrsWheel.get("producer");

			String radiusValue = mapParamemetrsWheel.get("radius");
			int radius = Integer.parseInt(radiusValue);

			String dateOfManufactureValue = mapParamemetrsWheel.get("dateOfManufacture");
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate dateOfManufacture = LocalDate.parse(dateOfManufactureValue, formatter);

			wheel = new Wheel(id, radius, producer, dateOfManufacture);
			LOGGER.info("The wheel was read from the file succesfully : " + wheel);
		} else {
			throw new ElementReadFromFileException("The wheel wasn't read from the file, because of wrong data  in wheel parameters.");
		}
		return wheel;
	}

}
