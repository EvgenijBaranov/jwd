package by.baranov.simplehttpserver.reader;

import java.io.BufferedReader;
import java.io.IOException;

public interface ReaderElement<T> {

	T parseElement(BufferedReader bufferedReader) throws IOException;

}
