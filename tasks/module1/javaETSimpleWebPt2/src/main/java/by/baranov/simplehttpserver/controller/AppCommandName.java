package by.baranov.simplehttpserver.controller;

import java.util.Optional;

import org.apache.log4j.Logger;

public enum AppCommandName {
	
	GET_ALL_TAXIPARK, 
	GET_TAXI_BY_TAXI_NUMBER, 
	GET_TAXICARGO_BY_TAXICARGO_NUMBER, 
	GET_MINIBUS_BY_MINIBUS_NUMBER,
	ADD_TRANSPORT_FROM_FILE_TO_REPOSITORY,
	CALCULATE_COST_TAXIPARK,
	SORT_TRANSPORT_BY_PARAMETERS;
	
	private static final Logger LOGGER = Logger.getLogger(AppCommandName.class);
	
	public static Optional<AppCommandName> fromString(String userCommand) {

		final AppCommandName[] values = AppCommandName.values();
		for (AppCommandName commandName : values) {
			if (commandName.name().equalsIgnoreCase(userCommand)) {
				return Optional.of(commandName);
			}
		}
		LOGGER.warn( AppCommandName.class.getSimpleName() + " hasn't name : " + userCommand);
		return Optional.empty();
	}

}
