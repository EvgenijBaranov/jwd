package by.baranov.simplehttpserver.exception;

public class DataFromFileOutOfBoundException extends Exception {

	public DataFromFileOutOfBoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public DataFromFileOutOfBoundException(String message) {
		super(message);
	}

}
