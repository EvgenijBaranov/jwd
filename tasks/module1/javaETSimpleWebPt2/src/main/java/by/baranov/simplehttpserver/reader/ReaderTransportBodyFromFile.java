package by.baranov.simplehttpserver.reader;

import java.io.BufferedReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.exception.ElementReadFromFileException;
import by.baranov.simplehttpserver.exception.FileNotAvailableException;
import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.validator.Validator;
import by.baranov.simplehttpserver.validator.ValidatorTransportBodyForFileReader;

public class ReaderTransportBodyFromFile implements ReaderElement<TransportBody> {
	private static final Logger LOGGER = Logger.getLogger(ReaderTransportBodyFromFile.class);
	private final Validator bodyValidator = new ValidatorTransportBodyForFileReader();
	
	@Override
	public TransportBody parseElement(BufferedReader bufferedReader) throws FileNotAvailableException, ElementReadFromFileException {

		Map<String, String> mapParamemetrsBody = ReaderUtility.getTransportElementParameters(bufferedReader);

		TransportBody body = null;

		if (bodyValidator.validate(mapParamemetrsBody)) {

			String idValue = mapParamemetrsBody.get("id");
			long id = Long.parseLong(idValue);

			String producer = mapParamemetrsBody.get("producer");

			String capacityValue = mapParamemetrsBody.get("capacity");
			double capacity = Double.parseDouble(capacityValue);

			String materialValue = mapParamemetrsBody.get("material");
			Material material = Material.valueOf(Material.class, materialValue.toUpperCase());

			String dateOfManufactureValue = mapParamemetrsBody.get("dateOfManufacture");
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate dateOfManufacture = LocalDate.parse(dateOfManufactureValue, formatter);

			body = new TransportBody(id, material, capacity, producer, dateOfManufacture);
			LOGGER.info("The transport body was read from the file succesfully : " + body);
		} else {
			throw new ElementReadFromFileException("The transport body wasn't read from the file, because of wrong data in transport body parameters.");
		}
		return body;
	}

}
