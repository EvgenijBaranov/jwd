package by.baranov.simplehttpserver.model;

public enum Material {
	STEEL, ALUMINIUM, WOOD, PLASTIC, CARBON
}
