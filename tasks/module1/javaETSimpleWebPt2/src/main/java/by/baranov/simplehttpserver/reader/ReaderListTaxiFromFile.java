package by.baranov.simplehttpserver.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.exception.ElementReadFromFileException;
import by.baranov.simplehttpserver.exception.FileNotAvailableException;
import by.baranov.simplehttpserver.model.Taxi;
import by.baranov.simplehttpserver.service.TaxiService;

public class ReaderListTaxiFromFile implements ReaderListTransport {
	private static final Logger LOGGER = Logger.getLogger(ReaderListTaxiFromFile.class);
	private final ReaderElement<Taxi> readerTaxi;
	private final TaxiService taxiService;

	
	public ReaderListTaxiFromFile(ReaderElement<Taxi> readerTaxi, TaxiService taxiService) {
		this.readerTaxi = readerTaxi;
		this.taxiService = taxiService;
	}

	@Override
	public String parseListTransport(BufferedReader bufferedReader) throws IOException {
		List<Taxi> listTaxi = new ArrayList<>();
		long numberTaxiWithWrongData = 1L;
		StringBuilder wrongInformationAboutParseListTaxi = new StringBuilder();
		
		Pattern patternForTaxi = Pattern.compile("\\s*Taxi\\s*\\{\\{\\s*");
		Pattern patternEndListTaxi = Pattern.compile("\\s*\\}\\],*\\s*");
		
		boolean isLineInTaxies = true;

		while (isLineInTaxies) {

			String lineInTaxies = null;
			try {
				lineInTaxies = bufferedReader.readLine();
			} catch (IOException exception) {
				throw new FileNotAvailableException("Can't read the file. The file isn't available for reading.", exception);
			}

			Matcher matcherForTaxi = patternForTaxi.matcher(lineInTaxies);
			if (matcherForTaxi.matches()) {
				LOGGER.info("Starting to parse a taxi ...");
				Taxi taxi = null;
				try {
					taxi = readerTaxi.parseElement(bufferedReader);
					listTaxi.add(taxi);
					numberTaxiWithWrongData++;
				} catch (ElementReadFromFileException exception) {
					wrongInformationAboutParseListTaxi.append("Taxi " + numberTaxiWithWrongData + "th has wrong data : "+ exception.getMessage() +"<br>");
					LOGGER.error("There are next complexities with creating " + (numberTaxiWithWrongData++) + "th taxi :", exception);
				}
			}

			Matcher matcherEndListTaxi = patternEndListTaxi.matcher(lineInTaxies);
			if (matcherEndListTaxi.matches()) {
				taxiService.addTaxies(listTaxi);
				isLineInTaxies = false;
				LOGGER.info("Taxi list was read from the file with next result : " + listTaxi);
			}
		}
		return wrongInformationAboutParseListTaxi.toString();
	}

}
