package by.baranov.simplehttpserver.command;

import java.util.Map;
import java.util.Optional;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.model.TaxiCargo;
import by.baranov.simplehttpserver.service.TaxiCargoService;
import by.baranov.simplehttpserver.specification.Specification;
import by.baranov.simplehttpserver.specification.SpecificationByTaxiCargoNumberImpl;

public class GetTaxiCargoByTaxiCargoNumberCommand implements AppCommand {
	private static final Logger LOGGER = Logger.getLogger(GetTaxiCargoByTaxiCargoNumberCommand.class);
	private final TaxiCargoService taxiCargoService;

	public GetTaxiCargoByTaxiCargoNumberCommand(TaxiCargoService taxiCargoService) {
		this.taxiCargoService = taxiCargoService;
		LOGGER.info(this.getClass().getSimpleName() + " is created with services : "
				+ taxiCargoService.getClass().getSimpleName());
	}

	@Override
	public String execute(Map<String, String> mapCommandNameAndParameters) {
		String inputedTaxiCargoNumber = mapCommandNameAndParameters.getOrDefault("taxiCargoNumber",
				"The map of command names and parameters hasn't name \"taxiCargoNumber\" in the html file");
		LOGGER.info(this.getClass().getSimpleName() + " has recieved next taxi cargo number : " + inputedTaxiCargoNumber);
		Specification<TaxiCargo> spec = new SpecificationByTaxiCargoNumberImpl(inputedTaxiCargoNumber);
		Optional<TaxiCargo> taxiCargo = taxiCargoService.getBy(spec);
		LOGGER.info(this.getClass().getSimpleName() + " has received taxi cargo : " + taxiCargo);

		StringBuilder result = new StringBuilder();
		result.append("<h2>Taxi cargo with number = " + inputedTaxiCargoNumber + "</h2>");
		result.append("<ul>");
		result.append("<li>");
		String resultTaxiCargo = taxiCargo.isPresent() ? taxiCargo.get().toString()
				: "There aren't such registartion number of the taxi cargo in the taxi park";
		result.append(resultTaxiCargo);
		result.append("</li>");
		result.append("</ul>");
		LOGGER.info(this.getClass().getSimpleName() + " is executed");
		return result.toString();
	}
}
