package by.baranov.simplehttpserver.model;

import java.time.LocalDate;
import java.util.Objects;

public class Wheel extends AbstractElementOfTransport {
	public static final int MAX_RADIUS = 20;
	public static final int MIN_RADIUS = 13;
	private int radius;

	public Wheel() {}
			
	public Wheel(long id, int radius, String producer, LocalDate dateOfManufacture) {
		super(id, producer, dateOfManufacture);
		this.radius = radius;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(radius);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Wheel))
			return false;
		Wheel other = (Wheel) obj;
		return radius == other.radius;
	}

	@Override
	public String toString() {
		return "Wheel [" + super.toString() + ", radius=" + radius + "]";
	}

}
