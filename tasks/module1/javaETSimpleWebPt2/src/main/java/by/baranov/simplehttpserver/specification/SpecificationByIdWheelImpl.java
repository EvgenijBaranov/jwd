package by.baranov.simplehttpserver.specification;

import by.baranov.simplehttpserver.model.Wheel;

public class SpecificationByIdWheelImpl implements Specification<Wheel>{
	private long id;

	public SpecificationByIdWheelImpl(long id) {
		this.id = id;
	}
	
	@Override
	public boolean match(Wheel wheel) {
		return this.id == wheel.getId();
	}
}
