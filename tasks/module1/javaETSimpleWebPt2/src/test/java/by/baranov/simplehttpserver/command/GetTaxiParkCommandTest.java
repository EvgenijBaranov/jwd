package by.baranov.simplehttpserver.command;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.simplehttpserver.model.AbstractTransport;
import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.model.MiniBus;
import by.baranov.simplehttpserver.model.Taxi;
import by.baranov.simplehttpserver.model.TaxiCargo;
import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.MiniBusService;
import by.baranov.simplehttpserver.service.TaxiCargoService;
import by.baranov.simplehttpserver.service.TaxiService;
import by.baranov.simplehttpserver.service.impl.MiniBusServiceImpl;
import by.baranov.simplehttpserver.service.impl.TaxiCargoServiceImpl;
import by.baranov.simplehttpserver.service.impl.TaxiServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class GetTaxiParkCommandTest {

	private List<Taxi> expectedListTaxi = new ArrayList<>();
	private List<TaxiCargo> expectedListTaxiCargo = new ArrayList<>();
	private List<MiniBus> expectedListMiniBus = new ArrayList<>();
	private TaxiService taxiService;
	private TaxiCargoService taxiCargoService;
	private MiniBusService miniBusService;
	private GetTaxiParkCommand command;

	@Mock
	private Repository<Taxi> repositoryTaxi;
	@Mock
	private Repository<TaxiCargo> repositoryTaxiCargo;
	@Mock
	private Repository<MiniBus> repositoryMiniBus;

	@Before
	public void initListTaxiWithTaxiServiceAndCommand() {
		Wheel wheel = new Wheel(1, 10, "OPEL", LocalDate.of(2020, 04, 9));
		Engine engine = new Engine(1, 100, "OPEL", LocalDate.of(2020, 04, 9));
		TransportBody body = new TransportBody(1, Material.ALUMINIUM, 120.8, "OPEL", LocalDate.of(2020, 04, 9));
		List<Wheel> wheelsForMiniBus = new ArrayList<>();
		for (int i = 0; i < MiniBus.NUMBERS_WHEEL; i++) {
			wheelsForMiniBus.add(wheel);
		}
		MiniBus miniBus = new MiniBus("MiniBus132", BrandTransport.FORD, body, engine, wheelsForMiniBus, new BigDecimal("32.1"));

		List<Wheel> wheelsForTaxiCargo = new ArrayList<>();
		for (int i = 0; i < TaxiCargo.NUMBERS_WHEEL; i++) {
			wheelsForTaxiCargo.add(wheel);
		}
		double maxPermissibleMassOfGoods = 3000;
		TaxiCargo taxiCargo = new TaxiCargo("TaxiCargo132", BrandTransport.FORD, body, engine, wheelsForTaxiCargo,
				maxPermissibleMassOfGoods, new BigDecimal("32.1"));

		List<Wheel> wheelsForTaxi = new ArrayList<>();
		for (int i = 0; i < Taxi.NUMBERS_WHEEL; i++) {
			wheelsForTaxi.add(wheel);
		}
		Taxi taxi = new Taxi("Taxi132", BrandTransport.FORD, body, engine, wheelsForTaxi, new BigDecimal("32.1"));

		for (int i = 0; i < 2; i++) {
			expectedListTaxi.add(taxi);
			expectedListTaxiCargo.add(taxiCargo);
			expectedListMiniBus.add(miniBus);
		}

		taxiService = new TaxiServiceImpl(repositoryTaxi);
		taxiCargoService = new TaxiCargoServiceImpl(repositoryTaxiCargo);
		miniBusService = new MiniBusServiceImpl(repositoryMiniBus);
		command = new GetTaxiParkCommand(taxiService, taxiCargoService, miniBusService);
	}

	@Test
	public void executeTest() {
		when(repositoryTaxi.getAll()).thenReturn(expectedListTaxi);
		when(repositoryTaxiCargo.getAll()).thenReturn(expectedListTaxiCargo);
		when(repositoryMiniBus.getAll()).thenReturn(expectedListMiniBus);

		StringBuilder expectedResultCommand = new StringBuilder();
		expectedResultCommand.append("<h1>Taxi park has next transport:</h1>");
		expectedResultCommand.append(getStringFromList(expectedListTaxi));
		expectedResultCommand.append(getStringFromList(expectedListTaxiCargo));
		expectedResultCommand.append(getStringFromList(expectedListMiniBus));

		Map<String, String> mapForCommand = new HashMap<>();
		String foundResultCommand = command.execute(mapForCommand);

		assertEquals(expectedResultCommand.toString(), foundResultCommand);
	}

	@After
	public void clearTaxiParkWithTaxiParkServiceAndCommand() {
		taxiService = null;
		taxiCargoService = null;
		miniBusService = null;
		expectedListTaxi = null;
		expectedListTaxiCargo = null;
		expectedListMiniBus = null;
		repositoryTaxi = null;
		repositoryTaxiCargo = null;
		repositoryMiniBus = null;
		command = null;
	}

	
	private StringBuilder getStringFromList(List<? extends AbstractTransport> listTransport) {
		StringBuilder result = new StringBuilder();

		result.append("<h2>" + listTransport.get(0).getClass().getSimpleName() + ":</h2>");
		result.append("<ul>");
		for (AbstractTransport transport : listTransport) {
			result.append("<li>");
			result.append(transport.toString());
			result.append("</li>");
		}
		result.append("</ul>");

		return result;
	}

}
