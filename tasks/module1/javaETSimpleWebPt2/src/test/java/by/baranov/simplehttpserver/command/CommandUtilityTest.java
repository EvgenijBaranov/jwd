package by.baranov.simplehttpserver.command;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.simplehttpserver.model.AbstractTransport;
import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.model.MiniBus;
import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.model.Wheel;

@RunWith(MockitoJUnitRunner.class)
public class CommandUtilityTest {
	
	private List<? extends AbstractTransport> listTransport;

	@Before
	public void init() {		
		Wheel wheel = new Wheel(1, 10, "OPEL", LocalDate.of(2020, 04, 9));
		Engine engine = new Engine(1, 100, "OPEL", LocalDate.of(2020, 04, 9));
		TransportBody body = new TransportBody(1, Material.ALUMINIUM, 120.8, "OPEL", LocalDate.of(2020, 04, 9));
		List<Wheel> wheelsForMiniBus = new ArrayList<>();
		for (int i = 0; i < MiniBus.NUMBERS_WHEEL; i++) {
			wheelsForMiniBus.add(wheel);
		}
		MiniBus miniBus1 = new MiniBus("MiniBus132", BrandTransport.FORD, body, engine, wheelsForMiniBus, new BigDecimal("10.4"));
		MiniBus miniBus2 = new MiniBus("MiniBus456", BrandTransport.FORD, body, engine, wheelsForMiniBus, new BigDecimal("10.4"));
		
		listTransport = Arrays.asList(miniBus1, miniBus2);			
	}

	@Test
	public void listTransportToStringForTemplateHTMLFileTest() {
		StringBuilder expectedResultForListTransport = new StringBuilder();
		expectedResultForListTransport.append("<h2>" + listTransport.get(0).getClass().getSimpleName()+ ":</h2>");
		expectedResultForListTransport.append("<ul>");
		for (AbstractTransport transport : listTransport) {
			expectedResultForListTransport.append("<li>");
			expectedResultForListTransport.append(transport.toString());
			expectedResultForListTransport.append("</li>");
		}
		expectedResultForListTransport.append("</ul>");
		String foundResultForListTransport = CommandUtility.listTransportToStringForTemplateHTMLFile(listTransport);
		assertEquals(expectedResultForListTransport.toString(), foundResultForListTransport);	
	}
	
	@After
	public void clear() {
		listTransport = null;
	}

}
