package by.baranov.simplehttpserver.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.WheelService;
import by.baranov.simplehttpserver.specification.Specification;
import by.baranov.simplehttpserver.specification.SpecificationByIdWheelImpl;

@RunWith(MockitoJUnitRunner.class)
public class WheelServiceImplTest {

	private WheelService wheelService;
	private Wheel wheel;

	@Mock
	private Repository<Wheel> repository;	
	
	@Before
	public void initWheelWithWheelService() {
		wheel = new Wheel(1, 10, "OPEL", LocalDate.of(2020, 04, 9));
		wheelService = new WheelServiceImpl(repository);
	}

	@Test
	public void getAllTest() {
		List<Wheel> expectedWheels = Arrays.asList(wheel, wheel);
		when(repository.getAll()).thenReturn(expectedWheels);
		List<Wheel> foundWheels = wheelService.getAll();
		assertEquals(expectedWheels, foundWheels);
	}
	
	@Test
	public void getBySpecifiationTest() {
		Optional<Wheel> expectedWheel = Optional.of(wheel);
		Specification<Wheel> specification = new SpecificationByIdWheelImpl(wheel.getId());
		when(repository.getBy(specification)).thenReturn(expectedWheel);
		Optional<Wheel> foundWheel = wheelService.getBy(specification);
		assertEquals(expectedWheel, foundWheel);		
	}
	
	@After
	public void clearWheelWithWheelService() {
		wheel = null;
		wheelService = null;
	}

}
