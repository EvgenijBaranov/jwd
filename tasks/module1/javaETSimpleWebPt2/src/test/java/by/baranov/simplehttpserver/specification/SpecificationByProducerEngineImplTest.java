package by.baranov.simplehttpserver.specification;

import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.simplehttpserver.model.Engine;

public class SpecificationByProducerEngineImplTest {
	private Engine engine;
	private SpecificationByProducerEngineImpl specification;

	@Before
	public void initEngineWithSpecification() {
		engine = new Engine(1, 100, "OPEL", LocalDate.of(2020, 04, 9));
		specification = new SpecificationByProducerEngineImpl("OPEL");
	}

	@Test
	public void matchTest() {
		boolean output = specification.match(engine);
		assertTrue(output);
	}

	@After
	public void clearEngineWithSpecification() {
		engine = null;
		specification = null;
	}

}
