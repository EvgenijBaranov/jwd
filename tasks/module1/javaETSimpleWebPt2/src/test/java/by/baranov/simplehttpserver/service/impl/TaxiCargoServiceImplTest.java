package by.baranov.simplehttpserver.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.model.TaxiCargo;
import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.TaxiCargoService;
import by.baranov.simplehttpserver.specification.Specification;
import by.baranov.simplehttpserver.specification.SpecificationByTaxiCargoNumberImpl;

@RunWith(MockitoJUnitRunner.class)
public class TaxiCargoServiceImplTest {
	
	private TaxiCargoService taxiCargoService;
	private TaxiCargo taxiCargo;

	@Mock
	private Repository<TaxiCargo> repository;	
	
	@Before
	public void initTaxiCargoWithTaxiCargoService() {
		Wheel wheel = new Wheel(1, 10, "OPEL", LocalDate.of(2020, 04, 9));
		Engine engine = new Engine(1, 100, "OPEL", LocalDate.of(2020, 04, 9));
		TransportBody body = new TransportBody(1, Material.ALUMINIUM, 120.8, "OPEL", LocalDate.of(2020, 04, 9));
		List<Wheel> wheels = new ArrayList<>();
		for (int i = 0; i < TaxiCargo.NUMBERS_WHEEL; i++) {
			wheels.add(wheel);
		}
		double maxPermissibleMassOfGoods = 3000;
		taxiCargo = new TaxiCargo("ASD132", BrandTransport.FORD, body, engine, wheels, maxPermissibleMassOfGoods, new BigDecimal("10.4"));
		taxiCargoService = new TaxiCargoServiceImpl(repository);
	}

	@Test
	public void getAllTest() {
		List<TaxiCargo> expectedTaxiesCargo = Arrays.asList(taxiCargo, taxiCargo);
		when(repository.getAll()).thenReturn(expectedTaxiesCargo);
		List<TaxiCargo> foundTaxiesCargo = taxiCargoService.getAll();
		assertEquals(expectedTaxiesCargo, foundTaxiesCargo);
	}
	
	@Test
	public void getBySpecifiationTest() {
		Optional<TaxiCargo> expectedTaxiCargo = Optional.of(taxiCargo);
		Specification<TaxiCargo> specification = new SpecificationByTaxiCargoNumberImpl(taxiCargo.getRegistrationNumber());
		when(repository.getBy(specification)).thenReturn(expectedTaxiCargo);
		Optional<TaxiCargo> foundTaxiCargo = taxiCargoService.getBy(specification);
		assertEquals(expectedTaxiCargo, foundTaxiCargo);		
	}
	
	@After
	public void clearTaxiCargoWithTaxiCargoService() {
		taxiCargo = null;
		taxiCargoService = null;
		repository = null;
	}

}
