package by.baranov.simplehttpserver.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.model.MiniBus;
import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.MiniBusService;
import by.baranov.simplehttpserver.specification.Specification;
import by.baranov.simplehttpserver.specification.SpecificationByMiniBusNumberImpl;

@RunWith(MockitoJUnitRunner.class)
public class MiniBusServiceImplTest {
	private MiniBusService miniBusService;
	private MiniBus miniBus;

	@Mock
	private Repository<MiniBus> repository;

	@Before
	public void initMiniBusWithMiniBusService() {
		Wheel wheel = new Wheel(1, 10, "OPEL", LocalDate.of(2020, 04, 9));
		Engine engine = new Engine(1, 100, "OPEL", LocalDate.of(2020, 04, 9));
		TransportBody body = new TransportBody(1, Material.ALUMINIUM, 120.8, "OPEL", LocalDate.of(2020, 04, 9));
		List<Wheel> wheels = new ArrayList<>();
		for (int i = 0; i < MiniBus.NUMBERS_WHEEL; i++) {
			wheels.add(wheel);
		}
		miniBus = new MiniBus("ASD132", BrandTransport.FORD, body, engine, wheels, new BigDecimal("10.4"));
		miniBusService = new MiniBusServiceImpl(repository);
	}

	@Test
	public void getAllTest() {
		List<MiniBus> expectedMiniBuses = Arrays.asList(miniBus, miniBus);
		when(repository.getAll()).thenReturn(expectedMiniBuses);
		List<MiniBus> foundMiniBuses = miniBusService.getAll();
		assertEquals(expectedMiniBuses, foundMiniBuses);
	}

	@Test
	public void getBySpecifiationTest() {
		Optional<MiniBus> expectedMiniBus = Optional.of(miniBus);
		Specification<MiniBus> specification = new SpecificationByMiniBusNumberImpl(miniBus.getRegistrationNumber());
		when(repository.getBy(specification)).thenReturn(expectedMiniBus);
		Optional<MiniBus> foundMiniBus = miniBusService.getBy(specification);
		assertEquals(expectedMiniBus, foundMiniBus);
	}

	@After
	public void clearMiniBusWithMiniBusService() {
		miniBus = null;
		miniBusService = null;
		repository = null;
	}

}
