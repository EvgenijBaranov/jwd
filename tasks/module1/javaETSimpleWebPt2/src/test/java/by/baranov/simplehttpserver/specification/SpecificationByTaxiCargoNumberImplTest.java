package by.baranov.simplehttpserver.specification;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.model.TaxiCargo;
import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.model.Wheel;

public class SpecificationByTaxiCargoNumberImplTest {
	private TaxiCargo taxiCargo;
	private SpecificationByTaxiCargoNumberImpl specification;

	@Before
	public void prepareTaxiCargoAndSpecification() {
		Wheel wheel = new Wheel(1, 10, "OPEL", LocalDate.of(2020, 04, 9));
		Engine engine = new Engine(1, 100, "OPEL", LocalDate.of(2020, 04, 9));
		TransportBody body = new TransportBody(1, Material.ALUMINIUM, 120.8, "OPEL", LocalDate.of(2020, 04, 9));
		List<Wheel> wheels = new ArrayList<>();
		for (int i = 0; i < TaxiCargo.NUMBERS_WHEEL; i++) {
			wheels.add(wheel);
		}
		double maxPermissibleMassOfGoods = 3000;
		taxiCargo = new TaxiCargo("ASD132", BrandTransport.FORD, body, engine, wheels, maxPermissibleMassOfGoods, new BigDecimal("10.4"));
		specification = new SpecificationByTaxiCargoNumberImpl("ASD132");
	}

	@Test
	public void testMatch() {
		boolean output = specification.match(taxiCargo);
		assertTrue(output);
	}

	@After
	public void clearTaxiCargoWithSpecification() {
		taxiCargo = null;
		specification = null;
	}

}
