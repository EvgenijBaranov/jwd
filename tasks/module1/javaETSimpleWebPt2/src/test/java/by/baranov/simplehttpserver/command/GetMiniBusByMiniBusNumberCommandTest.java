package by.baranov.simplehttpserver.command;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.model.MiniBus;
import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.MiniBusService;
import by.baranov.simplehttpserver.service.impl.MiniBusServiceImpl;
import by.baranov.simplehttpserver.specification.Specification;
import by.baranov.simplehttpserver.specification.SpecificationByMiniBusNumberImpl;

@RunWith(MockitoJUnitRunner.class)
public class GetMiniBusByMiniBusNumberCommandTest {

	private List<MiniBus> expectedListMiniBus = new ArrayList<>();
	private MiniBusService miniBusService;
	private GetMiniBusByMiniBusNumberCommand command;
	private StringBuilder expectedResultCommand = new StringBuilder();

	@Mock
	private Repository<MiniBus> repositoryMiniBus;

	@Before
	public void initListMiniBusWithMiniBusServiceAndCommand() {
		Wheel wheel = new Wheel(1, 10, "OPEL", LocalDate.of(2020, 04, 9));
		Engine engine = new Engine(1, 100, "OPEL", LocalDate.of(2020, 04, 9));
		TransportBody body = new TransportBody(1, Material.ALUMINIUM, 120.8, "OPEL", LocalDate.of(2020, 04, 9));
		List<Wheel> wheelsForMiniBus = new ArrayList<>();
		for (int i = 0; i < MiniBus.NUMBERS_WHEEL; i++) {
			wheelsForMiniBus.add(wheel);
		}
		MiniBus miniBus1 = new MiniBus("MiniBus132", BrandTransport.FORD, body, engine, wheelsForMiniBus, new BigDecimal("10.4"));
		MiniBus miniBus2 = new MiniBus("MiniBus654", BrandTransport.FORD, body, engine, wheelsForMiniBus, new BigDecimal("10.4"));
		expectedListMiniBus.add(miniBus1);
		expectedListMiniBus.add(miniBus2);
		
		miniBusService = new MiniBusServiceImpl(repositoryMiniBus);
		command = new GetMiniBusByMiniBusNumberCommand(miniBusService);
	}

	@Test
	public void executeTestWithNotEmptyTransport() {
		Specification<MiniBus> specification = new SpecificationByMiniBusNumberImpl("MiniBus132");
		when(repositoryMiniBus.getBy(specification)).thenReturn(Optional.of(expectedListMiniBus.get(0)));
		Optional<MiniBus> expectedMiniBus = miniBusService.getBy(specification);

		expectedResultCommand.append("<h2>Minibus with number = " + expectedMiniBus.get().getRegistrationNumber() + "</h2>");
		expectedResultCommand.append("<ul>");
		expectedResultCommand.append("<li>");
		expectedResultCommand.append(expectedMiniBus.get());
		expectedResultCommand.append("</li>");
		expectedResultCommand.append("</ul>");

		Map<String, String> mapForCommand = new HashMap<>();
		mapForCommand.put("miniBusNumber", "MiniBus132");
		String foundResultCommand = command.execute(mapForCommand);

		assertEquals(expectedResultCommand.toString(), foundResultCommand);
	}
	
	@Test
	public void executeTestWithEmptyTransport() {
		Specification<MiniBus> specification = new SpecificationByMiniBusNumberImpl("MiniBus789");
		when(repositoryMiniBus.getBy(specification)).thenReturn(Optional.empty());

		expectedResultCommand.append("<h2>Minibus with number = MiniBus789</h2>");
		expectedResultCommand.append("<ul>");
		expectedResultCommand.append("<li>");
		expectedResultCommand.append("There aren't such registartion number of the minibus in the taxi park");
		expectedResultCommand.append("</li>");
		expectedResultCommand.append("</ul>");

		Map<String, String> mapForCommand = new HashMap<>();
		mapForCommand.put("miniBusNumber", "MiniBus789");
		String foundResultCommand = command.execute(mapForCommand);

		assertEquals(expectedResultCommand.toString(), foundResultCommand);
	}

	@After
	public void clearListMiniBusWithMiniBusServiceAndCommand() {
		expectedResultCommand = null;
		miniBusService = null;
		expectedListMiniBus = null;
		repositoryMiniBus = null;
		command = null;
	}
}
