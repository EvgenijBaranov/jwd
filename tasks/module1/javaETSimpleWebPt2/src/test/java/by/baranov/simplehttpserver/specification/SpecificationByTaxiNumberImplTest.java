package by.baranov.simplehttpserver.specification;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.model.Taxi;
import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.model.Wheel;

public class SpecificationByTaxiNumberImplTest {
	private Taxi taxi;
	private SpecificationByTaxiNumberImpl specification;

	@Before
	public void initTaxiAndSpecification() {
		Wheel wheel = new Wheel(1, 10, "OPEL", LocalDate.of(2020, 04, 9));
		Engine engine = new Engine(1, 100, "OPEL", LocalDate.of(2020, 04, 9));
		TransportBody body = new TransportBody(1, Material.ALUMINIUM, 120.8, "OPEL", LocalDate.of(2020, 04, 9));
		List<Wheel> wheels = new ArrayList<>();
		for (int i = 0; i < Taxi.NUMBERS_WHEEL; i++) {
			wheels.add(wheel);
		}
		taxi = new Taxi("ASD132", BrandTransport.FORD, body, engine, wheels, new BigDecimal("10.4"));
		specification = new SpecificationByTaxiNumberImpl("ASD132");
	}

	@Test
	public void testMatch() {
		boolean output = specification.match(taxi);
		assertTrue(output);
	}

	@After
	public void clearTaxiWithSpecification() {
		taxi = null;
		specification = null;
	}

}
