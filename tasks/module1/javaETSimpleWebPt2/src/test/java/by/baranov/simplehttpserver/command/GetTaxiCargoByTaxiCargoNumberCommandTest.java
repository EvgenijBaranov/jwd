package by.baranov.simplehttpserver.command;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.model.TaxiCargo;
import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.TaxiCargoService;
import by.baranov.simplehttpserver.service.impl.TaxiCargoServiceImpl;
import by.baranov.simplehttpserver.specification.Specification;
import by.baranov.simplehttpserver.specification.SpecificationByTaxiCargoNumberImpl;

@RunWith(MockitoJUnitRunner.class)
public class GetTaxiCargoByTaxiCargoNumberCommandTest {

	private List<TaxiCargo> expectedListTaxiCargo = new ArrayList<>();
	private TaxiCargoService taxiCargoService;
	private GetTaxiCargoByTaxiCargoNumberCommand command;
	private StringBuilder expectedResultCommand = new StringBuilder();

	@Mock
	private Repository<TaxiCargo> repositoryTaxiCargo;

	@Before
	public void initListTaxiWithTaxiServiceAndCommand() {
		Wheel wheel = new Wheel(1, 10, "OPEL", LocalDate.of(2020, 04, 9));
		Engine engine = new Engine(1, 100, "OPEL", LocalDate.of(2020, 04, 9));
		TransportBody body = new TransportBody(1, Material.ALUMINIUM, 120.8, "OPEL", LocalDate.of(2020, 04, 9));

		List<Wheel> wheelsForTaxiCargo = new ArrayList<>();
		for (int i = 0; i < TaxiCargo.NUMBERS_WHEEL; i++) {
			wheelsForTaxiCargo.add(wheel);
		}
		double maxPermissibleMassOfGoods = 3000;
		TaxiCargo taxiCargo1 = new TaxiCargo("TaxiCargo132", BrandTransport.FORD, body, engine, wheelsForTaxiCargo, maxPermissibleMassOfGoods, new BigDecimal("10.4"));
		TaxiCargo taxiCargo2 = new TaxiCargo("TaxiCargo543", BrandTransport.FORD, body, engine, wheelsForTaxiCargo, maxPermissibleMassOfGoods, new BigDecimal("10.4"));
		
		expectedListTaxiCargo.add(taxiCargo1);
		expectedListTaxiCargo.add(taxiCargo2);

		taxiCargoService = new TaxiCargoServiceImpl(repositoryTaxiCargo);
		command = new GetTaxiCargoByTaxiCargoNumberCommand(taxiCargoService);
	}

	@Test
	public void executeTestWithNotEmptyTransport() {
		Specification<TaxiCargo> specification = new SpecificationByTaxiCargoNumberImpl("TaxiCargo132");
		when(repositoryTaxiCargo.getBy(specification)).thenReturn(Optional.of(expectedListTaxiCargo.get(0)));
		Optional<TaxiCargo> expectedTaxiCargo = taxiCargoService.getBy(specification);

		expectedResultCommand.append("<h2>Taxi cargo with number = " + expectedTaxiCargo.get().getRegistrationNumber() + "</h2>");
		expectedResultCommand.append("<ul>");
		expectedResultCommand.append("<li>");
		expectedResultCommand.append(expectedTaxiCargo.get());
		expectedResultCommand.append("</li>");
		expectedResultCommand.append("</ul>");

		Map<String, String> mapForCommand = new HashMap<>();
		mapForCommand.put("taxiCargoNumber", "TaxiCargo132");
		String foundResultCommand = command.execute(mapForCommand);

		assertEquals(expectedResultCommand.toString(), foundResultCommand);
	}
	
	@Test
	public void executeTestWithEmptyTransport() {
		Specification<TaxiCargo> specification = new SpecificationByTaxiCargoNumberImpl("TaxiCargo132");
		when(repositoryTaxiCargo.getBy(specification)).thenReturn(Optional.empty());

		expectedResultCommand.append("<h2>Taxi cargo with number = TaxiCargo132</h2>");
		expectedResultCommand.append("<ul>");
		expectedResultCommand.append("<li>");
		expectedResultCommand.append("There aren't such registartion number of the taxi cargo in the taxi park");
		expectedResultCommand.append("</li>");
		expectedResultCommand.append("</ul>");

		Map<String, String> mapForCommand = new HashMap<>();
		mapForCommand.put("taxiCargoNumber", "TaxiCargo132");
		String foundResultCommand = command.execute(mapForCommand);

		assertEquals(expectedResultCommand.toString(), foundResultCommand);
	}

	@After
	public void clearListTaxiWithTaxiServiceAndCommand() {
		expectedResultCommand = null;
		taxiCargoService = null;
		expectedListTaxiCargo = null;
		repositoryTaxiCargo = null;
		command = null;
	}
	
}
