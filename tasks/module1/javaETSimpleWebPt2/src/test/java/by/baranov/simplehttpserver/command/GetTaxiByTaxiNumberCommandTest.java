package by.baranov.simplehttpserver.command;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.model.Taxi;
import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.TaxiService;
import by.baranov.simplehttpserver.service.impl.TaxiServiceImpl;
import by.baranov.simplehttpserver.specification.Specification;
import by.baranov.simplehttpserver.specification.SpecificationByTaxiNumberImpl;

@RunWith(MockitoJUnitRunner.class)
public class GetTaxiByTaxiNumberCommandTest {

	private List<Taxi> expectedListTaxi = new ArrayList<>();
	private TaxiService taxiService;
	private GetTaxiByTaxiNumberCommand command;
	private StringBuilder expectedResultCommand = new StringBuilder();

	@Mock
	private Repository<Taxi> repositoryTaxi;

	@Before
	public void initListTaxiWithTaxiServiceAndCommand() {
		Wheel wheel = new Wheel(1, 10, "OPEL", LocalDate.of(2020, 04, 9));
		Engine engine = new Engine(1, 100, "OPEL", LocalDate.of(2020, 04, 9));
		TransportBody body = new TransportBody(1, Material.ALUMINIUM, 120.8, "OPEL", LocalDate.of(2020, 04, 9));
		List<Wheel> wheelsForTaxi = new ArrayList<>();
		for (int i = 0; i < Taxi.NUMBERS_WHEEL; i++) {
			wheelsForTaxi.add(wheel);
		}
		Taxi taxi1 = new Taxi("Taxi132", BrandTransport.FORD, body, engine, wheelsForTaxi, new BigDecimal("10.4"));
		Taxi taxi2 = new Taxi("Taxi543", BrandTransport.FORD, body, engine, wheelsForTaxi, new BigDecimal("10.4"));
		expectedListTaxi.add(taxi1);
		expectedListTaxi.add(taxi2);

		taxiService = new TaxiServiceImpl(repositoryTaxi);
		command = new GetTaxiByTaxiNumberCommand(taxiService);
	}

	@Test
	public void executeTestWithNotEmptyTransport() {
		Specification<Taxi> specification = new SpecificationByTaxiNumberImpl("Taxi132");
		when(repositoryTaxi.getBy(specification)).thenReturn(Optional.of(expectedListTaxi.get(0)));
		Optional<Taxi> expectedTaxi = taxiService.getBy(specification);

		expectedResultCommand.append("<h2>Taxi with number = " + expectedTaxi.get().getRegistrationNumber() + "</h2>");
		expectedResultCommand.append(getStringFromTransport(expectedTaxi.get()));

		Map<String, String> mapForCommand = new HashMap<>();
		mapForCommand.put("taxiNumber", "Taxi132");
		String foundResultCommand = command.execute(mapForCommand);

		assertEquals(expectedResultCommand.toString(), foundResultCommand);
	}
	
	@Test
	public void executeTestWithEmptyTransport() {
		Specification<Taxi> specification = new SpecificationByTaxiNumberImpl("Taxi789");
		when(repositoryTaxi.getBy(specification)).thenReturn(Optional.empty());

		expectedResultCommand.append("<h2>Taxi with number = Taxi789</h2>");
		expectedResultCommand.append(getStringFromTransport("There aren't such registartion number of the taxi in the taxi park"));

		Map<String, String> mapForCommand = new HashMap<>();
		mapForCommand.put("taxiNumber", "Taxi789");
		String foundResultCommand = command.execute(mapForCommand);

		assertEquals(expectedResultCommand.toString(), foundResultCommand);
	}

	@After
	public void clearListTaxiWithTaxiServiceAndCommand() {
		expectedResultCommand = null;
		taxiService = null;
		expectedListTaxi = null;
		repositoryTaxi = null;
		command = null;
	}

	private String getStringFromTransport(Object object) {
		
		StringBuilder result = new StringBuilder();
		result.append("<ul>");
		result.append("<li>");
		result.append(object.toString());
		result.append("</li>");
		result.append("</ul>");
		
		return result.toString();
	}
}
