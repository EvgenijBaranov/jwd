package by.baranov.simplehttpserver.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.util.Optional;

import org.junit.Test;

public class AppCommandNameTest {

	@Test
	public void rightCommandNameFromStringTest() {

		for (AppCommandName rightCommandName : AppCommandName.values()) {
			String rightCommandNameToString = rightCommandName.name();
			Optional<AppCommandName> foundCommandNameWithRightData = AppCommandName.fromString(rightCommandNameToString);
			assertSame(rightCommandName, foundCommandNameWithRightData.get());
		}
	}

	@Test
	public void wrongCommandNameFromStringTest() {

		String wrongCommandName = "---";
		Optional<AppCommandName> foundCommandNameWithWrongData = AppCommandName.fromString(wrongCommandName);

		assertEquals(Optional.empty(), foundCommandNameWithWrongData);
	}

}
