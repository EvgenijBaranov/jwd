package by.baranov.simplehttpserver.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.TransportBodyService;
import by.baranov.simplehttpserver.specification.Specification;
import by.baranov.simplehttpserver.specification.SpecificationByIdTransportBodyImpl;

@RunWith(MockitoJUnitRunner.class)
public class TransportBodyServiceImplTest {
	
	private TransportBodyService bodyService;
	private TransportBody body;

	@Mock
	private Repository<TransportBody> repository;	
	
	@Before
	public void initEngineWithWheelService() {
		body = new TransportBody(1, Material.ALUMINIUM, 120.8, "OPEL", LocalDate.of(2020, 04, 9));
		bodyService = new TransportBodyServiceImpl(repository);
	}

	@Test
	public void getAllTest() {
		List<TransportBody> expectedBodies = Arrays.asList(body, body);
		when(repository.getAll()).thenReturn(expectedBodies);
		List<TransportBody> foundBodies = bodyService.getAll();
		assertEquals(expectedBodies, foundBodies);
	}
	
	@Test
	public void getBySpecifiationTest() {
		Optional<TransportBody> expectedBody = Optional.of(body);
		Specification<TransportBody> specification = new SpecificationByIdTransportBodyImpl(body.getId());
		when(repository.getBy(specification)).thenReturn(expectedBody);
		Optional<TransportBody> foundBody = bodyService.getBy(specification);
		assertEquals(expectedBody.get(), foundBody.get());		
	}
	@After
	public void clearBodyWithBodyService() {
		body = null;
		bodyService = null;
	}

}
