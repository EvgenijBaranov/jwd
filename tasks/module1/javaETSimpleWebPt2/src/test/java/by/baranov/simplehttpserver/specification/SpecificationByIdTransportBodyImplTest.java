package by.baranov.simplehttpserver.specification;

import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.model.Material;

public class SpecificationByIdTransportBodyImplTest {
	private TransportBody body;
	private SpecificationByIdTransportBodyImpl specification;
	
	@Before
	public void initBodyOfTransportWithSpecification() {
		body = new TransportBody(1, Material.ALUMINIUM, 120.8, "OPEL", LocalDate.of(2020, 04, 9));		
		specification = new SpecificationByIdTransportBodyImpl(1);
	}
	
	@Test
	public void matchTest() {
		boolean output = specification.match(body);
		assertTrue(output);
	}
	
	@After
	public void clearBodyOfTransportWithSpecification() {
		body = null;		
		specification = null;
	}

}
