package by.baranov.simplehttpserver.specification;

import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.simplehttpserver.model.Wheel;

public class SpecificationByIdWheelImplTest {
	private Wheel wheel;
	private SpecificationByIdWheelImpl specification;

	@Before
	public void initWheelWithSpecification() {
		wheel = new Wheel(1, 10, "OPEL", LocalDate.of(2020, 04, 9));
		specification = new SpecificationByIdWheelImpl(1);
	}

	@Test
	public void matchTest() {
		boolean output = specification.match(wheel);
		assertTrue(output);
	}

	@After
	public void clearWheelWithSpecification() {
		wheel = null;
		specification = null;
	}	

}
