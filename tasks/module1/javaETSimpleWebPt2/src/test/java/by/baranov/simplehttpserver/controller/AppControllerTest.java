package by.baranov.simplehttpserver.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.simplehttpserver.command.AppCommand;
import by.baranov.simplehttpserver.command.GetMiniBusByMiniBusNumberCommand;
import by.baranov.simplehttpserver.command.GetTaxiByTaxiNumberCommand;
import by.baranov.simplehttpserver.command.GetTaxiCargoByTaxiCargoNumberCommand;
import by.baranov.simplehttpserver.command.GetTaxiParkCommand;
import by.baranov.simplehttpserver.model.AbstractTransport;
import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.model.MiniBus;
import by.baranov.simplehttpserver.model.Taxi;
import by.baranov.simplehttpserver.model.TaxiCargo;
import by.baranov.simplehttpserver.model.TransportBody;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.MiniBusService;
import by.baranov.simplehttpserver.service.TaxiCargoService;
import by.baranov.simplehttpserver.service.TaxiService;
import by.baranov.simplehttpserver.service.impl.MiniBusServiceImpl;
import by.baranov.simplehttpserver.service.impl.TaxiCargoServiceImpl;
import by.baranov.simplehttpserver.service.impl.TaxiServiceImpl;
import by.baranov.simplehttpserver.specification.Specification;
import by.baranov.simplehttpserver.specification.SpecificationByMiniBusNumberImpl;
import by.baranov.simplehttpserver.specification.SpecificationByTaxiCargoNumberImpl;
import by.baranov.simplehttpserver.specification.SpecificationByTaxiNumberImpl;


@RunWith(MockitoJUnitRunner.class)
public class AppControllerTest {
	private AppController controller;
	private MiniBusService miniBusService;
	private TaxiService taxiService;
	private TaxiCargoService taxiCargoService;
	
	private List<Taxi> expectedListTaxi = new ArrayList<>();
	private List<TaxiCargo> expectedListTaxiCargo = new ArrayList<>();
	private List<MiniBus> expectedListMiniBus = new ArrayList<>();

	@Mock
	private Repository<Taxi> repositoryTaxi;
	@Mock
	private Repository<TaxiCargo> repositoryTaxiCargo;
	@Mock
	private Repository<MiniBus> repositoryMiniBus;

	@Before
	public void initController() {
		Wheel wheel = new Wheel(1, 10, "OPEL", LocalDate.of(2020, 04, 9));
		Engine engine = new Engine(1, 100, "OPEL", LocalDate.of(2020, 04, 9));
		TransportBody body = new TransportBody(1, Material.ALUMINIUM, 120.8, "OPEL", LocalDate.of(2020, 04, 9));
		List<Wheel> wheelsForTaxi = new ArrayList<>();
		for (int i = 0; i < Taxi.NUMBERS_WHEEL; i++) {
			wheelsForTaxi.add(wheel);
		}
		Taxi taxi1 = new Taxi("number0", BrandTransport.FORD, body, engine, wheelsForTaxi, new BigDecimal("32.1"));
		Taxi taxi2 = new Taxi("number1", BrandTransport.FORD, body, engine, wheelsForTaxi, new BigDecimal("32.1"));
		expectedListTaxi.add(taxi1);
		expectedListTaxi.add(taxi2);

		List<Wheel> wheelsForTaxiCargo = new ArrayList<>();
		for (int i = 0; i < TaxiCargo.NUMBERS_WHEEL; i++) {
			wheelsForTaxiCargo.add(wheel);
		}
		double maxPermissibleMassOfGoods = 3000;
		TaxiCargo taxiCargo1 = new TaxiCargo("number0", BrandTransport.FORD, body, engine, wheelsForTaxiCargo, maxPermissibleMassOfGoods, new BigDecimal("32.1"));
		TaxiCargo taxiCargo2 = new TaxiCargo("number1", BrandTransport.FORD, body, engine, wheelsForTaxiCargo, maxPermissibleMassOfGoods, new BigDecimal("32.1"));		
		expectedListTaxiCargo.add(taxiCargo1);
		expectedListTaxiCargo.add(taxiCargo2);

		List<Wheel> wheelsForMiniBus = new ArrayList<>();
		for (int i = 0; i < MiniBus.NUMBERS_WHEEL; i++) {
			wheelsForMiniBus.add(wheel);
		}
		MiniBus miniBus1 = new MiniBus("number0", BrandTransport.FORD, body, engine, wheelsForMiniBus, new BigDecimal("32.1"));
		MiniBus miniBus2 = new MiniBus("number1", BrandTransport.FORD, body, engine, wheelsForMiniBus, new BigDecimal("32.1"));
		expectedListMiniBus.add(miniBus1);
		expectedListMiniBus.add(miniBus2);

		taxiService = new TaxiServiceImpl(repositoryTaxi);
		taxiCargoService = new TaxiCargoServiceImpl(repositoryTaxiCargo);
		miniBusService = new MiniBusServiceImpl(repositoryMiniBus);

		AppCommand commandGetTaxiByNumber = new GetTaxiByTaxiNumberCommand(taxiService);
		AppCommand commandGetTaxiCargoByNumber = new GetTaxiCargoByTaxiCargoNumberCommand(taxiCargoService);
		AppCommand commandGetMiniBusByNumber = new GetMiniBusByMiniBusNumberCommand(miniBusService);	
		AppCommand commandGetTaxiPark = new GetTaxiParkCommand(taxiService, taxiCargoService, miniBusService);
		
		Map<AppCommandName, AppCommand> mapForCommand = new HashMap<>();
		mapForCommand.put(AppCommandName.GET_TAXI_BY_TAXI_NUMBER, commandGetTaxiByNumber);
		mapForCommand.put(AppCommandName.GET_TAXICARGO_BY_TAXICARGO_NUMBER, commandGetTaxiCargoByNumber);
		mapForCommand.put(AppCommandName.GET_MINIBUS_BY_MINIBUS_NUMBER, commandGetMiniBusByNumber);
		mapForCommand.put(AppCommandName.GET_ALL_TAXIPARK, commandGetTaxiPark);
		
		AppCommandFactory factory = new AppCommandFactoryImpl(mapForCommand);
		controller = new AppController(factory);
	}

	@Test
	public void handleUserDateTaxiByNumberCommandTest() {
		
		Specification<Taxi> specification = new SpecificationByTaxiNumberImpl("number0");
		when(repositoryTaxi.getBy(specification)).thenReturn(Optional.of(expectedListTaxi.get(0)));
		Optional<Taxi> expectedTaxi = taxiService.getBy(specification);

		StringBuilder expectedStringFromController = new StringBuilder();
		expectedStringFromController.append("<h2>Taxi with number = " + expectedTaxi.get().getRegistrationNumber() + "</h2>");
		expectedStringFromController.append(getStringFromTransport(expectedTaxi.get()));
		
		Map<String, String> mapCommandNameAndParameters = new HashMap<>();
		mapCommandNameAndParameters.put("taxiNumber", "number0");
		mapCommandNameAndParameters.put("taxiNumber1", "number1");
		
		String foundString = controller.handleUserDate("GET_TAXI_BY_TAXI_NUMBER", mapCommandNameAndParameters);

		assertEquals(expectedStringFromController.toString(), foundString);
	}
	
	@Test
	public void handleUserDateTaxiCargoByNumberCommandTest() {
		
		Specification<TaxiCargo> specification = new SpecificationByTaxiCargoNumberImpl("number0");
		when(repositoryTaxiCargo.getBy(specification)).thenReturn(Optional.of(expectedListTaxiCargo.get(0)));
		Optional<TaxiCargo> expectedTaxiCargo = taxiCargoService.getBy(specification);

		StringBuilder expectedStringFromController = new StringBuilder();
		expectedStringFromController.append("<h2>Taxi cargo with number = " + expectedTaxiCargo.get().getRegistrationNumber() + "</h2>");
		expectedStringFromController.append(getStringFromTransport(expectedTaxiCargo.get()));
		
		Map<String, String> mapCommandNameAndParameters = new HashMap<>();
		mapCommandNameAndParameters.put("taxiCargoNumber", "number0");
		mapCommandNameAndParameters.put("taxiCargoNumber1", "number1");
		
		String foundString = controller.handleUserDate("GET_TAXICARGO_BY_TAXICARGO_NUMBER", mapCommandNameAndParameters);

		assertEquals(expectedStringFromController.toString(), foundString);
	}

	@Test
	public void handleUserDateMiniBusByNumberCommandTest() {
		
		Specification<MiniBus> specification = new SpecificationByMiniBusNumberImpl("number0");
		when(repositoryMiniBus.getBy(specification)).thenReturn(Optional.of(expectedListMiniBus.get(0)));
		Optional<MiniBus> expectedMiniBus = miniBusService.getBy(specification);
		
		StringBuilder expectedStringFromController = new StringBuilder();
		expectedStringFromController.append("<h2>Minibus with number = " + expectedMiniBus.get().getRegistrationNumber() + "</h2>");
		expectedStringFromController.append(getStringFromTransport(expectedMiniBus.get()));
		
		Map<String, String> mapCommandNameAndParameters = new HashMap<>();
		mapCommandNameAndParameters.put("miniBusNumber", "number0");
		mapCommandNameAndParameters.put("miniBusNumber1", "number1");
		
		String foundString = controller.handleUserDate("GET_MINIBUS_BY_MINIBUS_NUMBER", mapCommandNameAndParameters);
		
		assertEquals(expectedStringFromController.toString(), foundString);
	}
	
	@Test
	public void handleUserDateTaxiParkCommandTest() {
		
		when(repositoryTaxi.getAll()).thenReturn(expectedListTaxi);
		when(repositoryTaxiCargo.getAll()).thenReturn(expectedListTaxiCargo);
		when(repositoryMiniBus.getAll()).thenReturn(expectedListMiniBus);

		StringBuilder expectedStringFromController = new StringBuilder();
		expectedStringFromController.append("<h1>Taxi park has next transport:</h1>");
		expectedStringFromController.append(getStringFromTransportList(expectedListTaxi));
		expectedStringFromController.append(getStringFromTransportList(expectedListTaxiCargo));
		expectedStringFromController.append(getStringFromTransportList(expectedListMiniBus));

		Map<String, String> mapCommandNameAndParameters = new HashMap<>();
		String foundString = controller.handleUserDate("GET_ALL_TAXIPARK", mapCommandNameAndParameters);
		
		assertEquals(expectedStringFromController.toString(), foundString);
	}
	
	@After
	public void clearTaxiParkWithTaxiParkServiceAndCommand() {
		taxiService = null;
		taxiCargoService = null;
		miniBusService = null;
		expectedListTaxi = null;
		expectedListTaxiCargo = null;
		expectedListMiniBus = null;
		repositoryTaxi = null;
		repositoryTaxiCargo = null;
		repositoryMiniBus = null;
		controller = null;
	}
	
	private String getStringFromTransport(AbstractTransport transport) {
		
		StringBuilder result = new StringBuilder();
		result.append("<ul>");
		result.append("<li>");
		result.append(transport.toString());
		result.append("</li>");
		result.append("</ul>");
		
		return result.toString();
	}
	
	private String getStringFromTransportList(List<? extends AbstractTransport> listTransport) {
		
		StringBuilder result = new StringBuilder();
		result.append("<h2>" + listTransport.get(0).getClass().getSimpleName() + ":</h2>");
		result.append("<ul>");
		for (AbstractTransport transport : listTransport) {
			result.append("<li>");
			result.append(transport.toString());
			result.append("</li>");
		}
		result.append("</ul>");

		return result.toString();
	}
	
}
