package by.baranov.simplehttpserver.controller;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.simplehttpserver.command.AppCommand;
import by.baranov.simplehttpserver.command.GetMiniBusByMiniBusNumberCommand;
import by.baranov.simplehttpserver.command.GetTaxiByTaxiNumberCommand;
import by.baranov.simplehttpserver.model.MiniBus;
import by.baranov.simplehttpserver.model.Taxi;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.MiniBusService;
import by.baranov.simplehttpserver.service.TaxiService;
import by.baranov.simplehttpserver.service.impl.MiniBusServiceImpl;
import by.baranov.simplehttpserver.service.impl.TaxiServiceImpl;

public class AppCommandFactoryImplTest {

	private AppCommandFactory factory;	
	private AppCommand expectedCommand;
	private MiniBusService miniBusService;
	private TaxiService taxiService;
	private Repository<Taxi> repositoryTaxi;
	private Repository<MiniBus> repositoryMiniBus;

	@Before
	public void initFactory() {
		
		taxiService = new TaxiServiceImpl(repositoryTaxi);
		miniBusService = new MiniBusServiceImpl(repositoryMiniBus);
		
		AppCommand commandGetMiniBusByNumber = new GetMiniBusByMiniBusNumberCommand(miniBusService);
		AppCommand commandGetTaxiByNumber = new GetTaxiByTaxiNumberCommand(taxiService);
		expectedCommand = commandGetTaxiByNumber;

		Map<AppCommandName, AppCommand> mapForCommand = new HashMap<>();
		mapForCommand.put(AppCommandName.GET_MINIBUS_BY_MINIBUS_NUMBER, commandGetMiniBusByNumber);
		mapForCommand.put(AppCommandName.GET_TAXI_BY_TAXI_NUMBER, commandGetTaxiByNumber);

		factory = new AppCommandFactoryImpl(mapForCommand);
	}

	@Test
	public void getCommandTest() {

		AppCommand foundCommand = factory.getCommand("GET_TAXI_BY_TAXI_NUMBER");
		assertEquals(expectedCommand, foundCommand);
	}

	@Test
	public void getCommandWrongNameTest() {

		String expectedStringFromExecuteCommand = "Application has't such name in AppComandName";

		AppCommand foundCommand = factory.getCommand("wrong command");
		String foundString = foundCommand.execute(new HashMap<String, String>());

		assertEquals(expectedStringFromExecuteCommand, foundString);
	}

	@Test
	public void getCommandWrongNameForFactoryTest() {

		Optional<AppCommandName> nameCommand = AppCommandName.fromString("GET_TAXICARGO_BY_TAXICARGO_NUMBER");
		String expectedStringFromExecuteCommand = "There aren't command : " + nameCommand.get() + " in AppCommandFactory";

		AppCommand foundCommand = factory.getCommand("GET_TAXICARGO_BY_TAXICARGO_NUMBER");
		String foundString = foundCommand.execute(new HashMap<String, String>());

		assertEquals(expectedStringFromExecuteCommand, foundString);
	}
	
	@After
	public void clearFactory() {
		factory = null;	
		expectedCommand = null;		
	}

}
