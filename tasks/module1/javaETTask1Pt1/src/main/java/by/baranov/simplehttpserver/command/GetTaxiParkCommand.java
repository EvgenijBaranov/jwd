package by.baranov.simplehttpserver.command;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.model.MiniBus;
import by.baranov.simplehttpserver.model.Taxi;
import by.baranov.simplehttpserver.model.TaxiCargo;
import by.baranov.simplehttpserver.service.MiniBusService;
import by.baranov.simplehttpserver.service.TaxiCargoService;
import by.baranov.simplehttpserver.service.TaxiService;

public class GetTaxiParkCommand implements AppCommand {
	private static final Logger LOGGER = Logger.getLogger(GetTaxiParkCommand.class);
	private final TaxiService taxiService;
	private final TaxiCargoService taxiCargoService;
	private final MiniBusService miniBusService;

	public GetTaxiParkCommand(TaxiService taxiService, TaxiCargoService taxiCargoService, MiniBusService miniBusService) {
		this.taxiService = taxiService;
		this.taxiCargoService = taxiCargoService;
		this.miniBusService = miniBusService;
		LOGGER.info(this.getClass().getSimpleName() + " is created with services : " 
								+ taxiService.getClass().getSimpleName()
								+ taxiCargoService.getClass().getSimpleName()
								+ miniBusService.getClass().getSimpleName());
	}

	@Override
	public String execute(Map<String, String> mapCommandNameAndParameters) {

		List<Taxi> taxies = taxiService.getAll();
		LOGGER.info(this.getClass().getSimpleName() + " has received list taxi : " + taxies);
		List<TaxiCargo> taxiesCargo = taxiCargoService.getAll();
		LOGGER.info(this.getClass().getSimpleName() + " has received list taxi cargo : " + taxies);
		List<MiniBus> miniBuses = miniBusService.getAll();
		LOGGER.info(this.getClass().getSimpleName() + " has received list minibus : " + taxies);

		StringBuilder result = new StringBuilder();
		result.append("<h1>Taxi park has next transport:</h1>");
		result.append(CommandUtility.listTransportToStringForTemplateHTMLFile(taxies));
		result.append(CommandUtility.listTransportToStringForTemplateHTMLFile(taxiesCargo));
		result.append(CommandUtility.listTransportToStringForTemplateHTMLFile(miniBuses));
		LOGGER.info(this.getClass().getSimpleName() + " is executed");
		return result.toString();
	}

}
