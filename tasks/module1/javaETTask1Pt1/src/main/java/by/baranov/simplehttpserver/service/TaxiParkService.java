package by.baranov.simplehttpserver.service;

import java.util.List;

import by.baranov.simplehttpserver.model.MiniBus;
import by.baranov.simplehttpserver.model.Taxi;
import by.baranov.simplehttpserver.model.TaxiCargo;
import by.baranov.simplehttpserver.model.TaxiPark;

public interface TaxiParkService {
	
	TaxiPark createTaxiPark();
	
	List<Taxi> getTaxies(TaxiPark taxiPark);
	List<TaxiCargo> getTaxiCargo(TaxiPark taxiPark);
	List<MiniBus> getMiniBuses(TaxiPark taxiPark);	
	

}
