package by.baranov.simplehttpserver.model;

import java.util.List;
import java.util.Objects;

public class TaxiCargo extends AbstractTransport {
	public static final double MAX_VALUE_MASS_OF_GOODS = 2000;
	public static final double MIN_VALUE_MASS_OF_GOODS = 1000;
	public static final int NUMBERS_WHEEL = 6;
	public static final int MAX_QUANTITY_PASSENGERS = 2;
	private double maxPermissibleMassOfGoods;
	private double currentMassOfGoods;

	public TaxiCargo(String taxiNumber, BrandTransport brandTransport, BodyOfTransport bodyTransport,
			Engine engineTransport, List<Wheel> wheels, double maxPermissibleMassOfGoods) {
		super(taxiNumber, brandTransport, bodyTransport, engineTransport, wheels);
		this.maxPermissibleMassOfGoods = maxPermissibleMassOfGoods;
	}

	public double getMaxMassOfGoods() {
		return maxPermissibleMassOfGoods;
	}

	public void setMaxMassOfGoods(double maxPermissibleMassOfGoods) {
		this.maxPermissibleMassOfGoods = maxPermissibleMassOfGoods;
	}

	public double getCurrentMassOfGoods() {
		return currentMassOfGoods;
	}

	public void setCurrentMassOfGoods(double currentMassOfGoods) {
		this.currentMassOfGoods = currentMassOfGoods;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(currentMassOfGoods, maxPermissibleMassOfGoods);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof TaxiCargo))
			return false;
		TaxiCargo other = (TaxiCargo) obj;
		return Double.doubleToLongBits(currentMassOfGoods) == Double.doubleToLongBits(other.currentMassOfGoods)
				&& Double.doubleToLongBits(maxPermissibleMassOfGoods) == Double.doubleToLongBits(other.maxPermissibleMassOfGoods);
	}

	@Override
	public String toString() {
		return "\nTaxiCargo [" + super.toString() + ", maxLoadCapacity=" + maxPermissibleMassOfGoods + ", loadCapacity="
				+ currentMassOfGoods + "]";
	}

}
