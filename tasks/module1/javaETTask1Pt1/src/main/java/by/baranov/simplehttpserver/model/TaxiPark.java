package by.baranov.simplehttpserver.model;

import java.util.List;
import java.util.Objects;

public class TaxiPark {

	private List<Taxi> listTaxies;
	private List<TaxiCargo> listTaxiCargos;
	private List<MiniBus> listMiniBuses;

	public TaxiPark(List<Taxi> listTaxies, List<TaxiCargo> listTaxiCargos, List<MiniBus> listMiniBuses) {
		this.listTaxies = listTaxies;
		this.listTaxiCargos = listTaxiCargos;
		this.listMiniBuses = listMiniBuses;
	}

	public List<Taxi> getListTaxies() {
		return listTaxies;
	}

	public void setListTaxies(List<Taxi> listTaxies) {
		this.listTaxies = listTaxies;
	}

	public List<TaxiCargo> getListTaxiCargos() {
		return listTaxiCargos;
	}

	public void setListTaxiCargos(List<TaxiCargo> listTaxiCargos) {
		this.listTaxiCargos = listTaxiCargos;
	}

	public List<MiniBus> getListMiniBuses() {
		return listMiniBuses;
	}

	public void setListMiniBuses(List<MiniBus> listMiniBuses) {
		this.listMiniBuses = listMiniBuses;
	}

	
	
	@Override
	public int hashCode() {
		return Objects.hash(listMiniBuses, listTaxiCargos, listTaxies);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof TaxiPark))
			return false;
		TaxiPark other = (TaxiPark) obj;
		return Objects.equals(listMiniBuses, other.listMiniBuses)
				&& Objects.equals(listTaxiCargos, other.listTaxiCargos) && Objects.equals(listTaxies, other.listTaxies);
	}

	@Override
	public String toString() {
		return "TaxiPark [\n"
				+ "Taxies=" + listTaxies + ", \n"
				+ "TaxiCargos=" + listTaxiCargos + ", \n"
				+ "MiniBuses=" + listMiniBuses	+ "]";
	}

}
