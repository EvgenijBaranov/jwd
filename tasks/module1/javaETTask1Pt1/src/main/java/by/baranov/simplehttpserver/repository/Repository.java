package by.baranov.simplehttpserver.repository;

import java.util.List;
import java.util.Optional;

import by.baranov.simplehttpserver.service.CRUDOperation;
import by.baranov.simplehttpserver.specification.Specification;

public interface Repository<T> extends CRUDOperation<T>{
	
	Optional<List<T>> findSome(Specification<T> specification);
	
	T getRandom();
	
}