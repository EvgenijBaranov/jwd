package by.baranov.simplehttpserver.model;

import java.util.List;
import java.util.Objects;

public class MiniBus extends AbstractTransport {
	public static final int NUMBERS_WHEEL = 6;
	public static final int MAX_QUANTITY_PASSENGERS = 20;
	private int currentQuantityPassengers;

	public MiniBus(String busNumber, BrandTransport brandTransport, BodyOfTransport bodyTransport,
			Engine engineTransport, List<Wheel> wheels) {
		super(busNumber, brandTransport, bodyTransport, engineTransport, wheels);
	}

	public int getCurrentQuantityPassengers() {
		return currentQuantityPassengers;
	}

	public void setCurrentQuantityPassengers(int currentQuantityPassengers) {
		this.currentQuantityPassengers = currentQuantityPassengers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(currentQuantityPassengers);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof MiniBus))
			return false;
		MiniBus other = (MiniBus) obj;
		return currentQuantityPassengers == other.currentQuantityPassengers;
	}

	@Override
	public String toString() {
		return "\nMiniBus [" + super.toString() + ", currentQuantityPassengers=" + currentQuantityPassengers + "]";
	}

}
