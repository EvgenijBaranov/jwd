package by.baranov.simplehttpserver.service.impl;

import java.util.List;
import java.util.Optional;

import by.baranov.simplehttpserver.model.TaxiCargo;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.TaxiCargoService;
import by.baranov.simplehttpserver.specification.Specification;

public class TaxiCargoServiceImpl implements TaxiCargoService {
	private final Repository<TaxiCargo> repositoryTaxiCargo;

	public TaxiCargoServiceImpl(Repository<TaxiCargo> repositoryTaxiCargo) {
		this.repositoryTaxiCargo = repositoryTaxiCargo;
	}

	@Override
	public List<TaxiCargo> getAll() {
		List<TaxiCargo> taxiCargoAll = repositoryTaxiCargo.getAll();
		return taxiCargoAll;
	}

	@Override
	public Optional<TaxiCargo> getBy(Specification<TaxiCargo> specification) {
		Optional<TaxiCargo> taxiCargo = repositoryTaxiCargo.getBy(specification);
		return taxiCargo;
	}

}
