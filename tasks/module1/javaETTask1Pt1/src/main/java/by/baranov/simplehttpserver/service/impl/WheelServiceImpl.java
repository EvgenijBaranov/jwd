package by.baranov.simplehttpserver.service.impl;

import java.util.List;
import java.util.Optional;

import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.WheelService;
import by.baranov.simplehttpserver.specification.Specification;

public class WheelServiceImpl implements WheelService {
	private final Repository<Wheel> repository;

	public WheelServiceImpl(Repository<Wheel> repository) {
		this.repository = repository;
	}

	@Override
	public List<Wheel> getAll() {
		List<Wheel> wheelsAll = repository.getAll();
		return wheelsAll;
	}

	@Override
	public Optional<Wheel> getBy(Specification<Wheel> specification) {
		Optional<Wheel> wheel = repository.getBy(specification);
		return wheel;
	}

}
