package by.baranov.simplehttpserver.service.impl;

import java.util.List;
import java.util.Optional;

import by.baranov.simplehttpserver.model.BodyOfTransport;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.BodyOfTransportService;
import by.baranov.simplehttpserver.specification.Specification;

public class BodyOfTransportServiceImpl implements BodyOfTransportService{
	private final Repository<BodyOfTransport> repository;

	public BodyOfTransportServiceImpl(Repository<BodyOfTransport> repository) {
		this.repository = repository;
	}

	@Override
	public List<BodyOfTransport> getAll() {
		List<BodyOfTransport> bodiesAll = repository.getAll();
		return bodiesAll;
	}

	@Override
	public Optional<BodyOfTransport> getBy(Specification<BodyOfTransport> specification) {
		Optional<BodyOfTransport> body = repository.getBy(specification);
		return body;
	}

	

}
