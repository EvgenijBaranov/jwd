package by.baranov.simplehttpserver.service.impl;

import java.util.List;
import java.util.Optional;

import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.EngineService;
import by.baranov.simplehttpserver.specification.Specification;

public class EngineServiceImpl implements EngineService {
	private final Repository<Engine> repository;

	public EngineServiceImpl(Repository<Engine> repository) {
		this.repository = repository;
	}

	@Override
	public List<Engine> getAll() {
		List<Engine> enginesAll = repository.getAll();
		return enginesAll;
	}

	@Override
	public Optional<Engine> getBy(Specification<Engine> specification) {
		Optional<Engine> engine = repository.getBy(specification);
		return engine;
	}

}
