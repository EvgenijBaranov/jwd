package by.baranov.simplehttpserver.model;

import java.util.List;
import java.util.Objects;

public class Taxi extends AbstractTransport {
	public static final int NUMBERS_WHEEL = 4;
	public static final int MAX_QUANTITY_PASSENGERS = 4;
	private int currentQuantityPassengers;

	public Taxi(String taxiNumber, BrandTransport brandTransport, BodyOfTransport bodyTransport, Engine engineTransport, List<Wheel> wheels) {
		super(taxiNumber, brandTransport, bodyTransport, engineTransport, wheels);
	}

	public int getCurrentQuantityPassengers() {
		return currentQuantityPassengers;
	}

	public void setCurrentQuantityPassengers(int currentQuantityPassengers) {
		this.currentQuantityPassengers = currentQuantityPassengers;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(currentQuantityPassengers);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Taxi))
			return false;
		Taxi other = (Taxi) obj;
		return currentQuantityPassengers == other.currentQuantityPassengers;
	}

	@Override
	public String toString() {
		return "\nTaxi [" + super.toString() + ", currentQuantityPassengers=" + currentQuantityPassengers + "]";
	}
}
