package by.baranov.simplehttpserver.specification;

import by.baranov.simplehttpserver.model.Engine;

public class SpecificationByIdEngineImpl implements Specification<Engine> {
	private long id;

	public SpecificationByIdEngineImpl(long id) {
		this.id = id;
	}
	
	@Override
	public boolean match(Engine engine) {
		return this.id == engine.getId();
	}
}
