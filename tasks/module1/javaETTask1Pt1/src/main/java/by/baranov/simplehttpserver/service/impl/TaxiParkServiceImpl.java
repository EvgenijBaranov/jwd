package by.baranov.simplehttpserver.service.impl;

import java.util.List;

import by.baranov.simplehttpserver.model.MiniBus;
import by.baranov.simplehttpserver.model.Taxi;
import by.baranov.simplehttpserver.model.TaxiCargo;
import by.baranov.simplehttpserver.model.TaxiPark;
import by.baranov.simplehttpserver.service.MiniBusService;
import by.baranov.simplehttpserver.service.TaxiCargoService;
import by.baranov.simplehttpserver.service.TaxiParkService;
import by.baranov.simplehttpserver.service.TaxiService;

public class TaxiParkServiceImpl implements TaxiParkService {
	private final TaxiService taxiService;
	private final TaxiCargoService taxiCargoService;
	private final MiniBusService miniBusService;

	public TaxiParkServiceImpl(TaxiService taxiService, TaxiCargoService taxiCargoService, MiniBusService miniBusService) {
		this.taxiService = taxiService;
		this.taxiCargoService = taxiCargoService;
		this.miniBusService = miniBusService;
	}

	@Override
	public TaxiPark createTaxiPark() {
		List<Taxi> listTaxi = taxiService.getAll();
		List<TaxiCargo> listTaxiCargo = taxiCargoService.getAll();
		List<MiniBus> listMiniBus = miniBusService.getAll();
		return new TaxiPark(listTaxi, listTaxiCargo, listMiniBus);		
	}

	@Override
	public List<Taxi> getTaxies(TaxiPark taxiPark) {
		List<Taxi> taxies = taxiService.getAll();
		return taxies;
	}

	@Override
	public List<TaxiCargo> getTaxiCargo(TaxiPark taxiPark) {
		List<TaxiCargo> taxiesCargo = taxiCargoService.getAll();
		return taxiesCargo;
	}

	@Override
	public List<MiniBus> getMiniBuses(TaxiPark taxiPark) {
		List<MiniBus> listMiniBus = miniBusService.getAll();
		return listMiniBus;
	}

}
