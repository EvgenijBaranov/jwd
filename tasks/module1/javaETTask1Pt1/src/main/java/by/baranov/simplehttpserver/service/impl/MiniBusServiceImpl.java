package by.baranov.simplehttpserver.service.impl;

import java.util.List;
import java.util.Optional;

import by.baranov.simplehttpserver.model.MiniBus;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.MiniBusService;
import by.baranov.simplehttpserver.specification.Specification;

public class MiniBusServiceImpl implements MiniBusService {
	private final Repository<MiniBus> repositoryMiniBus;

	public MiniBusServiceImpl(Repository<MiniBus> repositoryMiniBus) {
		this.repositoryMiniBus = repositoryMiniBus;
	}

	@Override
	public List<MiniBus> getAll() {
		List<MiniBus> miniBusAll = repositoryMiniBus.getAll();
		return miniBusAll;
	}

	@Override
	public Optional<MiniBus> getBy(Specification<MiniBus> specification) {
		Optional<MiniBus> miniBus = repositoryMiniBus.getBy(specification);
		return miniBus;
	}

}
