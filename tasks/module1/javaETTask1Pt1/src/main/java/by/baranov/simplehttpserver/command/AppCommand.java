package by.baranov.simplehttpserver.command;

import java.util.Map;

public interface AppCommand {
	
	String execute(Map<String,String> mapCommandNameAndParameters);
	
}
