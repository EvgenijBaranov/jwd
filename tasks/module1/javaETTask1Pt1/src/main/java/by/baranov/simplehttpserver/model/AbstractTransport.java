package by.baranov.simplehttpserver.model;

import java.util.List;
import java.util.Objects;

public abstract class AbstractTransport {
	private String registrationNumber;
	private BrandTransport brandTransport;
	private BodyOfTransport bodyTransport;
	private Engine engineTransport;
	private List<Wheel> wheels;

	public AbstractTransport(String registrationNumber, BrandTransport brandTransport, BodyOfTransport bodyTransport,
			Engine engineTransport, List<Wheel> wheels) {
		this.registrationNumber = registrationNumber;
		this.brandTransport = brandTransport;
		this.bodyTransport = bodyTransport;
		this.engineTransport = engineTransport;
		this.wheels = wheels;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public BrandTransport getBrandTransport() {
		return brandTransport;
	}

	public void setBrandTransport(BrandTransport brandTransport) {
		this.brandTransport = brandTransport;
	}

	public BodyOfTransport getBodyTransport() {
		return bodyTransport;
	}

	public void setBodyTransport(BodyOfTransport bodyTransport) {
		this.bodyTransport = bodyTransport;
	}

	public Engine getEngineTransport() {
		return engineTransport;
	}

	public void setEngineTransport(Engine engineTransport) {
		this.engineTransport = engineTransport;
	}

	public List<Wheel> getWheels() {
		return wheels;
	}

	public void setWheels(List<Wheel> wheels) {
		this.wheels = wheels;
	}

	@Override
	public int hashCode() {
		return Objects.hash(bodyTransport, brandTransport, engineTransport, registrationNumber, wheels);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof AbstractTransport))
			return false;
		AbstractTransport other = (AbstractTransport) obj;
		return Objects.equals(bodyTransport, other.bodyTransport) && brandTransport == other.brandTransport
				&& Objects.equals(engineTransport, other.engineTransport)
				&& Objects.equals(registrationNumber, other.registrationNumber) && Objects.equals(wheels, other.wheels);
	}

	@Override
	public String toString() {
		return "registrationNumber=" + registrationNumber + ", brandTransport=" + brandTransport + ", bodyTransport="
				+ bodyTransport + ", engineTransport=" + engineTransport + ", wheels=" + wheels;
	}

}
