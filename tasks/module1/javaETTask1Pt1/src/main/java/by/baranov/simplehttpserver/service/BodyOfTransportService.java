package by.baranov.simplehttpserver.service;

import by.baranov.simplehttpserver.model.BodyOfTransport;

public interface BodyOfTransportService extends CRUDOperation<BodyOfTransport> {
	
}
