package by.baranov.simplehttpserver.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.model.BodyOfTransport;
import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.MiniBus;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.specification.Specification;

public class RepositoryMiniBusImpl implements Repository<MiniBus> {

	private static final Logger LOGGER = Logger.getLogger(RepositoryMiniBusImpl.class);
	private List<MiniBus> miniBuses = new ArrayList<>();

	public RepositoryMiniBusImpl(int numberMiniBusesCreatedInRepository, Repository<BodyOfTransport> bodies,
									Repository<Engine> engines, Repository<Wheel> wheels) {

		for (int i = 0; i < numberMiniBusesCreatedInRepository; i++) {

			Engine engine = engines.getRandom();
			BodyOfTransport body = bodies.getRandom();
			Wheel wheel = wheels.getRandom();
			List<Wheel> miniBusWheels = new ArrayList<>();
			for (int j = 0; j < MiniBus.NUMBERS_WHEEL; j++) {
				miniBusWheels.add(wheel);
			}

			int sizeEnumBrandTrasport = BrandTransport.values().length;
			Random rand = new Random();
			int randomIndexEnumBrandTransport = rand.nextInt(sizeEnumBrandTrasport);
			BrandTransport brand = BrandTransport.values()[randomIndexEnumBrandTransport];

			MiniBus miniBus = new MiniBus("number" + i, brand, body, engine, miniBusWheels);
			miniBuses.add(miniBus);
		}
	}

	@Override
	public Optional<List<MiniBus>> findSome(Specification<MiniBus> specification) {
		List<MiniBus> result = new ArrayList<>();
		for (MiniBus miniBus : miniBuses) {
			if (specification.match(miniBus)) {
				result.add(miniBus);
			}
		}
		return Optional.ofNullable(result);
	}

	@Override
	public List<MiniBus> getAll() {
		return miniBuses;
	}

	@Override
	public Optional<MiniBus> getBy(Specification<MiniBus> specification) {
		for (MiniBus miniBus : miniBuses) {
			if (specification.match(miniBus)) {
				return Optional.of(miniBus);
			}
		}
		LOGGER.warn(this.getClass().getSimpleName() + " didn't find a minibus by : " + specification.getClass().getSimpleName());
		return Optional.empty();
	}

	@Override
	public MiniBus getRandom() {
		int sizeRepository = miniBuses.size();
		Random rand = new Random();
		MiniBus randomMiniBus = miniBuses.get(rand.nextInt(sizeRepository));
		return randomMiniBus;
	}

	@Override
	public String toString() {
		return "Repository of miniBuses : " + miniBuses;
	}
	
}
