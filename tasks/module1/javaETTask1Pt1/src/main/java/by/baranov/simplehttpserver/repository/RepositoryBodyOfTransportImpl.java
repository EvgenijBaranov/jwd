package by.baranov.simplehttpserver.repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.model.BodyOfTransport;
import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.specification.Specification;

public class RepositoryBodyOfTransportImpl implements Repository<BodyOfTransport> {
	
	private static final Logger LOGGER = Logger.getLogger(RepositoryBodyOfTransportImpl.class);
	private List<BodyOfTransport> bodies = new ArrayList<>();

	public RepositoryBodyOfTransportImpl() {
		
		BodyOfTransport body0 = new BodyOfTransport(0, Material.STEEL, getCapacityBody(), "BMW", LocalDate.of(2015, 5, 29));
		BodyOfTransport body1 = new BodyOfTransport(1, Material.STEEL, getCapacityBody(), "Ford", LocalDate.of(1990, 10, 10));
		BodyOfTransport body2 = new BodyOfTransport(2, Material.ALUMINIUM, getCapacityBody(), "Hyundai", LocalDate.of(1993, 12, 10));
		BodyOfTransport body3 = new BodyOfTransport(3, Material.CARBON, getCapacityBody(), "Honda", LocalDate.of(2000, 12, 21));
		BodyOfTransport body4 = new BodyOfTransport(4, Material.STEEL, getCapacityBody(), "Volkswagen", LocalDate.of(2010, 9, 30));
		
		bodies.add(body0);
		bodies.add(body1);
		bodies.add(body2);
		bodies.add(body3);
		bodies.add(body4);
	}

	private double getCapacityBody() {
		return ThreadLocalRandom.current().nextDouble(BodyOfTransport.MIN_CAPACITY, BodyOfTransport.MAX_CAPACITY);
	}

	@Override
	public Optional<List<BodyOfTransport>> findSome(Specification<BodyOfTransport> specification) {
		List<BodyOfTransport> result = new ArrayList<>();
		for (BodyOfTransport body : bodies) {
			if (specification.match(body)) {
				result.add(body);
			}
		}
		return Optional.ofNullable(result);
	}

	@Override
	public Optional<BodyOfTransport> getBy(Specification<BodyOfTransport> specification) {
		for (BodyOfTransport body : bodies) {
			if (specification.match(body)) {
				return Optional.of(body);
			}
		}
		LOGGER.warn(this.getClass().getSimpleName() + " didn't find a body of transport by : " + specification.getClass().getSimpleName());
		return Optional.empty();
	}

	@Override
	public List<BodyOfTransport> getAll() {
		return bodies;
	}

	@Override
	public BodyOfTransport getRandom() {
		Random rand = new Random();
		int sizeRepository = bodies.size();
		BodyOfTransport randomBody = bodies.get(rand.nextInt(sizeRepository));
		return randomBody;
	}

	@Override
	public String toString() {
		return "Repository of bodies for transport : " + bodies;
	}	

}
