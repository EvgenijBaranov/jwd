package by.baranov.simplehttpserver.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.model.BodyOfTransport;
import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.TaxiCargo;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.specification.Specification;

public class RepositoryTaxiCargoImpl implements Repository<TaxiCargo> {
	
	private static final Logger LOGGER = Logger.getLogger(RepositoryTaxiCargoImpl.class);
	private List<TaxiCargo> taxiesCargo = new ArrayList<>();

	public RepositoryTaxiCargoImpl(int numberTaxiesCargoCreatedInRepository, Repository<BodyOfTransport> bodies,
										Repository<Engine> engines, Repository<Wheel> wheels) {

		for (int i = 0; i < numberTaxiesCargoCreatedInRepository; i++) {

			Engine engine = engines.getRandom();
			BodyOfTransport body = bodies.getRandom();
			Wheel wheel = wheels.getRandom();
			List<Wheel> taxiWheels = new ArrayList<>();
			for (int j = 0; j < TaxiCargo.NUMBERS_WHEEL; j++) {
				taxiWheels.add(wheel);
			}

			int sizeEnumBrandTrasport = BrandTransport.values().length;
			Random rand = new Random();
			int randomIndexEnumBrandTransport = rand.nextInt(sizeEnumBrandTrasport);
			BrandTransport brand = BrandTransport.values()[randomIndexEnumBrandTransport];

			double randomMaxPermissibleMassOfGoods = getMaxPermissibleMassOfGoods();

			TaxiCargo taxiCargo = new TaxiCargo("number" + i, brand, body, engine, taxiWheels,
					randomMaxPermissibleMassOfGoods);
			taxiesCargo.add(taxiCargo);
		}
	}

	private double getMaxPermissibleMassOfGoods() {
		return ThreadLocalRandom.current().nextDouble(TaxiCargo.MIN_VALUE_MASS_OF_GOODS,
				TaxiCargo.MAX_VALUE_MASS_OF_GOODS);
	}

	@Override
	public Optional<List<TaxiCargo>> findSome(Specification<TaxiCargo> specification) {
		List<TaxiCargo> result = new ArrayList<>();
		for (TaxiCargo taxiCargo : taxiesCargo) {
			if (specification.match(taxiCargo)) {
				result.add(taxiCargo);
			}
		}
		return Optional.ofNullable(result);
	}

	@Override
	public List<TaxiCargo> getAll() {
		return taxiesCargo;
	}

	@Override
	public Optional<TaxiCargo> getBy(Specification<TaxiCargo> specification) {
		for (TaxiCargo taxiCargo : taxiesCargo) {
			if (specification.match(taxiCargo)) {
				return Optional.of(taxiCargo);
			}
		}
		LOGGER.warn(this.getClass().getSimpleName() + " didn't find a taxi cargo by : " + specification.getClass().getSimpleName());
		return Optional.empty();
	}

	@Override
	public TaxiCargo getRandom() {
		int sizeRepository = taxiesCargo.size();
		Random rand = new Random();
		TaxiCargo randomTaxiCargo = taxiesCargo.get(rand.nextInt(sizeRepository));
		return randomTaxiCargo;
	}

	@Override
	public String toString() {
		return "Repository of taxi cargo : " + taxiesCargo;
	}

}
