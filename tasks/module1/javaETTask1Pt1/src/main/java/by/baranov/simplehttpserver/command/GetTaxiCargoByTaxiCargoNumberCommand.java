package by.baranov.simplehttpserver.command;

import java.util.Map;
import java.util.Optional;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.model.TaxiCargo;
import by.baranov.simplehttpserver.service.TaxiCargoService;
import by.baranov.simplehttpserver.specification.Specification;
import by.baranov.simplehttpserver.specification.SpecificationByTaxiCargoNumberImpl;

public class GetTaxiCargoByTaxiCargoNumberCommand implements AppCommand {
	private static final Logger LOGGER = Logger.getLogger(GetTaxiCargoByTaxiCargoNumberCommand.class);
	private final TaxiCargoService taxiCargoService;
	
	public GetTaxiCargoByTaxiCargoNumberCommand(TaxiCargoService taxiCargoService) {
		this.taxiCargoService = taxiCargoService;
		LOGGER.info(this.getClass().getSimpleName() + " is created with services : " + taxiCargoService.getClass().getSimpleName());
	}

	@Override
	public String execute(Map<String, String> mapCommandNameAndParameters) {
		String inputedTaxiCargoNumber = mapCommandNameAndParameters.get("taxiCargoNumber");
		LOGGER.info(this.getClass().getSimpleName() + " has recieved next taxi cargo number : " + inputedTaxiCargoNumber);
		Specification<TaxiCargo> spec = new SpecificationByTaxiCargoNumberImpl(inputedTaxiCargoNumber);
		Optional<TaxiCargo> taxiCargo = taxiCargoService.getBy(spec);
		LOGGER.info(this.getClass().getSimpleName() + " has received taxi cargo : " + taxiCargo);
		
		StringBuilder result = new StringBuilder();
		result.append("<h2>Taxi cargo with number = " + inputedTaxiCargoNumber + "</h2>");
		result.append(CommandUtility.transportToStringForTemplateHTMLFile(taxiCargo));
		LOGGER.info(this.getClass().getSimpleName() + " is executed");
		return result.toString();
	}
}
