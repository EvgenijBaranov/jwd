package by.baranov.simplehttpserver.model;

public enum BrandTransport {
	FORD, TOYOTA, OPEL, VOLVO, VOLKSWAGEN
}
