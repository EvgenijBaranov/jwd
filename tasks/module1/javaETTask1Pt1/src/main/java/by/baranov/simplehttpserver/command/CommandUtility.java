package by.baranov.simplehttpserver.command;

import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.model.AbstractTransport;

public class CommandUtility {
	private static final Logger LOGGER = Logger.getLogger(CommandUtility.class);
	
	public static StringBuilder transportToStringForTemplateHTMLFile(Optional<? extends AbstractTransport> transport) {
		StringBuilder result = new StringBuilder();
		result.append("<ul>");
		if (transport.isPresent()) {
			result.append(transport.get());
		} else {
			LOGGER.warn("Transport received by command doesn't exist");
			result.append("There aren't such registartion number");
		}
		result.append("</ul>");
		return result;
	}

	public static StringBuilder listTransportToStringForTemplateHTMLFile(List<? extends AbstractTransport> listTransport) {
		StringBuilder result = new StringBuilder();
		result.append("<h2>" + listTransport.get(0).getClass().getSimpleName()+ ":</h2>");
		result.append("<ul>");
		for (AbstractTransport transport : listTransport) {
			result.append("<li>");
			result.append(transport.toString());
			result.append("</li>");
		}
		result.append("</ul>");
		return result;
	}

}
