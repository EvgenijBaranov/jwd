package by.baranov.simplehttpserver.specification;

import by.baranov.simplehttpserver.model.Engine;

public class SpecificationByProducerEngineImpl implements Specification<Engine> {
	private String producer;

	public SpecificationByProducerEngineImpl(String producer) {
		this.producer = producer;
	}

	@Override
	public boolean match(Engine engine) {
		return this.producer.equalsIgnoreCase(engine.getProducer());
	}
}
