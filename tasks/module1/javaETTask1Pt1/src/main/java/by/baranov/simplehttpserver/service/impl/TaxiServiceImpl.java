package by.baranov.simplehttpserver.service.impl;

import java.util.List;
import java.util.Optional;

import by.baranov.simplehttpserver.model.Taxi;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.TaxiService;
import by.baranov.simplehttpserver.specification.Specification;

public class TaxiServiceImpl implements TaxiService {
	private final Repository<Taxi> repositoryTaxi;

	public TaxiServiceImpl(Repository<Taxi> repositoryTaxi) {
		this.repositoryTaxi = repositoryTaxi;
	}

	@Override
	public List<Taxi> getAll() {
		List<Taxi> taxiesAll = repositoryTaxi.getAll();
		return taxiesAll;
	}

	@Override
	public Optional<Taxi> getBy(Specification<Taxi> specification) {
		Optional<Taxi> taxi = repositoryTaxi.getBy(specification);
		return taxi;
	}

}
