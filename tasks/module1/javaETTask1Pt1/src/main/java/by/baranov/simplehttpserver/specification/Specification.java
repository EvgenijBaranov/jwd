package by.baranov.simplehttpserver.specification;

public interface Specification<T> {
	
	boolean match(T bean);
	
}
