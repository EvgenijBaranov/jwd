package by.baranov.simplehttpserver.controller;

import by.baranov.simplehttpserver.command.AppCommand;

public interface AppCommandFactory {
	
	AppCommand getCommand(String commandName);
	
}
