package by.baranov.simplehttpserver.specification;

import by.baranov.simplehttpserver.model.BodyOfTransport;

public class SpecificationByIdBodyOfTransportImpl implements Specification<BodyOfTransport>{
	private long id;

	public SpecificationByIdBodyOfTransportImpl(long id) {
		this.id = id;
	}
	
	@Override
	public boolean match(BodyOfTransport body) {
		return this.id == body.getId();
	}
}
