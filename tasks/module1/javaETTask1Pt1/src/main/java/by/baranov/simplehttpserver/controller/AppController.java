package by.baranov.simplehttpserver.controller;

import java.util.Map;

import org.apache.log4j.Logger;

import by.baranov.simplehttpserver.command.AppCommand;

public class AppController {
	private static final Logger LOGGER = Logger.getLogger(AppController.class);
	private final AppCommandFactory commandFactory;

	public AppController(AppCommandFactory commandFactory) {
		this.commandFactory = commandFactory;
	}

	public String handleUserDate(String userCommand, Map<String,String> mapCommandNameAndParameters) {
		AppCommand command = commandFactory.getCommand(userCommand);
		LOGGER.info(this.getClass().getSimpleName() + " has received commnad from command factory of application : " + command.getClass().getSimpleName());
		String resultCommand = command.execute(mapCommandNameAndParameters);
		return resultCommand;
	}

}
