package by.baranov.simplehttpserver.launch;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.EnumMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;

import by.baranov.simplehttpserver.command.AppCommand;
import by.baranov.simplehttpserver.command.GetMiniBusByMiniBusNumberCommand;
import by.baranov.simplehttpserver.command.GetTaxiByTaxiNumberCommand;
import by.baranov.simplehttpserver.command.GetTaxiCargoByTaxiCargoNumberCommand;
import by.baranov.simplehttpserver.command.GetTaxiParkCommand;
import by.baranov.simplehttpserver.controller.AppCommandFactory;
import by.baranov.simplehttpserver.controller.AppCommandFactoryImpl;
import by.baranov.simplehttpserver.controller.AppCommandName;
import by.baranov.simplehttpserver.controller.AppController;
import by.baranov.simplehttpserver.controller.MyHttpHandler;
import by.baranov.simplehttpserver.model.BodyOfTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.MiniBus;
import by.baranov.simplehttpserver.model.Taxi;
import by.baranov.simplehttpserver.model.TaxiCargo;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.repository.RepositoryBodyOfTransportImpl;
import by.baranov.simplehttpserver.repository.RepositoryEngineImpl;
import by.baranov.simplehttpserver.repository.RepositoryMiniBusImpl;
import by.baranov.simplehttpserver.repository.RepositoryTaxiCargoImpl;
import by.baranov.simplehttpserver.repository.RepositoryTaxiImpl;
import by.baranov.simplehttpserver.repository.RepositoryWheelImpl;
import by.baranov.simplehttpserver.service.MiniBusService;
import by.baranov.simplehttpserver.service.TaxiCargoService;
import by.baranov.simplehttpserver.service.TaxiService;
import by.baranov.simplehttpserver.service.WheelService;
import by.baranov.simplehttpserver.service.impl.MiniBusServiceImpl;
import by.baranov.simplehttpserver.service.impl.TaxiCargoServiceImpl;
import by.baranov.simplehttpserver.service.impl.TaxiServiceImpl;
import by.baranov.simplehttpserver.service.impl.WheelServiceImpl;
import by.baranov.simplehttpserver.specification.Specification;
import by.baranov.simplehttpserver.specification.SpecificationByIdWheelImpl;

public class ServerApp {
	private static final int QUANTITY_TAXI_IN_REPOSITORY = 5;
	private static final int QUANTITY_TAXI_CARGO_IN_REPOSITORY = 5;
	private static final int QUANTITY_MINI_BUS_IN_REPOSITORY = 5;
	private static final Logger LOGGER = Logger.getLogger(ServerApp.class);

	public static void main(String[] args) throws IOException {
		InetSocketAddress localhost = new InetSocketAddress(49090);
		HttpServer server = HttpServer.create(localhost, 0);
		
		Repository<BodyOfTransport> repositoryBodies = new RepositoryBodyOfTransportImpl();
		Repository<Engine> repositoryEngines = new RepositoryEngineImpl();
		Repository<Wheel> repositoryWheels = new RepositoryWheelImpl();
		Repository<Taxi> repositoryTaxies = new RepositoryTaxiImpl(QUANTITY_TAXI_IN_REPOSITORY, repositoryBodies, repositoryEngines, repositoryWheels);
		Repository<TaxiCargo> repositoryTaxiesCargo = new RepositoryTaxiCargoImpl(QUANTITY_TAXI_CARGO_IN_REPOSITORY, repositoryBodies, repositoryEngines, repositoryWheels);
		Repository<MiniBus> repositoryMiniBuses = new RepositoryMiniBusImpl(QUANTITY_MINI_BUS_IN_REPOSITORY, repositoryBodies, repositoryEngines, repositoryWheels);
		LOGGER.info("Repository of minibuses is created : " + repositoryMiniBuses);
		LOGGER.info("Repository of taxi cargo is created : " + repositoryTaxiesCargo);
		LOGGER.info("Repository of taxies is created : " + repositoryTaxies);
		
		TaxiService taxiService = new TaxiServiceImpl(repositoryTaxies);
		TaxiCargoService taxiCargoService = new TaxiCargoServiceImpl(repositoryTaxiesCargo);
		MiniBusService miniBusService = new MiniBusServiceImpl(repositoryMiniBuses);

		AppCommand printTaxiParkCommand = new GetTaxiParkCommand(taxiService, taxiCargoService, miniBusService);
		AppCommand getTaxiByTaxiNumberCommand = new GetTaxiByTaxiNumberCommand(taxiService);
		AppCommand getTaxiCargoByTaxiCargoNumberCommand = new GetTaxiCargoByTaxiCargoNumberCommand(taxiCargoService);
		AppCommand getMiniBusByMiniBusNumberCommand = new GetMiniBusByMiniBusNumberCommand(miniBusService);

		Map<AppCommandName, AppCommand> mapCommands = new EnumMap<>(AppCommandName.class);
		mapCommands.put(AppCommandName.GET_ALL_TAXIPARK, printTaxiParkCommand);
		mapCommands.put(AppCommandName.GET_TAXI_BY_TAXI_NUMBER, getTaxiByTaxiNumberCommand);
		mapCommands.put(AppCommandName.GET_TAXICARGO_BY_TAXICARGO_NUMBER, getTaxiCargoByTaxiCargoNumberCommand);
		mapCommands.put(AppCommandName.GET_MINIBUS_BY_MINIBUS_NUMBER, getMiniBusByMiniBusNumberCommand);
		LOGGER.info("Command map is assembled : " + mapCommands);

		AppCommandFactory commandFactory = new AppCommandFactoryImpl(mapCommands);
		
		AppController controller = new AppController(commandFactory);
		LOGGER.info("Controller is assembled");

		WheelService serviceWheel = new WheelServiceImpl(repositoryWheels);
		Specification<Wheel> spec = new SpecificationByIdWheelImpl(3);
		Optional<Wheel> wheel = serviceWheel.getBy(spec);
		if (wheel.isPresent()) {
			System.out.println(wheel.get());
		} else {
			System.out.println("Wheel with such id isn't exist");
		}

		HttpContext serverContext = server.createContext("/MySimpleServer", new MyHttpHandler(controller));

		ExecutorService executor = Executors.newFixedThreadPool(4);
		server.setExecutor(executor);
		server.start();
		System.out.println("Server started on " + server.getAddress().toString());

	}

}
