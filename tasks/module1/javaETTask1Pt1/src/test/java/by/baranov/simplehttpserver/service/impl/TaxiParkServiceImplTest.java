package by.baranov.simplehttpserver.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.simplehttpserver.model.BodyOfTransport;
import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.model.MiniBus;
import by.baranov.simplehttpserver.model.Taxi;
import by.baranov.simplehttpserver.model.TaxiCargo;
import by.baranov.simplehttpserver.model.TaxiPark;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.MiniBusService;
import by.baranov.simplehttpserver.service.TaxiCargoService;
import by.baranov.simplehttpserver.service.TaxiParkService;
import by.baranov.simplehttpserver.service.TaxiService;

@RunWith(MockitoJUnitRunner.class)
public class TaxiParkServiceImplTest {
	
	private List<Taxi> expectedListTaxi = new ArrayList<>();
	private List<TaxiCargo> expectedListTaxiCargo = new ArrayList<>();
	private List<MiniBus> expectedListMiniBus = new ArrayList<>();
	private TaxiPark expectedTaxiPark;
	private TaxiCargoService taxiCargoService;
	private MiniBusService miniBusService;
	private TaxiParkService taxiParkService;
	private TaxiService taxiService;
	
	@Mock
	private Repository<Taxi> repositoryTaxi;
	@Mock
	private Repository<TaxiCargo> repositoryTaxiCargo;
	@Mock
	private Repository<MiniBus> repositoryMiniBus;
	
	
	
	@Before
	public void initTaxiPark() {
		Wheel wheel = new Wheel(1, 10, "OPEL", LocalDate.of(2020, 04, 9));
		Engine engine = new Engine(1, 100, "OPEL", LocalDate.of(2020, 04, 9));
		BodyOfTransport body = new BodyOfTransport(1, Material.ALUMINIUM, 120.8, "OPEL", LocalDate.of(2020, 04, 9));
		List<Wheel> wheelsForMiniBus = new ArrayList<>();
		for (int i = 0; i < MiniBus.NUMBERS_WHEEL; i++) {
			wheelsForMiniBus.add(wheel);
		}
		MiniBus miniBus = new MiniBus("MiniBus132", BrandTransport.FORD, body, engine, wheelsForMiniBus);
		
		List<Wheel> wheelsForTaxiCargo = new ArrayList<>();
		for (int i = 0; i < TaxiCargo.NUMBERS_WHEEL; i++) {
			wheelsForTaxiCargo.add(wheel);
		}
		double maxPermissibleMassOfGoods = 3000;
		TaxiCargo taxiCargo = new TaxiCargo("TaxiCargo132", BrandTransport.FORD, body, engine, wheelsForTaxiCargo, maxPermissibleMassOfGoods);
		
		List<Wheel> wheelsForTaxi = new ArrayList<>();
		for (int i = 0; i < Taxi.NUMBERS_WHEEL; i++) {
			wheelsForTaxi.add(wheel);
		}
		Taxi taxi = new Taxi("Taxi132", BrandTransport.FORD, body, engine, wheelsForTaxi);
		
		
		for(int i = 0; i < 2; i++) {
			expectedListTaxi.add(taxi);
			expectedListTaxiCargo.add(taxiCargo);
			expectedListMiniBus.add(miniBus);
		}
		expectedTaxiPark = new TaxiPark(expectedListTaxi, expectedListTaxiCargo, expectedListMiniBus);
		
		taxiService = new TaxiServiceImpl(repositoryTaxi);
		taxiCargoService = new TaxiCargoServiceImpl(repositoryTaxiCargo);
		miniBusService = new MiniBusServiceImpl(repositoryMiniBus);
		taxiParkService = new TaxiParkServiceImpl(taxiService, taxiCargoService, miniBusService);
	}
	
	@Test
	public void createTaxiParkTest() {
		when(repositoryTaxi.getAll()).thenReturn(expectedListTaxi);
		when(repositoryTaxiCargo.getAll()).thenReturn(expectedListTaxiCargo);
		when(repositoryMiniBus.getAll()).thenReturn(expectedListMiniBus);		
		TaxiPark foundTaxiPark = taxiParkService.createTaxiPark();
		assertEquals(expectedTaxiPark, foundTaxiPark);		
	}
		
	@Test
	public void getTaxiesTest() {		
		when(repositoryTaxi.getAll()).thenReturn(expectedListTaxi);		
		List<Taxi> foundListTaxi = taxiParkService.getTaxies(expectedTaxiPark);
		assertEquals(expectedListTaxi, foundListTaxi);		
	}
	
	@Test
	public void getTaxiCargoTest() {		
		when(repositoryTaxiCargo.getAll()).thenReturn(expectedListTaxiCargo);		
		List<TaxiCargo> foundListTaxiCargo = taxiParkService.getTaxiCargo(expectedTaxiPark);
		assertEquals(expectedListTaxiCargo, foundListTaxiCargo);		
	}

	@Test
	public void getMiniBusTest() {		
		when(repositoryMiniBus.getAll()).thenReturn(expectedListMiniBus);		
		List<MiniBus> foundListMiniBus = taxiParkService.getMiniBuses(expectedTaxiPark);
		assertEquals(expectedListMiniBus, foundListMiniBus);		
	}
	
	@After
	public void clearTaxiPark() {
		taxiService = null;
		taxiCargoService = null;
		miniBusService = null;
		taxiParkService = null;
		expectedListTaxi = null;
		expectedListTaxiCargo = null;
		expectedListMiniBus = null;
		expectedTaxiPark = null;
		repositoryTaxi = null;
		repositoryTaxiCargo = null;
		repositoryMiniBus = null;
	}

}
