package by.baranov.simplehttpserver.specification;

import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.simplehttpserver.model.BodyOfTransport;
import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.model.MiniBus;
import by.baranov.simplehttpserver.model.Wheel;

public class SpecificationByMiniBusNumberImplTest {
	private MiniBus miniBus;
	private SpecificationByMiniBusNumberImpl specification;

	@Before
	public void prepareMiniBusAndSpecification() {
		Wheel wheel = new Wheel(1, 10, "OPEL", LocalDate.of(2020, 04, 9));
		Engine engine = new Engine(1, 100, "OPEL", LocalDate.of(2020, 04, 9));
		BodyOfTransport body = new BodyOfTransport(1, Material.ALUMINIUM, 120.8, "OPEL", LocalDate.of(2020, 04, 9));
		List<Wheel> wheels = new ArrayList<>();
		for (int i = 0; i < MiniBus.NUMBERS_WHEEL; i++) {
			wheels.add(wheel);
		}
		miniBus = new MiniBus("ASD132", BrandTransport.FORD, body, engine, wheels);
		specification = new SpecificationByMiniBusNumberImpl("ASD132");
	}

	@Test
	public void testMatch() {
		boolean output = specification.match(miniBus);
		assertTrue(output);
	}

	@After
	public void clearMiniBusWithSpecification() {
		miniBus = null;
		specification = null;
	}

}
