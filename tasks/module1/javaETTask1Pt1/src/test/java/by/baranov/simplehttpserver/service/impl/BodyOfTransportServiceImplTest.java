package by.baranov.simplehttpserver.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.simplehttpserver.model.BodyOfTransport;
import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.BodyOfTransportService;
import by.baranov.simplehttpserver.specification.Specification;
import by.baranov.simplehttpserver.specification.SpecificationByIdBodyOfTransportImpl;

@RunWith(MockitoJUnitRunner.class)
public class BodyOfTransportServiceImplTest {
	
	private BodyOfTransportService bodyService;
	private BodyOfTransport body;

	@Mock
	private Repository<BodyOfTransport> repository;	
	
	@Before
	public void initEngineWithWheelService() {
		body = new BodyOfTransport(1, Material.ALUMINIUM, 120.8, "OPEL", LocalDate.of(2020, 04, 9));
		bodyService = new BodyOfTransportServiceImpl(repository);
	}

	@Test
	public void getAllTest() {
		List<BodyOfTransport> expectedBodies = Arrays.asList(body, body);
		when(repository.getAll()).thenReturn(expectedBodies);
		List<BodyOfTransport> foundBodies = bodyService.getAll();
		assertEquals(expectedBodies, foundBodies);
	}
	
	@Test
	public void getBySpecifiationTest() {
		Optional<BodyOfTransport> expectedBody = Optional.of(body);
		Specification<BodyOfTransport> specification = new SpecificationByIdBodyOfTransportImpl(body.getId());
		when(repository.getBy(specification)).thenReturn(expectedBody);
		Optional<BodyOfTransport> foundBody = bodyService.getBy(specification);
		assertEquals(expectedBody.get(), foundBody.get());		
	}
	@After
	public void clearBodyWithBodyService() {
		body = null;
		bodyService = null;
	}

}
