package by.baranov.simplehttpserver.specification;

import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.simplehttpserver.model.BodyOfTransport;
import by.baranov.simplehttpserver.model.Material;

public class SpecificationByIdBodyOfTransportImplTest {
	private BodyOfTransport body;
	private SpecificationByIdBodyOfTransportImpl specification;
	
	@Before
	public void initBodyOfTransportWithSpecification() {
		body = new BodyOfTransport(1, Material.ALUMINIUM, 120.8, "OPEL", LocalDate.of(2020, 04, 9));		
		specification = new SpecificationByIdBodyOfTransportImpl(1);
	}
	
	@Test
	public void matchTest() {
		boolean output = specification.match(body);
		assertTrue(output);
	}
	
	@After
	public void clearBodyOfTransportWithSpecification() {
		body = null;		
		specification = null;
	}

}
