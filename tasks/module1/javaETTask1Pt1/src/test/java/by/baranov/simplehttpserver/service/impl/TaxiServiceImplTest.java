package by.baranov.simplehttpserver.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.simplehttpserver.model.BodyOfTransport;
import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.model.Taxi;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.TaxiService;
import by.baranov.simplehttpserver.specification.Specification;
import by.baranov.simplehttpserver.specification.SpecificationByTaxiNumberImpl;

@RunWith(MockitoJUnitRunner.class)
public class TaxiServiceImplTest {

	private TaxiService taxiService;
	private Taxi taxi;

	@Mock
	private Repository<Taxi> repository;

	@Before
	public void initTaxiWithTaxiService() {
		Wheel wheel = new Wheel(1, 10, "OPEL", LocalDate.of(2020, 04, 9));
		Engine engine = new Engine(1, 100, "OPEL", LocalDate.of(2020, 04, 9));
		BodyOfTransport body = new BodyOfTransport(1, Material.ALUMINIUM, 120.8, "OPEL", LocalDate.of(2020, 04, 9));
		List<Wheel> wheels = new ArrayList<>();
		for (int i = 0; i < Taxi.NUMBERS_WHEEL; i++) {
			wheels.add(wheel);
		}
		taxi = new Taxi("ASD132", BrandTransport.FORD, body, engine, wheels);
		taxiService = new TaxiServiceImpl(repository);
	}

	@Test
	public void getAllTest() {
		List<Taxi> expectedTaxies = Arrays.asList(taxi, taxi);
		when(repository.getAll()).thenReturn(expectedTaxies);
		List<Taxi> foundTaxies = taxiService.getAll();
		assertEquals(expectedTaxies, foundTaxies);
	}

	@Test
	public void getBySpecifiationTest() {
		Optional<Taxi> expectedTaxi = Optional.of(taxi);
		Specification<Taxi> specification = new SpecificationByTaxiNumberImpl(taxi.getRegistrationNumber());
		when(repository.getBy(specification)).thenReturn(expectedTaxi);
		Optional<Taxi> foundTaxi = taxiService.getBy(specification);
		assertEquals(expectedTaxi, foundTaxi);
	}

	@After
	public void clearTaxiWithTaxiService() {
		taxi = null;
		taxiService = null;
		repository = null;
	}

}
