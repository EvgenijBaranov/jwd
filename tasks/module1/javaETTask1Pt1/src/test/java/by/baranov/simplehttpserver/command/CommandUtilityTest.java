package by.baranov.simplehttpserver.command;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.simplehttpserver.model.AbstractTransport;
import by.baranov.simplehttpserver.model.BodyOfTransport;
import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.model.MiniBus;
import by.baranov.simplehttpserver.model.Wheel;

@RunWith(MockitoJUnitRunner.class)
public class CommandUtilityTest {
	
	private StringBuilder expectedResultForTransport = new StringBuilder();
	private Optional<? extends AbstractTransport> transport;
	private List<? extends AbstractTransport> listTransport;

	@Before
	public void init() {		
		Wheel wheel = new Wheel(1, 10, "OPEL", LocalDate.of(2020, 04, 9));
		Engine engine = new Engine(1, 100, "OPEL", LocalDate.of(2020, 04, 9));
		BodyOfTransport body = new BodyOfTransport(1, Material.ALUMINIUM, 120.8, "OPEL", LocalDate.of(2020, 04, 9));
		List<Wheel> wheelsForMiniBus = new ArrayList<>();
		for (int i = 0; i < MiniBus.NUMBERS_WHEEL; i++) {
			wheelsForMiniBus.add(wheel);
		}
		MiniBus miniBus1 = new MiniBus("MiniBus132", BrandTransport.FORD, body, engine, wheelsForMiniBus);
		MiniBus miniBus2 = new MiniBus("MiniBus456", BrandTransport.FORD, body, engine, wheelsForMiniBus);
		
		transport = Optional.of(miniBus1);
		listTransport = Arrays.asList(miniBus1, miniBus2);			
	}

	@Test
	public void transportToStringForTemplateHTMLFileTest() {
		expectedResultForTransport.append("<ul>");
		expectedResultForTransport.append(transport.get());
		expectedResultForTransport.append("</ul>");		
		StringBuilder foundResultForTransport = CommandUtility.transportToStringForTemplateHTMLFile(transport);
		assertEquals(expectedResultForTransport.toString(), foundResultForTransport.toString());		
	}

	@Test
	public void transportEmptyToStringForTemplateHTMLFileTest() {
		expectedResultForTransport.append("<ul>");
		expectedResultForTransport.append("There aren't such registartion number");
		expectedResultForTransport.append("</ul>");		
		StringBuilder foundResultForTransport = CommandUtility.transportToStringForTemplateHTMLFile(Optional.empty());
		assertEquals(expectedResultForTransport.toString(), foundResultForTransport.toString());		
	}
	@Test
	public void listTransportToStringForTemplateHTMLFileTest() {
		StringBuilder expectedResultForListTransport = new StringBuilder();
		expectedResultForListTransport.append("<h2>" + listTransport.get(0).getClass().getSimpleName()+ ":</h2>");
		expectedResultForListTransport.append("<ul>");
		for (AbstractTransport transport : listTransport) {
			expectedResultForListTransport.append("<li>");
			expectedResultForListTransport.append(transport.toString());
			expectedResultForListTransport.append("</li>");
		}
		expectedResultForListTransport.append("</ul>");
		StringBuilder foundResultForListTransport = CommandUtility.listTransportToStringForTemplateHTMLFile(listTransport);
		assertEquals(expectedResultForListTransport.toString(), foundResultForListTransport.toString());	
	}
	
	@After
	public void clear() {
		expectedResultForTransport = null;
		transport = null;
		listTransport = null;
	}

}
