package by.baranov.simplehttpserver.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.EngineService;
import by.baranov.simplehttpserver.specification.Specification;
import by.baranov.simplehttpserver.specification.SpecificationByIdEngineImpl;

@RunWith(MockitoJUnitRunner.class)
public class EngineServiceImplTest {
	
	private EngineService engineService;
	private Engine engine;

	@Mock
	private Repository<Engine> repository;	
	
	@Before
	public void initEngineWithWheelService() {
		engine = new Engine(1, 100, "OPEL", LocalDate.of(2020, 04, 9));
		engineService = new EngineServiceImpl(repository);
	}

	@Test
	public void getAllTest() {
		List<Engine> expectedEngines = Arrays.asList(engine, engine);
		when(repository.getAll()).thenReturn(expectedEngines);
		List<Engine> foundEngines = engineService.getAll();
		assertEquals(expectedEngines, foundEngines);
	}
	
	@Test
	public void getBySpecifiationTest() {
		Optional<Engine> expectedEngine = Optional.of(engine);
		Specification<Engine> specification = new SpecificationByIdEngineImpl(engine.getId());
		when(repository.getBy(specification)).thenReturn(expectedEngine);
		Optional<Engine> foundEngine = engineService.getBy(specification);
		assertEquals(expectedEngine, foundEngine);		
	}
	@After
	public void clearEngineWithEngineService() {
		engine = null;
		engineService = null;
	}
}
