package by.baranov.simplehttpserver.command;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.simplehttpserver.model.BodyOfTransport;
import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.model.TaxiCargo;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.TaxiCargoService;
import by.baranov.simplehttpserver.service.impl.TaxiCargoServiceImpl;
import by.baranov.simplehttpserver.specification.Specification;
import by.baranov.simplehttpserver.specification.SpecificationByTaxiCargoNumberImpl;

@RunWith(MockitoJUnitRunner.class)
public class GetTaxiCargoByTaxiCargoNumberCommandTest {

	private List<TaxiCargo> expectedListTaxiCargo = new ArrayList<>();
	private TaxiCargoService taxiCargoService;
	private GetTaxiCargoByTaxiCargoNumberCommand command;

	@Mock
	private Repository<TaxiCargo> repositoryTaxiCargo;

	@Before
	public void initListTaxiWithTaxiServiceAndCommand() {
		Wheel wheel = new Wheel(1, 10, "OPEL", LocalDate.of(2020, 04, 9));
		Engine engine = new Engine(1, 100, "OPEL", LocalDate.of(2020, 04, 9));
		BodyOfTransport body = new BodyOfTransport(1, Material.ALUMINIUM, 120.8, "OPEL", LocalDate.of(2020, 04, 9));

		List<Wheel> wheelsForTaxiCargo = new ArrayList<>();
		for (int i = 0; i < TaxiCargo.NUMBERS_WHEEL; i++) {
			wheelsForTaxiCargo.add(wheel);
		}
		double maxPermissibleMassOfGoods = 3000;
		TaxiCargo taxiCargo1 = new TaxiCargo("TaxiCargo132", BrandTransport.FORD, body, engine, wheelsForTaxiCargo, maxPermissibleMassOfGoods);
		TaxiCargo taxiCargo2 = new TaxiCargo("TaxiCargo543", BrandTransport.FORD, body, engine, wheelsForTaxiCargo, maxPermissibleMassOfGoods);
		
		expectedListTaxiCargo.add(taxiCargo1);
		expectedListTaxiCargo.add(taxiCargo2);

		taxiCargoService = new TaxiCargoServiceImpl(repositoryTaxiCargo);
		command = new GetTaxiCargoByTaxiCargoNumberCommand(taxiCargoService);
	}

	@Test
	public void executeTest() {
		Specification<TaxiCargo> specification = new SpecificationByTaxiCargoNumberImpl("TaxiCargo132");
		when(repositoryTaxiCargo.getBy(specification)).thenReturn(Optional.of(expectedListTaxiCargo.get(0)));
		Optional<TaxiCargo> expectedTaxiCargo = taxiCargoService.getBy(specification);

		StringBuilder expectedResultCommand = new StringBuilder();
		expectedResultCommand.append("<h2>Taxi cargo with number = " + expectedTaxiCargo.get().getRegistrationNumber() + "</h2>");
		expectedResultCommand.append(CommandUtility.transportToStringForTemplateHTMLFile(expectedTaxiCargo));

		Map<String, String> mapForCommand = new HashMap<>();
		mapForCommand.put("taxiCargoNumber", "TaxiCargo132");
		String foundResultCommand = command.execute(mapForCommand);

		assertEquals(expectedResultCommand.toString(), foundResultCommand);
	}

	@After
	public void clearListTaxiWithTaxiServiceAndCommand() {
		taxiCargoService = null;
		expectedListTaxiCargo = null;
		repositoryTaxiCargo = null;
		command = null;
	}
	
	
}
