package by.baranov.simplehttpserver.command;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.simplehttpserver.model.BodyOfTransport;
import by.baranov.simplehttpserver.model.BrandTransport;
import by.baranov.simplehttpserver.model.Engine;
import by.baranov.simplehttpserver.model.Material;
import by.baranov.simplehttpserver.model.Taxi;
import by.baranov.simplehttpserver.model.Wheel;
import by.baranov.simplehttpserver.repository.Repository;
import by.baranov.simplehttpserver.service.TaxiService;
import by.baranov.simplehttpserver.service.impl.TaxiServiceImpl;
import by.baranov.simplehttpserver.specification.Specification;
import by.baranov.simplehttpserver.specification.SpecificationByTaxiNumberImpl;

@RunWith(MockitoJUnitRunner.class)
public class GetTaxiByTaxiNumberCommandTest {

	private List<Taxi> expectedListTaxi = new ArrayList<>();
	private TaxiService taxiService;
	private GetTaxiByTaxiNumberCommand command;

	@Mock
	private Repository<Taxi> repositoryTaxi;

	@Before
	public void initListTaxiWithTaxiServiceAndCommand() {
		Wheel wheel = new Wheel(1, 10, "OPEL", LocalDate.of(2020, 04, 9));
		Engine engine = new Engine(1, 100, "OPEL", LocalDate.of(2020, 04, 9));
		BodyOfTransport body = new BodyOfTransport(1, Material.ALUMINIUM, 120.8, "OPEL", LocalDate.of(2020, 04, 9));
		List<Wheel> wheelsForTaxi = new ArrayList<>();
		for (int i = 0; i < Taxi.NUMBERS_WHEEL; i++) {
			wheelsForTaxi.add(wheel);
		}
		Taxi taxi1 = new Taxi("Taxi132", BrandTransport.FORD, body, engine, wheelsForTaxi);
		Taxi taxi2 = new Taxi("Taxi543", BrandTransport.FORD, body, engine, wheelsForTaxi);
		expectedListTaxi.add(taxi1);
		expectedListTaxi.add(taxi2);

		taxiService = new TaxiServiceImpl(repositoryTaxi);
		command = new GetTaxiByTaxiNumberCommand(taxiService);
	}

	@Test
	public void executeTest() {
		Specification<Taxi> specification = new SpecificationByTaxiNumberImpl("Taxi132");
		when(repositoryTaxi.getBy(specification)).thenReturn(Optional.of(expectedListTaxi.get(0)));
		Optional<Taxi> expectedTaxi = taxiService.getBy(specification);

		StringBuilder expectedResultCommand = new StringBuilder();
		expectedResultCommand.append("<h2>Taxi with number = " + expectedTaxi.get().getRegistrationNumber() + "</h2>");
		expectedResultCommand.append(CommandUtility.transportToStringForTemplateHTMLFile(expectedTaxi));

		Map<String, String> mapForCommand = new HashMap<>();
		mapForCommand.put("taxiNumber", "Taxi132");
		String foundResultCommand = command.execute(mapForCommand);

		assertEquals(expectedResultCommand.toString(), foundResultCommand);
	}

	@After
	public void clearListTaxiWithTaxiServiceAndCommand() {
		taxiService = null;
		expectedListTaxi = null;
		repositoryTaxi = null;
		command = null;
	}

}
