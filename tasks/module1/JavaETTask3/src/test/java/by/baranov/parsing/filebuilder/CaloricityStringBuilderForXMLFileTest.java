package by.baranov.parsing.filebuilder;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.parsing.entity.Caloricity;

public class CaloricityStringBuilderForXMLFileTest {
	private Caloricity caloricity;
	private CaloricityStringBuilderForXMLFile builder;
	
	@Before
	public void init() {
		caloricity = new Caloricity();
		builder = new CaloricityStringBuilderForXMLFile(caloricity);
	}

	@Test
	public void testBuildElement() {
		String result = builder.buildElement();
		int stringQuantity = result.split("\n").length;
		assertEquals(1, stringQuantity);
	}

	@After
	public void clean() {
		caloricity = null;
		builder = null;
	}

}
