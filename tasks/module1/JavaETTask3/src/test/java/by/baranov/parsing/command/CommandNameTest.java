package by.baranov.parsing.command;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.util.Optional;

import org.junit.Test;

public class CommandNameTest {

	@Test
	public void testFromString() {
		CommandName rightCommandName = CommandName.DEFAULT;
		Optional<CommandName> foundCommandName = CommandName.fromString("default");
		assertSame(rightCommandName, foundCommandName.get());
	}
	
	@Test
	public void testFromString2() {
		String wrongCommandName = "---";
		Optional<CommandName> foundCommandName = CommandName.fromString(wrongCommandName);
		assertEquals(Optional.empty(), foundCommandName);
	}

}
