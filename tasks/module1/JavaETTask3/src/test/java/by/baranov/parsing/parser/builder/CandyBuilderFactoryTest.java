package by.baranov.parsing.parser.builder;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.parsing.exception.BuilderException;

public class CandyBuilderFactoryTest {
	private CandyBuilderFactory factory;

	@Before
	public void init() {
		factory = new CandyBuilderFactory();
	}

	@Test
	public void testCreateCandyBuilder() throws BuilderException {
		String expectedBuilderName = "CandiesDOMBuilder";
		String foundBuilderName = factory.createCandyBuilder("dom").getClass().getSimpleName();
		assertEquals(expectedBuilderName, foundBuilderName);
	}

	@Test(expected = BuilderException.class)
	public void testCreateCandyBuilder2() throws BuilderException {
		factory.createCandyBuilder("dom2");
	}

	@After
	public void clean() {
		factory = null;
	}
}
