package by.baranov.parsing.validator;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.parsing.exception.FileNotAvailableException;
import by.baranov.parsing.exception.ValidationException;

public class ValidatorXSDForXMLFileTest {
	private ValidatorXSDForXMLFile validator;

	@Before
	public void init() {
		validator = new ValidatorXSDForXMLFile();
	}

	@Test
	public void testValidate() throws ValidationException, FileNotAvailableException {
		File file = new File(getClass().getClassLoader().getResource("candies.xml").getFile());
		String absolutePathToFileXML = file.getAbsolutePath();
		validator.validate(absolutePathToFileXML);
	}
	
	@Test(expected = ValidationException.class)
	public void testValidate2() throws ValidationException, FileNotAvailableException {
		validator.validate("");
	}

	@After
	public void clean() {
		validator = null;
	}
}
