package by.baranov.parsing.command;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.parsing.entity.Candy;
import by.baranov.parsing.repository.Repository;
import by.baranov.parsing.repository.RepositoryCandy;
import by.baranov.parsing.service.CandyService;
import by.baranov.parsing.service.CandyServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class AddCandiesFromFileCommandTest {
	private Repository<Candy> repository;
	private CandyService service;
	private AddCandiesFromFileCommand command;

	@Mock
	private HttpServletRequest request;
	@Mock
	private HttpServletResponse response;
	@Mock
	private HttpSession session;

	@Before
	public void init() {
		repository = new RepositoryCandy();
		service = new CandyServiceImpl(repository);
		command = new AddCandiesFromFileCommand(service);
	}

	@Test
	public void testExecute() {
		File file = new File(getClass().getClassLoader().getResource("candies.xml").getFile());
		String absolutePathToFileXML = file.getAbsolutePath();
		
		when(request.getParameter("filePath")).thenReturn(absolutePathToFileXML);
		when(request.getParameter("parser")).thenReturn("sax");
		when(request.getSession()).thenReturn(session);
		
		command.execute(request, response);

		int foundQuantityCandy = service.getAll().size();
		assertEquals(16, foundQuantityCandy);
	}

	@After
	public void clean() {
		repository = null;
		service = null;
		command = null;
		request = null;
		response = null;
		session = null;
	}
}
