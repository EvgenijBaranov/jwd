package by.baranov.parsing.filebuilder;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.parsing.entity.IngredientsCaramelCandy;

public class IngredientsCaramelCandyStringBuilderForXMLFileTest {
	private IngredientsCaramelCandy ingredients;
	private IngredientsCaramelCandyStringBuilderForXMLFile builder;
	
	@Before
	public void init() {
		ingredients = new IngredientsCaramelCandy();
		builder = new IngredientsCaramelCandyStringBuilderForXMLFile(ingredients);
	}

	@Test
	public void testBuildElement() {
		String result = builder.buildElement();
		int stringQuantity = result.split("\n").length;
		assertEquals(5, stringQuantity);
	}

	@After
	public void clean() {
		ingredients = null;
		builder = null;
	}
}
