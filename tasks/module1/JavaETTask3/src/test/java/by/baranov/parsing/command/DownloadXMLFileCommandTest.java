package by.baranov.parsing.command;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.parsing.entity.Candy;
import by.baranov.parsing.repository.Repository;
import by.baranov.parsing.repository.RepositoryCandy;
import by.baranov.parsing.service.CandyService;
import by.baranov.parsing.service.CandyServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class DownloadXMLFileCommandTest {
	private Repository<Candy> repository;
	private CandyService service;
	private DownloadXMLFileCommand command;

	@Mock
	private HttpServletRequest request;
	@Mock
	private HttpServletResponse response;
	@Mock
	private ServletOutputStream outputStream;

	@Before
	public void init() throws IOException {
		repository = new RepositoryCandy();
		service = new CandyServiceImpl(repository);
		command = new DownloadXMLFileCommand(service);
	}

	@Test
	public void testExecute() throws IOException {
		when(response.getOutputStream()).thenReturn(outputStream);
		doNothing().when(response).setContentType(anyString());
		doNothing().when(response).setHeader(isA(String.class), isA(String.class));
		String commandResult = command.execute(request, response);
		assertEquals(TypeActionForCommand.FORWARD + "/jsp/resultParsing.jsp", commandResult);
	}

	@After
	public void clean() {
		repository = null;
		service = null;
		command = null;
		request = null;
		response = null;
		outputStream = null;
	}
}
