package by.baranov.parsing.filebuilder;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.parsing.entity.IngredientsChocolateCandy;

public class IngredientsChocolateCandyStringBuilderForXMLFileTest {
	private IngredientsChocolateCandy ingredients;
	private IngredientsChocolateCandyStringBuilderForXMLFile builder;
	
	@Before
	public void init() {
		ingredients = new IngredientsChocolateCandy();
		builder = new IngredientsChocolateCandyStringBuilderForXMLFile(ingredients);
	}

	@Test
	public void testBuildElement() {
		String result = builder.buildElement();
		int stringQuantity = result.split("\n").length;
		assertEquals(6, stringQuantity);
	}

	@After
	public void clean() {
		ingredients = null;
		builder = null;
	}
}
