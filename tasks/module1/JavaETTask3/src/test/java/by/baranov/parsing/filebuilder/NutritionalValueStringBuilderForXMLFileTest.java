package by.baranov.parsing.filebuilder;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.parsing.entity.NutritionalValue;

public class NutritionalValueStringBuilderForXMLFileTest {
	private NutritionalValue nutritionlaValue;
	private NutritionalValueStringBuilderForXMLFile builder;
	
	@Before
	public void init() {
		nutritionlaValue = new NutritionalValue();
		builder = new NutritionalValueStringBuilderForXMLFile(nutritionlaValue);
	}

	@Test
	public void testBuildElement() {
		String result = builder.buildElement();
		int stringQuantity = result.split("\n").length;
		assertEquals(5, stringQuantity);
	}

	@After
	public void clean() {
		nutritionlaValue = null;
		builder = null;
	}

}
