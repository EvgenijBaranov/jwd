package by.baranov.parsing.service;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.parsing.entity.Caloricity;
import by.baranov.parsing.entity.Candy;
import by.baranov.parsing.entity.CaramelCandy;
import by.baranov.parsing.entity.IngredientsCaramelCandy;
import by.baranov.parsing.entity.NutritionalValue;
import by.baranov.parsing.entity.UnitCaloricity;
import by.baranov.parsing.entity.UnitMass;
import by.baranov.parsing.repository.Repository;
import by.baranov.parsing.repository.RepositoryCandy;

public class CandyServiceImplTest {
	private Repository<Candy> repository;
	private CandyService candyService;
	private Candy candy;
	
	@Before
	public void init() {
		LocalDate date = LocalDate.of(2020, 03, 11);
		LocalTime time = LocalTime.of(12, 00);
		Caloricity caloricity = new Caloricity(UnitCaloricity.KCAL, 21.12f);
		NutritionalValue nutritional = new NutritionalValue(UnitMass.G, 21.12f, 21.12f, 21.12f);
		IngredientsCaramelCandy ingredients = new IngredientsCaramelCandy(UnitMass.G, 21.12f, 21.12f, 21.12f);
		candy = new CaramelCandy("id111", date, time, "candy",caloricity, nutritional, "Roshen", ingredients);
		repository = new RepositoryCandy();
		candyService = new CandyServiceImpl(repository);
	}

	@Test
	public void testGetAll() {
		List<Candy> expectedCandies = Arrays.asList(candy, candy);
		candyService.addList(expectedCandies);
		List<Candy> foundCandies = candyService.getAll();
		assertEquals(expectedCandies, foundCandies);
	}
	
	@Test
	public void testCreateStringFromRepositoryForXMLFile() {
		List<Candy> candies = Arrays.asList(candy, candy);
		candyService.addList(candies);
		String result = candyService.createStringFromRepositoryForXMLFile();
		int stringQuantity = result.split("\n").length;
		assertEquals(35, stringQuantity);
	}
	

	@After
	public void clean() {
		repository = null;		
		candyService = null;
		candy = null;
	}
}
