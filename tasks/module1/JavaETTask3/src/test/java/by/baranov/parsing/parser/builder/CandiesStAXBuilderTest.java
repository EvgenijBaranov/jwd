package by.baranov.parsing.parser.builder;

import static org.junit.Assert.assertEquals;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.parsing.entity.Candy;
import by.baranov.parsing.exception.FileNotAvailableException;
import by.baranov.parsing.exception.ParserException;

public class CandiesStAXBuilderTest {
	private CandiesStAXBuilder builder;
	
	@Before
	public void init() {
		builder = new CandiesStAXBuilder();
	}

	@Test
	public void testBuildListCandies() throws FileNotAvailableException, ParserException {
		Path pathToFileXMLInResourceDirectory = Paths.get("src", "test", "resources", "candies.xml");
		String absolutePathToFileXML = pathToFileXMLInResourceDirectory.toFile().getAbsolutePath();
		builder.buildListCandies(absolutePathToFileXML);
		List<Candy> candies = builder.getCandies();
		assertEquals(16, candies.size());
	}
	
	@Test(expected = FileNotAvailableException.class)
	public void testBuildListCandies2() throws FileNotAvailableException, ParserException {
		builder.buildListCandies("");
	}
	
	@After
	public void clean() {
		builder = null;
	}
}
