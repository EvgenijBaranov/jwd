package by.baranov.parsing.validator;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.parsing.exception.ValidationException;

public class ValidatorPathFileTest {
	private ValidatorPathFile validator;

	@Before
	public void init() {
		validator = new ValidatorPathFile();
	}

	@Test
	public void testValidate() throws ValidationException {
		File file = new File(getClass().getClassLoader().getResource("candies.xml").getFile());
		String absolutePathToFileXML = file.getAbsolutePath();
		validator.validate(absolutePathToFileXML);
	}
	
	@Test(expected = ValidationException.class)
	public void testValidate2() throws ValidationException {
		validator.validate("d;asdasd/adasda:sad");
	}

	@After
	public void clean() {
		validator = null;
	}
}
