package by.baranov.parsing.command;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.parsing.service.CandyService;

public class CommandRegistryImplTest {
	private CommandRegistryImpl registration;
	private CandyService service;

	@Before
	public void init() {
		registration = new CommandRegistryImpl();
	}

	@Test
	public void testGetCommand() {
		registration.register(CommandName.PARSE_XML_FILE_BY_PARSER, new AddCandiesFromFileCommand(service));
		String expectedNameCommand = "AddCandiesFromFileCommand";
		String foundNameCommmand = registration.getCommand(CommandName.PARSE_XML_FILE_BY_PARSER).getClass()
				.getSimpleName();
		assertEquals(expectedNameCommand, foundNameCommmand);
	}

	@Test
	public void testGetCommand2() {
		String expectedNameCommand = "DefaultCommand";
		String foundNameCommmand = registration.getCommand(CommandName.DEFAULT).getClass().getSimpleName();
		assertEquals(expectedNameCommand, foundNameCommmand);
	}

	@After
	public void clean() {
		registration = null;
	}
}
