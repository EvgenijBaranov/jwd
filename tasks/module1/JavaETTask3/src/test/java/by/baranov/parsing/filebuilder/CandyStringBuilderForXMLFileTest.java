package by.baranov.parsing.filebuilder;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import by.baranov.parsing.entity.Candy;
import by.baranov.parsing.entity.CaramelCandy;
import by.baranov.parsing.entity.NutritionalValue;

public class CandyStringBuilderForXMLFileTest {
	private CandyStringBuilderForXMLFile candyBuilder;
	private Candy candy;
	
	@Before
	public void init() {
		candy = new CaramelCandy();
		candyBuilder = new CandyStringBuilderForXMLFile(candy);
	}

	@Test
	public void testBuildElement() {
		String result = candyBuilder.buildElement();
		int stringQuantity = result.split("\n").length;
		assertEquals(3, stringQuantity);
	}

	@Test
	public void testAddNode() {
		NutritionalValue nutritionlaValue = new NutritionalValue();
		NutritionalValueStringBuilderForXMLFile nutritionalBuilder = new NutritionalValueStringBuilderForXMLFile(nutritionlaValue);
		candyBuilder.addNode(nutritionalBuilder);
		String result = candyBuilder.buildElement();
		int stringQuantity = result.split("\n").length;
		assertEquals(8, stringQuantity);
	}

	@After
	public void clean() {
		candy = null;
		candyBuilder = null;
	}
}
