package by.baranov.parsing.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import by.baranov.parsing.service.CandyServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ControllerTest {
	private Controller controller;
	private ApplicationContext context;
	@Mock
	private HttpServletRequest request;
	@Mock
	private HttpServletResponse response;
	@Mock
	private HttpSession session;

	@Before
	public void init() {
		controller = new Controller();
		context = ApplicationContext.getInstance();
		context.initialize();
	}

	@Test
	public void testDoPost() throws ServletException, IOException {
		File file = new File(getClass().getClassLoader().getResource("candies.xml").getFile());
		String absolutePathToFileXML = file.getAbsolutePath();

		when(request.getParameter("commandName")).thenReturn("PARSE_XML_FILE_BY_PARSER");
		when(request.getParameter("filePath")).thenReturn(absolutePathToFileXML);
		when(request.getParameter("parser")).thenReturn("sax");
		when(request.getSession()).thenReturn(session);

		controller.doPost(request, response);

		int foundQuantityCandy = context.getBean(CandyServiceImpl.class).getAll().size();
		assertEquals(16, foundQuantityCandy);
	}

	@After
	public void clean() {
		controller = null;
		context = null;
		request = null;
		response = null;
		session = null;
	}
}
