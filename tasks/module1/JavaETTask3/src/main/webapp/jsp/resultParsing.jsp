<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>Result parsing xml file</title>
<style>
table, th, td {
	border: 2px solid green;
	text-align: center;
}

table {
	width: 100%;
	border-collapse: collapse;
}

caption {
	text-align: left;
}
</style>
</head>
<body>

    <h3 align="center"> Result parsing xml file using <c:out value="${ parserName }" />  parser is presented in tables.</h3>
    
	<table>
		<caption>
			<b>Table №1 - Chocolate candies</b>
		</caption>
		<tr>
			<th rowspan="3">ID</th>
			<th rowspan="3">Name</th>
			<th rowspan="3">Producer</th>
			<th rowspan="3">Date of manufacture</th>
			<th rowspan="3">Time of manufacture</th>
			<th rowspan="3">Caloricity, <br /> kcal</th>
			<th colspan="3">Nutritional value, g</th>
			<th colspan="6">Ingredients, mg</th>

		</tr>
		<tr>
			<th rowspan="2">protein</th>
			<th rowspan="2">fat</th>
			<th rowspan="2">carbohydrate</th>
			<th rowspan="2">sugar</th>
			<th colspan="2">chocolate</th>
			<th rowspan="2">vanillin</th>
			<th colspan="2">filling</th>
		</tr>
		<tr>
			<th>type</th>
			<th>value</th>
			<th>type</th>
			<th>value</th>
		</tr>
		<c:forEach var="candy" items="${ candies }">
			<c:if
				test="${ candy['class'].name eq 'by.baranov.parsing.entity.ChocolateCandy' }">
				<tr>
					<td><c:out value="${ candy.id }" /></td>
					<td><c:out value="${ candy.name }" /></td>
					<td><c:out value="${ candy.production }" /></td>
					<td><c:out value="${ candy.dateOfManufacture }" /></td>
					<td><c:out value="${ candy.timeOfManufacture }" /></td>
					<td><c:out value="${ candy.caloricity.caloricityValue }" /></td>
					<td><c:out value="${ candy.nutritionalValue.proteinValue }" /></td>
					<td><c:out value="${ candy.nutritionalValue.fatValue }" /></td>
					<td><c:out value="${ candy.nutritionalValue.carbohydrateValue }" /></td>
					<td><c:out value="${ candy.ingredients.sugarValue }" /></td>
					<td><c:out value="${ candy.ingredients.typeChocolate.name }" /></td>
					<td><c:out value="${ candy.ingredients.chocolateValue }" /></td>
					<td><c:out value="${ candy.ingredients.vanilineValue }" /></td>
					<td><c:out value="${ candy.ingredients.typeFilling.name }" /></td>
					<td><c:out value="${ candy.ingredients.fillingValue }" /></td>
				</tr>
			</c:if>
		</c:forEach>
	</table>
	
	<br/><br/>
	
	<table>
		<caption>
			<b>Table №2 - Caramel candies</b>
		</caption>
		<tr>
			<th rowspan="2">ID</th>
			<th rowspan="2">Name</th>
			<th rowspan="2">Producer</th>
			<th rowspan="2">Date of manufacture</th>
			<th rowspan="2">Time of manufacture</th>
			<th rowspan="2">Caloricity, <br/> kcal </th>
			<th colspan="3">Nutritional value, g</th>
			<th colspan="3">Ingredients, mg</th>
		</tr>
		<tr>
			<th>protein</th>
			<th>fat</th>
			<th>carbohydrate</th>
			<th>sugar</th>
			<th>water</th>
			<th>vanillin</th>
		</tr>
		<c:forEach var="candy" items="${ candies }">
			<c:if
				test="${ candy['class'].name eq 'by.baranov.parsing.entity.CaramelCandy' }">
				<tr>
					<td><c:out value="${ candy.id }" /></td>
					<td><c:out value="${ candy.name }" /></td>
					<td><c:out value="${ candy.production }" /></td>
					<td><c:out value="${ candy.dateOfManufacture }" /></td>
					<td><c:out value="${ candy.timeOfManufacture }" /></td>
					<td><c:out value="${ candy.caloricity.caloricityValue }" /></td>
					<td><c:out value="${ candy.nutritionalValue.proteinValue }" /></td>
					<td><c:out value="${ candy.nutritionalValue.fatValue }" /></td>
					<td><c:out value="${ candy.nutritionalValue.carbohydrateValue }" /></td>
					<td><c:out value="${ candy.ingredients.sugarValue }" /></td>
					<td><c:out value="${ candy.ingredients.waterValue }" /></td>
					<td><c:out value="${ candy.ingredients.vanilineValue }" /></td>
				</tr>
			</c:if>
		</c:forEach>
	</table>
	<form action="${pageContext.request.contextPath}/controller" method="POST" accept-charset="utf-8">
        <p>Do you want to download the result as an xml file?  
	        <input type="hidden" name="commandName" value="DOWNLOAD_XML_FILE"> 
	        <input type="submit" value="Download" />
        </p>
    </form>
	
	<p>
	   <a href="${pageContext.request.contextPath}/index.jsp">Return to choose other file or parser</a>
	</p>
</body>
</html>