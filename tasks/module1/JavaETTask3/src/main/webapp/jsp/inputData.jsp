<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>Parsing XML file</title>
</head>
<body>
	<h1>Hello User</h1>
	<p>
	   Here you can parse xml file using next parsers : SAX, StAX or DOM parser
	</p>
	
	<form action="controller" method="POST" id="formChooseParser" accept-charset="utf-8">
		<label for="filePath">Enter the file path for parsing: </label> 
		<input id="filePath" type="text" name="filePath" /> 
		<br /> <br /> 
		<label for="parsers">Choose parser for parsing file:</label> 
		<select id="parsers" name="parser" form="formChooseParser">
			<option disabled>Choose parser for xml file</option>
			<option value="sax">SAX parser</option>
			<option value="stax">StAX parser</option>
			<option value="dom">DOM parser</option>
		</select> 
		<input type="hidden" name="commandName" value="PARSE_XML_FILE_BY_PARSER"> 
		<input type="submit" value="Parse file" />
	</form>
	
</body>
</html>