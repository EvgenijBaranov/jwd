<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>Error page</title>
</head>
<body>
	<p>
		Unfortunately there are next problem in the application: ${ errorMessage }
	</p>
	<a href="${pageContext.request.contextPath}/index.jsp">Choose other file or parser</a>
</body>
</html>