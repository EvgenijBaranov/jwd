package by.baranov.parsing.command;

public enum TypeActionForCommand {
	FORWARD, REDIRECT
}
