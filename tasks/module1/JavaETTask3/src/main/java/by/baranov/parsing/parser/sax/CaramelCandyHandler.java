package by.baranov.parsing.parser.sax;

import java.util.EnumSet;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import by.baranov.parsing.entity.CaramelCandy;
import by.baranov.parsing.entity.UnitCaloricity;
import by.baranov.parsing.entity.UnitMass;
import by.baranov.parsing.parser.builder.CaramelCandyTagsEnum;
import by.baranov.parsing.parser.builder.Utils;

public class CaramelCandyHandler extends DefaultHandler {
	private static final Logger LOGGER = LogManager.getLogger();
	private CaramelCandy currentCandy = null;
	private CaramelCandyTagsEnum currentEnum = null;
	private EnumSet<CaramelCandyTagsEnum> tagsWithText;
	private XMLReader reader;
	private ContentHandler handler;
	private NutritionalValueCandyHandler nutritionalValueHandler = new NutritionalValueCandyHandler();
	private IngredientsCaramelCandyHandler ingredientsCaramelCandyHandler = new IngredientsCaramelCandyHandler();

	public CaramelCandyHandler() {
		tagsWithText = EnumSet.range(CaramelCandyTagsEnum.NAME, CaramelCandyTagsEnum.VANILINE);
	}

	public void collectCaramelCandy(XMLReader reader, ContentHandler handler, CaramelCandy candy) {
		this.reader = reader;
		this.handler = handler;
		currentCandy = candy;
		reader.setContentHandler(this);
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) {

		if (CaramelCandyTagsEnum.CALORICITY.getTagName().equalsIgnoreCase(localName) && Utils.isExistAttributeForSAXParser(attributes)) {
			String attributeCaloricityName = CaramelCandyTagsEnum.UNIT_CALORICITY.getTagName();
			UnitCaloricity notDefaultUnitValue = UnitCaloricity.valueOf(attributes.getValue(attributeCaloricityName).toUpperCase());
			currentCandy.getCaloricity().setUnit(notDefaultUnitValue);
			LOGGER.log(Level.DEBUG, "CaramelCandy Caloricity unit is {}", currentCandy.getCaloricity().getUnit().name());
		}

		if (CaramelCandyTagsEnum.NUTRITIONAL_VALUE.getTagName().equalsIgnoreCase(localName)) {
			if(Utils.isExistAttributeForSAXParser(attributes)) {
				String attributeNutritionalValueName = CaramelCandyTagsEnum.UNIT_NUTRITIONAL.getTagName();
				UnitMass notDefaultUnitValue = UnitMass.valueOf(attributes.getValue(attributeNutritionalValueName).toUpperCase());
				currentCandy.getNutritionalValue().setUnit(notDefaultUnitValue);
				LOGGER.log(Level.DEBUG, "CaramelCandy NutritionalValue unit is {}", currentCandy.getNutritionalValue().getUnit().name());
			}
			nutritionalValueHandler.collectNutritionalValue(reader, this, currentCandy.getNutritionalValue());
		}

		if (CaramelCandyTagsEnum.INGREDIENTS.getTagName().equalsIgnoreCase(localName)) {
			if(Utils.isExistAttributeForSAXParser(attributes)) {
				String attributeIngridientsName = CaramelCandyTagsEnum.UNIT_INGREDIENT.getTagName();
				UnitMass notDefaultUnitValue = UnitMass.valueOf(attributes.getValue(attributeIngridientsName).toUpperCase());
				currentCandy.getIngredients().setUnit(notDefaultUnitValue);
				LOGGER.log(Level.DEBUG, "CaramelCandy Ingridients unit is {}", currentCandy.getIngredients().getUnit().name());
			}
			ingredientsCaramelCandyHandler.collectIngredientsCaramelCandy(reader, this, currentCandy.getIngredients());
		}

		CaramelCandyTagsEnum temp = CaramelCandyTagsEnum.valueOf(localName.toUpperCase().replace("-", "_"));
		if (tagsWithText.contains(temp)) {
			currentEnum = temp;
		}
	}
	
	@Override
	public void characters(char[] characterData, int startPositionCharacterData, int lengthCharacterData) {
		String resultReading = new String(characterData, startPositionCharacterData, lengthCharacterData);
		if (currentEnum != null) {
			switch (currentEnum) {
				case NAME:
					currentCandy.setName(resultReading);
					LOGGER.log(Level.DEBUG, "CaramelCandy name is {}", currentCandy.getName());
					break;
				case CALORICITY :
					float caloricityValue = Float.parseFloat(resultReading);
					currentCandy.getCaloricity().setCaloricityValue(caloricityValue);
					LOGGER.log(Level.DEBUG, "CaramelCandy caloricityValue is {}", currentCandy.getCaloricity().getCaloricityValue());
					break;
				case PRODUCTION :
					currentCandy.setProduction(resultReading);
					LOGGER.log(Level.DEBUG, "CaramelCandy production is {}", currentCandy.getProduction());
					break;
				default:
					throw new EnumConstantNotPresentException(currentEnum.getDeclaringClass(), currentEnum.name());
			}
			currentEnum = null;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		if (CaramelCandyTagsEnum.CARAMEL_CANDY.getTagName().equalsIgnoreCase(localName)) {
			reader.setContentHandler(handler);
		}
	}

}
