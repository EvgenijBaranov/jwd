package by.baranov.parsing.entity;

import java.util.Objects;

public class NutritionalValue {
	private UnitMass unit = UnitMass.G;
	private float proteinValue;
	private float fatValue;
	private float carbohydrateValue;

	public NutritionalValue() {}
	
	public NutritionalValue(UnitMass unit, float proteinValue, float fatValue, float carbohydrateValue) {
		this.unit = unit;
		this.proteinValue = proteinValue;
		this.fatValue = fatValue;
		this.carbohydrateValue = carbohydrateValue;
	}

	public UnitMass getUnit() {
		return unit;
	}

	public void setUnit(UnitMass unit) {
		this.unit = unit;
	}

	public float getProteinValue() {
		return proteinValue;
	}

	public void setProteinValue(float proteinValue) {
		this.proteinValue = proteinValue;
	}

	public float getFatValue() {
		return fatValue;
	}

	public void setFatValue(float fatValue) {
		this.fatValue = fatValue;
	}

	public float getCarbohydrateValue() {
		return carbohydrateValue;
	}

	public void setCarbohydrateValue(float carbohydrateValue) {
		this.carbohydrateValue = carbohydrateValue;
	}

	@Override
	public int hashCode() {
		return Objects.hash(carbohydrateValue, fatValue, proteinValue, unit);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof NutritionalValue))
			return false;
		NutritionalValue other = (NutritionalValue) obj;
		return Float.floatToIntBits(carbohydrateValue) == Float.floatToIntBits(other.carbohydrateValue)
				&& Float.floatToIntBits(fatValue) == Float.floatToIntBits(other.fatValue)
				&& Float.floatToIntBits(proteinValue) == Float.floatToIntBits(other.proteinValue) && unit == other.unit;
	}

	@Override
	public String toString() {
		return "NutritionalValue [unit=" + unit.name().toLowerCase() + ": protein=" + proteinValue + ", fat=" + fatValue
				+ ", carbohydrate=" + carbohydrateValue + "]";
	}
	
}
