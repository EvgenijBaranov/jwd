package by.baranov.parsing.parser.sax;

import java.util.EnumSet;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import by.baranov.parsing.entity.IngredientsCaramelCandy;
import by.baranov.parsing.parser.builder.CaramelCandyTagsEnum;

public class IngredientsCaramelCandyHandler extends DefaultHandler {
	private static final Logger LOGGER = LogManager.getLogger();
	private IngredientsCaramelCandy currentIngredientsCaramelCandy = null;
	private CaramelCandyTagsEnum currentEnum = null;
	private EnumSet<CaramelCandyTagsEnum> tagsIngredientsWithText;
	private XMLReader reader;
	private ContentHandler handler;

	public IngredientsCaramelCandyHandler() {
		tagsIngredientsWithText = EnumSet.range(CaramelCandyTagsEnum.WATER, CaramelCandyTagsEnum.VANILINE);
	}

	public void collectIngredientsCaramelCandy(XMLReader reader, ContentHandler handler, IngredientsCaramelCandy ingredientsCaramelCandy) {
		this.reader = reader;
		this.handler = handler;
		currentIngredientsCaramelCandy = ingredientsCaramelCandy;
		reader.setContentHandler(this);
	}

	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) {

		CaramelCandyTagsEnum temp = CaramelCandyTagsEnum.valueOf(localName.toUpperCase().replace("-", "_"));
		if (tagsIngredientsWithText.contains(temp)) {
			currentEnum = temp;
		}
	}
	
	@Override
	public void characters(char[] characterData, int startPositionCharacterData, int lengthCharacterData) {
		String resultReading = new String(characterData, startPositionCharacterData, lengthCharacterData);
		if (currentEnum != null) {
			switch (currentEnum) {
				case WATER :
					float waterValue = Float.parseFloat(resultReading);
					currentIngredientsCaramelCandy.setWaterValue(waterValue);
					LOGGER.log(Level.DEBUG, "IngredientsCaramelCandy waterValue is {}", currentIngredientsCaramelCandy.getWaterValue());
					break;
				case SUGAR :
					float sugarValue = Float.parseFloat(resultReading);
					currentIngredientsCaramelCandy.setSugarValue(sugarValue);
					LOGGER.log(Level.DEBUG, "IngredientsCaramelCandy sugarValue is {}", currentIngredientsCaramelCandy.getSugarValue());
					break;
				case VANILINE :
					float vanilineValue = Float.parseFloat(resultReading);
					currentIngredientsCaramelCandy.setVanilineValue(vanilineValue);
					LOGGER.log(Level.DEBUG, "IngredientsCaramelCandy vanilineValue is {}", currentIngredientsCaramelCandy.getVanilineValue());
					break;
				default:
					throw new EnumConstantNotPresentException(currentEnum.getDeclaringClass(), currentEnum.name());
			}
			currentEnum = null;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		if (CaramelCandyTagsEnum.INGREDIENTS.getTagName().equalsIgnoreCase(localName)) {
			reader.setContentHandler(handler);
		}
	}
}
