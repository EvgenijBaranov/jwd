package by.baranov.parsing.service;

import java.util.List;

import by.baranov.parsing.entity.Candy;
import by.baranov.parsing.entity.CaramelCandy;
import by.baranov.parsing.entity.ChocolateCandy;
import by.baranov.parsing.filebuilder.CaloricityStringBuilderForXMLFile;
import by.baranov.parsing.filebuilder.CandyStringBuilderForXMLFile;
import by.baranov.parsing.filebuilder.IngredientsCaramelCandyStringBuilderForXMLFile;
import by.baranov.parsing.filebuilder.IngredientsChocolateCandyStringBuilderForXMLFile;
import by.baranov.parsing.filebuilder.NutritionalValueStringBuilderForXMLFile;
import by.baranov.parsing.repository.Repository;

public class CandyServiceImpl implements CandyService {
	private final Repository<Candy> repositoryCandy;

	public CandyServiceImpl(Repository<Candy> repositoryCandy) {
		this.repositoryCandy = repositoryCandy;
	}

	@Override
	public List<Candy> getAll() {
		return repositoryCandy.getAll();
	}

	@Override
	public void addList(List<Candy> listElements) {
		repositoryCandy.addList(listElements);
	}

	@Override
	public String createStringFromRepositoryForXMLFile() {
		List<Candy> candies = repositoryCandy.getAll();
		StringBuilder result = new StringBuilder();
		result.append("<candies>\n");

		for (Candy candy : candies) {
			CandyStringBuilderForXMLFile candyBuilder = new CandyStringBuilderForXMLFile(candy);
			candyBuilder.addNode(new CaloricityStringBuilderForXMLFile(candy.getCaloricity()));
			candyBuilder.addNode(new NutritionalValueStringBuilderForXMLFile(candy.getNutritionalValue()));

			if (candy instanceof ChocolateCandy) {
				candyBuilder.addNode(new IngredientsChocolateCandyStringBuilderForXMLFile(((ChocolateCandy)candy).getIngredients()));
				result.append("\n<chocolate-candy id=\"");
				result.append(candyBuilder.buildElement());
				result.append("</chocolate-candy>\n");
			} else if (candy instanceof CaramelCandy) {
				candyBuilder.addNode(new IngredientsCaramelCandyStringBuilderForXMLFile(((CaramelCandy)candy).getIngredients()));
				result.append("\n<caramel-candy id=\"");
				result.append(candyBuilder.buildElement());
				result.append("</caramel-candy>\n");
			}
		}
		result.append("\n</candies>");
		return result.toString();
	}

}
