package by.baranov.parsing.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.parsing.command.AddCandiesFromFileCommand;
import by.baranov.parsing.command.Command;
import by.baranov.parsing.command.CommandName;
import by.baranov.parsing.command.CommandRegistry;
import by.baranov.parsing.command.CommandRegistryImpl;
import by.baranov.parsing.command.DefaultCommand;
import by.baranov.parsing.command.DownloadXMLFileCommand;
import by.baranov.parsing.entity.Candy;
import by.baranov.parsing.repository.Repository;
import by.baranov.parsing.repository.RepositoryCandy;
import by.baranov.parsing.service.CandyService;
import by.baranov.parsing.service.CandyServiceImpl;

public class ApplicationContext {
	private static final ApplicationContext INSTANCE = new ApplicationContext();
	private static final Logger LOGGER = LogManager.getLogger();
	private final Map<Class<?>, Object> beens = new HashMap<>();
	
	public static ApplicationContext getInstance() {
		return INSTANCE;
	}
	
	public void initialize() {
		Repository<Candy> candyRepository = new RepositoryCandy();
		CandyService candyService = new CandyServiceImpl(candyRepository);	
		Command addCandiesFromFileCommand = new AddCandiesFromFileCommand(candyService);
		Command defaultCommand = new DefaultCommand();
		Command downloadXMLFile = new DownloadXMLFileCommand(candyService);
		CommandRegistry registrationCommands = new  CommandRegistryImpl();
		registrationCommands.register(CommandName.PARSE_XML_FILE_BY_PARSER, addCandiesFromFileCommand);
		registrationCommands.register(CommandName.DEFAULT, defaultCommand);
		registrationCommands.register(CommandName.DOWNLOAD_XML_FILE, downloadXMLFile);
		
		beens.put(candyRepository.getClass(), candyRepository);
		beens.put(candyService.getClass(), candyService);
		beens.put(registrationCommands.getClass(), registrationCommands);
		LOGGER.log(Level.INFO, "ApplicationContext was initialized");
	}
	
	public void destroy() {
		beens.clear();
		LOGGER.log(Level.INFO, "ApplicationContext was destroyed");
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getBean(Class<T> clazz) {
		return (T) beens.get(clazz);
	}

}
