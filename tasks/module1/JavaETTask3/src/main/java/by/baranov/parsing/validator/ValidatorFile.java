package by.baranov.parsing.validator;

import by.baranov.parsing.exception.FileNotAvailableException;
import by.baranov.parsing.exception.ValidationException;

public interface ValidatorFile {

	void validate(String filePath) throws ValidationException, FileNotAvailableException;

}
