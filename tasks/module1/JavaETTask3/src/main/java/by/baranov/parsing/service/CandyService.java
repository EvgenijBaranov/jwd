package by.baranov.parsing.service;

import by.baranov.parsing.entity.Candy;

public interface CandyService extends CRUDOperation<Candy>{
	
	String createStringFromRepositoryForXMLFile();
	
}
