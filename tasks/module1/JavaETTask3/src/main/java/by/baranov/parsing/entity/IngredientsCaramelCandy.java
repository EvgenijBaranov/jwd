package by.baranov.parsing.entity;

import java.util.Objects;

public class IngredientsCaramelCandy {
	private UnitMass unit = UnitMass.MG;
	private float sugarValue;
	private float waterValue;
	private float vanilineValue;

	public IngredientsCaramelCandy() {}
	
	public IngredientsCaramelCandy(UnitMass unit, float sugarValue, float waterValue, float vanilineValue) {
		this.unit = unit;
		this.sugarValue = sugarValue;
		this.waterValue = waterValue;
		this.vanilineValue = vanilineValue;
	}

	public UnitMass getUnit() {
		return unit;
	}

	public void setUnit(UnitMass unit) {
		this.unit = unit;
	}

	public float getSugarValue() {
		return sugarValue;
	}

	public void setSugarValue(float sugarValue) {
		this.sugarValue = sugarValue;
	}

	public float getWaterValue() {
		return waterValue;
	}

	public void setWaterValue(float waterValue) {
		this.waterValue = waterValue;
	}

	public float getVanilineValue() {
		return vanilineValue;
	}

	public void setVanilineValue(float vanilineValue) {
		this.vanilineValue = vanilineValue;
	}

	@Override
	public int hashCode() {
		return Objects.hash(sugarValue, unit, vanilineValue, waterValue);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof IngredientsCaramelCandy))
			return false;
		IngredientsCaramelCandy other = (IngredientsCaramelCandy) obj;
		return Float.floatToIntBits(sugarValue) == Float.floatToIntBits(other.sugarValue) && unit == other.unit
				&& Float.floatToIntBits(vanilineValue) == Float.floatToIntBits(other.vanilineValue)
				&& Float.floatToIntBits(waterValue) == Float.floatToIntBits(other.waterValue);
	}

	@Override
	public String toString() {
		return "IngredientsCaramelCandy[unit=" + unit.name().toLowerCase() + ", sugar=" + sugarValue + ", water=" + waterValue
				+ ", vaniline=" + vanilineValue + "]";
	}

}
