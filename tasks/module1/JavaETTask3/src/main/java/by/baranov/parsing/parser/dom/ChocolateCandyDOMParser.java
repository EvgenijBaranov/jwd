package by.baranov.parsing.parser.dom;

import java.time.LocalDate;
import java.time.LocalTime;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;

import by.baranov.parsing.entity.ChocolateCandy;
import by.baranov.parsing.parser.builder.ChocolateCandyTagsEnum;
import by.baranov.parsing.parser.builder.Utils;

public class ChocolateCandyDOMParser {
	private static final Logger LOGGER = LogManager.getLogger();
	private ChocolateCandy chocolateCandy = new ChocolateCandy();

	public ChocolateCandy parseChocolateCandy(Element candyElement) {
		
		setAttributes(candyElement);
		chocolateCandy.setName(Utils.getElementTextContent(candyElement, ChocolateCandyTagsEnum.NAME.getTagName()));
		LOGGER.log(Level.DEBUG, "ChocolateCandy name is {}", chocolateCandy.getName());
		chocolateCandy.setCaloricity(new CaloricityDOMParser().parseCaloricity(candyElement));
		chocolateCandy.setNutritionalValue(new NutritionalValueDOMParser().parseNutritionalValue(candyElement));
		chocolateCandy.setProduction(Utils.getElementTextContent(candyElement, ChocolateCandyTagsEnum.PRODUCTION.getTagName()));
		LOGGER.log(Level.DEBUG, "ChocolateCandy production is {}", chocolateCandy.getProduction());
		chocolateCandy.setIngredients(new IngredientsChocolateCandyDOMParser().parseIngredientsChocolateCandy(candyElement));
		
		return chocolateCandy;
	}
	
	private void setAttributes(Element candyElement) {
		chocolateCandy.setId(candyElement.getAttribute(ChocolateCandyTagsEnum.ID.getTagName()));
		LOGGER.log(Level.DEBUG, "ChocolateCandy ID is {}", chocolateCandy.getId());
		String dateFromFile = candyElement.getAttribute(ChocolateCandyTagsEnum.DATE_OF_MANUFACTURE.getTagName());
		chocolateCandy.setDateOfManufacture(LocalDate.parse(dateFromFile));
		LOGGER.log(Level.DEBUG, "ChocolateCandy dateOfManufacture is {}", chocolateCandy.getDateOfManufacture());
		String timeFromFile = candyElement.getAttribute(ChocolateCandyTagsEnum.TIME_OF_MANUFACTURE.getTagName());
		chocolateCandy.setTimeOfManufacture(LocalTime.parse(timeFromFile));
		LOGGER.log(Level.DEBUG, "ChocolateCandy timeOfManufacture is {}", chocolateCandy.getTimeOfManufacture());
	}
	
}
