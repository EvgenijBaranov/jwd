package by.baranov.parsing.filebuilder;

import by.baranov.parsing.entity.IngredientsChocolateCandy;

public class IngredientsChocolateCandyStringBuilderForXMLFile implements StringBuilderForXMLFile{
	private final IngredientsChocolateCandy ingredients;
	
	public IngredientsChocolateCandyStringBuilderForXMLFile(IngredientsChocolateCandy ingredients) {
		this.ingredients = ingredients;
	}

	@Override
	public String buildElement() {
		StringBuilder builder = new StringBuilder();
		
		builder.append("<ingredients unit-ingredient=\"");
		builder.append(ingredients.getUnit().name().toLowerCase());
		builder.append("\">\n");
		builder.append("<sugar>");
		builder.append(ingredients.getSugarValue());
		builder.append("</sugar>\n");
		builder.append("<chocolate type-chocolate=\"");
		builder.append(ingredients.getTypeChocolate().name().toLowerCase());
		builder.append("\">");
		builder.append(ingredients.getChocolateValue());
		builder.append("</chocolate>\n");
		builder.append("<vaniline>");
		builder.append(ingredients.getVanilineValue());
		builder.append("</vaniline>\n");
		builder.append("<filling type-filling=\"");
		builder.append(ingredients.getTypeFilling().name().toLowerCase());
		builder.append("\">");
		builder.append(ingredients.getFillingValue());
		builder.append("</filling>\n");
		builder.append("</ingredients>\n");
		
		return builder.toString();
	}
	
}
