package by.baranov.parsing.parser.builder;

import by.baranov.parsing.exception.BuilderException;

public class CandyBuilderFactory {

	public Builder createCandyBuilder(String typeCandyBuilder) throws BuilderException {

		switch (typeCandyBuilder) {
		case "dom":
			return new CandiesDOMBuilder();
		case "sax":
			return new CandiesSAXBuilder();
		case "stax":
			return new CandiesStAXBuilder();
		default:
			throw new BuilderException("Unfortunately CandyBuilderFactory hasn't " + typeCandyBuilder + " builder");
		}
	}
}
