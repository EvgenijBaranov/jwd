package by.baranov.parsing.parser.dom;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;

import by.baranov.parsing.entity.NutritionalValue;
import by.baranov.parsing.entity.UnitMass;
import by.baranov.parsing.parser.builder.ChocolateCandyTagsEnum;
import by.baranov.parsing.parser.builder.Utils;

public class NutritionalValueDOMParser {
	private static final Logger LOGGER = LogManager.getLogger();
	private NutritionalValue nutritionalValue = new NutritionalValue();
	
	public NutritionalValue parseNutritionalValue(Element candyElement) {
		Element nutritionalValueElement = (Element) candyElement.getElementsByTagName(ChocolateCandyTagsEnum.NUTRITIONAL_VALUE.getTagName()).item(0);
		if(nutritionalValueElement.hasAttribute(ChocolateCandyTagsEnum.UNIT_NUTRITIONAL.getTagName())) {
			String nutritionalValueAttribute = nutritionalValueElement.getAttribute(ChocolateCandyTagsEnum.UNIT_NUTRITIONAL.getTagName());
			UnitMass unit = UnitMass.valueOf(nutritionalValueAttribute.toUpperCase().replace("-", "_"));
			nutritionalValue.setUnit(unit);
			LOGGER.log(Level.DEBUG, "NutritionalValue unit is {}", nutritionalValue.getUnit().name());
		}
		String proteinValue = Utils.getElementTextContent(nutritionalValueElement, ChocolateCandyTagsEnum.PROTEIN.getTagName());
		nutritionalValue.setProteinValue(Float.parseFloat(proteinValue));
		LOGGER.log(Level.DEBUG, "NutritionalValue proteinValue is {}", nutritionalValue.getProteinValue());
		String fatValue = Utils.getElementTextContent(nutritionalValueElement, ChocolateCandyTagsEnum.FAT.getTagName());
		nutritionalValue.setFatValue(Float.parseFloat(fatValue));
		LOGGER.log(Level.DEBUG, "NutritionalValue fatValue is {}", nutritionalValue.getFatValue());
		String carbohydrateValue = Utils.getElementTextContent(nutritionalValueElement, ChocolateCandyTagsEnum.CARBOHYDRATE.getTagName());
		nutritionalValue.setCarbohydrateValue(Float.parseFloat(carbohydrateValue));
		LOGGER.log(Level.DEBUG, "NutritionalValue carbohydrateValue is {}", nutritionalValue.getCarbohydrateValue());
		return nutritionalValue;
	}
	
}
