package by.baranov.parsing.controller;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.parsing.command.Command;
import by.baranov.parsing.command.CommandName;
import by.baranov.parsing.command.CommandRegistry;
import by.baranov.parsing.command.CommandRegistryImpl;
import by.baranov.parsing.command.TypeActionForCommand;

@WebServlet(urlPatterns = "/controller")
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String PARAMETER_NAME_COMMAND_NAME = "commandName";
	private static final Logger LOGGER = LogManager.getLogger();

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	    
		String stringCommand = request.getParameter(PARAMETER_NAME_COMMAND_NAME);
		LOGGER.log(Level.DEBUG, "String Command from request : {}", stringCommand);

		Optional<CommandName> optionalCommandName = CommandName.fromString(stringCommand);
		CommandName commandName = optionalCommandName.orElse(CommandName.DEFAULT);
		LOGGER.log(Level.DEBUG, "CommandName from request : {}", commandName.name());

		CommandRegistry registartion = ApplicationContext.getInstance().getBean(CommandRegistryImpl.class);

		Command command = registartion.getCommand(commandName);
		String commandResult = command.execute(request, response);
		LOGGER.log(Level.DEBUG, "commandResult from command {} : {}", command.getClass().getSimpleName(), commandResult);
		if (commandResult.startsWith(TypeActionForCommand.REDIRECT.name())) {
			String pathToPageForResponse = commandResult.replaceFirst(TypeActionForCommand.REDIRECT.name(), "");
			LOGGER.log(Level.DEBUG, "pathToPageForResponse : {}", pathToPageForResponse);
			response.sendRedirect(request.getContextPath() + pathToPageForResponse);
		} else if (commandResult.startsWith(TypeActionForCommand.FORWARD.name())) {
			String pathToPageForResponse = commandResult.replaceFirst(TypeActionForCommand.FORWARD.name(), "");
			LOGGER.log(Level.DEBUG, "pathToPageForResponse : {}", pathToPageForResponse);
			getServletContext().getRequestDispatcher(pathToPageForResponse).forward(request, response);
		}
	}
}
