package by.baranov.parsing.exception;

import java.io.IOException;

public class FileNotAvailableException extends IOException {

	private static final long serialVersionUID = 1L;

	public FileNotAvailableException(String message, Throwable cause) {
		super(message, cause);
	}

	public FileNotAvailableException(String message) {
		super(message);
	}

}
