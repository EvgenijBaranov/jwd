package by.baranov.parsing.validator;

import java.io.File;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import by.baranov.parsing.exception.FileNotAvailableException;
import by.baranov.parsing.exception.ValidationException;

public class ValidatorXSDForXMLFile implements ValidatorFile {
	private static final Logger LOGGER = LogManager.getLogger();
	private static final String XSD_SCHEMA_FILE_NAME = "candies.xsd";
	
	@Override
	public void validate(String xmlFilePath) throws ValidationException, FileNotAvailableException {
		
		File file = new File(getClass().getClassLoader().getResource(XSD_SCHEMA_FILE_NAME).getFile());
		String absolutePathToSchemaXSD = file.getAbsolutePath();
		LOGGER.log(Level.DEBUG, "Path to xsd schema : {}", absolutePathToSchemaXSD);
		
		try {
			SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			factory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			factory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");

			Schema schema = factory.newSchema(new File(absolutePathToSchemaXSD));
			Source source = new StreamSource(xmlFilePath);
			
			Validator validator = schema.newValidator();
			validator.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			validator.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
			validator.validate(source);
		} catch (IOException ioException) {
			throw new FileNotAvailableException("ERROR I/O: Unfortunately file isn't available.", ioException);
		} catch (SAXException saxException) {
			throw new ValidationException("Validation XML file using XSD schema is failure.", saxException);
		}catch(NullPointerException nullPointerException) {
			throw new FileNotAvailableException("ERROR I/O: Unfortunately you didn't write file path.", nullPointerException);
		}
		LOGGER.log(Level.INFO, "ValidatorXSDForXMLFile is SUCCESS");
	}

}
