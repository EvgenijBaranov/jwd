package by.baranov.parsing.entity;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

public abstract class Candy {
	private String id;
	private LocalDate dateOfManufacture;
	private LocalTime timeOfManufacture;
	private String name;
	private Caloricity caloricity;
	private NutritionalValue nutritionalValue;
	private String production;
	
	public Candy() {
		caloricity = new Caloricity();
		nutritionalValue = new NutritionalValue();
	}

	public Candy(String id, LocalDate dateOfManufacture, LocalTime timeOfManufacture, String name,
			Caloricity caloricity, NutritionalValue nutritionalValue, String production) {
		this.id = id;
		this.dateOfManufacture = dateOfManufacture;
		this.timeOfManufacture = timeOfManufacture;
		this.name = name;
		this.caloricity = caloricity;
		this.nutritionalValue = nutritionalValue;
		this.production = production;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LocalDate getDateOfManufacture() {
		return dateOfManufacture;
	}

	public void setDateOfManufacture(LocalDate dateOfManufacture) {
		this.dateOfManufacture = dateOfManufacture;
	}

	public LocalTime getTimeOfManufacture() {
		return timeOfManufacture;
	}

	public void setTimeOfManufacture(LocalTime timeOfManufacture) {
		this.timeOfManufacture = timeOfManufacture;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Caloricity getCaloricity() {
		return caloricity;
	}

	public void setCaloricity(Caloricity caloricity) {
		this.caloricity = caloricity;
	}

	public NutritionalValue getNutritionalValue() {
		return nutritionalValue;
	}

	public void setNutritionalValue(NutritionalValue nutritionalValue) {
		this.nutritionalValue = nutritionalValue;
	}

	public String getProduction() {
		return production;
	}

	public void setProduction(String production) {
		this.production = production;
	}

	@Override
	public int hashCode() {
		return Objects.hash(caloricity, dateOfManufacture, id, name, nutritionalValue, production, timeOfManufacture);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Candy))
			return false;
		Candy other = (Candy) obj;
		return Objects.equals(caloricity, other.caloricity)
				&& Objects.equals(dateOfManufacture, other.dateOfManufacture) 
				&& Objects.equals(id, other.id)
				&& Objects.equals(name, other.name) 
				&& Objects.equals(nutritionalValue, other.nutritionalValue)
				&& Objects.equals(production, other.production)
				&& Objects.equals(timeOfManufacture, other.timeOfManufacture);
	}

	@Override
	public String toString() {
		return "id=" + id + ", dateOfManufacture=" + dateOfManufacture + ", timeOfManufacture="
				+ timeOfManufacture + ", name=" + name + ", " + caloricity + ", "
				+ nutritionalValue + ", production=" + production;
	}

}
