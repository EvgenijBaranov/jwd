package by.baranov.parsing.parser.builder;

import java.io.IOException;
import java.util.List;

import javax.xml.XMLConstants;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import by.baranov.parsing.entity.Candy;
import by.baranov.parsing.exception.FileNotAvailableException;
import by.baranov.parsing.exception.ParserException;
import by.baranov.parsing.parser.sax.CandiesHandler;

public class CandiesSAXBuilder implements Builder{
	private static final Logger LOGGER = LogManager.getLogger();
	private List<Candy> candies;

	@Override
	public void buildListCandies(String fileName) throws ParserException, FileNotAvailableException {
		try {
			LOGGER.log(Level.DEBUG, "CandiesSAXBuilder starts parsing ...");
			XMLReader reader = XMLReaderFactory.createXMLReader();
			reader.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			reader.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
			CandiesHandler handler = new CandiesHandler(reader);
			reader.setContentHandler(handler);
			reader.parse(fileName);
			candies = handler.getCandies();
			LOGGER.log(Level.DEBUG, "CandiesSAXBuilder candies was builded successfully with the result : {}", candies);
		} catch (SAXException saxException) {
			throw new ParserException("ERROR: SAX parser failure.", saxException);
		} catch (IOException ioException) {
			throw new FileNotAvailableException("ERROR I/O: the file " + fileName + " isn't available for SAX parser", ioException);
		}
	}
	
	@Override
	public List<Candy> getCandies() {
		return candies;
	}
	
}
