package by.baranov.parsing.entity;

public enum FillingCandy {
	NUT, COCONUT, JAM;
	
	public String getName() {
		return toString();
	}
}
