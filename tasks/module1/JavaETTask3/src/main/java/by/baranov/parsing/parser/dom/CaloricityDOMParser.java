package by.baranov.parsing.parser.dom;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;

import by.baranov.parsing.entity.Caloricity;
import by.baranov.parsing.entity.UnitCaloricity;
import by.baranov.parsing.parser.builder.ChocolateCandyTagsEnum;
import by.baranov.parsing.parser.builder.Utils;

public class CaloricityDOMParser {
	private static final Logger LOGGER = LogManager.getLogger();
	private Caloricity caloricity = new Caloricity();

	public Caloricity parseCaloricity(Element candyElement) {
		
		Element caloricityElement = (Element) candyElement.getElementsByTagName(ChocolateCandyTagsEnum.CALORICITY.getTagName()).item(0);
		if(caloricityElement.hasAttribute(ChocolateCandyTagsEnum.UNIT_CALORICITY.getTagName())) {
			String calorisityAttribute = caloricityElement.getAttribute(ChocolateCandyTagsEnum.UNIT_CALORICITY.getTagName());
			UnitCaloricity unit = UnitCaloricity.valueOf(calorisityAttribute.toUpperCase().replace("-", "_"));
			caloricity.setUnit(unit);
			LOGGER.log(Level.DEBUG, "Caloricity unit is {}", caloricity.getUnit().name());
		}
		String caloricityValue = Utils.getElementTextContent(candyElement, ChocolateCandyTagsEnum.CALORICITY.getTagName());
		caloricity.setCaloricityValue(Float.parseFloat(caloricityValue));
		LOGGER.log(Level.DEBUG, "Caloricity caloricityValue is {}", caloricity.getCaloricityValue());
		return caloricity;
	}
	
}
