package by.baranov.parsing.parser.sax;

import java.util.EnumSet;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import by.baranov.parsing.entity.FillingCandy;
import by.baranov.parsing.entity.IngredientsChocolateCandy;
import by.baranov.parsing.entity.TypeChocolate;
import by.baranov.parsing.parser.builder.ChocolateCandyTagsEnum;
import by.baranov.parsing.parser.builder.Utils;

public class IngredientsChocolateCandyHandler extends DefaultHandler {
	private static final Logger LOGGER = LogManager.getLogger();
	private IngredientsChocolateCandy currentIngredientsChocolateCandy = null;
	private ChocolateCandyTagsEnum currentEnum = null;
	private EnumSet<ChocolateCandyTagsEnum> tagsIngredientsWithText;
	private XMLReader reader;
	private ContentHandler handler;

	public IngredientsChocolateCandyHandler() {
		tagsIngredientsWithText = EnumSet.range(ChocolateCandyTagsEnum.SUGAR, ChocolateCandyTagsEnum.FILLING);
	}

	public void collectIngredientsChocolateCandy(XMLReader reader, ContentHandler handler, IngredientsChocolateCandy ingredientsChocolateCandy) {
		this.reader = reader;
		this.handler = handler;
		currentIngredientsChocolateCandy = ingredientsChocolateCandy;
		reader.setContentHandler(this);
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) {

		if (ChocolateCandyTagsEnum.CHOCOLATE.getTagName().equalsIgnoreCase(localName) && Utils.isExistAttributeForSAXParser(attributes)) {
			String attributeChocolateName = ChocolateCandyTagsEnum.TYPE_CHOCOLATE.getTagName();
			TypeChocolate notDefaultTypeChocolateValue = TypeChocolate.valueOf(attributes.getValue(attributeChocolateName).toUpperCase());
			currentIngredientsChocolateCandy.setTypeChocolate(notDefaultTypeChocolateValue);
			LOGGER.log(Level.DEBUG, "IngredientsChocolateCandy typeChocolate is {}", currentIngredientsChocolateCandy.getTypeChocolate().name());
		}

		if (ChocolateCandyTagsEnum.FILLING.getTagName().equalsIgnoreCase(localName) && Utils.isExistAttributeForSAXParser(attributes)) {
			String attributeFillingName = ChocolateCandyTagsEnum.TYPE_FILLING.getTagName();
			FillingCandy notDefaultFillingChocolateCandyValue = FillingCandy.valueOf(attributes.getValue(attributeFillingName).toUpperCase());
			currentIngredientsChocolateCandy.setTypeFilling(notDefaultFillingChocolateCandyValue);
			LOGGER.log(Level.DEBUG, "IngredientsChocolateCandy typeFilling is {}", currentIngredientsChocolateCandy.getTypeFilling().name());
		} 
		
		ChocolateCandyTagsEnum temp = ChocolateCandyTagsEnum.valueOf(localName.toUpperCase().replace("-", "_"));
		if (tagsIngredientsWithText.contains(temp)) {
			currentEnum = temp;
		}
	}
	
	@Override
	public void characters(char[] characterData, int startPositionCharacterData, int lengthCharacterData) {
		String resultReading = new String(characterData, startPositionCharacterData, lengthCharacterData);
		if (currentEnum != null) {
			switch (currentEnum) {
				case SUGAR :
					float sugarValue = Float.parseFloat(resultReading);
					currentIngredientsChocolateCandy.setSugarValue(sugarValue);
					LOGGER.log(Level.DEBUG, "IngredientsChocolateCandy sugarValue is {}", currentIngredientsChocolateCandy.getSugarValue());
					break;
				case CHOCOLATE :
					float chocolateValue = Float.parseFloat(resultReading);
					currentIngredientsChocolateCandy.setChocolateValue(chocolateValue);
					LOGGER.log(Level.DEBUG, "IngredientsChocolateCandy chocolateValue is {}", currentIngredientsChocolateCandy.getChocolateValue());
					break;
				case VANILINE :
					float vanilineValue = Float.parseFloat(resultReading);
					currentIngredientsChocolateCandy.setVanilineValue(vanilineValue);
					LOGGER.log(Level.DEBUG, "IngredientsChocolateCandy vanilineValue is {}", currentIngredientsChocolateCandy.getVanilineValue());
					break;
				case FILLING :
					float fillingValue = Float.parseFloat(resultReading);
					currentIngredientsChocolateCandy.setFillingValue(fillingValue);
					LOGGER.log(Level.DEBUG, "IngredientsChocolateCandy fillingValue is {}", currentIngredientsChocolateCandy.getFillingValue());
					break;
				default:
					throw new EnumConstantNotPresentException(currentEnum.getDeclaringClass(), currentEnum.name());
			}
			currentEnum = null;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		if (ChocolateCandyTagsEnum.INGREDIENTS.getTagName().equalsIgnoreCase(localName)) {
			reader.setContentHandler(handler);
		}
	}
	
}
