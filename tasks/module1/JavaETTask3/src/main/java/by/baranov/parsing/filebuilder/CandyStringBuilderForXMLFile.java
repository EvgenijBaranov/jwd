package by.baranov.parsing.filebuilder;

import java.util.LinkedList;
import java.util.List;

import by.baranov.parsing.entity.Candy;

public class CandyStringBuilderForXMLFile implements StringBuilderForXMLFile {
	private final Candy candy;
	private final List<StringBuilderForXMLFile> nodes = new LinkedList<>(); 

	public CandyStringBuilderForXMLFile(Candy candy) {
		this.candy = candy;
	}

	@Override
	public String buildElement() {
		StringBuilder builder = new StringBuilder();
	
		builder.append(candy.getId());
		builder.append("\" date-of-manufacture=\"");
		builder.append(candy.getDateOfManufacture());
		builder.append("\" time-of-manufacture=\"");
		builder.append(candy.getTimeOfManufacture());
		builder.append("\">\n");
		builder.append("<name>");
		builder.append(candy.getName());
		builder.append("</name>\n");
		builder.append("<production>");
		builder.append(candy.getProduction());
		builder.append("</production>\n");
		for(StringBuilderForXMLFile node : nodes) {
			builder.append(node.buildElement());
		}
		
		return builder.toString();
	}

	@Override
	public void addNode(StringBuilderForXMLFile node) {
		nodes.add(node);
	}
}
