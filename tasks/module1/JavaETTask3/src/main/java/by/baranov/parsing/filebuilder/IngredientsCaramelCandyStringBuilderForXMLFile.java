package by.baranov.parsing.filebuilder;

import by.baranov.parsing.entity.IngredientsCaramelCandy;

public class IngredientsCaramelCandyStringBuilderForXMLFile implements StringBuilderForXMLFile {
	private final IngredientsCaramelCandy ingredients;

	public IngredientsCaramelCandyStringBuilderForXMLFile(IngredientsCaramelCandy ingredients) {
		this.ingredients = ingredients;
	}

	@Override
	public String buildElement() {
		StringBuilder builder = new StringBuilder();

		builder.append("<ingredients unit-ingredient=\"");
		builder.append(ingredients.getUnit().name().toLowerCase());
		builder.append("\">\n");
		builder.append("<water>");
		builder.append(ingredients.getWaterValue());
		builder.append("</water>\n");
		builder.append("<sugar>");
		builder.append(ingredients.getSugarValue());
		builder.append("</sugar>\n");
		builder.append("<vaniline>");
		builder.append(ingredients.getVanilineValue());
		builder.append("</vaniline>\n");
		builder.append("</ingredients>\n");

		return builder.toString();
	}
}
