package by.baranov.parsing.parser.dom;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;

import by.baranov.parsing.entity.IngredientsCaramelCandy;
import by.baranov.parsing.entity.UnitMass;
import by.baranov.parsing.parser.builder.CaramelCandyTagsEnum;
import by.baranov.parsing.parser.builder.Utils;

public class IngredientsCaramelCandyDOMParser {
	private static final Logger LOGGER = LogManager.getLogger();
	private IngredientsCaramelCandy ingredients = new IngredientsCaramelCandy();

	public IngredientsCaramelCandy parseIngredientsCaramelCandy(Element candyElement) {
		Element ingredientsElement = (Element) candyElement.getElementsByTagName(CaramelCandyTagsEnum.INGREDIENTS.getTagName()).item(0);
		
		if(ingredientsElement.hasAttribute(CaramelCandyTagsEnum.UNIT_INGREDIENT.getTagName())) {
			String ingredientsAttribute = ingredientsElement.getAttribute(CaramelCandyTagsEnum.UNIT_INGREDIENT.getTagName());
			UnitMass unit = UnitMass.valueOf(ingredientsAttribute.toUpperCase().replace("-", "_"));
			ingredients.setUnit(unit);
			LOGGER.log(Level.DEBUG, "IngredientsCaramelCandy unit is {}", ingredients.getUnit().name());
		}
		
		String waterValue = Utils.getElementTextContent(ingredientsElement, CaramelCandyTagsEnum.WATER.getTagName());
		ingredients.setWaterValue(Float.parseFloat(waterValue));
		LOGGER.log(Level.DEBUG, "IngredientsCaramelCandy waterValue is {}", ingredients.getWaterValue());

		String sugarValue = Utils.getElementTextContent(ingredientsElement, CaramelCandyTagsEnum.SUGAR.getTagName());
		ingredients.setSugarValue(Float.parseFloat(sugarValue));
		LOGGER.log(Level.DEBUG, "IngredientsCaramelCandy sugarValue is {}", ingredients.getSugarValue());
		
		String vanilineValue = Utils.getElementTextContent(ingredientsElement, CaramelCandyTagsEnum.VANILINE.getTagName());
		ingredients.setVanilineValue(Float.parseFloat(vanilineValue));
		LOGGER.log(Level.DEBUG, "IngredientsCaramelCandy vanilineValue is {}", ingredients.getVanilineValue());
		
		return ingredients;
	}
	
}
