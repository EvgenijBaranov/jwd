package by.baranov.parsing.parser.sax;

import java.util.EnumSet;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import by.baranov.parsing.entity.NutritionalValue;
import by.baranov.parsing.parser.builder.ChocolateCandyTagsEnum;

public class NutritionalValueCandyHandler extends DefaultHandler {
	private static final Logger LOGGER = LogManager.getLogger();
	private NutritionalValue currentNutritionalValue = null;
	private ChocolateCandyTagsEnum currentEnum = null;
	private EnumSet<ChocolateCandyTagsEnum> tagsNutritionalValueWithBody;
	private XMLReader reader;
	private ContentHandler handler;

	public NutritionalValueCandyHandler() {
		tagsNutritionalValueWithBody = EnumSet.range(ChocolateCandyTagsEnum.PROTEIN, ChocolateCandyTagsEnum.CARBOHYDRATE);
	}

	public void collectNutritionalValue(XMLReader reader, ContentHandler handler, NutritionalValue nutritionalValue) {
		this.reader = reader;
		this.handler = handler;
		currentNutritionalValue = nutritionalValue;
		reader.setContentHandler(this);
	}

	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) {

		ChocolateCandyTagsEnum temp = ChocolateCandyTagsEnum.valueOf(localName.toUpperCase().replace("-", "_"));
		if (tagsNutritionalValueWithBody.contains(temp)) {
			currentEnum = temp;
		}
	}
	
	@Override
	public void characters(char[] characterData, int startPositionCharacterData, int lengthCharacterData) {
		String resultReading = new String(characterData, startPositionCharacterData, lengthCharacterData);
		if (currentEnum != null) {
			switch (currentEnum) {
				case PROTEIN :
					float proteinValue = Float.parseFloat(resultReading);
					currentNutritionalValue.setProteinValue(proteinValue);
					LOGGER.log(Level.DEBUG, "NutritionalValue proteinValue is {}", currentNutritionalValue.getProteinValue());
					break;
				case FAT :
					float fatValue = Float.parseFloat(resultReading);
					currentNutritionalValue.setFatValue(fatValue);
					LOGGER.log(Level.DEBUG, "NutritionalValue fatValue is {}", currentNutritionalValue.getFatValue());
					break;
				case CARBOHYDRATE :
					float carbohydrateValue = Float.parseFloat(resultReading);
					currentNutritionalValue.setCarbohydrateValue(carbohydrateValue);
					LOGGER.log(Level.DEBUG, "NutritionalValue carbohydrateValue is {}", currentNutritionalValue.getCarbohydrateValue());
					break;
				default:
					throw new EnumConstantNotPresentException(currentEnum.getDeclaringClass(), currentEnum.name());
			}
			currentEnum = null;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		if (ChocolateCandyTagsEnum.NUTRITIONAL_VALUE.getTagName().equalsIgnoreCase(localName)) {
			reader.setContentHandler(handler);
		}
	}
}
