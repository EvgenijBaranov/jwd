package by.baranov.parsing.parser.stax;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.parsing.entity.Caloricity;
import by.baranov.parsing.entity.UnitCaloricity;
import by.baranov.parsing.parser.builder.ChocolateCandyTagsEnum;
import by.baranov.parsing.parser.builder.Utils;

public class CaloricityStAXParser {
	private static final Logger LOGGER = LogManager.getLogger();
	private	Caloricity caloricity = new Caloricity();
	
	public Caloricity parseCaloricity(XMLStreamReader reader) throws XMLStreamException {
		if(Utils.isExistAttributeForStAXParser(reader)) {
			String attributeCaloricityName = ChocolateCandyTagsEnum.UNIT_CALORICITY.getTagName();
			UnitCaloricity unitCaloricity = UnitCaloricity.valueOf(reader.getAttributeValue("", attributeCaloricityName).toUpperCase());
			caloricity.setUnit(unitCaloricity);
			LOGGER.log(Level.DEBUG, "Caloricity unit is {}", caloricity.getUnit().name());
		}
		String bodyCaloricityTag = Utils.getTagBodyOfXMLFile(reader);
		float caloricityValue = Float.parseFloat(bodyCaloricityTag);
		caloricity.setCaloricityValue(caloricityValue);
		LOGGER.log(Level.DEBUG, "Caloricity caloricityValue is {}", caloricity.getCaloricityValue());
		return caloricity;
	}
	
}
