package by.baranov.parsing.entity;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

public class CaramelCandy extends Candy {
	private IngredientsCaramelCandy ingredients;

	public CaramelCandy() {
		ingredients = new IngredientsCaramelCandy();
	}
	
	public CaramelCandy(String id, LocalDate dateOfManufacture, LocalTime timeOfManufacture, String name,
			Caloricity caloricity, NutritionalValue nutritionalValue, String production,
			IngredientsCaramelCandy ingredients) {
		super(id, dateOfManufacture, timeOfManufacture, name, caloricity, nutritionalValue, production);
		this.ingredients = ingredients;
	}

	public IngredientsCaramelCandy getIngredients() {
		return ingredients;
	}

	public void setIngredients(IngredientsCaramelCandy ingredients) {
		this.ingredients = ingredients;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(ingredients);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CaramelCandy))
			return false;
		CaramelCandy other = (CaramelCandy) obj;
		return Objects.equals(ingredients, other.ingredients);
	}

	@Override
	public String toString() {
		return "CaramelCandy [" + super.toString() + ", " + ingredients + "]";
	}

}
