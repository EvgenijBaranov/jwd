package by.baranov.parsing.parser.dom;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;

import by.baranov.parsing.entity.FillingCandy;
import by.baranov.parsing.entity.IngredientsChocolateCandy;
import by.baranov.parsing.entity.TypeChocolate;
import by.baranov.parsing.entity.UnitMass;
import by.baranov.parsing.parser.builder.ChocolateCandyTagsEnum;
import by.baranov.parsing.parser.builder.Utils;

public class IngredientsChocolateCandyDOMParser {
	private static final Logger LOGGER = LogManager.getLogger();
	private IngredientsChocolateCandy ingredients = new IngredientsChocolateCandy();

	public IngredientsChocolateCandy parseIngredientsChocolateCandy(Element candyElement) {
		Element ingredientsElement = (Element) candyElement.getElementsByTagName(ChocolateCandyTagsEnum.INGREDIENTS.getTagName()).item(0);
		
		if(ingredientsElement.hasAttribute(ChocolateCandyTagsEnum.UNIT_INGREDIENT.getTagName())) {
			String ingredientsAttribute = ingredientsElement.getAttribute(ChocolateCandyTagsEnum.UNIT_INGREDIENT.getTagName());
			UnitMass unit = UnitMass.valueOf(ingredientsAttribute.toUpperCase().replace("-", "_"));
			ingredients.setUnit(unit);
			LOGGER.log(Level.DEBUG, "IngredientsChocolateCandy unit is {}", ingredients.getUnit().name());
		}
		
		String sugarValue = Utils.getElementTextContent(ingredientsElement, ChocolateCandyTagsEnum.SUGAR.getTagName());
		ingredients.setSugarValue(Float.parseFloat(sugarValue));
		LOGGER.log(Level.DEBUG, "IngredientsChocolateCandy sugarValue is {}", ingredients.getSugarValue());
		
		setChocolateElement(ingredientsElement);
		
		String vanilineValue = Utils.getElementTextContent(ingredientsElement, ChocolateCandyTagsEnum.VANILINE.getTagName());
		ingredients.setVanilineValue(Float.parseFloat(vanilineValue));
		LOGGER.log(Level.DEBUG, "IngredientsChocolateCandy vanilineValue is {}", ingredients.getVanilineValue());
		
		setFillingElement(ingredientsElement);
		
		return ingredients;
	}
	
	private void setChocolateElement(Element ingredientsElement ) {
		Element chocolateElement = (Element) ingredientsElement.getElementsByTagName(ChocolateCandyTagsEnum.CHOCOLATE.getTagName()).item(0);
		if(chocolateElement.hasAttribute(ChocolateCandyTagsEnum.TYPE_CHOCOLATE.getTagName())) {
			String chocolateAttribute = chocolateElement.getAttribute(ChocolateCandyTagsEnum.TYPE_CHOCOLATE.getTagName());
			TypeChocolate typeChocolate = TypeChocolate.valueOf(chocolateAttribute.toUpperCase().replace("-", "_"));
			ingredients.setTypeChocolate(typeChocolate);
			LOGGER.log(Level.DEBUG, "IngredientsChocolateCandy typeChocolate is {}", ingredients.getTypeChocolate().name());
		}
		String chocolateValue = Utils.getElementTextContent(ingredientsElement, ChocolateCandyTagsEnum.CHOCOLATE.getTagName());
		ingredients.setChocolateValue(Float.parseFloat(chocolateValue));
		LOGGER.log(Level.DEBUG, "IngredientsChocolateCandy chocolateValue is {}", ingredients.getChocolateValue());
	}
	
	private void setFillingElement(Element ingredientsElement) {
		Element fillingElement = (Element) ingredientsElement.getElementsByTagName(ChocolateCandyTagsEnum.FILLING.getTagName()).item(0);
		if(fillingElement.hasAttribute(ChocolateCandyTagsEnum.TYPE_FILLING.getTagName())) {
			String fillingAttribute = fillingElement.getAttribute(ChocolateCandyTagsEnum.TYPE_FILLING.getTagName());
			FillingCandy fillingCandy = FillingCandy.valueOf(fillingAttribute.toUpperCase().replace("-", "_"));
			ingredients.setTypeFilling(fillingCandy);
			LOGGER.log(Level.DEBUG, "IngredientsChocolateCandy typeFilling is {}", ingredients.getTypeFilling().name());
		}
		String fillingValue = Utils.getElementTextContent(ingredientsElement, ChocolateCandyTagsEnum.FILLING.getTagName());
		ingredients.setFillingValue(Float.parseFloat(fillingValue));
		LOGGER.log(Level.DEBUG, "IngredientsChocolateCandy fillingValue is {}", ingredients.getFillingValue());
	}
	
}
