package by.baranov.parsing.parser.stax;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import by.baranov.parsing.entity.CaramelCandy;
import by.baranov.parsing.parser.builder.CaramelCandyTagsEnum;
import by.baranov.parsing.parser.builder.ChocolateCandyTagsEnum;
import by.baranov.parsing.parser.builder.Utils;

public class CaramelCandyStAXParser {
	private CaramelCandy caramelCandy = new CaramelCandy();
	
	public CaramelCandy parseCaramelCandy(XMLStreamReader reader) throws XMLStreamException {
		String attributeIDCaramelCandyName = CaramelCandyTagsEnum.ID.getTagName();
		caramelCandy.setId(reader.getAttributeValue("", attributeIDCaramelCandyName));
		String attributeDateOfManufactureName = CaramelCandyTagsEnum.DATE_OF_MANUFACTURE.getTagName();
		caramelCandy.setDateOfManufacture(LocalDate.parse(reader.getAttributeValue("", attributeDateOfManufactureName)));
		String attributeTimeOfManufactureName = CaramelCandyTagsEnum.TIME_OF_MANUFACTURE.getTagName();
		caramelCandy.setTimeOfManufacture(LocalTime.parse(reader.getAttributeValue("", attributeTimeOfManufactureName)));

		String name;
		while (reader.hasNext()) {
			int type = reader.next();
			if (type == XMLStreamConstants.START_ELEMENT) {
				name = reader.getLocalName();
				switch (ChocolateCandyTagsEnum.valueOf(name.toUpperCase().replace("-", "_"))) {
					case NAME:
						caramelCandy.setName(Utils.getTagBodyOfXMLFile(reader));
						break;
					case CALORICITY:
						caramelCandy.setCaloricity(new CaloricityStAXParser().parseCaloricity(reader));
						break;
					case NUTRITIONAL_VALUE:
						caramelCandy.setNutritionalValue(new NutritionalValueStAXParser().parseNutritionalValue(reader));
						break;
					case PRODUCTION:
						caramelCandy.setProduction(Utils.getTagBodyOfXMLFile(reader));
						break;
					case INGREDIENTS:
						caramelCandy.setIngredients(new IngredientsCaramelCandyStAXParser().parseIngredients(reader));
						break;
					default:
						throw new XMLStreamException("Unknown element in tag <chocolate-candy>");
				}
			} else if (type == XMLStreamConstants.END_ELEMENT) {
				name = reader.getLocalName();
				if (name.equalsIgnoreCase(CaramelCandyTagsEnum.CARAMEL_CANDY.getTagName())) {
					return caramelCandy;
				}
			}
		}
		throw new XMLStreamException("Unknown element in tag <chocolate-candy>");
	}

}
