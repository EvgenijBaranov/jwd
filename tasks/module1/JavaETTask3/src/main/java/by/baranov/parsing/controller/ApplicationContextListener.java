package by.baranov.parsing.controller;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ApplicationContextListener implements ServletContextListener{
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ApplicationContext.getInstance().initialize();
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		ApplicationContext.getInstance().destroy();
	}

}
