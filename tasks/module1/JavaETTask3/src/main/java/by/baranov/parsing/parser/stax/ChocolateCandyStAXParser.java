package by.baranov.parsing.parser.stax;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.parsing.entity.ChocolateCandy;
import by.baranov.parsing.parser.builder.ChocolateCandyTagsEnum;
import by.baranov.parsing.parser.builder.Utils;

public class ChocolateCandyStAXParser {
	private static final Logger LOGGER = LogManager.getLogger();
	private ChocolateCandy chocolateCandy = new ChocolateCandy();
	
	public ChocolateCandy parseChocolateCandy(XMLStreamReader reader) throws XMLStreamException {
		String attributeIDChocoladeCandyName = ChocolateCandyTagsEnum.ID.getTagName();
		chocolateCandy.setId(reader.getAttributeValue("", attributeIDChocoladeCandyName));
		LOGGER.log(Level.DEBUG, "ChocolateCandy ID is {}", chocolateCandy.getId());
		String attributeDateOfManufactureName = ChocolateCandyTagsEnum.DATE_OF_MANUFACTURE.getTagName();
		chocolateCandy.setDateOfManufacture(LocalDate.parse(reader.getAttributeValue("", attributeDateOfManufactureName)));
		LOGGER.log(Level.DEBUG, "ChocolateCandy dateOfManufacture is {}", chocolateCandy.getDateOfManufacture());
		String attributeTimeOfManufactureName = ChocolateCandyTagsEnum.TIME_OF_MANUFACTURE.getTagName();
		chocolateCandy.setTimeOfManufacture(LocalTime.parse(reader.getAttributeValue("", attributeTimeOfManufactureName)));
		LOGGER.log(Level.DEBUG, "ChocolateCandy timeOfManufacture is {}", chocolateCandy.getTimeOfManufacture());
		
		String name;
		while (reader.hasNext()) {
			int type = reader.next();
			if (type == XMLStreamConstants.START_ELEMENT) {
				name = reader.getLocalName();
				switch (ChocolateCandyTagsEnum.valueOf(name.toUpperCase().replace("-", "_"))) {
					case NAME:
						chocolateCandy.setName(Utils.getTagBodyOfXMLFile(reader));
						LOGGER.log(Level.DEBUG, "ChocolateCandy name is {}", chocolateCandy.getName());
						break;
					case CALORICITY:
						chocolateCandy.setCaloricity(new CaloricityStAXParser().parseCaloricity(reader));
						break;
					case NUTRITIONAL_VALUE:
						chocolateCandy.setNutritionalValue(new NutritionalValueStAXParser().parseNutritionalValue(reader));
						break;
					case PRODUCTION:
						chocolateCandy.setProduction(Utils.getTagBodyOfXMLFile(reader));
						LOGGER.log(Level.DEBUG, "ChocolateCandy name is {}", chocolateCandy.getProduction());
						break;
					case INGREDIENTS:
						chocolateCandy.setIngredients(new IngredientsChocolateCandyStAXParser().parseIngredients(reader));
						break;
					default:
						throw new XMLStreamException("Unknown element in tag <chocolate-candy>");
				}
			} else if (type == XMLStreamConstants.END_ELEMENT) {
				name = reader.getLocalName();
				if (name.equalsIgnoreCase(ChocolateCandyTagsEnum.CHOCOLATE_CANDY.getTagName())) {
					LOGGER.log(Level.DEBUG, "ChocolateCandy was parsed succesfully with result {}", chocolateCandy);
					return chocolateCandy;
				}
			}
		}
		throw new XMLStreamException("Unknown element in tag <chocolate-candy>");
	}

}
