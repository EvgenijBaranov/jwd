package by.baranov.parsing.parser.builder;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.parsing.entity.Candy;
import by.baranov.parsing.entity.CaramelCandy;
import by.baranov.parsing.entity.ChocolateCandy;
import by.baranov.parsing.exception.FileNotAvailableException;
import by.baranov.parsing.exception.ParserException;
import by.baranov.parsing.parser.stax.CaramelCandyStAXParser;
import by.baranov.parsing.parser.stax.ChocolateCandyStAXParser;

public class CandiesStAXBuilder implements Builder{
	private static final Logger LOGGER = LogManager.getLogger();
	private List<Candy> candies = new ArrayList<>();

	@Override
	public void buildListCandies(String filePath) throws ParserException, FileNotAvailableException {
		LOGGER.log(Level.DEBUG, "CandiesStAXBuilder starts parsing ...");
		XMLInputFactory inputFactory = XMLInputFactory.newFactory();
		inputFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		inputFactory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
		
		XMLStreamReader reader = null;
		String name;

		try (InputStream inputStream = Files.newInputStream(Paths.get(filePath))) {
			reader = inputFactory.createXMLStreamReader(inputStream);

			while (reader.hasNext()) {
				int type = reader.next();
				if (type == XMLStreamConstants.START_ELEMENT) {
					name = reader.getLocalName();

					if (name.equalsIgnoreCase(ChocolateCandyTagsEnum.CHOCOLATE_CANDY.getTagName())) {
						LOGGER.log(Level.DEBUG, "CandiesStAXBuilder starts chocolate candy parsing ...");
						ChocolateCandy chocolateCandy = new ChocolateCandyStAXParser().parseChocolateCandy(reader);
						candies.add(chocolateCandy);
						LOGGER.log(Level.DEBUG, "CandiesStAXBuilder chocolate candy was builded successfully with the result : {}", chocolateCandy);
					}
					if (name.equalsIgnoreCase(CaramelCandyTagsEnum.CARAMEL_CANDY.getTagName())) {
						LOGGER.log(Level.DEBUG, "CandiesStAXBuilder starts caramel candy parsing ...");
						CaramelCandy caramelCandy = new CaramelCandyStAXParser().parseCaramelCandy(reader);
						candies.add(caramelCandy);
						LOGGER.log(Level.DEBUG, "CandiesStAXBuilder caramel candy was builded successfully with the result : {}", caramelCandy);
					}
				}
			}
		} catch (XMLStreamException staxException) {
			throw new ParserException("ERROR: StAX parser failure.", staxException);
		} catch (IOException ioException) {
			throw new FileNotAvailableException("ERROR I/O: the file " + filePath + " isn't available for StAX parser", ioException);
		}
	}
	
	@Override
	public List<Candy> getCandies() {
		return candies;
	}

}
