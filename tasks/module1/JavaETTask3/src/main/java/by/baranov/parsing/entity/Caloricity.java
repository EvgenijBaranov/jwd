package by.baranov.parsing.entity;

import java.util.Objects;

public class Caloricity {
	private UnitCaloricity unit = UnitCaloricity.KCAL;
	private float caloricityValue;

	public Caloricity() {}
	
	public Caloricity(UnitCaloricity unit, float caloricityValue) {
		this.unit = unit;
		this.caloricityValue = caloricityValue;
	}

	public UnitCaloricity getUnit() {
		return unit;
	}

	public void setUnit(UnitCaloricity unit) {
		this.unit = unit;
	}

	public float getCaloricityValue() {
		return caloricityValue;
	}

	public void setCaloricityValue(float caloricityValue) {
		this.caloricityValue = caloricityValue;
	}

	@Override
	public int hashCode() {
		return Objects.hash(caloricityValue, unit);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Caloricity))
			return false;
		Caloricity other = (Caloricity) obj;
		return Float.floatToIntBits(caloricityValue) == Float.floatToIntBits(other.caloricityValue)
				&& unit == other.unit;
	}

	@Override
	public String toString() {
		return "Caloricity [unit=" + unit.name().toLowerCase() + ": value=" + caloricityValue + "]";
	}
	
}
