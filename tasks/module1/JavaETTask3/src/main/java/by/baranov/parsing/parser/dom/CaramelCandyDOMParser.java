package by.baranov.parsing.parser.dom;

import java.time.LocalDate;
import java.time.LocalTime;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;

import by.baranov.parsing.entity.CaramelCandy;
import by.baranov.parsing.parser.builder.CaramelCandyTagsEnum;
import by.baranov.parsing.parser.builder.ChocolateCandyTagsEnum;
import by.baranov.parsing.parser.builder.Utils;

public class CaramelCandyDOMParser {
	private static final Logger LOGGER = LogManager.getLogger();
	private CaramelCandy caramelCandy = new CaramelCandy();

	public CaramelCandy parseCaramelCandy(Element candyElement) {
		setAttributes(candyElement);
		caramelCandy.setName(Utils.getElementTextContent(candyElement, CaramelCandyTagsEnum.NAME.getTagName()));
		LOGGER.log(Level.DEBUG, "CaramelCandy name is {}", caramelCandy.getName());
		caramelCandy.setCaloricity(new CaloricityDOMParser().parseCaloricity(candyElement));
		caramelCandy.setNutritionalValue(new NutritionalValueDOMParser().parseNutritionalValue(candyElement));
		caramelCandy.setProduction(Utils.getElementTextContent(candyElement, ChocolateCandyTagsEnum.PRODUCTION.getTagName()));
		LOGGER.log(Level.DEBUG, "CaramelCandy production is {}", caramelCandy.getProduction());
		caramelCandy.setIngredients(new IngredientsCaramelCandyDOMParser().parseIngredientsCaramelCandy(candyElement));

		return caramelCandy;
	}

	private void setAttributes(Element candyElement) {
		caramelCandy.setId(candyElement.getAttribute(CaramelCandyTagsEnum.ID.getTagName()));
		LOGGER.log(Level.DEBUG, "CaramelCandy ID is {}", caramelCandy.getId());
		String dateFromFile = candyElement.getAttribute(CaramelCandyTagsEnum.DATE_OF_MANUFACTURE.getTagName());
		caramelCandy.setDateOfManufacture(LocalDate.parse(dateFromFile));
		LOGGER.log(Level.DEBUG, "CaramelCandy dateOfManufacture is {}", caramelCandy.getDateOfManufacture());
		String timeFromFile = candyElement.getAttribute(CaramelCandyTagsEnum.TIME_OF_MANUFACTURE.getTagName());
		caramelCandy.setTimeOfManufacture(LocalTime.parse(timeFromFile));
		LOGGER.log(Level.DEBUG, "CaramelCandy timeOfManufacture is {}", caramelCandy.getTimeOfManufacture());
	}
}
