package by.baranov.parsing.parser.builder;

import java.util.List;

import by.baranov.parsing.entity.Candy;
import by.baranov.parsing.exception.FileNotAvailableException;
import by.baranov.parsing.exception.ParserException;

public interface Builder {
	
	void buildListCandies(String filePath) throws ParserException, FileNotAvailableException;
	
	List<Candy> getCandies();
	
}
