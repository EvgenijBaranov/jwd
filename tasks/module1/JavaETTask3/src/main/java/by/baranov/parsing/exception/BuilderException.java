package by.baranov.parsing.exception;

public class BuilderException extends Exception {

	private static final long serialVersionUID = 1L;

	public BuilderException(String message, Throwable cause) {
		super(message, cause);
	}

	public BuilderException(String message) {
		super(message);
	}

}
