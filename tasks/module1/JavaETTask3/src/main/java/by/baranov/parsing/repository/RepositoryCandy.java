package by.baranov.parsing.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.parsing.entity.Candy;

public class RepositoryCandy implements Repository<Candy> {
	private static final Logger LOGGER = LogManager.getLogger();
	private List<Candy> candies = new ArrayList<>();

	@Override
	public List<Candy> getAll() {
		return candies;
	}

	@Override
	public void addList(List<Candy> listElements) {
		candies.addAll(listElements);
		LOGGER.log(Level.INFO, "Candy list {} is added succesfully in candy repository", listElements);
	}

}
