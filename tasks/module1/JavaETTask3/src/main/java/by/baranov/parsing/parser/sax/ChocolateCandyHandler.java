package by.baranov.parsing.parser.sax;

import java.util.EnumSet;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import by.baranov.parsing.entity.ChocolateCandy;
import by.baranov.parsing.entity.UnitCaloricity;
import by.baranov.parsing.entity.UnitMass;
import by.baranov.parsing.parser.builder.ChocolateCandyTagsEnum;
import by.baranov.parsing.parser.builder.Utils;

public class ChocolateCandyHandler extends DefaultHandler {
	private static final Logger LOGGER = LogManager.getLogger();
	private ChocolateCandy currentCandy = null;
	private ChocolateCandyTagsEnum currentEnum = null;
	private EnumSet<ChocolateCandyTagsEnum> tagsWithText;
	private XMLReader reader;
	private ContentHandler handler;
	private NutritionalValueCandyHandler nutritionalValueHandler = new NutritionalValueCandyHandler();
	private IngredientsChocolateCandyHandler ingredientsChocolateCandyHandler = new IngredientsChocolateCandyHandler();
	
	public ChocolateCandyHandler() {
		tagsWithText = EnumSet.range(ChocolateCandyTagsEnum.NAME, ChocolateCandyTagsEnum.FILLING);
	}

	public void collectChocolateCandy(XMLReader reader, ContentHandler handler, ChocolateCandy candy) {
		this.reader = reader;
		this.handler = handler;
		currentCandy = candy;
		reader.setContentHandler(this);
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) {

		if (ChocolateCandyTagsEnum.CALORICITY.getTagName().equalsIgnoreCase(localName) && Utils.isExistAttributeForSAXParser(attributes)) {
			String attributeCaloricityName = ChocolateCandyTagsEnum.UNIT_CALORICITY.getTagName();
			UnitCaloricity notDefaultUnitValue = UnitCaloricity.valueOf(attributes.getValue(attributeCaloricityName).toUpperCase());
			currentCandy.getCaloricity().setUnit(notDefaultUnitValue);
			LOGGER.log(Level.DEBUG, "ChocolateCandy Caloricity unit is {}", currentCandy.getCaloricity().getUnit().name());
		}

		if (ChocolateCandyTagsEnum.NUTRITIONAL_VALUE.getTagName().equalsIgnoreCase(localName)) {
			if(Utils.isExistAttributeForSAXParser(attributes)) {
				String attributeNutritionalValueName = ChocolateCandyTagsEnum.UNIT_NUTRITIONAL.getTagName();
				UnitMass notDefaultUnitValue = UnitMass.valueOf(attributes.getValue(attributeNutritionalValueName).toUpperCase());
				currentCandy.getNutritionalValue().setUnit(notDefaultUnitValue);
				LOGGER.log(Level.DEBUG, "ChocolateCandy NutritionalValue unit is {}", currentCandy.getNutritionalValue().getUnit().name());
			}
			nutritionalValueHandler.collectNutritionalValue(reader, this, currentCandy.getNutritionalValue());
		}

		if (ChocolateCandyTagsEnum.INGREDIENTS.getTagName().equalsIgnoreCase(localName)) {
			if(Utils.isExistAttributeForSAXParser(attributes)){
				String attributeIngridientsName = ChocolateCandyTagsEnum.UNIT_INGREDIENT.getTagName();
				UnitMass notDefaultUnitValue = UnitMass.valueOf(attributes.getValue(attributeIngridientsName).toUpperCase());
				currentCandy.getIngredients().setUnit(notDefaultUnitValue);
				LOGGER.log(Level.DEBUG, "ChocolateCandy Ingridients unit is {}", currentCandy.getIngredients().getUnit().name());
			}
			ingredientsChocolateCandyHandler.collectIngredientsChocolateCandy(reader, this, currentCandy.getIngredients());
		}

		ChocolateCandyTagsEnum temp = ChocolateCandyTagsEnum.valueOf(localName.toUpperCase().replace("-", "_"));
		if (tagsWithText.contains(temp)) {
			currentEnum = temp;
		}
	}

	@Override
	public void characters(char[] characterData, int startPositionCharacterData, int lengthCharacterData) {
		String resultReading = new String(characterData, startPositionCharacterData, lengthCharacterData);
		if (currentEnum != null) {
			switch (currentEnum) {
				case NAME:
					currentCandy.setName(resultReading);
					LOGGER.log(Level.DEBUG, "ChocolateCandy name is {}", currentCandy.getName());
					break;
				case CALORICITY :
					float caloricityValue = Float.parseFloat(resultReading);
					currentCandy.getCaloricity().setCaloricityValue(caloricityValue);
					LOGGER.log(Level.DEBUG, "ChocolateCandy caloricityValue is {}", currentCandy.getCaloricity().getCaloricityValue());
					break;
				case PRODUCTION :
					currentCandy.setProduction(resultReading);
					LOGGER.log(Level.DEBUG, "ChocolateCandy production is {}", currentCandy.getProduction());
					break;
				default:
					throw new EnumConstantNotPresentException(currentEnum.getDeclaringClass(), currentEnum.name());
			}
			currentEnum = null;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		if (ChocolateCandyTagsEnum.CHOCOLATE_CANDY.getTagName().equalsIgnoreCase(localName)) {
			reader.setContentHandler(handler);
		}
	}

}
