package by.baranov.parsing.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.parsing.entity.Candy;
import by.baranov.parsing.exception.BuilderException;
import by.baranov.parsing.exception.FileNotAvailableException;
import by.baranov.parsing.exception.ParserException;
import by.baranov.parsing.exception.ValidationException;
import by.baranov.parsing.parser.builder.Builder;
import by.baranov.parsing.parser.builder.CandyBuilderFactory;
import by.baranov.parsing.service.CandyService;
import by.baranov.parsing.validator.ValidatorPathFile;
import by.baranov.parsing.validator.ValidatorXSDForXMLFile;

public class AddCandiesFromFileCommand implements Command {
	private static final String PARAMETER_NAME_FILE_PATH = "filePath";
	private static final String PARAMETER_NAME_PARSER = "parser";
	private static final String ATTRIBUTE_NAME_FOR_ERROR = "errorMessage";
	private static final String RESULT_FILE_PATH = "/jsp/resultParsing.jsp";
	private static final String ERROR_FILE_PATH = "/jsp/errorPage.jsp";
	private static final Logger LOGGER = LogManager.getLogger();
	private final CandyService candyService;

	public AddCandiesFromFileCommand(CandyService candyService) {
		this.candyService = candyService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		String resultFilePathFromCommand = RESULT_FILE_PATH;
		String inputedFilePath = request.getParameter(PARAMETER_NAME_FILE_PATH);
		String parserName = request.getParameter(PARAMETER_NAME_PARSER);
		request.getSession().setAttribute("parserName", parserName);
		LOGGER.log(Level.DEBUG, "inputedFilePath : {} , parserName : {}", inputedFilePath, parserName);
		ValidatorPathFile validatorFilePath = new ValidatorPathFile();
		ValidatorXSDForXMLFile validatorXSDForXMLFile = new ValidatorXSDForXMLFile();
		try {
			validatorFilePath.validate(inputedFilePath);
			validatorXSDForXMLFile.validate(inputedFilePath);
			Builder builder = new CandyBuilderFactory().createCandyBuilder(parserName);
			builder.buildListCandies(inputedFilePath);
			List<Candy> candiesFromXMLFile = builder.getCandies();
			candyService.addList(candiesFromXMLFile);
			List<Candy> candiesFromRepository = candyService.getAll();
			request.getSession().setAttribute("candies", candiesFromRepository);
		} catch (ValidationException exception) {
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_ERROR, exception.getMessage());
			resultFilePathFromCommand = ERROR_FILE_PATH;
			LOGGER.log(Level.ERROR, "Validation error : ", exception);
		} catch (FileNotAvailableException ioException) {
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_ERROR, ioException.getMessage());
			resultFilePathFromCommand = ERROR_FILE_PATH;
			LOGGER.log(Level.ERROR, "File isn't available : ", ioException);
		} catch (ParserException parseException) {
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_ERROR, parseException.getMessage());
			resultFilePathFromCommand = ERROR_FILE_PATH;
			LOGGER.log(Level.ERROR, "Parsing error : ", parseException);
		} catch (BuilderException builderException) {
			request.getSession().setAttribute(ATTRIBUTE_NAME_FOR_ERROR, builderException.getMessage());
			resultFilePathFromCommand = ERROR_FILE_PATH;
			LOGGER.log(Level.ERROR, "Builder error : ", builderException);
		}
		LOGGER.log(Level.INFO, "{} was executed succesfully. Page path for controller is {}", getClass().getSimpleName(), resultFilePathFromCommand);
		
		return TypeActionForCommand.REDIRECT + resultFilePathFromCommand;
	}

}
