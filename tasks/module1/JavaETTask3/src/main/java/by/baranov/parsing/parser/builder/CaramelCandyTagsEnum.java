package by.baranov.parsing.parser.builder;

public enum CaramelCandyTagsEnum {
	CARAMEL_CANDY("caramel-candy"),
	ID("id"),
	DATE_OF_MANUFACTURE("date-of-manufacture"),
	TIME_OF_MANUFACTURE("time-of-manufacture"),
	UNIT_CALORICITY("unit-caloricity"),
	NUTRITIONAL_VALUE("nutritional-value"),
	UNIT_NUTRITIONAL("unit-nutritional"),
	INGREDIENTS("ingredients"),
	UNIT_INGREDIENT("unit-ingredient"),
	NAME("name"),
	CALORICITY("caloricity"),
	PROTEIN("protein"),
	FAT("fat"),
	CARBOHYDRATE("carbohydrate"),
	PRODUCTION("production"),
	WATER("water"),
	SUGAR("sugar"),
	VANILINE("vaniline");
	
	private String tagName;

	private CaramelCandyTagsEnum(String tagName) {
		this.tagName = tagName;
	}

	public String getTagName() {
		return tagName;
	}
}
