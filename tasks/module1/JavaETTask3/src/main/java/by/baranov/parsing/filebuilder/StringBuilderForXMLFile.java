package by.baranov.parsing.filebuilder;

public interface StringBuilderForXMLFile {
	
	String buildElement();
	
	default void addNode(StringBuilderForXMLFile node) {}
	
}
