package by.baranov.parsing.parser.stax;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.parsing.entity.FillingCandy;
import by.baranov.parsing.entity.IngredientsChocolateCandy;
import by.baranov.parsing.entity.TypeChocolate;
import by.baranov.parsing.entity.UnitMass;
import by.baranov.parsing.parser.builder.ChocolateCandyTagsEnum;
import by.baranov.parsing.parser.builder.Utils;

public class IngredientsChocolateCandyStAXParser {
	private static final Logger LOGGER = LogManager.getLogger();
	private IngredientsChocolateCandy ingredients = new IngredientsChocolateCandy();

	public IngredientsChocolateCandy parseIngredients(XMLStreamReader reader) throws XMLStreamException {
		if(Utils.isExistAttributeForStAXParser(reader)) {
			String attributeIngredientsName = ChocolateCandyTagsEnum.UNIT_INGREDIENT.getTagName();
			UnitMass unitIngredientsValue = UnitMass.valueOf(reader.getAttributeValue("", attributeIngredientsName).toUpperCase());
			ingredients.setUnit(unitIngredientsValue);
			LOGGER.log(Level.DEBUG, "IngredientsChocolateCandy unit is {}", ingredients.getUnit().name());
		}
		while (reader.hasNext()) {
			int type = reader.next();
			String name;
			if (type == XMLStreamConstants.START_ELEMENT) {
				name = reader.getLocalName();
				switch(ChocolateCandyTagsEnum.valueOf(name.toUpperCase())) {
					case SUGAR:
						float sugarValue = Float.parseFloat(Utils.getTagBodyOfXMLFile(reader));
						ingredients.setSugarValue(sugarValue);
						LOGGER.log(Level.DEBUG, "IngredientsChocolateCandy sugarValue is {}", ingredients.getSugarValue());
						break;
					case CHOCOLATE:
						setAttributeChocolateTag(reader);
						float chocolateValue = Float.parseFloat(Utils.getTagBodyOfXMLFile(reader));
						ingredients.setChocolateValue(chocolateValue);
						LOGGER.log(Level.DEBUG, "IngredientsChocolateCandy chocolateValue is {}", ingredients.getChocolateValue());
						break;
					case VANILINE:
						float vanilineValue = Float.parseFloat(Utils.getTagBodyOfXMLFile(reader));
						ingredients.setVanilineValue(vanilineValue);
						LOGGER.log(Level.DEBUG, "IngredientsChocolateCandy vanilineValue is {}", ingredients.getVanilineValue());
						break;
					case FILLING:
						setAttributeFillingTag(reader);
						float fillingValue = Float.parseFloat(Utils.getTagBodyOfXMLFile(reader));
						ingredients.setFillingValue(fillingValue);
						LOGGER.log(Level.DEBUG, "IngredientsChocolateCandy fillingValue is {}", ingredients.getFillingValue());
						break;
					default:
						throw new XMLStreamException("Unknown element in tag <ingredients>");
				}
			} else if (type == XMLStreamConstants.END_ELEMENT) {
				name = reader.getLocalName();
				if (name.equalsIgnoreCase(ChocolateCandyTagsEnum.INGREDIENTS.getTagName())) {
					LOGGER.log(Level.DEBUG, "IngredientsChocolateCandy was parsed succesfully with result {}", ingredients);
					return ingredients;
				}
			}
		}
		throw new XMLStreamException("Unknown element in tag <ingredients>");
	}

	private void setAttributeChocolateTag(XMLStreamReader reader) {
		if(Utils.isExistAttributeForStAXParser(reader)) {
			String attributeChocolateName = ChocolateCandyTagsEnum.TYPE_CHOCOLATE.getTagName();
			TypeChocolate typeChocolate = TypeChocolate.valueOf(reader.getAttributeValue("", attributeChocolateName).toUpperCase());
			ingredients.setTypeChocolate(typeChocolate);
			LOGGER.log(Level.DEBUG, "IngredientsChocolateCandy typeChocolate is {}", ingredients.getTypeChocolate().name());
		}
	}
	
	private void setAttributeFillingTag(XMLStreamReader reader) {
		if(Utils.isExistAttributeForStAXParser(reader)) {
			String attributeFillingName = ChocolateCandyTagsEnum.TYPE_FILLING.getTagName();
			FillingCandy fillingCandy = FillingCandy.valueOf(reader.getAttributeValue("", attributeFillingName).toUpperCase());
			ingredients.setTypeFilling(fillingCandy);
			LOGGER.log(Level.DEBUG, "IngredientsChocolateCandy fillingCandy is {}", ingredients.getTypeFilling().name());
		}
	}
}
