package by.baranov.parsing.filebuilder;

import by.baranov.parsing.entity.Caloricity;

public class CaloricityStringBuilderForXMLFile implements StringBuilderForXMLFile {
	private final Caloricity caloricity;

	public CaloricityStringBuilderForXMLFile(Caloricity caloricity) {
		this.caloricity = caloricity;
	}

	@Override
	public String buildElement() {
		StringBuilder builder = new StringBuilder();
		
		builder.append("<caloricity unit-caloricity=\"");
		builder.append(caloricity.getUnit().name().toLowerCase());
		builder.append("\">");
		builder.append(caloricity.getCaloricityValue());
		builder.append("</caloricity>\n");
		
		return builder.toString();
	}
	
}
