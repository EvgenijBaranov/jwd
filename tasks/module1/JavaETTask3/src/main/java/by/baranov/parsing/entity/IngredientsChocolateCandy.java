package by.baranov.parsing.entity;

import java.util.Objects;

public class IngredientsChocolateCandy {
	private UnitMass unit = UnitMass.MG;
	private float sugarValue;
	private TypeChocolate typeChocolate = TypeChocolate.BLACK;
	private float chocolateValue;
	private float vanilineValue;
	private FillingCandy typeFilling = FillingCandy.NUT;
	private float fillingValue;

	public IngredientsChocolateCandy() {
	}

	public IngredientsChocolateCandy(UnitMass unit, float sugarValue, TypeChocolate typeChocolate, float chocolateValue,
			float vanilineValue, FillingCandy typeFilling, float fillingValue) {
		this.unit = unit;
		this.sugarValue = sugarValue;
		this.typeChocolate = typeChocolate;
		this.chocolateValue = chocolateValue;
		this.vanilineValue = vanilineValue;
		this.typeFilling = typeFilling;
		this.fillingValue = fillingValue;
	}

	public UnitMass getUnit() {
		return unit;
	}

	public void setUnit(UnitMass unit) {
		this.unit = unit;
	}

	public float getSugarValue() {
		return sugarValue;
	}

	public void setSugarValue(float sugarValue) {
		this.sugarValue = sugarValue;
	}

	public TypeChocolate getTypeChocolate() {
		return typeChocolate;
	}

	public void setTypeChocolate(TypeChocolate typeChocolate) {
		this.typeChocolate = typeChocolate;
	}

	public float getChocolateValue() {
		return chocolateValue;
	}

	public void setChocolateValue(float chocolateValue) {
		this.chocolateValue = chocolateValue;
	}

	public float getVanilineValue() {
		return vanilineValue;
	}

	public void setVanilineValue(float vanilineValue) {
		this.vanilineValue = vanilineValue;
	}

	public FillingCandy getTypeFilling() {
		return typeFilling;
	}

	public void setTypeFilling(FillingCandy typeFilling) {
		this.typeFilling = typeFilling;
	}

	public float getFillingValue() {
		return fillingValue;
	}

	public void setFillingValue(float fillingValue) {
		this.fillingValue = fillingValue;
	}

	@Override
	public int hashCode() {
		return Objects.hash(chocolateValue, typeFilling, fillingValue, sugarValue, typeChocolate, unit, vanilineValue);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof IngredientsChocolateCandy))
			return false;
		IngredientsChocolateCandy other = (IngredientsChocolateCandy) obj;
		return Float.floatToIntBits(chocolateValue) == Float.floatToIntBits(other.chocolateValue)
				&& typeFilling == other.typeFilling
				&& Float.floatToIntBits(fillingValue) == Float.floatToIntBits(other.fillingValue)
				&& Float.floatToIntBits(sugarValue) == Float.floatToIntBits(other.sugarValue)
				&& typeChocolate == other.typeChocolate && unit == other.unit
				&& Float.floatToIntBits(vanilineValue) == Float.floatToIntBits(other.vanilineValue);
	}

	@Override
	public String toString() {
		return "IngredientsChocolateCandy[unit=" + unit.name().toLowerCase() + ", sugar=" + sugarValue + ", typeChocolate=" + typeChocolate
				+ ", chocolate=" + chocolateValue + ", vaniline=" + vanilineValue + ", typeFilling=" + typeFilling
				+ ", fillingValue=" + fillingValue + "]";
	}

}
