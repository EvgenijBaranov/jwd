package by.baranov.parsing.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DefaultCommand implements Command {
	private static final String RESULT_FILE_PATH = "/jsp/inputData.jsp";

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {

		return TypeActionForCommand.FORWARD + RESULT_FILE_PATH;

	}

}
