package by.baranov.parsing.repository;

import by.baranov.parsing.service.CRUDOperation;

public interface Repository<T> extends CRUDOperation<T>{
	
}
