package by.baranov.parsing.entity;

public enum TypeChocolate {
	BLACK, WHITE, MILK;
	
	public String getName() {
		return toString();
	}
}
