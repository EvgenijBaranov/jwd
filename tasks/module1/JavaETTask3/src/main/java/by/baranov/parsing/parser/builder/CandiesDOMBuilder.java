package by.baranov.parsing.parser.builder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import by.baranov.parsing.entity.Candy;
import by.baranov.parsing.entity.CaramelCandy;
import by.baranov.parsing.entity.ChocolateCandy;
import by.baranov.parsing.exception.FileNotAvailableException;
import by.baranov.parsing.exception.ParserException;
import by.baranov.parsing.parser.dom.CaramelCandyDOMParser;
import by.baranov.parsing.parser.dom.ChocolateCandyDOMParser;

public class CandiesDOMBuilder implements Builder {
	private static final Logger LOGGER = LogManager.getLogger();
	private List<Candy> candies = new ArrayList<>();
	
	@Override
	public void buildListCandies(String filePath) throws FileNotAvailableException, ParserException {
		LOGGER.log(Level.DEBUG, "CandiesDOMBuilder starts parsing ...");
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
		
		try {
			DocumentBuilder docBuilder = factory.newDocumentBuilder();
			Document document = docBuilder.parse(filePath);
			Element root = document.getDocumentElement();
			NodeList chocolatecCandyList = root.getElementsByTagName(ChocolateCandyTagsEnum.CHOCOLATE_CANDY.getTagName());
			for(int i = 0; i < chocolatecCandyList.getLength(); i++) {
				LOGGER.log(Level.DEBUG, "CandiesDOMBuilder starts chocolate candy parsing ...");
				Element candyElement = (Element) chocolatecCandyList.item(i);
				ChocolateCandy chocolateCandy = new ChocolateCandyDOMParser().parseChocolateCandy(candyElement);
				candies.add(chocolateCandy);
				LOGGER.log(Level.DEBUG, "CandiesDOMBuilder chocolate candy was builded successfully with the result : {}", chocolateCandy);
			}
			
			NodeList caramelCandyList = root.getElementsByTagName(CaramelCandyTagsEnum.CARAMEL_CANDY.getTagName());
			for(int i = 0; i < caramelCandyList.getLength(); i++) {
				LOGGER.log(Level.DEBUG, "CandiesDOMBuilder starts caramel candy parsing ...");
				Element candyElement = (Element) caramelCandyList.item(i);
				CaramelCandy caramelCandy = new CaramelCandyDOMParser().parseCaramelCandy(candyElement);
				candies.add(caramelCandy);
				LOGGER.log(Level.DEBUG, "CandiesDOMBuilder caramel candy was builded successfully with the result : {}", caramelCandy);
			}
		} catch (IOException ioException) {
			throw new FileNotAvailableException("ERROR I/O: the file " + filePath + " isn't available for DOM parser", ioException);
		} catch (SAXException | ParserConfigurationException domException) {
			throw new ParserException("ERROR: DOM parser failure.", domException);
		} 
	}
	
	@Override
	public List<Candy> getCandies() {
		return candies;
	}

}
