package by.baranov.parsing.exception;

public class ParserException extends Exception {

	private static final long serialVersionUID = 1L;

	public ParserException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ParserException(String message, Throwable cause) {
		super(message, cause);
	}

}
