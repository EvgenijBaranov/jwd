package by.baranov.parsing.parser.builder;

public enum ChocolateCandyTagsEnum {
	CHOCOLATE_CANDY("chocolate-candy"),
	ID("id"),
	DATE_OF_MANUFACTURE("date-of-manufacture"),
	TIME_OF_MANUFACTURE("time-of-manufacture"),
	UNIT_CALORICITY("unit-caloricity"),
	NUTRITIONAL_VALUE("nutritional-value"),
	UNIT_NUTRITIONAL("unit-nutritional"),
	INGREDIENTS("ingredients"),
	UNIT_INGREDIENT("unit-ingredient"),
	TYPE_CHOCOLATE("type-chocolate"),
	TYPE_FILLING("type-filling"),
	NAME("name"),
	CALORICITY("caloricity"),
	PROTEIN("protein"),
	FAT("fat"),
	CARBOHYDRATE("carbohydrate"),
	PRODUCTION("production"),
	SUGAR("sugar"),
	CHOCOLATE("chocolate"),
	VANILINE("vaniline"),
	FILLING("filling");
	
	private String tagName;

	private ChocolateCandyTagsEnum(String tagName) {
		this.tagName = tagName;
	}

	public String getTagName() {
		return tagName;
	}
	
}
