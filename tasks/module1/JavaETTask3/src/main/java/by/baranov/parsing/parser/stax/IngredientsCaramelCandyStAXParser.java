package by.baranov.parsing.parser.stax;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.parsing.entity.IngredientsCaramelCandy;
import by.baranov.parsing.entity.UnitMass;
import by.baranov.parsing.parser.builder.CaramelCandyTagsEnum;
import by.baranov.parsing.parser.builder.Utils;

public class IngredientsCaramelCandyStAXParser {
	private static final Logger LOGGER = LogManager.getLogger();
	private IngredientsCaramelCandy ingredients = new IngredientsCaramelCandy();

	public IngredientsCaramelCandy parseIngredients(XMLStreamReader reader) throws XMLStreamException {
		if(Utils.isExistAttributeForStAXParser(reader)) {
			String attributeIngredientsName = CaramelCandyTagsEnum.UNIT_INGREDIENT.getTagName();
			UnitMass unitIngredientsValue = UnitMass.valueOf(reader.getAttributeValue("", attributeIngredientsName).toUpperCase());
			ingredients.setUnit(unitIngredientsValue);
			LOGGER.log(Level.DEBUG, "IngredientsCaramelCandy unit is {}", ingredients.getUnit().name());
		}
		while (reader.hasNext()) {
			int type = reader.next();
			String name;
			if (type == XMLStreamConstants.START_ELEMENT) {
				name = reader.getLocalName();
				switch(CaramelCandyTagsEnum.valueOf(name.toUpperCase())) {
					case WATER:
						float waterValue = Float.parseFloat(Utils.getTagBodyOfXMLFile(reader));
						ingredients.setWaterValue(waterValue);
						LOGGER.log(Level.DEBUG, "IngredientsCaramelCandy waterValue is {}", ingredients.getWaterValue());
						break;
					case SUGAR:
						float sugarValue = Float.parseFloat(Utils.getTagBodyOfXMLFile(reader));
						ingredients.setSugarValue(sugarValue);
						LOGGER.log(Level.DEBUG, "IngredientsCaramelCandy sugarValue is {}", ingredients.getSugarValue());
						break;
					case VANILINE:
						float vanilineValue = Float.parseFloat(Utils.getTagBodyOfXMLFile(reader));
						ingredients.setVanilineValue(vanilineValue);
						LOGGER.log(Level.DEBUG, "IngredientsCaramelCandy vanilineValue is {}", ingredients.getVanilineValue());
						break;
					default:
						throw new XMLStreamException("Unknown element in tag <ingredients>");
				}
			} else if (type == XMLStreamConstants.END_ELEMENT) {
				name = reader.getLocalName();
				if (name.equalsIgnoreCase(CaramelCandyTagsEnum.INGREDIENTS.getTagName())) {
					LOGGER.log(Level.DEBUG, "IngredientsCaramelCandy was parsed succesfully with result {}", ingredients);
					return ingredients;
				}
			}
		}
		throw new XMLStreamException("Unknown element in tag <ingredients>");
	}
}
