package by.baranov.parsing.service;

import java.util.List;

public interface CRUDOperation<T> {
	
	List<T> getAll();
	
	void addList(List<T> listElements);
}
