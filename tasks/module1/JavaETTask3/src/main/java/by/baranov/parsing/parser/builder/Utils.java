package by.baranov.parsing.parser.builder;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;

public class Utils {
	public static String getTagBodyOfXMLFile(XMLStreamReader reader) throws XMLStreamException {
		String tagBody = null;
		if (reader.hasNext()) {
			reader.next();
			tagBody = reader.getText().trim();
		}
		return tagBody;
	}
	public static boolean isExistAttributeForSAXParser(Attributes attributes) {
		return attributes.getLength() == 1;
	}

	public static boolean isExistAttributeForStAXParser(XMLStreamReader reader) {
		return reader.getAttributeCount() == 1;
	}
	
	public static String getElementTextContent(Element element, String tagName) {
		NodeList listNodes = element.getElementsByTagName(tagName);
		Node node = listNodes.item(0);
		String tagBody = node.getTextContent();
		return tagBody;
	}
}
