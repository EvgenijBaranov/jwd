package by.baranov.parsing.entity;

public enum UnitMass {
	MG, G, KG
}
