package by.baranov.parsing.validator;

import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import by.baranov.parsing.exception.ValidationException;

public class ValidatorPathFile implements ValidatorFile {

	@Override
	public void validate(String xmlFilePath) throws ValidationException {
		Path path = null;
		try {
			path = Paths.get(xmlFilePath);
		} catch (InvalidPathException e) {
			throw new ValidationException("Error: \"" + xmlFilePath + "\" can't be converted to a Path");
		}

		Optional<String> fileExtension = getFileExtension(xmlFilePath);
		boolean isValidFileExtension = fileExtension.isPresent() && "xml".equalsIgnoreCase(fileExtension.get());
		boolean  isValidFile = Files.isReadable(path) && isValidFileExtension;
		if(!isValidFile) {
			throw new ValidationException("Error: \""  + xmlFilePath + "\" can't be readed or hasn't xml extension");
		}		
	}

	private Optional<String> getFileExtension(String filePath) {
		return Optional	.ofNullable(filePath)
						.filter(name -> name.contains("."))
						.map(f -> f.substring(filePath.lastIndexOf(".") + 1));
	}

}
