package by.baranov.parsing.filebuilder;

import by.baranov.parsing.entity.NutritionalValue;

public class NutritionalValueStringBuilderForXMLFile implements StringBuilderForXMLFile {
	private final NutritionalValue nutritionalValue;

	public NutritionalValueStringBuilderForXMLFile(NutritionalValue nutritionalValue) {
		this.nutritionalValue = nutritionalValue;
	}

	@Override
	public String buildElement() {
		StringBuilder builder = new StringBuilder();
	
		builder.append("<nutritional-value unit-nutritional=\"");
		builder.append(nutritionalValue.getUnit().name().toLowerCase());
		builder.append("\">\n");
		builder.append("<protein>");
		builder.append(nutritionalValue.getProteinValue());
		builder.append("</protein>\n");
		builder.append("<fat>");
		builder.append(nutritionalValue.getFatValue());
		builder.append("</fat>\n");
		builder.append("<carbohydrate>");
		builder.append(nutritionalValue.getCarbohydrateValue());
		builder.append("</carbohydrate>\n");
		builder.append("</nutritional-value>\n");
		
		return builder.toString();
	}

}
