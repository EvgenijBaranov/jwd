package by.baranov.parsing.parser.sax;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import by.baranov.parsing.entity.Candy;
import by.baranov.parsing.entity.CaramelCandy;
import by.baranov.parsing.entity.ChocolateCandy;
import by.baranov.parsing.parser.builder.CaramelCandyTagsEnum;
import by.baranov.parsing.parser.builder.ChocolateCandyTagsEnum;

public class CandiesHandler extends DefaultHandler {
	private static final Logger LOGGER = LogManager.getLogger();
	private List<Candy> candies = new ArrayList<>();
	private ChocolateCandyHandler chocolateCandyHandler = new ChocolateCandyHandler();
	private CaramelCandyHandler caramelCandyHandler = new CaramelCandyHandler();
	private XMLReader reader;

	public CandiesHandler(XMLReader reader) {
		this.reader = reader;
	}

	public List<Candy> getCandies() {
		return candies;
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) {
		if (ChocolateCandyTagsEnum.CHOCOLATE_CANDY.getTagName().equalsIgnoreCase(localName)) {
			LOGGER.log(Level.DEBUG, "tag name : {}", localName);
			ChocolateCandy currentChocolateCandy = new ChocolateCandy();
			LOGGER.log(Level.DEBUG, "SAX Parser, parsing chocolate candy ...");
			currentChocolateCandy.setId(attributes.getValue(ChocolateCandyTagsEnum.ID.getTagName()));
			LOGGER.log(Level.DEBUG, "CholateCandy ID is {}", currentChocolateCandy.getId());
			currentChocolateCandy.setDateOfManufacture(LocalDate.parse(attributes.getValue(ChocolateCandyTagsEnum.DATE_OF_MANUFACTURE.getTagName())));
			LOGGER.log(Level.DEBUG, "CholateCandy dateOfManufacture is {}", currentChocolateCandy.getDateOfManufacture());
			currentChocolateCandy.setTimeOfManufacture(LocalTime.parse(attributes.getValue(ChocolateCandyTagsEnum.TIME_OF_MANUFACTURE.getTagName())));
			LOGGER.log(Level.DEBUG, "CholateCandy timeeOfManufacture is {}", currentChocolateCandy.getTimeOfManufacture());
			chocolateCandyHandler.collectChocolateCandy(reader, this, currentChocolateCandy);
			candies.add(currentChocolateCandy);
			LOGGER.log(Level.DEBUG, "CholateCandy was parsed succesfully with result: {}", currentChocolateCandy);
		}
		
		if (CaramelCandyTagsEnum.CARAMEL_CANDY.getTagName().equalsIgnoreCase(localName)) {
			LOGGER.log(Level.DEBUG, "tag name : {}", localName);
			CaramelCandy currentCaramelCandy = new CaramelCandy();
			LOGGER.log(Level.DEBUG, "SAX Parser, parsing caramel candy ...");
			currentCaramelCandy.setId(attributes.getValue(CaramelCandyTagsEnum.ID.getTagName()));
			LOGGER.log(Level.DEBUG, "CaramelCandy ID is {}", currentCaramelCandy.getId());
			currentCaramelCandy.setDateOfManufacture(LocalDate.parse(attributes.getValue(CaramelCandyTagsEnum.DATE_OF_MANUFACTURE.getTagName())));
			LOGGER.log(Level.DEBUG, "CaramelCandy dateOfManufacture is {}", currentCaramelCandy.getDateOfManufacture());
			currentCaramelCandy.setTimeOfManufacture(LocalTime.parse(attributes.getValue(CaramelCandyTagsEnum.TIME_OF_MANUFACTURE.getTagName())));
			LOGGER.log(Level.DEBUG, "CaramelCandy timeeOfManufacture is {}", currentCaramelCandy.getTimeOfManufacture());
			caramelCandyHandler.collectCaramelCandy(reader, this, currentCaramelCandy);
			candies.add(currentCaramelCandy);
			LOGGER.log(Level.DEBUG, "CaramelCandy was parsed succesfully with result: {}", currentCaramelCandy);
		}
	}
}
