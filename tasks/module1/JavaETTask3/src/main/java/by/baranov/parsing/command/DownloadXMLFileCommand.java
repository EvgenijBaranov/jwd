package by.baranov.parsing.command;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.parsing.service.CandyService;

public class DownloadXMLFileCommand implements Command {
	private static final String ATTRIBUTE_NAME_FOR_ERROR = "errorMessage";
	private static final String FILE_NAME_FOR_DOWNLOAD = "candy.xml";
	private static final String RESULT_FILE_PATH = "/jsp/resultParsing.jsp";
	private static final String ERROR_FILE_PATH = "/jsp/errorPage.jsp";
	private static final Logger LOGGER = LogManager.getLogger();
	private final CandyService candyService;

	public DownloadXMLFileCommand(CandyService candyService) {
		this.candyService = candyService;
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String commandResult = RESULT_FILE_PATH;
		String stringXMLFile = candyService.createStringFromRepositoryForXMLFile();
		LOGGER.log(Level.DEBUG, "Result of executing the {} : {}", getClass().getSimpleName(), stringXMLFile);
		response.setContentType("text/xml");
		response.setHeader("Content-Disposition", "attachment;filename=" + FILE_NAME_FOR_DOWNLOAD);
		
		try (BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(response.getOutputStream()))){
			bufferedWriter.write(stringXMLFile);
			bufferedWriter.flush();
		} catch (IOException exception) {
			LOGGER.log(Level.ERROR, "There are some problems with writing to response : {}", exception);
			request.setAttribute(ATTRIBUTE_NAME_FOR_ERROR, exception);
			commandResult = ERROR_FILE_PATH;
		}
		
		return TypeActionForCommand.FORWARD + commandResult;
	}

}
