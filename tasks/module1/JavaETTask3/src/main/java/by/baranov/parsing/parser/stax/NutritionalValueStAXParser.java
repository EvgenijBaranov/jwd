package by.baranov.parsing.parser.stax;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.baranov.parsing.entity.NutritionalValue;
import by.baranov.parsing.entity.UnitMass;
import by.baranov.parsing.parser.builder.ChocolateCandyTagsEnum;
import by.baranov.parsing.parser.builder.Utils;

public class NutritionalValueStAXParser {
	private static final Logger LOGGER = LogManager.getLogger();
	private NutritionalValue nutritionalValue = new NutritionalValue();
	
	public NutritionalValue parseNutritionalValue(XMLStreamReader reader) throws XMLStreamException {
		LOGGER.log(Level.DEBUG, "Start parse NutritionalValue...");
		if(Utils.isExistAttributeForStAXParser(reader)) {
			String attributeNutritionalValueName = ChocolateCandyTagsEnum.UNIT_NUTRITIONAL.getTagName();
			UnitMass unitNutritionalValue = UnitMass.valueOf(reader.getAttributeValue("", attributeNutritionalValueName).toUpperCase());
			nutritionalValue.setUnit(unitNutritionalValue);
			LOGGER.log(Level.DEBUG, "NutritionalValue unit is {}", nutritionalValue.getUnit().name());
		}
		while (reader.hasNext()) {
			int type = reader.next();
			String name;
			if (type == XMLStreamConstants.START_ELEMENT) {
				name = reader.getLocalName();
				switch (ChocolateCandyTagsEnum.valueOf(name.toUpperCase())) {
					case PROTEIN:
						float proteinValue = Float.parseFloat(Utils.getTagBodyOfXMLFile(reader));
						nutritionalValue.setProteinValue(proteinValue);
						LOGGER.log(Level.DEBUG, "NutritionalValue proteinValue is {}", nutritionalValue.getProteinValue());
						break;
					case FAT:
						float fatValue = Float.parseFloat(Utils.getTagBodyOfXMLFile(reader));
						nutritionalValue.setFatValue(fatValue);
						LOGGER.log(Level.DEBUG, "NutritionalValue fatValue is {}", nutritionalValue.getFatValue());
						break;
					case CARBOHYDRATE:
						float carbohydrateValue = Float.parseFloat(Utils.getTagBodyOfXMLFile(reader));
						nutritionalValue.setCarbohydrateValue(carbohydrateValue);
						LOGGER.log(Level.DEBUG, "NutritionalValue carbohydrateValue is {}", nutritionalValue.getCarbohydrateValue());
						break;
					default:
						throw new XMLStreamException("Unknown element in tag <nutritional-value>");
				}
			} else if (type == XMLStreamConstants.END_ELEMENT) {
				name = reader.getLocalName();
				if (name.equalsIgnoreCase(ChocolateCandyTagsEnum.NUTRITIONAL_VALUE.getTagName())) {
					LOGGER.log(Level.DEBUG, "NutritionalValue was parsed succesfully with result {}", nutritionalValue);
					return nutritionalValue;
				}
			}
		}
		throw new XMLStreamException("Unknown element in tag <nutritional-value>");
	}
	
}
