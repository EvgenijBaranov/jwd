package by.baranov.parsing.command;

import java.util.Optional;

public enum CommandName {
	DEFAULT,
	PARSE_XML_FILE_BY_PARSER,
	DOWNLOAD_XML_FILE;
	
	public static Optional<CommandName> fromString(String userCommand) {

		CommandName[] values = CommandName.values();
		for (CommandName commandName : values) {
			if (commandName.name().equalsIgnoreCase(userCommand)) {
				return Optional.of(commandName);
			}
		}
		return Optional.empty();
	}

}
