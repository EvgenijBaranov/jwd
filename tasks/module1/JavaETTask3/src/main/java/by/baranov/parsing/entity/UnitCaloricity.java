package by.baranov.parsing.entity;

public enum UnitCaloricity {
	KCAL, CAL, KJ
}
