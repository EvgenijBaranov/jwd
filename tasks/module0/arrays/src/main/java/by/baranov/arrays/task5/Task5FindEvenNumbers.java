package by.baranov.arrays.task5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Task5FindEvenNumbers {
	double[] array;

	public Task5FindEvenNumbers(double[] array) {
		if (array.length == 0) {
			throw new IllegalArgumentException("invalid input, array can't be empty");
		}
		this.array = array;
	}

	public void printArrayEvenNumbers() {
		double[] arrayEvenNumbers = defineArrayEvenNumbers();
		System.out.println("\n_TASK-5_\n");
		if (arrayEvenNumbers.length == 0) {
			printMessage();
		} else {
			System.out.println("The original array " + Arrays.toString(array) + "\n new array with even numbers : "
					+ Arrays.toString(arrayEvenNumbers));
		}
	}

	private double[] defineArrayEvenNumbers() {
		List<Double> result = new ArrayList<Double>();
		for (double elem : array) {
			if (elem % 2 == 0) {
				result.add(elem);
			}
		}

		double[] newArray = new double[result.size()];
		int i = 0;
		for (double elem : result) {
			newArray[i] = elem;
			i++;
		}
		return newArray;
	}

	private void printMessage() {
		System.out.println("Unfortunately the array " + Arrays.toString(array) + " hasn't even numbers");
	}

}
