package by.baranov.multidimensionalarrays.task34;

public class Task34RandomMatrix {
	private long[][] matrix;

	public Task34RandomMatrix(int quantityStrings, int quantityColumns) {
		checkInputData(quantityStrings, quantityColumns);
		this.matrix = new long[quantityStrings][quantityColumns];
	}

	private void checkInputData(int quantityStrings, int quantityColumns) {
		if (quantityStrings < 1 || quantityColumns < 1) {
			throw new IllegalArgumentException("Invalid input, the dimension of the matrix can't be less than 1");
		}
		if(quantityStrings < quantityColumns) {
			throw new IllegalArgumentException("Invalid input, quantity columns must be more than quantity strings");
		}
	}

	public void printMatrixAscendingDescendingStrings() {
		System.out.println("\n_TASK-34_\n");
		System.out.println("The Matrix where the number of 1 in a column is equal to the column number:");
		fillMatrix();
		printMatrix();
	}

	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if (j + 1 > i) {
					matrix[i][j] = 1;
				}
			}
		}
	}

	private void printMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			System.out.print("[");
			for (int j = 0; j < matrix[i].length; j++) {
				System.out.printf("%3d ", matrix[i][j]);
				if (j < matrix[i].length - 1 ) {
					System.out.print(", ");
				}
			}
			System.out.print("]\n");
		}
	}
}
