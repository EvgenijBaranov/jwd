package by.baranov.multidimensionalarrays.task36;

import java.util.Random;

public class Task36MatrixSmoothing {
	private double[][] matrix;
	private Random rand = new Random();

	public Task36MatrixSmoothing(int quantityStrings, int quantityColumns) {
		if (quantityStrings < 1 || quantityColumns < 1) {
			throw new IllegalArgumentException("Invalid input, the dimension of the matrix can't be less than 1");
		}
		this.matrix = new double[quantityStrings][quantityColumns];
	}

	public void printSmothedMatrix() {
		System.out.println("\n_TASK-36_\n");
		System.out.println("The origin matrix :");
		fillMatrix();
		printMatrix(matrix);
		double[][] smothedMatrix = smoothMatrix();
		System.out.print("\nThe smoothed matrix : \n");			
		printMatrix(smothedMatrix);
	}

	private double[][] smoothMatrix() {	
		double[][] newMatrix = new double[matrix.length][matrix[0].length];		
		for (int i = 0; i < newMatrix.length; i++) {
			for (int j = 0; j < newMatrix[0].length; j++) {
				newMatrix[i][j] = (j == 0 || j == matrix[0].length - 1) ? matrix[i][j] / 2 : (matrix[i][j -1] + matrix[i][j + 1]) / 2;			
			}
		}			
		return newMatrix;
	}
	
	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = rand.nextInt(9);
			}
		}
	}
	
	private void printMatrix(double[][] newMatrix) {
		for (int i = 0; i < newMatrix.length; i++) {
			System.out.print("[ ");
			for (int j = 0; j < newMatrix[0].length; j++) {
				System.out.printf("%5.2f ", newMatrix[i][j]);
				if (j < newMatrix[0].length - 1) {
					System.out.print(", ");
				}
			}
			System.out.print("]\n");
		}
	}
}
