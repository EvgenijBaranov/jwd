package by.baranov.arraysort.task6;

import java.util.Arrays;

public class Task6ShellSort {
	double[] array;

	public Task6ShellSort(double[] array) {
		if (array.length < 1) {
			throw new IllegalArgumentException("The invalid input, array can't have dimension less than 1");
		}
		this.array = array;
	}

	public void printSortedArray() {
		System.out.println("\n_TASK-6_\n");
		System.out.println("The origin array : " + Arrays.toString(array));
		sortArray();
		System.out.println("The sorted array (shell sort) : " + Arrays.toString(array));
	}

	private void sortArray() {
		double tempElement;
		for (int i = 0; i < array.length;) {
			if (array[i] > array[i + 1]) {
				tempElement = array[i];
				array[i] = array[i + 1];
				array[i + 1] = tempElement;
				if (i > 0) {
					i--;
				} 
			} else if(i < array.length - 2){
				i++;
			}else{
				break;
			}
		}
	}
}
