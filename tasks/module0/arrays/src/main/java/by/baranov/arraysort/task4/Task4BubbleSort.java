package by.baranov.arraysort.task4;

import java.util.Arrays;

public class Task4BubbleSort {
	private double[] sequenceNumbers;

	public Task4BubbleSort(double[] sequenceNumbers) {
		if (sequenceNumbers.length < 2 ) {
			throw new IllegalArgumentException("Invalid input, sequences can't have less than 2 elements");
		}
		this.sequenceNumbers = sequenceNumbers;		
	}
	
	public void printResultSorting() {
		System.out.println("\n_TASK-4_\n");
		System.out.println("The original sequence: " + Arrays.toString(sequenceNumbers));
		double[] sortedSequence = sortSequence();
		System.out.println("The sorted decreasing sequence (bubble sort) - " + Arrays.toString(sortedSequence));
	}
	
	private double[] sortSequence() {
		double tempElement = 0;
		for(int quantityIteration = 0; quantityIteration < sequenceNumbers.length - 1; quantityIteration++) {
			for(int i = 0, j = 1; i < sequenceNumbers.length; i++, j++) {				
				if(j < sequenceNumbers.length) {
					if(sequenceNumbers[i] < sequenceNumbers[j]) {
						tempElement = sequenceNumbers[i];
						sequenceNumbers[i] = sequenceNumbers[j];	
						sequenceNumbers[j] = tempElement;										
					}
				}
			}
		}		
		return sequenceNumbers;
	}	
}
