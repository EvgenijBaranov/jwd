package by.baranov.arrays.task1;

import java.util.Arrays;

public class Task1Array {

	private long[] array;

	public Task1Array(long[] array) {
		checkInputData(array);
		this.array = array;
	}

	private void checkInputData(long[] array) {
		for (long elem : array) {
			if (elem < 1) {
				throw new IllegalArgumentException("invalid input, the array can only consist of positive integers");
			}
		}

	}

	public void printSumMultipleMembers(long number) {
		System.out.println("\n\n\n ______ARRAYS______\n\n");
		System.out.println("\n_TASK-1_\n");
		long sumMultipleNumbers = calculateSumMultipleNumbers(number);
		System.out.printf("the sum of the elements in the array " + Arrays.toString(array) + ", multiples of %d  is equal to %d %n" , number, sumMultipleNumbers);
	}

	private long calculateSumMultipleNumbers(long number) {
		long result = 0;
		for (long elem : array) {
			if (elem % number == 0) {
				result += elem;
			}
		}
		return result;
	}
	
}
