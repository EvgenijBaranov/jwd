package by.baranov.multidimensionalarrays.task37;

import java.util.Random;

public class Task37RearrangeRandomStringsMatrix {
	private double[][] matrix;
	private Random rand = new Random();

	public Task37RearrangeRandomStringsMatrix(int quantityStrings, int quantityColumns) {
		if (quantityStrings < 1 || quantityColumns < 1) {
			throw new IllegalArgumentException("Invalid input, the dimension of the matrix can't be less than 1");
		}
		this.matrix = new double[quantityStrings][quantityColumns];
	}

	public void printRearrangedStringsMatrix() {
		System.out.println("\n_TASK-37_\n");
		System.out.println("The origin matrix :");
		fillMatrix();
		printMatrix(matrix);
		int indexFirstStringMatrix = rand.nextInt(matrix.length - 1);
		int indexSecondStringMatrix = rand.nextInt(matrix.length - 1);
		rearrangeStrings(indexFirstStringMatrix, indexSecondStringMatrix);
		System.out.print("\nThe matrix with rearranged stirngs " + indexFirstStringMatrix + " and "
										+ indexSecondStringMatrix + " has next form : \n");
		printMatrix(matrix);
	}

	private void rearrangeStrings(int indexFirstStringMatrix, int indexSecondStringMatrix) {
		indexFirstStringMatrix = indexFirstStringMatrix < indexSecondStringMatrix ? indexFirstStringMatrix : indexSecondStringMatrix;
		indexSecondStringMatrix = indexFirstStringMatrix < indexSecondStringMatrix ? indexSecondStringMatrix : indexFirstStringMatrix;
		double[] tempStringMatrix = new double[matrix[0].length];
		for (int i = 0; i < matrix.length; i++) {
			if (i == indexFirstStringMatrix) {
				for (int j = 0; j < matrix[i].length; j++) {
					tempStringMatrix[j] = matrix[i][j];
				}
			} else if (i == indexSecondStringMatrix) {
				for (int j = 0; j < matrix[i].length; j++) {
					matrix[indexFirstStringMatrix][j] = matrix[i][j];
					matrix[i][j] = tempStringMatrix[j];
				}
			}
		}
	}
	
	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = rand.nextInt(9);
			}
		}
	}

	private void printMatrix(double[][] newMatrix) {
		for (int i = 0; i < newMatrix.length; i++) {
			System.out.print("[ ");
			for (int j = 0; j < newMatrix[0].length; j++) {
				System.out.printf("%5.2f ", newMatrix[i][j]);
				if (j < newMatrix[0].length - 1) {
					System.out.print(", ");
				}
			}
			System.out.print("]\n");
		}
	}
}
