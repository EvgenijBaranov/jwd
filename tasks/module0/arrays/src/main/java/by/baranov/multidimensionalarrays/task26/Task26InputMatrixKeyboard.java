package by.baranov.multidimensionalarrays.task26;

public class Task26InputMatrixKeyboard {
	private double[][] matrix;

	public Task26InputMatrixKeyboard(double[][] matrix) {
		if (matrix.length < 1) {
			throw new IllegalArgumentException("Invalid input, the dimension of the matrix can't be less than 1");
		}
		this.matrix = matrix;
	}

	public void printQuadraticMatrix() {
		System.out.println("\n_TASK-26_\n");
		System.out.println("The origin matrix :");
		printMatrix();
		double sumNegativeElements = defineSumNegativeElements();
		double[] maxElementsString = defineMaxElementsStrings();
		System.out.println("\nThe sum of the negative elements equal : " + sumNegativeElements);
		printMaxElementsStrings(maxElementsString);
		double maxElementMatrix = defineMaxElement(maxElementsString);
		double minElementMatrix = defineMinElement();
		System.out.println("\nThe matrix after rearranging maximum (" + maxElementMatrix + ") and minimum ("
																		+ minElementMatrix + ") elements:");
		rearrangeMaxMinElements(maxElementMatrix, minElementMatrix);
		printMatrix();
	}

	private double defineMinElement() {
		double minElement = Double.MAX_VALUE;
		for (double[] elementsString : matrix) {
			for (double elem : elementsString) {
				minElement = elem < minElement ? elem : minElement;
			}
		}
		return minElement;
	}

	private double defineMaxElement(double[] maxElementsString) {
		double maxElement = Double.MIN_VALUE;
		for (double elem : maxElementsString) {
			maxElement = elem > maxElement ? elem : maxElement;
		}
		return maxElement;
	}

	private void printMaxElementsStrings(double[] maxElementsString) {
		System.out.print("The number of the string - the maximum element of the string : ");
		for (int i = 0; i < maxElementsString.length; i++) {
			System.out.print("[" + i + " - " + maxElementsString[i] + "]");
			if (i < maxElementsString.length - 1) {
				System.out.print(" , ");
			}
		}
	}

	private void rearrangeMaxMinElements(double maxElementMatrix, double minElementMatrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				matrix[i][j] = (matrix[i][j] == maxElementMatrix) ? minElementMatrix
						: (matrix[i][j] == minElementMatrix) ? maxElementMatrix : matrix[i][j];
			}
		}

	}

	private double[] defineMaxElementsStrings() {
		double[] maxElementsString = new double[matrix.length];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				if (matrix[i][j] > maxElementsString[i]) {
					maxElementsString[i] = matrix[i][j];
				}
			}
		}
		return maxElementsString;
	}

	private double defineSumNegativeElements() {
		double sum = 0;
		for (double[] elemString : matrix) {
			for (double elem : elemString) {
				if (elem < 0) {
					sum += elem;
				}
			}
		}
		return sum;
	}

	private void printMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			System.out.print("[ ");
			for (int j = 0; j < matrix[0].length; j++) {
				System.out.printf("%6.2f ", matrix[i][j]);
				if (j < matrix[0].length - 1) {
					System.out.print(", ");
				}
			}
			System.out.print("]\n");
		}
	}
}
