package by.baranov.multidimensionalarrays.task4;

import java.util.Arrays;
import java.util.Random;

public class Task4FirstLastStringsMatrix {
	private double[][] matrix = new double[5][10];
	private Random rand = new Random();

	public void printFirstLastStringsMatrix() {
		System.out.println("\n_TASK-4_\n");
		fillMatrix();
		System.out.println("The origin matrix :\n");
		printMatrix();
		System.out.println("\nThe first and last strings :\n\n" + Arrays.toString(matrix[0]) + "\n"
															  + Arrays.toString(matrix[matrix.length - 1]));

	}
	
	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = rand.nextInt(9);
			}
		}
	}
	
	private void printMatrix() {
		for (double[] elem : matrix) {
			System.out.println(Arrays.toString(elem));
		}
	}
}
