package by.baranov.arrays.task3;

import java.util.Arrays;

public class Task3SequenceNumbers {
	double [] array;

	public Task3SequenceNumbers(double[] array) {
		if(array.length == 0) {
			throw new IllegalArgumentException("invalid input, array can't be empty");
		}
		this.array = array;
	}
	
	public void printFirstMemberInfo() {
		String signFirstMember = defineSignNumbers();
		System.out.println("\n_TASK-3_\n");
		System.out.println(" The first element of sequence " + Arrays.toString(array) + " is : " + signFirstMember);
	}

	private String defineSignNumbers() {
		return array[0] < 0 ? "negative" :"positive";
		
	}
}
