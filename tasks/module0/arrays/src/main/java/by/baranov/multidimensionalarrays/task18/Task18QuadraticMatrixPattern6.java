package by.baranov.multidimensionalarrays.task18;

import java.util.Arrays;

public class Task18QuadraticMatrixPattern6 {
	private int[][] matrix;

	public Task18QuadraticMatrixPattern6(int n) {
		if (n < 1 || n % 2 != 0) {
			throw new IllegalArgumentException("Invalid input, matrix can't have odd quantity of strings and columns, "
					+ "as well as dimension of matrix can't be less than 1");
		}
		this.matrix = new int[n][n];
	}

	public void printQuadraticMatrix() {
		System.out.println("\n_TASK-18_\n");
		System.out.println(
				"For n = " + matrix.length + " the quadratic matrix in accordance with the template 6 has the form : ");
		fillMatrix();
		printMatrix();
	}

	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for(int j = 0; j < matrix.length - i; j++) {
				matrix[i][j] = i + 1;
			}			
		}
	}	

	private void printMatrix() {
		for (int[] elem : matrix) {
			System.out.println(Arrays.toString(elem));
		}
	}
}
