package by.baranov.multidimensionalarrays.task32;

import java.util.Random;

public class Task32MatrixSortStrings {
	private long[][] matrix;
	Random rand = new Random();

	public Task32MatrixSortStrings(int quantityStrings, int quantityColumns) {
		if (quantityStrings < 1 || quantityColumns < 1) {
			throw new IllegalArgumentException("Invalid input, the dimension of the matrix can't be less than 1");
		}
		this.matrix = new long[quantityStrings][quantityColumns];
	}

	public void printMatrixAscendingDescendingStrings() {
		System.out.println("\n\n_TASK-32_\n");
		System.out.println("The origin matrix :");
		fillMatrix();
		printMatrix();
		sortMatrixAscending();
		System.out.println("The matrix with ascending strings : ");
		printMatrix();
		System.out.println("The matrix with descending strings : ");
		sortMatrixDescending();
		printMatrix();
	}

	private void sortMatrixDescending() {
		for (int i = 0; i < matrix.length; i++) {
			sortStringDescendingMatrix(matrix[i]);
		}
	}

	private void sortStringDescendingMatrix(long[] stringMatrix) {
		long tempElement;
		for (int i = 0; i < stringMatrix.length;) {
			if (stringMatrix[i] < stringMatrix[i + 1]) {
				tempElement = stringMatrix[i];
				stringMatrix[i] = stringMatrix[i + 1];
				stringMatrix[i + 1] = tempElement;
				if (i > 0) {
					i--;
				}
			} else if (i < stringMatrix.length - 2) {
				i++;
			} else {
				break;
			}
		}
	}

	private void sortMatrixAscending() {
		for (int i = 0; i < matrix.length; i++) {
			sortStringAscendingMatrix(matrix[i]);
		}
	}

	private void sortStringAscendingMatrix(long[] stringMatrix) {
		long tempElement;
		for (int i = 0; i < stringMatrix.length;) {
			if (stringMatrix[i] > stringMatrix[i + 1]) {
				tempElement = stringMatrix[i];
				stringMatrix[i] = stringMatrix[i + 1];
				stringMatrix[i + 1] = tempElement;
				if (i > 0) {
					i--;
				}
			} else if (i < stringMatrix.length - 2) {
				i++;
			} else {
				break;
			}
		}
	}

	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = rand.nextInt(999);
			}
		}
	}

	private void printMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			System.out.print("[ ");
			for (int j = 0; j < matrix[i].length; j++) {
				System.out.printf("%3d ", matrix[i][j]);
				if (j < matrix.length) {
					System.out.print(", ");
				}
			}
			System.out.print("]\n");
		}
	}
}
