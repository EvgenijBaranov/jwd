package by.baranov.arrays.task4;

import java.util.Arrays;

public class Task4SequenceNumbers {
	double[] array;

	public Task4SequenceNumbers(double[] array) {
		if(array.length == 0) {
			throw new IllegalArgumentException("invalid input, array can't be empty");
		}
		this.array = array;
	}

	public void printSequenceInfo() {
		System.out.println("\n_TASK-4_\n");
		System.out.println(" The sequence " + Arrays.toString(array) + " is increasing : " + isIncreasingSequence());
	}

	private boolean isIncreasingSequence() {		
		for (int i = 1; i < array.length; i++) {
			if (array[i] < array[i - 1]) {
				return false;
			}
		}
		return true;
	}
}
