package by.baranov.arrays.task8;

import java.util.Arrays;

enum SignNumbers {
	POSITIVE, NEGATIVE, ZERO
}

public class Task8StatisticsOfSequence {
	double[] array;

	public Task8StatisticsOfSequence(double[] array) {
		if (array.length == 0) {
			throw new IllegalArgumentException("invalid input, array can't be empty");
		}
		this.array = array;
	}

	public void printStatisticsOfSequence() {
		long quantityPositiveNumbers = defineQuantity(SignNumbers.POSITIVE);
		long quantityNegativeNumbers = defineQuantity(SignNumbers.NEGATIVE);
		long quantityZeroNumbers = defineQuantity(SignNumbers.ZERO);
		System.out.println("\n\n_TASK-8_\n");
		System.out.printf(
				"The sequence " + Arrays.toString(array)
						+ " has:%n %d positive numbers, %d negative numbers, %d zeros%n",
								quantityPositiveNumbers, quantityNegativeNumbers, quantityZeroNumbers);

	}

	private long defineQuantity(SignNumbers typeNumbers) {
		long quantityElem = 0;
		switch (typeNumbers) {
		case POSITIVE:
			for (double elem : array) {
				if (elem > 0) {
					quantityElem++;
				}
			}
			break;
		case NEGATIVE:
			for (double elem : array) {
				if (elem < 0) {
					quantityElem++;
				}
			}
			break;
		case ZERO:
			for (double elem : array) {
				if (elem == 0) {
					quantityElem++;
				}
			}
			break;
		default:
			throw new EnumConstantNotPresentException(SignNumbers.class, typeNumbers.toString());
		}
		return quantityElem;
	}
}
