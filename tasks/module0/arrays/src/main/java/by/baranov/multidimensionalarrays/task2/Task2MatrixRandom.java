package by.baranov.multidimensionalarrays.task2;

import java.util.Arrays;
import java.util.Random;

public class Task2MatrixRandom {
	private double[][] array = new double[2][3];
	private Random rand = new Random();

	public void printMatrix() {
		System.out.println("\n_TASK-2_\n");
		System.out.println("The multidimensional array 2x3 filled with random numbers from 0 to 9 :\n");
		fillMatrix();
		for(double[] elem : array) {
			System.out.println(Arrays.toString(elem));
		}			
	}
	private void fillMatrix() {
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				array[i][j] = rand.nextInt(9);
			}
		}
	}
}
