package by.baranov.multidimensionalarrays.task30;

import java.util.Random;

public class Task30Matrix10x20 {
	private long[][] matrix;
	Random rand = new Random();

	public Task30Matrix10x20(int quantityStrings, int quantityColumns) {
		if (quantityStrings < 1 || quantityColumns < 1) {
			throw new IllegalArgumentException("Invalid input, the dimension of the matrix can't be less than 1");
		}
		this.matrix = new long[quantityStrings][quantityColumns];
	}

	public void printMatrixAndNumbersColumnsCorrespondCondition() {
		System.out.println("\n\n_TASK-30_\n");
		System.out.println("The origin matrix :");
		fillMatrix();
		printMatrix();
		System.out.print("The string numbers, that include the number 5 more than 3 times : ");
		printIndexStringsIncludedNumber5();
	}

	private void printIndexStringsIncludedNumber5() {
		for (int i = 0; i < matrix.length; i++) {
			int counter = 0;
			for (int j = 0; j < matrix.length; j++) {
				counter = matrix[i][j] == 5 ? counter += 1 : counter;
			}
			if(counter == 3){
				System.out.print(i + ", ");
			}			
		}
	}

	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				matrix[i][j] = rand.nextInt(15);
			}
		}
	}

	private void printMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			System.out.print("[ ");
			for (int j = 0; j < matrix.length; j++) {
				System.out.printf("%2d ", matrix[i][j]);
				if (j < matrix.length - 1) {
					System.out.print(", ");
				}
			}
			System.out.print("]\n");
		}
	}
}
