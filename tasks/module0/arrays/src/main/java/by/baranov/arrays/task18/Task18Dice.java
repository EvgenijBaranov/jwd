package by.baranov.arrays.task18;

public class Task18Dice {
	private int number;

	public Task18Dice(int number) {
		if (number < 0 || number > 6) {
			throw new IllegalArgumentException(" the dice can only have an integer number from 1 to 6 ");
		}
		this.number = number;
	}

	public int getNumber() {
		return number;
	}

	@Override
	public String toString() {
		return number + " ";
	}

}
