package by.baranov.multidimensionalarrays.task39;

import java.util.Random;

public class Task39MultiplicationMatrix {
	private long[][] matrixFirst;
	private long[][] matrixSecond;
	private Random rand = new Random();

	public Task39MultiplicationMatrix(int quantityStrings, int quantityColumns) {
		if (quantityStrings < 1 || quantityColumns < 1) {
			throw new IllegalArgumentException("Invalid input, the dimension of the matrix can't be less than 1");
		}
		this.matrixFirst = new long[quantityStrings][quantityColumns];
		this.matrixSecond = new long[quantityColumns][quantityStrings];
	}

	public void printMultiplicationMatrix() {
		System.out.println("\n_TASK-39_\n");
		System.out.println("The origin matrix 1:");
		fillMatrix(matrixFirst);
		printMatrix(matrixFirst);
		System.out.println("The origin matrix 2:");
		fillMatrix(matrixSecond);
		printMatrix(matrixSecond);		
		System.out.print("\nThe result of multiplying two matrices : \n");
		long[][] resultMatrix = multiplicationTwoMatrix();
		printMatrix(resultMatrix);
	}

	private long[][] multiplicationTwoMatrix() {
		long[][] resultMatrix = new long[matrixFirst.length][matrixSecond[0].length];
		for (int i = 0; i < resultMatrix.length; i++) {
			for (int j = 0; j < resultMatrix[0].length; j++) {
				long tempValue = 0;
				for(int k = 0; k < matrixSecond.length; k++) {
					tempValue += matrixFirst[i][k] * matrixSecond[k][j];
				}
				resultMatrix[i][j] = tempValue;						
			}
		}	
		return resultMatrix;
	}

	private void fillMatrix(long[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = rand.nextInt(9);
			}
		}
	}

	private void printMatrix(long[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			System.out.print("[ ");
			for (int j = 0; j < matrix[0].length; j++) {
				System.out.printf("%3d ", matrix[i][j]);
				if (j < matrix[0].length - 1) {
					System.out.print(", ");
				}
			}
			System.out.print("]\n");
		}
	}
}
