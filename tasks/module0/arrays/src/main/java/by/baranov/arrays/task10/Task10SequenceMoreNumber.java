package by.baranov.arrays.task10;

import java.util.Arrays;

public class Task10SequenceMoreNumber {
	long[] array;

	public Task10SequenceMoreNumber(long[] array) {
		if (array.length == 0) {
			throw new IllegalArgumentException("invalid input, array can't be empty");
		}
		this.array = array;
	}

	public void printNumbersMore(long minNumber) {
		System.out.println("\n\n_TASK-10_\n");
		System.out.println("The original sequence : " + Arrays.toString(array));
		System.out.print("the secuence with numbers more then " + minNumber + " :  ");
		for (long elem : array) {
			if (elem >= minNumber) {
				System.out.print(elem + ", ");
			}
		}
	}
}
