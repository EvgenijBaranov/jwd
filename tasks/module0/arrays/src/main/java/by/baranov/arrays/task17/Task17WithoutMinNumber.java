package by.baranov.arrays.task17;

import java.util.Arrays;

public class Task17WithoutMinNumber {
	private double[] sequence;

	public Task17WithoutMinNumber(double[] sequence) {
		if (sequence.length < 1) {
			throw new IllegalArgumentException("The sequence must have numbers more than 1");
		}
		this.sequence = sequence;
	}

	public void printSequenceWithoutMinElements() {
		System.out.println("\n_TASK-17_\n");
		System.out.println("The origin sequence : " + Arrays.toString(sequence));
		double minNumber = defineMinNumber();
		int quantityMinMembers = defineQuantityMinNumbers(minNumber);
		double[] newSequence = sequenceWithoutMinElement(minNumber, quantityMinMembers);
		System.out.println("The sequence without minimum number " + minNumber + " : " + Arrays.toString(newSequence));
	}

	private int defineQuantityMinNumbers(double minNumber) {
		int counter = 0;
		for (double elem : sequence) {
			counter = (elem == minNumber) ? counter += 1 : counter;
		}
		return counter;
	}

	private double[] sequenceWithoutMinElement(double minNumber, int quantityMinMembers) {
		double[] newSequence = new double[sequence.length - quantityMinMembers];
		for (int i = 0, j = 0; i < sequence.length; i++) {
			if (sequence[i] != minNumber) {
				newSequence[j] = sequence[i];
				j++;
			}
		}
		return newSequence;
	}

	private double defineMinNumber() {
		double minNumber = Double.MAX_VALUE;
		for (double elem : sequence) {
			minNumber = (elem < minNumber) ? elem : minNumber;
		}
		return minNumber;
	}
}
