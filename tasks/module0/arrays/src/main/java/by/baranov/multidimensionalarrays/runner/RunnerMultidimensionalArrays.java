package by.baranov.multidimensionalarrays.runner;

import by.baranov.multidimensionalarrays.task1.Task1CreateMatrix3x4;
import by.baranov.multidimensionalarrays.task10.Task10StringColumnMatrix;
import by.baranov.multidimensionalarrays.task11.Task11MatrixPrintStringColumnDifferentWay;
import by.baranov.multidimensionalarrays.task12.Task12QuadraticMatrix;
import by.baranov.multidimensionalarrays.task13.Task13QuadraticMatrixPattern1;
import by.baranov.multidimensionalarrays.task14.Task14QuadraticMatrixPattern2;
import by.baranov.multidimensionalarrays.task15.Task15QuadraticMatrixPattern3;
import by.baranov.multidimensionalarrays.task16.Task16QuadraticMatrixPattern4;
import by.baranov.multidimensionalarrays.task17.Task17QuadraticMatrixPattern5;
import by.baranov.multidimensionalarrays.task18.Task18QuadraticMatrixPattern6;
import by.baranov.multidimensionalarrays.task19.Task19QuadraticMatrixPattern7;
import by.baranov.multidimensionalarrays.task2.Task2MatrixRandom;
import by.baranov.multidimensionalarrays.task20.Task20QuadraticMatrixPattern8;
import by.baranov.multidimensionalarrays.task21.Task21QuadraticMatrixPattern9;
import by.baranov.multidimensionalarrays.task22.Task22QuadraticMatrixPattern10;
import by.baranov.multidimensionalarrays.task23.Task23QuadricMatrixCorrespondRule;
import by.baranov.multidimensionalarrays.task24.Task24QuadraticMatrixCorrespondArray;
import by.baranov.multidimensionalarrays.task25.Task25QuadraticMatrixPattern11;
import by.baranov.multidimensionalarrays.task26.Task26InputMatrixKeyboard;
import by.baranov.multidimensionalarrays.task27.Task27RearrangeColumnsMatrix;
import by.baranov.multidimensionalarrays.task28.Task28SumElementsColumns;
import by.baranov.multidimensionalarrays.task29.Task29PositiveElelmntsDiagonalMatrix;
import by.baranov.multidimensionalarrays.task3.Task3FirstLastColumnMatrix;
import by.baranov.multidimensionalarrays.task30.Task30Matrix10x20;
import by.baranov.multidimensionalarrays.task31.Task31MatrixQuantityDoubleDigit;
import by.baranov.multidimensionalarrays.task32.Task32MatrixSortStrings;
import by.baranov.multidimensionalarrays.task33.Task33MatrixSortColumns;
import by.baranov.multidimensionalarrays.task34.Task34RandomMatrix;
import by.baranov.multidimensionalarrays.task35.Task35FindMaxReplaceOddElemnts;
import by.baranov.multidimensionalarrays.task36.Task36MatrixSmoothing;
import by.baranov.multidimensionalarrays.task37.Task37RearrangeRandomStringsMatrix;
import by.baranov.multidimensionalarrays.task38.Task38SumMatrix;
import by.baranov.multidimensionalarrays.task39.Task39MultiplicationMatrix;
import by.baranov.multidimensionalarrays.task4.Task4FirstLastStringsMatrix;
import by.baranov.multidimensionalarrays.task40.Task40MagicMatrix;
import by.baranov.multidimensionalarrays.task5.Task5EvenStringsMatrix;
import by.baranov.multidimensionalarrays.task6.Task6EvenCulomnsWithCondition;
import by.baranov.multidimensionalarrays.task7.Task7SumModulesNegativeElementsMatrix;
import by.baranov.multidimensionalarrays.task8.Task8MatrixQuantityElementsEqualNumber;
import by.baranov.multidimensionalarrays.task9.Task9SquareMatrix;

public class RunnerMultidimensionalArrays {

	public static void main(String[] args) {
		Task1CreateMatrix3x4 task1 = new Task1CreateMatrix3x4();
		task1.printMatrix();

		Task2MatrixRandom task2 = new Task2MatrixRandom();
		task2.printMatrix();

		Task3FirstLastColumnMatrix task3 = new Task3FirstLastColumnMatrix();
		task3.printFirstLastColumnsMatrix();

		Task4FirstLastStringsMatrix task4 = new Task4FirstLastStringsMatrix();
		task4.printFirstLastStringsMatrix();

		Task5EvenStringsMatrix task5 = new Task5EvenStringsMatrix();
		task5.printEvenStringsMatrix();

		Task6EvenCulomnsWithCondition task6 = new Task6EvenCulomnsWithCondition();
		task6.printEvenStringsMatrix();

		Task7SumModulesNegativeElementsMatrix task7 = new Task7SumModulesNegativeElementsMatrix();
		task7.printSumModulesNegativeNumbersMatrix();

		Task8MatrixQuantityElementsEqualNumber task8 = new Task8MatrixQuantityElementsEqualNumber(3, 4);
		task8.printSumModulesNegativeNumbersMatrix();

		Task9SquareMatrix task9 = new Task9SquareMatrix(5);
		task9.printSumModulesNegativeNumbersMatrix();

		Task10StringColumnMatrix task10 = new Task10StringColumnMatrix(5, 4);
		task10.printStringColumnMatrix(6, 5);

		Task11MatrixPrintStringColumnDifferentWay task11 = new Task11MatrixPrintStringColumnDifferentWay(6, 4);
		task11.printStringColumnMatrix();

		Task12QuadraticMatrix task12 = new Task12QuadraticMatrix(4);
		task12.printQuadraticMatrix();

		Task13QuadraticMatrixPattern1 task13 = new Task13QuadraticMatrixPattern1(8);
		task13.printQuadraticMatrix();

		Task14QuadraticMatrixPattern2 task14 = new Task14QuadraticMatrixPattern2(8);
		task14.printQuadraticMatrix();

		Task15QuadraticMatrixPattern3 task15 = new Task15QuadraticMatrixPattern3(8);
		task15.printQuadraticMatrix();

		Task16QuadraticMatrixPattern4 task16 = new Task16QuadraticMatrixPattern4(8);
		task16.printQuadraticMatrix();

		Task17QuadraticMatrixPattern5 task17 = new Task17QuadraticMatrixPattern5(8);
		task17.printQuadraticMatrix();

		Task18QuadraticMatrixPattern6 task18 = new Task18QuadraticMatrixPattern6(8);
		task18.printQuadraticMatrix();

		Task19QuadraticMatrixPattern7 task19 = new Task19QuadraticMatrixPattern7(8);
		task19.printQuadraticMatrix();

		Task20QuadraticMatrixPattern8 task20 = new Task20QuadraticMatrixPattern8(8);
		task20.printQuadraticMatrix();

		Task21QuadraticMatrixPattern9 task21 = new Task21QuadraticMatrixPattern9(8);
		task21.printQuadraticMatrix();

		Task22QuadraticMatrixPattern10 task22 = new Task22QuadraticMatrixPattern10(8);
		task22.printQuadraticMatrix();

		Task23QuadricMatrixCorrespondRule task23 = new Task23QuadricMatrixCorrespondRule(8);
		task23.printQuadraticMatrix();

		double[] arrayTask24 = { 2, -3, 4, -5, 6 };
		Task24QuadraticMatrixCorrespondArray task24 = new Task24QuadraticMatrixCorrespondArray(arrayTask24);
		task24.printQuadraticMatrix();

		Task25QuadraticMatrixPattern11 task25 = new Task25QuadraticMatrixPattern11(8);
		task25.printQuadraticMatrix();

		double[][] arrayTask26 = { { 2, -3, 4, -5, 6 }, { 23, -5, 14, -10, 6 }, { 22, -2, 43, -5, 16 } };
		Task26InputMatrixKeyboard task26 = new Task26InputMatrixKeyboard(arrayTask26);
		task26.printQuadraticMatrix();

		Task27RearrangeColumnsMatrix task27 = new Task27RearrangeColumnsMatrix(4, 5);
		task27.printAfterRearangeColumns(1, 3);

		Task28SumElementsColumns task28 = new Task28SumElementsColumns(5, 6);
		task28.printSumElementsColumns();

		Task29PositiveElelmntsDiagonalMatrix task29 = new Task29PositiveElelmntsDiagonalMatrix(6);
		task29.printQuadraticMatrix();

		Task30Matrix10x20 task30 = new Task30Matrix10x20(10, 20);
		task30.printMatrixAndNumbersColumnsCorrespondCondition();

		Task31MatrixQuantityDoubleDigit task31 = new Task31MatrixQuantityDoubleDigit(5, 6);
		task31.printMatrixAndQuantityDoubleDigit();

		Task32MatrixSortStrings task32 = new Task32MatrixSortStrings(4, 5);
		task32.printMatrixAndQuantityDoubleDigit();;

		Task33MatrixSortColumns task33 = new Task33MatrixSortColumns(4, 6);
		task33.printMatrixAscendingDescendingStrings();

		Task34RandomMatrix task34 = new Task34RandomMatrix(6, 4);
		task34.printMatrixAscendingDescendingStrings();

		Task35FindMaxReplaceOddElemnts task35 = new Task35FindMaxReplaceOddElemnts(4, 6);
		task35.printChangedMatrix();

		Task36MatrixSmoothing task36 = new Task36MatrixSmoothing(4, 6);
		task36.printSmothedMatrix();
		
		Task37RearrangeRandomStringsMatrix task37 = new Task37RearrangeRandomStringsMatrix(5,7);
		task37.printRearrangedStringsMatrix();
		
		Task38SumMatrix task38 = new Task38SumMatrix(4,5);
		task38.printSumMatrix();
		
		Task39MultiplicationMatrix task39 = new Task39MultiplicationMatrix(3,4);
		task39.printMultiplicationMatrix();
		
		Task40MagicMatrix task40 = new Task40MagicMatrix(9);
		task40.printMagicMatrix();
	}
}
