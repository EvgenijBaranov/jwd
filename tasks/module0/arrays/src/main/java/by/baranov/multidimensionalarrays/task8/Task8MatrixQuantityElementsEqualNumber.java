package by.baranov.multidimensionalarrays.task8;

import java.util.Arrays;
import java.util.Random;

public class Task8MatrixQuantityElementsEqualNumber {
	private double[][] matrix;
	private Random rand = new Random();
	
	public Task8MatrixQuantityElementsEqualNumber(int matrixStrings, int matrixColumns) {
		if (matrixStrings < 1 || matrixColumns < 1) {
			throw new IllegalArgumentException("Invalid input, matrix can't have quantity strings or columns less than 1");
		}
		matrix = new double[matrixStrings][matrixColumns];		
	}

	public void printSumModulesNegativeNumbersMatrix() {
		System.out.println("\n_TASK-8_\n");		
		fillMatrix();
		System.out.println("The origin matrix :\n");
		printMatrix();
		double qyantityNumbers = calculateQuantityNumbers();
		System.out.println("\nThe number 7 is appeared in the matrix : " + qyantityNumbers +" times");
	}

	private double calculateQuantityNumbers() {
		double counter = 0;
		
		for(double[] elementsString : matrix) {
			for(double elem : elementsString) {
				counter = elem == 7 ? counter += 1 : counter;
			}
		}
		return counter;
	}

	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = rand.nextInt(10);
			}
		}
	}

	private void printMatrix() {
		for (double[] elem : matrix) {
			System.out.println(Arrays.toString(elem));
		}
	}
}
