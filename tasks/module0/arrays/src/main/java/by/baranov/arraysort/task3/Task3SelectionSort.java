package by.baranov.arraysort.task3;

import java.util.Arrays;

public class Task3SelectionSort {
	private double[] sequenceNumbers;
	
	public Task3SelectionSort(double[] sequenceNumbers) {
		if (sequenceNumbers.length < 2 ) {
			throw new IllegalArgumentException("Invalid input, sequences can't have less than 2 elements");
		}
		this.sequenceNumbers = sequenceNumbers;		
	}
	
	public void printResultSorting() {
		System.out.println("\n_TASK-3_\n");
		System.out.println("The original increasing sequence: " + Arrays.toString(sequenceNumbers));
		double[] sortedSequence = sortSequence();
		System.out.println("The sorted decreasing sequence (selection sort) - " + Arrays.toString(sortedSequence));
	}
	
	private double[] sortSequence() {
		
		for(int i = 0; i < sequenceNumbers.length; i++) {
			double tempMaxElement = sequenceNumbers[i];
			int indexTempMaxElem = i;
			for(int j = i; j < sequenceNumbers.length; j++) {
				if(sequenceNumbers[j] > tempMaxElement) {
					tempMaxElement = sequenceNumbers[j];
					indexTempMaxElem = j;
				}
			}
			double tempVar = sequenceNumbers[i];
			sequenceNumbers[i] = tempMaxElement;
			sequenceNumbers[indexTempMaxElem] = tempVar;
		}
		return sequenceNumbers;
	}

}
