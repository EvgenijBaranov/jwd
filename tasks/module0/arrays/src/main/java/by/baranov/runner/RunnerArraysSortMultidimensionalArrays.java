package by.baranov.runner;

import by.baranov.arrays.runner.RunnerArrays;
import by.baranov.arraysort.runner.RunnerArraySort;
import by.baranov.multidimensionalarrays.runner.RunnerMultidimensionalArrays;

public class RunnerArraysSortMultidimensionalArrays {
	public static void main(String[] args) {
		RunnerMultidimensionalArrays.main(args);
		RunnerArraySort.main(args);
		RunnerArrays.main(args);
	}
}
