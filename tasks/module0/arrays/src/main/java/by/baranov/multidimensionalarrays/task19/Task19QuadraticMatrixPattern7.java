package by.baranov.multidimensionalarrays.task19;

import java.util.Arrays;

public class Task19QuadraticMatrixPattern7 {
	private int[][] matrix;

	public Task19QuadraticMatrixPattern7(int n) {
		if (n < 1 || n % 2 != 0) {
			throw new IllegalArgumentException("Invalid input, matrix can't have odd quantity of strings and columns, "
					+ "as well as dimension of matrix can't be less than 1");
		}
		this.matrix = new int[n][n];
	}

	public void printQuadraticMatrix() {
		System.out.println("\n_TASK-19_\n");
		System.out.println(
				"For n = " + matrix.length + " the quadratic matrix in accordance with the template 7 has the form : ");
		fillMatrix();
		printMatrix();
	}

	private void fillMatrix() {
		fillUpperPartMatrix();
		fillLowerPartMatrix();

	}

	private void fillUpperPartMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length - i; j++) {
				if (j >= i) {
					matrix[i][j] = 1;
				}
			}
		}
	}

	private void fillLowerPartMatrix() {
		for (int i = matrix.length - 1; i >= 0; i--) {
			for (int j = 0; j < matrix.length - i; j++) {
				if (j >= i) {
					matrix[matrix.length - 1 - i][matrix.length - 1 - j] = 1;
				}
			}
		}
	}

	private void printMatrix() {
		for (int[] elem : matrix) {
			System.out.println(Arrays.toString(elem));
		}
	}
}
