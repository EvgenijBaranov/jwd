package by.baranov.arraysort.task2;

import java.util.Arrays;

public class Task2CombineIncreasingSequences {
	private double[] firstSequence;
	private double[] secondSequence;

	public Task2CombineIncreasingSequences(double[] firstSequence, double[] secondSequence) {
		if (firstSequence.length == 0 || secondSequence.length == 0) {
			throw new IllegalArgumentException("Invalid input, sequences can't be empty");
		}
		this.firstSequence = firstSequence;
		this.secondSequence = secondSequence;
	}

	public void printCombinedSequences() {
		System.out.println("\n_TASK-2_\n");
		System.out.println("The original increasing sequences: first - " + Arrays.toString(firstSequence)
				+ " , second - " + Arrays.toString(secondSequence));
		double[] resultSequence = combineSequence();
		System.out.println("The combined increasing sequence - " + Arrays.toString(resultSequence));
	}

	private double[] combineSequence() {
		double[] arrayMinSize = firstSequence.length < secondSequence.length ? firstSequence : secondSequence;
		double[] arrayMaxSize = firstSequence.length > secondSequence.length ? firstSequence : secondSequence;

		for (int i = 0; i < arrayMaxSize.length; i++) {
			for (int j = 0; j < arrayMinSize.length; j++) {
				if (arrayMaxSize[i] > arrayMinSize[j]) {
					double temp = arrayMaxSize[i];
					arrayMaxSize[i] = arrayMinSize[j];
					arrayMinSize[j] = temp;
				}
			}
		}
		return arrayMaxSize;
	}

}
