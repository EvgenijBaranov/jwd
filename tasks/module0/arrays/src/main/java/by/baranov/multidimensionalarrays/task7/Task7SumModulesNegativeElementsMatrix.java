package by.baranov.multidimensionalarrays.task7;

import java.util.Arrays;
import java.util.Random;

public class Task7SumModulesNegativeElementsMatrix {
	private double[][] matrix = new double[5][5];
	private Random rand = new Random();

	public void printSumModulesNegativeNumbersMatrix() {
		System.out.println("\n_TASK-7_\n");
		
		fillMatrix();
		System.out.println("The origin matrix :\n");
		printMatrix();
		double sum = calculateSumNegativeNumbers();
		System.out.println("\nThe sum of the modules of the negative numbers of the matrix : " + sum);
	}

	private double calculateSumNegativeNumbers() {
		double sum = 0;
		for(double[] elementsString : matrix) {
			for(double elem : elementsString) {
				sum = elem < 0 ? sum += elem : sum;
			}
		}
		return sum;
	}

	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = rand.nextInt(10) - 4;
			}
		}
	}

	private void printMatrix() {
		for (double[] elem : matrix) {
			System.out.println(Arrays.toString(elem));
		}
	}

}
