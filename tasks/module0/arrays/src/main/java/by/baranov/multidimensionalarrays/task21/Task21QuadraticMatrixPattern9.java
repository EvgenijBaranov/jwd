package by.baranov.multidimensionalarrays.task21;

import java.util.Arrays;

public class Task21QuadraticMatrixPattern9 {
	private int[][] matrix;

	public Task21QuadraticMatrixPattern9(int n) {
		if (n < 1 || n % 2 != 0) {
			throw new IllegalArgumentException("Invalid input, matrix can't have odd quantity of strings and columns, "
					+ "as well as dimension of matrix can't be less than 1");
		}
		this.matrix = new int[n][n];
	}

	public void printQuadraticMatrix() {
		System.out.println("\n_TASK-21_\n");
		System.out.println(
				"For n = " + matrix.length + " the quadratic matrix in accordance with the template 9 has the form : ");
		fillMatrix();
		printMatrix();
	}

	private void fillMatrix() {
		int k = matrix.length;		
		for (int i = 0; i < matrix.length; i++, k = k - 1 - i) {
			for (int j = 0; j < matrix.length; j++) {
				if (i >= j) {					
					matrix[i][j] = k++;
				}
			}
		}
	}

	private void printMatrix() {
		for (int[] elem : matrix) {
			System.out.println(Arrays.toString(elem));
		}
	}
}
