package by.baranov.arrays.task13;

import java.util.Arrays;

public class Task13QuantityNumbersInSequence {
	private long[] sequence;

	public Task13QuantityNumbersInSequence(long[] sequence) {
		for (long elem : sequence) {
			if (elem < 0) {
				throw new IllegalArgumentException("The sequence must have only natural numbers");
			}
		}
		this.sequence = sequence;
	}

	public void printQuantityNumbers(double dividedNumber, double lowerLimit, double upperLimit) {
		checkInputData(dividedNumber, lowerLimit, upperLimit);
		System.out.println("\n_TASK-13_\n");
		System.out.println("The origin sequence : " + Arrays.toString(sequence));
		System.out.println("The sequence with numbers, which appropriate to condition : ");
		System.out.println("numbers are divisible by " + dividedNumber + " and lay within from " + lowerLimit + " to "
				+ upperLimit + " : ");
		printNumbersWithCondition(dividedNumber, lowerLimit, upperLimit);
	}

	private void printNumbersWithCondition(double dividedNumber, double lowerLimit, double upperLimit) {
		for (long elem : sequence) {
			if (elem % dividedNumber == 0 && lowerLimit < elem && elem < upperLimit) {
				System.out.print(elem + ", ");
			}
		}
	}

	private void checkInputData(double dividedNumber, double lowerLimit, double upperLimit) {
		if (lowerLimit > upperLimit) {
			throw new IllegalArgumentException("The upper limit must be more than lower limit");
		}
		if (dividedNumber == 0) {
			throw new IllegalArgumentException("The inputted number can't be equal 0");
		}
	}
}
