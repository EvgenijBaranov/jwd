package by.baranov.multidimensionalarrays.task17;

import java.util.Arrays;

public class Task17QuadraticMatrixPattern5 {
	private int[][] matrix;

	public Task17QuadraticMatrixPattern5(int n) {
		if (n < 1 || n % 2 != 0) {
			throw new IllegalArgumentException("Invalid input, matrix can't have odd quantity of strings and columns, "
					+ "as well as dimension of matrix can't be less than 1");
		}
		this.matrix = new int[n][n];
	}

	public void printQuadraticMatrix() {
		System.out.println("\n_TASK-17_\n");
		System.out.println(
				"For n = " + matrix.length + " the quadratic matrix in accordance with the template 5 has the form : ");
		fillMatrix();
		printMatrix();
	}

	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			matrix[i] = (i == 0 || i == matrix.length - 1) ? defineMatrixForFirstLastStrings() : defineMatrixForOtherStrings();
		}
	}

	private int[] defineMatrixForFirstLastStrings() {
		int[] matrixEvenStrings = new int[matrix.length];
		for (int i = 0; i < matrix.length; i++) {
			matrixEvenStrings[i] = 1;
		}
		return matrixEvenStrings;
	}

	private int[] defineMatrixForOtherStrings() {
		int[] matrixOddStrings = new int[matrix.length];		
		for (int i = 0 ; i < matrix.length; i++) {
			matrixOddStrings[i] = (i == 0 || i == matrix.length-1 ) ? 1 : 0 ;			
		}
		return matrixOddStrings;
	}

	private void printMatrix() {
		for (int[] elem : matrix) {
			System.out.println(Arrays.toString(elem));
		}
	}
}
