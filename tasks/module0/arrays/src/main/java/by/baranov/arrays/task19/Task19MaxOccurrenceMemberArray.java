package by.baranov.arrays.task19;

import java.util.Arrays;

public class Task19MaxOccurrenceMemberArray {
	private long[] array;

	public Task19MaxOccurrenceMemberArray(long[] sequence) {
		if (sequence.length < 1) {
			throw new IllegalArgumentException("The sequence must have numbers more than 1");
		}
		this.array = sequence;
	}

	public void printMaxOccurrenceMinElement() {
		System.out.println("\n_TASK-19_\n");
		System.out.println("The origin array : " + Arrays.toString(array));
		int[] frequencyOccurrenceMembers = defineFrequencyOccurrenceMemberArray();
		System.out.println("Array element and frequency of its occurrence : "
				+ printElementAndOccurrenceInArray(array, frequencyOccurrenceMembers));
		int maxQuantityOccurrence = defineMaxNumberOccurrence(frequencyOccurrenceMembers);
		long maxOccurrenceElement = defineMaxOccurenceElement(maxQuantityOccurrence, frequencyOccurrenceMembers);
		System.out.println("Minimum element, which is occurrence most frequency is : " + maxOccurrenceElement
				+ " , its frequency of occurrence : " + maxQuantityOccurrence);
	}

	private long defineMaxOccurenceElement(int maxQuantityOccurrence, int[] frequencyOccurrenceMembers) {
		long maxOccurrenceElement = Long.MAX_VALUE;
		for (int i = 0; i < array.length; i++) {
			if (frequencyOccurrenceMembers[i] == maxQuantityOccurrence) {
				if (array[i] < maxOccurrenceElement) {
					maxOccurrenceElement = array[i];
				}
			}
		}
		return maxOccurrenceElement;
	}

	private int defineMaxNumberOccurrence(int[] frequencyOccurrenceMembers) {
		int maxQuantityOccurence = 0;
		for (int elem : frequencyOccurrenceMembers) {
			if (elem > maxQuantityOccurence) {
				maxQuantityOccurence = elem;
			}
		}
		return maxQuantityOccurence;
	}

	private String printElementAndOccurrenceInArray(long[] arrayElements, int[] frequencyOccurrenceMembers) {
		String result = "[";
		for (int i = 0; i < arrayElements.length; i++) {
			result = result + arrayElements[i] + "-" + frequencyOccurrenceMembers[i];
			if (i < arrayElements.length - 1) {
				result += ", ";
			}
		}
		result += "]";
		return result;
	}

	private int[] defineFrequencyOccurrenceMemberArray() {
		int[] frequencyElements = new int[array.length];
		for (int i = 0; i < array.length; i++) {
			int frequency = defineQuantityNumbers(array[i]);
			frequencyElements[i] = frequency;
		}
		return frequencyElements;
	}

	private int defineQuantityNumbers(double number) {
		int counter = 0;
		for (long elem : array) {
			counter = (elem == number) ? counter += 1 : counter;
		}
		return counter;
	}
}
