package by.baranov.multidimensionalarrays.task11;

import java.util.Arrays;
import java.util.Random;

public class Task11MatrixPrintStringColumnDifferentWay {
	private double[][] matrix;
	private Random rand = new Random();

	public Task11MatrixPrintStringColumnDifferentWay(int numberStrings, int numberColumns) {
		if (numberStrings < 1 || numberColumns < 1) {
			throw new IllegalArgumentException(
					"Invalid input, matrix can't have quantity strings or columns less than 1");
		}
		matrix = new double[numberStrings][numberColumns];
	}

	public void printStringColumnMatrix() {
		System.out.println("\n_TASK-11_\n");
		System.out.println("The origin matrix :\n");
		fillMatrix();
		printMatrix();
		System.out.println("\nPrinting a matrix with some reversed strings : \n");
		for (int i = 0; i < matrix.length; i++) {
			if (i % 2 == 0) {
				printStringReverseOrder(i);
			} else {
				printStringDirectOrder(i);
			}
		}
	}

	private void printStringReverseOrder(int numberString) {
		System.out.print("[");
		for (int j = matrix[numberString].length - 1; j >= 0; j--) {
			System.out.print(matrix[numberString][j]);
			if (j != 0) {
				System.out.print(", ");
			}
		}
		System.out.print("]");
	}

	private void printStringDirectOrder(int numberString) {
		System.out.println("\n"+Arrays.toString(matrix[numberString]));
	}

	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = rand.nextInt(10);
			}
		}
	}

	private void printMatrix() {
		for (double[] elem : matrix) {
			System.out.println(Arrays.toString(elem));
		}
	}
}
