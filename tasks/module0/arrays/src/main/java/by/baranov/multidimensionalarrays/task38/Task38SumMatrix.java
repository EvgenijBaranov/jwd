package by.baranov.multidimensionalarrays.task38;

import java.util.Random;

public class Task38SumMatrix {
	private long[][] matrixFirst;
	private long[][] matrixSecond;
	private Random rand = new Random();

	public Task38SumMatrix(int quantityStrings, int quantityColumns) {
		if (quantityStrings < 1 || quantityColumns < 1) {
			throw new IllegalArgumentException("Invalid input, the dimension of the matrix can't be less than 1");
		}
		this.matrixFirst = new long[quantityStrings][quantityColumns];
		this.matrixSecond = new long[quantityStrings][quantityColumns];
	}

	public void printSumMatrix() {
		System.out.println("\n_TASK-38_\n");
		System.out.println("The origin matrix 1:");
		fillMatrix(matrixFirst);
		printMatrix(matrixFirst);
		System.out.println("The origin matrix 2:");
		fillMatrix(matrixSecond);
		printMatrix(matrixSecond);		
		System.out.print("\nSum of two matrices : \n");
		sumTwoMatrix();
		printMatrix(matrixFirst);
	}

	private void sumTwoMatrix() {
		for (int i = 0; i < matrixFirst.length; i++) {
			for (int j = 0; j < matrixFirst[0].length; j++) {
				matrixFirst[i][j] += matrixSecond[i][j];								
			}
		}		
	}

	private void fillMatrix(long[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = rand.nextInt(9);
			}
		}
	}

	private void printMatrix(long[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			System.out.print("[ ");
			for (int j = 0; j < matrix[0].length; j++) {
				System.out.printf("%2d ", matrix[i][j]);
				if (j < matrix[0].length - 1) {
					System.out.print(", ");
				}
			}
			System.out.print("]\n");
		}
	}
}
