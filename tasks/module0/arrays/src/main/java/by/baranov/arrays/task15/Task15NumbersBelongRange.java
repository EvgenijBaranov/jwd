package by.baranov.arrays.task15;

import java.util.Arrays;

public class Task15NumbersBelongRange {
	private double[] sequence;

	public Task15NumbersBelongRange(double[] sequence) {
		if (sequence.length < 1) {
			throw new IllegalArgumentException("The sequence must have numbers more than 1");
		}
		this.sequence = sequence;
	}

	public void printNumbersBelongRange(double lowerLimit, double upperLimit) {
		checkInputData(lowerLimit, upperLimit);
		System.out.println("\n_TASK-15_\n");
		System.out.println("The origin sequence : " + Arrays.toString(sequence));
		System.out.print("The numbers within bounds : [" + lowerLimit + " , " + upperLimit + "] are ");
		printNumbersWithinRange(lowerLimit, upperLimit);
	}

	private void printNumbersWithinRange(double lowerLimit, double upperLimit) {
		for (double elem : sequence) {
			if (lowerLimit < elem && elem < upperLimit) {
				System.out.print(elem + ", ");
			}
		}
	}

	private void checkInputData(double lowerLimit, double upperLimit) {
		if (lowerLimit > upperLimit) {
			throw new IllegalArgumentException("The upper limit must be more than lower limit");
		}
	}
}
