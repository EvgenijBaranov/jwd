package by.baranov.multidimensionalarrays.task28;

import java.util.Random;

public class Task28SumElementsColumns {
	private double[][] matrix;
	private Random rand = new Random();

	public Task28SumElementsColumns(int quantityStrings, int quantityColumns) {
		if (quantityStrings < 1 || quantityColumns < 1) {
			throw new IllegalArgumentException("Invalid input, the dimension of the matrix can't be less than 1");
		}
		this.matrix = new double[quantityStrings][quantityColumns];
	}

	public void printSumElementsColumns() {
		System.out.println("\n_TASK-28_\n");
		System.out.println("The origin matrix :");
		fillMatrix();
		printMatrix();
		System.out.print("\nThe sum of column elements:");
		double[] sumColumnElements = defineSumElementsColumns();
		printSumColumnElements(sumColumnElements);
		double maxSumElementsColumns = defineMaxSumElementsColumn(sumColumnElements);
		System.out.println("\nThe maximum sum of the column elements : " + maxSumElementsColumns);

	}

	private double defineMaxSumElementsColumn(double[] sumColumnElements) {
		double maxElement = Double.MIN_VALUE;
		for(double elem : sumColumnElements){
			if(elem > maxElement){
				maxElement = elem;
			}
		}
		return maxElement;
	}

	private double[] defineSumElementsColumns() {
		double[] sumElementsColumns = new double[matrix.length];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				sumElementsColumns[i] += matrix[i][j];
			}
		}
		return sumElementsColumns;
	}

	private void printSumColumnElements(double[] sumColumnElements) {
		System.out.print(" [index of the column - sum of elements column] : \n");
		for (int i = 0; i < sumColumnElements.length; i++) {
			System.out.print("[ " + i + " - " + sumColumnElements[i] + " ]");
			if(i < sumColumnElements.length -1){
				System.out.print(", ");
			}
		}
	}

	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = rand.nextInt(9);
			}
		}
	}

	private void printMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			System.out.print("[ ");
			for (int j = 0; j < matrix[0].length; j++) {
				System.out.printf("%6.2f ", matrix[i][j]);
				if (j < matrix[0].length - 1) {
					System.out.print(", ");
				}
			}
			System.out.print("]\n");
		}
	}
}
