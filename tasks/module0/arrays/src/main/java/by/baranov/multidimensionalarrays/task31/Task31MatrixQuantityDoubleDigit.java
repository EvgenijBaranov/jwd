package by.baranov.multidimensionalarrays.task31;

import java.util.Random;

public class Task31MatrixQuantityDoubleDigit {
	private long[][] matrix;
	Random rand = new Random();

	public Task31MatrixQuantityDoubleDigit(int quantityStrings, int quantityColumns) {
		if (quantityStrings < 1 || quantityColumns < 1) {
			throw new IllegalArgumentException("Invalid input, the dimension of the matrix can't be less than 1");
		}
		this.matrix = new long[quantityStrings][quantityColumns];
	}

	public void printMatrixAndQuantityDoubleDigit() {
		System.out.println("\n\n_TASK-31_\n");
		System.out.println("The origin matrix :");
		fillMatrix();
		printMatrix();
		long quantityDoubleDigit = defineQuantityDoubleDigit();
		System.out.print("The quantity double digits in the matrix : " + quantityDoubleDigit);
}

	private long defineQuantityDoubleDigit() {
		long quantityDoubleDigit = 0;
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				quantityDoubleDigit = (1 <= matrix[i][j] / 10 && matrix[i][j] / 10 < 10) ? quantityDoubleDigit += 1 : quantityDoubleDigit;
			}
		}
		return quantityDoubleDigit;
	}
	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				matrix[i][j] = rand.nextInt(999);
			}
		}
	}

	private void printMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			System.out.print("[ ");
			for (int j = 0; j < matrix.length; j++) {
				System.out.printf("%3d ", matrix[i][j]);
				if (j < matrix.length - 1) {
					System.out.print(", ");
				}
			}
			System.out.print("]\n");
		}
	}
}
