package by.baranov.arrays.task7;

import java.util.Arrays;

public class Task7ChangeNumbers {
	double[] array;

	public Task7ChangeNumbers(double[] array) {
		if (array.length == 0) {
			throw new IllegalArgumentException("invalid input, array can't be empty");
		}
		this.array = array;
	}

	public void printRestrictedArray(double maxNumber) {
		double[] restrictedArray = defineRestrictedArray(maxNumber);
		System.out.println("\n_TASK-7_\n");
		System.out.printf(
				"The original array " + Arrays.toString(array) + "\n"
						+ "new restricted array with elements less than %-5.2f : " + Arrays.toString(restrictedArray), maxNumber);
	}

	private double[] defineRestrictedArray(double maxNumber) {
		double[] newArray = new double[array.length];
		for (int i = 0; i < array.length; i++) {
			if (array[i] > maxNumber) {
				newArray[i] = maxNumber;
			} else {
				newArray[i] = array[i];
			}
		}
		return newArray;
	}

}
