package by.baranov.arraysort.task1;

import java.util.Arrays;

public class Task1CombineTwoArrays {
	private double[] arrayBig;
	private double[] arraySmall;

	public Task1CombineTwoArrays(double[] arrayBig, double[] arraySmall) {
		checkInputData(arrayBig, arraySmall);
		this.arrayBig = arrayBig;
		this.arraySmall = arraySmall;
	}

	public void printCombinedArray(int indexBigArrayForInput) {
		checkPositionInput(indexBigArrayForInput);
		System.out.println("\n\n\n_____ARRAY_SORT____\n\n");
		System.out.println("\n_TASK-1_\n");
		System.out.println("The original arrays : big array - " + Arrays.toString(arrayBig) + " , small array - " + Arrays.toString(arraySmall));
		System.out.println("The position of the big array for input the small array: " + indexBigArrayForInput);
		double[] combinedArray = combineArrays(indexBigArrayForInput);
		System.out.println("The combined array : " + Arrays.toString(combinedArray));

	}

	private void checkPositionInput(int indexBigArrayForInput) {
		if (arrayBig.length - arraySmall.length <= indexBigArrayForInput) {
			throw new IllegalArgumentException("Invalid input, for such an input position the small arrays can't be located in a large array");
		}
	}

	private double[] combineArrays(int indexBigArrayForInput) {
		int shiftMembersBigArray = arraySmall.length;
		for (int i = indexBigArrayForInput + 1, j = 0; i < arrayBig.length; i++, j++) {
			if (i + shiftMembersBigArray < arrayBig.length) {
				arrayBig[i + shiftMembersBigArray] = arrayBig[i];				
			}
			if(j < arraySmall.length) {
				arrayBig[i] = arraySmall[j];
			}			
		}
		return arrayBig;
	}

	private void checkInputData(double[] arrayBig, double[] arraySmall) {
		if (arrayBig.length == 0 || arraySmall.length == 0) {
			throw new IllegalArgumentException("Invalid input, arrays can't be empty");
		}
		if (arrayBig.length <= arraySmall.length) {
			throw new IllegalArgumentException("Invalid input, the first array must be more than second");
		}

	}
}
