package by.baranov.multidimensionalarrays.task23;

public class Task23QuadricMatrixCorrespondRule {
	private double[][] matrix;

	public Task23QuadricMatrixCorrespondRule(int n) {
		if (n < 1) {
			throw new IllegalArgumentException("Invalid input, the dimension of matrix can't be less than 1");
		}
		this.matrix = new double[n][n];
	}

	public void printQuadraticMatrix() {
		System.out.println("\n_TASK-23_\n");
		System.out.println("For N = " + matrix.length
				+ " the quadratic matrix in accordance with the rule A[I,J] = sin((I^2-J^2)/N) has the form : ");
		fillMatrix();
		printMatrix();
	}

	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				matrix[i][j] = Math.sin((Math.pow(i, 2) - Math.pow(j, 2)) / matrix.length);
			}
		}
	}

	private void printMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			System.out.print("[ ");
			for (int j = 0; j < matrix.length; j++) {
				System.out.printf("%5.2f ", matrix[i][j]);
				if (j < matrix.length - 1) {
					System.out.print(", ");
				}
			}
			System.out.print("]\n");
		}
	}
}
