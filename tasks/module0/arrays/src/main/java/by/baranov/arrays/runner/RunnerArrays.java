package by.baranov.arrays.runner;

import by.baranov.arrays.task1.Task1Array;
import by.baranov.arrays.task10.Task10SequenceMoreNumber;
import by.baranov.arrays.task11.Task11NumbersConditionFromSequence;
import by.baranov.arrays.task12.Task12SumPrimeNumbers;
import by.baranov.arrays.task13.Task13QuantityNumbersInSequence;
import by.baranov.arrays.task14.Task14SumMaxEvenMemberMinOddMember;
import by.baranov.arrays.task15.Task15NumbersBelongRange;
import by.baranov.arrays.task16.Task16MaxSumDefinedMembers;
import by.baranov.arrays.task17.Task17WithoutMinNumber;
import by.baranov.arrays.task18.Task18SuperLock;
import by.baranov.arrays.task19.Task19MaxOccurrenceMemberArray;
import by.baranov.arrays.task2.Task2ArrayIndexZeroElements;
import by.baranov.arrays.task20.Task20CompressArray;
import by.baranov.arrays.task3.Task3SequenceNumbers;
import by.baranov.arrays.task4.Task4SequenceNumbers;
import by.baranov.arrays.task5.Task5FindEvenNumbers;
import by.baranov.arrays.task6.Task6MinLengthBetweenNumbers;
import by.baranov.arrays.task7.Task7ChangeNumbers;
import by.baranov.arrays.task8.Task8StatisticsOfSequence;
import by.baranov.arrays.task9.Task9ChangeMaxMinElements;

public class RunnerArrays {

	public static void main(String[] args) {

		long[] arrayTask1 = { 14, 24, 42, 54, 48 };
		Task1Array task1 = new Task1Array(arrayTask1);
		task1.printSumMultipleMembers(6);

		long[] arrayTask2 = { 14, 0, 4, 0, 48, 0, 34, 0, 0 };
		Task2ArrayIndexZeroElements task2 = new Task2ArrayIndexZeroElements(arrayTask2);
		task2.printArrayWithIndexZeroElement();

		double[] arrayTask3 = { 14, 0, 4, 0, 48.3, 0, 34, 0, 0 };
		Task3SequenceNumbers task3 = new Task3SequenceNumbers(arrayTask3);
		task3.printFirstMemberInfo();

		double[] arrayTask4 = { 1, 3, 4, 13, 13, 50, 60, 89, 90, 100 };
		Task4SequenceNumbers task4 = new Task4SequenceNumbers(arrayTask4);
		task4.printSequenceInfo();

		double[] arrayTask5 = { 1, 3, 4, 13, 13, 50, 60, 89, 90, 100 };
		Task5FindEvenNumbers task5 = new Task5FindEvenNumbers(arrayTask5);
		task5.printArrayEvenNumbers();

		double[] arrayTask6 = { 1, 3, 4, 13, 13, 100, 60, 89, 90, 50 };
		Task6MinLengthBetweenNumbers task6 = new Task6MinLengthBetweenNumbers(arrayTask6);
		task6.printMinLengthBetweenNumbers();

		double[] arrayTask7 = { 1, 3, 4, 13, 13, 100, 60, 89, 90, 50 };
		Task7ChangeNumbers task7 = new Task7ChangeNumbers(arrayTask7);
		task7.printRestrictedArray(60);

		double[] arrayTask8 = { -1, 0, -4, -13, 0, -100, 60, 89, 90, 50, -70, 0 };
		Task8StatisticsOfSequence task8 = new Task8StatisticsOfSequence(arrayTask8);
		task8.printStatisticsOfSequence();

		double[] arrayTask9 = { -1, 0, -4, -13, 0, -100, 60, 89, 90, 50, -70, 0 };
		Task9ChangeMaxMinElements task9 = new Task9ChangeMaxMinElements(arrayTask9);
		task9.printChangedSequence();

		long[] arrayTask10 = { -1, 0, -4, -13, 0, -100, 60, 89, 90, 50, -70, 0 };
		Task10SequenceMoreNumber task10 = new Task10SequenceMoreNumber(arrayTask10);
		task10.printNumbersMore(0);

		long[] sequenceTask11 = { 12, 25, 4, 5, 1, 63, 99 };
		Task11NumbersConditionFromSequence task11 = new Task11NumbersConditionFromSequence(sequenceTask11);
		task11.printNumbersWithCondition(5);

		double[] arrayTask12 = { 12, 25, 4, 5, 1, 63, 99 };
		Task12SumPrimeNumbers task12 = new Task12SumPrimeNumbers(arrayTask12);
		task12.printSumPrimeNumbers();

		long[] arrayTask13 = { 12, 25, 4, 5, 1, 63, 99 };
		Task13QuantityNumbersInSequence task13 = new Task13QuantityNumbersInSequence(arrayTask13);
		task13.printQuantityNumbers(5, 4, 30);

		double[] arrayTask14 = { 12, 25, 4, 5, 1, 63, 99 };
		Task14SumMaxEvenMemberMinOddMember task14 = new Task14SumMaxEvenMemberMinOddMember(arrayTask14);
		task14.printSumMaxEvenMinOddNumbers();

		double[] arrayTask15 = { 12, 25, 4, 5, 1, 63, 99 };
		Task15NumbersBelongRange task15 = new Task15NumbersBelongRange(arrayTask15);
		task15.printNumbersBelongRange(10, 30);

		double[] arrayTask16 = { 12, 25, 4, 5, 1, 63, 99, 200 };
		Task16MaxSumDefinedMembers task16 = new Task16MaxSumDefinedMembers(arrayTask16);
		task16.printNumbersBelongRange();

		double[] arrayTask17 = { -2, 1, 12, 25, 4, 5, -2, 1, 63, 99, 200, -2 };
		Task17WithoutMinNumber task17 = new Task17WithoutMinNumber(arrayTask17);
		task17.printSequenceWithoutMinElements();		
		
		long[] arrayTask19 = { 0, 1, 1, 12, 12, 25, 4, 5, 1, 63, 99, 12, 25, 25 };
		Task19MaxOccurrenceMemberArray task19 = new Task19MaxOccurrenceMemberArray(arrayTask19);
		task19.printMaxOccurrenceMinElement();
		
		double[] arrayTask20 = { -2, 1, 12, 25, 4, 5, -2, 1, 63, 99, 200 };
		Task20CompressArray  task20 = new Task20CompressArray(arrayTask20); 
		task20.printCompressArray();
		
		Task18SuperLock task18 = new Task18SuperLock();
		task18.trySolve();

	}
}
