package by.baranov.arrays.task18;

import java.util.Arrays;
import java.util.Scanner;

public class Task18SuperLock {
	private Task18Dice[] sequenceDices = new Task18Dice[10];
	private Task18Dice[] rightSequence = { new Task18Dice(3), new Task18Dice(4), new Task18Dice(3), new Task18Dice(4),
											new Task18Dice(3), new Task18Dice(4), new Task18Dice(3), new Task18Dice(4),
											  new Task18Dice(3), new Task18Dice(4) };
	private int indexFirstEmptyDiceSequence;
	private int defaultValueIndex = 2;

	public Task18SuperLock() {
		sequenceDices[0] = new Task18Dice(3);
		sequenceDices[1] = new Task18Dice(4);
		indexFirstEmptyDiceSequence = defaultValueIndex;
	}

	public void trySolve() {
		System.out.println("\n_TASK-18_\n");
		int numberInput = 0;
		boolean isWrongSequenceDices = true;
		System.out.println("You try to open the lock.\n");
		while (isWrongSequenceDices) {
			System.out.printf(
					"You need enter %d dices with numbers from %d to %d. %n"
							+ "Each dice can have only an integer number from 1 to 6 %n%n (to open the lock input next 3 4 3 4 3 4 3 4 )",
									sequenceDices.length - defaultValueIndex, defaultValueIndex, sequenceDices.length - 1);
			for (int i = defaultValueIndex; i < sequenceDices.length; i++) {
				System.out.print("Enter " + (i - defaultValueIndex) + "th dice: ");
				Scanner scan = new Scanner(System.in);
				String resultInput = scan.next();
				try {
					numberInput = Integer.parseInt(resultInput);
					checkParseValue(numberInput);
					putDice(new Task18Dice(numberInput));
				} catch (NumberFormatException e) {
					System.out.println("You have a mistake somewhere , dice can have only an integer number from 1 to 6");
					i--;
				}
			}
			if (checkSequenceDices()) {
				isWrongSequenceDices = false;
			} else {
				System.out.println("\n Unfortunately, Your attempt failed. Try again! \n");
			}
		}
		System.out.println("You win, lock is open !");
		System.out.println("Right sequence : " + Arrays.toString(rightSequence));
		System.out.println("Your sequence  : " + Arrays.toString(sequenceDices));
	}

	private boolean checkSequenceDices() {
		for (int i = 0; i < rightSequence.length; i++) {
			if (sequenceDices[i].getNumber() != rightSequence[i].getNumber()) {
				indexFirstEmptyDiceSequence = defaultValueIndex;
				return false;
			}
		}
		indexFirstEmptyDiceSequence = defaultValueIndex;
		return true;
	}

	private void putDice(Task18Dice dice) {
		sequenceDices[indexFirstEmptyDiceSequence] = dice;
		indexFirstEmptyDiceSequence++;
	}

	private void checkParseValue(int numberInput) {
		if (numberInput < 0 || numberInput > 6) {
			throw new NumberFormatException(" the dice can only have an integer number from 1 to 6 ");
		}
	}
}
