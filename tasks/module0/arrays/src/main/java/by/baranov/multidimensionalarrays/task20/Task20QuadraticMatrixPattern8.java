package by.baranov.multidimensionalarrays.task20;

import java.util.Arrays;

public class Task20QuadraticMatrixPattern8 {
	private int[][] matrix;

	public Task20QuadraticMatrixPattern8(int n) {
		if (n < 1 || n % 2 != 0) {
			throw new IllegalArgumentException("Invalid input, matrix can't have odd quantity of strings and columns, "
					+ "as well as dimension of matrix can't be less than 1");
		}
		this.matrix = new int[n][n];
	}

	public void printQuadraticMatrix() {
		System.out.println("\n_TASK-20_\n");
		System.out.println(
				"For n = " + matrix.length + " the quadratic matrix in accordance with the template 8 has the form : ");
		fillMatrix();
		printMatrix();
	}

	private void fillMatrix() {
		fillLeftPartMatrix();
		fillRightPartMatrix();
	}

	private void fillLeftPartMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length - i; j++) {
				if (j <= i) {
					matrix[i][j] = 1;
				}
			}
		}
	}

	private void fillRightPartMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length - i; j++) {
				if (j <= i) {
					matrix[matrix.length - 1 - i][matrix.length - 1 - j] = 1;
				}
			}
		}
	}

	private void printMatrix() {
		for (int[] elem : matrix) {
			System.out.println(Arrays.toString(elem));
		}
	}
}
