package by.baranov.multidimensionalarrays.task35;

import java.util.Random;

public class Task35FindMaxReplaceOddElemnts {
	private long[][] matrix;
	private Random rand = new Random();

	public Task35FindMaxReplaceOddElemnts(int quantityStrings, int quantityColumns) {
		if (quantityStrings < 1 || quantityColumns < 1) {
			throw new IllegalArgumentException("Invalid input, the dimension of the matrix can't be less than 1");
		}
		this.matrix = new long[quantityStrings][quantityColumns];
	}

	public void printChangedMatrix() {
		System.out.println("\n_TASK-35_\n");
		System.out.println("The origin matrix :");
		fillMatrix();
		printMatrix();
		long maxElement = defineMaxElement();
		System.out.print("\nThe maximum element of the matrix : " + maxElement);		
		replaceOddElements(maxElement);
		System.out.println("\nChanging the odd matrix elements on " + maxElement + ": ");
		printMatrix();
	}

	private void replaceOddElements(long maxElement) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				if(matrix[i][j] % 2 != 0) {
					matrix[i][j] = maxElement;
				}				
			}
		}		
	}

	private long defineMaxElement() {
		long maxElement = Long.MIN_VALUE;
		for(long[] elemString : matrix){
			for(long elem : elemString) {
				if(elem > maxElement){
					maxElement = elem;
				}
			}			
		}
		return maxElement;
	}
	
	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = rand.nextInt(9);
			}
		}
	}

	private void printMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			System.out.print("[ ");
			for (int j = 0; j < matrix[0].length; j++) {
				System.out.printf("%2d ", matrix[i][j]);
				if (j < matrix[0].length - 1) {
					System.out.print(", ");
				}
			}
			System.out.print("]\n");
		}
	}
}
