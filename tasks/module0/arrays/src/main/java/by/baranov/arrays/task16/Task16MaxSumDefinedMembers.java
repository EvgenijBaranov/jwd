package by.baranov.arrays.task16;

import java.util.Arrays;

public class Task16MaxSumDefinedMembers {
	private double[] sequence;

	public Task16MaxSumDefinedMembers(double[] sequence) {
		if (sequence.length < 1) {
			throw new IllegalArgumentException("The sequence must have numbers more than 1");
		}
		if (sequence.length % 2 != 0) {
			throw new IllegalArgumentException("The sequence must have even numbers");
		}
		
		this.sequence = sequence;
	}

	public void printNumbersBelongRange() {
		System.out.println("\n\n_TASK-16_\n");
		System.out.println("The origin sequence [a(1), a(2), ... ,a(n),]: " + Arrays.toString(sequence));
		System.out.println("We are looking for : max((a(1) + a(n)), (a(2) + a(n-1)), ... , (a(n/2) + a(n/2+1))");
		System.out.println("The result of the maximum sum function : " + defineMaxSum());
	}

	private double defineMaxSum() {
		double maxSum = 0;
		for(int i = 0, j = sequence.length - 1; i < sequence.length / 2 ; i++, j--){
			if(sequence[i] + sequence[j] > maxSum){
				maxSum = sequence[i] + sequence[j];
			}
		}
		return maxSum;
	}

	
}
