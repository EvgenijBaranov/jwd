package by.baranov.arraysort.runner;

import by.baranov.arraysort.task1.Task1CombineTwoArrays;
import by.baranov.arraysort.task2.Task2CombineIncreasingSequences;
import by.baranov.arraysort.task3.Task3SelectionSort;
import by.baranov.arraysort.task4.Task4BubbleSort;
import by.baranov.arraysort.task5.Task5InsertionSort;
import by.baranov.arraysort.task6.Task6ShellSort;

public class RunnerArraySort {
	
	public static void main(String[] args) {
		
		double[] firstArraySortTask1 = { 14, 0, 4, 1, 48, 2, 34, 3, 6, 5 };
		double[] secondArraySortTask1 = { 5.323, 4.214, 34.234, 2.567 };
		Task1CombineTwoArrays task1 = new Task1CombineTwoArrays(firstArraySortTask1, secondArraySortTask1);
		task1.printCombinedArray(3);
		
		double[] firstSequenceTask2 = { 1, 3, 5, 7, 9, 11, 13, 15, 17, 19 };
		double[] secondSequenceTask2 = { 2, 4, 6, 8 };
		Task2CombineIncreasingSequences task2 = new Task2CombineIncreasingSequences(firstSequenceTask2, secondSequenceTask2);
		task2.printCombinedSequences();
		
		double[] sequenceTask3 = { 1, 3, 5, 7, 9, 11, 13, 15, 19, 19 };
		Task3SelectionSort task3 = new Task3SelectionSort (sequenceTask3);
		task3.printResultSorting();
		
		double[] sequenceTask4 = { 1, 3, 5, 7, 9, 11, 13, 15, 19, 19 };
		Task4BubbleSort task4 = new Task4BubbleSort (sequenceTask4);
		task4.printResultSorting();
		
		double[] sequenceTask5 = { 19, 18, 17, 16, 15, 8, 14, 13, 12, 11, 10 };
		Task5InsertionSort task5 = new Task5InsertionSort (sequenceTask5);
		task5.printResultSorting();
		
		double[] sequenceTask6 = { 12, 25.1, 4, 5, 1, 63, 99, 1, 0 };
		Task6ShellSort task6 = new Task6ShellSort(sequenceTask6);
		task6.printSortedArray();
		
	}
}
