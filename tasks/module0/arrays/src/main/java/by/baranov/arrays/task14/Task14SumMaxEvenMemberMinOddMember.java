package by.baranov.arrays.task14;

import java.util.Arrays;

public class Task14SumMaxEvenMemberMinOddMember {
	private double[] sequence;

	public Task14SumMaxEvenMemberMinOddMember(double[] sequence) {
		if (sequence.length < 1) {
			throw new IllegalArgumentException("The sequence must have numbers more than 1");
		}
		this.sequence = sequence;
	}

	public void printSumMaxEvenMinOddNumbers() {
		System.out.println("\n\n_TASK-14_\n");
		System.out.println("The origin sequence : " + Arrays.toString(sequence));
		double sumPrimeNumbers = defineMaxEvenMember() + definMinOddMember();
		System.out.println("Sum of the maximum number of even members " + defineMaxEvenMember()
				+ " and the minimum number of the odd members " + definMinOddMember() + " is equal : " + sumPrimeNumbers);
	}

	private double defineMaxEvenMember() {
		double maxEvenMember = 0;
		for (int i = 0; i < sequence.length; i = i + 2) {
			if (sequence[i] > maxEvenMember) {
				maxEvenMember = sequence[i];
			}
		}
		return maxEvenMember;
	}

	private double definMinOddMember() {
		double minOddMember = Double.MAX_VALUE;
		for (int i = 1; i < sequence.length; i = i + 2) {
			if (sequence[i] < minOddMember) {
				minOddMember = sequence[i];
			}
		}
		return minOddMember;
	}
}
