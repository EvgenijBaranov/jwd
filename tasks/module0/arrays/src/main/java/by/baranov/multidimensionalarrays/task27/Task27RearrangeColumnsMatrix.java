package by.baranov.multidimensionalarrays.task27;

import java.util.Random;

public class Task27RearrangeColumnsMatrix {
	private double[][] matrix;
	private Random rand = new Random();

	public Task27RearrangeColumnsMatrix(int quantityStrings, int quantityColumns) {
		if (quantityStrings < 1 || quantityColumns < 1) {
			throw new IllegalArgumentException("Invalid input, the dimension of the matrix can't be less than 1");
		}
		this.matrix = new double[quantityStrings][quantityColumns];
	}

	public void printAfterRearangeColumns( int firstColumn, int secondColumn) {
		checkInputData(firstColumn, secondColumn);
		System.out.println("\n_TASK-27_\n");
		System.out.println("The origin matrix :");
		fillMatrix();
		printMatrix();
		System.out.print("\nThe column "+firstColumn+" is rearranged with " + secondColumn + " column. ");
		System.out.println("The matrix after rearranging colluns:");
		rearrangeColumns(firstColumn, secondColumn);
		printMatrix();
	}

	private void checkInputData(int firstColumn, int secondColumn) {
		if(firstColumn >= matrix[0].length || secondColumn >= matrix[0].length) {
			throw new IllegalArgumentException("Invalid input, the matrix has a dimension less than the indices of the entered columns");
		}		
	}

	private void rearrangeColumns(int firstColumn, int secondColumn) {
		double tempElement = 0;
		for (int i = 0; i < matrix.length; i++) {			
				tempElement = matrix[i][firstColumn];
				matrix[i][firstColumn] = matrix[i][secondColumn];
				matrix[i][secondColumn] = tempElement;
		}
	}	

	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = rand.nextInt(9);
			}
		}
	}
	
	private void printMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			System.out.print("[ ");
			for (int j = 0; j < matrix[0].length; j++) {
				System.out.printf("%6.2f ", matrix[i][j]);
				if (j < matrix[0].length - 1) {
					System.out.print(", ");
				}
			}
			System.out.print("]\n");
		}
	}
}
