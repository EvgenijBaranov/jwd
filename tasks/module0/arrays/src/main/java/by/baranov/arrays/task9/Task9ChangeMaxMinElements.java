package by.baranov.arrays.task9;

import java.util.Arrays;

public class Task9ChangeMaxMinElements {
	double[] array;

	public Task9ChangeMaxMinElements(double[] array) {
		checkInputData(array);
		this.array = array;
	}

	public void printChangedSequence() {
		double[] changedArray = defineChangedArray(array);

		System.out.println("\n_TASK-9_\n");
		System.out.printf("The original sequence " + Arrays.toString(array) + "\n"
				+ "the modified sequence with rearranged minimum and maximum elements "
				+ Arrays.toString(changedArray));
	}

	private double[] defineChangedArray(double[] oldArray) {
		double[] sortedArray = oldArray.clone();
		Arrays.sort(sortedArray);
		double[] changedArray = oldArray.clone();
		for (int i = 0; i < changedArray.length; i++) {
			changedArray[i] = changedArray[i] == sortedArray[0] ? sortedArray[sortedArray.length - 1]
					: changedArray[i] == sortedArray[sortedArray.length - 1] ? sortedArray[0] : changedArray[i];
		}
		return changedArray;
	}

	private void checkInputData(double[] array) {
		if (array.length < 2) {
			throw new IllegalArgumentException("invalid input, array can't has less than 2 elements");
		}
		double[] sortedArray = array.clone();
		Arrays.sort(sortedArray);
		if (sortedArray[0] == sortedArray[1]
				|| sortedArray[sortedArray.length - 1] == sortedArray[sortedArray.length - 2]) {
			throw new IllegalArgumentException("invalid input, maximum and minimum elements of array must be unique");
		}
	}
}
