package by.baranov.multidimensionalarrays.task10;

import java.util.Arrays;
import java.util.Random;

public class Task10StringColumnMatrix {
	private double[][] matrix;
	private int stringPrint;
	private int columnPrint;
	private Random rand = new Random();

	public Task10StringColumnMatrix(int numberStrings, int numberColumns) {
		if (numberStrings < 1 || numberColumns < 1) {
			throw new IllegalArgumentException(
					"Invalid input, matrix can't have quantity strings or columns less than 1");
		}
		matrix = new double[numberStrings][numberColumns];
	}

	public void printStringColumnMatrix(int numberStringPrint, int numberColumnPrint) {
		System.out.println("\n_TASK-10_\n");
		checkInputData(numberStringPrint, numberColumnPrint);
		System.out.println("\nThe origin matrix :\n");
		fillMatrix();
		printMatrix();
		System.out.println("\nThe string number : " + stringPrint + "\n" + Arrays.toString(matrix[stringPrint]));
		printColumn(columnPrint);
	}

	private void checkInputData(int numberStringPrint, int numberColumnPrint) {
		if (numberStringPrint > matrix.length - 1 || numberStringPrint < 1) {
			System.out.println("The inputted number of the string is out of bounds,"
					+ "so number of the string is assumed : " + (matrix.length - 1));
			this.stringPrint = matrix.length - 1;
		} else {
			this.stringPrint = numberStringPrint;
		}

		if (numberColumnPrint > matrix[0].length - 1 || numberColumnPrint < 1) {
			System.out.println("The inputted number of the column is out of bounds, "
					+ "so the number of the column is assumed : " + (matrix[matrix.length - 1].length - 1));
			this.columnPrint = matrix[0].length - 1;
		} else {
			this.columnPrint = numberColumnPrint;
		}
	}

	private void printColumn(int columnPrint) {
		System.out.println("\nThe column number : " + columnPrint);
		for(double[] elem : matrix) {
			System.out.println(elem[columnPrint]);
		}
	}

	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = rand.nextInt(10);
			}
		}
	}

	private void printMatrix() {
		for (double[] elem : matrix) {
			System.out.println(Arrays.toString(elem));
		}
	}
}
