package by.baranov.multidimensionalarrays.task9;

import java.util.Arrays;
import java.util.Random;

public class Task9SquareMatrix {

	private double[][] matrix;
	private Random rand = new Random();

	public Task9SquareMatrix(int dimension) {
		if (dimension < 1) {
			throw new IllegalArgumentException("Invalid input, matrix can't have quantity strings or columns less than 1");
		}
		matrix = new double[dimension][dimension];		
	}

	public void printSumModulesNegativeNumbersMatrix() {
		System.out.println("\n_TASK-9_\n");
		fillMatrix();
		System.out.println("The origin matrix :\n");
		printMatrix();
		String diagonalNumbers = defineDiagonalNumbers();
		System.out.println("\nThe diagonal matrix numbers : " + diagonalNumbers );
	}

	private String defineDiagonalNumbers() {
		String elements = "";
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if (i == j) {
					elements += matrix[i][j] + ", ";
				}
			}
		}
		return elements;
	}

	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = rand.nextInt(10);
			}
		}
	}

	private void printMatrix() {
		for (double[] elem : matrix) {
			System.out.println(Arrays.toString(elem));
		}
	}

}
