package by.baranov.multidimensionalarrays.task24;

import java.util.Arrays;

public class Task24QuadraticMatrixCorrespondArray {
	private double[][] matrix;
	private double[] array;

	public Task24QuadraticMatrixCorrespondArray(double[] array) {
		if (array.length < 1) {
			throw new IllegalArgumentException("Invalid input, the dimension of matrix can't be less than 1");
		}
		this.array = array;
		this.matrix = new double[array.length][array.length];
	}

	public void printQuadraticMatrix() {
		System.out.println("\n_TASK-24_\n");
		System.out.println("For array = " + Arrays.toString(array)
				+ " the quadratic matrix in accordance with the rule has the form : ");
		System.out.println("For n = " + matrix.length
				+ " the quadratic matrix in accordance with the rule has the form : ");
		fillMatrix();
		printMatrix();
	}

	private void fillMatrix() {

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				matrix[i][j] = Math.pow(array[j], i + 1);
			}
		}
	}

	private void printMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			System.out.print("[ ");
			for (int j = 0; j < matrix.length; j++) {
				System.out.printf("%8.2f ", matrix[i][j]);
				if (j < matrix.length - 1) {
					System.out.print(", ");
				}
			}
			System.out.print("]\n");
		}
	}
}
