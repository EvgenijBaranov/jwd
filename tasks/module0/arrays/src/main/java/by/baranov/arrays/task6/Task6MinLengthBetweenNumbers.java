package by.baranov.arrays.task6;

import java.util.Arrays;

public class Task6MinLengthBetweenNumbers {
	double[] array;

	public Task6MinLengthBetweenNumbers(double[] array) {
		if (array.length == 0) {
			throw new IllegalArgumentException("invalid input, array can't be empty");
		}
		this.array = array;
	}

	public void printMinLengthBetweenNumbers() {
		double minLength = calculateMinLength();
		System.out.println("\n_TASK-6_\n");
		System.out.println("For sequence " + Arrays.toString(array) + 
								" min length between two elements is equal : " + minLength);
	}

	private double calculateMinLength() {
		double[] newArray = array.clone();
		Arrays.sort(newArray);
		return newArray[newArray.length - 1] - newArray[0];
	}

}
