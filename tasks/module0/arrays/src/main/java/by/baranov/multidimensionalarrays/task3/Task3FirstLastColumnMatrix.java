package by.baranov.multidimensionalarrays.task3;

import java.util.Arrays;
import java.util.Random;

public class Task3FirstLastColumnMatrix {
	private double[][] matrix = new double[5][10];
	private Random rand = new Random();

	public void printFirstLastColumnsMatrix() {
		System.out.println("\n_TASK-3_\n");
		fillMatrix();
		System.out.println("The origin matrix :\n");
		printMatrix();
		System.out.println("\nThe first and last columns :\n");
		for(double[] elem : matrix) {
			System.out.println(elem[0] + " : " + elem[elem.length - 1]);
		}
	}

	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = rand.nextInt(9);
			}
		}
	}

	private void printMatrix() {
		for (double[] elem : matrix) {
			System.out.println(Arrays.toString(elem));
		}
	}
}
