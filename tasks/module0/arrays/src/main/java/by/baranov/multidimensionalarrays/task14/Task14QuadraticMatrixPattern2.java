package by.baranov.multidimensionalarrays.task14;

import java.util.Arrays;

public class Task14QuadraticMatrixPattern2 {
	private int[][] matrix;

	public Task14QuadraticMatrixPattern2(int n) {
		if (n < 1 || n % 2 != 0) {
			throw new IllegalArgumentException("Invalid input, matrix can't have odd quantity of strings and columns, "
					+ "as well as dimension of matrix can't be less than 1");
		}
		this.matrix = new int[n][n];
	}

	public void printQuadraticMatrix() {
		System.out.println("\n_TASK-14_\n");
		System.out.println(
				"For n = " + matrix.length + " the quadratic matrix in accordance with the template 2 has the form : ");
		fillMatrix();
		printMatrix();
	}

	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = (i + j == matrix.length -1) ? (i + 1) : 0;
			}
		}
	}
	
	private void printMatrix() {
		for (int[] elem : matrix) {
			System.out.println(Arrays.toString(elem));
		}
	}
}
