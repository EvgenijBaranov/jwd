package by.baranov.multidimensionalarrays.task15;

import java.util.Arrays;

public class Task15QuadraticMatrixPattern3 {
	private int[][] matrix;	

	public Task15QuadraticMatrixPattern3(int n) {
		if (n < 1) {
			throw new IllegalArgumentException(
					"Invalid input, matrix can't have quantity strings or columns less than 1");
		}
		this.matrix = new int[n][n];
	}

	public void printQuadraticMatrix() {
		System.out.println("\n_TASK-15_\n");
		System.out.println("For n = " + matrix.length + " the quadratic matrix has the form : ");
		fillMatrix();
		printMatrix();
	}

	private void fillMatrix() {
		int inputElelment = matrix.length + 1;
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = (i == j) ? inputElelment -= 1 : 0;
			}
		}
	}

	private void printMatrix() {
		for (int[] elem : matrix) {
			System.out.println(Arrays.toString(elem));
		}
	}
}
