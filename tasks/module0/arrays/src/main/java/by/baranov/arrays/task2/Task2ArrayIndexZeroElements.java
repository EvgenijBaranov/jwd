package by.baranov.arrays.task2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Task2ArrayIndexZeroElements {
	long[] array;

	public Task2ArrayIndexZeroElements(long[] array) {
		if(array.length == 0) {
			throw new IllegalArgumentException("invalid input, array can't be empty");
		}
		this.array = array;
	}

	public void printArrayWithIndexZeroElement() {
		System.out.println("\n_TASK-2_\n");
		long[] newArray = createArrayWithIndexNullElement();
		System.out.println("The origin array : " + Arrays.toString(array));
		System.out.println("Array with indexes of zero elements : " + Arrays.toString(newArray));
	}

	private long[] createArrayWithIndexNullElement() {
		List<Long> result = new ArrayList<Long>();
		for (int index = 0; index < array.length; index++) {
			if (array[index] == 0) {
				result.add((long) index);
			}
		}
		long[] newArray = new long[result.size()];
		int i = 0;
		for (long elem : result) {
			newArray[i] = elem;
			i++;
		}
		return newArray;
	}	

}
