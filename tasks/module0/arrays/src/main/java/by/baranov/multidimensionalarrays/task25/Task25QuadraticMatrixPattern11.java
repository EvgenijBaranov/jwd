package by.baranov.multidimensionalarrays.task25;

public class Task25QuadraticMatrixPattern11 {
	private int[][] matrix;

	public Task25QuadraticMatrixPattern11(int n) {
		if (n < 1) {
			throw new IllegalArgumentException("Invalid input, the dimension of the matrix can't be less than 1");
		}
		this.matrix = new int[n][n];
	}

	public void printQuadraticMatrix() {
		System.out.println("\n_TASK-25_\n");
		System.out.println("For n = " + matrix.length
				+ " the quadratic matrix in accordance with the template 11 has the form : ");
		fillMatrix();
		printMatrix();
	}

	private void fillMatrix() {
		int n = matrix.length;
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				matrix[i][j] = n * i + j + 1;
			}
		}
	}

	private void printMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			System.out.print("[ ");
			for (int j = 0; j < matrix.length; j++) {
				System.out.printf("%3d ", matrix[i][j]);
				if (j < matrix.length - 1) {
					System.out.print(", ");
				}
			}
			System.out.print("]\n");
		}
	}
}
