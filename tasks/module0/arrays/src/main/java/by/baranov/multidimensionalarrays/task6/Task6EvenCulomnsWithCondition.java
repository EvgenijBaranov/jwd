package by.baranov.multidimensionalarrays.task6;

import java.util.Arrays;
import java.util.Random;

public class Task6EvenCulomnsWithCondition {
	private double[][] matrix = new double[5][7];
	private Random rand = new Random();

	public void printEvenStringsMatrix() {
		System.out.println("\n_TASK-6_\n");
		fillMatrix();
		System.out.println("The origin matrix :\n");
		printMatrix();
		System.out.println("\nOnly even columns of matrix, in which the first element more the last :\n");

		for (int j = 0; j < matrix[0].length; j = j + 2) {
			if (matrix[0][j] > matrix[matrix.length - 1][j]) {
				System.out.println("Column is appropriate to condititon :" + (j + 1));
				for (int i = 0; i < matrix.length; i++) {
					System.out.println(matrix[i][j]);
				}
			}
		}
	}

	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = rand.nextInt(9);
			}
		}
	}

	private void printMatrix() {
		for (double[] elem : matrix) {
			System.out.println(Arrays.toString(elem));
		}
	}
}
