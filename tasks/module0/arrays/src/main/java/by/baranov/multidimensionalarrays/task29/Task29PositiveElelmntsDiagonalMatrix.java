package by.baranov.multidimensionalarrays.task29;

import java.util.Random;

public class Task29PositiveElelmntsDiagonalMatrix {
	private double[][] matrix;
	Random rand = new Random();

	public Task29PositiveElelmntsDiagonalMatrix(int n) {
		if (n < 1) {
			throw new IllegalArgumentException("Invalid input, the dimension of matrix can't be less than 1");
		}
		this.matrix = new double[n][n];
	}

	public void printQuadraticMatrix() {
		System.out.println("\n_TASK-29_\n");
		System.out.println("The origin matrix :");
		fillMatrix();
		printMatrix();
		System.out.print("The diagonal positive members :");
		printPositiveDiagonalMembers();
	}

	private void printPositiveDiagonalMembers() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				if(i == j && matrix[i][j] > 0){
					System.out.print(matrix[i][j] + ", ");
				}
				
			}
		}
		
	}

	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				matrix[i][j] = rand.nextInt(10) - 5;
			}
		}
	}

	private void printMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			System.out.print("[ ");
			for (int j = 0; j < matrix.length; j++) {
				System.out.printf("%5.2f ", matrix[i][j]);
				if (j < matrix.length - 1) {
					System.out.print(", ");
				}
			}
			System.out.print("]\n");
		}
	}
}
