package by.baranov.multidimensionalarrays.task1;

import java.util.Arrays;

public class Task1CreateMatrix3x4 {
	private double[][] array = new double[3][4];

	public void printMatrix() {
		System.out.println("\n\n\n______MULTIDIMENSIONAL_ARRAYS______\n\n");
		System.out.println("\n_TASK-1_\n");
		System.out.println("The multidimensional array 3x4 filled with 0 and 1 :\n");

		for (double[] elem : array) {
			elem[2] = 1;
		}
		
		for (double[] elem : array) {
			System.out.println(Arrays.toString(elem));
		}
	}
}
