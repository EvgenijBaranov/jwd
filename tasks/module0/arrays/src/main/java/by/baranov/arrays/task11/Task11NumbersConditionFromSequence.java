package by.baranov.arrays.task11;

import java.util.Arrays;

public class Task11NumbersConditionFromSequence {
	private long[] sequence;

	public Task11NumbersConditionFromSequence(long[] sequence) {
		for (int i = 0; i < sequence.length; i++) {
			if (sequence[i] < 0) {
				throw new IllegalArgumentException("The sequence must have only natural numbers");
			}
		}
		this.sequence = sequence;
	}

	public void printNumbersWithCondition(double number) {
		if (number <= 0) {
			throw new IllegalArgumentException(
					"The inputted number, which will be use for finding a remainder, must be only > 0");
		}
		System.out.println("\n\n_TASK-11_\n");
		System.out.println("The origin sequence : " + Arrays.toString(sequence));
		System.out.println("The sequence with numbers, which appropriate to condition : ");
		System.out.println(
				"remainder L from division element of sequence on M = " + number + " falls within 0 < L < M-1");
		defineNumbersWithCondition(number);
	}

	private void defineNumbersWithCondition(double number) {
		for (long elem : sequence) {
			if (elem % number < number - 1) {
				System.out.print(elem + ", ");
			}
		}
	}
}
