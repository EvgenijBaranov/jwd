package by.baranov.arrays.task20;

import java.util.Arrays;

public class Task20CompressArray {
	private double[] array;

	public Task20CompressArray(double[] sequence) {
		if (sequence.length < 1) {
			throw new IllegalArgumentException("The array must have dimension more than 1");
		}
		this.array = sequence;
	}

	public void printCompressArray() {
		System.out.println("\n_TASK-20_\n");
		System.out.println("The origin array : " + Arrays.toString(array));
		printArrayWithoutEvenMembers();
	}

	private void printArrayWithoutEvenMembers() {
		for (int i = 1; i < array.length; i = i + 2) {
			array[i] = 0;
		}
		System.out.println("The array after replacement even members by 0 : " + Arrays.toString(array));

	}

}
