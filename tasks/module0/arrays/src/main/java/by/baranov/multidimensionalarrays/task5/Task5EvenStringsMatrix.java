package by.baranov.multidimensionalarrays.task5;

import java.util.Arrays;
import java.util.Random;

public class Task5EvenStringsMatrix {
	private double[][] matrix = new double[9][10];
	private Random rand = new Random();

	public void printEvenStringsMatrix() {
		System.out.println("\n_TASK-5_\n");
		fillMatrix();
		System.out.println("The origin matrix :\n");
		printMatrix();
		System.out.println("\nOnly even strings of matrix :\n");

		for (int i = 0; i < matrix.length; i = i + 2) {
			System.out.println(Arrays.toString(matrix[i]));
		}
	}

	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = rand.nextInt(9);
			}
		}
	}

	private void printMatrix() {
		for (double[] elem : matrix) {
			System.out.println(Arrays.toString(elem));
		}
	}
}
