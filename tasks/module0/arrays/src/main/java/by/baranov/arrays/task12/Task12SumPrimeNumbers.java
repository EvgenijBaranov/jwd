package by.baranov.arrays.task12;

import java.util.Arrays;

public class Task12SumPrimeNumbers {
	private double[] sequence;

	public Task12SumPrimeNumbers(double[] sequence) {
		if (sequence.length < 1) {
			throw new IllegalArgumentException("The sequence must have numbers more than 1");
		}
		this.sequence = sequence;
	}

	public void printSumPrimeNumbers() {
		System.out.println("\n\n_TASK-12_\n");
		System.out.println("The origin sequence : " + Arrays.toString(sequence));
		printPrimeNumbersSecuence();
		double sumPrimeNumbers = defineSumPrimeNumbers();
		System.out.println("\nSum of primes in the sequence : " + sumPrimeNumbers);
		
	}

	private double defineSumPrimeNumbers() {
		double sum = 0;
		for(double elem : sequence) {
			if(isPrimeNumber(elem)){
				sum += elem;
			}
		}		
		return sum;		
	}

	private void printPrimeNumbersSecuence() {
		System.out.print("Prime numbers of the sequence : ");
		for(double elem : sequence) {
			if (isPrimeNumber(elem)) {
				System.out.print(elem + ", ");				
			}
		}			
	}

	private boolean isPrimeNumber(double number) {
		boolean isPrimeNumber = true;		
		for (int i = 2; i < number; i++) {
			if (number % i == 0) {
				isPrimeNumber = false;
			}
		}
		return isPrimeNumber;
	}
}
