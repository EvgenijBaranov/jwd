package by.baranov.arraysort.task5;

import java.util.Arrays;

public class Task5InsertionSort {
	private double[] sequenceNumbers;

	public Task5InsertionSort(double[] sequenceNumbers) {
		if (sequenceNumbers.length < 2) {
			throw new IllegalArgumentException("Invalid input, sequences can't have less than 2 elements");
		}
		this.sequenceNumbers = sequenceNumbers;
	}

	public void printResultSorting() {
		System.out.println("\n_TASK-5_\n");
		System.out.println("The original sequence: " + Arrays.toString(sequenceNumbers));
		double[] sortedSequence = sortSequence();
		System.out.println("The sorted increasing sequence (insertion sort) - " + Arrays.toString(sortedSequence));
	}

	private double[] sortSequence() {
		for (int i = 0; i < sequenceNumbers.length; i++) {
			double tempMinElement = sequenceNumbers[i];
			int j = i - 1;
			for (; j >= 0; j--) {
				if (sequenceNumbers[j] > tempMinElement) {
					sequenceNumbers[j + 1] = sequenceNumbers[j];
				} else {
					break;
				}
			}
			sequenceNumbers[j + 1] = tempMinElement;
		}
		return sequenceNumbers;
	}

}
