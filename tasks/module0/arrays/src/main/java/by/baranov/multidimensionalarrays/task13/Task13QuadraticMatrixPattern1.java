package by.baranov.multidimensionalarrays.task13;

import java.util.Arrays;

public class Task13QuadraticMatrixPattern1 {
	private int[][] matrix;

	public Task13QuadraticMatrixPattern1(int n) {
		if (n < 1 || n % 2 != 0) {
			throw new IllegalArgumentException("Invalid input, matrix can't have odd quantity of strings and columns, "
					+ "as well as dimension of matrix can't be less than 1");
		}
		this.matrix = new int[n][n];
	}

	public void printQuadraticMatrix() {
		System.out.println("\n_TASK-13_\n");
		System.out.println(
				"For n = " + matrix.length + " the quadratic matrix in accordance with the template 1 has the form : ");
		fillMatrix();
		printMatrix();
	}

	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			matrix[i] = (i % 2 == 0) ? defineMatrixForEvenStrings() : defineMatrixForOddStrings();
		}
	}

	private int[] defineMatrixForEvenStrings() {
		int[] matrixEvenStrings = new int[matrix.length];
		for (int i = 0; i < matrix.length; i++) {
			matrixEvenStrings[i] = i + 1;
		}
		return matrixEvenStrings;
	}

	private int[] defineMatrixForOddStrings() {
		int[] matrixOddStrings = new int[matrix.length];
		int number = matrix.length;
		for (int i = 0 ; i < matrix.length; i++, number--) {
			matrixOddStrings[i] = number ;			
		}
		return matrixOddStrings;
	}

	private void printMatrix() {
		for (int[] elem : matrix) {
			System.out.println(Arrays.toString(elem));
		}
	}
}
