package by.baranov.multidimensionalarrays.task33;

import java.util.Random;

public class Task33MatrixSortColumns {
	private long[][] matrix;
	Random rand = new Random();

	public Task33MatrixSortColumns(int quantityStrings, int quantityColumns) {
		if (quantityStrings < 1 || quantityColumns < 1) {
			throw new IllegalArgumentException("Invalid input, the dimension of the matrix can't be less than 1");
		}
		this.matrix = new long[quantityStrings][quantityColumns];
	}

	public void printMatrixAscendingDescendingStrings() {
		System.out.println("\n_TASK-33_\n");
		System.out.println("The origin matrix :");
		fillMatrix();
		printMatrix();
		sortMatrixAscendingColumns();
		System.out.println("\nThe matrix with ascending columns : ");
		printMatrix();
		System.out.println("\nThe matrix with descending columns : ");
		sortMatrixDescendingColumns();
		printMatrix();
	}

	private void sortMatrixDescendingColumns() {
		for (int j = 0; j < matrix[0].length; j++) {
			long tempElement;
			for (int i = 0; i < matrix.length;) {
				if (matrix[i][j] < matrix[i + 1][j]) {
					tempElement = matrix[i][j];
					matrix[i][j] = matrix[i + 1][j];
					matrix[i + 1][j] = tempElement;
					if (i > 0) {
						i--;
					}
				} else if (i < matrix.length - 2) {
					i++;
				} else {
					break;
				}
			}
		}
	}

	private void sortMatrixAscendingColumns() {
		for (int j = 0; j < matrix[0].length; j++) {
			long tempElement;
			for (int i = 0; i < matrix.length;) {
				if (matrix[i][j] > matrix[i + 1][j]) {
					tempElement = matrix[i][j];
					matrix[i][j] = matrix[i + 1][j];
					matrix[i + 1][j] = tempElement;
					if (i > 0) {
						i--;
					}
				} else if (i < matrix.length - 2) {
					i++;
				} else {
					break;
				}
			}
		}
	}

	private void fillMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = rand.nextInt(999);
			}
		}
	}

	private void printMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			System.out.print("[ ");
			for (int j = 0; j < matrix[i].length; j++) {
				System.out.printf("%3d ", matrix[i][j]);
				if (j < matrix[i].length - 1) {
					System.out.print(", ");
				}
			}
			System.out.print("]\n");
		}
	}
}
