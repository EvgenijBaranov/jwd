package by.baranov.multidimensionalarrays.task40;

public class Task40MagicMatrix {
	private long[][] matrix;

	public Task40MagicMatrix(int dimension) {
		if (dimension < 1) {
			throw new IllegalArgumentException("Invalid input, the dimension of the matrix can't be less than 1");
		}
		this.matrix = new long[dimension][dimension];
	}

	public void printMagicMatrix() {
		System.out.println("\n_TASK-40_\n");
		System.out.println("The magic matrix :");
		fillMatrix();
		printMatrix(matrix);
		double magicConst = matrix.length * (Math.pow(matrix.length, 2) + 1) / 2;
		System.out.println("Magic number of the matrix = " + magicConst);

	}

	private void fillMatrix() {

		if (matrix.length % 2 != 0) {
			fillOddMatrix(matrix);
		} else if (matrix.length % 2 == 0 && matrix.length % 4 == 0){
			fillEven2And4Matrix(matrix);
		}else{
			fillEven2Matrix(matrix);
		}
	}

	private void fillEven2Matrix(long[][] matrix2) {
		// TODO Auto-generated method stub
		
	}

	private void fillEven2And4Matrix(long[][] matrix2) {
		// TODO Auto-generated method stub
		
	}

	private void fillOddMatrix(long[][] matrix) {
		int i = 0;
		int j = matrix.length / 2;
		int tempI = i;
		int tempJ = j;
		int quantityElements = matrix.length * matrix[0].length;
		for (int k = 1; k <= quantityElements; k++) {
			if (i < 0) {
				i = matrix.length - 1;
				if (j == matrix[0].length) {
					j = tempJ;
					i = ++tempI;
				}
			} else if (j == matrix[0].length) {
				j = 0;
			} else if (matrix[i][j] != 0) {
				i = i + 2;
				j--;
			}
			matrix[i][j] = k;
			tempI = i;
			tempJ = j;
			i--;
			j++;
		}
	}

	private void printMatrix(long[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			System.out.print("[ ");
			for (int j = 0; j < matrix[0].length; j++) {
				System.out.printf("%2d ", matrix[i][j]);
				if (j < matrix[0].length - 1) {
					System.out.print(", ");
				}
			}
			System.out.print("]\n");
		}
	}
}
