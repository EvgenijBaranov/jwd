package by.baranov.cycles.runner;

import by.baranov.cycles.task1.Task1PrintNumbers;
import by.baranov.cycles.task10.Task10ProductSquareOfNumbers;
import by.baranov.cycles.task11.Task11DifferenceCubesOfNumbers;
import by.baranov.cycles.task12.Task12SequenceNumber;
import by.baranov.cycles.task13.Task13TableValuesOfFunction;
import by.baranov.cycles.task14.Task14SequenceNumbers;
import by.baranov.cycles.task15.Task15GeometricProgression;
import by.baranov.cycles.task3.Task3MultiplicationTable;
import by.baranov.cycles.task4.Task4EvenNumbers;
import by.baranov.cycles.task5.Task5SumEvenNumbers;
import by.baranov.cycles.task6.Task6SumNumbersFrom1;
import by.baranov.cycles.task7.Task7Function;
import by.baranov.cycles.task8.Task8Function;
import by.baranov.cycles.task9.Task9SumSquareOfNumbers;

public class RunnerCycles {
	

	
	public static void main(String[] args) {
		System.out.println("\n\n________ CYCLES _____\n");
		
		System.out.println("_TASK-1__\n");
		Task1PrintNumbers task1 = new Task1PrintNumbers(1, 5);
		task1.printNumbers();
		
		System.out.println("\n__TASK-2__\n");
		Task1PrintNumbers task2 = new Task1PrintNumbers(5, 1);
		task2.printNumbers();
		
		Task3MultiplicationTable table = new Task3MultiplicationTable(3);
		table.printMultiplicationTable();
		
		Task4EvenNumbers evenNumbers = new Task4EvenNumbers(2,100);
		evenNumbers.printEvenNumbers();
		
		Task5SumEvenNumbers task5 = new Task5SumEvenNumbers(1,99);
		task5.printSumEvenNumbers();
		
		Task6SumNumbersFrom1 task6 = new Task6SumNumbersFrom1(10);
		task6.printSumNumbersFrom1();
		
		Task7Function task7 = new Task7Function();
		task7.printValueFunction(-10, 10, 2);
		
		Task8Function task8 = new Task8Function();
		task8.printValueFunction(-10, 10, 2);
		
		Task9SumSquareOfNumbers task9 = new Task9SumSquareOfNumbers(1,100);
		task9.printSumSquareOfNumbers();
		
		Task10ProductSquareOfNumbers task10 = new Task10ProductSquareOfNumbers("1","200");
		task10.printProductSquareOfNumbers();
		
		Task11DifferenceCubesOfNumbers task11 = new Task11DifferenceCubesOfNumbers("1","4");
		task11.printDifferenceCubesOfNumbers();
		
		Task12SequenceNumber task12 = new Task12SequenceNumber(100);
		task12.printProductMembers();
		
		Task13TableValuesOfFunction task13 = new Task13TableValuesOfFunction(-0.5, 0, 5);
		task13.printTableValuesOfFunction(-5, 5, 0.5);
		
		Task14SequenceNumbers task14 = new Task14SequenceNumbers(10);
		task14.printSequence();
		
		Task15GeometricProgression task15 = new Task15GeometricProgression(1,2);
		task15.printSumProgression(10);
		
	}

}
