package by.baranov.cycles.task13;

public class Task13TableValuesOfFunction {
	private double coefficientA;
	private double coefficientB;
	private double coefficientC;
	
	public Task13TableValuesOfFunction(double coefficientA, double coefficientB, double coefficientC) {
		this.coefficientA = coefficientA;
		this.coefficientB = coefficientB;
		this.coefficientC = coefficientC;
	}
	
	
	public void printTableValuesOfFunction(double startNumber, double lastNumber, double step) {
		System.out.println("\n\n_TASK-13_");
		printTableHeader();
		for(double x = startNumber; x < lastNumber; x += step) {
			System.out.printf(":%5.1f : %5.1f :%n", x, calculateY(x));
		}
		printTableTail();
	}
	private double calculateY(double x) {
		return coefficientA * Math.pow(x, 2) + coefficientB * x + coefficientC;
	}
	
	private void printTableHeader() {
		System.out.println("\n Table of values of function y = 5 - (x^2)/2 \n");
		printTableTail();
		System.out.println(":  x   :   y   :");
		printTableTail();
	}
	private void printTableTail() {
		System.out.println("----------------");
	}
	
}
