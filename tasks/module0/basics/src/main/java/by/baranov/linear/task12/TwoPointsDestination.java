package by.baranov.linear.task12;

public class TwoPointsDestination {
	private static double calculateDistance(Task12Point pointFirst, Task12Point pointSecond) {
		double result = Math.hypot(pointFirst.getX() - pointSecond.getX(), pointFirst.getY() - pointSecond.getY());
		return result;
	}
	public static void printDistance(Task12Point pointFirst, Task12Point pointSecond) {
		double result = calculateDistance(pointFirst, pointSecond);
		System.out.println("\n_TASK-12_\n\n distance between "+ pointFirst+ " and " + pointSecond +" equals "+result);
	}
}
