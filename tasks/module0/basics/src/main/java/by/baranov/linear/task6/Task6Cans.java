package by.baranov.linear.task6;

public class Task6Cans {
	
	public static void printTotalCapacityBigCans(long quantitySmallCans, long quantityBigCans) {
		checkData(quantitySmallCans, quantityBigCans);
		double totalCapacityBigCans = getTotalCapacityBigCans(quantitySmallCans, quantityBigCans);
		System.out.printf("\nTASK-6_\n\nif quantity of small and big cans equals accordingly n = %d m = %d %n then total capacity of big cangs equals = %-5.2f%n", quantitySmallCans, quantityBigCans, totalCapacityBigCans);
	}
	private static void checkData(long quantitySmallCans, long quantityBigCans) {
		if(quantitySmallCans <= 0 || quantityBigCans <= 0) {
			throw new IllegalArgumentException("quantity of big and small cans has to be integer positive number");
		}
	}
	private static double getTotalCapacityBigCans(long quantitySmallCans, long quantityBigCans) {
		double totalCapacityBigCans = (80 / quantitySmallCans + 12) * quantityBigCans;
		return totalCapacityBigCans;
	}
	

}
