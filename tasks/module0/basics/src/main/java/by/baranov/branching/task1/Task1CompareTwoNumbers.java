package by.baranov.branching.task1;

public class Task1CompareTwoNumbers 
{
	public static void compareTwoNumbers(int numberOne, int numberTwo){
		System.out.println("\n\n\n___ BRANCHING ____\n\n");
		System.out.println("_TASK-1_\n");
		int result = numberOne < numberTwo ? 7 : 8;
		System.out.printf("first number: %d , second number: %d , result of program: %d%n", numberOne, numberTwo, result);
		
	}
}
