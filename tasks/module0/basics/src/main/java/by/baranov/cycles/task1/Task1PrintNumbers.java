package by.baranov.cycles.task1;

public class Task1PrintNumbers {
	private double from;
	private double to;
	
	public Task1PrintNumbers(double from, double to) {		
			this.from = from;
			this.to = to;		
	}
	
	public void printNumbers() {		
		System.out.printf("numbers from %-5.2f to %-5.2f :  ", from, to);
		if(from <= to) {
			printAscend();
		} else {
			printDescend();
		}		
	}	
	
	private void printAscend() {
		for(double i = from; i < to; i++) {
			System.out.print(i + ", ");
		}
		System.out.println(" ");
	}
	private void printDescend(){
		for(double i = from; i > to; i--) {
			System.out.print(i + ", ");
		}
		System.out.println(" ");
	}
	
}
