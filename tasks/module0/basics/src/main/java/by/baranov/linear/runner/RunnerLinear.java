package by.baranov.linear.runner;

import by.baranov.linear.task1.Task1TwoNumbers;
import by.baranov.linear.task10.Task10Expression;
import by.baranov.linear.task11.Task11RightTriangle;
import by.baranov.linear.task12.Task12Point;
import by.baranov.linear.task12.TwoPointsDestination;
import by.baranov.linear.task13.Task13ApexTriangle;
import by.baranov.linear.task13.Task13Calculator;
import by.baranov.linear.task14.Task14Circle;
import by.baranov.linear.task15.Task15PowerPI;
import by.baranov.linear.task6.Task6Cans;
import by.baranov.linear.task7.Task7Rectangle;
import by.baranov.linear.task8.Task8Expression;
import by.baranov.linear.task9.Task9Expression;

public class RunnerLinear {	
	private static void task2(double a) {
		double c = a + 3;
		System.out.println("\n_TASK-2_\n");
		System.out.printf("function : c = 3 + a %n for a = %-5.2f %n c(a) = %-8.2f%n", a, c );
	}
	private static void task3(double x, double y) {
		double z = 2 * x + (y - 2) * 5;
		System.out.println("\n_TASK-3_\n");
		System.out.printf("function : z = 2 * x + (y - 2) * 5 %n for x = %-5.2f , y = %-5.2f %n z(x,y) = %-8.2f%n", x, y, z );
	}
	private static void task4(double a, double b, double c) {
		double z = (a - 3) * b / 2 + c;
		System.out.println("\n_TASK-4_\n");
		System.out.printf("function : z = (a - 3) * b / 2 + c %n for a = %-5.2f , b = %-5.2f, c = %-5.2f %n z(x,y) = %-8.2f%n", a, b, c, z );
	}
	private static void task5(double numberOne, double numberTwo) {
		double averageNumber = (numberOne + numberTwo) / 2;
		System.out.println("\n_TASK-5_\n");
		System.out.printf("for numbers: %5.2f, %5.2f %naverageNumber = %5.2f%n", numberOne, numberTwo, averageNumber);
	}
	

	public static void main(String[] args) {
		Task1TwoNumbers multitude = new Task1TwoNumbers(100, -20);
		multitude.printResult();
		
		task2(5);	
		task3(5,10);
		task4(5,10,15);
		task5(20,30);
		
		Task6Cans.printTotalCapacityBigCans(10, 3);
		
		Task7Rectangle rectangle = new Task7Rectangle(5);
		System.out.println(rectangle);
		
		Task8Expression expressionTask8 = new Task8Expression(12, 5, 123);
		expressionTask8.printResultExpression();
		
		Task9Expression expressionTask9 = new Task9Expression(42, 34, 45, 32);
		expressionTask9.printResultExpression();
		
		Task10Expression expressionTask10 = new Task10Expression(3 , 4);
		expressionTask10.printResultExpression();
		
		Task11RightTriangle rightTriangle = new Task11RightTriangle(3,4);
		rightTriangle.printSquare();
		
		Task12Point point1 = new Task12Point(0,0);
		Task12Point point2 = new Task12Point(3,4);
		TwoPointsDestination.printDistance(point1, point2);
		
		Task13ApexTriangle firstApex = new Task13ApexTriangle(2,3);
		Task13ApexTriangle secondApex = new Task13ApexTriangle(3,4);
		Task13ApexTriangle thirdApex = new Task13ApexTriangle(6,10);
		Task13Calculator.printSquarePerimeter(firstApex, secondApex, thirdApex);
		
		Task14Circle circleTask14 = new Task14Circle(5);
		circleTask14.printRadiusSquare();
		
		Task15PowerPI.printNumbers(4);
		
		
	}	
}
