package by.baranov.branching.task8;

public class Task8MinSquareOfNumbers {
	public static void findMinSquareNumbers(double numberOne, double numberTwo){
		System.out.println("\n_TASK-8_\n");
		double numberOneSquare = Math.pow(numberOne, 2);
		double numberTwoSquare = Math.pow(numberTwo, 2);
		double result = numberOneSquare < numberTwoSquare ? numberOneSquare : numberTwoSquare;
		System.out.printf("number first: %-5.2f , number two: %-5.2f -- minimum square of number: %-8.2f %n", numberOne, numberTwo, result);
	}	
}
