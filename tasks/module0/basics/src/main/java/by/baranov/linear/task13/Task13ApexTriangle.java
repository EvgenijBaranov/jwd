package by.baranov.linear.task13;

public class Task13ApexTriangle {
	private double x;
	private double y;
	public Task13ApexTriangle(double x, double y) {
		this.x = x;
		this.y = y;
	}
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	@Override
	public String toString() {
		return "ApexTriangle (x=" + x + ", y=" + y + ")";
	}
	
}
