package by.baranov.branching.task3;

import java.util.Scanner;

public class Task3CompareWith3 {
	
	private static final int COMPARE_NUMBER = 3;
	
	public static void compareTwoNumbers(){
		System.out.println("\n_TASK-3_\n");
		int numberConsole = parseInput();		
		String result = numberConsole < COMPARE_NUMBER ? "yes" : "no";
		System.out.printf("input number: %d , comparion number: %d , result of program: %s%n", numberConsole, COMPARE_NUMBER, result);
		
	}
	private static int parseInput(){
		int numberInput = 0;
		boolean isNumber = false;		
		while(!isNumber){
			System.out.print("input integer, please: ");
			Scanner scanInput = new Scanner(System.in);
			String resultInput = scanInput.next();
			;
			try{
				numberInput = Integer.parseInt(resultInput);
				isNumber = true;
			}catch (NumberFormatException e){
				System.out.println("illegal input, you need input int number, integer");
			}			
		}		
		return numberInput;
	}
}
