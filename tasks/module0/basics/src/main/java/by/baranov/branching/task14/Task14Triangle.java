package by.baranov.branching.task14;

public class Task14Triangle {
	private double firstAngle;
	private double secondAngle;
	private double thirdAngle;

	public Task14Triangle(double firstAngle, double secondAngle, double thirdAngle) {
		super();
		this.firstAngle = firstAngle;
		this.secondAngle = secondAngle;
		this.thirdAngle = thirdAngle;
	}

	public void printInformationTriangle() {
		System.out.println("\n_TASK-14_\n");
		System.out.println(this + " is exist : " + isExist());
		System.out.println(this + " is rectangular : " + isRectangular());
	}

	private boolean isRectangular() {
		return isExist() && hasRightAngle();
	}

	private boolean isExist() {
		return firstAngle + secondAngle + thirdAngle == 180 ? true : false;
	}

	private boolean hasRightAngle() {
		return firstAngle == 90 || secondAngle == 90 || thirdAngle == 90 ? true : false;
	}

	@Override
	public String toString() {
		return "Triangle with angles (" + firstAngle + ", " + secondAngle + ", " + thirdAngle + ")";
	}

}
