package by.baranov.cycles.task5;

public class Task5SumEvenNumbers {
	private long from;
	private long to;
	
	public Task5SumEvenNumbers(long from, long to) {
		super();
		this.from = from;
		this.to = to;
	}
	
	public void printSumEvenNumbers(){
		System.out.println("\n_TASK-5_\n");
		System.out.printf("total sum of even numbers from %d to %d equals %d%n", from, to, calculateSumEvenNumbers());
		
	}
	private long calculateSumEvenNumbers() {
		long number = from;
		long sum = 0;
		while(number <= to) {
			if(isEvenNumber(number)) {
				sum += number;
			}
			number++;
		}
		return sum;
	}
	private boolean isEvenNumber(long number) {
		return number%2 == 0 ? true : false;
	}
}
