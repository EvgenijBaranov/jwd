package by.baranov.branching.task13;

public class Task13DefinitionMaxRemotePoint {
	
	public static void printMinRemotePoint(Task13Point firstPoint, Task13Point secondPoint){
		System.out.println("\n_TASK-13_\n");		
		Task13Point minRemotePoint = defineMinRemotePoint(firstPoint, secondPoint);
		System.out.println("Between " + firstPoint + " and " + secondPoint + " the least remote point is : " + minRemotePoint);
	}
	
	private static Task13Point defineMinRemotePoint(Task13Point firstPoint, Task13Point secondPoint) {
		double distanceFirstPoint = defineDistanceFromOrigin(firstPoint);
		double distanceSecondPoint = defineDistanceFromOrigin(secondPoint);
		
		return distanceFirstPoint < distanceSecondPoint ? firstPoint : secondPoint;
	}

	private static double defineDistanceFromOrigin(Task13Point point) {
		double distance = Math.hypot(point.getX(), point.getY());
		return distance;
	}
}
