package by.baranov.branching.task6;

public class Task6FindMaxNumber {
	public static void findMaxNumber(int numberOne, int numberTwo){
		System.out.println("\n_TASK-6_\n");
		int result = numberOne > numberTwo ? numberOne : numberTwo;
		System.out.printf("number first: %d , number two: %d -- maximum number: %d %n", numberOne, numberTwo, result);
	}	
}
