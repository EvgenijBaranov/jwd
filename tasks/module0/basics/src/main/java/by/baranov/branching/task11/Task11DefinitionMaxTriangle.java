package by.baranov.branching.task11;

public class Task11DefinitionMaxTriangle {

	public static void printMaxTriangle(Task11Triangle firstTriangle, Task11Triangle secondTriangle){
		System.out.println("_TASK-11_\n");		
		Task11Triangle maxTriangle = defineMaxTriangle(firstTriangle, secondTriangle);
		System.out.println("Between " + firstTriangle + " and " + secondTriangle + " the biggest square has : " + maxTriangle);
	}
	
	private static Task11Triangle defineMaxTriangle(Task11Triangle firstTriangle, Task11Triangle secondTriangle) {
		double squareFirstTriangle = defineSquareTriangle(firstTriangle);
		double squareSecondTriangle = defineSquareTriangle(secondTriangle);
		
		return squareFirstTriangle > squareSecondTriangle ? firstTriangle : secondTriangle;
	}

	private static double defineSquareTriangle(Task11Triangle triangle) {
		double perimeter = triangle.getFirstSide() + triangle.getSecondSide() + triangle.getThirdSide();
		double halfPerimeter = perimeter / 2;
		double square = Math.sqrt(halfPerimeter * (halfPerimeter - triangle.getFirstSide())
				* (halfPerimeter - triangle.getSecondSide()) * (halfPerimeter - triangle.getThirdSide()));
		return square;
	}
}
