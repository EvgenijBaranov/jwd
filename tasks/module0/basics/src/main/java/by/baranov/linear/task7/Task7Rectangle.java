package by.baranov.linear.task7;

public class Task7Rectangle {
	private long width;
	private long length;
	
	public Task7Rectangle(long width){
		checkInputData(width);
		this.width = width;
		this.length = 2 * width;
	}
	
	private static void checkInputData(long width) {
		if(width <= 0) {
			throw new IllegalArgumentException("width of rectangle must be more zero");
		}		
	}
	
	private long square() {
		return width * length;
	}
	@Override
	public String toString() {
		return "\n_TASK-7_\n\nRectangle [width=" + width + ", length=" + length + "] has area = " + square();
	}
	
}
