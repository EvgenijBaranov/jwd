package by.baranov.branching.task7;

public class Task7Expression {
	private double a;
	private double b;
	private double c;
	
	public Task7Expression(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	public void printResultExpression(double x){
		System.out.println("\n TASK-7_\n");
		double result = calculate(x);
		System.out.printf("if a = %-5.2f , b = %-5.2f , с = %-5.2f , x = %-5.2f , %n", a,b,c,x);
		System.out.printf("then | a*x^2 + b*x + с | = %-8.2f %n", result);
	} 
	
	private double calculate(double x){
		double resultCalculation = Math.abs(a * Math.pow(x, 2) + b * x + c);
		return resultCalculation;
	}
	
}
