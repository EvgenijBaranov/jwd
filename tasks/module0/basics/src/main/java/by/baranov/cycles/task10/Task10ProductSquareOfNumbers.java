package by.baranov.cycles.task10;

import java.math.BigDecimal;

public class Task10ProductSquareOfNumbers {
	private BigDecimal firstNumber;
	private BigDecimal lastNumber;
	
	public Task10ProductSquareOfNumbers(String firstNumber, String lastNumber) {
		this.firstNumber = new BigDecimal(firstNumber);
		this.lastNumber = new BigDecimal(lastNumber);
	}
	
	public void printProductSquareOfNumbers() {
		System.out.println("\n_TASK-10_\n");
		BigDecimal sum = calculateProductSquare(firstNumber);
		System.out.printf("product square of numbers between %-5.2f and %-5.2f equals %-7.2f%n", firstNumber, lastNumber, sum);
		
	}
	private BigDecimal calculateProductSquare(BigDecimal firstNumber) {
		if(firstNumber.compareTo(lastNumber) < 0) {
			return calculateProductSquare(firstNumber.add(new BigDecimal(1)) ).multiply(firstNumber.pow(2)) ;
		}
		return new BigDecimal(1);
	}
	
//	private double firstNumber;
//	private double lastNumber;
//	
//	public Task10ProductSquareOfNumbers(double firstNumber, double lastNumber) {
//		this.firstNumber = firstNumber;
//		this.lastNumber = lastNumber;
//	}
//	
//	public void printProductSquareOfNumbers() {
//		System.out.println("\n\nTASK-10_\n");
//		double sum = calculateProductSquare(firstNumber);
//		System.out.printf("product square of numbers between %-5.2f and %-5.2f equals %-7.2f%n", firstNumber, lastNumber, sum);
//		
//	}
//	private double calculateProductSquare(double firstNumber) {
//		if(firstNumber <= lastNumber) {
//			return calculateProductSquare(firstNumber + 1 ) * Math.pow(firstNumber, 2);
//		}
//		return 1;
//	}
}
