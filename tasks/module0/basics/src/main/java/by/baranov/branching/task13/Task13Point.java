package by.baranov.branching.task13;

public class Task13Point {
	private double x;
	private double y;
	
	public Task13Point(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	@Override
	public String toString() {
		return "Point (" + x + ", " + y + ")";
	}	
	
}
