package by.baranov.cycles.task4;

public class Task4EvenNumbers {
	private long from;
	private long to;
	
	public Task4EvenNumbers(long from, long to) {		
		this.from = from;
		this.to = to;
	}
	
	public void printEvenNumbers() {
		System.out.println("\n_TASK-4_\n");
		System.out.printf("even numbers from %d to %d : %n", from, to);
		long number = from;
		while(number <= to) {
			if(isEvenNumber(number)) {
				System.out.print(number + ", ");
			}
			number++;
		}
		System.out.println("");
	}
	
	private boolean isEvenNumber(long number) {
		return number%2 == 0 ? true : false;
	}
}
