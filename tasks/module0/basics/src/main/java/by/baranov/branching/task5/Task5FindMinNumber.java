package by.baranov.branching.task5;

public class Task5FindMinNumber {
	public static void findMinNumber(int numberOne, int numberTwo){
		System.out.println("\n_TASK-5_\n");
		int result = numberOne < numberTwo ? numberOne : numberTwo;
		System.out.printf("number first: %d , number two: %d -- minimum number: %d %n", numberOne, numberTwo, result);
	}		
}
