package by.baranov.linear.task13;

public class Task13Calculator {
	public static void printSquarePerimeter(Task13ApexTriangle first, Task13ApexTriangle second, Task13ApexTriangle third) {
		double firstSide = Math.hypot(first.getX() - second.getX(), first.getY() - second.getY());
		double secondSide = Math.hypot(first.getX() - third.getX(), first.getY() - third.getY());
		double thirdSide = Math.hypot(second.getX() - third.getX(), second.getY() - third.getY());
		double perimeter = firstSide + secondSide + thirdSide;
		double halfPerimeter = perimeter/2;
		double square = Math.sqrt(halfPerimeter*(halfPerimeter-firstSide)*(halfPerimeter-secondSide)*(halfPerimeter-thirdSide));
		System.out.println("\nTASK-13_\n");
		System.out.println("Triangle has next apexes : " + first + ", " + second + ", " + third );
		System.out.printf("Its square = %-6.2f , perimeter = %-6.2f%n", square, perimeter);
	}	
}
