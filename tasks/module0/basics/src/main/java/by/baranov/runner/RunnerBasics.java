package by.baranov.runner;

import by.baranov.branching.runner.RunnerBranching;
import by.baranov.cycles.runner.RunnerCycles;
import by.baranov.linear.runner.RunnerLinear;

public class RunnerBasics {

	public static void main(String[] args) {
		RunnerLinear.main(args);
		RunnerCycles.main(args);
		RunnerBranching.main(args);

	}

}
