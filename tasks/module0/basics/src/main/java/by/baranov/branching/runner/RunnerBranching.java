package by.baranov.branching.runner;

import by.baranov.branching.task1.Task1CompareTwoNumbers;
import by.baranov.branching.task10.Task10Circle;
import by.baranov.branching.task11.Task11DefinitionMaxTriangle;
import by.baranov.branching.task11.Task11Triangle;
import by.baranov.branching.task12.Task12ThreeNumbers;
import by.baranov.branching.task13.Task13DefinitionMaxRemotePoint;
import by.baranov.branching.task13.Task13Point;
import by.baranov.branching.task14.Task14Triangle;
import by.baranov.branching.task15.Task15ChangeTwoNumbers;
import by.baranov.branching.task2.Task2CompareTwoNumbers;
import by.baranov.branching.task3.Task3CompareWith3;
import by.baranov.branching.task4.Task4EqualsNumbers;
import by.baranov.branching.task5.Task5FindMinNumber;
import by.baranov.branching.task6.Task6FindMaxNumber;
import by.baranov.branching.task7.Task7Expression;
import by.baranov.branching.task8.Task8MinSquareOfNumbers;
import by.baranov.branching.task9.Task9Triangle;

public class RunnerBranching {
	
	public static void main(String[] args) {
		Task1CompareTwoNumbers.compareTwoNumbers(1, 2);
		
		Task2CompareTwoNumbers.compareTwoNumbers(1, 2);
		
		Task3CompareWith3.compareTwoNumbers();
		
		Task4EqualsNumbers.compareNumbers(5,5);
		
		Task5FindMinNumber.findMinNumber(5, 3);
		
		Task6FindMaxNumber.findMaxNumber(5, 3);
		
		Task7Expression task7 = new Task7Expression(-2.1, -4, 1.6);
		task7.printResultExpression(100);
		
		Task8MinSquareOfNumbers.findMinSquareNumbers(12, -15);
		
		
		Task9Triangle triangle = new Task9Triangle(10,11,11);
		triangle.printIsEquilateral();
		
		Task10Circle circle1 = new Task10Circle(10.3);
		Task10Circle circle2 = new Task10Circle(5.2);
		Task10Circle.printMinCircle(circle1, circle2);
		
		Task11Triangle triangle1 = new Task11Triangle(10.3,11.5,14);
		Task11Triangle triangle2 = new Task11Triangle(20.3,21.5,24);
		Task11DefinitionMaxTriangle.printMaxTriangle(triangle1, triangle2);
		
		Task12ThreeNumbers task12 = new Task12ThreeNumbers(2, -4, -3);
		task12.printChangedNumbers();
		
		Task13Point point1 = new Task13Point(2, -4);
		Task13Point point2 = new Task13Point(1, -1);
		Task13DefinitionMaxRemotePoint.printMinRemotePoint(point1, point2);

		Task14Triangle task14 = new Task14Triangle(90,60,30);		
		task14.printInformationTriangle();
		
		Task15ChangeTwoNumbers task15 = new Task15ChangeTwoNumbers(3,5);
		task15.printChangedNumbers();
	}

}
