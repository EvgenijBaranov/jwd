package by.baranov.branching.task9;

public class Task9Triangle {
	private double sideFirst;
	private double sideSecond;
	private double sideThird;
	
	public Task9Triangle(double sideFirst, double sideSecond, double sideThird) {
		checkInputData (sideFirst, sideSecond, sideThird);
		this.sideFirst = sideFirst;
		this.sideSecond = sideSecond;
		this.sideThird = sideThird;
	}
	
	public void printIsEquilateral(){
		System.out.println("\n_TASK-9_\n");		
		String result = isEquilateral() ? "equilateral" : "not equilateral";
		System.out.printf("Triangle with dimensions of sides : %-5.2f, %-5.2f, %-5.2f is %s %n", sideFirst, sideSecond, sideThird, result);
	}
	
	private boolean isEquilateral(){
		return sideFirst == sideSecond && sideSecond == sideThird;
	}
	private void checkInputData (double sideFirst, double sideSecond, double sideThird){
		if(sideFirst < 0 || sideSecond < 0 || sideThird < 0){
			throw new IllegalArgumentException(" illegal input, sides of triangle must have dimensions more than zero ");
		}
	}

}
