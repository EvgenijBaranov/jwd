package by.baranov.linear.task15;

public class Task15PowerPI {
	public static void printNumbers(long power) {
		System.out.println("\n\n_TASK-15_\n");
		for(long i = 0; i < power; i++) {
			System.out.println("PI^" + i + " = " + Math.pow(Math.PI, i));
		}
	}
}
