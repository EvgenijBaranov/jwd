package by.baranov.cycles.task9;

public class Task9SumSquareOfNumbers {
	private double firstNumber;
	private double lastNumber;
	
	public Task9SumSquareOfNumbers(double firstNumber, double lastNumber) {
		this.firstNumber = firstNumber;
		this.lastNumber = lastNumber;
	}
	
	public void printSumSquareOfNumbers() {
		System.out.println("\n\n_TASK-9_\n");
		double sum = calculateSumSquare(firstNumber);
		System.out.printf("sum square of numbers between %-5.2f and %-5.2f equals %-7.2f%n", firstNumber, lastNumber, sum);
		
	}
	private double calculateSumSquare(double firstNumber) {
		if(firstNumber <= lastNumber) {
			return calculateSumSquare(firstNumber + 1 ) + Math.pow(firstNumber, 2);
		}
		return 0;
	}
}
