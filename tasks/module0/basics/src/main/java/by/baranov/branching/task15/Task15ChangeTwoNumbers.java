package by.baranov.branching.task15;

import by.baranov.branching.task12.Task12ThreeNumbers;

public class Task15ChangeTwoNumbers {
	private double firstNumber;
	private double secondNumber;

	public Task15ChangeTwoNumbers(double firstNumber, double secondNumber) {
		if(firstNumber == secondNumber) {
			throw new IllegalArgumentException("illegal input, numbers must be different");
		}
		this.firstNumber = firstNumber;
		this.secondNumber = secondNumber;
	}
	public void printChangedNumbers() {
		Task15ChangeTwoNumbers changedNumbers = changeNumbers(firstNumber, secondNumber);
		System.out.println("\n_TASK-15_\n");
		System.out.println("original numbers : ("+ firstNumber + ", " + secondNumber + " )");
		System.out.println("after changes : "+ changedNumbers);
	}
	
	private Task15ChangeTwoNumbers changeNumbers(double firstNumber, double secondNumber) {
		double first = firstNumber < secondNumber ? (firstNumber + secondNumber)/2 : 2 * firstNumber * secondNumber;
		double second = secondNumber < firstNumber ? (firstNumber + secondNumber)/2 : 2 * firstNumber * secondNumber;

		return new Task15ChangeTwoNumbers(first, second);
	}

	@Override
	public String toString() {
		return "(" + firstNumber + ", " + secondNumber + ")";
	}

}
