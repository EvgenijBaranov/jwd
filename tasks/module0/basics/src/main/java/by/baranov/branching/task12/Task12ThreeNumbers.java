package by.baranov.branching.task12;

public class Task12ThreeNumbers {
	private double firstNumber;
	private double secondNumber;
	private double thirdNumber;

	public Task12ThreeNumbers(double firstNumber, double secondNumber, double thirdNumber) {
		super();
		this.firstNumber = firstNumber;
		this.secondNumber = secondNumber;
		this.thirdNumber = thirdNumber;
	}

	public void printChangedNumbers() {
		Task12ThreeNumbers changedNumbers = changeNumbers(firstNumber, secondNumber, thirdNumber);
		System.out.println("\n_TASK-12_\n");
		System.out.println("original numbers : ("+ firstNumber + ", " + secondNumber + ", " + thirdNumber +" )");
		System.out.println("after changes : "+ changedNumbers);
	}

	private Task12ThreeNumbers changeNumbers(double firstNumber, double secondNumber, double thirdNumber) {
		double first = firstNumber > 0 ? Math.pow(firstNumber, 2) : Math.pow(firstNumber, 4);
		double second = secondNumber > 0 ? Math.pow(secondNumber, 2) : Math.pow(secondNumber, 4);
		double third = thirdNumber > 0 ? Math.pow(thirdNumber, 2) : Math.pow(thirdNumber, 4);

		return new Task12ThreeNumbers(first, second, third);
	}

	@Override
	public String toString() {
		return "(" + firstNumber + ", " + secondNumber + ", " + thirdNumber + ")";
	}
	
	
}
