package by.baranov.linear.task10;

public class Task10Expression {
	private double x;
	private double y;
	
	public Task10Expression(double x, double y) {
		this.x = x;
		this.y = y;
	}
	private double calculate() {
		double result = (Math.sin(x) + Math.cos(y)) / (Math.cos(x) - Math.sin(y)) * Math.tan(x * y);
		return result;
	}
	public void printResultExpression() {
		System.out.printf("\n\n_TASK-10_\n\nif x = %-5.2f , y = %-5.2f %nthen expression equals = %-8.2f", x, y, calculate());
	}
}
