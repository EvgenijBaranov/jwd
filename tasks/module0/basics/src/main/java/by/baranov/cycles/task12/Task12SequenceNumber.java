package by.baranov.cycles.task12;

public class Task12SequenceNumber {
	private static long FIRST_MEMBER = 1;
	private long quantityMembers;
		
	public Task12SequenceNumber(long quantityMembers) {
		if(quantityMembers < 1){
			throw new IllegalArgumentException("illegal input");
		}
		this.quantityMembers = quantityMembers;
	}

	public void printProductMembers() {
		System.out.println("\n_TASK-12_\n");
		long result = calculateProductMembers(quantityMembers);
		
		System.out.printf("product first %d members sequence equals %d", quantityMembers,result);
	}
	private long calculateProductMembers(long quantityMembers) {
		if(quantityMembers > 0) {
			return calculateProductMembers(quantityMembers - 1 ) * (FIRST_MEMBER + 6 * (quantityMembers - 1));
		}
		return 1;
	}
}
