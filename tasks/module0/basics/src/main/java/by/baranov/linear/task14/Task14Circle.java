package by.baranov.linear.task14;

public class Task14Circle {
	private double radius;

	public Task14Circle(double radius) {
		checkData(radius);
		this.radius = radius;
	}
	public void printRadiusSquare() {
		System.out.println("\n_TASK-14_\n");
		System.out.printf(this + " has square = %-5.2f and circumference = %-7.2f", calculateSquare(), calculateСircumference() );
	}
	private double calculateSquare() {
		return Math.PI * Math.pow(radius, 2); 
	}
	private double calculateСircumference() {
		return 2 * Math.PI * radius;
	}
	private void checkData(double radius) {
		if(radius < 0) {
			throw new IllegalArgumentException("radius can't be less zero");
		}
	}
	@Override
	public String toString() {
		return "Circle [radius=" + radius + "]";
	}
	
}
