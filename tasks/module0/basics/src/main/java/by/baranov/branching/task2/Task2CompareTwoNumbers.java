package by.baranov.branching.task2;

public class Task2CompareTwoNumbers {
	public static void compareTwoNumbers(int numberOne, int numberTwo){
		System.out.println("\n_TASK-2_\n");
		String result = numberOne < numberTwo ? "yes" : "no";
		System.out.printf("first number: %d , second number: %d , result of program: %s%n", numberOne, numberTwo, result);
		
	}
}
