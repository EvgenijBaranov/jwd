package by.baranov.branching.task11;

public class Task11Triangle {
	
	private double firstSide;
	private double secondSide;
	private double thirdSide;
	
	public Task11Triangle(double firstSide, double secondSide, double thirdSide) {
		checkInputData (firstSide, secondSide, thirdSide);
		this.firstSide = firstSide;
		this.secondSide = secondSide;
		this.thirdSide = thirdSide;
	}
	
	private void checkInputData (double sideFirst, double sideSecond, double sideThird){
		if(sideFirst < 0 || sideSecond < 0 || sideThird < 0){
			throw new IllegalArgumentException(" illegal input, sides of triangle must have dimensions more than zero ");
		}
	}

	public double getFirstSide() {
		return firstSide;
	}

	public double getSecondSide() {
		return secondSide;
	}

	public double getThirdSide() {
		return thirdSide;
	}

	@Override
	public String toString() {
		return "Triangle (" + firstSide + ", " + secondSide + ", " + thirdSide + ")";
	}		
	
}
