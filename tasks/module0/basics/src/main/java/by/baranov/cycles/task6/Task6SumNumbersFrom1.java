package by.baranov.cycles.task6;

public class Task6SumNumbersFrom1 {
	private long number;

	public Task6SumNumbersFrom1(long number) {
		this.number = number;
	}
	
	private long calculateSumNumbersFrom1() {
		long sum = calculate(number);
		return sum;
	}
	
	private long calculate(long maxNumber) {
		if(maxNumber >= 1) {
			return calculate(maxNumber - 1) + maxNumber;
		}
		return maxNumber;
	}
	public void printSumNumbersFrom1() {
		System.out.println("\n_TASK-6_\n");
		System.out.printf("total sum all numbers from 1 to %d equals %d%n", number,calculateSumNumbersFrom1() );
	}
}
