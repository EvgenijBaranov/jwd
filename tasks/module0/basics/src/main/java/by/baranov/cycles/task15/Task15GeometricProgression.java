package by.baranov.cycles.task15;

public class Task15GeometricProgression {
	private long firstMember;
	private long step;
	
	public Task15GeometricProgression(long firstMember, long step) {		
		this.firstMember = firstMember;
		this.step = step;
	}
	public void printSumProgression(long quantityMembers) {
		System.out.println("\n_TASK-15_\n");
		
		double sumMembers = calculateSumProgression(quantityMembers);
		System.out.printf("Geometric progression with first member = %d and step = %d  consists of %d members ,"
				+ "\n which sum = %-7.2f", firstMember, step, quantityMembers, sumMembers);
	}
	private double calculateSumProgression(long n) {
		double result = firstMember * (1 - Math.pow(step, n)) / (1 - step);
		return result;
	}
}
