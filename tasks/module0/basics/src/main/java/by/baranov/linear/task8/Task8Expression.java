package by.baranov.linear.task8;

public class Task8Expression {
	private double a;
	private double b;
	private double c;
	
	public Task8Expression(double a, double b, double c) {
		checkData(a,b,c);
		this.a = a;
		this.b = b;
		this.c = c;
	}
	private static void checkData(double a, double b, double c) {
		double discriminant = Math.pow(b,2) + 4*a*c;
		if(discriminant <= 0) {
			throw new IllegalArgumentException("Expression can't be calculated because of wrong data");
		} 
	}	

	public void printResultExpression() {
		System.out.printf("\n_TASK-8_\n\nif a = %5.2f , b = %5.2f , c = %5.2f %nthen expression equals = %7.2f",a, b, c, calculateExpression());
	}
	
	private double calculateExpression() {
		double result = (b + Math.sqrt(Math.pow(b,2) + 4*a*c))/(2*a) - Math.pow(a, 3) * c + Math.pow(b, -2); 
		return result;
	}
	
	
}
