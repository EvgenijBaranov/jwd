package by.baranov.cycles.task8;

public class Task8Function {
	private static final double C = 10;
	
	public void printValueFunction(double from, double to, double step) {
		if(from > to) {
			throw new IllegalArgumentException("Illegal input, left value must be more than right value");
		}
		System.out.println("\n\n_TASK-8_\n");
		System.out.printf("values of function(x,y) on the length from %-5.2f to %-5.2f are next: ", from, to);
		
		for(double i = from; i < to; i += step){
			System.out.print("("+ i +", "+ calculateY(i) +"), " );
		}		
	}
	
	private double calculateY(double x) {
		return x == 15 ? (x + C) * 2 : (x - C) + 6 ;
	}
}
