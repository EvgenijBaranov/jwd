package by.baranov.linear.task1;



public class Task1TwoNumbers {
	private double numberOne;
	private double numberTwo;
	
	public Task1TwoNumbers(double numberOne, double numberTwo){		
		 this.numberOne = numberOne;
		 this.numberTwo = numberTwo;
	}
	
	private double sum() {
		return numberOne + numberTwo;
	}
	private double difference() {
		return numberOne - numberTwo;
	}
	private double multiply() {
		return numberOne * numberTwo;
	}
	private double divide() {
		return numberOne / numberTwo;
	}
	public void printResult() {
		System.out.println("__ LINEAR __");
		System.out.println("\n_TASK-1_\n");		
		System.out.printf("%-10s : %5.2f , %5.2f%n", "Numbers",numberOne,numberTwo);
		System.out.printf("%-20s%n", "---------------------------");
		System.out.printf("%-10s : %9.2f%n", "Sum", sum());
		System.out.printf("%-10s : %9.2f%n", "Difference", difference());
		System.out.printf("%-10s : %9.2f%n", "Product", multiply());
		System.out.printf("%-10s : %9.2f%n", "Quotient", divide());
		
	}
}
