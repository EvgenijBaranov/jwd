package by.baranov.branching.task10;

public class Task10Circle {
	private double radius;

	public Task10Circle(double radius) {
		checkInputData(radius);
		this.radius = radius;
	}
	public static void printMinCircle(Task10Circle circle1, Task10Circle circle2){
		System.out.println("\n_Task-10_\n");
		Task10Circle minCircle = chooseMinCircle(circle1, circle2) ;
		System.out.println( "Between "+circle1 +" and "+ circle2+ " the smallest is "+ minCircle+ "\n");
	}
	
	private static Task10Circle chooseMinCircle (Task10Circle circle1, Task10Circle circle2){
		return circle1.radius <= circle2.getRadius() ? circle1 : circle2;
	}
	private void checkInputData(double radius){
		if(radius <= 0){
			throw new IllegalArgumentException(" illegal input of data, radius of circle can't be less than zero");
		}		
	}
	
	public double getRadius() {
		return radius;
	}
	@Override
	public String toString() {
		return "Circle (radius=" + radius + ") ";
	}
	
}
