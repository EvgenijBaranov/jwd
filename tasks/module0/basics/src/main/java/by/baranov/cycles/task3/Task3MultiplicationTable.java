package by.baranov.cycles.task3;

public class Task3MultiplicationTable {
	private double number;

	public Task3MultiplicationTable(double number) {
		this.number = number;
	}
	
	public void printMultiplicationTable() {
		System.out.println("\n_TASK-3_\n");
		System.out.printf("multiplication table on %-5.1f:%n", number);
		for(int i = 1; i < 10; i++) {
			System.out.println(number + " x " + i + " = " + number*i);
		}
		
	}
	
}
