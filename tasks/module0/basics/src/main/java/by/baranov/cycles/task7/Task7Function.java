package by.baranov.cycles.task7;

public class Task7Function {
	
	public void printValueFunction(double from, double to, double step) {
		if(from > to) {
			throw new IllegalArgumentException("Illegal input, left value must be more than right value");
		}
		System.out.println("\n_TASK-7_\n");
		System.out.printf("values of function(x,y) on the length from %-5.2f to %-5.2f are next: ", from, to);
		
		for(double i = from; i < to; i += step){
			System.out.print("("+ i +", "+ calculateY(i) +"), " );
		}		
	}
	
	private double calculateY(double x) {
		return x > 2 ? x : -x;
	}
}
