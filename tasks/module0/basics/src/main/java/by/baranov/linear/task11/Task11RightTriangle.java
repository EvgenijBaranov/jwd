package by.baranov.linear.task11;

public class Task11RightTriangle {
	private double sideOne;
	private double sideTwo;
	
	public Task11RightTriangle(double sideOne, double sideTwo) {
		if(sideOne < 0 || sideTwo < 0) {
			throw new IllegalArgumentException("it has input illegal data");
		}
		this.sideOne = sideOne;
		this.sideTwo = sideTwo;
	}
	 private double calculateSquare() {
		 return sideOne * sideTwo;
	 }
	public void printSquare() {
		System.out.printf("%n%n_TASK-11_%n%nFor right-triangle with sides: %-5.2f, %-5.2f%n has square = %-7.2f%n", sideOne, sideTwo, calculateSquare() );
	}
	
	
}
