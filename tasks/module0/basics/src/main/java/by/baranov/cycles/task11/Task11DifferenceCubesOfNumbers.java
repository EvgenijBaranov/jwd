package by.baranov.cycles.task11;

import java.math.BigDecimal;

public class Task11DifferenceCubesOfNumbers {
	private BigDecimal firstNumber;
	private BigDecimal lastNumber;
	
	public Task11DifferenceCubesOfNumbers(String firstNumber, String lastNumber) {
		this.firstNumber = new BigDecimal(firstNumber);		
		this.lastNumber = new BigDecimal(lastNumber);		
	}
	
	public void printDifferenceCubesOfNumbers() {
		System.out.println("\n_TASK-11_\n");
		BigDecimal differnce = calculateDifferenceCubes(firstNumber);
		System.out.printf("difference of cubes of numbers between %-5.2f and %-5.2f equals %-7.2f%n", firstNumber, lastNumber, differnce);		
	}
	
	private BigDecimal calculateDifferenceCubes(BigDecimal firstNumber) {
		if(firstNumber.compareTo(lastNumber) <= 0) {
			if(this.firstNumber == firstNumber) {
				return calculateDifferenceCubes(firstNumber.add(new BigDecimal("1")) ).add(firstNumber.pow(3)) ;
			}			
			return calculateDifferenceCubes(firstNumber.add(new BigDecimal("1")) ).subtract(firstNumber.pow(3)) ;
		}
		return new BigDecimal("0");
	}
}
