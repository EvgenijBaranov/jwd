package by.baranov.linear.task12;

public class Task12Point {
	private double x;
	private double y;
	
	public Task12Point(double x, double y) {
		this.x = x;
		this.y = y;
	}
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	@Override
	public String toString() {
		return "Point (x=" + x + ", y=" + y + ")";
	}

}
