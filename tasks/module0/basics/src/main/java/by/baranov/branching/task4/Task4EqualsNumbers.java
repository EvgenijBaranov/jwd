package by.baranov.branching.task4;

public class Task4EqualsNumbers {
	public static void compareNumbers(int first, int second){
		String result = first == second ? "equal":"not equal ";
		System.out.println("\n_TASK-4_\n");	
		System.out.printf("numbers %d and %d %s%n", first, second, result);
		
	}
	
}
