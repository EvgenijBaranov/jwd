package by.baranov.linear.task9;

public class Task9Expression {
	private double a;
	private double b;
	private double c;
	private double d;
	
	public Task9Expression(double a, double b, double c, double d) {
		checkInputData(a,b,c,d);
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
	}
	
	private static void checkInputData(double a, double b, double c, double d) {
		if(c == 0 || d== 0) {
			throw new IllegalArgumentException("Wrong input data, c and d can't be equal zero");
		} 
	}
	
	public void printResultExpression() {
		System.out.printf("\n\n_TASK-9_\n\nif a = %-5.2f, b = %-5.2f, c = %-5.2f, d = %-5.2f, %nthen expression equals = %-10.4f", a, b, c, d, calculateExpression());
	}
	
	private double calculateExpression() {
		double result = a / c * b / d - (a * b - c)/ c / d;
		return result;
	}
	
}
