package by.baranov.cycles.task14;

public class Task14SequenceNumbers {
	private long n;

	public Task14SequenceNumbers(long n) {
		if(n < 1) {
			throw new IllegalArgumentException("illegal input, quantity members of sequence can't be less than 1");
		}
		this.n = n;
	}
	
	public void printSequence() {
		System.out.println("\n_TASK-14_\n");
		double sumSequence = calculateSumMembersOfSequence(n);
		System.out.printf("if quantity of members of sequence equals %d , then sum of members = %-6.4f %n", n, sumSequence);
	}
	
	private double calculateSumMembersOfSequence(long n) {
		if(n > 1) {
			return calculateSumMembersOfSequence(n - 1) + Math.pow(n, -1);
		} return 1;  
	}
}
