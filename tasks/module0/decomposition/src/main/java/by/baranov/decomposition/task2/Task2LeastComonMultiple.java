package by.baranov.decomposition.task2;

public class Task2LeastComonMultiple {
	private long firstNumber;
	private long secondNumber;
	private Task2GreatestComonDivisor greatestComonDivisor;

	public Task2LeastComonMultiple(long firstNumber, long secondNumber) {
		checkInputData(firstNumber, secondNumber);
		this.firstNumber = firstNumber;
		this.secondNumber = secondNumber;
		greatestComonDivisor = new Task2GreatestComonDivisor(firstNumber, secondNumber);
	}

	private void checkInputData(long inputFirstNumber, long inputSecondNumber) {
		if (inputFirstNumber < 1 || inputSecondNumber < 1) {
			throw new IllegalArgumentException(" Invalid input, inputted numbers must be more than zero ");
		}
	}

	public long calculateLeastComonMultiple() {
		long leastComonMultiple = firstNumber * secondNumber / greatestComonDivisor.calculateGreatestComonDivisor();		
		return leastComonMultiple;
	}

	public long getFirstNumber() {
		return firstNumber;
	}

	public long getSecondNumber() {
		return secondNumber;
	}

	public Task2GreatestComonDivisor getGreatestComonDivisor() {
		return greatestComonDivisor;
	}

	public void setFirstNumber(long firstNumber) {
		this.firstNumber = firstNumber;
	}

	public void setSecondNumber(long secondNumber) {
		this.secondNumber = secondNumber;
	}

	public void setGreatestComonDivisor(Task2GreatestComonDivisor greatestComonDivisor) {
		this.greatestComonDivisor = greatestComonDivisor;
	}

	@Override
	public String toString() {
		return "LeastComonMultiple (" + firstNumber + ", " + secondNumber + ") is equal : " + calculateLeastComonMultiple();
	}
	
	
	
}
