package by.baranov.decomposition.task4;


public class Task4LeastCommonMultiple {	
	private long[] numbers;

	public Task4LeastCommonMultiple(long... numbers) {
		checkInputData(numbers);
		this.numbers = numbers;
	}

	private long calculateLeastComonMultiple(long... numbers) {
		long leastComonMultiple = numbers[0];
		for(int i = 1; i < numbers.length; i++) {
			leastComonMultiple = calculateLeastComonMultiple(leastComonMultiple, numbers[i]);			
		}
		return leastComonMultiple;
	}
	
	private long calculateLeastComonMultiple(long firstNumber, long secondNumber) {
		long leastComonMultiple = firstNumber * secondNumber / calculateGreatestComonDivisor(firstNumber, secondNumber);		
		return leastComonMultiple;
	}

	private long calculateGreatestComonDivisor(long firstNumber, long secondNumber) {
		long minNumber = firstNumber < secondNumber ? firstNumber : secondNumber;		
		while (minNumber > 0) {
			if (firstNumber % minNumber == 0 && secondNumber % minNumber == 0 ) {
				return minNumber;
			}
			minNumber--;
		}
		return minNumber;
	}

	private void checkInputData(long... numbers) {		
		if(numbers.length < 1) {
			throw new IllegalArgumentException(" Invalid input, quantity numbers must be more than zero ");
		}
		for(int i = 0; i < numbers.length; i++) {
			if (numbers[i] < 1) {
				throw new IllegalArgumentException(" Invalid input, inputted numbers must be more than zero ");
			}
		}
	}

	@Override
	public String toString() {
		return "LeastComonMultiple (" + numbersToString(this.numbers) +") is equal : " + calculateLeastComonMultiple(this.numbers);
	}
	
	private String numbersToString(long... numbers ) {
		String result = "";
		for(int i = 0; i < numbers.length; i++) {
			result += numbers[i];
			if(i < numbers.length - 1) {
				result += ", ";
			}
		}
		return result;
	}
}
