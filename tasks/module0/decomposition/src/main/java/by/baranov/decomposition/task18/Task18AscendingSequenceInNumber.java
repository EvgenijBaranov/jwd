package by.baranov.decomposition.task18;

public class Task18AscendingSequenceInNumber {
	private int exponentNumber;

	public Task18AscendingSequenceInNumber(int exponentNumber) {
		checkInputData(exponentNumber);
		this.exponentNumber = exponentNumber;
	}

	public void printAscendingNumber() {
		System.out.println("Numbers having " + exponentNumber + " digits and an ascending sequence of diqits :");
		for (int i = 1; i < 9; i++) {
			long number = defineAscendingNumber(i);
			if (number != 0) {
				System.out.print(number + ", ");
			}
		}
	}

	private long defineAscendingNumber(long firstNumber) {
		String numberString = String.valueOf(firstNumber);
		for (int i = 2; i <= exponentNumber; i++) {
			if (firstNumber + 1 > 9) {
				return 0;
			}
			numberString += ++firstNumber;
		}
		return Long.parseLong(numberString);
	}

	private void checkInputData(int exponentNumber) {
		if (exponentNumber < 1) {
			throw new IllegalArgumentException(" Invalid input, exponent number must be greater than zero ");
		}
	}
}
