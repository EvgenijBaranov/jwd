package by.baranov.decomposition.task5;

public class Task5SumMaxMinNumbers {
	private double[] numbers;

	public Task5SumMaxMinNumbers(double... numbers) {
		checkInputData(numbers);
		this.numbers = numbers;
	}

	public double sumMaxMinMemebers() {
		double maxMemeber = defineMaxMember();
		double minMember = defineMinMember();		
		return maxMemeber + minMember;
	}
	
	private double defineMaxMember() {
		double maxMember = numbers[0];
		for(double elem : numbers) {
			if(elem > maxMember) {
				maxMember = elem;
			}
		}	
		return maxMember;
	}

	private double defineMinMember() {
		double minMember = numbers[0];
		for(double elem : numbers) {
			if(elem < minMember) {
				minMember = elem;
			}
		}	
		return minMember;
	}


	private void checkInputData(double... numbers) {		
		if(numbers.length < 1) {
			throw new IllegalArgumentException(" Invalid input, quantity numbers must be more than zero ");
		}		
	}

	@Override
	public String toString() {
		return "For the sequence (" + numbersToString(this.numbers) +") sum maximum and minimum numbers is equal : " + sumMaxMinMemebers();
	}
	
	private String numbersToString(double... numbers ) {
		String result = "";
		for(int i = 0; i < numbers.length; i++) {
			result += numbers[i];
			if(i < numbers.length - 1) {
				result += ", ";
			}
		}
		return result;
	}

}
