package by.baranov.decomposition.task16;

import java.util.ArrayList;
import java.util.List;

public class Task16NumbersTwins {
	private int firstNumber;

	public Task16NumbersTwins(int firstNumber) {
		checkInputData(firstNumber);
		this.firstNumber = firstNumber;
	}

	private void checkInputData(int firstNumber) {
		if (firstNumber < 3) {
			throw new IllegalArgumentException(
					" Invalid input, the first number of twin numbers must be greater than 2 ");
		}
	}

	public void printTwinNumbers() {
		System.out.println("For a range from " + firstNumber + " to " + 2 * firstNumber + " the numbers of twins :");
		System.out.println(defineTwinNumbers());
	}

	private List<Task16TwinNumbers> defineTwinNumbers() {
		List<Task16TwinNumbers> list = new ArrayList<>();

		for (int i = firstNumber; i < 2 * firstNumber; i++) {
			if (isPrimeNumber(i) && isPrimeNumber(i + 2)) {
				list.add(new Task16TwinNumbers(i));
			}
		}
		return list;
	}

	private boolean isPrimeNumber(int number) {
		for (int i = 2; i < number; i++) {
			if (number % i == 0) {
				return false;
			}
		}
		return true;
	}

}
