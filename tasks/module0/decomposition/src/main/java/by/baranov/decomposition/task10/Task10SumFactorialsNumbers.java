package by.baranov.decomposition.task10;

public class Task10SumFactorialsNumbers {
	private long lowerLimit;
	private long upperLimit;

	public Task10SumFactorialsNumbers(long lowerLimit, long upperLimit) {
		checkInputData(lowerLimit, upperLimit);
		this.lowerLimit = lowerLimit;
		this.upperLimit = upperLimit;
	}

	public void printSumFactorialsLimits() {
		System.out.println("For range from " + lowerLimit + " to " + upperLimit
				+ " sum of factorials the numbers is equal : " + sumFactorialsNumbers());
	}

	private void checkInputData(long lowerLimit, long upperLimit) {
		if (lowerLimit < 1 || upperLimit < 1) {
			throw new IllegalArgumentException(" Invalid input, range of numbers can't be less than 1 ");
		}
		if (lowerLimit > upperLimit) {
			throw new IllegalArgumentException("Invalid input, upper limit must be greater than lower limit ");
		}
	}

	private long sumFactorialsNumbers() {
		long sumFactorials = 0;
		for (long i = lowerLimit; i <= upperLimit; i++) {
			sumFactorials += factorial(i);
		}
		return sumFactorials;
	}

	private long factorial(long number) {
		if (number > 1) {
			return factorial(number - 1) * number;
		}
		return 1;
	}

}
