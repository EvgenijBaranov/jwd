package by.baranov.decomposition.task7;

import java.util.Arrays;

public class Task7Plane {
	private Task7Point[] points;
	private Task7Point firstPointMaxDistance;
	private Task7Point secondPointMaxDistance;

	public Task7Plane(Task7Point... points) {
		checkInputData(points);
		this.points = points;
	}

	private void checkInputData(Task7Point... points) {
		if (points.length < 2) {
			throw new IllegalArgumentException(" Invalid input, quantity points must be greater than 1 ");
		}
	}

	private double defineMaxDistance(Task7Point... points) {
		double maxDistance = Double.MIN_VALUE;
		for (int i = 0; i < points.length; i++) {
			for (int j = i + 1; j < points.length; j++) {
				double distance = defineDistance(points[i], points[j]);
				if (distance > maxDistance) {
					maxDistance = distance;
					firstPointMaxDistance = points[i];
					secondPointMaxDistance = points[j];
				}
			}
		}
		return maxDistance;
	}

	private double defineDistance(Task7Point firstPoint, Task7Point secondPoint) {
		double distance = Math.hypot(firstPoint.getX() - secondPoint.getX(), firstPoint.getY() - secondPoint.getY());
		return distance;
	}

	@Override
	public String toString() {
		return "Plane has points " + Arrays.toString(points) + ", maximum distance "+ defineMaxDistance(points) +" between points {"
				+ firstPointMaxDistance + ", " + secondPointMaxDistance + "} ";
	}

}
