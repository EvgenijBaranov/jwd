package by.baranov.decomposition.task14;


public class Task14NumberWithMoreDigit {
	private double firstNumber;
	private double secondNumber;

	public Task14NumberWithMoreDigit(double firstNumber, double secondNumber) {
		this.firstNumber = firstNumber;
		this.secondNumber = secondNumber;
	}

	public void printWhereMoreDigits() {
		System.out.println("From number " + firstNumber + " and " + secondNumber
				+ " the number with the maximum number of digits is " + defineNumberMaxDigit());
	}

	private double defineNumberMaxDigit() {
		long quantityNumbersFirst = calculateQuantityDigits(firstNumber);
		long quantityNumbersSecond = calculateQuantityDigits(secondNumber);
		double numberMaxDigit = quantityNumbersFirst > quantityNumbersSecond ? firstNumber : secondNumber;
		return numberMaxDigit;
	}

	private long calculateQuantityDigits(double number) {
		String numberString = String.valueOf(number);
		char[] simbols = numberString.toCharArray();
		long quantityNumbers = simbols.length;
		for (int i = 0; i < simbols.length; i++) {
			if (simbols[i] == '.') {
				quantityNumbers--;
			}
		}
		return quantityNumbers;
	}

}
