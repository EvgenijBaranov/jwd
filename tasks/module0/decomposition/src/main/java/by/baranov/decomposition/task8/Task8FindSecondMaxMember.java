package by.baranov.decomposition.task8;

import java.util.Arrays;

public class Task8FindSecondMaxMember {
	private double[] array;

	public Task8FindSecondMaxMember(double... array) {
		checkInputData(array);
		this.array = array;
	}

	private void checkInputData(double... array) {
		if (array.length < 1) {
			throw new IllegalArgumentException(" Invalid input, array can't be empty ");
		}
	}
	
	public void printSecondMaxElement() {
		System.out.println("For array "+ Arrays.toString(array)+ " the second maximum element is equal : " + defineSeconMaxElement());
	}
	
	private double defineSeconMaxElement() {
		double secondMaxElelment = Double.MAX_VALUE;
		double maxElement = defineMaxElement();
		double minRemainder = Double.MAX_VALUE;
		for(double elem : array) {
			if(elem != maxElement && (maxElement - elem ) < minRemainder) {
				secondMaxElelment = elem;
				minRemainder = maxElement - elem;
			}
		}
		return secondMaxElelment;
	}

	private double defineMaxElement() {
		double maxElement = Double.MIN_VALUE;
		for(double elem : array) {
			if(elem > maxElement) {
				maxElement = elem;
			}
		}
		return maxElement;
	}

}
