package by.baranov.decomposition.task20;

public class Task20SubstractionDigitsFromNumber {
	private long number;

	public Task20SubstractionDigitsFromNumber(long number) {
		checkInputData(number);
		this.number = number;
	}

	public void printQuantityOperations() {
		System.out.println("If we want to achive zero by substracting the digits from the number " + number + ", "
				+ "then we need to perform this operation " + defineQuantityOperations() + " times");
	}

	private long defineQuantityOperations() {
		long quantityOperation = 0;
		long newNumber = number;
		while (newNumber > 0) {
			newNumber = substractDigits(newNumber);
			quantityOperation++;
		}
		return quantityOperation;
	}

	private long substractDigits(long number) {
		String numberString = String.valueOf(number);
		char[] digits = numberString.toCharArray();
		for (int i = 0; i < digits.length; i++) {
			number -= Long.parseLong(String.valueOf(digits[i]));
		}
		return number;
	}

	private void checkInputData(long number) {
		if (number < 1) {
			throw new IllegalArgumentException(" Invalid input, number must be greater than zero ");
		}
	}

}
