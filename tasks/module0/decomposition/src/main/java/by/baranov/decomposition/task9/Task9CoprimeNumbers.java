package by.baranov.decomposition.task9;

import java.util.Arrays;

public class Task9CoprimeNumbers {
	private double[] numbers;

	public Task9CoprimeNumbers(double... numbers) {
		checkInputData(numbers);
		this.numbers = numbers;
	}

	private void checkInputData(double[] numbers) {
		if (numbers.length < 2) {
			throw new IllegalArgumentException(" Invalid input, quantity numbers can't be less than 2 ");
		}
		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i] <= 0) {
				throw new IllegalArgumentException("Invalid input, inputted numbers must be more than zero ");
			}
		}
	}

	public void printResultComparison() {
		System.out.println("All numbers in sequence : " + Arrays.toString(numbers) + " is coprime numbers : "
				+ isCoprimeNumbers());
	}

	private boolean isCoprimeNumbers() {
		boolean isComprimeNumbers = true;
		for (int i = 0; i < numbers.length; i++) {
			for (int j = i + 1; j < numbers.length; j++) {
				if (isHasGreatestComonDivisor(numbers[i], numbers[j])) {
					return false;
				}
			}
		}

		return isComprimeNumbers;
	}

	private boolean isHasGreatestComonDivisor(double firstNumber, double secondNumber) {
		return calculateGreatestComonDivisor(firstNumber, secondNumber) == 1 ? false : true;
	}

	private double calculateGreatestComonDivisor(double firstNumber, double secondNumber) {
		double minNumber = firstNumber < secondNumber ? firstNumber : secondNumber;
		while (minNumber > 0) {
			if (firstNumber % minNumber == 0 && secondNumber % minNumber == 0) {
				return minNumber;
			}
			minNumber--;
		}
		return minNumber;
	}

}
