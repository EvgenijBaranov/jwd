package by.baranov.decomposition.task7;

import java.util.Random;

public class Task7GeneratorPoints {
	private static Random rand = new Random(10);
	
	public static Task7Point[] generatePoints(int quantityPoints) {
		checkInputData(quantityPoints);
		Task7Point[] points = new Task7Point[quantityPoints];
		for(int i = 0; i < quantityPoints; i++){
			points[i] = new Task7Point(rand.nextInt(10) , rand.nextInt(10));
		}
		
		return points;
	}
	
	private static void checkInputData(int quantityPoints) {
		if (quantityPoints < 1) {
			throw new IllegalArgumentException(" Invalid input, quantity points must be greater than zero ");
		}
	}
}
