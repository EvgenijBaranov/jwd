package by.baranov.decomposition.task17;

public class Task17NumberArmstrong {
	private long lowerLimit;
	private long upperLimit;

	public Task17NumberArmstrong(long lowerLimit, long upperLimit) {
		checkInputData(lowerLimit, upperLimit);
		this.lowerLimit = lowerLimit < upperLimit ? lowerLimit : upperLimit;
		this.upperLimit = lowerLimit > upperLimit ? lowerLimit : upperLimit;
	}

	public void printArmstrongNumbers() {
		System.out.println("For range from " + lowerLimit + " to " + upperLimit + " armstrong numbers is : ");
		for (long i = lowerLimit; i < upperLimit; i++) {
			if (isArmstrongNumber(i)) {
				System.out.print(i + ", ");
			}
		}
	}

	private boolean isArmstrongNumber(long number) {
		boolean isArmstrongNumber = false;
		long sumDigitsInPowQuantityNumbers = sumDigitsInPowQuantityNumbers(number);
		if (number == sumDigitsInPowQuantityNumbers) {
			isArmstrongNumber = true;
		}
		return isArmstrongNumber;

	}

	private long sumDigitsInPowQuantityNumbers(long number) {
		long quantityDigits = calculateQuantityDigits(number);
		long sumDigitsInPowQuantityNumber = 0;
		char[] simbols = defineArrayDigits(number);
		for (int i = 0; i < simbols.length; i++) {
			long digit = Long.valueOf(String.valueOf(simbols[i]));
			sumDigitsInPowQuantityNumber += Math.pow(digit, quantityDigits);
		}
		return sumDigitsInPowQuantityNumber;
	}

	private long calculateQuantityDigits(long number) {
		char[] simbols = defineArrayDigits(number);
		long quantityNumbers = simbols.length;
		return quantityNumbers;
	}

	private char[] defineArrayDigits(long number) {
		String numberString = String.valueOf(number);
		char[] simbols = numberString.toCharArray();
		return simbols;
	}

	private void checkInputData(long lowerLimit, long upperLimit) {
		if (lowerLimit < 1 || upperLimit < 1) {
			throw new IllegalArgumentException(" Invalid input, lower and upper limits must be greater than zero ");
		}
	}

}
