package by.baranov.decomposition.task1;

public class Task1Triangle {
	private Task1Apex firstApex;
	private Task1Apex secondApex;
	private Task1Apex thirdApex;

	public Task1Triangle(Task1Apex firstApex, Task1Apex secondApex, Task1Apex thirdApex) {
		this.firstApex = firstApex;
		this.secondApex = secondApex;
		this.thirdApex = thirdApex;
	}

	public double calculateSquare() {
		double firstSide = Math.hypot(firstApex.getX() - secondApex.getX(), firstApex.getY() - secondApex.getY());
		double secondSide = Math.hypot(firstApex.getX() - thirdApex.getX(), firstApex.getY() - thirdApex.getY());
		double thirdSide = Math.hypot(secondApex.getX() - thirdApex.getX(), secondApex.getY() - thirdApex.getY());
		double perimeter = firstSide + secondSide + thirdSide;
		double halfPerimeter = perimeter / 2;
		double square = Math.sqrt(halfPerimeter * (halfPerimeter - firstSide) * (halfPerimeter - secondSide) * (halfPerimeter - thirdSide));
		return square;
	}

	public Task1Apex getFirstApex() {
		return firstApex;
	}

	public Task1Apex getSecondApex() {
		return secondApex;
	}

	public Task1Apex getThirdApex() {
		return thirdApex;
	}

	public void setFirstApex(Task1Apex firstApex) {
		this.firstApex = firstApex;
	}

	public void setSecondApex(Task1Apex secondApex) {
		this.secondApex = secondApex;
	}

	public void setThirdApex(Task1Apex thirdApex) {
		this.thirdApex = thirdApex;
	}

	@Override
	public String toString() {
		return "Triangle [" + firstApex + "," + secondApex + "," + thirdApex + "]";
	}

}
