package by.baranov.decomposition.task6;

import org.decimal4j.util.DoubleRounder;

public class Task6RegularHexagon {
	private double lengthSide;

	public Task6RegularHexagon(double lengthSide) {
		checkInputData(lengthSide);
		this.lengthSide = lengthSide;
	}

	private double defineSquareRegularHexagon(double lengthSide) {
		return 6 * defineSquareRegularTriangle(lengthSide);
	}

	private double defineSquareRegularTriangle(double lengthSide) {
		double square = Math.pow(lengthSide, 2) * Math.sqrt(3) / 4;
		return square;
	}

	private void checkInputData(double lengthSide) {
		if (lengthSide <= 0) {
			throw new IllegalArgumentException(
					" Invalid input, the length of the regular hexagon must be greater than zero ");
		}
	}

	@Override
	public String toString() {
		return "RegularHexagon with length side = " + lengthSide + " has square equal : "
				+ DoubleRounder.round(defineSquareRegularHexagon(lengthSide), 3);
	}

}
