package by.baranov.decomposition.task15;

import java.util.Arrays;
import java.util.Random;

public class Task15ArrayElementsLimit {
	private int[] array = new int[10];
	private int upperLimitMember;
	private int sumMembers;
	private Random rand = new Random();

	public Task15ArrayElementsLimit(int upperLimitMember, int sumMembers) {
		checkInputData(upperLimitMember, sumMembers);
		this.upperLimitMember = upperLimitMember;
		this.sumMembers = sumMembers;
	}

	public void printFilledArray() {
		System.out.println("Array, which member less or equal than " + upperLimitMember
				+ " and sum of its members is equal " + sumMembers + ", has next form:");
		fillArray(upperLimitMember, sumMembers);
		System.out.println(Arrays.toString(array));

	}

	private void fillArray(int upperLimitMember, int maxSumMembers) {
		int maxMember = defineMaxElement(upperLimitMember, maxSumMembers);
		for (int i = 0; i < array.length; i++) {
			int temp = rand.nextInt(maxMember);
			if ((maxSumMembers - sumElementsTo(i) - temp) / maxMember > array.length - 2 - i) {
				if (i < array.length - 1) {
					array[i] = maxMember;
					continue;
				} else {
					array[i] = maxSumMembers - sumElementsTo(i);
				}

			} else if (maxSumMembers - sumElementsTo(i) - temp < 0) {
				array[i] = maxSumMembers - sumElementsTo(i);
				break;
			} else {
				array[i] = temp;
			}
		}
	}

	private int defineMaxElement(int upperLimitMember, int maxSumMembers) {
		return upperLimitMember > maxSumMembers ? maxSumMembers : upperLimitMember;
	}

	private int sumElementsTo(int index) {
		int sumElements = 0;
		for (int i = 0; i < index; i++) {
			sumElements += array[i];
		}
		return sumElements;
	}

	private void checkInputData(int upperLimitMember2, int sumMembers2) {
		if (upperLimitMember2 <= 0 || sumMembers2 <= 0) {
			throw new IllegalArgumentException(
					" Invalid input, upper limit member of the array nad their sum must be greater than zero ");
		} else if (sumMembers2 / upperLimitMember2 >= array.length) {
			throw new IllegalArgumentException(
					" Invalid input, such input doesn't allow to create an array, because the array consists of 10 members");
		}
	}
}
