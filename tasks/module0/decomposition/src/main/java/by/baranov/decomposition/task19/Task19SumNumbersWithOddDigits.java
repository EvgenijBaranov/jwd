package by.baranov.decomposition.task19;

public class Task19SumNumbersWithOddDigits {
	private int exponentNumber;

	public Task19SumNumbersWithOddDigits(int exponentNumber) {
		checkInputData(exponentNumber);
		this.exponentNumber = exponentNumber;
	}

	public void printSumNumbersWithOddDigits() {
		long sum = defineSumNumbersWithOddDigits();
		System.out.println("Sum of numbers having only odd digits for  " + exponentNumber + " number is equal : " + sum);
		System.out.println("The sum has " + defineQuantityEvenDigitInNumber(sum) + " even digits");
	}

	private long defineQuantityEvenDigitInNumber(long number) {
		long quantityEvenDigit = 0;
		String numberString = String.valueOf(number);
		char[] digits = numberString.toCharArray();
		for (int i = 0; i < digits.length; i++) {
			if (isEvenNumber(Long.parseLong(String.valueOf(digits[i])))) {
				quantityEvenDigit++;
			}
		}
		return quantityEvenDigit;
	}

	private long defineSumNumbersWithOddDigits() {
		long firstNumber = (long) Math.pow(10, exponentNumber - 1);
		long sumNumbersWithOddDigits = 0;
		for (long number = firstNumber; number < 10 * firstNumber; number++) {
			if (isNumberWithOddDigits(number)) {
				sumNumbersWithOddDigits += number;
			}
		}
		return sumNumbersWithOddDigits;
	}

	private boolean isNumberWithOddDigits(long number) {
		String numberString = String.valueOf(number);
		char[] digits = numberString.toCharArray();
		for (int i = 0; i < digits.length; i++) {
			if (isEvenNumber(Long.parseLong(String.valueOf(digits[i])))) {
				return false;
			}
		}
		return true;
	}

	private boolean isEvenNumber(long number) {
		return number % 2 == 0 ? true : false;
	}

	private void checkInputData(int exponentNumber) {
		if (exponentNumber < 1) {
			throw new IllegalArgumentException(" Invalid input, exponent number must be greater than zero ");
		}
	}
}
