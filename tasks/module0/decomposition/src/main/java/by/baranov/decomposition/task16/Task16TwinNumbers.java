package by.baranov.decomposition.task16;

public class Task16TwinNumbers {
	private long firstNumber;

	public Task16TwinNumbers(long firstNumber) {
		this.firstNumber = firstNumber;
	}

	@Override
	public String toString() {
		return "(" + firstNumber + ", " + (firstNumber + 2) + ")";
	}
}
