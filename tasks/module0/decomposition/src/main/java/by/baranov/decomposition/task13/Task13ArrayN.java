package by.baranov.decomposition.task13;

import java.util.Arrays;

public class Task13ArrayN {
	private long[] array;

	public void printArrayWith(long number) {

		fillArray(number);
		System.out.println("For number : " + number + " array with its digits has form : " + Arrays.toString(array));
	}

	private void fillArray(long number) {
		char[] simbols = defineArrayDigits(number);
		array = new long[simbols.length];
		for (int i = 0; i < simbols.length; i++) {
			array[i] = Long.parseLong(String.valueOf(simbols[i]));
		}
	}

	private char[] defineArrayDigits(long number) {
		String numberString = String.valueOf(number);
		char[] simbols = numberString.toCharArray();
		return simbols;
	}

}
