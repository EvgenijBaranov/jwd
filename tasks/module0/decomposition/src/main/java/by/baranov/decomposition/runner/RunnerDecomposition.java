package by.baranov.decomposition.runner;

import by.baranov.decomposition.task1.Task1Apex;
import by.baranov.decomposition.task1.Task1Triangle;
import by.baranov.decomposition.task10.Task10SumFactorialsNumbers;
import by.baranov.decomposition.task11.Task11Array;
import by.baranov.decomposition.task12.Task12Quadrangle;
import by.baranov.decomposition.task13.Task13ArrayN;
import by.baranov.decomposition.task14.Task14NumberWithMoreDigit;
import by.baranov.decomposition.task15.Task15ArrayElementsLimit;
import by.baranov.decomposition.task16.Task16NumbersTwins;
import by.baranov.decomposition.task17.Task17NumberArmstrong;
import by.baranov.decomposition.task18.Task18AscendingSequenceInNumber;
import by.baranov.decomposition.task19.Task19SumNumbersWithOddDigits;
import by.baranov.decomposition.task2.Task2LeastComonMultiple;
import by.baranov.decomposition.task20.Task20SubstractionDigitsFromNumber;
import by.baranov.decomposition.task3.Task3GreatestComonDivisor;
import by.baranov.decomposition.task4.Task4LeastCommonMultiple;
import by.baranov.decomposition.task5.Task5SumMaxMinNumbers;
import by.baranov.decomposition.task6.Task6RegularHexagon;
import by.baranov.decomposition.task7.Task7GeneratorPoints;
import by.baranov.decomposition.task7.Task7Plane;
import by.baranov.decomposition.task8.Task8FindSecondMaxMember;
import by.baranov.decomposition.task9.Task9CoprimeNumbers;

public class RunnerDecomposition {

	public static void main(String[] args) {
		System.out.println("\n_____DECOMPOSITION____\n");

		System.out.println("\n_TASK-1_\n");
		Task1Apex firstApex = new Task1Apex(0, 0);
		Task1Apex secondApex = new Task1Apex(3, 0);
		Task1Apex thirdApex = new Task1Apex(0, 4);
		Task1Triangle triangle = new Task1Triangle(firstApex, secondApex, thirdApex);
		System.out.println("For " + triangle + " square is equal : " + triangle.calculateSquare());

		System.out.println("\n_TASK-2_\n");
		Task2LeastComonMultiple task2 = new Task2LeastComonMultiple(24, 54);
		System.out.println(task2);
		System.out.println(task2.getGreatestComonDivisor());

		System.out.println("\n_TASK-3_\n");
		Task3GreatestComonDivisor task3 = new Task3GreatestComonDivisor(24, 54, 36, 18, 9);
		System.out.println(task3);

		System.out.println("\n_TASK-4_\n");
		Task4LeastCommonMultiple task4 = new Task4LeastCommonMultiple(24, 54, 36, 18);
		System.out.println(task4);

		System.out.println("\n_TASK-5_\n");
		Task5SumMaxMinNumbers task5 = new Task5SumMaxMinNumbers(4.5, 3.2, -2.3, 9, 6);
		System.out.println(task5);

		System.out.println("\n_TASK-6_\n");
		Task6RegularHexagon task6 = new Task6RegularHexagon(5);
		System.out.println(task6);

		System.out.println("\n_TASK-7_\n");
		Task7Plane task7 = new Task7Plane(Task7GeneratorPoints.generatePoints(6));
		System.out.println(task7);

		System.out.println("\n_TASK-8_\n");
		Task8FindSecondMaxMember task8 = new Task8FindSecondMaxMember(3, 2, 4, 5, 6, 9, -3, 0, 9.1);
		task8.printSecondMaxElement();

		System.out.println("\n_TASK-9_\n");
		Task9CoprimeNumbers task9 = new Task9CoprimeNumbers(3, 5, 7, 11, 1, 17, 10);
		task9.printResultComparison();

		System.out.println("\n_TASK-10_\n");
		Task10SumFactorialsNumbers task10 = new Task10SumFactorialsNumbers(1, 4);
		task10.printSumFactorialsLimits();

		System.out.println("\n_TASK-11_\n");
		Task11Array task11 = new Task11Array(3, 2, 7, 4, 1, 9, 11, 13);
		task11.printSumElements(1, 3);
		task11.printSumElements(3, 5);
		task11.printSumElements(4, 6);
		
		System.out.println("\n_TASK-12_\n");
		Task12Quadrangle task12 = new Task12Quadrangle(3, 4, 5, 6);
		task12.printSquareQuadrangle();
		
		System.out.println("\n_TASK-13_\n");
		Task13ArrayN task13 = new Task13ArrayN();
		task13.printArrayWith(1234567890);
		
		System.out.println("\n_TASK-14_\n");
		Task14NumberWithMoreDigit task14 = new Task14NumberWithMoreDigit(100.234 , 2.341465);
		task14.printWhereMoreDigits();
		
		System.out.println("\n_TASK-15_\n");
		Task15ArrayElementsLimit task15 = new Task15ArrayElementsLimit(10, 50);
		task15.printFilledArray();
		
		System.out.println("\n_TASK-16_\n");
		Task16NumbersTwins  task16 = new Task16NumbersTwins(25);
		task16.printTwinNumbers();
		
		System.out.println("\n_TASK-17_\n");
		Task17NumberArmstrong task17 = new Task17NumberArmstrong(1, 2000);
		task17.printArmstrongNumbers();
		
		System.out.println("\n\n_TASK-18_\n");
		Task18AscendingSequenceInNumber task18 = new Task18AscendingSequenceInNumber(5);
		task18.printAscendingNumber();
		
		System.out.println("\n\n_TASK-19_\n");
		Task19SumNumbersWithOddDigits task19 = new Task19SumNumbersWithOddDigits(2);
		task19.printSumNumbersWithOddDigits();
		
		System.out.println("\n_TASK-20_\n");
		Task20SubstractionDigitsFromNumber task20 = new Task20SubstractionDigitsFromNumber(1234);
		task20.printQuantityOperations();
		
	}

}
