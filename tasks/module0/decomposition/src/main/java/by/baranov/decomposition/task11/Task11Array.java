package by.baranov.decomposition.task11;

import java.util.Arrays;

public class Task11Array {
	private long[] numbers;

	public Task11Array(long... numbers) {
		checkInputData(numbers);
		this.numbers = numbers;
	}

	private void checkInputData(long... numbers) {
		if (numbers.length < 1) {
			throw new IllegalArgumentException(" Invalid input, quantity numbers must be more than zero ");
		}
	}

	public void printSumElements(int fromIndex, int toIndex) {
		checkInputIndex(fromIndex, toIndex);
		System.out.println("For array " + Arrays.toString(numbers) + " sum elements from index " + fromIndex
								+ " to index " + toIndex + " is equal : " + calculateSumElements(fromIndex, toIndex));
	}

	private void checkInputIndex(int fromIndex, int toIndex) {
		if (fromIndex > toIndex) {
			throw new IllegalArgumentException(" Invalid input, left index must be greater than right ");
		} else if (fromIndex < 0 || toIndex < 0) {
			throw new IllegalArgumentException(" Invalid input, indexes must be greater than zero ");
		}else if (fromIndex > numbers.length -1 || toIndex > numbers.length -1) {
			throw new IllegalArgumentException(" Invalid input, indexes must be less than quantity inputted numbers ");
		}
	}

	private long calculateSumElements(int fromIndex, int toIndex) {
		long sum = 0;
		for (int i = fromIndex; i <= toIndex; i++) {
			sum += numbers[i];
		}
		return sum;
	}
}
