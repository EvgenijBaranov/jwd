package by.baranov.decomposition.task3;

import java.util.Arrays;

public class Task3GreatestComonDivisor {
	private long[] numbers;

	public Task3GreatestComonDivisor(long... numbers) {
		checkInputData(numbers);
		this.numbers = numbers;
	}

	private void checkInputData(long... numbers) {
		if (numbers.length < 1) {
			throw new IllegalArgumentException(" Invalid input, quantity numbers must be more than zero ");
		}
		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i] < 1) {
				throw new IllegalArgumentException(" Invalid input, inputted numbers must be more than zero ");
			}
		}
	}

	private long calculateGreatestComonDivisor() {
		long[] array = numbers;
		Arrays.sort(array);
		long minNumber = array[0];

		while (minNumber > 0) {
			if (isGreatestComonDivisor(minNumber)) {
				return minNumber;
			}
			minNumber--;
		}
		return minNumber;
	}

	private boolean isGreatestComonDivisor(long greatestDevisor) {

		boolean isGreatestComonDivisor = true;

		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i] % greatestDevisor != 0) {
				return false;
			}
		}
		return isGreatestComonDivisor;
	}

	@Override
	public String toString() {
		return "GreatestComonDivisor (" + numbersToString(numbers) + ") is equal : " + calculateGreatestComonDivisor();
	}

	private String numbersToString(long... numbers) {
		String result = "";
		for (int i = 0; i < numbers.length; i++) {
			result += numbers[i];
			if (i < numbers.length - 1) {
				result += ", ";
			}			
		}
		return result;
	}
}
