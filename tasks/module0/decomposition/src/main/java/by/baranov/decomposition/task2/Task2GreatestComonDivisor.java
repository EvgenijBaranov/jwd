package by.baranov.decomposition.task2;

public class Task2GreatestComonDivisor {
	private long firstNumber;
	private long secondNumber;

	public Task2GreatestComonDivisor(long firstNumber, long secondNumber) {
		checkInputData(firstNumber, secondNumber);
		this.firstNumber = firstNumber;
		this.secondNumber = secondNumber;
	}

	private void checkInputData(long inputFirstNumber, long inputSecondNumber) {
		if (inputFirstNumber < 1 || inputSecondNumber < 1) {
			throw new IllegalArgumentException(" Invalid input, inputted numbers must be more than zero ");
		}
	}

	public long calculateGreatestComonDivisor() {
		
		long minNumber = firstNumber < secondNumber ? firstNumber : secondNumber;

		while (minNumber > 0) {
			if (firstNumber % minNumber == 0 && secondNumber % minNumber == 0) {
				return minNumber;
			}
			minNumber--;
		}
		return minNumber;
	}

	public long getFirstNumber() {
		return firstNumber;
	}

	public long getSecondNumber() {
		return secondNumber;
	}

	public void setFirstNumber(long firstNumber) {
		this.firstNumber = firstNumber;
	}

	public void setSecondNumber(long secondNumber) {
		this.secondNumber = secondNumber;
	}

	@Override
	public String toString() {
		return "GreatestComonDivisor (" + firstNumber + ", " + secondNumber + ") is equal : " + calculateGreatestComonDivisor();
	}

}
