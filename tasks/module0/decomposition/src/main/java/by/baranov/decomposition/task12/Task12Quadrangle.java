package by.baranov.decomposition.task12;

public class Task12Quadrangle {
	private double firstSide;
	private double secondSide;
	private double thirdSide;
	private double forthSide;

	public Task12Quadrangle(double firstSide, double secondSide, double thirdSide, double forthSide) {
		checkInputData(firstSide, secondSide, thirdSide, forthSide);
		this.firstSide = firstSide;
		this.secondSide = secondSide;
		this.thirdSide = thirdSide;
		this.forthSide = forthSide;
	}

	private void checkInputData(double firstSide, double secondSide, double thirdSide, double forthSide) {
		if (firstSide <= 0 || secondSide <= 0 || thirdSide <= 0 || forthSide <= 0) {
			throw new IllegalArgumentException(" Invalid input, length side must be greater than zero ");
		}
	}

	public void printSquareQuadrangle() {
		System.out.println("Quadrangle with sides X=" + firstSide + " Y=" + secondSide + " Z=" + thirdSide + "T="
				+ forthSide + " has square = " + calculateSquareQuadrangle());
	}

	private double calculateSquareQuadrangle() {
		double squareRightTriangleXY = firstSide * secondSide / 2;
		double commonSideTriangles = calculateDiagonalRightTriangle(firstSide, secondSide);
		double squareTriangleTZ = calculateSquareTriangle(commonSideTriangles, thirdSide, forthSide);
		return squareRightTriangleXY + squareTriangleTZ;
	}

	private double calculateDiagonalRightTriangle(double sideFirst, double sideSecond) {
		return Math.hypot(sideFirst, sideSecond);
	}

	private double calculateSquareTriangle(double sideFirst, double sideSecond, double sideThird) {
		double perimeter = sideFirst + sideSecond + sideThird;
		double halfPerimeter = perimeter / 2;
		double square = Math.sqrt(halfPerimeter * (halfPerimeter - sideFirst) * (halfPerimeter - sideSecond)
				* (halfPerimeter - sideThird));
		return square;
	}

}
