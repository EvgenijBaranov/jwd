package by.baranov.simpleobjects.task10.controller;

public interface AppCommandFactory {
	AppCommand getCommand(String commandName);
}
