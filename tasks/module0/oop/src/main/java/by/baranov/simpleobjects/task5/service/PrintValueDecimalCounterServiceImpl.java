package by.baranov.simpleobjects.task5.service;

import by.baranov.simpleobjects.task5.model.DecimalCounter;

public class PrintValueDecimalCounterServiceImpl implements PrintValueDecimalCounterService{

	@Override
	public void printValueDecimalCounter(DecimalCounter counter) {
		System.out.println(counter);
		
	}

}
