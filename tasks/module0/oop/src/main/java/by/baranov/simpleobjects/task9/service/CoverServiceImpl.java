package by.baranov.simpleobjects.task9.service;

import by.baranov.simpleobjects.task9.model.Cover;

public class CoverServiceImpl implements CoverService{

	@Override
	public Cover createCover(String coverName) {
		Cover[] covers = Cover.values();
		Cover coverFromString = null; 
		for(Cover cover : covers) {
			if(cover.name().compareToIgnoreCase(coverName) == 0) {
				coverFromString = cover;
			}
		}		
		return coverFromString;
	}
}
