package by.baranov.simpleobjects.task10.model;

import java.time.LocalTime;
import java.util.Arrays;

public class Airline {
	private String pointDestination;
	private String flightNumber;
	private TypeAircraft typeAircraft;
	private LocalTime timeDeparture;
	private Day[] daysDeparture;

	public Airline(String pointDestination, String flightNumber, TypeAircraft typeAircraft, 
						LocalTime timeDeparture, Day... daysDeparture) {
		this.pointDestination = pointDestination;
		this.flightNumber = flightNumber;
		this.typeAircraft = typeAircraft;
		this.timeDeparture = timeDeparture;
		this.daysDeparture = daysDeparture;
	}	
	
	public String getPointDestination() {
		return pointDestination;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public TypeAircraft getTypeAircraft() {
		return typeAircraft;
	}

	public LocalTime getTimeDeparture() {
		return timeDeparture;
	}

	public Day[] getDaysDeparture() {
		return daysDeparture;
	}

	public void setPointDestination(String pointDestination) {
		this.pointDestination = pointDestination;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public void setTypeAircraft(TypeAircraft typeAircraft) {
		this.typeAircraft = typeAircraft;
	}

	public void setTimeDeparture(LocalTime timeDeparture) {
		this.timeDeparture = timeDeparture;
	}

	public void setDaysDeparture(Day... daysDeparture) {
		this.daysDeparture = daysDeparture;
	}

	@Override
	public String toString() {
		return "Airline [" + pointDestination + ", flightNumber=" + flightNumber + ", aircraft="
				+ typeAircraft + ", timeDeparture=" + timeDeparture + ", daysDeparture="
				+ Arrays.toString(daysDeparture) + "]";
	}	
}
