package by.baranov.aggregationcomposition.task1.controller;

import by.baranov.aggregationcomposition.task1.model.Text;
import by.baranov.aggregationcomposition.task1.service.TextService;

public class PrintTitleCommand implements AppCommand {
	private final TextService textService;
	
	public PrintTitleCommand(TextService textService) {
		this.textService = textService;		
	}

	@Override
	public void execute(Text text) {
		textService.printTitle(text);
	}
}
