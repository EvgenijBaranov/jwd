package by.baranov.simpleobjects.task1.controller;

import java.util.Scanner;

import by.baranov.simpleobjects.task1.model.Test1;
import by.baranov.simpleobjects.task1.service.Test1Service;

public class NumbersChangerCommand implements AppCommand {

	private final Test1Service test1Service;

	public NumbersChangerCommand(Test1Service test1Service) {
		this.test1Service = test1Service;
	}

	@Override
	public void execute(Test1 test) {
		System.out.println("Enter first number(double)");
		double firstNumber = inputNumber();
		System.out.println("Enter second number(double)");
		double secondNumber = inputNumber();
		test1Service.changeValues(test, firstNumber, secondNumber);
	}

	private double inputNumber() {		
		double number = 0;
		boolean isNumberNotEntered = true;
		while(isNumberNotEntered) {
			try {
				Scanner scan = new Scanner(System.in);
				number = scan.nextDouble();
				isNumberNotEntered = false;
			}catch(RuntimeException ex) {
				System.out.println("You need to enter double number. Try again.");
			}
		}
		return number;
	}
}
