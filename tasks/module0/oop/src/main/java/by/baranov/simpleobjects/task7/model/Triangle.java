package by.baranov.simpleobjects.task7.model;

public class Triangle {
	private Point firstPoint;
	private Point secondPoint;
	private Point thirdPoint;
	

	public Triangle(Point firstPoint, Point secondPoint, Point thirdPoint) {
		this.firstPoint = firstPoint;
		this.secondPoint = secondPoint;
		this.thirdPoint = thirdPoint;
	}

	public Point getFirstPoint() {
		return firstPoint;
	}

	public Point getSecondPoint() {
		return secondPoint;
	}

	public Point getThirdPoint() {
		return thirdPoint;
	}

	public void setFirstPoint(Point firstApex) {
		this.firstPoint = firstApex;
	}

	public void setSecondPoint(Point secondApex) {
		this.secondPoint = secondApex;
	}

	public void setThirdPoint(Point thirdApex) {
		this.thirdPoint = thirdApex;
	}

	@Override
	public String toString() {
		return "Triangle [" + firstPoint + "," + secondPoint + "," + thirdPoint + "]";
	}
}
