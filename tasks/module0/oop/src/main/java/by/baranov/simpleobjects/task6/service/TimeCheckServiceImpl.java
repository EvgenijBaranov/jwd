package by.baranov.simpleobjects.task6.service;

import by.baranov.simpleobjects.task6.model.Time;

public class TimeCheckServiceImpl implements TimeCheckService{
	
	@Override
	public boolean checkAddHours(Time time, int quantityHours) {
		int newHour = time.getCurrentHours() + quantityHours;
		return newHour > Time.MIN_BOUND_HOURS && newHour < Time.MAX_BOUND_HOURS ? true : false;
	}

	@Override
	public boolean checkAddMinutes(Time time, int quantityMinutes) {
		int newMinute = time.getCurrentHours() + quantityMinutes;
		return newMinute > Time.MIN_BOUND_MINUTES && newMinute < Time.MAX_BOUND_MINUTES ? true : false;
	}

	@Override
	public boolean checkAddSeconds(Time time, int quantitySeconds) {
		int newSecond = time.getCurrentHours() + quantitySeconds;
		return newSecond > Time.MIN_BOUND_SECONDS && newSecond < Time.MAX_BOUND_SECONDS ? true : false;
	}

	@Override
	public boolean checkHours(int quantityHours) {		
		return quantityHours > Time.MIN_BOUND_MINUTES && quantityHours < Time.MAX_BOUND_MINUTES ? true : false;
	}

	@Override
	public boolean checkMinutes(int quantityMinutes) {
		return quantityMinutes > Time.MIN_BOUND_MINUTES && quantityMinutes < Time.MAX_BOUND_MINUTES ? true : false;
	}

	@Override
	public boolean checkSeconds(int quantitySeconds) {
		return quantitySeconds > Time.MIN_BOUND_SECONDS && quantitySeconds < Time.MAX_BOUND_SECONDS ? true : false;
	}

}
