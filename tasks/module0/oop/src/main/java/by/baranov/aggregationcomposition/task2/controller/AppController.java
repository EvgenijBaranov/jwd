package by.baranov.aggregationcomposition.task2.controller;

import by.baranov.aggregationcomposition.task2.model.Car;

public class AppController {
	private final AppCommandFactory commandFactory;

	public AppController(AppCommandFactory factory) {
		this.commandFactory = factory;
	}
	public void handleUserData(Car car, String commnadUser) {
        final AppCommand command = commandFactory.getCommand(commnadUser);
        command.execute(car);
    }	
}
