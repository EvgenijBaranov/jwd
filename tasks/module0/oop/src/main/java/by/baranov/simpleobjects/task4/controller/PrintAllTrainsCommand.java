package by.baranov.simpleobjects.task4.controller;

import java.util.List;

import by.baranov.simpleobjects.task4.model.Train;
import by.baranov.simpleobjects.task4.service.TrainsPrintService;

public class PrintAllTrainsCommand implements AppCommand {
	private TrainsPrintService trainsPrintService;

	public PrintAllTrainsCommand(TrainsPrintService trainsPrintService) {
		super();
		this.trainsPrintService = trainsPrintService;
	}

	@Override
	public void execute(List<Train> trains) {
		trainsPrintService.printTrains(trains);
	}
}
