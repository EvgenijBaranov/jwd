package by.baranov.simpleobjects.task9.controller;

public enum AppCommandName {
	PRINT_BOOKS("-p"),
	PRINT_BOOKS_OF_AUTHOR("-pba"),
	PRINT_BOOKS_OF_PUBLISHER("-pbp"),
	PRINT_BOOKS_AFTER_YEAR("-pbay");
	
	private String shortCommand;
	
	AppCommandName(String shortCommand){
		this.shortCommand = shortCommand;
	}
	
	 public static AppCommandName fromString(String name) {

	        final AppCommandName[] values = AppCommandName.values();
	        for (AppCommandName commandName : values) {
	            if (commandName.shortCommand.equals(name) || commandName.name().equals(name)) {
	            	System.out.println(commandName);
	                return commandName;
	            }
	        }
	        return null;
	 }

	public String getShortCommand() {
		return shortCommand;
	}
	 
}
