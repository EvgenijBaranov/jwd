package by.baranov.aggregationcomposition.task2.model;

public class PetrolTank {
	private static long counter = 0;
	private final long id = counter++;
	private double capacity;
	private double currentCapacity;

	public PetrolTank(double capacity) {
		this.capacity = capacity;
	}

	public double getCapacity() {
		return capacity;
	}

	public void setCapacity(double capacity) {
		this.capacity = capacity;
	}

	public double getCurrentCapacity() {
		return currentCapacity;
	}

	public void setCurrentCapacity(double currentCapacity) {
		this.currentCapacity = currentCapacity;
	}

	public long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "PetrolTank [id=" + id + ", capacity=" + capacity + ", currentCapacity=" + currentCapacity + "]";
	}

}
