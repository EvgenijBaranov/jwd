package by.baranov.simpleobjects.task4.controller;

import java.util.List;
import java.util.Scanner;

import by.baranov.simpleobjects.task4.model.Train;
import by.baranov.simpleobjects.task4.service.TrainsPrintService;

public class PrintTrainByNumberCommand implements AppCommand {
	private TrainsPrintService trainsPrintService;

	public PrintTrainByNumberCommand(TrainsPrintService trainsPrintService) {
		this.trainsPrintService = trainsPrintService;
	}

	@Override
	public void execute(List<Train> trains) {
		Scanner scan = new Scanner(System.in);
		String numberTrain = scan.next();
		trainsPrintService.printTrainByNumber(trains, numberTrain);
	}
}
