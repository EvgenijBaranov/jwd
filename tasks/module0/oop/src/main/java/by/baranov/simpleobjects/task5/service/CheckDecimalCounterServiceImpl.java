package by.baranov.simpleobjects.task5.service;

import by.baranov.simpleobjects.task5.model.DecimalCounter;

public class CheckDecimalCounterServiceImpl implements CheckDecimalCounterService{
	
	@Override
	public boolean checkForIncrease(DecimalCounter counter, int defaultIncreaseNumber) {
		return counter.getCurrentValue() + defaultIncreaseNumber <= DecimalCounter.MAX_VALUE ? true : false; 		
	}

	@Override
	public boolean checkForDecrease(DecimalCounter counter, int defaultDecreaseNumber) {
		return counter.getCurrentValue() - defaultDecreaseNumber >= DecimalCounter.MIN_VALUE ? true : false; 	
	}

}
