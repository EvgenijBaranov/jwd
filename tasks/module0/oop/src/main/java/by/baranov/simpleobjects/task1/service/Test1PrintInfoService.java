package by.baranov.simpleobjects.task1.service;

import by.baranov.simpleobjects.task1.model.Test1;

public interface Test1PrintInfoService {
	void printValues(Test1 test);
}
