package by.baranov.aggregationcomposition.task1.service;

import java.util.List;

import by.baranov.aggregationcomposition.task1.model.Sentence;
import by.baranov.aggregationcomposition.task1.model.Text;

public class TextServiceImpl implements TextService {

	@Override
	public Text createText(Sentence title, List<Sentence> sentences) {
		return new Text(title, sentences);
	}

	@Override
	public void addSentence(Text text, Sentence sentence) {
		List<Sentence> currentText = text.getContent();
		currentText.add(sentence);
		text.setContent(currentText);
	}

	@Override
	public void changeTitle(Text text, Sentence newTitle) {
		text.setTitle(newTitle);
	}

	@Override
	public void printText(Text text) {
		System.out.println(text.getContent());
	}

	@Override
	public void printTitle(Text text) {
		Sentence title = text.getTitle();
		System.out.println(title);
	}
}
