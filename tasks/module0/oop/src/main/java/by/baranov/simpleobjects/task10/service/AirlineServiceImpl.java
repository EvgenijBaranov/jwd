package by.baranov.simpleobjects.task10.service;

import java.time.LocalTime;

import by.baranov.simpleobjects.task10.model.Airline;
import by.baranov.simpleobjects.task10.model.Day;
import by.baranov.simpleobjects.task10.model.TypeAircraft;

public class AirlineServiceImpl implements AirlineService{

	@Override
	public Airline createAirline(String pointDestination, String flightNumber, TypeAircraft typeAircraft,
			LocalTime timeDeparture, Day... daysDeparture) {		
		
		Airline airline = new Airline(pointDestination, flightNumber, typeAircraft, timeDeparture, daysDeparture);
		
		return airline;
	}

}
