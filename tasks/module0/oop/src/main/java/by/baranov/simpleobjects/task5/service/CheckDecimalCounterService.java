package by.baranov.simpleobjects.task5.service;

import by.baranov.simpleobjects.task5.model.DecimalCounter;

public interface CheckDecimalCounterService {
	boolean checkForIncrease(DecimalCounter counter, int defaultIncreaseNumber);
	boolean checkForDecrease(DecimalCounter counter, int defaultDecreaseNumber);	
}
