package by.baranov.simpleobjects.task9.model;

public enum Cover {
	HARDCOVER, PAPERBACK
}
