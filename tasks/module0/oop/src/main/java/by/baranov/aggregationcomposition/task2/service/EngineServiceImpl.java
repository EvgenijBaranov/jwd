package by.baranov.aggregationcomposition.task2.service;

import java.time.LocalDate;

import by.baranov.aggregationcomposition.task2.model.Engine;

public class EngineServiceImpl implements EngineService{

	@Override
	public Engine createEngine(double power, LocalDate dateOfManufacture, double consumptionPetrol) {
		return new Engine(power, dateOfManufacture, consumptionPetrol);
	}

	@Override
	public void turnOn(Engine engine) {
		boolean isTurnedOn = engine.isTurnedOn();
		if(isTurnedOn == false){
			System.out.println("Engine is turned on");
			engine.setTurnedOn(true);
		}		
	}	
	
	@Override
	public void turnOff(Engine engine) {
		boolean isTurnedOn = engine.isTurnedOn();
		if(isTurnedOn == true){
			System.out.println("Engine is turned off");
			engine.setTurnedOn(false);
		}
	}

	@Override
	public boolean checkDataInput(Engine engine) {
		if (engine.getPower() <= 0 || engine.getDateOfManufacture().isAfter(LocalDate.now())
			|| engine.getConsumptionPetrol() <= 0) {
			return false;
		}
		return true;
	}

	@Override
	public boolean isTurnedOff(Engine engine) {
		return engine.isTurnedOn() ? false : true;		
	}
	
}
