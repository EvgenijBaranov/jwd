package by.baranov.simpleobjects.task4.service;

import java.util.List;

import by.baranov.simpleobjects.task4.model.Train;

public interface TrainsSortService {
	void sortTrainsByNumberTrain(List<Train> trains);	
	void sortTrainsByPointDestinationAndTimeDeparture(List<Train> trains);
}
