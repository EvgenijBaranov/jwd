package by.baranov.simpleobjects.task6.service;

import by.baranov.simpleobjects.task6.model.Time;

public interface TimeAddService {

	void addHours(Time time, int quantityHours);

	void addMinutes(Time time, int quantityMinutes);

	void addSeconds(Time time, int quantitySeconds);

}
