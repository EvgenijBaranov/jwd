package by.baranov.aggregationcomposition.task2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import by.baranov.aggregationcomposition.task2.controller.AppCommand;
import by.baranov.aggregationcomposition.task2.controller.AppCommandFactory;
import by.baranov.aggregationcomposition.task2.controller.AppCommandFactoryImpl;
import by.baranov.aggregationcomposition.task2.controller.AppCommandName;
import by.baranov.aggregationcomposition.task2.controller.AppController;
import by.baranov.aggregationcomposition.task2.controller.ChangeWheelCommand;
import by.baranov.aggregationcomposition.task2.controller.DriveCarCommand;
import by.baranov.aggregationcomposition.task2.controller.FillPetrolTankCommand;
import by.baranov.aggregationcomposition.task2.controller.PrintBrandCarCommand;
import by.baranov.aggregationcomposition.task2.controller.PrintCarCommand;
import by.baranov.aggregationcomposition.task2.model.BrandCar;
import by.baranov.aggregationcomposition.task2.model.Car;
import by.baranov.aggregationcomposition.task2.model.Engine;
import by.baranov.aggregationcomposition.task2.model.PetrolTank;
import by.baranov.aggregationcomposition.task2.model.Speedometer;
import by.baranov.aggregationcomposition.task2.model.Wheel;
import by.baranov.aggregationcomposition.task2.service.BrandCarService;
import by.baranov.aggregationcomposition.task2.service.BrandCarServiceImpl;
import by.baranov.aggregationcomposition.task2.service.CarService;
import by.baranov.aggregationcomposition.task2.service.CarServiceImpl;
import by.baranov.aggregationcomposition.task2.service.EngineService;
import by.baranov.aggregationcomposition.task2.service.EngineServiceImpl;
import by.baranov.aggregationcomposition.task2.service.PetrolTankService;
import by.baranov.aggregationcomposition.task2.service.PetrolTankServiceImpl;
import by.baranov.aggregationcomposition.task2.service.SpeedometerService;
import by.baranov.aggregationcomposition.task2.service.SpeedometerServiceImpl;
import by.baranov.aggregationcomposition.task2.service.WheelService;
import by.baranov.aggregationcomposition.task2.service.WheelServiceImpl;

public class App {

	public static void main(String[] args) {

		System.out.println("\n_TASK-2_\n");

		BrandCarService brandCarCreateService = new BrandCarServiceImpl();
		BrandCarService brandCarCheckService = new BrandCarServiceImpl();
		SpeedometerService speedometerCreateService = new SpeedometerServiceImpl();
		EngineService engineCreateService = new EngineServiceImpl();
		PetrolTankService petrolTankCreateService = new PetrolTankServiceImpl();
		WheelService wheelCreateService = new WheelServiceImpl();
		
		CarService carCreateService = new CarServiceImpl(engineCreateService, petrolTankCreateService,
				speedometerCreateService);
		CarService carCheckService = new CarServiceImpl(engineCreateService, petrolTankCreateService,
				speedometerCreateService);
		CarService carPrintService = new CarServiceImpl(engineCreateService, petrolTankCreateService,
				speedometerCreateService);
		CarService carPrintBrandService = new CarServiceImpl(engineCreateService, petrolTankCreateService,
				speedometerCreateService);
		CarService carChangeWheelService = new CarServiceImpl(engineCreateService, petrolTankCreateService,
				speedometerCreateService);
		CarService carFillPetrolTankService = new CarServiceImpl(engineCreateService, petrolTankCreateService,
				speedometerCreateService);
		CarService carDriveService = new CarServiceImpl(engineCreateService, petrolTankCreateService,
				speedometerCreateService);		

		AppCommand printCarCommand = new PrintCarCommand(carPrintService);
		AppCommand printBrandCarCommand = new PrintBrandCarCommand(carPrintBrandService);
		AppCommand changeWheelCommand = new ChangeWheelCommand(carChangeWheelService);
		AppCommand fillPetrolTankCommand = new FillPetrolTankCommand(carFillPetrolTankService);
		AppCommand driveCarCommand = new DriveCarCommand(carDriveService);

		Map<AppCommandName, AppCommand> commands = new EnumMap<>(AppCommandName.class);
		commands.put(AppCommandName.CHANGE_WHEEL, changeWheelCommand);
		commands.put(AppCommandName.DRIVE, driveCarCommand);
		commands.put(AppCommandName.FILL_PETROLTANK_CAR, fillPetrolTankCommand);
		commands.put(AppCommandName.PRINT_BRAND_CAR, printBrandCarCommand);
		commands.put(AppCommandName.PRINT_CAR, printCarCommand);
		
		AppCommandFactory factory = new AppCommandFactoryImpl(commands);
		AppController controller = new AppController(factory);

		System.out.println("You need to create a car.");
		BrandCar brandCar = null;
		boolean isNotEnteredBrandCar = true;
		while (isNotEnteredBrandCar) {
			System.out.println("Creation brand car.");
			System.out.println("Enter brand car(String):");
			Scanner scanBrandCar = new Scanner(System.in);
			String brandCarString = scanBrandCar.nextLine();
			brandCar = brandCarCreateService.createBrandCar(brandCarString);
			if (brandCar != null && brandCarCheckService.checkInputDataForBrandCar(brandCar)) {
				isNotEnteredBrandCar = false;
			} else {
				System.out.println("You entered wrong car brand. Try again.");
			}
		}

		Speedometer speedometer = speedometerCreateService.createSpeedometer();

		Engine engine = null;
		boolean isNotEnteredEngineCar = true;
		while (isNotEnteredEngineCar) {
			System.out.println("Creation engine car.");
			try {
				Scanner scanEngine = new Scanner(System.in);
				System.out.println("Enter power engine(double):");
				double powerEngine = scanEngine.nextDouble();
				System.out.println("Enter consumption petrol of the engine(double):");
				double consumptionPetrol = scanEngine.nextDouble();
				System.out.println("You need to enter date of manufacture for the engine (yyyy-MM-dd):");
				String date = scanEngine.next();
				LocalDate dateManufacture = LocalDate.parse(date);	
				engine = engineCreateService.createEngine(powerEngine, dateManufacture, consumptionPetrol);
				if (engineCreateService.checkDataInput(engine)) {
					isNotEnteredEngineCar = false;
				} else {
					System.out.println("You entered wrong data for car engine. Try again.");
				}
			} catch (RuntimeException ex) {
				System.out.println("You entered wrong data for car engine. Try again.");
			}
		}

		PetrolTank petrolTank = null;
		boolean isNotEnteredPetrolTankCar = true;
		while (isNotEnteredPetrolTankCar) {
			System.out.println("Creation petrol tank of the car.");
			try {
				Scanner scanPetrolTank = new Scanner(System.in);
				System.out.println("Enter petrol tank capacity(double):");
				double petrolTankCapacity = scanPetrolTank.nextDouble();
				petrolTank = petrolTankCreateService.createPetrolTank(petrolTankCapacity);
				if (petrolTankCreateService.checkInputDataForPetrolTank(petrolTank)) {
					isNotEnteredPetrolTankCar = false;
				} else {
					System.out.println("You entered wrong data for petrol tank of the car. Try again.");
				}
			} catch (RuntimeException ex) {
				System.out.println("You entered wrong data for petrol tank of the car. Try again.");
			}
		}
		Car car = null;
		boolean isWrongEnterWheels = true;
		while(isWrongEnterWheels) {
			List<Wheel> wheels = new ArrayList<>();
			boolean isNotEnteredWheelsCar = true;
			while (isNotEnteredWheelsCar) {
				System.out.println("Creation wheels for the car.");
				for(int i = 0; i < Car.NUMBERS_WHEEL; i++) {
					System.out.println("Creation car wheel number " + (i + 1));
					boolean isNotEnteredWheelCar = true;
					while(isNotEnteredWheelCar) {
						try {
							Scanner scanWheelCar = new Scanner(System.in);
							System.out.println("Enter wheel radius(double):");
							double radiusWheel = scanWheelCar.nextDouble();
							Wheel wheel = wheelCreateService.crateWheel(radiusWheel);
							if (wheelCreateService.checkInputDataForWheel(wheel)) {
								wheels.add(wheel);
								isNotEnteredWheelCar = false;
							} else {
								System.out.println("You entered wrong data car wheel number " + (i + 1) + ". Try again.");
							}
						} catch (RuntimeException ex) {
							System.out.println("You entered wrong data car wheel number " + (i + 1) + ". Try again.");
						}
					}
				}
				isNotEnteredWheelsCar = false;
				car = carCreateService.createCar(brandCar, engine, petrolTank, speedometer, wheels);
			}
			if(carCheckService.checkWheels(car)) {
				isWrongEnterWheels = false;
			} else {
				System.out.println("You entered wrong data for car wheels. They can't have different radius. Try again.");
			}
		}
		
		boolean isRunning = true;
		while (isRunning) {
			System.out.println();
			System.out.println(AppCommandName.CHANGE_WHEEL + " or " + AppCommandName.CHANGE_WHEEL.getShortCommand());
			System.out.println(AppCommandName.DRIVE + " or " + AppCommandName.DRIVE.getShortCommand());
			System.out.println(
					AppCommandName.FILL_PETROLTANK_CAR + " or " + AppCommandName.FILL_PETROLTANK_CAR.getShortCommand());
			System.out.println(
					AppCommandName.PRINT_BRAND_CAR + " or " + AppCommandName.PRINT_BRAND_CAR.getShortCommand());
			System.out.println(
					AppCommandName.PRINT_CAR + " or " + AppCommandName.PRINT_CAR.getShortCommand());

			System.out.println("for quit -  " + "-q");
			System.out.println("Your command is :");
			Scanner scan = new Scanner(System.in);
			String command = scan.next();
			if (command.equalsIgnoreCase("-q")) {
				isRunning = false;
			} else {
				controller.handleUserData(car, command);
			}
		}
	}
}

		
		
		
//PetrolTank petrolTank = new PetrolTank(50);
//Engine engine = new Engine(150.5, LocalDate.of(1996, 12, 21), 8);
//Wheel wheel1 = new Wheel(25);
//Wheel wheel2 = new Wheel(25);
//Wheel wheel3 = new Wheel(25);
//Wheel wheel4 = new Wheel(25);
//List<Wheel> wheels = Arrays.asList(new Wheel[] {wheel1,wheel2, wheel3, wheel4});
//Car car = new Car(BrandCar.TOYOTA, engine, petrolTank, wheels);
//System.out.println(car);
//car.drive(500);		
//System.out.println(car);
//car.changeWheel(3);
//System.out.println(car);
