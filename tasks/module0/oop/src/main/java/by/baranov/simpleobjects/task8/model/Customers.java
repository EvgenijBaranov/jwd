package by.baranov.simpleobjects.task8.model;

import java.util.Arrays;

public class Customers {
	private Customer[] customers;

	public Customers(Customer... customers) {
		this.customers = customers;
	}

	public Customer[] getCustomers() {
		return customers;
	}

	public void setCustomers(Customer[] customers) {
		this.customers = customers;
	}

	@Override
	public String toString() {
		return "Customers [" + Arrays.toString(customers) + "]";
	}
	
	

}
