package by.baranov.simpleobjects.task6.model;

public class Time {
	public static int MAX_BOUND_HOURS = 23;
	public static int MIN_BOUND_HOURS = 0;
	public static int MAX_BOUND_MINUTES = 59;
	public static int MIN_BOUND_MINUTES = 0;
	public static int MAX_BOUND_SECONDS = 59;
	public static int MIN_BOUND_SECONDS = 0;
	public static int DEFAULT_VALUE_FOR_WRONG_SET = 0;

	private int currentHours;
	private int currentMinutes;
	private int currentSeconds;
	
	public Time() {
		this.currentHours = MAX_BOUND_HOURS;
		this.currentMinutes = MAX_BOUND_MINUTES;
		this.currentSeconds = MAX_BOUND_SECONDS;
	}
	
	public int getCurrentHours() {
		return currentHours;
	}

	public int getCurrentMinutes() {
		return currentMinutes;
	}

	public int getCurrentSeconds() {
		return currentSeconds;
	}

	public void setCurrentHours(int currentHours) {
		this.currentHours = currentHours;
	}

	public void setCurrentMinutes(int currentMinutes) {
		this.currentMinutes = currentMinutes;
	}

	public void setCurrentSeconds(int currentSeconds) {
		this.currentSeconds = currentSeconds;
	}

	@Override
	public String toString() {
		return "Time [" + currentHours + " : " + currentMinutes + " : " + currentSeconds + "]";
	}

}
