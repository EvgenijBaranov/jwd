package by.baranov.simpleobjects.task4.model;

import java.time.LocalTime;

public class Train {
	private String numberTrain;
	private String pointDestination;
	private LocalTime timeDeparture;

	public Train(String numberTrain, String pointDestination, LocalTime timeDeparture) {
		this.numberTrain = numberTrain;
		this.pointDestination = pointDestination;
		this.timeDeparture = timeDeparture;	
	}

	public void setPointDestination(String pointDestination) {
		this.pointDestination = pointDestination;
	}

	public void setNumberTrain(String numberTrain) {
		this.numberTrain = numberTrain;
	}

	public void setTimeDeparture(LocalTime timeDeparture) {
		this.timeDeparture = timeDeparture;
	}

	public String getPointDestination() {
		return pointDestination;
	}

	public String getNumberTrain() {
		return numberTrain;
	}

	public LocalTime getTimeDeparture() {
		return timeDeparture;
	}

	@Override
	public String toString() {
		return "Train [numberTrain=" + numberTrain + ", destination=" + pointDestination + ", 	timeDeparture="
				+ timeDeparture + "]";
	}

}
