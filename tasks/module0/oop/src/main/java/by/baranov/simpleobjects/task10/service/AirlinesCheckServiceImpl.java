package by.baranov.simpleobjects.task10.service;

import java.time.LocalTime;

import by.baranov.simpleobjects.task10.model.Airline;
import by.baranov.simpleobjects.task10.model.Airlines;

public class AirlinesCheckServiceImpl implements AirlinesCheckService{

	@Override
	public boolean checkInputAirlines(Airlines airlines) {
		Airline[] arrayAirlines = airlines.getAirlines();
		for(Airline airline : arrayAirlines) {
			if(airline == null) {
				return false;
			}			
		}
		boolean result = false;
		for(int i = 0; i < arrayAirlines.length; i++) {
			for(int j = i + 1; j < arrayAirlines.length; j++) {
				boolean isEqualFlightNumbers = isEqualFlightNumbers(arrayAirlines[i].getFlightNumber(), 
																	arrayAirlines[j].getFlightNumber());
				boolean isEqualTimeDeparture = isEqualTimeDeparture(arrayAirlines[i].getTimeDeparture(), 
																	arrayAirlines[j].getTimeDeparture());
				boolean isNotEqualPointDestination = isNotEqualPointDestination(arrayAirlines[i].getPointDestination(), 
																				arrayAirlines[j].getPointDestination());		
				result = isEqualFlightNumbers && isEqualTimeDeparture && isNotEqualPointDestination;	
			}
		}
		return result;
	}
	
	
	private boolean isEqualFlightNumbers(String firstFlightNumber, String secondFlightNumber){
		return firstFlightNumber.equalsIgnoreCase(secondFlightNumber);
	}
	private boolean isEqualTimeDeparture(LocalTime firstTimeDeparture, LocalTime secondTimeDeparture){
		return firstTimeDeparture.compareTo(secondTimeDeparture) == 0 ? true : false;
	}
	private boolean isNotEqualPointDestination(String firstPointDestination, String secondPointDestination){
		return firstPointDestination.equalsIgnoreCase(secondPointDestination) ? false : true;
	}
}
