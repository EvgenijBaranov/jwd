package by.baranov.aggregationcomposition.task2.service;

import by.baranov.aggregationcomposition.task2.model.Wheel;

public interface WheelService {

	Wheel crateWheel(double radius);

	boolean checkInputDataForWheel(Wheel wheel);
}
