package by.baranov.aggregationcomposition.task1;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import by.baranov.aggregationcomposition.task1.controller.AddTextCommand;
import by.baranov.aggregationcomposition.task1.controller.AppCommand;
import by.baranov.aggregationcomposition.task1.controller.AppCommandFactory;
import by.baranov.aggregationcomposition.task1.controller.AppCommandFactoryImpl;
import by.baranov.aggregationcomposition.task1.controller.AppCommandName;
import by.baranov.aggregationcomposition.task1.controller.AppController;
import by.baranov.aggregationcomposition.task1.controller.PrintTextCommand;
import by.baranov.aggregationcomposition.task1.controller.PrintTitleCommand;
import by.baranov.aggregationcomposition.task1.model.Sentence;
import by.baranov.aggregationcomposition.task1.model.Text;
import by.baranov.aggregationcomposition.task1.model.Word;
import by.baranov.aggregationcomposition.task1.service.SentenceService;
import by.baranov.aggregationcomposition.task1.service.SentenceServiceImpl;
import by.baranov.aggregationcomposition.task1.service.TextService;
import by.baranov.aggregationcomposition.task1.service.TextServiceImpl;
import by.baranov.aggregationcomposition.task1.service.WordService;
import by.baranov.aggregationcomposition.task1.service.WordServiceImpl;

public class App {

	public static void main(String[] args) {

		System.out.println("\n_TASK-1_\n");

		WordService wordCreateService = new WordServiceImpl();
		SentenceService sentenceCreateService = new SentenceServiceImpl();

		TextService textCreateService = new TextServiceImpl();
		TextService textAddSentenceService = new TextServiceImpl();
		TextService textPrintService = new TextServiceImpl();
		TextService textTitlePrintService = new TextServiceImpl();

		AppCommand printTextCommand = new PrintTextCommand(textPrintService);
		AppCommand printTextTitleCommand = new PrintTitleCommand(textTitlePrintService);
		AppCommand AddTextCommand = new AddTextCommand(textAddSentenceService, sentenceCreateService, wordCreateService);

		Map<AppCommandName, AppCommand> commands = new EnumMap<>(AppCommandName.class);
		commands.put(AppCommandName.ADD_TEXT, AddTextCommand);
		commands.put(AppCommandName.PRINT_TEXT, printTextCommand);
		commands.put(AppCommandName.PRINT_TITLE_OF_TEXT, printTextTitleCommand);

		AppCommandFactory factory = new AppCommandFactoryImpl(commands);
		AppController controller = new AppController(factory);

		System.out.println("You need to create three sentences with three words in each.");
		
		List<Sentence> listSentencesForText = new ArrayList<>();
		List<Word> listWordsForSentence = new ArrayList<>();
		Sentence title = null;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.println("Creation word number " + (j + 1));
				Scanner scan = new Scanner(System.in);
				String wordString = scan.nextLine();
				Word word = wordCreateService.createWord(wordString);
				listWordsForSentence.add(word);
			}
			Sentence sentence = sentenceCreateService.createSentence(listWordsForSentence);
			listSentencesForText.add(sentence);
			if(i == 0) {
				title = sentence;
			}
		}
		
		Text text = textCreateService.createText(title, listSentencesForText);
		
		boolean isRunning = true;
		while (isRunning) {
			System.out.println();
			System.out.println(AppCommandName.ADD_TEXT + " or " + AppCommandName.ADD_TEXT.getShortCommand());
			System.out.println(AppCommandName.PRINT_TEXT + " or " + AppCommandName.PRINT_TEXT.getShortCommand());
			System.out.println(AppCommandName.PRINT_TITLE_OF_TEXT + " or " + AppCommandName.PRINT_TITLE_OF_TEXT.getShortCommand());		

			System.out.println("for quit -  " + "-q");
			System.out.println("Your command is :");
			Scanner scan = new Scanner(System.in);
			String command = scan.next();
			if (command.equalsIgnoreCase("-q")) {
				isRunning = false;
			} else {
				controller.handleUserData(text, command);
			}
		}
	}
}
