package by.baranov.simpleobjects.task8.service;

import by.baranov.simpleobjects.task8.model.Customer;

public class CustomerServiceImpl implements CustomerService{

	@Override
	public Customer createCustomer(String surname, String name, String patronymic,
									long creditCardNumber,	long bankAccountNumber) {	
		
		return new Customer(surname, name, patronymic, creditCardNumber, bankAccountNumber);
	}	
}
