package by.baranov.simpleobjects.task7.service;

import org.decimal4j.util.DoubleRounder;

import by.baranov.simpleobjects.task7.model.Point;
import by.baranov.simpleobjects.task7.model.Triangle;

public class TriangleServiceImpl implements TriangleService {

	@Override
	public Triangle createTriangle(Point firstPoint, Point secondPoint, Point thirdPoint) {		
		Triangle tringle  = new Triangle (firstPoint, secondPoint, thirdPoint);
		return tringle;
	}

	@Override
	public double calculateSquareTrinagle(Triangle triangle) {
		double firstSide = calculateLengthSideTriangle(triangle.getFirstPoint(), triangle.getSecondPoint());
		double secondSide = calculateLengthSideTriangle(triangle.getFirstPoint(), triangle.getThirdPoint());
		double thirdSide = calculateLengthSideTriangle(triangle.getSecondPoint(), triangle.getThirdPoint());
		double perimeter = firstSide + secondSide + thirdSide;
		double halfPerimeter = perimeter / 2;
		double square = Math.sqrt(halfPerimeter * (halfPerimeter - firstSide) 
												* (halfPerimeter - secondSide)
												* (halfPerimeter - thirdSide));
		return DoubleRounder.round(square, 3);
	}

	@Override
	public double calculatePerimeterTrinagle(Triangle triangle) {
		double firstSide = calculateLengthSideTriangle(triangle.getFirstPoint(), triangle.getSecondPoint());
		double secondSide = calculateLengthSideTriangle(triangle.getFirstPoint(), triangle.getThirdPoint());
		double thirdSide = calculateLengthSideTriangle(triangle.getSecondPoint(), triangle.getThirdPoint());
		double perimeter = firstSide + secondSide + thirdSide;
		return perimeter;
	}

	@Override
	public Point definePointIntersectionMedians(Triangle triangle) {
		double xFirstPoint = triangle.getFirstPoint().getX();
		double xSecondPoint = triangle.getSecondPoint().getX();
		double xThirdPoint = triangle.getThirdPoint().getX();
		double x = (xFirstPoint + xSecondPoint + xThirdPoint) / 3;
		double yFirstPoint = triangle.getFirstPoint().getY();
		double ySecondPoint = triangle.getSecondPoint().getY();
		double yThirdPoint = triangle.getThirdPoint().getY();
		double y = (yFirstPoint + ySecondPoint + yThirdPoint) / 3;
		Point pointIntersectionMedians = new Point(x, y);
		return pointIntersectionMedians;
	}

	
	private double calculateLengthSideTriangle(Point firstPoint, Point secondPoint) {
		double lengthSide = Math.hypot(firstPoint.getX() - secondPoint.getX(), firstPoint.getY() - secondPoint.getY());
		return lengthSide;
	}
}
