package by.baranov.simpleobjects.task3.service;

import java.util.List;

import by.baranov.simpleobjects.task3.model.Student;

public interface StudentService {
	List<Student> findExcelentStudents(List<Student> students);
}
