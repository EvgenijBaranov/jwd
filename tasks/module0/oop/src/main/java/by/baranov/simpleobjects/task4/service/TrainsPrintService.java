package by.baranov.simpleobjects.task4.service;

import java.util.List;

import by.baranov.simpleobjects.task4.model.Train;

public interface TrainsPrintService {
	void printTrains(List<Train> trains);
	void printTrainByNumber(List<Train> trains, String numberTrain);
}
