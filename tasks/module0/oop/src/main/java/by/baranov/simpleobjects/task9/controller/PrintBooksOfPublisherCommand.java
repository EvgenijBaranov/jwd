package by.baranov.simpleobjects.task9.controller;

import java.util.Scanner;

import by.baranov.simpleobjects.task9.model.Books;
import by.baranov.simpleobjects.task9.service.BooksService;

public class PrintBooksOfPublisherCommand implements AppCommand {
	private final BooksService booksService;

	public PrintBooksOfPublisherCommand(BooksService booksService) {
		this.booksService = booksService;
	}

	@Override
	public void execute(Books books) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter name of the publisher");
		String publisherName = scan.next();
		booksService.printBooksOfPublisher(books, publisherName);;

	}
}
