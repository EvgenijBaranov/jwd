package by.baranov.simpleobjects.task6.controller;

import by.baranov.simpleobjects.task6.model.Time;

public interface AppCommand {
	void execute(Time time);
}
