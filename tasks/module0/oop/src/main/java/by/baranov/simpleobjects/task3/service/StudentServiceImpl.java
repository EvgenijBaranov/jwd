package by.baranov.simpleobjects.task3.service;

import java.util.ArrayList;
import java.util.List;

import by.baranov.simpleobjects.task3.model.Student;

public class StudentServiceImpl implements StudentService {
	private static final int EXCELENT_MARK = 9;
	private List<Student> excelentStudent = new ArrayList<>();

	@Override
	public List<Student> findExcelentStudents(List<Student> students) {
		for (Student student : students) {
			if (isAdvancedStudent(student)) {
				excelentStudent.add(student);
			}
		}
		return excelentStudent;
	}

	private boolean isAdvancedStudent(Student student) {
		for (int i = 0; i < student.getMarks().length; i++) {
			if (student.getMarks()[i] < EXCELENT_MARK) {
				return false;
			}
		}
		return true;
	}
}
