package by.baranov.simpleobjects.task5.service;

import by.baranov.simpleobjects.task5.model.DecimalCounter;

public interface PrintValueDecimalCounterService {
	void printValueDecimalCounter(DecimalCounter counter);
}
