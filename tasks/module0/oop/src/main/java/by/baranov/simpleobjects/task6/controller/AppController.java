package by.baranov.simpleobjects.task6.controller;

import by.baranov.simpleobjects.task6.model.Time;

public class AppController {
	private final AppCommandFactory commandFactory;

	public AppController(AppCommandFactory factory) {
		this.commandFactory = factory;
	}
	public void handleUserData(Time time, String commnadUser) {
        final AppCommand command = commandFactory.getCommand(commnadUser);
        command.execute(time);
    }	
}
