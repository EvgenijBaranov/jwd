package by.baranov.simpleobjects.task1.controller;

import by.baranov.simpleobjects.task1.model.Test1;

public interface AppCommand {
	void execute(Test1 test);
}
