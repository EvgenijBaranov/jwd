package by.baranov.simpleobjects.task8.service;

import by.baranov.simpleobjects.task8.model.Customer;

public interface CustomersCheckService {

	boolean checkCustomersInput(Customer... customers);
	
	boolean checkInputedBounds(long fromNumberCreditCard, long toNumberCreditCard);

	
	
}
