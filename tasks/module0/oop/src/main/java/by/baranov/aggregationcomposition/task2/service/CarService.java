package by.baranov.aggregationcomposition.task2.service;

import java.util.List;

import by.baranov.aggregationcomposition.task2.model.BrandCar;
import by.baranov.aggregationcomposition.task2.model.Car;
import by.baranov.aggregationcomposition.task2.model.Engine;
import by.baranov.aggregationcomposition.task2.model.PetrolTank;
import by.baranov.aggregationcomposition.task2.model.Speedometer;
import by.baranov.aggregationcomposition.task2.model.Wheel;

public interface CarService {

	Car createCar(BrandCar brand, Engine engine, PetrolTank petrolTank, Speedometer speedometer, List<Wheel> wheels);

	boolean checkWheels(Car car);

	void drive(Car car, double distance);

	void changeWheel(Car car, long idWheel);

	boolean isExistWheelForCar(Car car, long idWheel);

	void fillPetrolTank(Car car);
	
	void printBrandCar(Car car);
	
	void printCar(Car car);
}
