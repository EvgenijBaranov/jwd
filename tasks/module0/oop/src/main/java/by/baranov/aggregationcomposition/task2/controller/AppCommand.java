package by.baranov.aggregationcomposition.task2.controller;

import by.baranov.aggregationcomposition.task2.model.Car;

public interface AppCommand {
	void execute(Car car);
}
