package by.baranov.simpleobjects.task9.service;

import by.baranov.simpleobjects.task9.model.Book;
import by.baranov.simpleobjects.task9.model.Cover;

public class BookServiceImpl implements BookService {

	@Override
	public Book createBook(String title, String publisher, int publishingYear, int quantityPages,
							String price, Cover typeCover, String... namesAuthors) {
		
		String[] authors = namesAuthors;
		Book book = new Book(title, publisher, publishingYear, quantityPages, price, typeCover, authors);
		
		return book;
	}

}
