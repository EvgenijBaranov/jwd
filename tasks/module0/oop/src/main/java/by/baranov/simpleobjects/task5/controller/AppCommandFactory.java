package by.baranov.simpleobjects.task5.controller;

public interface AppCommandFactory {
	AppCommand getCommand(String commandName);
}
