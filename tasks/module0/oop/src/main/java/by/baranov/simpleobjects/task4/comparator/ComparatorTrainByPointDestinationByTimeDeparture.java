package by.baranov.simpleobjects.task4.comparator;

import java.util.Comparator;

import by.baranov.simpleobjects.task4.model.Train;

public class ComparatorTrainByPointDestinationByTimeDeparture implements Comparator<Train>{

	@Override
	public int compare(Train train1, Train train2) {
		int resultComparison = train1.getPointDestination().compareTo(train2.getPointDestination());
		if(resultComparison != 0) {
			return resultComparison;
		}
		return train1.getTimeDeparture().compareTo(train2.getTimeDeparture());
	}

}
