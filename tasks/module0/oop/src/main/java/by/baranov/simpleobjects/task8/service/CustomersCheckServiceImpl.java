package by.baranov.simpleobjects.task8.service;

import by.baranov.simpleobjects.task8.model.Customer;

public class CustomersCheckServiceImpl implements CustomersCheckService {

	@Override
	public boolean checkCustomersInput(Customer... customers) {		
		for (int i = 0; i < customers.length; i++) {
			for (int j = i + 1; j < customers.length; j++) {
				if (customers[i].getBankAccountNumber() == customers[j].getBankAccountNumber()) {
					return false;
				} else if (customers[i].getCreditCardNumber() == customers[j].getCreditCardNumber()) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public boolean checkInputedBounds(long fromNumberCreditCard, long toNumberCreditCard) {
		return fromNumberCreditCard < toNumberCreditCard ? true : false;
	}	

}
