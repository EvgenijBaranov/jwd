package by.baranov.aggregationcomposition.task1.service;

import java.util.List;

import by.baranov.aggregationcomposition.task1.model.Sentence;
import by.baranov.aggregationcomposition.task1.model.Word;

public class SentenceServiceImpl implements SentenceService {

	@Override
	public Sentence createSentence(List<Word> words) {
		return new Sentence(words);
	}

	@Override
	public List<Word> getAllWords(Sentence sentence) {
		List<Word> list = sentence.getWords();
		return list;
	}

	@Override
	public void addWord(Sentence sentence, Word word) {
		List<Word> currentListWords = sentence.getWords();
		currentListWords.add(word);
		sentence.setWords(currentListWords);
	}

	@Override
	public void printSentence(Sentence sentence) {
		System.out.println(sentence);
	}

}
