package by.baranov.simpleobjects.task9.service;

import by.baranov.simpleobjects.task9.model.Book;
import by.baranov.simpleobjects.task9.model.Books;

public interface BooksService {

	Books createBooks(Book... books);

	void printBooksOfAuthor(Books books, String author);

	void printBooksOfPublisher(Books books, String publisher);

	void printBooksAfterYear(Books books, int year);

	void printAllBooks(Books books);
}
