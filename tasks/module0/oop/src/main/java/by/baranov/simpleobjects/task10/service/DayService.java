package by.baranov.simpleobjects.task10.service;

import by.baranov.simpleobjects.task10.model.Day;

public interface DayService {
	Day createDay(String dayName);
}
