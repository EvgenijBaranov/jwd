package by.baranov.simpleobjects.task4.comparator;

import java.util.Comparator;

import by.baranov.simpleobjects.task4.model.Train;

public class ComparatorTrainNumber implements Comparator<Train>{
	@Override
	public int compare(Train train1, Train train2) {
		return train1.getNumberTrain().compareTo(train2.getNumberTrain());
	}
}
