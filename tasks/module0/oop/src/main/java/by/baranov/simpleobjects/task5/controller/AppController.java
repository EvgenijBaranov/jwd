package by.baranov.simpleobjects.task5.controller;

import by.baranov.simpleobjects.task5.model.DecimalCounter;

public class AppController {
	private final AppCommandFactory commandFactory;

	public AppController(AppCommandFactory factory) {
		this.commandFactory = factory;
	}
	public void handleUserData(DecimalCounter counter, String commnadUser) {
        final AppCommand command = commandFactory.getCommand(commnadUser);
        command.execute(counter);
    }	
}
