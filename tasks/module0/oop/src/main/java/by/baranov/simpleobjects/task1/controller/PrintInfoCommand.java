package by.baranov.simpleobjects.task1.controller;

import by.baranov.simpleobjects.task1.model.Test1;
import by.baranov.simpleobjects.task1.service.Test1PrintInfoService;

public class PrintInfoCommand implements AppCommand{
	private final Test1PrintInfoService test1Service;

	public PrintInfoCommand(Test1PrintInfoService test1Service) {
		super();
		this.test1Service = test1Service;
	}

	@Override
	public void execute(Test1 test) {
		test1Service.printValues(test);		
	}
	
}
