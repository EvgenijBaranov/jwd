package by.baranov.simpleobjects.task10.service;

import java.time.LocalTime;

import by.baranov.simpleobjects.task10.model.Day;
import by.baranov.simpleobjects.task10.model.TypeAircraft;

public class AirlineCheckServiceImpl implements AirlineCheckService {

	@Override
	public boolean chekInputDataForAirline(String pointDestination, String flightNumber, TypeAircraft typeAircraft,
			LocalTime timeDeparture, Day... daysDeparture) {
		boolean isRightInputPointDestination = isStringNotEmpty(pointDestination);
		boolean isRightInputFlightNumber = isStringNotEmpty(flightNumber);
		boolean isRightInputTypeAircraft = isTypeAircraft(typeAircraft);
		boolean isRightInputTimeDeparture = checkInputHourAndMinuteForTimeDeparture(timeDeparture);
		boolean isRightInputВaysDeparture = isDaysDeparture(daysDeparture);

		boolean result = isRightInputPointDestination && isRightInputFlightNumber && isRightInputTypeAircraft
							&& isRightInputTimeDeparture && isRightInputВaysDeparture;		
		return result;
	}

	@Override
	public boolean checkInputHourAndMinuteForTimeDeparture(LocalTime timeDeparture) {
		boolean isRightHour = timeDeparture.getHour() <= LocalTime.MAX.getHour()
				|| timeDeparture.getHour() >= LocalTime.MIN.getHour();
		boolean isRightMinute = timeDeparture.getMinute() <= LocalTime.MAX.getMinute()
				|| timeDeparture.getMinute() >= LocalTime.MIN.getMinute();

		return isRightHour && isRightMinute;
	}

	@Override
	public boolean isStringNotEmpty(String... strings) {
		for (String string : strings) {
			if (string.isEmpty() || string == null) {
				return false;
			}
		}
		return true;
	}

	private boolean isTypeAircraft(TypeAircraft typeAircraft) {
		return typeAircraft == null ? false : true;
	}

	private boolean isDaysDeparture(Day... daysDeparture) {
		for (Day dayDeparture : daysDeparture) {
			if (dayDeparture == null) {
				return false;
			}
		}
		return true;
	}
}
