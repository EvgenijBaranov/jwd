package by.baranov.simpleobjects.task3.controller;

import java.util.List;

import by.baranov.simpleobjects.task3.model.Student;
import by.baranov.simpleobjects.task3.service.StudentService;
import by.baranov.simpleobjects.task3.service.StudentsPrintInfoService;

public class FindExcelentStudentsCommand implements AppCommand{
	private final StudentService studentService;
	private final StudentsPrintInfoService printService;
	
	public FindExcelentStudentsCommand(StudentService studentService, StudentsPrintInfoService printService) {
		this.studentService = studentService;
		this.printService = printService;
	}

	@Override
	public void execute(List<Student> students) {
		List<Student> excelentStudents = studentService.findExcelentStudents(students);
		printService.printStudents(excelentStudents);
	}

}
