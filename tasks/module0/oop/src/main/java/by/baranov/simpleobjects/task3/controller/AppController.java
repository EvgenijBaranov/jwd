package by.baranov.simpleobjects.task3.controller;

import java.util.List;

import by.baranov.simpleobjects.task3.model.Student;

public class AppController {
	private final AppCommandFactory commandFactory;

	public AppController(AppCommandFactory factory) {
		this.commandFactory = factory;
	}
	public void handleUserData(List<Student> students, String commnadUser) {
        final AppCommand command = commandFactory.getCommand(commnadUser);
        command.execute(students);
    }	
}
