package by.baranov.simpleobjects.task8.service;

import java.util.Arrays;

import by.baranov.simpleobjects.task8.comparator.ComparatorCustomerNameAlphabetically;
import by.baranov.simpleobjects.task8.model.Customer;
import by.baranov.simpleobjects.task8.model.Customers;

public class CustomersServiceImpl implements CustomersService{

	@Override
	public Customers createCustomers(Customer... customers) {
		Customer[] arrayCustomers = customers;
		return new Customers(arrayCustomers);
	}

	@Override
	public void printCustomersAlphabetically(Customers customers) {
		Customer[] arrayCustomers = customers.getCustomers();
		Arrays.sort(arrayCustomers, new ComparatorCustomerNameAlphabetically());
		System.out.println("\n Customer list alphabetically : \n" + Arrays.toString(arrayCustomers));		
	}

	@Override
	public void printCustomersWithBankAccountNumber(Customers customers, long bankAccountNumberFrom,
																		long bankAccountNumberTo) {
		Customer[] arrayCustomers = customers.getCustomers();
		Arrays.sort(arrayCustomers, new ComparatorCustomerNameAlphabetically());
		for (Customer customer : arrayCustomers) {
			if (customer.getBankAccountNumber() >= bankAccountNumberFrom && customer.getBankAccountNumber() <= bankAccountNumberTo) {
				System.out.print(customer);
			}
		}
		
	}

	@Override
	public void printCustomers(Customers customers) {
		Customer[] arrayCustomers = customers.getCustomers();		
		System.out.println("\n Customers : \n" + Arrays.toString(arrayCustomers));
		
	}	

}
