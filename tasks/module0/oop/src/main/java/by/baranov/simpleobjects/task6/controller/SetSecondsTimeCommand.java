package by.baranov.simpleobjects.task6.controller;

import java.util.Scanner;

import by.baranov.simpleobjects.task6.model.Time;
import by.baranov.simpleobjects.task6.service.TimeCheckService;
import by.baranov.simpleobjects.task6.service.TimeSetService;

public class SetSecondsTimeCommand implements AppCommand {
	private final  TimeSetService timeSetService;
	private final  TimeCheckService timeCheckService;	
	
	public SetSecondsTimeCommand(TimeSetService timeSetService, TimeCheckService timeCheckService) {
		this.timeSetService = timeSetService;
		this.timeCheckService = timeCheckService;
	}

	@Override
	public void execute(Time time) {
		System.out.println("Enter seconds : ");
		boolean isRunning = true;
		int inputedSeconds = 0;
		while(isRunning) {
			Scanner scan = new Scanner(System.in);
			try {
				inputedSeconds = scan.nextInt();
				isRunning = false;
			}catch(RuntimeException e) {
				System.out.println("You must input integer number, try again :");
			}			
		}
		if(timeCheckService.checkMinutes(inputedSeconds)) {
			timeSetService.setSeconds(time, inputedSeconds);
		} else {
			System.out.println("Invalid seconds input, seconds must be in the range from " + Time.MIN_BOUND_SECONDS
					+ " to " + Time.MAX_BOUND_SECONDS + " , seconds are set to " + Time.DEFAULT_VALUE_FOR_WRONG_SET);
			timeSetService.setSeconds(time, Time.DEFAULT_VALUE_FOR_WRONG_SET);
		}
	}
}
