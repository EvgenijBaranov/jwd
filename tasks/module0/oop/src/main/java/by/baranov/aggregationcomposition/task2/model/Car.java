package by.baranov.aggregationcomposition.task2.model;

import java.util.ArrayList;
import java.util.List;

public class Car {
	private static long counter = 0;
	private final long id = counter++;
	public static final int NUMBERS_WHEEL = 4;
	private BrandCar brand;
	private Engine engine;
	private List<Wheel> wheels = new ArrayList<>();
	private PetrolTank petrolTank;
	private Speedometer speedometer;

	public Car(BrandCar brand, Engine engine, PetrolTank petrolTank, Speedometer speedometer, List<Wheel> wheels) {
		this.brand = brand;
		this.engine = engine;
		this.petrolTank = petrolTank;
		this.speedometer = speedometer;
		this.wheels = wheels;
	}

	public BrandCar getBrand() {
		return brand;
	}

	public void setBrand(BrandCar brand) {
		this.brand = brand;
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public List<Wheel> getWheels() {
		return wheels;
	}

	public void setWheels(List<Wheel> wheels) {
		this.wheels = wheels;
	}

	public PetrolTank getPetrolTank() {
		return petrolTank;
	}

	public void setPetrolTank(PetrolTank petrolTank) {
		this.petrolTank = petrolTank;
	}

	public Speedometer getSpeedometer() {
		return speedometer;
	}

	public void setSpeedometer(Speedometer speedometer) {
		this.speedometer = speedometer;
	}

	public long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Car [\nid=" + id + ", " + brand + ", \n" + engine + ", \nwheels=" + wheels + ", \n" + petrolTank
				+ ", \n" + speedometer + "\n]";
	}

}
