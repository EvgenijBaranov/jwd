package by.baranov.simpleobjects.task9.service;

import by.baranov.simpleobjects.task9.model.Cover;

public interface BookCheckService {
	
	boolean checkInputDataBook(String title, String publisher, int publishingYear, int quantityPages2,
								String price, Cover typeCover, String[] namesAuthors);
	
	boolean checkPublishingYear(int publishingYear);
}
