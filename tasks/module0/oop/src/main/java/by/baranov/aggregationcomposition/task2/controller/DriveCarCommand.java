package by.baranov.aggregationcomposition.task2.controller;

import java.util.Scanner;

import by.baranov.aggregationcomposition.task2.model.Car;
import by.baranov.aggregationcomposition.task2.service.CarService;

public class DriveCarCommand implements AppCommand {
	private final CarService carService;

	public DriveCarCommand(CarService carService) {
		this.carService = carService;
	}

	@Override
	public void execute(Car car) {
		boolean isDistanceNotEntered = true;
		double distance = 0;
		while (isDistanceNotEntered) {
			try {
				System.out.println("You need to enter distance:");
				Scanner scan = new Scanner(System.in);
				distance = scan.nextDouble();
				if (distance > 0) {
					isDistanceNotEntered = false;
				} else {
					System.out.println("You entered wrong distance. Try again");
				}
			} catch (RuntimeException ex) {
				System.out.println("You entered wrong distance. Try again");
			}
		}
		carService.drive(car, distance);
	}
}
