package by.baranov.simpleobjects.task8.model;

public class Customer {
	private static long counter = 0;
	private final long id = counter++;
	private String surname;
	private String name;
	private String patronymic;
	private long creditCardNumber;
	private long bankAccountNumber;

	public Customer(String surname, String name, String patronymic, long creditCardNumber, long bankAccountNumber) {		
		this.surname = surname;
		this.name = name;
		this.patronymic = patronymic;
		this.creditCardNumber = creditCardNumber;
		this.bankAccountNumber = bankAccountNumber;
	}	

	public String getSurname() {
		return surname;
	}

	public String getName() {
		return name;
	}

	public String getPatronymic() {
		return patronymic;
	}

	public long getCreditCardNumber() {
		return creditCardNumber;
	}

	public long getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}

	public void setCreditCardNumber(long creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public void setBankAccountNumber(long bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	@Override
	public String toString() {
		return "\nCustomer [id=" + id + ", " + surname + " " + name + " " + patronymic + ", creditCardNumber="
				+ creditCardNumber + ", bankAccountNumber=" + bankAccountNumber + "]";
	}

}
