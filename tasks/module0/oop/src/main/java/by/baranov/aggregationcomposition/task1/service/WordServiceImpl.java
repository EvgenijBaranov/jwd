package by.baranov.aggregationcomposition.task1.service;

import by.baranov.aggregationcomposition.task1.model.Word;

public class WordServiceImpl implements WordService {

	@Override
	public Word createWord(String word) {
		return new Word(word);
	}

}
