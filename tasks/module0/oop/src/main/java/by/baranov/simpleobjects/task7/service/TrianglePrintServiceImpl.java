package by.baranov.simpleobjects.task7.service;

import by.baranov.simpleobjects.task7.model.Triangle;

public class TrianglePrintServiceImpl implements TrianglePrintService {

	@Override
	public void printTriangle(Triangle triangle) {
		System.out.println(triangle);
	}

}
