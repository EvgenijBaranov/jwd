package by.baranov.aggregationcomposition.task2.service;

import by.baranov.aggregationcomposition.task2.model.BrandCar;

public interface BrandCarService {
	
	BrandCar createBrandCar(String brandCar);
	
	boolean checkInputDataForBrandCar(BrandCar brandCar);
}
