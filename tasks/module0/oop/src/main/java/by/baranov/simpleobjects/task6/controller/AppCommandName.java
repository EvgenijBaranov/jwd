package by.baranov.simpleobjects.task6.controller;

public enum AppCommandName {
	PRINT_CURRENT_TIME("-p"),
	INCREASE_TIME_ON_HOURS("-inh"),
	INCREASE_TIME_ON_MINUTES("-inm"),
	INCREASE_TIME_ON_SECONDES("-ins"),
	SET_HOURS_TIME("-sh"),
	SET_MINUTES_TIME("-sm"),
	SET_SECONDS_TIME("-ss");	
	
	private String shortCommand;
	
	AppCommandName(String shortCommand){
		this.shortCommand = shortCommand;
	}
	
	 public static AppCommandName fromString(String name) {

	        final AppCommandName[] values = AppCommandName.values();
	        for (AppCommandName commandName : values) {
	            if (commandName.shortCommand.equals(name) || commandName.name().equals(name)) {
	            	System.out.println(commandName);
	                return commandName;
	            }
	        }
	        return null;
	 }

	public String getShortCommand() {
		return shortCommand;
	}
	 
}
