package by.baranov.simpleobjects.task8.controller;

import by.baranov.simpleobjects.task8.model.Customers;
import by.baranov.simpleobjects.task8.service.CustomersService;

public class PrintCustomersCommand implements AppCommand{
	private final CustomersService customersService;
	
	
	public PrintCustomersCommand(CustomersService customersService) {
		this.customersService = customersService;
	}

	@Override
	public void execute(Customers customers) {
		customersService.printCustomers(customers);		
	}
}
