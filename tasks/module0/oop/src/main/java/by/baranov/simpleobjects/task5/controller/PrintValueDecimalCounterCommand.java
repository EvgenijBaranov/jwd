package by.baranov.simpleobjects.task5.controller;

import by.baranov.simpleobjects.task5.model.DecimalCounter;
import by.baranov.simpleobjects.task5.service.PrintValueDecimalCounterService;

public class PrintValueDecimalCounterCommand implements AppCommand{
	private PrintValueDecimalCounterService printService;
	
	
	public PrintValueDecimalCounterCommand(PrintValueDecimalCounterService printService) {
		this.printService = printService;
	}


	@Override
	public void execute(DecimalCounter counter) {
		printService.printValueDecimalCounter(counter);
		
	}

}
