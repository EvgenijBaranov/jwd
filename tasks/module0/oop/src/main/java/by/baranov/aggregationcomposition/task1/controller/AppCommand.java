package by.baranov.aggregationcomposition.task1.controller;

import by.baranov.aggregationcomposition.task1.model.Text;

public interface AppCommand {
	void execute(Text text);
}
