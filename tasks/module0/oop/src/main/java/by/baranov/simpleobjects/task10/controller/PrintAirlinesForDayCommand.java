package by.baranov.simpleobjects.task10.controller;

import java.util.Scanner;

import by.baranov.simpleobjects.task10.model.Airlines;
import by.baranov.simpleobjects.task10.model.Day;
import by.baranov.simpleobjects.task10.service.AirlinesService;
import by.baranov.simpleobjects.task10.service.DayService;

public class PrintAirlinesForDayCommand implements AppCommand {
	private final AirlinesService airlinesService;
	private final DayService dayService;

	public PrintAirlinesForDayCommand(AirlinesService airlinesService, DayService dayService) {
		super();
		this.airlinesService = airlinesService;
		this.dayService = dayService;
	}

	@Override
	public void execute(Airlines airlines) {
		System.out.println("You need to enter day, for which you want to print airlines :");
		boolean isNotEnterDay = true;
		Day day = null;
		while (isNotEnterDay) {
			Scanner scan = new Scanner(System.in);
			String dayString = scan.nextLine();
			day = dayService.createDay(dayString);
			if (day != null) {
				isNotEnterDay = false;
			}else {
				System.out.println("You entered wrong day. Try again");
			}
		}
		airlinesService.printAirlinesForDay(airlines, day);
	}
}
