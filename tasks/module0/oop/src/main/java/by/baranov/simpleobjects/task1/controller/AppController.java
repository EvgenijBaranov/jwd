package by.baranov.simpleobjects.task1.controller;

import by.baranov.simpleobjects.task1.model.Test1;

public class AppController {
	private final AppCommandFactory commandFactory;

	public AppController(AppCommandFactory factory) {
		this.commandFactory = factory;
	}
	public void handleUserData(Test1 test, String commnadUser) {
        final AppCommand command = commandFactory.getCommand(commnadUser);
        command.execute(test);
    }	
}
