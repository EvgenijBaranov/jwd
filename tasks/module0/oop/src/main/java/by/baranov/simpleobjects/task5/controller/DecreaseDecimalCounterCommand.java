package by.baranov.simpleobjects.task5.controller;

import by.baranov.simpleobjects.task5.model.DecimalCounter;
import by.baranov.simpleobjects.task5.service.CheckDecimalCounterService;
import by.baranov.simpleobjects.task5.service.DecimalCounterService;

public class DecreaseDecimalCounterCommand implements AppCommand {
	private DecimalCounterService decimalCounterService;
	private CheckDecimalCounterService checkDecimalCounterService;

	public DecreaseDecimalCounterCommand(DecimalCounterService decimalCounterService,
			CheckDecimalCounterService checkDecimalCounterService) {
		this.decimalCounterService = decimalCounterService;
		this.checkDecimalCounterService = checkDecimalCounterService;
	}

	@Override
	public void execute(DecimalCounter counter) {
		if (checkDecimalCounterService.checkForDecrease(counter, DecimalCounter.DEFAULT_VALUE_FOR_CHANGE)) {
			decimalCounterService.decrease(counter);
		} else {
			System.out.println(
					"I can't decrease the counter value, because the decreasing value will be less than lower limit of the counter");
		}
	}
}
