package by.baranov.simpleobjects.task7.service;

import by.baranov.simpleobjects.task7.model.Point;

public class PointServiceImpl implements PointService {

	@Override
	public Point createPoint(double x, double y) {
		return new Point(x, y);
	}

}
