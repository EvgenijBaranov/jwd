package by.baranov.simpleobjects.task8.controller;

import java.util.Scanner;

import by.baranov.simpleobjects.task8.model.Customers;
import by.baranov.simpleobjects.task8.service.CustomersCheckService;
import by.baranov.simpleobjects.task8.service.CustomersService;

public class PrintCustomersWithBankAccountNumbersCommand implements AppCommand{
	private final CustomersService customersService;
	private final CustomersCheckService checkService;	

	public PrintCustomersWithBankAccountNumbersCommand(CustomersService customersService, CustomersCheckService checkService) {
		this.customersService = customersService;
		this.checkService = checkService;
	}

	@Override
	public void execute(Customers customers) {
		boolean isNotInputBankNumbers = true;
		long  bankAccountNumberFrom = 0;
		long bankAccountNumberTo = 0;
		while(isNotInputBankNumbers) {
			System.out.println("You need to enter bounds of bank account numbers :");			
			try {
				Scanner scan = new Scanner(System.in);
				System.out.println("number bank account from : ");
				bankAccountNumberFrom = scan.nextLong();
				System.out.println("number bank account to : ");
				bankAccountNumberTo = scan.nextLong();
				if(checkService.checkInputedBounds(bankAccountNumberFrom, bankAccountNumberTo)) {
					isNotInputBankNumbers = false;
				} else {
					System.out.println("First number bank account must be less than second number bank account. Try again");
				}
			} catch(RuntimeException e) {
				System.out.println("You need to enter long numbers. Try again");
			}			
		}		
		customersService.printCustomersWithBankAccountNumber(customers, bankAccountNumberFrom, bankAccountNumberTo);;		
	}
}
