package by.baranov.simpleobjects.task6;

import java.util.EnumMap;
import java.util.Map;
import java.util.Scanner;

import by.baranov.simpleobjects.task6.controller.AppCommand;
import by.baranov.simpleobjects.task6.controller.AppCommandFactory;
import by.baranov.simpleobjects.task6.controller.AppCommandFactoryImpl;
import by.baranov.simpleobjects.task6.controller.AppCommandName;
import by.baranov.simpleobjects.task6.controller.AppController;
import by.baranov.simpleobjects.task6.controller.IncreaseHoursCommand;
import by.baranov.simpleobjects.task6.controller.IncreaseMinutesCommand;
import by.baranov.simpleobjects.task6.controller.PrintTimeCommand;
import by.baranov.simpleobjects.task6.controller.SetHoursTimeCommand;
import by.baranov.simpleobjects.task6.controller.SetMinutesTimeCommand;
import by.baranov.simpleobjects.task6.controller.SetSecondsTimeCommand;
import by.baranov.simpleobjects.task6.model.Time;
import by.baranov.simpleobjects.task6.service.TimeAddService;
import by.baranov.simpleobjects.task6.service.TimeAddServiceImpl;
import by.baranov.simpleobjects.task6.service.TimeCheckService;
import by.baranov.simpleobjects.task6.service.TimeCheckServiceImpl;
import by.baranov.simpleobjects.task6.service.TimePrintService;
import by.baranov.simpleobjects.task6.service.TimePrintServiceImpl;
import by.baranov.simpleobjects.task6.service.TimeSetService;
import by.baranov.simpleobjects.task6.service.TimeSetServiceImpl;

public class App {

	public static void main(String[] args) {

		System.out.println("\n_TASK-6_\n");

		TimeAddService addHoursService = new TimeAddServiceImpl();
		TimeAddService addMinutesService = new TimeAddServiceImpl();
		TimeAddService addSecondsService = new TimeAddServiceImpl();		
		TimeCheckService checkHoursService = new TimeCheckServiceImpl();
		TimeCheckService checkMinutesService = new TimeCheckServiceImpl();
		TimeCheckService checkSecondsService = new TimeCheckServiceImpl();		
		TimePrintService printTimeService = new TimePrintServiceImpl();		
		TimeSetService setHoursService = new TimeSetServiceImpl();
		TimeSetService setMinutesService = new TimeSetServiceImpl();
		TimeSetService setSecondsService = new TimeSetServiceImpl();

		AppCommand addHourCommand = new IncreaseHoursCommand(addHoursService, checkHoursService);
		AppCommand addMinutesCommand = new IncreaseMinutesCommand(addMinutesService, checkMinutesService);
		AppCommand addSecondsCommand = new IncreaseMinutesCommand(addSecondsService, checkSecondsService);
		AppCommand printTimeCommand = new PrintTimeCommand(printTimeService);
		AppCommand setHoursCommand = new SetHoursTimeCommand(setHoursService, checkHoursService);
		AppCommand setMinutesCommand = new SetMinutesTimeCommand(setMinutesService, checkMinutesService);
		AppCommand setSecondsCommand = new SetSecondsTimeCommand(setSecondsService, checkSecondsService);
		
		Map<AppCommandName, AppCommand> commands = new EnumMap<>(AppCommandName.class);
		commands.put(AppCommandName.INCREASE_TIME_ON_HOURS, addHourCommand);
		commands.put(AppCommandName.INCREASE_TIME_ON_MINUTES, addMinutesCommand);
		commands.put(AppCommandName.INCREASE_TIME_ON_SECONDES, addSecondsCommand);
		commands.put(AppCommandName.PRINT_CURRENT_TIME, printTimeCommand);
		commands.put(AppCommandName.SET_HOURS_TIME, setHoursCommand);
		commands.put(AppCommandName.SET_MINUTES_TIME, setMinutesCommand);
		commands.put(AppCommandName.SET_SECONDS_TIME, setSecondsCommand);

		AppCommandFactory factory = new AppCommandFactoryImpl(commands);
		AppController controller = new AppController(factory);

		Time time = new Time();
		boolean isRunning = true;
		while (isRunning) {
			System.out.println();
			System.out.println(AppCommandName.INCREASE_TIME_ON_HOURS + " or " + AppCommandName.INCREASE_TIME_ON_HOURS.getShortCommand());
			System.out.println( AppCommandName.INCREASE_TIME_ON_MINUTES + " or " + AppCommandName.INCREASE_TIME_ON_MINUTES.getShortCommand());
			System.out.println( AppCommandName.INCREASE_TIME_ON_SECONDES + " or " + AppCommandName.INCREASE_TIME_ON_SECONDES.getShortCommand());			
			System.out.println( AppCommandName.PRINT_CURRENT_TIME + " or " + AppCommandName.PRINT_CURRENT_TIME.getShortCommand());
			System.out.println( AppCommandName.SET_HOURS_TIME + " or " + AppCommandName.SET_HOURS_TIME.getShortCommand());
			System.out.println( AppCommandName.SET_MINUTES_TIME + " or " + AppCommandName.SET_MINUTES_TIME.getShortCommand());
			System.out.println( AppCommandName.SET_SECONDS_TIME + " or " + AppCommandName.SET_SECONDS_TIME.getShortCommand());
			System.out.println("for quit -  " + "-q");
			System.out.println("Your command is :");
			Scanner scan = new Scanner(System.in);
			String command = scan.next();
			if (command.equalsIgnoreCase("-q")) {
				isRunning = false;
			} else {
				controller.handleUserData(time, command);
			}

		}

	}

}
