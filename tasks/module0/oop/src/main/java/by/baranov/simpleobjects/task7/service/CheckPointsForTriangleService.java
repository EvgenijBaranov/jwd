package by.baranov.simpleobjects.task7.service;

import by.baranov.simpleobjects.task7.model.Point;

public interface CheckPointsForTriangleService {
	boolean checkPointsTriangle(Point firstPoint, Point secondPoint, Point thirdPoint);
}
