package by.baranov.simpleobjects.task3.service;

import java.util.List;

import by.baranov.simpleobjects.task3.model.Student;

public interface StudentsPrintInfoService {
	void printStudents(List<Student> students);
}
