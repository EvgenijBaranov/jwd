package by.baranov.simpleobjects.task10.service;

import by.baranov.simpleobjects.task10.model.TypeAircraft;

public class TypeAircraftServiceImpl implements TypeAircraftService {

	@Override
	public TypeAircraft createTypeAircraft(String typeAircraftName) {
		TypeAircraft[] typesAircraft = TypeAircraft.values();
		TypeAircraft typeAircraftFromString = null;
		for (TypeAircraft typeAircraft : typesAircraft) {
			if (typeAircraft.name().compareToIgnoreCase(typeAircraftName) == 0) {
				typeAircraftFromString = typeAircraft;
			}
		}
		return typeAircraftFromString;
	}
}
