package by.baranov.simpleobjects.task10.service;

import by.baranov.simpleobjects.task10.model.Day;

public class DayServiceImpl implements DayService {

	@Override
	public Day createDay(String dayName) {
		Day[] daysOfWeek = Day.values();
		Day dayFromString = null;
		for (Day day : daysOfWeek) {
			if (day.name().compareToIgnoreCase(dayName) == 0) {
				dayFromString = day;
			}
		}
		return dayFromString;
	}

}
