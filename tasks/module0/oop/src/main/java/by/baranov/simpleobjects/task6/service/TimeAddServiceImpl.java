package by.baranov.simpleobjects.task6.service;

import by.baranov.simpleobjects.task6.model.Time;

public class TimeAddServiceImpl implements TimeAddService{

	@Override
	public void addHours(Time time, int quantityHours) {
		int currentHour = time.getCurrentHours();
		time.setCurrentHours(currentHour + quantityHours);		
	}

	@Override
	public void addMinutes(Time time, int quantityMinutes) {
		int currentMinute = time.getCurrentMinutes();
		time.setCurrentMinutes(currentMinute + quantityMinutes);		
	}

	@Override
	public void addSeconds(Time time, int quantitySeconds) {
		int currentSecond = time.getCurrentSeconds();
		time.setCurrentSeconds(currentSecond + quantitySeconds);		
	}
	
}
