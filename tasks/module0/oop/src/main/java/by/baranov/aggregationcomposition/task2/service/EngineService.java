package by.baranov.aggregationcomposition.task2.service;

import java.time.LocalDate;

import by.baranov.aggregationcomposition.task2.model.Engine;

public interface EngineService {

	Engine createEngine(double power, LocalDate dateOfManufacture, double consumptionPetrol);

	void turnOn(Engine engine);
	
	void turnOff (Engine engine);
	
	boolean isTurnedOff(Engine engine);
	
	boolean checkDataInput(Engine engine);

}
