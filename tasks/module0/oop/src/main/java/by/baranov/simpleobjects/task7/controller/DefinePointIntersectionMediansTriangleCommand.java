package by.baranov.simpleobjects.task7.controller;

import by.baranov.simpleobjects.task7.model.Point;
import by.baranov.simpleobjects.task7.model.Triangle;
import by.baranov.simpleobjects.task7.service.TriangleService;

public class DefinePointIntersectionMediansTriangleCommand implements AppCommand{
	private final TriangleService triangleService;
	
	public DefinePointIntersectionMediansTriangleCommand(TriangleService triangleService) {
		this.triangleService = triangleService;
	}

	@Override
	public void execute(Triangle triangle) {
		Point pointIntersectionMedians = triangleService.definePointIntersectionMedians(triangle);
		System.out.println("Point intersection medians of triangle : " + pointIntersectionMedians);		
	}
}
