package by.baranov.simpleobjects.task4.controller;

public interface AppCommandFactory {
	AppCommand getCommand(String commandName);
}
