package by.baranov.aggregationcomposition.task2.model;

public class Wheel {
	private static long counter = 0;
	private final long id = counter++;
	private final double radius;

	public Wheel(double radius) {
		this.radius = radius;
	}

	public double getRadius() {
		return radius;
	}

	public long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Wheel [id=" + id + ", radius=" + radius + "]";
	}
}
