package by.baranov.simpleobjects.task8.controller;

import by.baranov.simpleobjects.task8.model.Customers;
import by.baranov.simpleobjects.task8.service.CustomersService;

public class PrintCustomersAlphabeticallyCommand implements AppCommand{
	private final CustomersService customersService;
	
	
	public PrintCustomersAlphabeticallyCommand(CustomersService customersService) {
		this.customersService = customersService;
	}

	@Override
	public void execute(Customers customers) {
		customersService.printCustomersAlphabetically(customers);		
	}
}
