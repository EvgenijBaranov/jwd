package by.baranov.aggregationcomposition.task2.service;

import by.baranov.aggregationcomposition.task2.model.Wheel;

public class WheelServiceImpl implements WheelService{

	@Override
	public Wheel crateWheel(double radius) {
		return new Wheel(radius);
	}

	@Override
	public boolean checkInputDataForWheel(Wheel wheel) {
		return wheel.getRadius() >= 0;
	}

}
