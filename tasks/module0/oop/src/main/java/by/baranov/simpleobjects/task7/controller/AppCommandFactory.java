package by.baranov.simpleobjects.task7.controller;

public interface AppCommandFactory {
	AppCommand getCommand(String commandName);
}
