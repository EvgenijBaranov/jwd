package by.baranov.simpleobjects.task7.controller;

import by.baranov.simpleobjects.task7.model.Triangle;
import by.baranov.simpleobjects.task7.service.TriangleService;

public class CalculateSquareTriangleCommand implements AppCommand{
private final TriangleService triangleService;
	
	public CalculateSquareTriangleCommand(TriangleService triangleService) {
		this.triangleService = triangleService;
	}

	@Override
	public void execute(Triangle triangle) {
		double square = triangleService.calculateSquareTrinagle(triangle);
		System.out.println("Perimeter triangle : " + square);		
	}
	
}
