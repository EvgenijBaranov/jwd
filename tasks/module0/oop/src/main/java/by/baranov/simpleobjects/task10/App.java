package by.baranov.simpleobjects.task10;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import by.baranov.simpleobjects.task10.controller.AppCommand;
import by.baranov.simpleobjects.task10.controller.AppCommandFactory;
import by.baranov.simpleobjects.task10.controller.AppCommandFactoryImpl;
import by.baranov.simpleobjects.task10.controller.AppCommandName;
import by.baranov.simpleobjects.task10.controller.AppController;
import by.baranov.simpleobjects.task10.controller.PrintAirlinesForDayAfterTimeDepartureCommand;
import by.baranov.simpleobjects.task10.controller.PrintAirlinesForDayCommand;
import by.baranov.simpleobjects.task10.controller.PrintAirlinesOfPointDestinationCommand;
import by.baranov.simpleobjects.task10.controller.PrintAllAirlinesCommand;
import by.baranov.simpleobjects.task10.model.Airline;
import by.baranov.simpleobjects.task10.model.Airlines;
import by.baranov.simpleobjects.task10.model.Day;
import by.baranov.simpleobjects.task10.model.TypeAircraft;
import by.baranov.simpleobjects.task10.service.AirlineCheckService;
import by.baranov.simpleobjects.task10.service.AirlineCheckServiceImpl;
import by.baranov.simpleobjects.task10.service.AirlineService;
import by.baranov.simpleobjects.task10.service.AirlineServiceImpl;
import by.baranov.simpleobjects.task10.service.AirlinesService;
import by.baranov.simpleobjects.task10.service.AirlinesServiceImpl;
import by.baranov.simpleobjects.task10.service.DayService;
import by.baranov.simpleobjects.task10.service.DayServiceImpl;
import by.baranov.simpleobjects.task10.service.TypeAircraftService;
import by.baranov.simpleobjects.task10.service.TypeAircraftServiceImpl;

public class App {

	public static void main(String[] args) {

		System.out.println("\n_TASK-10_\n");

		AirlineService airlineCreateService = new AirlineServiceImpl();
		AirlineCheckService airlineCheckService = new AirlineCheckServiceImpl();
		DayService dayCreateService = new DayServiceImpl();
		TypeAircraftService typeAircraftCreateService = new TypeAircraftServiceImpl();

		AirlinesService airlinesCreateService = new AirlinesServiceImpl();
		AirlinesService airlinesPrintService = new AirlinesServiceImpl();
		AirlinesService airlinesPrintForDayService = new AirlinesServiceImpl();
		AirlinesService airlinesPrintForPointDestinationService = new AirlinesServiceImpl();
		AirlinesService airlinesPrintForDayAfterTimeDepartureService = new AirlinesServiceImpl();

		AppCommand printAirlinesCommand = new PrintAllAirlinesCommand(airlinesPrintService);
		AppCommand printAirlinesForDayCommand = new PrintAirlinesForDayCommand(airlinesPrintForDayService,
				dayCreateService);
		AppCommand printAirlinesForDayAfterTimeDepartureCommand = new PrintAirlinesForDayAfterTimeDepartureCommand(
				airlinesPrintForDayAfterTimeDepartureService, airlineCheckService, dayCreateService);
		AppCommand printAirlinesForPointDestinationCommand = new PrintAirlinesOfPointDestinationCommand(
				airlinesPrintForPointDestinationService, airlineCheckService);

		Map<AppCommandName, AppCommand> commands = new EnumMap<>(AppCommandName.class);
		commands.put(AppCommandName.PRINT_AIRLINES, printAirlinesCommand);
		commands.put(AppCommandName.PRINT_AIRLINES_OF_DAY, printAirlinesForDayCommand);
		commands.put(AppCommandName.PRINT_AIRLINES_OF_DAY_AFTER_TIME, printAirlinesForDayAfterTimeDepartureCommand);
		commands.put(AppCommandName.PRINT_AIRLINES_OF_POINT_DESTINATION, printAirlinesForPointDestinationCommand);

		AppCommandFactory factory = new AppCommandFactoryImpl(commands);
		AppController controller = new AppController(factory);

		boolean isNotAirlinesCreated = true;
		List<Airline> listAirlines = null;
		System.out.println("You need to create five airlines.");
		while (isNotAirlinesCreated) {
			listAirlines = new ArrayList<>();
			for (int i = 0; i < 5; i++) {
				System.out.println("Creation airline : " + (i + 1));
				System.out.println(
						"Enter the data in the folowing sequence : \n point destination(String), flight number(String), type aircraft(String), "
								+  "time departure(LocalTime), days departure(String... days) :");
				boolean isWrongEnterDataForAirline = true;
				while (isWrongEnterDataForAirline) {
					try {
						Scanner scan = new Scanner(System.in);
						System.out.println("Point destination (String):");
						String pointDestination = scan.nextLine();
						
						System.out.println("Flight number (String):");
						String flightNumber = scan.nextLine();
						
						System.out.println("Type aircraft (String):");
						String typeAircraftString = scan.nextLine();
						TypeAircraft typeAircraft = typeAircraftCreateService.createTypeAircraft(typeAircraftString);
						
						System.out.println("Time departure(LocalTime):");
						System.out.println("Hour of time departure(int):");
						int hourTimeDeparture = scan.nextInt();
						System.out.println("Minute of time departure(String):");
						int minuteTimeDeparture = scan.nextInt();
						LocalTime timeDeparture = LocalTime.of(hourTimeDeparture, minuteTimeDeparture);
						
						System.out.println("Days departure (String... days):");
						System.out.println("Enter quantity days departure for airline(int):");
						int quantityDays = scan.nextInt();
						Day[] daysDeparture = new Day[quantityDays];
						for(int j = 0; j < quantityDays; j++) {
							System.out.println("Enter day number "+(j + 1) + " in word(String):");
							Scanner scanDays = new Scanner(System.in);
							String dayName = scanDays.nextLine();
							daysDeparture[j] = dayCreateService.createDay(dayName);
						}
						if (airlineCheckService.chekInputDataForAirline(pointDestination, flightNumber, typeAircraft, timeDeparture, daysDeparture)) {
							isWrongEnterDataForAirline = false;
							Airline airline = airlineCreateService.createAirline(pointDestination, flightNumber, typeAircraft, timeDeparture, daysDeparture);
							listAirlines.add(airline);
						}
					} catch (RuntimeException e) {
						System.out.println("You entered wrong data for airline. Try again.");
					}
				}				
			}
			isNotAirlinesCreated = false;
		}

		Airlines airlines = airlinesCreateService.createAirlines(listAirlines.get(0), listAirlines.get(1), listAirlines.get(2),
				listAirlines.get(3), listAirlines.get(4));

		boolean isRunning = true;
		while (isRunning) {
			System.out.println();
			System.out.println(AppCommandName.PRINT_AIRLINES + " or " + AppCommandName.PRINT_AIRLINES.getShortCommand());
			System.out.println(AppCommandName.PRINT_AIRLINES_OF_DAY + " or "
					+ AppCommandName.PRINT_AIRLINES_OF_DAY.getShortCommand());
			System.out.println(AppCommandName.PRINT_AIRLINES_OF_DAY_AFTER_TIME + " or "
					+ AppCommandName.PRINT_AIRLINES_OF_DAY_AFTER_TIME.getShortCommand());
			System.out.println(AppCommandName.PRINT_AIRLINES_OF_POINT_DESTINATION + " or "
					+ AppCommandName.PRINT_AIRLINES_OF_POINT_DESTINATION.getShortCommand());

			System.out.println("for quit -  " + "-q");
			System.out.println("Your command is :");
			Scanner scan = new Scanner(System.in);
			String command = scan.next();
			if (command.equalsIgnoreCase("-q")) {
				isRunning = false;
			} else {
				controller.handleUserData(airlines, command);
			}
		}
	}

//	Airline airline1 = new Airline("Minsk", "AS-123", TypeAircraft.AIRBUS_A300, LocalTime.of(10, 10), Day.FRIDAY, Day.SUNDAY);
//	Airline airline2 = new Airline("Vitebsk", "BV-1234", TypeAircraft.BOEING_747, LocalTime.of(21, 40), Day.SUNDAY);
//	Airline airline3 = new Airline("Brest", "VD-2134", TypeAircraft.BOEING_747, LocalTime.of(13, 40), Day.MONDAY);
//	Airline airline4 = new Airline("Minsk", "SA-234", TypeAircraft.AIRBUS_A320, LocalTime.of(11, 20), Day.SUNDAY);
//	Airline airline5 = new Airline("Gomel", "SA-2344", TypeAircraft.BOEING_767, LocalTime.of(9, 20), Day.SUNDAY, Day.MONDAY, Day.FRIDAY);
//	airlines.printAirlinesIn("Minsk");
//	airlines.printAirlinesForDay(Day.FRIDAY);
//	airlines.printAirlinesForDayAfter(Day.FRIDAY, LocalTime.of(10, 00));

}
