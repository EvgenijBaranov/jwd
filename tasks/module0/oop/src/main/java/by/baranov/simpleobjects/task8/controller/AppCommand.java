package by.baranov.simpleobjects.task8.controller;

import by.baranov.simpleobjects.task8.model.Customers;

public interface AppCommand {
	void execute(Customers customers);
}
