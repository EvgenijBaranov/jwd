package by.baranov.simpleobjects.task5.controller;

import by.baranov.simpleobjects.task5.model.DecimalCounter;
import by.baranov.simpleobjects.task5.service.CheckDecimalCounterService;
import by.baranov.simpleobjects.task5.service.DecimalCounterService;

public class IncreaseDecimalCounterCommand implements AppCommand {
	private DecimalCounterService decimalCounterService;
	private CheckDecimalCounterService checkDecimalCounterService;

	public IncreaseDecimalCounterCommand(DecimalCounterService decimalCounterService,
			CheckDecimalCounterService checkDecimalCounterService) {
		this.decimalCounterService = decimalCounterService;
		this.checkDecimalCounterService = checkDecimalCounterService;
	}

	@Override
	public void execute(DecimalCounter counter) {
		if (checkDecimalCounterService.checkForIncrease(counter, DecimalCounter.DEFAULT_VALUE_FOR_CHANGE)) {
			decimalCounterService.increase(counter);
		} else {
			System.out.println(
					"I can't increase the counter value, because the increasing value will be greater than upper limit of the counter");
		}
	}

}
