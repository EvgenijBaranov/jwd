package by.baranov.simpleobjects.task5;

import java.util.EnumMap;
import java.util.Map;
import java.util.Scanner;

import by.baranov.simpleobjects.task5.controller.AppCommand;
import by.baranov.simpleobjects.task5.controller.AppCommandFactory;
import by.baranov.simpleobjects.task5.controller.AppCommandFactoryImpl;
import by.baranov.simpleobjects.task5.controller.AppCommandName;
import by.baranov.simpleobjects.task5.controller.AppController;
import by.baranov.simpleobjects.task5.controller.DecreaseDecimalCounterCommand;
import by.baranov.simpleobjects.task5.controller.IncreaseDecimalCounterCommand;
import by.baranov.simpleobjects.task5.controller.PrintValueDecimalCounterCommand;
import by.baranov.simpleobjects.task5.model.DecimalCounter;
import by.baranov.simpleobjects.task5.service.CheckDecimalCounterService;
import by.baranov.simpleobjects.task5.service.CheckDecimalCounterServiceImpl;
import by.baranov.simpleobjects.task5.service.DecimalCounterService;
import by.baranov.simpleobjects.task5.service.DecimalCounterServiceImpl;
import by.baranov.simpleobjects.task5.service.PrintValueDecimalCounterService;
import by.baranov.simpleobjects.task5.service.PrintValueDecimalCounterServiceImpl;

public class App {

	public static void main(String[] args) {

		System.out.println("\n_TASK-5_\n");

		PrintValueDecimalCounterService printCounterService = new PrintValueDecimalCounterServiceImpl();
		DecimalCounterService increaseCounterService = new DecimalCounterServiceImpl();
		DecimalCounterService decreaseCounterService = new DecimalCounterServiceImpl();
		CheckDecimalCounterService checkDecimalCounterService = new CheckDecimalCounterServiceImpl();

		AppCommand printCounterCommand = new PrintValueDecimalCounterCommand(printCounterService);
		AppCommand increaseCounterCommand = new IncreaseDecimalCounterCommand(increaseCounterService,
				checkDecimalCounterService);
		AppCommand decreaseCounterCommand = new DecreaseDecimalCounterCommand(decreaseCounterService,
				checkDecimalCounterService);

		Map<AppCommandName, AppCommand> commands = new EnumMap<>(AppCommandName.class);
		commands.put(AppCommandName.PRINT_COUNTER, printCounterCommand);
		commands.put(AppCommandName.INCREASE_COUNTER, increaseCounterCommand);
		commands.put(AppCommandName.DECREASE_COUNTER, decreaseCounterCommand);

		AppCommandFactory factory = new AppCommandFactoryImpl(commands);
		AppController controller = new AppController(factory);

		DecimalCounter defaultCounter = new DecimalCounter();
		System.out.println("Default value of the counter : " + defaultCounter.getCurrentValue());
		DecimalCounter counter = new DecimalCounter(99);

		boolean isRunning = true;
		while (isRunning) {
			System.out.println();
			System.out.println(AppCommandName.PRINT_COUNTER + " or " + AppCommandName.PRINT_COUNTER.getShortCommand());
			System.out.println( AppCommandName.INCREASE_COUNTER + " or " + AppCommandName.INCREASE_COUNTER.getShortCommand());
			System.out.println( AppCommandName.DECREASE_COUNTER + " or " + AppCommandName.DECREASE_COUNTER.getShortCommand());			
			System.out.println("for quit -  " + "-q");
			System.out.println("Your command is :");
			Scanner scan = new Scanner(System.in);
			String command = scan.next();
			if (command.equalsIgnoreCase("-q")) {
				isRunning = false;
			} else {
				controller.handleUserData(counter, command);
			}
		}
	}
}
