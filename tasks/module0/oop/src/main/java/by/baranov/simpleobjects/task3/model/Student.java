package by.baranov.simpleobjects.task3.model;

import java.util.Arrays;

public class Student {
	private String surname;
	private String group;
	private int[] marks;

	public Student(String surname, String group, int[] marks) {
		this.surname = surname;
		this.group = group;
		this.marks = marks;
	}	

	public String getSurname() {
		return surname;
	}

	public String getGroup() {
		return group;
	}

	public int[] getMarks() {
		return marks;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public void setMarks(int[] marks) {
		this.marks = marks;
	}

	@Override
	public String toString() {
		return "Student [name=" + surname + ", group=" + group + ", marks=" + Arrays.toString(marks) + "]";
	}

}
