package by.baranov.simpleobjects.task1.service;

import by.baranov.simpleobjects.task1.model.Test1;

public interface Test1Service {
	void changeValues(Test1 test, double firstNumber, double secondNumber);
	double sumValues(Test1 test);
	double maxValue(Test1 test);
}
