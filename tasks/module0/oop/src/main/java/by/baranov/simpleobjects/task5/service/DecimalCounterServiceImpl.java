package by.baranov.simpleobjects.task5.service;

import by.baranov.simpleobjects.task5.model.DecimalCounter;

public class DecimalCounterServiceImpl implements DecimalCounterService {
	
	@Override
	public void increase(DecimalCounter counter) {
		int currentValue = counter.getCurrentValue() + DecimalCounter.DEFAULT_VALUE_FOR_CHANGE;
		counter.setCurrentValue(currentValue);
	}

	@Override
	public void decrease(DecimalCounter counter) {
		int currentValue = counter.getCurrentValue() + DecimalCounter.DEFAULT_VALUE_FOR_CHANGE;
		counter.setCurrentValue(currentValue);
	}
}
