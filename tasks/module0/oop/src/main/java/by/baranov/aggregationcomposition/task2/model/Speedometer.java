package by.baranov.aggregationcomposition.task2.model;

public class Speedometer {
	private static long counter = 0;
	private final long id = counter++;
	private double distance = 0;

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	@Override
	public String toString() {
		return "Speedometer [id=" + id + "distance=" + distance + "]";
	}

}
