package by.baranov.simpleobjects.task10.model;

public enum TypeAircraft {
	AIRBUS_A300, AIRBUS_A310, AIRBUS_A320, BOEING_737, BOEING_747, BOEING_767
}
