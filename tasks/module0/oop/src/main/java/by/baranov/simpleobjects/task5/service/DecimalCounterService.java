package by.baranov.simpleobjects.task5.service;

import by.baranov.simpleobjects.task5.model.DecimalCounter;

public interface DecimalCounterService {
	void increase(DecimalCounter counter);
	void decrease(DecimalCounter counter);	
	
}
