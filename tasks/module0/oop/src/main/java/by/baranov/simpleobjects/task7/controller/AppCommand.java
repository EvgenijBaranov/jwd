package by.baranov.simpleobjects.task7.controller;

import by.baranov.simpleobjects.task7.model.Triangle;

public interface AppCommand {
	void execute(Triangle triangle);
}
