package by.baranov.aggregationcomposition.task1.controller;

import by.baranov.aggregationcomposition.task1.model.Text;
import by.baranov.aggregationcomposition.task1.service.TextService;

public class PrintTextCommand implements AppCommand {
	private final TextService textService;
	
	public PrintTextCommand(TextService textService) {
		this.textService = textService;		
	}

	@Override
	public void execute(Text text) {
		textService.printText(text);
	}
}
