package by.baranov.simpleobjects.task10.controller;

import java.util.Map;

public class AppCommandFactoryImpl implements AppCommandFactory {

	private final Map<AppCommandName, AppCommand> commandMap;

	public AppCommandFactoryImpl(Map<AppCommandName, AppCommand> commandMap) {
		this.commandMap = commandMap;
	}

	@Override
	public AppCommand getCommand(String commandName) {
		AppCommandName appCommandName = AppCommandName.fromString(commandName);
		return commandMap.getOrDefault(appCommandName, userData -> System.out.println("BAD COMMAND"));
	}
}
