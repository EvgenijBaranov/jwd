package by.baranov.simpleobjects.task7.service;

import by.baranov.simpleobjects.task7.model.Triangle;

public interface TriangleCheckService {
	boolean checkPointsTriangle(Triangle triangle);
}
