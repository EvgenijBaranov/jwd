package by.baranov.simpleobjects.task4.service;

import java.util.Collections;
import java.util.List;

import by.baranov.simpleobjects.task4.comparator.ComparatorTrainByPointDestinationByTimeDeparture;
import by.baranov.simpleobjects.task4.comparator.ComparatorTrainNumber;
import by.baranov.simpleobjects.task4.model.Train;

public class TrainsSortServiceImpl implements TrainsSortService{

	@Override
	public void sortTrainsByNumberTrain(List<Train> trains) {
		Collections.sort(trains, new ComparatorTrainNumber());		
	}

	@Override
	public void sortTrainsByPointDestinationAndTimeDeparture(List<Train> trains) {
		Collections.sort(trains, new ComparatorTrainByPointDestinationByTimeDeparture ());			
	}
}
