package by.baranov.aggregationcomposition.task2.service;

import by.baranov.aggregationcomposition.task2.model.Engine;
import by.baranov.aggregationcomposition.task2.model.PetrolTank;
import by.baranov.aggregationcomposition.task2.model.Speedometer;

public interface SpeedometerService {
	
	Speedometer createSpeedometer();
	
	void addDistance(Speedometer speedometer, double distance);
	
	double calculateMaxDistance(PetrolTank petrolTank, Engine engine);
}
