package by.baranov.simpleobjects.task7.controller;

public enum AppCommandName {
	PRINT_TRIANGLE("-p"),
	CALCULATE_PERIMETER_TRIANGLE("-cp"),
	CALCULATE_SQUARE_TRIANGLE("-cq"),
	DEFINE_POINT_INTERSECTION_MEDIANS_TRIANGLE("-dpim");
	
	private String shortCommand;
	
	AppCommandName(String shortCommand){
		this.shortCommand = shortCommand;
	}
	
	 public static AppCommandName fromString(String name) {

	        final AppCommandName[] values = AppCommandName.values();
	        for (AppCommandName commandName : values) {
	            if (commandName.shortCommand.equals(name) || commandName.name().equals(name)) {
	            	System.out.println(commandName);
	                return commandName;
	            }
	        }
	        return null;
	 }

	public String getShortCommand() {
		return shortCommand;
	}
	 
}
