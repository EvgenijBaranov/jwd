package by.baranov.simpleobjects.task8.service;

import by.baranov.simpleobjects.task8.model.Customer;
import by.baranov.simpleobjects.task8.model.Customers;

public interface CustomersService {

	Customers createCustomers(Customer... customers);
	
	void printCustomers(Customers customers);

	void printCustomersAlphabetically(Customers customers);

	void printCustomersWithBankAccountNumber(Customers customers, long bankAccountNumberFrom,
																	long bankAccountNumberTo);

}
