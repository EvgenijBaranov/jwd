package by.baranov.simpleobjects.task6.controller;

import java.util.Scanner;

import by.baranov.simpleobjects.task6.model.Time;
import by.baranov.simpleobjects.task6.service.TimeAddService;
import by.baranov.simpleobjects.task6.service.TimeCheckService;

public class IncreaseSecondsCommand implements AppCommand{
	private final TimeAddService timeAddService;
	private final TimeCheckService timeCheckService;

	public IncreaseSecondsCommand(TimeAddService timeAddService, TimeCheckService timeCheckService) {
		this.timeAddService = timeAddService;
		this.timeCheckService = timeCheckService;
	}

	@Override
	public void execute(Time time) {
		System.out.println("Enter quantity minutes, which you want to add: ");
		boolean isRunning = true;
		int quantitySeconds = 0;
		while(isRunning) {
			Scanner scan = new Scanner(System.in);
			try {
				quantitySeconds = scan.nextInt();
				isRunning = false;
			}catch(RuntimeException e) {
				System.out.println("You must input integer number, try again :");
			}			
		}
		if (timeCheckService.checkAddSeconds(time, quantitySeconds)) {
			timeAddService.addSeconds(time, quantitySeconds);;
		} else {
			System.out.println("Invalid seconds input, second must be in the range " + "from " + Time.MAX_BOUND_SECONDS
					+ " to " + Time.MAX_BOUND_SECONDS + " , seconds are set to " + Time.DEFAULT_VALUE_FOR_WRONG_SET);
			time.setCurrentSeconds(Time.DEFAULT_VALUE_FOR_WRONG_SET);
		}
	}
}
