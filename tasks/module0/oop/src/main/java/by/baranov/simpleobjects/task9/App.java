package by.baranov.simpleobjects.task9;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import by.baranov.simpleobjects.task9.controller.AppCommand;
import by.baranov.simpleobjects.task9.controller.AppCommandFactory;
import by.baranov.simpleobjects.task9.controller.AppCommandFactoryImpl;
import by.baranov.simpleobjects.task9.controller.AppCommandName;
import by.baranov.simpleobjects.task9.controller.AppController;
import by.baranov.simpleobjects.task9.controller.PrintAllBooksCommand;
import by.baranov.simpleobjects.task9.controller.PrintBooksAfterYearCommand;
import by.baranov.simpleobjects.task9.controller.PrintBooksOfAuthorCommand;
import by.baranov.simpleobjects.task9.controller.PrintBooksOfPublisherCommand;
import by.baranov.simpleobjects.task9.model.Book;
import by.baranov.simpleobjects.task9.model.Books;
import by.baranov.simpleobjects.task9.model.Cover;
import by.baranov.simpleobjects.task9.service.BookCheckService;
import by.baranov.simpleobjects.task9.service.BookCheckServiceImpl;
import by.baranov.simpleobjects.task9.service.BookService;
import by.baranov.simpleobjects.task9.service.BookServiceImpl;
import by.baranov.simpleobjects.task9.service.BooksService;
import by.baranov.simpleobjects.task9.service.BooksServiceImpl;
import by.baranov.simpleobjects.task9.service.CoverService;
import by.baranov.simpleobjects.task9.service.CoverServiceImpl;

public class App {

	public static void main(String[] args) {

		System.out.println("\n_TASK-9_\n");

		BookService bookCreateService = new BookServiceImpl();
		CoverService coverCreateService = new CoverServiceImpl();
		BookCheckService bookCheckService = new BookCheckServiceImpl();
		
		BooksService booksCreateService = new BooksServiceImpl();
		BooksService booksPrintService = new BooksServiceImpl();
		BooksService booksPrintByAuthorService = new BooksServiceImpl();
		BooksService booksPrintByPublisherService = new BooksServiceImpl();
		BooksService booksPrintAfterYearPublisherService = new BooksServiceImpl();
		
		AppCommand printBooksCommand = new PrintAllBooksCommand(booksPrintService);
		AppCommand printBooksOfAuthorCommand = new PrintBooksOfAuthorCommand(booksPrintByAuthorService);
		AppCommand printBooksOfPublisherCommand = new PrintBooksOfPublisherCommand(booksPrintByPublisherService);
		AppCommand printBooksAfterYearCommand = new PrintBooksAfterYearCommand(booksPrintAfterYearPublisherService, bookCheckService);
		
		Map<AppCommandName, AppCommand> commands = new EnumMap<>(AppCommandName.class);
		commands.put(AppCommandName.PRINT_BOOKS, printBooksCommand);
		commands.put(AppCommandName.PRINT_BOOKS_AFTER_YEAR, printBooksAfterYearCommand);
		commands.put(AppCommandName.PRINT_BOOKS_OF_AUTHOR, printBooksOfAuthorCommand);
		commands.put(AppCommandName.PRINT_BOOKS_OF_PUBLISHER, printBooksOfPublisherCommand);
		
		AppCommandFactory factory = new AppCommandFactoryImpl(commands);
		AppController controller = new AppController(factory);

		boolean isNotBooksCreated = true;
		List<Book> listBooks = null;		
		System.out.println("You need to create five books.");
		while (isNotBooksCreated) {
			listBooks = new ArrayList<>();
			for (int i = 0; i < 5; i++) {
				System.out.println("Creation book : " + (i + 1));
				System.out.println("Enter the data in the folowing sequence : title(String), publisher(String), year publiction(int), "
											+ "quantity pages(int), price(String), cover(String), authors(String...names) :");
				boolean isWrongEnterDataForBook = true;
				while(isWrongEnterDataForBook){
					try{
						Scanner scan = new Scanner(System.in);
						System.out.println("Title (String):");
						String title = scan.nextLine();
						System.out.println("Publisher (String):");
						String publisher = scan.nextLine();
						System.out.println("Year publiction (int):");
						int yearPubliction = scan.nextInt();
						System.out.println("Quantity pages (int):");
						int quantityPages = scan.nextInt();
						System.out.println("Price (String):");
						String price = scan.nextLine();
						System.out.println("Cover (String) (HARDCOVER or PAPERBACK):");
						String typeCover = scan.nextLine();
						Cover cover = coverCreateService.createCover(typeCover);
						System.out.println("Enter quantity authors (int) :");
						int quantityAuthors = scan.nextInt();
						String[] authors = new String[quantityAuthors];
						for(int j = 0; j < authors.length; j++) {
							System.out.println("author name number: " + (j + 1));
							authors[j] = scan.nextLine();
						}
						if(bookCheckService.checkInputDataBook(title, publisher, yearPubliction, quantityPages, price, cover, authors)) {
							isWrongEnterDataForBook = false;
							Book book = bookCreateService.createBook(title, publisher, yearPubliction, quantityPages, price, cover, authors);
							listBooks.add(book);
						}						
					}catch(RuntimeException e){
						System.out.println("You entered wrong data. Try again.");
					}
				}
			}
			isNotBooksCreated = false;			
		}
		
		Books books = booksCreateService.createBooks(listBooks.get(0), listBooks.get(1), listBooks.get(2),
														listBooks.get(3), listBooks.get(4));
		
		boolean isRunning = true;
		while (isRunning) {
			System.out.println();
			System.out.println(AppCommandName.PRINT_BOOKS + " or "
					+ AppCommandName.PRINT_BOOKS.getShortCommand());
			System.out.println(AppCommandName.PRINT_BOOKS_AFTER_YEAR + " or "
					+ AppCommandName.PRINT_BOOKS_AFTER_YEAR.getShortCommand());
			System.out.println(AppCommandName.PRINT_BOOKS_OF_AUTHOR + " or "
					+ AppCommandName.PRINT_BOOKS_OF_AUTHOR.getShortCommand());
			System.out.println(AppCommandName.PRINT_BOOKS_OF_PUBLISHER + " or "
					+ AppCommandName.PRINT_BOOKS_OF_PUBLISHER.getShortCommand());
			
			System.out.println("for quit -  " + "-q");
			System.out.println("Your command is :");
			Scanner scan = new Scanner(System.in);
			String command = scan.next();
			if (command.equalsIgnoreCase("-q")) {
				isRunning = false;
			} else {
				controller.handleUserData(books, command);
			}
		}
	}
	
//	Book book1 = new Book("Thinking in Java", "Prentice_Hall", 2006, 1150, "47", Cover.HARDCOVER,  "Bruce_Eckel");		
//	Book book2 = new Book("Effective_Java", "Addison_Wesley_Professional", 2018, 412, "36", HARDCOVER, "Joshua_Blosh");		
//	Book book3 = new Book("Modern_Java_in_Action", "Manning_Publications", 2018, 592, "35", HARDCOVER, 
//								"Raoul_Gabriel_Urma", " Mario_Fusco", " Alan_Mycroft");
//	Book book4 = new Book("Java_methods_programing", "Four_quarters", 2013, 869, "20", Cover.HARDCOVER, "Igor_Blinov");
//	Book book5 = new Book("Design_Patterns_Elements_of_Reusable_Object_Oriented_Software", "Addison_Wesley_Professional", 1994, 416, "68.9", Cover.PAPERBACK,
//								"Erich_Gamma", "Richard_Helm", "Ralph_Johnson", "John_Vlissides");
	
	
}
