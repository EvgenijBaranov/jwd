package by.baranov.simpleobjects.task8.service;

public interface CustomerCheckService {
	
	boolean checkEnterDataCustomer(String surname, String name, String patronymic, long creditCardNumber, long bankAccountNumber);
}
