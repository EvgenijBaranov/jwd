package by.baranov.simpleobjects.task8.service;

public class CustomerCheckServiceImpl implements CustomerCheckService {

	@Override
	public boolean checkEnterDataCustomer(String surname, String name, String patronymic, long creditCardNumber,
			long bankAccountNumber) {

		boolean isRightName = checkFullName(surname, name, patronymic);
		boolean isRightBankDetail = checkBankDetail(creditCardNumber, bankAccountNumber);
		return isRightName && isRightBankDetail;
	}

	private boolean checkFullName(String surname2, String name2, String patronymic2) {
		return isRightString(surname2) && isRightString(name2) && isRightString(patronymic2);
	}

	private boolean isRightString(String name) {
		return name.isEmpty() || name == null ? false : true;
	}

	private boolean checkBankDetail(long creditCardNumber2, long bankAccountNumber2) {
		return creditCardNumber2 < 1 || bankAccountNumber2 < 1 ? false : true;

	}
}
