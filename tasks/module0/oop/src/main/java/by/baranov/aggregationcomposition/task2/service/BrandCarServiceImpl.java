package by.baranov.aggregationcomposition.task2.service;

import by.baranov.aggregationcomposition.task2.model.BrandCar;

public class BrandCarServiceImpl implements BrandCarService {

	@Override
	public BrandCar createBrandCar(String brandCarString) {
		BrandCar[] brandCars = BrandCar.values();
		BrandCar brandCarsFromString = null;
		for (BrandCar brandCar : brandCars) {
			if (brandCar.name().compareToIgnoreCase(brandCarString) == 0) {
				brandCarsFromString = brandCar;
			}
		}
		return brandCarsFromString;
	}

	@Override
	public boolean checkInputDataForBrandCar(BrandCar brandCar) {
		BrandCar[] brandCars = BrandCar.values();
		for (BrandCar existedBrandCar : brandCars) {
			if (existedBrandCar.name().compareToIgnoreCase(brandCar.name()) == 0) {
				return true;
			}
		}
		return false;
	}
}
