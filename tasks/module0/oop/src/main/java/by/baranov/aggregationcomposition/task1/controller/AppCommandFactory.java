package by.baranov.aggregationcomposition.task1.controller;

public interface AppCommandFactory {
	AppCommand getCommand(String commandName);
}
