package by.baranov.aggregationcomposition.task1.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import by.baranov.aggregationcomposition.task1.model.Sentence;
import by.baranov.aggregationcomposition.task1.model.Text;
import by.baranov.aggregationcomposition.task1.model.Word;
import by.baranov.aggregationcomposition.task1.service.SentenceService;
import by.baranov.aggregationcomposition.task1.service.TextService;
import by.baranov.aggregationcomposition.task1.service.WordService;

public class AddTextCommand implements AppCommand {
	private final TextService textService;
	private final SentenceService sentenceService;
	private final WordService wordService;

	public AddTextCommand(TextService textService, SentenceService sentenceService, WordService wordService) {
		this.textService = textService;
		this.sentenceService = sentenceService;
		this.wordService = wordService;
	}

	@Override
	public void execute(Text text) {
		boolean isNotEnteredSentence = true;
		Sentence sentence = null;
		while (isNotEnteredSentence) {
			try {
				System.out.println("You need to enter sentence, which can conclude different quantity words. "
						+ "\nWhat quantity words your sentence will be cunclude?");
				Scanner scan = new Scanner(System.in);
				int quantityWords = scan.nextInt();
				List<Word> listWords = new ArrayList<>();
				for (int i = 0; i < quantityWords; i++) {
					System.out.println("Enter word number " + (i + 1) +":");
					Scanner scanWord = new Scanner(System.in);
					String wordString = scanWord.next();
					Word word = wordService.createWord(wordString);
					listWords.add(word);					
				}
				sentence = sentenceService.createSentence(listWords);
				isNotEnteredSentence = false;
			} catch (RuntimeException ex) {
				System.out.println("You entered wrong quantity words, quantity must be integer.");
			}
			textService.addSentence(text, sentence);
		}
	}
}
