package by.baranov.aggregationcomposition.task2.controller;

public enum AppCommandName {
	PRINT_CAR("-p"),
	PRINT_BRAND_CAR("-pb"),
	FILL_PETROLTANK_CAR("-fp"),
	DRIVE("-d"),
	CHANGE_WHEEL("-chw");
	
	private String shortCommand;
	
	AppCommandName(String shortCommand){
		this.shortCommand = shortCommand;
	}
	
	 public static AppCommandName fromString(String name) {

	        final AppCommandName[] values = AppCommandName.values();
	        for (AppCommandName commandName : values) {
	            if (commandName.shortCommand.equals(name) || commandName.name().equals(name)) {
	            	System.out.println(commandName);
	                return commandName;
	            }
	        }
	        return null;
	 }

	public String getShortCommand() {
		return shortCommand;
	}
	 
}
