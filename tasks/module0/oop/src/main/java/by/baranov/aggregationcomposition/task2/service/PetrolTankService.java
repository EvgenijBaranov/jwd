package by.baranov.aggregationcomposition.task2.service;

import by.baranov.aggregationcomposition.task2.model.PetrolTank;

public interface PetrolTankService {
	
	PetrolTank createPetrolTank(double capacity);
	
	boolean checkInputDataForPetrolTank(PetrolTank petrolTank);
	
	boolean isEmpty(PetrolTank petrolTank);
	
	void fillPetrolTank(PetrolTank petrolTank);
	
	void calculateCurrentCapacity(PetrolTank petrolTank, double distance, double consumptionPetrol);
	
}
