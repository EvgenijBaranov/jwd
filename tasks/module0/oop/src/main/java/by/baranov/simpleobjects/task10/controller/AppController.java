package by.baranov.simpleobjects.task10.controller;

import by.baranov.simpleobjects.task10.model.Airlines;

public class AppController {
	private final AppCommandFactory commandFactory;

	public AppController(AppCommandFactory factory) {
		this.commandFactory = factory;
	}
	public void handleUserData(Airlines airlines, String commnadUser) {
        final AppCommand command = commandFactory.getCommand(commnadUser);
        command.execute(airlines);
    }	
}
