package by.baranov.simpleobjects.task10.controller;

import java.time.LocalTime;
import java.util.Scanner;

import by.baranov.simpleobjects.task10.model.Airlines;
import by.baranov.simpleobjects.task10.model.Day;
import by.baranov.simpleobjects.task10.service.AirlineCheckService;
import by.baranov.simpleobjects.task10.service.AirlinesService;
import by.baranov.simpleobjects.task10.service.DayService;

public class PrintAirlinesForDayAfterTimeDepartureCommand implements AppCommand {
	private final AirlinesService airlinesService;
	private final AirlineCheckService airlineCheckService;
	private final DayService dayService; 

	public PrintAirlinesForDayAfterTimeDepartureCommand(AirlinesService airlinesService,
										AirlineCheckService airlineCheckService, DayService dayService) {
		this.airlinesService = airlinesService;
		this.airlineCheckService = airlineCheckService;
		this.dayService = dayService;		
	}

	@Override
	public void execute(Airlines airlines) {
		System.out.println("For printing airlines, taht execute flights during the day after certain time,"
									+ " you need to enter the day and time :");
		boolean isNotEnterDay = true;
		Day day = null;
		while (isNotEnterDay) {
			System.out.println("Enter the day:");
			Scanner scan = new Scanner(System.in);
			String dayString = scan.nextLine();
			day = dayService.createDay(dayString);
			if (day != null) {
				isNotEnterDay = false;
			}else {
				System.out.println("You entered wrong day. Try again");
			}
		}
		boolean isNotEnterTime = true;
		LocalTime timeDeparture = null;
		while (isNotEnterTime) {
			try {
				System.out.println("Enter hour:");
				Scanner scan = new Scanner(System.in);
				int hour = scan.nextInt();
				System.out.println("Enter minute:");
				int minute = scan.nextInt();
				timeDeparture = LocalTime.of(hour, minute);
				if (airlineCheckService.checkInputHourAndMinuteForTimeDeparture(timeDeparture)) {
					isNotEnterTime = false;
				}
			} catch(RuntimeException ex) {
				System.out.println("You entered wrong time. Try again");
			}			
		}
		airlinesService.printAirlinesForDayAfter(airlines, day, timeDeparture);
	}

}
