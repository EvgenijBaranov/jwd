package by.baranov.simpleobjects.task7.service;

import by.baranov.simpleobjects.task7.model.Point;

public class CheckPointsForTriangleServiceImpl implements CheckPointsForTriangleService{

	@Override
	public boolean checkPointsTriangle(Point firstPoint, Point secondPoint, Point thirdPoint) {
		if(firstPoint.equals(secondPoint) || firstPoint.equals(thirdPoint) || secondPoint.equals(thirdPoint)) {
			return false;
		}
		return true;
	}

}
