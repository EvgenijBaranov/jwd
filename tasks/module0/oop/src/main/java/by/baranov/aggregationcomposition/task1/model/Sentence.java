package by.baranov.aggregationcomposition.task1.model;

import java.util.ArrayList;
import java.util.List;

public class Sentence {
	private List<Word> words = new ArrayList<>();

	public Sentence(List<Word> words) {
		this.words = words;
	}

	public List<Word> getWords() {
		return words;
	}

	public void setWords(List<Word> words) {
		this.words = words;
	}

	@Override
	public String toString() {
		String sentence = "";
		for (Word word : words) {
			sentence += word.getWord() + " ";
		}
		return sentence;
	}

}
