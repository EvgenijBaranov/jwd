package by.baranov.simpleobjects.task10.service;

import java.time.LocalTime;

import by.baranov.simpleobjects.task10.model.Airline;
import by.baranov.simpleobjects.task10.model.Airlines;
import by.baranov.simpleobjects.task10.model.Day;

public interface AirlinesService {
	
	Airlines createAirlines(Airline... airlines);

	void printAirlinesIn(Airlines airlines, String pointDestination);

	void printAirlinesForDay(Airlines airlines, Day day);

	void printAirlinesForDayAfter(Airlines airlines, Day day, LocalTime timeDeparture);
	
	void printAllAirlines(Airlines airlines);

}
