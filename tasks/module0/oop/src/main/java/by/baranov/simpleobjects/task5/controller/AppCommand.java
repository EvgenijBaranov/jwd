package by.baranov.simpleobjects.task5.controller;

import by.baranov.simpleobjects.task5.model.DecimalCounter;

public interface AppCommand {
	void execute(DecimalCounter counter);
}
