package by.baranov.simpleobjects.task10.controller;

public enum AppCommandName {
	PRINT_AIRLINES("-p"),
	PRINT_AIRLINES_OF_POINT_DESTINATION("-ppd"),
	PRINT_AIRLINES_OF_DAY("-pd"),
	PRINT_AIRLINES_OF_DAY_AFTER_TIME("-pdt");
	
	private String shortCommand;
	
	AppCommandName(String shortCommand){
		this.shortCommand = shortCommand;
	}
	
	 public static AppCommandName fromString(String name) {

	        final AppCommandName[] values = AppCommandName.values();
	        for (AppCommandName commandName : values) {
	            if (commandName.shortCommand.equals(name) || commandName.name().equals(name)) {
	            	System.out.println(commandName);
	                return commandName;
	            }
	        }
	        return null;
	 }

	public String getShortCommand() {
		return shortCommand;
	}
	 
}
