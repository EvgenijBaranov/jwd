package by.baranov.aggregationcomposition.task2.controller;

public interface AppCommandFactory {
	AppCommand getCommand(String commandName);
}
