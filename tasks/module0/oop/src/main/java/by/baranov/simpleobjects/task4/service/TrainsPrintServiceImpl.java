package by.baranov.simpleobjects.task4.service;

import java.util.List;

import by.baranov.simpleobjects.task4.model.Train;

public class TrainsPrintServiceImpl implements TrainsPrintService{
	
	@Override
	public void printTrains(List<Train> trains) {
		for (Train train : trains) {
			System.out.println(train + ", ");
		}
		
	}

	@Override
	public void printTrainByNumber(List<Train> trains, String numberTrain) {
		for (Train train : trains) {
			if(train.getNumberTrain().equalsIgnoreCase(numberTrain)) {
				System.out.println(train + ", ");
			}
			
		}
		
	}

}
