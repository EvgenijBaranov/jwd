package by.baranov.simpleobjects.task9.service;

import by.baranov.simpleobjects.task9.model.Book;
import by.baranov.simpleobjects.task9.model.Cover;

public interface BookService {
	
	Book createBook(String title, String publisher, int publishingYear, int quantityPages,
						String price, Cover typeCover, String... namesAuthors);
}
