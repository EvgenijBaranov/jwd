package by.baranov.simpleobjects.task1.service;

import by.baranov.simpleobjects.task1.model.Test1;

public class Test1PrintInfoServiceImpl implements Test1PrintInfoService{

	@Override
	public void printValues(Test1 test) {
		System.out.println("First number : " + test.getFirstValue());
		System.out.println("Second number : " + test.getSecondValue());
	}

}
