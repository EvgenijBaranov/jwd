package by.baranov.simpleobjects.task1.model;

public class Test1 {	
	private double firstValue;
	private double secondValue;
	
	public double getFirstValue() {
		return firstValue;
	}

	public double getSecondValue() {
		return secondValue;
	}

	public void setFirstValue(double firstValue) {
		this.firstValue = firstValue;
	}

	public void setSecondValue(double secondValue) {
		this.secondValue = secondValue;
	}

	@Override
	public String toString() {
		return "Test1 [firstValue=" + firstValue + ", secondValue=" + secondValue + "]";
	}	

}
