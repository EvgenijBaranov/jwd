package by.baranov.simpleobjects.task2.model;

public class Test2 {	
	private static final double DEFAULT_FIRST_VALUE = 10;
	private static final double DEFAULT_SECOND_VALUE = 20;
	private double firstValue;
	private double secondValue;
	
	public Test2() {
		this.firstValue = DEFAULT_FIRST_VALUE;
		this.secondValue = DEFAULT_SECOND_VALUE;
	}
	
	public Test2(double firstValue, double secondValue) {
		this.firstValue = firstValue;
		this.secondValue = secondValue;
	}

	public double getFirstValue() {
		return firstValue;
	}

	public double getSecondValue() {
		return secondValue;
	}

	public void setFirstValue(double firstValue) {
		this.firstValue = firstValue;
	}

	public void setSecondValue(double secondValue) {
		this.secondValue = secondValue;
	}	

	@Override
	public String toString() {
		return "Test2 [firstValue=" + firstValue + ", secondValue=" + secondValue + "]";
	}	

}
