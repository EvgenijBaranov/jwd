package by.baranov.simpleobjects.task6.controller;

import java.util.Scanner;

import by.baranov.simpleobjects.task6.model.Time;
import by.baranov.simpleobjects.task6.service.TimeAddService;
import by.baranov.simpleobjects.task6.service.TimeCheckService;

public class IncreaseMinutesCommand implements AppCommand {
	private final TimeAddService timeAddService;
	private final TimeCheckService timeCheckService;

	public IncreaseMinutesCommand(TimeAddService timeAddService, TimeCheckService timeCheckService) {
		this.timeAddService = timeAddService;
		this.timeCheckService = timeCheckService;
	}

	@Override
	public void execute(Time time) {
		System.out.println("Enter quantity minutes, which you want to add: ");
		boolean isRunning = true;
		int quantityMinutes = 0;
		while(isRunning) {
			Scanner scan = new Scanner(System.in);
			try {
				quantityMinutes = scan.nextInt();
				isRunning = false;
			}catch(RuntimeException e) {
				System.out.println("You must input integer number, try again :");
			}			
		}
		if (timeCheckService.checkAddMinutes(time, quantityMinutes)) {
			timeAddService.addMinutes(time, quantityMinutes);
		} else {
			System.out.println("Invalid minutes input, minutes must be in the range " + "from " + Time.MIN_BOUND_MINUTES
					+ " to " + Time.MAX_BOUND_MINUTES + " , minutes are set to " + Time.DEFAULT_VALUE_FOR_WRONG_SET);
			time.setCurrentMinutes(Time.DEFAULT_VALUE_FOR_WRONG_SET);
		}
	}
}
