package by.baranov.aggregationcomposition.task1.model;

import java.util.ArrayList;
import java.util.List;

public class Text {
	private Sentence title;
	private List<Sentence> content = new ArrayList<>();

	public Text(Sentence title, List<Sentence> sentences) {
		this.title = title;
		this.content = sentences;
	}

	public Sentence getTitle() {
		return title;
	}

	public List<Sentence> getContent() {
		return content;
	}

	public void setTitle(Sentence title) {
		this.title = title;
	}

	public void setContent(List<Sentence> content) {
		this.content = content;
	}

	@Override
	public String toString() {
		String text = "";
		for (Sentence sentence : content) {
			text += sentence.toString() + ". ";
		}
		return "Text [\ntitle = \"" + title + "\"\ncontent = \"" + text + "\"]";
	}

}
