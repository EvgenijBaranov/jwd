package by.baranov.simpleobjects.task3.service;

import java.util.List;

import by.baranov.simpleobjects.task3.model.Student;

public class StudentsPrintInfoServiceImpl implements StudentsPrintInfoService {

	@Override
	public void printStudents(List<Student> students) {
		for (Student student : students) {
			System.out.println(student + ", ");
		}
	}
}
