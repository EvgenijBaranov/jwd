package by.baranov.simpleobjects.task6.controller;

import java.util.Scanner;

import by.baranov.simpleobjects.task6.model.Time;
import by.baranov.simpleobjects.task6.service.TimeCheckService;
import by.baranov.simpleobjects.task6.service.TimeSetService;

public class SetMinutesTimeCommand implements AppCommand {
	private final  TimeSetService timeSetService;
	private final  TimeCheckService timeCheckService;	
	
	public SetMinutesTimeCommand(TimeSetService timeSetService, TimeCheckService timeCheckService) {
		this.timeSetService = timeSetService;
		this.timeCheckService = timeCheckService;
	}

	@Override
	public void execute(Time time) {
		System.out.println("Enter minutes : ");
		boolean isRunning = true;
		int inputedMinutes = 0;
		while(isRunning) {
			Scanner scan = new Scanner(System.in);
			try {
				inputedMinutes = scan.nextInt();
				isRunning = false;
			}catch(RuntimeException e) {
				System.out.println("You must input integer number, try again :");
			}			
		}
		if(timeCheckService.checkMinutes(inputedMinutes)) {
			timeSetService.setMinutes(time, inputedMinutes);
		} else {
			System.out.println("Invalid minutes input, minutes must be in the range from " + Time.MIN_BOUND_MINUTES
					+ " to " + Time.MAX_BOUND_MINUTES + " , minutes are set to " + Time.DEFAULT_VALUE_FOR_WRONG_SET);
			timeSetService.setMinutes(time, Time.DEFAULT_VALUE_FOR_WRONG_SET);
		}
	}

}
