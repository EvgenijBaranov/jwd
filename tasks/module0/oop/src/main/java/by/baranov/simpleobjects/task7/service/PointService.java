package by.baranov.simpleobjects.task7.service;

import by.baranov.simpleobjects.task7.model.Point;

public interface PointService {
	Point createPoint(double x, double y);
}
