package by.baranov.simpleobjects.task10.service;

import by.baranov.simpleobjects.task10.model.TypeAircraft;

public interface TypeAircraftService {
	TypeAircraft createTypeAircraft(String typeAircraftName);
}
