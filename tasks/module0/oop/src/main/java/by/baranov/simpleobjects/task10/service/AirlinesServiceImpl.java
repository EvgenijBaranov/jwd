package by.baranov.simpleobjects.task10.service;

import java.time.LocalTime;

import by.baranov.simpleobjects.task10.model.Airline;
import by.baranov.simpleobjects.task10.model.Airlines;
import by.baranov.simpleobjects.task10.model.Day;

public class AirlinesServiceImpl implements AirlinesService {

	@Override
	public Airlines createAirlines(Airline... airlines) {
		return new Airlines(airlines);
	}

	@Override
	public void printAirlinesIn(Airlines airlines, String pointDestination) {
		for (Airline airline : airlines.getAirlines()) {
			if (airline.getPointDestination().compareToIgnoreCase(pointDestination) == 0) {
				System.out.println(airline);
			}
		}
	}

	@Override
	public void printAirlinesForDay(Airlines airlines, Day day) {
		for (Airline airline : airlines.getAirlines()) {
			for (Day dayDeparture : airline.getDaysDeparture()) {
				if (dayDeparture.equals(day)) {
					System.out.println(airline);
				}
			}
		}
	}

	@Override
	public void printAirlinesForDayAfter(Airlines airlines, Day day, LocalTime timeDeparture) {
		for (Airline airline : airlines.getAirlines()) {
			for (Day dayDeparture : airline.getDaysDeparture()) {
				if (dayDeparture.equals(day) && airline.getTimeDeparture().isAfter(timeDeparture)) {
					System.out.println(airline);
				}
			}
		}
	}

	@Override
	public void printAllAirlines(Airlines airlines) {
		for(Airline airline : airlines.getAirlines()) {
			System.out.println(airline);
		}
		
	}
}
