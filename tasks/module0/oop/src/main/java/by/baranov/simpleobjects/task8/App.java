package by.baranov.simpleobjects.task8;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import by.baranov.simpleobjects.task8.controller.AppCommand;
import by.baranov.simpleobjects.task8.controller.AppCommandFactory;
import by.baranov.simpleobjects.task8.controller.AppCommandFactoryImpl;
import by.baranov.simpleobjects.task8.controller.AppCommandName;
import by.baranov.simpleobjects.task8.controller.AppController;
import by.baranov.simpleobjects.task8.controller.PrintCustomersAlphabeticallyCommand;
import by.baranov.simpleobjects.task8.controller.PrintCustomersCommand;
import by.baranov.simpleobjects.task8.controller.PrintCustomersWithBankAccountNumbersCommand;
import by.baranov.simpleobjects.task8.model.Customer;
import by.baranov.simpleobjects.task8.model.Customers;
import by.baranov.simpleobjects.task8.service.CustomerCheckService;
import by.baranov.simpleobjects.task8.service.CustomerCheckServiceImpl;
import by.baranov.simpleobjects.task8.service.CustomerService;
import by.baranov.simpleobjects.task8.service.CustomerServiceImpl;
import by.baranov.simpleobjects.task8.service.CustomersCheckService;
import by.baranov.simpleobjects.task8.service.CustomersCheckServiceImpl;
import by.baranov.simpleobjects.task8.service.CustomersService;
import by.baranov.simpleobjects.task8.service.CustomersServiceImpl;

public class App {

	public static void main(String[] args) {

		System.out.println("\n_TASK-8_\n");

		CustomerService customerCreateService = new CustomerServiceImpl();
		CustomerCheckService customerCreateCheckService = new CustomerCheckServiceImpl();
		
		CustomersService customersCreateService = new CustomersServiceImpl();
		CustomersService customersPrintAlphabeticallyService = new CustomersServiceImpl();
		CustomersService customersAllPrintService = new CustomersServiceImpl();
		CustomersService customersPrintWithNumberBankAccountService = new CustomersServiceImpl();
		CustomersCheckService customersCreateCheckService = new CustomersCheckServiceImpl();		
		CustomersCheckService customersNumbersBankAccountCheckService = new CustomersCheckServiceImpl();
		
		AppCommand printCustomersCommand = new PrintCustomersCommand(customersAllPrintService);
		AppCommand printCustomersAlphabeticallyCommand = new PrintCustomersAlphabeticallyCommand(customersPrintAlphabeticallyService);
		AppCommand customersPrintWithNumberBankAccountCommand = new PrintCustomersWithBankAccountNumbersCommand(customersPrintWithNumberBankAccountService, customersNumbersBankAccountCheckService);
		

		Map<AppCommandName, AppCommand> commands = new EnumMap<>(AppCommandName.class);
		commands.put(AppCommandName.PRINT_CUSTOMERS, printCustomersCommand);
		commands.put(AppCommandName.PRINT_CUSTOMERS_ALPHABETICALLY, printCustomersAlphabeticallyCommand);
		commands.put(AppCommandName.PRINT_CUSTOMERS_WITH_NUMBER_BANK_CARD, customersPrintWithNumberBankAccountCommand);
		
		AppCommandFactory factory = new AppCommandFactoryImpl(commands);
		AppController controller = new AppController(factory);

		boolean isNotCustomersCreated = true;
		List<Customer> listCustomers = null;		
		System.out.println("You need to create five customers.");
		while (isNotCustomersCreated) {
			listCustomers = new ArrayList<>();
			for (int i = 0; i < 5; i++) {
				System.out.println("Creation customer : " + (i + 1));
				System.out.println("Enter the data in the folowing sequence : surname(String), name(String), patronymic(String), "
											+ "credit card number(long), bank account number(long) :");
				boolean isWrongEnterDataForCustomer = true;
				while(isWrongEnterDataForCustomer){
					try{
						Scanner scan = new Scanner(System.in);
						System.out.println("Surname (String):");
						String surname = scan.next();
						System.out.println("Name (String):");
						String name = scan.next();
						System.out.println("Patronymic (String):");
						String patronymic = scan.next();
						System.out.println("Credit card number(long):");
						long creditCardNumber = scan.nextLong();
						System.out.println("Bank account number(long):");
						long bankAccountNumber = scan.nextLong();						
						isWrongEnterDataForCustomer = false;
						Customer tempCustomer = customerCreateService.createCustomer(surname, name, patronymic, creditCardNumber, bankAccountNumber);
						listCustomers.add(tempCustomer);
					}catch(RuntimeException e){
						System.out.println("You entered wrong data. Try again.");
					}
				}
			}
			if (customersCreateCheckService.checkCustomersInput(listCustomers.get(0), listCustomers.get(1), listCustomers.get(2),
																listCustomers.get(3), listCustomers.get(4))) {
				isNotCustomersCreated = false;
			} else {
				System.out.println("Customers can't have the same credit card number or bank account number. Try again");
			}
		}
		
		Customers customers = customersCreateService.createCustomers(listCustomers.get(0), listCustomers.get(1), listCustomers.get(2),
																	 listCustomers.get(3), listCustomers.get(4));
		
		boolean isRunning = true;
		while (isRunning) {
			System.out.println();
			System.out.println(AppCommandName.PRINT_CUSTOMERS + " or "
					+ AppCommandName.PRINT_CUSTOMERS.getShortCommand());
			System.out.println(AppCommandName.PRINT_CUSTOMERS_ALPHABETICALLY + " or "
					+ AppCommandName.PRINT_CUSTOMERS_ALPHABETICALLY.getShortCommand());
			System.out.println(AppCommandName.PRINT_CUSTOMERS_WITH_NUMBER_BANK_CARD + " or "
					+ AppCommandName.PRINT_CUSTOMERS_WITH_NUMBER_BANK_CARD.getShortCommand());
			
			System.out.println("for quit -  " + "-q");
			System.out.println("Your command is :");
			Scanner scan = new Scanner(System.in);
			String command = scan.next();
			if (command.equalsIgnoreCase("-q")) {
				isRunning = false;
			} else {
				controller.handleUserData(customers, command);
			}
		}
	}
	
//	Customer customer1 = new Customer("Ivanov","Ivan","Ivanovich", 11111, 9999);
//	Customer customer2 = new Customer("Petrov","Petr","Petrovich", 22222, 3333);
//	Customer customer3 = new Customer("Sidorov","Ivan","Ivanovich", 44444, 4444);
//	Customer customer4 = new Customer("Ivanov","Ivan","Petrovich", 11112, 9994);
//	Customer customer5 = new Customer("Ivanov","Petr","Petrovich", 11113, 9993);
	
	
}
