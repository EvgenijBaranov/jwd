package by.baranov.simpleobjects.task1;

import java.util.EnumMap;
import java.util.Map;
import java.util.Scanner;

import by.baranov.simpleobjects.task1.controller.AppCommand;
import by.baranov.simpleobjects.task1.controller.AppCommandFactory;
import by.baranov.simpleobjects.task1.controller.AppCommandFactoryImpl;
import by.baranov.simpleobjects.task1.controller.AppCommandName;
import by.baranov.simpleobjects.task1.controller.AppController;
import by.baranov.simpleobjects.task1.controller.MaxValueCommand;
import by.baranov.simpleobjects.task1.controller.NumbersChangerCommand;
import by.baranov.simpleobjects.task1.controller.PrintInfoCommand;
import by.baranov.simpleobjects.task1.controller.SumValuesCommand;
import by.baranov.simpleobjects.task1.model.Test1;
import by.baranov.simpleobjects.task1.service.Test1PrintInfoService;
import by.baranov.simpleobjects.task1.service.Test1PrintInfoServiceImpl;
import by.baranov.simpleobjects.task1.service.Test1Service;
import by.baranov.simpleobjects.task1.service.Test1ServiceImpl;

public class App {

	public static void main(String[] args) {
		System.out.println("\n_____OOP_SIMPLE_OBJECTS____\n");

		System.out.println("\n_TASK-1_\n");
		Test1Service testServiceMaxValue = new Test1ServiceImpl();
		Test1Service testServiceSumValues = new Test1ServiceImpl();
		Test1Service testServiceChangeValues = new Test1ServiceImpl();
		Test1PrintInfoService testServicePrintValues = new Test1PrintInfoServiceImpl();

		AppCommand maxValueCommand = new MaxValueCommand(testServiceMaxValue);
		AppCommand sumValuesCommand = new SumValuesCommand(testServiceSumValues);
		AppCommand changeNumbersCommand = new NumbersChangerCommand(testServiceChangeValues);
		AppCommand printInfoCommand = new PrintInfoCommand(testServicePrintValues);

		Map<AppCommandName, AppCommand> commands = new EnumMap<>(AppCommandName.class);
		commands.put(AppCommandName.FIND_MAX_VALUE, maxValueCommand);
		commands.put(AppCommandName.FIND_SUM_VALUES, sumValuesCommand);
		commands.put(AppCommandName.CHANGE_DATA, changeNumbersCommand);
		commands.put(AppCommandName.PRINT_VALUE, printInfoCommand);

		AppCommandFactory factory = new AppCommandFactoryImpl(commands);
		AppController controller = new AppController(factory);

		Test1 test1 = new Test1();
		boolean isRunning = true;
		while (isRunning) {
			System.out.println();
			System.out.println(AppCommandName.CHANGE_DATA + 
								" or " + AppCommandName.CHANGE_DATA.getShortCommand());
			System.out.println(AppCommandName.FIND_MAX_VALUE + 
								" or " + AppCommandName.FIND_MAX_VALUE.getShortCommand());
			System.out.println(AppCommandName.FIND_SUM_VALUES + 
								" or " + AppCommandName.FIND_SUM_VALUES.getShortCommand());
			System.out.println(AppCommandName.PRINT_VALUE + 
								" or " + AppCommandName.PRINT_VALUE.getShortCommand());
			System.out.println("for quit -  "+ "-q");
			System.out.println("Your command is :");
			
			Scanner scan = new Scanner(System.in);
			String command = scan.next();			
						
			if (command.equalsIgnoreCase("-q")) {
				isRunning = false;
			} else {
				controller.handleUserData(test1, command);
			}

		}

	}
}
