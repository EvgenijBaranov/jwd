package by.baranov.simpleobjects.task3.controller;

import java.util.List;

import by.baranov.simpleobjects.task3.model.Student;
import by.baranov.simpleobjects.task3.service.StudentsPrintInfoService;

public class PrintInfoCommand implements AppCommand{
	private final StudentsPrintInfoService printService;

	public PrintInfoCommand(StudentsPrintInfoService printService) {
		super();
		this.printService = printService;
	}

	@Override
	public void execute(List<Student> students) {
		printService.printStudents(students);
	}
	
}
