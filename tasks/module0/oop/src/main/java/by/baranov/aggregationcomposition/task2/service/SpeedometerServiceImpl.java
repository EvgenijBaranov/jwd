package by.baranov.aggregationcomposition.task2.service;

import by.baranov.aggregationcomposition.task2.model.Engine;
import by.baranov.aggregationcomposition.task2.model.PetrolTank;
import by.baranov.aggregationcomposition.task2.model.Speedometer;

public class SpeedometerServiceImpl implements SpeedometerService{
	
	@Override
	public Speedometer createSpeedometer() {
		return new Speedometer();
	}

	@Override
	public void addDistance(Speedometer speedometer, double distance) {
		double currentDistance = speedometer.getDistance() + distance;
		speedometer.setDistance(currentDistance);		
	}
	
	@Override
	public double calculateMaxDistance(PetrolTank petrolTank, Engine engine) {
		double currentCapacityPetrolTank = petrolTank.getCurrentCapacity();		
		double maxDistance = currentCapacityPetrolTank / engine.getConsumptionPetrol();
		return maxDistance;		
	}
}
