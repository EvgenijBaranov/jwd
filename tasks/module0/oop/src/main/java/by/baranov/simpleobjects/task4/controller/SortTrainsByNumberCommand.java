package by.baranov.simpleobjects.task4.controller;

import java.util.List;

import by.baranov.simpleobjects.task4.model.Train;
import by.baranov.simpleobjects.task4.service.TrainsSortService;

public class SortTrainsByNumberCommand implements AppCommand{
	private TrainsSortService trainsSortService;	
	
	public SortTrainsByNumberCommand(TrainsSortService trainsSortService) {
		this.trainsSortService = trainsSortService;
	}

	@Override
	public void execute(List<Train> trains) {
		trainsSortService.sortTrainsByNumberTrain(trains);	
		
	}

}
