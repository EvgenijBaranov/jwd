package by.baranov.simpleobjects.task3;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import by.baranov.simpleobjects.task3.controller.AppCommand;
import by.baranov.simpleobjects.task3.controller.AppCommandFactory;
import by.baranov.simpleobjects.task3.controller.AppCommandFactoryImpl;
import by.baranov.simpleobjects.task3.controller.AppCommandName;
import by.baranov.simpleobjects.task3.controller.AppController;
import by.baranov.simpleobjects.task3.controller.FindExcelentStudentsCommand;
import by.baranov.simpleobjects.task3.controller.PrintInfoCommand;
import by.baranov.simpleobjects.task3.model.Student;
import by.baranov.simpleobjects.task3.service.StudentService;
import by.baranov.simpleobjects.task3.service.StudentServiceImpl;
import by.baranov.simpleobjects.task3.service.StudentsPrintInfoService;
import by.baranov.simpleobjects.task3.service.StudentsPrintInfoServiceImpl;

public class App {
	public static void main(String[] args) {
		
		System.out.println("\n_TASK-3_\n");
		
		StudentService excelentStudentService = new StudentServiceImpl();
		StudentsPrintInfoService studentsPrintService = new StudentsPrintInfoServiceImpl();

		AppCommand excelentStudentCommand = new FindExcelentStudentsCommand(excelentStudentService, studentsPrintService);		
		AppCommand printInfoCommand = new PrintInfoCommand(studentsPrintService);

		Map<AppCommandName, AppCommand> commands = new EnumMap<>(AppCommandName.class);
		commands.put(AppCommandName.FIND_EXCELENT_STUDENTS, excelentStudentCommand);
		commands.put(AppCommandName.PRINT_STUDENTS, printInfoCommand);

		AppCommandFactory factory = new AppCommandFactoryImpl(commands);
		AppController controller = new AppController(factory);

		Student student1 = new Student("Ivanov", "123", new int[] { 6, 8, 4, 9, 10 });
		Student student2 = new Student("Petrov", "1245rt", new int[] { 9, 9, 10, 9, 10 });
		Student student3 = new Student("Sidorov", "123", new int[] { 9, 9, 10, 9, 10 });
		Student student4 = new Student("Nikitin", "4123", new int[] { 8, 9, 10, 9, 10 });
		Student student5 = new Student("Bykov", "123sd", new int[] { 8, 9, 10, 9, 10 });
		Student student6 = new Student("Alferov", "1245rt", new int[] { 9, 9, 10, 9, 10 });
		Student student7 = new Student("Smirnov", "213", new int[] { 8, 9, 10, 9, 10 });
		Student student8 = new Student("Vinogradov", "1245rt", new int[] { 10, 9, 10, 9, 10 });
		Student student9 = new Student("Tsyalkovski", "456", new int[] { 8, 9, 10, 9, 10 });
		Student student10 = new Student("Skanavi", "123", new int[] { 8, 9, 7, 9, 10 });
		List<Student> students = Arrays.asList(new Student[] {student1, student2, student3, student4, student5, 
																student6, student7, student8, student9, student10});
		boolean isRunning = true;
		while (isRunning) {
			System.out.println();
			System.out.println(AppCommandName.FIND_EXCELENT_STUDENTS + 
								" or " + AppCommandName.FIND_EXCELENT_STUDENTS.getShortCommand());
			System.out.println(AppCommandName.PRINT_STUDENTS + 
								" or " + AppCommandName.PRINT_STUDENTS.getShortCommand());			
			System.out.println("for quit -  "+ "-q");
			System.out.println("Your command is :");
			Scanner scan = new Scanner(System.in);
			String command = scan.next();
			if (command.equalsIgnoreCase("-q")) {
				isRunning = false;
			} else {
				controller.handleUserData(students, command);
			}

		}


	}
}
