package by.baranov.simpleobjects.task7.controller;

import by.baranov.simpleobjects.task7.model.Triangle;

public class AppController {
	private final AppCommandFactory commandFactory;

	public AppController(AppCommandFactory factory) {
		this.commandFactory = factory;
	}
	public void handleUserData(Triangle triangle, String commnadUser) {
        final AppCommand command = commandFactory.getCommand(commnadUser);
        command.execute(triangle);
    }	
}
