package by.baranov.simpleobjects.task7.service;

import by.baranov.simpleobjects.task7.model.Triangle;

public class TriangleCheckServiceImpl implements TriangleCheckService{

	@Override
	public boolean checkPointsTriangle(Triangle triangle) {
		if(triangle.getFirstPoint().equals(triangle.getSecondPoint()) || 
				triangle.getFirstPoint().equals(triangle.getThirdPoint()) ||
						triangle.getSecondPoint().equals(triangle.getThirdPoint())) {
			return false;
		}
		return true;
	}
}
