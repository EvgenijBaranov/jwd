package by.baranov.simpleobjects.task6.service;

import by.baranov.simpleobjects.task6.model.Time;

public interface TimePrintService {
	void printTime(Time time);
}
