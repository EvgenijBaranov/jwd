package by.baranov.aggregationcomposition.task2.controller;

import java.util.Scanner;

import by.baranov.aggregationcomposition.task2.model.Car;
import by.baranov.aggregationcomposition.task2.service.CarService;

public class ChangeWheelCommand implements AppCommand {
	private final CarService carService;

	public ChangeWheelCommand(CarService carServcie) {
		this.carService = carServcie;
	}

	@Override
	public void execute(Car car) {

		boolean isIdWheelWrong = true;
		long idWheel = 0;
		while (isIdWheelWrong) {
			System.out.println("You need enter wheel id, that you want to change :");
			try {
				Scanner scan = new Scanner(System.in);
				idWheel = scan.nextLong();
				if (carService.isExistWheelForCar(car, idWheel)) {
					isIdWheelWrong = false;
				} else {
					System.out.println("This car hasn't wheel with such id. Try again.");
				}

			} catch (RuntimeException ex) {
				System.out.println("This car hasn't wheel with such id. Try again.");
			}
		}
		carService.changeWheel(car, idWheel);

	}

}
