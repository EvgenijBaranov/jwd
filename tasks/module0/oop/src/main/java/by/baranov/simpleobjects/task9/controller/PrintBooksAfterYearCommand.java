package by.baranov.simpleobjects.task9.controller;

import java.util.Scanner;

import by.baranov.simpleobjects.task9.model.Books;
import by.baranov.simpleobjects.task9.service.BookCheckService;
import by.baranov.simpleobjects.task9.service.BooksService;

public class PrintBooksAfterYearCommand implements AppCommand {
	private final BooksService booksService;
	private final BookCheckService bookCheckService;

	public PrintBooksAfterYearCommand(BooksService booksService, BookCheckService bookCheckService) {
		this.booksService = booksService;
		this.bookCheckService = bookCheckService;
	}

	@Override
	public void execute(Books books) {
		
		boolean isWrongYearPublication = true;
		int yearPublicationBook = 0;
		while(isWrongYearPublication) {
			System.out.println("Enter the year of publication of the book (int between ):");
			try {
				Scanner scan = new Scanner(System.in);
				yearPublicationBook = scan.nextInt();
				if(bookCheckService.checkPublishingYear(yearPublicationBook)) {
					isWrongYearPublication = false;
				}
			}catch (RuntimeException ex){
				System.out.println("You entered wrong year of publication. Try again");
			}
		}
		booksService.printBooksAfterYear(books, yearPublicationBook);
	}	
}
