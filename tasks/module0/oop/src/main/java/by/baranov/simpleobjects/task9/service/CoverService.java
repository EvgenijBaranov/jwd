package by.baranov.simpleobjects.task9.service;

import by.baranov.simpleobjects.task9.model.Cover;

public interface CoverService {
	Cover createCover(String cover);
}
