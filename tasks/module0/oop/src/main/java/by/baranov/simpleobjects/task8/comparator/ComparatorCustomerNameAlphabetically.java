package by.baranov.simpleobjects.task8.comparator;

import java.util.Comparator;

import by.baranov.simpleobjects.task8.model.Customer;

public class ComparatorCustomerNameAlphabetically implements Comparator<Customer> {

	@Override
	public int compare(Customer customer1, Customer customer2) {
		int resultComparisonBySurname = customer1.getSurname().compareTo(customer2.getSurname());
		if (resultComparisonBySurname != 0) {
			return resultComparisonBySurname;
		}

		int resultComparisonByName = customer1.getName().compareTo(customer2.getName());
		if (resultComparisonByName != 0) {
			return resultComparisonByName;
		}

		return customer1.getPatronymic().compareTo(customer2.getPatronymic());
	}

}
