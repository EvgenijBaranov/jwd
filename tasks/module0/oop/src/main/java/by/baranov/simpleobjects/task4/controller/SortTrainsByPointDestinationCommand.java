package by.baranov.simpleobjects.task4.controller;

import java.util.List;

import by.baranov.simpleobjects.task4.model.Train;
import by.baranov.simpleobjects.task4.service.TrainsSortService;

public class SortTrainsByPointDestinationCommand implements AppCommand{
	private TrainsSortService trainsSortService;	

	public SortTrainsByPointDestinationCommand(TrainsSortService trainsSortService) {
		this.trainsSortService = trainsSortService;		
	}

	@Override
	public void execute(List<Train> trains) {
		trainsSortService.sortTrainsByPointDestinationAndTimeDeparture(trains);
	
	}

}
