package by.baranov.simpleobjects.task2;

import by.baranov.simpleobjects.task2.model.Test2;

public class App {
	public static void main(String[] args) {
		System.out.println("\n_TASK-2_\n");
		Test2 test2Default = new Test2();
		System.out.println("The first default value : " + test2Default.getFirstValue() + ", " + "the second : " + test2Default.getSecondValue());
		Test2 task2 = new Test2(23, 43);
		System.out.println("The first value : " + task2.getFirstValue() + ", the second : " + task2.getSecondValue());

	}
}
