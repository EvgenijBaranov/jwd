package by.baranov.simpleobjects.task3.controller;

public interface AppCommandFactory {
	AppCommand getCommand(String commandName);
}
