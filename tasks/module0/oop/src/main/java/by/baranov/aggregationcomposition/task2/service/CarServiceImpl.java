package by.baranov.aggregationcomposition.task2.service;

import java.util.List;

import by.baranov.aggregationcomposition.task2.model.BrandCar;
import by.baranov.aggregationcomposition.task2.model.Car;
import by.baranov.aggregationcomposition.task2.model.Engine;
import by.baranov.aggregationcomposition.task2.model.PetrolTank;
import by.baranov.aggregationcomposition.task2.model.Speedometer;
import by.baranov.aggregationcomposition.task2.model.Wheel;

public class CarServiceImpl implements CarService {
	private final EngineService engineService;
	private final PetrolTankService petrolTankService;
	private final SpeedometerService speedometerService;

	public CarServiceImpl(EngineService engineService, PetrolTankService petrolTankService,
			SpeedometerService speedometerService) {
		
		this.engineService = engineService;
		this.petrolTankService = petrolTankService;
		this.speedometerService = speedometerService;
	}

	@Override
	public Car createCar(BrandCar brand, Engine engine, PetrolTank petrolTank, Speedometer speedometer,
			List<Wheel> wheels) {
		return new Car(brand, engine, petrolTank, speedometer, wheels);
	}

	@Override
	public boolean checkWheels(Car car) {
		List<Wheel> wheels = car.getWheels();
		if (wheels.size() != Car.NUMBERS_WHEEL) {
			return false;
		}
		for (int i = 0; i < wheels.size(); i++) {
			for (int j = i + 1; j < wheels.size(); j++) {
				if (wheels.get(i).getRadius() != wheels.get(j).getRadius()) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public void drive(Car car, double distance) {
		PetrolTank petrolTank = car.getPetrolTank();
		Engine engine = car.getEngine();
		Speedometer speedometer = car.getSpeedometer();
		if (petrolTankService.isEmpty(petrolTank)) {
			System.out.println("fill petrol tank");
			petrolTankService.fillPetrolTank(petrolTank);
		}
		if (engineService.isTurnedOff(engine)) {
			engineService.turnOn(engine);
		}
		double maxDistanceOnCurrentCapacityTank = speedometerService.calculateMaxDistance(petrolTank, engine);
		petrolTankService.calculateCurrentCapacity(petrolTank, distance, engine.getConsumptionPetrol());
		if (petrolTankService.isEmpty(petrolTank)) {
			distance = maxDistanceOnCurrentCapacityTank;
			engineService.turnOff(engine);			
		}
		speedometerService.addDistance(speedometer, distance);
		System.out.println(car.getBrand() + " drove " + speedometer.getDistance() + " kilometers");
	}

	@Override
	public void changeWheel(Car car, long idWheel) {
		List<Wheel> wheels = car.getWheels();
		if (isExistWheelForCar(car, idWheel)) {
			wheels.set((int) idWheel, new Wheel(wheels.get((int) idWheel).getRadius()));
		} else {
			System.out.println("Wheel with id = " + idWheel + " doesn't exist");
		}
	}

	@Override
	public boolean isExistWheelForCar(Car car, long idWheel) {
		List<Wheel> wheels = car.getWheels();
		for (Wheel wheel : wheels) {
			if (wheel.getId() == idWheel) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void fillPetrolTank(Car car) {
		petrolTankService.fillPetrolTank(car.getPetrolTank());
	}

	@Override
	public void printBrandCar(Car car) {
		System.out.println(car.getBrand());
	}

	@Override
	public void printCar(Car car) {
		System.out.println(car);		
	}

}
