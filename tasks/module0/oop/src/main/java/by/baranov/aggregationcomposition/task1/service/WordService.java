package by.baranov.aggregationcomposition.task1.service;

import by.baranov.aggregationcomposition.task1.model.Word;

public interface WordService {
	Word createWord(String word);
}
