package by.baranov.simpleobjects.task8.service;

import by.baranov.simpleobjects.task8.model.Customer;

public interface CustomerService {
	
	Customer createCustomer(String surname, String name, String patronymic, long creditCardNumber, long bankAccountNumber);
	
}
