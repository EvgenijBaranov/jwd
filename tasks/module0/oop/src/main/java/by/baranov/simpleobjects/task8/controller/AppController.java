package by.baranov.simpleobjects.task8.controller;

import by.baranov.simpleobjects.task8.model.Customers;

public class AppController {
	private final AppCommandFactory commandFactory;

	public AppController(AppCommandFactory factory) {
		this.commandFactory = factory;
	}
	public void handleUserData(Customers customers, String commnadUser) {
        final AppCommand command = commandFactory.getCommand(commnadUser);
        command.execute(customers);
    }	
}
