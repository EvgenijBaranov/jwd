package by.baranov.simpleobjects.task7.controller;

import by.baranov.simpleobjects.task7.model.Triangle;
import by.baranov.simpleobjects.task7.service.TriangleService;

public class CalculatePerimeterTriangleCommand implements AppCommand{
	private final TriangleService triangleService;
	
	public CalculatePerimeterTriangleCommand(TriangleService triangleService) {
		this.triangleService = triangleService;
	}

	@Override
	public void execute(Triangle triangle) {
		double perimeter = triangleService.calculatePerimeterTrinagle(triangle);
		System.out.println("Perimeter triangle : " + perimeter);		
	}
}
