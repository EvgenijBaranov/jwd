package by.baranov.simpleobjects.task4.controller;

public enum AppCommandName {
	PRINT_TRAINS("-p"),
	PRINT_TRAIN_BY_TRAIN_NUMBER("-pn"),
	SORT_TRAINS_BY_TRAIN_NUMBER("-sn"),
	SORT_TRAINS_BY_POINT_DESTINATION("-spd");	
	
	private String shortCommand;
	
	AppCommandName(String shortCommand){
		this.shortCommand = shortCommand;
	}
	
	 public static AppCommandName fromString(String name) {

	        final AppCommandName[] values = AppCommandName.values();
	        for (AppCommandName commandName : values) {
	            if (commandName.shortCommand.equals(name) || commandName.name().equals(name)) {
	            	System.out.println(commandName);
	                return commandName;
	            }
	        }
	        return null;
	 }

	public String getShortCommand() {
		return shortCommand;
	}
	 
}
