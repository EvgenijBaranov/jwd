package by.baranov.simpleobjects.task6.controller;

import by.baranov.simpleobjects.task6.model.Time;
import by.baranov.simpleobjects.task6.service.TimePrintService;

public class PrintTimeCommand implements AppCommand{
	private final TimePrintService printTimeService;

	public PrintTimeCommand(TimePrintService printTimeService) {
		this.printTimeService = printTimeService;
	}

	@Override
	public void execute(Time time) {
		printTimeService.printTime(time);		
	}
	
	
}
