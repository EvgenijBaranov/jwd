package by.baranov.simpleobjects.task10.service;

import by.baranov.simpleobjects.task10.model.Airlines;

public interface AirlinesCheckService {
	boolean checkInputAirlines(Airlines airlines);
}
