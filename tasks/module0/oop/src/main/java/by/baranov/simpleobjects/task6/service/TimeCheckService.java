package by.baranov.simpleobjects.task6.service;

import by.baranov.simpleobjects.task6.model.Time;

public interface TimeCheckService {
	boolean checkAddHours(Time time, int quantityHours);

	boolean checkAddMinutes(Time time, int quantityMinutes);

	boolean checkAddSeconds(Time time, int quantitySeconds);

	boolean checkHours(int quantityHours);

	boolean checkMinutes(int quantityMinutes);

	boolean checkSeconds(int quantitySeconds);
}
