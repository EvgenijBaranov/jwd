package by.baranov.simpleobjects.task10.controller;

import by.baranov.simpleobjects.task10.model.Airlines;

public interface AppCommand {
	void execute(Airlines airlines);
}
