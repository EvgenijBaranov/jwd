package by.baranov.simpleobjects.task4.controller;

import java.util.List;

import by.baranov.simpleobjects.task4.model.Train;

public interface AppCommand {
	void execute(List<Train> trains);
}
