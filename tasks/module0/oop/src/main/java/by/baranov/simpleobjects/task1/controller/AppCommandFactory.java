package by.baranov.simpleobjects.task1.controller;

public interface AppCommandFactory {
	AppCommand getCommand(String commandName);
}
