package by.baranov.simpleobjects.task3.controller;

import java.util.List;
import by.baranov.simpleobjects.task3.model.Student;

public interface AppCommand {
	void execute(List<Student> students);
}
