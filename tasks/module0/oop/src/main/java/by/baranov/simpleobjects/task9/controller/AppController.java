package by.baranov.simpleobjects.task9.controller;

import by.baranov.simpleobjects.task9.model.Books;

public class AppController {
	private final AppCommandFactory commandFactory;

	public AppController(AppCommandFactory factory) {
		this.commandFactory = factory;
	}
	public void handleUserData(Books books, String commnadUser) {
        final AppCommand command = commandFactory.getCommand(commnadUser);
        command.execute(books);
    }	
}
