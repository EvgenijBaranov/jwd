package by.baranov.simpleobjects.task1.service;

import by.baranov.simpleobjects.task1.model.Test1;

public class Test1ServiceImpl implements Test1Service {

	public void changeValues(Test1 test, double firstNumber, double secondNumber) {
		test.setFirstValue(firstNumber);
		test.setSecondValue(secondNumber);
	}

	public double sumValues(Test1 test) {
		return test.getFirstValue() + test.getSecondValue();
	}

	public double maxValue(Test1 test) {
		return Math.max(test.getFirstValue(), test.getSecondValue());
	}

}
