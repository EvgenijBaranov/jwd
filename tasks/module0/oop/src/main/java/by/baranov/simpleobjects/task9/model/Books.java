package by.baranov.simpleobjects.task9.model;

import java.util.Arrays;

public class Books {
	private Book[] books;

	public Books(Book... books) {
		this.books = books;
	}

	public Book[] getBooks() {
		return books;
	}

	public void setBooks(Book[] books) {
		this.books = books;
	}

	@Override
	public String toString() {
		return "Books [" + Arrays.toString(books) + "]";
	}
}
