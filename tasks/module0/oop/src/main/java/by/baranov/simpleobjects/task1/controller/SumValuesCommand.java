package by.baranov.simpleobjects.task1.controller;

import by.baranov.simpleobjects.task1.model.Test1;
import by.baranov.simpleobjects.task1.service.Test1Service;

public class SumValuesCommand implements AppCommand{

	private final Test1Service test1Service;
	
	public SumValuesCommand(Test1Service test1Service) {
		this.test1Service = test1Service;
	}

	@Override
	public void execute(Test1 test) {
		
		System.out.println(test1Service.sumValues(test));
		
	}

}
