package by.baranov.simpleobjects.task6.controller;

public interface AppCommandFactory {
	AppCommand getCommand(String commandName);
}
