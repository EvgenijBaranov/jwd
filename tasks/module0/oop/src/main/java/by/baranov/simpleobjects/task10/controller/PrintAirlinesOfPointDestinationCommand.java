package by.baranov.simpleobjects.task10.controller;

import java.util.Scanner;

import by.baranov.simpleobjects.task10.model.Airlines;
import by.baranov.simpleobjects.task10.service.AirlineCheckService;
import by.baranov.simpleobjects.task10.service.AirlinesService;

public class PrintAirlinesOfPointDestinationCommand implements AppCommand {
	private final AirlinesService airlinesService;
	private final AirlineCheckService airlineCheckService;

	public PrintAirlinesOfPointDestinationCommand(AirlinesService airlinesService,
			AirlineCheckService airlineCheckService) {
		this.airlinesService = airlinesService;
		this.airlineCheckService = airlineCheckService;
	}

	@Override
	public void execute(Airlines airlines) {
		
		boolean isNotEnterPointDestination = true;
		String pointDestination = null;
		while (isNotEnterPointDestination) {
			System.out.println("You need to enter point destination :");
			Scanner scan = new Scanner(System.in);
			pointDestination = scan.nextLine();
			if (airlineCheckService.isStringNotEmpty(pointDestination)) {
				isNotEnterPointDestination = false;
			} else {
				System.out.println("You entered wrong point destination. Try again.");
			}
		}
		airlinesService.printAirlinesIn(airlines, pointDestination);
	}
}
