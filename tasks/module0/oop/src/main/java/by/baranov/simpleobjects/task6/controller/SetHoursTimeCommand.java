package by.baranov.simpleobjects.task6.controller;

import java.util.Scanner;

import by.baranov.simpleobjects.task6.model.Time;
import by.baranov.simpleobjects.task6.service.TimeCheckService;
import by.baranov.simpleobjects.task6.service.TimeSetService;

public class SetHoursTimeCommand implements AppCommand {
	private final  TimeSetService timeSetService;
	private final  TimeCheckService timeCheckService;	
	
	public SetHoursTimeCommand(TimeSetService timeSetService, TimeCheckService timeCheckService) {
		this.timeSetService = timeSetService;
		this.timeCheckService = timeCheckService;
	}

	@Override
	public void execute(Time time) {
		System.out.println("Enter hours : ");
		boolean isRunning = true;
		int inputedHours = 0;
		while(isRunning) {
			Scanner scan = new Scanner(System.in);
			try {
				inputedHours = scan.nextInt();
				isRunning = false;
			}catch(RuntimeException e) {
				System.out.println("You must input integer number, try again :");
			}			
		}
		if(timeCheckService.checkHours(inputedHours)) {
			timeSetService.setHours(time, inputedHours);
		} else {
			System.out.println("Invalid hours input, hours must be in the range from " + Time.MIN_BOUND_HOURS
					+ " to " + Time.MAX_BOUND_HOURS + " , hours are set to " + Time.DEFAULT_VALUE_FOR_WRONG_SET);
			timeSetService.setHours(time, Time.DEFAULT_VALUE_FOR_WRONG_SET);
		}
	}
}
