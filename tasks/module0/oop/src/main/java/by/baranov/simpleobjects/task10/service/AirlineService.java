package by.baranov.simpleobjects.task10.service;

import java.time.LocalTime;

import by.baranov.simpleobjects.task10.model.Airline;
import by.baranov.simpleobjects.task10.model.Day;
import by.baranov.simpleobjects.task10.model.TypeAircraft;

public interface AirlineService {
	Airline createAirline(String pointDestination, String flightNumber, TypeAircraft typeAircraft, 
			LocalTime timeDeparture, Day... daysDeparture);
}
