package by.baranov.aggregationcomposition.task2.model;

import java.time.LocalDate;

public class Engine {
	private final double consumptionPetrol;
	private static long counter = 0;
	private final long id = counter++;
	private final double power;
	private final LocalDate dateOfManufacture;
	private boolean isTurnedOn = false;

	public Engine(double power, LocalDate dateOfManufacture, double consumptionPetrol) {
		this.power = power;
		this.dateOfManufacture = dateOfManufacture;
		this.consumptionPetrol = consumptionPetrol;
	}

	public double getPower() {
		return power;
	}

	public LocalDate getDateOfManufacture() {
		return dateOfManufacture;
	}

	public boolean isTurnedOn() {
		return isTurnedOn;
	}

	public void setTurnedOn(boolean isTurnedOn) {
		this.isTurnedOn = isTurnedOn;
	}

	public double getConsumptionPetrol() {
		return consumptionPetrol;
	}

	@Override
	public String toString() {
		return "Engine [id=" + id + ", power=" + power + ", dateOfManufacture=" + dateOfManufacture + "]";
	}

}
