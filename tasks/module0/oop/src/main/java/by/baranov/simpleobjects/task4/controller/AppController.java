package by.baranov.simpleobjects.task4.controller;

import java.util.List;

import by.baranov.simpleobjects.task4.model.Train;

public class AppController {
	private final AppCommandFactory commandFactory;

	public AppController(AppCommandFactory factory) {
		this.commandFactory = factory;
	}
	public void handleUserData(List<Train> trains, String commnadUser) {
        final AppCommand command = commandFactory.getCommand(commnadUser);
        command.execute(trains);
    }	
}
