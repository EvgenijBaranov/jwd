package by.baranov.aggregationcomposition.task1.controller;

import by.baranov.aggregationcomposition.task1.model.Text;

public class AppController {
	private final AppCommandFactory commandFactory;

	public AppController(AppCommandFactory factory) {
		this.commandFactory = factory;
	}
	public void handleUserData(Text text, String commnadUser) {
        final AppCommand command = commandFactory.getCommand(commnadUser);
        command.execute(text);
    }	
}
