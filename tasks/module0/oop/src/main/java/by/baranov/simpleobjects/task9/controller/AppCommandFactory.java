package by.baranov.simpleobjects.task9.controller;

public interface AppCommandFactory {
	AppCommand getCommand(String commandName);
}
