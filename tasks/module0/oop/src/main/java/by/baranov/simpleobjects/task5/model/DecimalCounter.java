package by.baranov.simpleobjects.task5.model;

import java.util.Random;

public class DecimalCounter {
	public static int DEFAULT_VALUE_FOR_CHANGE = 1;
	public static int MAX_VALUE = 99;
	public static int MIN_VALUE = 0;
	private int currentValue;
	private Random rand = new Random();

	public DecimalCounter() {
		this.currentValue = MIN_VALUE;
	}

	public DecimalCounter(int upperBoundRandomValue) {
		this.currentValue = rand.nextInt(upperBoundRandomValue);
	}
	
	public int getCurrentValue() {
		return currentValue;
	}

	public void setCurrentValue(int currentValue) {
		this.currentValue = currentValue;
	}

	@Override
	public String toString() {
		return "DecimalCounter [currentValue=" + currentValue + "]";
	}	

}
