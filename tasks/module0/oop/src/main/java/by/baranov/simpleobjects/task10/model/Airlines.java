package by.baranov.simpleobjects.task10.model;

public class Airlines {
	private Airline[] airlines;

	public Airlines(Airline... airlines) {
		this.airlines = airlines;
	}

	public Airline[] getAirlines() {
		return airlines;
	}

	public void setAirlines(Airline[] airlines) {
		this.airlines = airlines;
	}

}
