package by.baranov.aggregationcomposition.task1.service;

import java.util.List;

import by.baranov.aggregationcomposition.task1.model.Sentence;
import by.baranov.aggregationcomposition.task1.model.Text;

public interface TextService {
	
	Text createText(Sentence title, List<Sentence> sentences);
	
	void addSentence(Text text, Sentence sentence);
	
	void changeTitle(Text text, Sentence newTitle);
	
	void printText(Text text);
	
	void printTitle(Text text);
}
