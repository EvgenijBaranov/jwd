package by.baranov.simpleobjects.task9.service;

import java.time.LocalDateTime;

import by.baranov.simpleobjects.task9.model.Book;
import by.baranov.simpleobjects.task9.model.Cover;

public class BookCheckServiceImpl implements BookCheckService {

	@Override
	public boolean checkInputDataBook(String title, String publisher, int publishingYear, int quantityPages,
			String price, Cover typeCover, String[] namesAuthors) {
		boolean isRightInputTitle = isStringNotEmpty(title);
		boolean isRightInputPublisher = isStringNotEmpty(publisher);
		boolean isRightInputPublishingYear = checkPublishingYear(publishingYear);
		boolean isRightInputQuantityPages = isExistPages(quantityPages);
		boolean isRightInputPrice = isPrice(price);
		boolean isRightInputCover = isCover(typeCover);
		boolean isRightInputAuthors = isStringNotEmpty(namesAuthors);

		boolean resultCheckInputedData = isRightInputTitle && isRightInputPublisher && isRightInputPublishingYear
											&& isRightInputQuantityPages && isRightInputPrice && isRightInputCover && isRightInputAuthors;

		return resultCheckInputedData;
	}
	
	@Override
	public boolean checkPublishingYear(int publishingYear) {
		return publishingYear >= Book.YEAR_PUBLCATION_FIRST_BOOK && publishingYear <= LocalDateTime.now().getYear() ? true : false;		
	}
	
	private boolean isPrice(String price) {
		try {
			Double.parseDouble(price);
			return true;
		} catch (RuntimeException ex) {
		}
		return false;
	}

	private boolean isStringNotEmpty(String... strings) {
		for (String string : strings) {
			if (string.isEmpty() || string == null) {
				return false;
			}
		}
		return true;
	}

	private boolean isExistPages(int quantityPages) {
		return quantityPages > 1 ? true : false;
	}

	private boolean isCover(Cover typeCover) {
		if(typeCover != null) {
			for(Cover cover : Cover.values()) {
				if(cover.equals(typeCover)) {
					return true;
				}
			}
		}		
		return false;
	}	

}
