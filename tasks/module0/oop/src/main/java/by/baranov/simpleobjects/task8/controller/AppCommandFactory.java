package by.baranov.simpleobjects.task8.controller;

public interface AppCommandFactory {
	AppCommand getCommand(String commandName);
}
