package by.baranov.aggregationcomposition.task2.service;

import by.baranov.aggregationcomposition.task2.model.PetrolTank;

public class PetrolTankServiceImpl implements PetrolTankService {

	@Override
	public PetrolTank createPetrolTank(double capacity) {
		return new PetrolTank(capacity);
	}

	@Override
	public boolean checkInputDataForPetrolTank(PetrolTank petrolTank) {
		return petrolTank.getCapacity() >= 0 ? true : false;
	}

	@Override
	public void fillPetrolTank(PetrolTank petrolTank) {
		double capacity = petrolTank.getCapacity();
		petrolTank.setCurrentCapacity(capacity);
		System.out.println("The petrol tank is full.");
	}

	@Override
	public boolean isEmpty(PetrolTank petrolTank) {
		return petrolTank.getCurrentCapacity() == 0;
	}

	@Override
	public void calculateCurrentCapacity(PetrolTank petrolTank, double distance, double consumptionPetrol) {
		double currentCapacity = petrolTank.getCurrentCapacity() - distance * consumptionPetrol / 100;
		if (currentCapacity <= 0) {
			System.out.println("Petrol tank is empty");
			currentCapacity = 0;
		} else {
			System.out.println("There are " + currentCapacity + " liters of fuel in petrol tank");
		}
		petrolTank.setCurrentCapacity(currentCapacity);
	}

}
