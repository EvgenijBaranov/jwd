package by.baranov.simpleobjects.task6.controller;

import java.util.Scanner;

import by.baranov.simpleobjects.task6.model.Time;
import by.baranov.simpleobjects.task6.service.TimeAddService;
import by.baranov.simpleobjects.task6.service.TimeCheckService;

public class IncreaseHoursCommand implements AppCommand {
	private final TimeAddService timeAddService;
	private final TimeCheckService timeCheckService;

	public IncreaseHoursCommand(TimeAddService timeAddService, TimeCheckService timeCheckService) {
		super();
		this.timeAddService = timeAddService;
		this.timeCheckService = timeCheckService;
	}

	@Override
	public void execute(Time time) {
		System.out.println("Enter quantity hours, which you want to add: ");
		boolean isRunning = true;
		int quantityHours = 0;
		while (isRunning) {
			Scanner scan = new Scanner(System.in);
			try {
				quantityHours = scan.nextInt();
				isRunning = false;
			} catch (RuntimeException e) {
				System.out.println("You must input integer number, try again :");
			}
		}
		if (timeCheckService.checkAddHours(time, quantityHours)) {
			timeAddService.addHours(time, quantityHours);
		} else {
			System.out.println("Invalid hours input, hours must be in the range " + "from " + Time.MIN_BOUND_HOURS
					+ " to " + Time.MAX_BOUND_HOURS + " , hours are set to " + Time.DEFAULT_VALUE_FOR_WRONG_SET);
			time.setCurrentHours(Time.DEFAULT_VALUE_FOR_WRONG_SET);
		}
	}

}
