package by.baranov.simpleobjects.task6.service;

import by.baranov.simpleobjects.task6.model.Time;

public class TimePrintServiceImpl implements TimePrintService{

	@Override
	public void printTime(Time time) {
		System.out.println(time);		
	}
}
