package by.baranov.simpleobjects.task7.service;

import by.baranov.simpleobjects.task7.model.Triangle;

public interface TrianglePrintService {
	
	void printTriangle(Triangle triangle);
}
