package by.baranov.simpleobjects.task4;

import java.time.LocalTime;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import by.baranov.simpleobjects.task4.controller.AppCommand;
import by.baranov.simpleobjects.task4.controller.AppCommandFactory;
import by.baranov.simpleobjects.task4.controller.AppCommandFactoryImpl;
import by.baranov.simpleobjects.task4.controller.AppCommandName;
import by.baranov.simpleobjects.task4.controller.AppController;
import by.baranov.simpleobjects.task4.controller.PrintAllTrainsCommand;
import by.baranov.simpleobjects.task4.controller.PrintTrainByNumberCommand;
import by.baranov.simpleobjects.task4.controller.SortTrainsByNumberCommand;
import by.baranov.simpleobjects.task4.controller.SortTrainsByPointDestinationCommand;
import by.baranov.simpleobjects.task4.model.Train;
import by.baranov.simpleobjects.task4.service.TrainsPrintService;
import by.baranov.simpleobjects.task4.service.TrainsPrintServiceImpl;
import by.baranov.simpleobjects.task4.service.TrainsSortService;
import by.baranov.simpleobjects.task4.service.TrainsSortServiceImpl;

public class App {

	public static void main(String[] args) {
		
		System.out.println("\n_TASK-4_\n");
		
		TrainsPrintService printAllTrainsService = new TrainsPrintServiceImpl();
		TrainsPrintService printTrainByNumberTrainService = new TrainsPrintServiceImpl();
		TrainsSortService sortTrainsByNumberTrainService = new TrainsSortServiceImpl();
		TrainsSortService sortTrainsByNumberPointDestinationService = new TrainsSortServiceImpl();

		AppCommand printAllTrainsCommand = new PrintAllTrainsCommand(printAllTrainsService);
		AppCommand printTrainByNumberCommand = new PrintTrainByNumberCommand(printTrainByNumberTrainService);
		AppCommand sortTrainsByNumberTrainCommand = new SortTrainsByNumberCommand(sortTrainsByNumberTrainService);
		AppCommand sortTrainsByPointDestinationCommand = new SortTrainsByPointDestinationCommand(sortTrainsByNumberPointDestinationService);

		Map<AppCommandName, AppCommand> commands = new EnumMap<>(AppCommandName.class);
		commands.put(AppCommandName.PRINT_TRAINS, printAllTrainsCommand);
		commands.put(AppCommandName.PRINT_TRAIN_BY_TRAIN_NUMBER, printTrainByNumberCommand);
		commands.put(AppCommandName.SORT_TRAINS_BY_POINT_DESTINATION, sortTrainsByPointDestinationCommand);
		commands.put(AppCommandName.SORT_TRAINS_BY_TRAIN_NUMBER, sortTrainsByNumberTrainCommand);

		AppCommandFactory factory = new AppCommandFactoryImpl(commands);
		AppController controller = new AppController(factory);

		
		Train train1 = new Train("B100","Minsk", LocalTime.of(10, 00));
		Train train2 = new Train("A110","Polotsk", LocalTime.of(9, 40));
		Train train3 = new Train("D100","Minsk", LocalTime.of(7, 20));
		Train train4 = new Train("Q100","Minsk", LocalTime.of(12, 00));
		Train train5 = new Train("P100","Grodno", LocalTime.of(10, 00));
		List<Train> trains = Arrays.asList(new Train[] {train1, train2, train3, train4, train5});

		boolean isRunning = true;
		while (isRunning) {
			System.out.println();
			System.out.println(AppCommandName.PRINT_TRAIN_BY_TRAIN_NUMBER + 
								" or " + AppCommandName.PRINT_TRAIN_BY_TRAIN_NUMBER.getShortCommand());
			System.out.println(AppCommandName.PRINT_TRAINS + 
								" or " + AppCommandName.PRINT_TRAINS.getShortCommand());
			System.out.println(AppCommandName.SORT_TRAINS_BY_POINT_DESTINATION + 
								" or " + AppCommandName.SORT_TRAINS_BY_POINT_DESTINATION.getShortCommand());
			System.out.println(AppCommandName.SORT_TRAINS_BY_TRAIN_NUMBER + 
								" or " + AppCommandName.SORT_TRAINS_BY_TRAIN_NUMBER.getShortCommand());
			System.out.println("for quit -  "+ "-q");
			System.out.println("Your command is :");
			Scanner scan = new Scanner(System.in);
			String command = scan.next();
			if (command.equalsIgnoreCase("-q")) {
				isRunning = false;
			} else {
				controller.handleUserData(trains, command);
			}

		}

	}

}
