package by.baranov.simpleobjects.task9.controller;

import by.baranov.simpleobjects.task9.model.Books;
import by.baranov.simpleobjects.task9.service.BooksService;

public class PrintAllBooksCommand implements AppCommand {
	private final BooksService booksService;

	public PrintAllBooksCommand(BooksService booksService) {
		this.booksService = booksService;
	}

	@Override
	public void execute(Books books) {
		booksService.printAllBooks(books);
	}
}
