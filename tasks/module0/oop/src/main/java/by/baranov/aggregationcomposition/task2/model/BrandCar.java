package by.baranov.aggregationcomposition.task2.model;

public enum BrandCar {
	FORD, TOYOTA, OPEL, VOLVO, VOLKSWAGEN
}
