package by.baranov.simpleobjects.task7.controller;

import by.baranov.simpleobjects.task7.model.Triangle;
import by.baranov.simpleobjects.task7.service.TrianglePrintService;

public class PrintTriangleCommand implements AppCommand{
	private final TrianglePrintService printTriangleService;	

	public PrintTriangleCommand(TrianglePrintService printTriangleService) {
		this.printTriangleService = printTriangleService;
	}

	@Override
	public void execute(Triangle triangle) {
		printTriangleService.printTriangle(triangle);
	}
}
