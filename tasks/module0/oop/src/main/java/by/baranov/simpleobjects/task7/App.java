package by.baranov.simpleobjects.task7;

import java.util.EnumMap;
import java.util.Map;
import java.util.Scanner;

import by.baranov.simpleobjects.task7.controller.AppCommand;
import by.baranov.simpleobjects.task7.controller.AppCommandFactory;
import by.baranov.simpleobjects.task7.controller.AppCommandFactoryImpl;
import by.baranov.simpleobjects.task7.controller.AppCommandName;
import by.baranov.simpleobjects.task7.controller.AppController;
import by.baranov.simpleobjects.task7.controller.CalculatePerimeterTriangleCommand;
import by.baranov.simpleobjects.task7.controller.DefinePointIntersectionMediansTriangleCommand;
import by.baranov.simpleobjects.task7.controller.PrintTriangleCommand;
import by.baranov.simpleobjects.task7.model.Point;
import by.baranov.simpleobjects.task7.model.Triangle;
import by.baranov.simpleobjects.task7.service.CheckPointsForTriangleService;
import by.baranov.simpleobjects.task7.service.CheckPointsForTriangleServiceImpl;
import by.baranov.simpleobjects.task7.service.PointService;
import by.baranov.simpleobjects.task7.service.PointServiceImpl;
import by.baranov.simpleobjects.task7.service.TriangleCheckService;
import by.baranov.simpleobjects.task7.service.TriangleCheckServiceImpl;
import by.baranov.simpleobjects.task7.service.TrianglePrintService;
import by.baranov.simpleobjects.task7.service.TrianglePrintServiceImpl;
import by.baranov.simpleobjects.task7.service.TriangleService;
import by.baranov.simpleobjects.task7.service.TriangleServiceImpl;

public class App {

	public static void main(String[] args) {

		System.out.println("\n_TASK-7_\n");

		PointService pointCreateService = new PointServiceImpl();
		CheckPointsForTriangleService checkPointsSrevice = new CheckPointsForTriangleServiceImpl();
		TriangleService createTriangleService = new TriangleServiceImpl();
		TriangleCheckService checkTriangleService = new TriangleCheckServiceImpl();

		TrianglePrintService printTriangleService = new TrianglePrintServiceImpl();
		TriangleService calculatePerimeterTriangleService = new TriangleServiceImpl();
		TriangleService calculateSquareTriangleService = new TriangleServiceImpl();
		TriangleService definePointIntersectionMediansTriangleService = new TriangleServiceImpl();

		AppCommand printTriangleCommand = new PrintTriangleCommand(printTriangleService);
		AppCommand calculatePerimeterTriangleCommand = new CalculatePerimeterTriangleCommand(
				calculatePerimeterTriangleService);
		AppCommand calculateSquareTriangleCommand = new CalculatePerimeterTriangleCommand(
				calculateSquareTriangleService);
		AppCommand definePointIntersectionMediansTriangleCommand = new DefinePointIntersectionMediansTriangleCommand(
				definePointIntersectionMediansTriangleService);

		Map<AppCommandName, AppCommand> commands = new EnumMap<>(AppCommandName.class);
		commands.put(AppCommandName.CALCULATE_PERIMETER_TRIANGLE, calculatePerimeterTriangleCommand);
		commands.put(AppCommandName.CALCULATE_SQUARE_TRIANGLE, calculateSquareTriangleCommand);
		commands.put(AppCommandName.DEFINE_POINT_INTERSECTION_MEDIANS_TRIANGLE, definePointIntersectionMediansTriangleCommand);
		commands.put(AppCommandName.PRINT_TRIANGLE, printTriangleCommand);

		AppCommandFactory factory = new AppCommandFactoryImpl(commands);
		AppController controller = new AppController(factory);

		boolean isPointsNotEntered = true;
		Point firstPoint = null;
		Point secondPoint = null;
		Point thirdPoint = null;
		Point tempPoint = null;
		System.out.println("For creation triangle you need input three points.");
		while (isPointsNotEntered) {
			
			for (int i = 0; i < 3; i++) {
				System.out.println("Creation point number : " + (i + 1));
				System.out.println("Enter x and y:");
				boolean isPointNotEntered = true;
				while(isPointNotEntered){
					try{
						System.out.println("x:");
						Scanner scan = new Scanner(System.in);
						double x = scan.nextDouble();
						System.out.println("y:");
						double y = scan.nextDouble();
						isPointNotEntered = false;
						tempPoint = pointCreateService.createPoint(x, y);
					}catch(RuntimeException e){
						System.out.println("You need to enter double numbers x nad y. Try again.");
					}
				}
				if(i == 0) {
					firstPoint = tempPoint;
				}else if(i == 1) {
					secondPoint = tempPoint;
				}else {
					thirdPoint = tempPoint;
				}
			}
			if (checkPointsSrevice.checkPointsTriangle(firstPoint, secondPoint, thirdPoint)) {
				isPointsNotEntered = false;
			} else {
				System.out.println("You need to enter deifferent three point. Try again");
			}
		}
		Triangle triangle = createTriangleService.createTriangle(firstPoint, secondPoint, thirdPoint);
		
		boolean isRunning = true;
		while (isRunning) {
			System.out.println();
			System.out.println(AppCommandName.CALCULATE_PERIMETER_TRIANGLE + " or "
					+ AppCommandName.CALCULATE_PERIMETER_TRIANGLE.getShortCommand());
			System.out.println(AppCommandName.CALCULATE_SQUARE_TRIANGLE + " or "
					+ AppCommandName.CALCULATE_SQUARE_TRIANGLE.getShortCommand());
			System.out.println(AppCommandName.DEFINE_POINT_INTERSECTION_MEDIANS_TRIANGLE + " or "
					+ AppCommandName.DEFINE_POINT_INTERSECTION_MEDIANS_TRIANGLE.getShortCommand());
			System.out
					.println(AppCommandName.PRINT_TRIANGLE + " or " + AppCommandName.PRINT_TRIANGLE.getShortCommand());
			System.out.println("for quit -  " + "-q");
			System.out.println("Your command is :");
			Scanner scan = new Scanner(System.in);
			String command = scan.next();
			if (command.equalsIgnoreCase("-q")) {
				isRunning = false;
			} else {
				controller.handleUserData(triangle, command);
			}
		}
	}
}
