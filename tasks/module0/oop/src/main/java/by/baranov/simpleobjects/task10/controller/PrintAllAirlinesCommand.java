package by.baranov.simpleobjects.task10.controller;

import by.baranov.simpleobjects.task10.model.Airlines;
import by.baranov.simpleobjects.task10.service.AirlinesService;

public class PrintAllAirlinesCommand implements AppCommand {
	private final AirlinesService airlinesService;

	public PrintAllAirlinesCommand(AirlinesService airlinesService) {
		this.airlinesService = airlinesService;
	}

	@Override
	public void execute(Airlines airlines) {
		airlinesService.printAllAirlines(airlines);
	}
}
