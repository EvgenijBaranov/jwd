package by.baranov.simpleobjects.task7.service;

import by.baranov.simpleobjects.task7.model.Point;
import by.baranov.simpleobjects.task7.model.Triangle;

public interface TriangleService {

	Triangle createTriangle(Point firstPoint, Point secondPoint, Point thirdPoint);

	double calculateSquareTrinagle(Triangle triangle);

	double calculatePerimeterTrinagle(Triangle triangle);

	Point definePointIntersectionMedians(Triangle triangle);

}
