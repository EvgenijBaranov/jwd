package by.baranov.simpleobjects.task9.service;

import by.baranov.simpleobjects.task9.model.Book;
import by.baranov.simpleobjects.task9.model.Books;

public class BooksServiceImpl implements BooksService {

	@Override
	public Books createBooks(Book... books) {
		Book[] arrayBooks = books;
		return new Books(arrayBooks);
	}

	@Override
	public void printBooksOfAuthor(Books books, String author) {
		Book[] arrayBooks = books.getBooks();
		for (Book book : arrayBooks) {
			for (String authorBook : book.getNamesAuthors()) {
				if (authorBook.compareToIgnoreCase(author) == 0) {
					System.out.println(book);
				}
			}
		}
	}

	@Override
	public void printBooksOfPublisher(Books books, String publisher) {
		Book[] arrayBooks = books.getBooks();
		for (Book book : arrayBooks) {
			if (book.getPublisher().compareToIgnoreCase(publisher) == 0) {
				System.out.println(book);
			}
		}
	}

	@Override
	public void printBooksAfterYear(Books books, int year) {
		Book[] arrayBooks = books.getBooks();
		for (Book book : arrayBooks) {
			if (book.getPublishingYear() > year) {
				System.out.println(book);
			}
		}
	}

	@Override
	public void printAllBooks(Books books) {
		System.out.println(books);
	}
}
