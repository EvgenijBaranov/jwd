package by.baranov.simpleobjects.task9.model;

import java.math.BigDecimal;
import java.util.Arrays;

public class Book {
	public static final int YEAR_PUBLCATION_FIRST_BOOK = 1455;
	private static long counter = 0;
	private final long id = counter++;
	private String title;
	private String[] namesAuthors;
	private String publisher;
	private int publishingYear;
	private int quantityPages;
	private BigDecimal price;
	private Cover typeCover;

	public Book(String title, String publisher, int publishingYear, int quantityPages,
				String price, Cover typeCover, String... namesAuthors) {

		this.title = title;
		this.namesAuthors = namesAuthors;
		this.publisher = publisher;
		this.publishingYear = publishingYear;
		this.quantityPages = quantityPages;
		this.price = new BigDecimal(price);
		this.typeCover = typeCover;
	}

	public String getTitle() {
		return title;
	}

	public String[] getNamesAuthors() {
		return namesAuthors;
	}

	public String getPublisher() {
		return publisher;
	}

	public int getPublishingYear() {
		return publishingYear;
	}

	public int getQuantityPages() {
		return quantityPages;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public Cover getTypeCover() {
		return typeCover;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setNamesAuthors(String[] namesAuthors) {
		this.namesAuthors = namesAuthors;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public void setPublishingYear(int publishingYear) {
		this.publishingYear = publishingYear;
	}

	public void setQuantityPages(int quantityPages) {
		this.quantityPages = quantityPages;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public void setTypeCover(Cover typeCover) {
		this.typeCover = typeCover;
	}

	@Override
	public String toString() {
		return "\nBook [id=" + id + ", " + title + ", " + Arrays.toString(namesAuthors) + ", " + publisher + ", "
				+ publishingYear + ", " + quantityPages + ", " + price + ", " + typeCover + "]";
	}
}
