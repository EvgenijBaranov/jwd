package by.baranov.simpleobjects.task6.service;

import by.baranov.simpleobjects.task6.model.Time;

public class TimeSetServiceImpl implements TimeSetService {

	@Override
	public void setHours(Time time, int hour) {
		time.setCurrentHours(hour);
	}

	@Override
	public void setMinutes(Time time, int minute) {
		time.setCurrentMinutes(minute);
	}

	@Override
	public void setSeconds(Time time, int second) {
		time.setCurrentSeconds(second);
	}

}
