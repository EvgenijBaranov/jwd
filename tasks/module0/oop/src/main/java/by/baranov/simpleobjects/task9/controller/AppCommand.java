package by.baranov.simpleobjects.task9.controller;

import by.baranov.simpleobjects.task9.model.Books;

public interface AppCommand {
	void execute(Books books);
}
