package by.baranov.aggregationcomposition.task1.service;

import java.util.List;

import by.baranov.aggregationcomposition.task1.model.Sentence;
import by.baranov.aggregationcomposition.task1.model.Word;

public interface SentenceService {

	Sentence createSentence(List<Word> words);

	void addWord(Sentence sentence, Word word);

	List<Word> getAllWords(Sentence sentence);
	
	void printSentence(Sentence sentence);
}
