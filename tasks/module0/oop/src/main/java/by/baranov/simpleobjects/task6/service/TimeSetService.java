package by.baranov.simpleobjects.task6.service;

import by.baranov.simpleobjects.task6.model.Time;

public interface TimeSetService {

	void setHours(Time time, int hour);

	void setMinutes(Time time, int minute);

	void setSeconds(Time time, int second);
}
