package by.baranov.aggregationcomposition.task2.controller;

import by.baranov.aggregationcomposition.task2.model.Car;
import by.baranov.aggregationcomposition.task2.service.CarService;

public class PrintCarCommand implements AppCommand{
	private final CarService carService;
	
	public PrintCarCommand(CarService carService) {
		this.carService = carService;
	}

	@Override
	public void execute(Car car) {
		carService.printCar(car);		
	}

}
